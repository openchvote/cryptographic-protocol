# Release Notes

## Version 2.4.2

#### Bugs

- Improper test in Line 84 of methods `ch.openchvote.core.electionauthority.plain.statesS400::handleMEE3`
  and `ch.openchvote.core.electionauthority.writein.statesS400::handleMEE3` replaced. Similar replacement in Lines 71
  and 72 in `ch.openchvote.core.electionauthority.plain.statesS410::handleMEE3`
  and `ch.openchvote.core.electionauthority.writein.statesS410::handleMEE3`, respectively (issue brought up by Véronique
  Cortier, Alexandre Debant, and Pierrick Gaudry).

## Version 2.4.1

#### Bugs

- HashMaps in `RandomEventDefinitionService` replaced by ConcurrentHashMaps, after observing occasional test failures in
  rare cases.

## Version 2.4

#### Changes

- Maven project modules grouped into submodules `base`, `core`, `javascript`, and `simulation`.
- Some POM.xml files of affected modules simplified (`default` profile removed).
- Logger levels shifted one level down for making `Level.Info` the default level for the simulator.
- Logger messages simplified and unified.
- Module `algorithms-js-service` renamed into `services-js`.
- Class `Tuple` extended with static factory method.

## Version 2.3.1

#### Changes

- Change of default order of building and execution: Now default is without JavaScript algorithms.
- Excluding of library with vulnerability `org.apache.commons:commons-compress` in
  dependency `org.testcontainers:testcontainers:1.20.4`.

## Version 2.3

#### New Features

- JavaScript implementations of all algorithms needed by either the voting or inspection client. Corresponding
  modules `algorithms-js` and `algorithms-js-service` added to the project structure.
- Support of JavaScript algorithms added to election simulator.
- Class `RandomElectionParametersFactory` replaced by service `EventDefinitionService`. This is a preparation for
  integrating eCH.
- Election event description, election group descriptions, counting circle descriptions added to `ElectionsParameters`
  and `VotingParameters` (following the specification).

#### Changes

- Simplified methods `runEvents` and `testEvents` in `ElectionSimulator`, but with the additional option of running
  different protocols simultaneously. `runEvents` renamed into `simulateEvents`.
- Cosmetic changes made to algorithms `GetChallenge` and `GetChallenges` (following the specification).
- Order of parameters of `GenElectorateData`, `CheckBallot`, `GenResponse`, `GetEncryptions`, and `VerifyElection`
  modified (following the specification).
- Election event identifier `U` moved from election setup `ES` to election parameters `EP` (following the
  specification).
- Variable `PV` in `ElectionSetup` renamed into `PR` (following the specification).
- Renaming of description vectors (following the specification).
- Election parameters `EP` split into election parameters `EP` and election descriptions `ED`. Similarly, voting
  parameters `VP_v` split into voting parameters `VP_v` and voting descriptions `VD_v` (following the specification).
- Class `SimpleLoggerFinder` moved into `SimpleLogger`.
- Class `SimpleLogger` and its usage simplified and with added flexibility.
- Thread-safe maps and sets implemented using `new ConcurrentHashMap()` and `ConcurrentHashMap.newKeySet()`,
  respectively.

#### Bugs

- Missing `bold_u` added to `VotingParameters` (following the specification).
- Method `Set.Phi` renamed into `Set.Psi`.
- Missing self-activation added to `handlePAP2` in verifier's state `S500` (solves rare problems of blocked threads).
- Incorrectly fixed number of voters in tests resolved.
- Method `isDone` added to interface `Notifier`. Corresponding check added to the given notifier when constructing a new
  synchronizer (to avoid rare blocking threads).

## Version 2.2

#### New Features

- Verifier, verification phase and algorithm `VerifyElection` introduced (following the specification).
- Simulation support for mixed settings (Java/JavaScript) introduced.
- On receiving an `ERROR` status, the simulation coordinator aborts the protocol run.
- Redesigned synchronizer based on composable notifiers, event data collectors, and event service monitors.

#### Changes

- Publication `PAT1` renamed to `PAP1`. New publications `PAP2` and `PEP1` introduced.
- Tasks and error states renamed (following the specification).
- RecHash extended to support boolean and ordered sets, and restricted to positive integer (following the
  specification).
- Status `FAILED` renamed into `ERROR`. Coordinator sends out `onError()` to subscribers upon receiving an `ERROR`
  status.
- `ElectionSimulator` simplified. Different code for running and testing an election event.
- Logger removed from `ElectionSimulator`.
- Method `Sequence::isUniform` renamed into `Sequence::isConstant`.
- Some additional methods added to `Set`, `Vector`, and `Matrix`.
- Input validation of GetEncryption completed (following the specification).

#### Bugs

- Protocol run with property `randomFactoryMode=DETERMINISTIC` made deterministic (except for parallel election events).
- Local variable `bold_pk_prime_j` in `S500::handleRCA1` renamed into `bold_pk_prime_0`.
- Wrong argument `D` in `TA3::run` changed to `D_0`.

## Version 2.1

#### New Features

- Election groups fully integrated.
- Extended set of matrix and vector operations added and used in accordance with specification.
- Interface `AlgorithmService` introduced: all calls of top-level algorithms via `AlgorithmService` to support multiple
  programming
  languages.

#### Changes

- Names of some builder methods simplified.
- For-loops replaced by foreach-loops over corresponding `IntSet` ranges.
- Section "Constraints" introduced in top-level algorithms (following the specification)
- Write-in algorithms `GenKeyPairs` and `GetPublicKeys` simplified (following the specification).
- Interfaces SecurityParameters and UsabilityParameters introduced.
- Parameter `U` removed from `GetGenerators`, `GenShuffleProof`, and `CheckShuffleProof` (following the specification).
- Missing call of `GetPublicKey` added to `GetPublicKeys`.
- Constraint for `s` added to `GetPublicKey` and `GetPublicKeys` (following the specification).
- Methods `mapToRing` and `mapToGroup` added to classes `ZZ` and `ZZPlus`. `mapToGroup` called in `GenBallot` (following
  the
  specification).

#### Bugs

- `CheckVerificationCodes`: Upper bound in for-loop fixed.
- Minor bug in `GenRandomInteger` fixed.
- Variable `p_hat` in `GetPrimes` renamed into `bold_p`.
- Variable `bold_y_star` in `GenWriteInProof` renamed into `bold_Y_star`.

## Version 2.0

#### New Features

- Migration to Java 21 completed.
- Perfect immutability in utility classes (using sealed classes), e.g., in `Vector`, `ByteArray`, or tuples.
- Introduction of *phases* and *roles* using Java annotations.
- Distinction between public and secret event data.
- New set of communication services for mails, publications, requests/responses, input/output, and test data.
- Service implementations for serialization, encryption, and signatures.
- Simplified and redesigned coordinator.
- Simplified and redesigned election simulator based of different event synchronizers.
- Distinction between *security parameters* and *usability parameters*.
- Introduction of a new `TypeReference` class and simplified serialization/deserialization process.

#### Changes

- Threads replaced by virtual threads.
- Java and Maven modules `services` and `runner` renamed into `simpleservices` and `simulator`, respectively.
- Class `ElectionRunner` renamed into `ElectionSimulator`.
- Custom `ServiceLoader` class replaced by Java `ServiceLoader` class.
- Logger interface and SimpleLogger replaced by Java `System.Logger` interface.
- New naming conventions for states, for example `S200` for initial state in second protocol state.
- Renamed messages contents (depending on the communication service)
- Event context passed to message and notification handlers (instead of event data).
- Simplified tuple implementation (`equals`, `hashCode`, `toString` delegated to super class).
- New immutable utility class `EntryMap` replaces `IterableMap`.
- Classes `Value` and `IntMap` renamed into `Data` and `DataMap`, respectively.
- Model class `Point` introduced.
- Legacy security level 1 (80 bits) removed. Security levels 2 and 3 renamed into 1 and 2, respectively (
  following the specification).
- Length of parameter `q_hat` fixed to `2 * tau`, with corresponding simplifications in `GenCredentials`
  and `GetElectionCard` (following the specification).
- Optimized group parameter generation in `GetGroupParameters` (following the specification), including parallelization.
- All algorithms support the special case `s=0`.
- New utility class for serialization/deserialization with static methods only.
- `@SuppressWarning` annotations more systematically added to classes and methods to reduce the number of warnings
- Consistent customized exception handlung in all modules.

#### Bug Fixes

- Generators `g` and `h` changed to 2 and 3, respectively (this update has been forgotten in Version 1.3).

## Version 1.3

#### Changes

- The group of quadratic residues replaced by the multiplicative group of absolute values modulo a safe prime p.
    - Classes `QuadraticResidue` and `Mod` removed.
    - New classes `ZZ`, `ZPlus`, and `GG` introduced to replace `QuadraticResidue`and `Mod`.
    - All algorithm classes in module *algorithms* and class `Parameters` adjusted accordingly and further homogenized.
- Implied algorithm changes implemented in:
    - `GenRandomElement`
    - `GetPrimes`
    - `GetGenerators`
    - `GenBallot`
    - `GetEncodedStrings`
    - `GetDecodedStrings`
- Classes `Parameters` and `SecurityLevel` reorganized:
    - New interfaces for different parameter subgroups introduced.
    - New class `UsabilityConfiguration` introduced, old class `Configuration` renamed into `ServiceLoader`.
    - Classes `UsabilityConfiguration`, `SecurityLevel`, `SecurityParameter`, and `SystemParameters` moved from module
      *algorithms* to module *protocol*.
- Packages in module *utilities* reorganized:
    - Improvements made to classes `Set` and `ÌntSet`.
    - New interfaces `Streamable` and `Streamable` introduced.
- Interface `SymmetricEncryption` renamed into `BlockCipher`.
- *Finalization code* renamed into *participation code* (following the specification). Variable names changes
  accordingly.
- *Voting card* renamed into *election card* (following the specification). Variable names changes accordingly.
- Algorithm `GenRandomElement` removed (following the specification), corresponding changes made to `GenResponse`
  and `GenBallotProof`.
- Parameter names in `GenRandomInteger` renamed (following the specification).

## Version 1.2.2

#### Changes

- Utilities module simplified, re-organized, and slightly extended.

## Version 1.2.1

#### Changes

- Property files removed from party modules. Usage of simulationConfiguration class adjusted.
- Some checkstyle issues resolved.
- Final keyword added to utility and immutable classes.

#### Bug Fixes

- Misspelled module names changed.

## Version 1.2

#### New Features

- Migration to Java 17 with JPMS completed.
- *CHVote Protocol Specification – Version 3.3* implemented:
    - Minor changes to algorithm `GenResponse`.
    - Election authority tasks T8 and T9 adjusted to modified specification.
    - Algorithm `RecHash` modified to avoid collisions (tests adjusted).
- New interfaces `services.ch.openchvote.base.framework.CertificateService`
  and `party.ch.openchvote.base.framework.State.MessageHandler` added to allow code simplifications.

#### Changes

- Change of Maven's group ID due to publication process to Maven Central.
- Simplified algorithm `RecHash` using Java 17 preview pattern matching feature.
- Traditional switch-statements replaced by new ones (no fall-through behaviour).
- Performance of parameter generation in `GetGroupParameters` improved.
- Conversion algorithms renamed to minimize method overloading.
- Election setup redesigned.
- Message handling in states simplified.
- `EventData.ValueMap` renamed into `EventData.IntMap`
- Field `EventContext.currentStateId` moved to `EventData`, which allowed many additional simplification.
- Unnecessary dependency from `coordinator` to `algorithms` removed.
- Unified exception classes inheriting from a common base exception class.
- Class `ByteArray` slightly simplified.

#### Bug Fixes

- Bug related to hashing of quadratic residues fixed in `RecHash`.
- Bug related to big-endian byte order fixed in `GenRandomInteger`.
- Memory overflow problem solved in `IntSet.toString`.
- Missing check implemented in algorithm `GetAllPoints`.
- Some spelling errors in comments and code corrected.

## Version 1.1

#### New Features

- Class`party.ch.openchvote.base.framework.EventData` extended with two local classes `Value` and `ValueMap`.
  All `EventData` subclasses greatly simplified.
- *CHVote Protocol Specification – Version 3.2* implemented:
    - Administrator participates in generating the shared encryption key and the distributed decryption.
    - Order of pre-election phases modified.
    - Batch ZKP verification algorithms removed.

#### Changes

- Simplified naming conventions for states and tasks.
- Algorithms `GetElectionIndices` and `GetElectionIndex` replaced by simplified algorithm `GetEligibility`.
- Simplified `ElectionResult` classes without redundant fields from `ElectionParameters`.
- `ElectionRunner` test class simplified based on adjustments in `Voter` class.
- Broader range of `ElectionRunner` test cases.
- Sizes of precomputation tables for fixed-base exponentiations adjusted to specification.
- Extended and updated documentation.

#### Bug Fixes

- Precomputation tables for fixed base of type `QuadraticResidue`  initialized with correct `BigInteger` base (sqrt
  instead of value).
- Manipulations of precomputation tables synchronized.
- Missing internal messages added to support elections with empty electorate.

#### Notes

This release is compatible with Version 3.2 of the *CHVote Protocol Specification*.

## Version 1.0

#### Notes

Initial release of this software. Compatible with Version 3.1 of the *CHVote Protocol Specification*.
