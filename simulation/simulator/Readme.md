# Simulator

This module provides a class `ElectionSimulator` (to be launched via the class `Main`), which can be used for simulating
election events based on
various parameters. By running such a simulation using an election simulator, every relevant aspect of the CHVote
Protocol
is included exactly as defined in [CHVote Protocol Specification](https://eprint.iacr.org/2017/325). To start such a
simulation, the election simulator creates all necessary protocol parties. In the current release of OpenCHVote, all
parties
are equipped with some simple infrastructure services from the `simpleservices` module.

## Executing the Election Simulator

By following the above installation instructions, all artefact JAR files are created and installed into the local Maven
repository. To execute an exemplary election simulation, type:

```console
mvn -q exec:exec
```
Output:
```text
[INFO] LibVMGJ is not available, computations will be slower.
[INFO] plain-2 SimulationCoordinator-1: Event initialized 
[INFO] plain-1 SimulationCoordinator-1: Event initialized 
[INFO] plain-1 SimulationCoordinator-1: Phase started (Initialization)
[INFO] plain-2 SimulationCoordinator-1: Phase started (Initialization)
[INFO] plain-2 SimulationCoordinator-1: Phase started (Preparation)
[INFO] plain-1 SimulationCoordinator-1: Phase started (Preparation)
[INFO] plain-2 SimulationCoordinator-1: Phase started (Election)
[INFO] plain-1 SimulationCoordinator-1: Phase started (Election)
  :
  :
Pierre_Funnell       0    0    0    1    0      1  
Suzanne_Schillaci    0    1    0    0    0      1  
-------------------------------------------------
Abe_Rodarte          0    0    0    1    0      1  
Delma_Mitchem        0    0    0    1    0      1  
Josephine_Haydel     0    0    0    1    0      1  
Pierre_Mencer        0    0    0    1    0      1  
-------------------------------------------------
Ballots              0    1    0    3    1      5  
-------------------------------------------------
[INFO] ElectionSimulator: Events terminated [writein-1]
```

**Notes**

- The displayed election results differ from run to run due to random choices made by simulated voters.
- You can safely ignore the warning: `INFO: LibVMGJ is not available, computations will be slower.`

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of
this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts
is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as
follows:

```console
mvn clean install [-DskipTests]
-- a lot of output
```

## Maven Dependencies

The following dependency tree shows the Maven dependencies of this module.

```console
mvn dependency:tree -Dscope=compile -Dverbose=true
[INFO] --- dependency:3.6.0:tree (default-cli) @ simulator ---
[INFO] io.gitlab.openchvote:simulator:jar:2.4
[INFO] +- io.gitlab.openchvote:administrator:jar:2.4:compile
[INFO] |  \- io.gitlab.openchvote:protocol:jar:2.4:compile
[INFO] |     +- io.gitlab.openchvote:algorithms:jar:2.4:compile
[INFO] |     |  \- io.gitlab.openchvote:utilities:jar:2.4:compile
[INFO] |     |     \- com.verificatum:vmgj:jar:1.2.2:compile
[INFO] |     \- (io.gitlab.openchvote:framework:jar:2.4:compile - omitted for duplicate)
[INFO] +- io.gitlab.openchvote:election-authority:jar:2.4:compile
[INFO] |  \- (io.gitlab.openchvote:protocol:jar:2.4:compile - omitted for duplicate)
[INFO] +- io.gitlab.openchvote:printing-authority:jar:2.4:compile
[INFO] |  \- (io.gitlab.openchvote:protocol:jar:2.4:compile - omitted for duplicate)
[INFO] +- io.gitlab.openchvote:verifier:jar:2.4:compile
[INFO] |  \- (io.gitlab.openchvote:protocol:jar:2.4:compile - omitted for duplicate)
[INFO] +- io.gitlab.openchvote:voter:jar:2.4:compile
[INFO] |  +- io.gitlab.openchvote:voting-client:jar:2.4:compile
[INFO] |  |  \- (io.gitlab.openchvote:protocol:jar:2.4:compile - omitted for duplicate)
[INFO] |  \- io.gitlab.openchvote:inspection-client:jar:2.4:compile
[INFO] |     \- (io.gitlab.openchvote:protocol:jar:2.4:compile - omitted for duplicate)
[INFO] +- io.gitlab.openchvote:core-services:jar:2.4:compile
[INFO] |  \- (io.gitlab.openchvote:protocol:jar:2.4:compile - omitted for duplicate)
[INFO] \- io.gitlab.openchvote:simulation-services:jar:2.4:compile
[INFO]    \- io.gitlab.openchvote:framework:jar:2.4:compile
```

## Java Module Dependencies

The following module dependency diagram illustrates the Java module dependencies to other modules within this project:

![Java module dependencies](img/simulator.svg)
