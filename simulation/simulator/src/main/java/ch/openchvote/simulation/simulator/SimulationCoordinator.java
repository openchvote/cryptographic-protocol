/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.simulator;

import ch.openchvote.base.framework.Coordinator;

/**
 * This class provides an implementation of the abstract {@link Coordinator} class for creating coordinators of an
 * election simulation. The abstract methods as implemented ensure that the phases of an election event are started and
 * stopped automatically, when corresponding preconditions are met. For taking these decisions, the coordinator must
 * know all involved parties, which means that they all must have been registered.
 */
public class SimulationCoordinator extends Coordinator {

    /**
     * Creates a new coordinator based on the given id.
     *
     * @param id The coordinator's unique id
     */
    public SimulationCoordinator(String id) {
        super(id);
    }

    @Override
    protected void handleReadyStatus(String eventId, String phaseId) {
        if (this.readyToStart(eventId, phaseId)) {
            this.startPhase(eventId, phaseId);
            // necessary to avoid blocking the simulation in some edge cases
            if (this.requiresStop(phaseId) && this.readyToStop(eventId, phaseId)) {
                this.stopPhase(eventId, phaseId);
            }
        }
    }

    @Override
    protected void handleDoneStatus(String eventId, String phaseId) {
        if (this.requiresStop(phaseId) && this.readyToStop(eventId, phaseId)) {
            this.stopPhase(eventId, phaseId);
        }
    }

    @Override
    protected void handleErrorStatus(String eventId) {
        if (this.readyToAbort(eventId)) {
            this.abortEvent(eventId);
        }
    }

    @Override
    protected void handleFinalStatus(String eventId) {
        if (this.readyToTerminate(eventId)) {
            this.terminateEvent(eventId);
        }
    }

}
