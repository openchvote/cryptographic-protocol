/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.simulator;

import ch.openchvote.core.protocol.parameters.SecurityLevel;
import ch.openchvote.core.protocol.parameters.UsabilityConfiguration;
import ch.openchvote.base.utilities.tools.RandomFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * Instances of this class represent a set of simulation parameters. These parameters are read from a configuration
 * file. The names of the properties in the configuration file must be exactly as follows:
 * <ul>
 * <li>{@code "loggerLevel"}: Name of the logger level, for example {@code "DEBUG"}</li>
 * <li>{@code "randomFactoryMode"}: Name of the random factory's mode, for example {@code "RANDOM"}</li>
 * <li>{@code "securityLevel"}: Name of the security level, for example {@code "LEVEL_1"}</li>
 * <li>{@code "usabilityConfiguration"}: Name of the usability configuration, for example {@code "A57_A57_A10_A16"}
 * <li>{@code "numberOfAuthorities"}: Number of election authorities</li>
 * <li>{@code "numberOfVoters"}: Number of voters</li>
 * <li>{@code "maxNumberOfElections"}: Maximum number of elections in an election event</li>
 * <li>{@code "maxNumberOfElectionGroups"}: Maximum number of election groups in an election event</li>
 * <li>{@code "maxNumberOfCandidates"}: Maximum number of candidates in every election</li>
 * <li>{@code "maxNumberOfSelections"}: Maximum number of selections in every election</li>
 * <li>{@code "maxNumberOfCountingCircles"}: Maximum number of counting circles</li>
 * <li>{@code "probCountingCircle"}: Probability of a counting circle to allow votes for an election</li>
 * <li>{@code "probEligibility"}: Probability af a voter being eligible in an election</li>
 * <li>{@code "probWriteIns"}: Probability that write-ins are allowed in an election</li>
 * <li>{@code "probParticipation"}: Probability of a voter to participate in an election</li>
 * <li>{@code "probJavaScriptClient"}: Probability of a voting or inspection client to execute algorithms in JavaScript</li>
 * </ul>
 * If corresponding entries are missing in the configuration file, the specified default values are taken, for example
 * {@link SimulationConfiguration#DEFAULT_SECURITY_LEVEL} for the default security level.
 */
public class SimulationConfiguration extends Properties {

    // default name of the configuration file
    static private final String DEFAULT_CONFIG_FILE_NAME = "config.properties";

    // default simulation properties
    static private final System.Logger.Level DEFAULT_LOGGER_LEVEL = System.Logger.Level.INFO;
    static private final RandomFactory.Mode DEFAULT_RANDOM_FACTORY_MODE = RandomFactory.Mode.RANDOM;

    // default event setup
    static private final String DEFAULT_SECURITY_LEVEL = SecurityLevel.LEVEL_1.getId();
    static private final String DEFAULT_USABILITY_CONFIGURATION = UsabilityConfiguration.DEFAULT.getId();

    // default number of parties
    static private final int DEFAULT_NUMBER_OF_AUTHORITIES = 4;
    static private final int DEFAULT_NUMBER_OF_VOTERS = 10; // number of voters

    // default election parameters
    static private final int DEFAULT_MAX_NUMBER_OF_ELECTIONS = 5; // number of elections in an election event
    static private final int DEFAULT_MAX_NUMBER_OF_ELECTION_GROUPS = 3; // number of elections in an election event
    static private final int DEFAULT_MAX_NUMBER_OF_CANDIDATES = 8; // number of candidates in every election
    static private final int DEFAULT_MAX_NUMBER_OF_SELECTIONS = 4; // number of selections in every election
    static private final int DEFAULT_MAX_NUMBER_OF_COUNTING_CIRCLES = 5; // number of counting circles

    // default simulation probabilities
    static private final double DEFAULT_PROB_COUNTING_CIRCLES = 0.75; // probability of a counting circle to allow votes for an election
    static private final double DEFAULT_PROB_ELIGIBILITY = 0.95; // probability af a voter being eligible in an election
    static private final double DEFAULT_PROB_WRITE_INS = 0.7; // probability that write-ins are allowed in an election
    static private final double DEFAULT_PROB_PARTICIPATION = 0.8; // probability of a voter to participate in an election
    static private final double DEFAULT_PROB_JAVASCRIPT = 0.2; // probability of a voting or inspection client to execute algorithms in JavaScript

    // property names in configuration file
    static private final String LOGGER_LEVEL = "loggerLevel";
    static private final String RANDOM_FACTORY_MODE = "randomFactoryMode";
    static private final String SECURITY_LEVEL = "securityLevel";
    static private final String USABILITY_CONFIGURATION = "usabilityConfiguration";
    static private final String NUMBER_OF_AUTHORITIES = "numberOfAuthorities";
    static private final String NUMBER_OF_VOTERS = "numberOfVoters";
    static private final String MAX_NUMBER_OF_ELECTIONS = "maxNumberOfElections";
    static private final String MAX_NUMBER_OF_ELECTION_GROUPS = "maxNumberOfElectionGroups";
    static private final String MAX_NUMBER_OF_CANDIDATES = "maxNumberOfCandidates";
    static private final String MAX_NUMBER_OF_SELECTIONS = "maxNumberOfSelections";
    static private final String MAX_NUMBER_OF_COUNTING_CIRCLES = "maxNumberOfCountingCircles";
    static private final String PROB_COUNTING_CIRCLES = "probCountingCircle";
    static private final String PROB_ELIGIBILITY = "probEligibility";
    static private final String PROB_WRITE_INS = "probWriteIns";
    static private final String PROB_PARTICIPATION = "probParticipation";
    static private final String PROB_JAVASCRIPT_CLIENT = "probJavaScriptClient";

    // private fields
    private final System.Logger.Level loggerLevel;
    private final RandomFactory.Mode randomFactoryMode;
    private final String securityLevel;
    private final String usabilityConfiguration;
    private final int numberOfAuthorities;
    private final int numberOfVoters;
    private final int maxNumberOfElections;
    private final int maxNumberOfElectionGroups;
    private final int maxNumberOfCandidates;
    private final int maxNumberOfSelections;
    private final int maxNumberOfCountingCircles;
    private final double probCountingCircle;
    private final double probEligibility;
    private final double probWriteIns;
    private final double probParticipation;
    private final double probJavaScriptClient;

    /**
     * Reads the default simulation configuration file and creates a new instance of this class with the properties set
     * to the values as defined in the configuration file. Throws an exception if the file does not exist.
     */
    public SimulationConfiguration() {
        this(DEFAULT_CONFIG_FILE_NAME);
    }

    /**
     * Reads the simulation configuration file for the given file name and creates a new instance of this class with the
     * properties set to the values as defined in the configuration file. Throws an exception if the file does not
     * exist.
     *
     * @param configFileName The name of the configuration file
     */
    public SimulationConfiguration(String configFileName) {
        try (var inputStream = this.getClass().getResourceAsStream(configFileName)) {
            this.load(inputStream);
            this.loggerLevel = System.Logger.Level.valueOf(this.getProperty(LOGGER_LEVEL, DEFAULT_LOGGER_LEVEL.name()));
            this.randomFactoryMode = RandomFactory.Mode.valueOf(this.getProperty(RANDOM_FACTORY_MODE, DEFAULT_RANDOM_FACTORY_MODE.name()));
            this.securityLevel = this.getProperty(SECURITY_LEVEL, DEFAULT_SECURITY_LEVEL);
            this.usabilityConfiguration = this.getProperty(USABILITY_CONFIGURATION, DEFAULT_USABILITY_CONFIGURATION);
            this.numberOfAuthorities = this.readIntValue(NUMBER_OF_AUTHORITIES, DEFAULT_NUMBER_OF_AUTHORITIES);
            this.numberOfVoters = this.readIntValue(NUMBER_OF_VOTERS, DEFAULT_NUMBER_OF_VOTERS);
            this.maxNumberOfElections = this.readIntValue(MAX_NUMBER_OF_ELECTIONS, DEFAULT_MAX_NUMBER_OF_ELECTIONS);
            this.maxNumberOfElectionGroups = this.readIntValue(MAX_NUMBER_OF_ELECTION_GROUPS, DEFAULT_MAX_NUMBER_OF_ELECTION_GROUPS);
            this.maxNumberOfCandidates = this.readIntValue(MAX_NUMBER_OF_CANDIDATES, DEFAULT_MAX_NUMBER_OF_CANDIDATES);
            this.maxNumberOfSelections = this.readIntValue(MAX_NUMBER_OF_SELECTIONS, DEFAULT_MAX_NUMBER_OF_SELECTIONS);
            this.maxNumberOfCountingCircles = this.readIntValue(MAX_NUMBER_OF_COUNTING_CIRCLES, DEFAULT_MAX_NUMBER_OF_COUNTING_CIRCLES);
            this.probCountingCircle = this.readDoubleValue(PROB_COUNTING_CIRCLES, DEFAULT_PROB_COUNTING_CIRCLES);
            this.probEligibility = this.readDoubleValue(PROB_ELIGIBILITY, DEFAULT_PROB_ELIGIBILITY);
            this.probWriteIns = this.readDoubleValue(PROB_WRITE_INS, DEFAULT_PROB_WRITE_INS);
            this.probParticipation = this.readDoubleValue(PROB_PARTICIPATION, DEFAULT_PROB_PARTICIPATION);
            this.probJavaScriptClient = this.readDoubleValue(PROB_JAVASCRIPT_CLIENT, DEFAULT_PROB_JAVASCRIPT);
        } catch (IOException exception) {
            throw new RuntimeException("Configuration file not found: " + configFileName);
        }
        if (!this.checkProperties()) {
            throw new RuntimeException("Invalid simulation parameters");
        }
    }

    /**
     * Creates a new instance of this class with the properties set to the given values.
     *
     * @param loggerLevel                The given logger level
     * @param randomFactoryMode          The given mode of the random factory
     * @param securityLevel              The given security level
     * @param usabilityConfiguration     The given usability configuration
     * @param numberOfAuthorities        Number of authorities
     * @param numberOfVoters             Number of voters
     * @param maxNumberOfElections       Maximum number of elections in an election event
     * @param maxNumberOfElectionGroups  Maximum number of election groups in an election event
     * @param maxNumberOfCandidates      Maximum number of candidates in every election
     * @param maxNumberOfSelections      Maximum number of selections in every election
     * @param maxNumberOfCountingCircles Maximum number of counting circles
     * @param probCountingCircle         Probability of a counting circle to allow votes for an election
     * @param probEligibility            Probability af a voter being eligible in an election
     * @param probWriteIns               Probability that write-ins are allowed in an election
     * @param probParticipation          Probability of a voter to participate in an election
     * @param probJavaScriptClient       Probability of a voting or inspection client to execute algorithms in
     *                                   JavaScript
     */
    public SimulationConfiguration(System.Logger.Level loggerLevel, RandomFactory.Mode randomFactoryMode, SecurityLevel securityLevel, UsabilityConfiguration usabilityConfiguration, int numberOfAuthorities, int numberOfVoters, int maxNumberOfElections, int maxNumberOfElectionGroups, int maxNumberOfCandidates, int maxNumberOfSelections, int maxNumberOfCountingCircles, double probCountingCircle, double probEligibility, double probWriteIns, double probParticipation, double probJavaScriptClient) {
        this.loggerLevel = loggerLevel;
        this.randomFactoryMode = randomFactoryMode;
        this.securityLevel = securityLevel.getId();
        this.usabilityConfiguration = usabilityConfiguration.getId();
        this.numberOfAuthorities = numberOfAuthorities;
        this.numberOfVoters = numberOfVoters;
        this.maxNumberOfElections = maxNumberOfElections;
        this.maxNumberOfElectionGroups = maxNumberOfElectionGroups;
        this.maxNumberOfCandidates = maxNumberOfCandidates;
        this.maxNumberOfSelections = maxNumberOfSelections;
        this.maxNumberOfCountingCircles = maxNumberOfCountingCircles;
        this.probCountingCircle = probCountingCircle;
        this.probEligibility = probEligibility;
        this.probWriteIns = probWriteIns;
        this.probParticipation = probParticipation;
        this.probJavaScriptClient = probJavaScriptClient;
        if (!this.checkProperties()) {
            throw new RuntimeException("Invalid simulation parameters");
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean checkProperties() {
        return this.loggerLevel != null
                && this.randomFactoryMode != null
                && this.securityLevel != null
                && this.usabilityConfiguration != null
                && this.numberOfAuthorities >= 1
                && this.numberOfVoters >= 0
                && this.maxNumberOfElections >= 0
                && this.maxNumberOfElectionGroups >= (this.maxNumberOfElections == 0 ? 0 : 1)
                && this.maxNumberOfCandidates >= 2
                && this.maxNumberOfSelections >= 1
                && this.maxNumberOfSelections < this.maxNumberOfCandidates
                && this.maxNumberOfCountingCircles >= (this.numberOfVoters == 0 ? 0 : 1)
                && 0.0 <= this.probCountingCircle && this.probCountingCircle <= 1.0
                && 0.0 <= this.probEligibility && this.probEligibility <= 1.0
                && 0.0 <= this.probWriteIns && this.probWriteIns <= 1.0
                && 0.0 <= this.probParticipation && this.probParticipation <= 1.0
                && 0.0 <= this.probJavaScriptClient && this.probJavaScriptClient <= 1.0;
    }

    /**
     * Returns the logger level used while conducting the simulation.
     *
     * @return The logger level
     */
    public System.Logger.Level getLoggerLevel() {
        return this.loggerLevel;
    }

    /**
     * Returns the mode of the random factory used while conducting the simulation.
     *
     * @return The mode of the random factory
     */
    public RandomFactory.Mode getRandomFactoryMode() {
        return this.randomFactoryMode;
    }

    /**
     * Returns the security level used while conducting the simulation.
     *
     * @return The security level
     */
    public String getSecurityLevel() {
        return this.securityLevel;
    }

    /**
     * Returns the usability configuration used while conducting the simulation.
     *
     * @return The usability configuration
     */
    public String getUsabilityConfiguration() {
        return this.usabilityConfiguration;
    }

    /**
     * Returns the number of authorities involved when conducting simulation.
     *
     * @return The maximum number of authorities
     */
    public int getNumberOfAuthorities() {
        return this.numberOfAuthorities;
    }

    /**
     * Returns the number of voters created in a simulated election event.
     *
     * @return The number of voters
     */
    public int getNumberOfVoters() {
        return this.numberOfVoters;
    }

    /**
     * Returns the maximum number of elections created in a simulated election event.
     *
     * @return The maximum number of elections
     */
    public int getMaxNumberOfElections() {
        return this.maxNumberOfElections;
    }

    /**
     * Returns the maximum number of election groups created in a simulated election event.
     *
     * @return The maximum number of election groups
     */
    public int getMaxNumberOfElectionGroups() {
        return this.maxNumberOfElectionGroups;
    }

    /**
     * Returns the maximum number of candidates created in a simulated election event.
     *
     * @return The maximum number of candidates
     */
    public int getMaxNumberOfCandidates() {
        return this.maxNumberOfCandidates;
    }

    /**
     * Returns the maximum number of selections allowed in a simulated election event.
     *
     * @return The maximum number of selections
     */
    public int getMaxNumberOfSelections() {
        return this.maxNumberOfSelections;
    }

    /**
     * Returns the maximum number of counting circles created in a simulated election event.
     *
     * @return The maximum number of counting circles
     */
    public int getMaxNumberOfCountingCircles() {
        return this.maxNumberOfCountingCircles;
    }

    /**
     * Returns the probability of a counting circle to allow votes for an election.
     *
     * @return The probability of a counting circle to allow votes for an election
     */
    public double getProbCountingCircle() {
        return this.probCountingCircle;
    }

    /**
     * Returns the probability af a voter being eligible in an election.
     *
     * @return The probability af a voter being eligible in an election
     */
    public double getProbEligibility() {
        return this.probEligibility;
    }

    /**
     * Returns the probability that write-ins are allowed in an election.
     *
     * @return The probability that write-ins are allowed in an election
     */
    public double getProbWriteIns() {
        return this.probWriteIns;
    }

    /**
     * Returns the probability of a voter to participate in an election.
     *
     * @return The probability of a voter to participate in an election
     */
    public double getProbParticipation() {
        return this.probParticipation;
    }

    /**
     * Returns the probability of a voting or inspection client to execute algorithms in JavaScript.
     *
     * @return The probability of executing algorithms in JavaScript
     */
    public double getProbJavaScriptClient() {
        return this.probJavaScriptClient;
    }

    // private helper method for parsing a property value of type int
    private int readIntValue(String propertyName, int defaultValue) {
        return Integer.parseInt(this.getProperty(propertyName, Integer.toString(defaultValue)));
    }

    // private helper method for parsing a property value of type double
    private double readDoubleValue(String propertyName, double defaultValue) {
        return Double.parseDouble(this.getProperty(propertyName, Double.toString(defaultValue)));
    }

    @Override
    public String toString() {
        return "SimulationConfiguration{" +
                "loggerLevel=" + this.loggerLevel +
                ", randomFactoryMode=" + this.randomFactoryMode +
                ", securityLevel='" + this.securityLevel + '\'' +
                ", usabilityConfiguration='" + this.usabilityConfiguration + '\'' +
                ", numberOfAuthorities=" + this.numberOfAuthorities +
                ", numberOfVoters=" + this.numberOfVoters +
                ", maxNumberOfElections=" + this.maxNumberOfElections +
                ", maxNumberOfElectionGroups=" + this.maxNumberOfElectionGroups +
                ", maxNumberOfCandidates=" + this.maxNumberOfCandidates +
                ", maxNumberOfSelections=" + this.maxNumberOfSelections +
                ", maxNumberOfCountingCircles=" + this.maxNumberOfCountingCircles +
                ", probCountingCircle=" + this.probCountingCircle +
                ", probEligibility=" + this.probEligibility +
                ", probWriteIns=" + this.probWriteIns +
                ", probParticipation=" + this.probParticipation +
                ", probJavaScriptClient=" + this.probJavaScriptClient +
                '}';
    }

}
