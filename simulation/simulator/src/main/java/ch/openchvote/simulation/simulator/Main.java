/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.simulator;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionResult;
import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.base.framework.communication.Publication;
import ch.openchvote.base.framework.services.SerializationService;
import ch.openchvote.base.framework.services.Service;
import ch.openchvote.core.protocol.protocols.plain.Plain;
import ch.openchvote.core.protocol.protocols.writein.WriteIn;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;

import java.util.List;

import static java.lang.System.Logger.Level.INFO;

/**
 * This class provides an exemplary main method for simulating election events. The supplied command-line arguments are
 * ignored.
 */
public class Main {

    private static final String FORMAT_STRING = "%-10s: %s";

    /**
     * Creates an {@link ElectionSimulator} instance and executes three election events simultaneously. The mandatory
     * command line arguments are ignored.
     *
     * @param args The command line arguments
     */
    static public void main(String[] args) {

        // create election simulator based on config file
        var electionSimulator = new ElectionSimulator();

        // get logger
        var logger = System.getLogger(Main.class.getSimpleName());

        // add tasks and publication consumer
        electionSimulator.addInitializationTask(eventId -> logger.log(INFO, String.format(FORMAT_STRING, eventId, "Event initialized")));
        electionSimulator.addTerminationTask(eventId -> logger.log(INFO, String.format(FORMAT_STRING, eventId, "Event terminated")));
        electionSimulator.addAbortionTask(eventId -> logger.log(INFO, String.format(FORMAT_STRING, eventId, "Event aborted")));
        electionSimulator.addPublicationConsumer(Main::printPublications);

        // simulate events
        electionSimulator.simulateEvent(WriteIn.class);
        electionSimulator.simulateEvents(Plain.class, 2);
        electionSimulator.simulateEvents(List.of(WriteIn.class, Plain.class, Plain.class));
    }

    // private method to output multiple publications in console
    static private void printPublications(List<Publication> publications) {
        publications.forEach(Main::printPublication);
    }

    // private method to output a single publication in console
    static private void printPublication(Publication publication) {

        // get logger
        var logger = System.getLogger(Main.class.getSimpleName());

        // deserialize publication content
        var contentId = publication.getContentId();
        var contentClass = Content.getClass(contentId);
        var serializerService = Service.load(SerializationService.Content.class);
        var content = serializerService.deserialize(publication.getContentString(), contentClass);

        // dispatch different publication types: plain
        var eventId = publication.getEventId();
        if (content instanceof ch.openchvote.core.protocol.protocols.plain.content.publication.PAP1 PAP1) {
            var EP = PAP1.get_EP();
            var ED = PAP1.get_ED();
            var bold_n = EP.get_bold_n();
            var bold_u = EP.get_bold_u();
            var bold_d_E = ED.get_bold_d_E();
            var bold_d_A = ED.get_bold_d_A();
            var ER = PAP1.get_ER();
            logger.log(INFO, String.format(FORMAT_STRING, eventId, "Election result published"));
            Main.printResult(eventId, bold_n, bold_u, bold_d_E, bold_d_A, ER);
        }
        // dispatch different publication types: writein
        if (content instanceof ch.openchvote.core.protocol.protocols.writein.content.publication.PAP1 PAP1) {
            var EP = PAP1.get_EP();
            var ED = PAP1.get_ED();
            var bold_n = EP.get_bold_n();
            var bold_u = EP.get_bold_u();
            var bold_d_E = ED.get_bold_d_E();
            var bold_d_A = ED.get_bold_d_A();
            var ER = PAP1.get_ER();
            logger.log(INFO, String.format(FORMAT_STRING, eventId, "Election rresult published"));
            Main.printResult(eventId, bold_n, bold_u, bold_d_E, bold_d_A, ER);
        }
        if (content instanceof ch.openchvote.core.protocol.protocols.plain.content.publication.PVP1 PVP1) {
            var v = PVP1.get_v();
            logger.log(INFO, String.format(FORMAT_STRING, eventId, v ? "Election result verified" : "Verification failed"));
        }
        if (content instanceof ch.openchvote.core.protocol.protocols.writein.content.publication.PVP1 PVP1) {
            var v = PVP1.get_v();
            logger.log(INFO, String.format(FORMAT_STRING, eventId, v ? "Election result verified" : "Verification failed"));
        }
    }

    // helper method to output the election result to the console
    @SuppressWarnings("unused")
    static private void printResult(String eventId, IntVector bold_n, IntVector bold_u, Vector<String> bold_d_E, Vector<String> bold_d_A, ElectionResult electionResult) {
        System.out.println(electionResult);
        System.out.println();
    }

}
