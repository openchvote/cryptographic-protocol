/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.simulator.random;

import ch.openchvote.core.administrator.EventDefinitionService;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionParameters;
import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.core.algorithms.protocols.common.model.SystemParties;
import ch.openchvote.core.algorithms.protocols.plain.model.ElectionParametersPlain;
import ch.openchvote.core.algorithms.protocols.writein.model.ElectionParametersWriteIn;
import ch.openchvote.core.protocol.protocols.CHVote;
import ch.openchvote.simulation.simulator.SimulationConfiguration;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tools.RandomFactory;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class provides two {@link EventDefinitionService} implementations for the two protocols {@code Plain} and
 * {@code WriteIn}. These services can be configured by registering parameters for specific election events. For a given
 * set of such parameters, event setup, election parameters, and election descriptions are generated at random using the
 * service's principal methods {@link EventDefinitionService#getEventSetup},
 * {@link EventDefinitionService#getElectionParameters}, and
 * {@link EventDefinitionService#getElectionDescriptions(String)}.
 *
 * @param <EP> The generic type of the election parameters
 */
public abstract class RandomEventDefinitionService<EP extends ElectionParameters> implements EventDefinitionService<EP> {

    // various maps for storing the registered parameters
    static private final Map<String, String> PROTOCOL_NAMES = new ConcurrentHashMap<>();
    static private final Map<String, SystemParties> SYSTEM_PARTIES = new ConcurrentHashMap<>();
    static private final Map<String, Vector<String>> VOTER_DESCRIPTIONS = new ConcurrentHashMap<>();
    static private final Map<String, SimulationConfiguration> SIMULATION_CONFIGURATIONS = new ConcurrentHashMap<>();

    static private final Map<String, EventSetup> ES_MAP = new ConcurrentHashMap<>();
    static private final Map<String, ElectionDescriptions> ED_MAP = new ConcurrentHashMap<>();

    @Override
    public EventSetup getEventSetup(String eventId) {
        if (!ES_MAP.containsKey(eventId)) {
            var simulationConfiguration = SIMULATION_CONFIGURATIONS.get(eventId);
            var PR = PROTOCOL_NAMES.get(eventId);
            var SL = simulationConfiguration.getSecurityLevel();
            var UC = simulationConfiguration.getUsabilityConfiguration();
            var SP = SYSTEM_PARTIES.get(eventId);
            ES_MAP.put(eventId, new EventSetup(PR, SL, UC, SP));
        }
        return ES_MAP.get(eventId);
    }

    @Override
    public ElectionDescriptions getElectionDescriptions(String eventId) {
        if (!ED_MAP.containsKey(eventId)) {
            var EP = this.getElectionParameters(eventId);
            var t = EP.get_t();
            var n = EP.get_n();
            var u = EP.get_u();
            var w = EP.get_w();
            var D = get_D();
            var bold_d_E = get_bold_d_E(t);
            var bold_d_G = get_bold_d_G(u);
            var bold_d_A = get_bold_d_A(n);
            var bold_d_V = VOTER_DESCRIPTIONS.get(eventId);
            var bold_d_C = get_bold_d_C(w);
            ED_MAP.put(eventId, new ElectionDescriptions(D, bold_d_E, bold_d_G, bold_d_A, bold_d_V, bold_d_C));
        }
        return ED_MAP.get(eventId);
    }


    /**
     * Registers the given protocolName version by associating it to the given event id.
     *
     * @param eventId      The given event id
     * @param protocolName The given protocolName
     */
    public static void registerProtocolName(String eventId, String protocolName) {
        PROTOCOL_NAMES.put(eventId, protocolName);
    }

    /**
     * Registers the given system parties by associating then to the given event id.
     *
     * @param eventId       The given event id
     * @param systemParties The given system parties
     */
    public static void registerSystemParties(String eventId, SystemParties systemParties) {
        SYSTEM_PARTIES.put(eventId, systemParties);
    }

    /**
     * Registers the given voter descriptions by associating then to the given event id.
     *
     * @param eventId           The given event id
     * @param voterDescriptions The given voter descriptions
     */
    public static void registerVoterDescriptions(String eventId, Vector<String> voterDescriptions) {
        VOTER_DESCRIPTIONS.put(eventId, voterDescriptions);
    }

    /**
     * Registers the given simulation configuration by associating it to the given event id.
     *
     * @param eventId                 The given event id
     * @param simulationConfiguration The given simulation configuration
     */
    public static void registerSimulationConfiguration(String eventId, SimulationConfiguration simulationConfiguration) {
        SIMULATION_CONFIGURATIONS.put(eventId, simulationConfiguration);
    }

    /**
     * Service class implementation for the protocol {@code Plain}.
     */
    public static class Plain extends RandomEventDefinitionService<ElectionParametersPlain> {

        static private final Map<String, ElectionParametersPlain> EP_MAP = new HashMap<>();

        @Override
        public boolean canHandle(Class<? extends CHVote<?, ?>> protocol) {
            return protocol == ch.openchvote.core.protocol.protocols.plain.Plain.class;
        }

        @Override
        public ElectionParametersPlain getElectionParameters(String eventId) {
            if (!EP_MAP.containsKey(eventId)) {
                var simulationConfiguration = SIMULATION_CONFIGURATIONS.get(eventId);
                var random = RandomFactory.getInstance(simulationConfiguration.getRandomFactoryMode());
                var t_max = simulationConfiguration.getMaxNumberOfElections();
                var n_max = simulationConfiguration.getMaxNumberOfCandidates();
                var k_max = simulationConfiguration.getMaxNumberOfSelections();
                var u_max = simulationConfiguration.getMaxNumberOfElectionGroups();
                var w_max = simulationConfiguration.getMaxNumberOfCountingCircles();
                var t = t_max == 0 ? 0 : random.nextInt(t_max) + 1;
                var u = u_max == 0 ? 0 : random.nextInt(u_max) + 1;
                var w = w_max == 0 ? 0 : random.nextInt(w_max) + 1;
                var N_E = VOTER_DESCRIPTIONS.get(eventId).getLength();
                var bold_n = get_bold_n(t, n_max, random);
                var bold_k = get_bold_k(t, k_max, bold_n, random);
                var bold_u = get_bold_u(t, u, random);
                var bold_w = get_bold_w(N_E, w, random);
                var bold_E = get_bold_E(N_E, u, simulationConfiguration.getProbCountingCircle(), simulationConfiguration.getProbEligibility(), random);
                EP_MAP.put(eventId, new ElectionParametersPlain(eventId, bold_n, bold_k, bold_u, bold_w, bold_E));
            }
            return EP_MAP.get(eventId);
        }

    }

    /**
     * Service class implementation for the protocol {@code WriteIn}.
     */
    static public class WriteIn extends RandomEventDefinitionService<ElectionParametersWriteIn> {

        static private final Map<String, ElectionParametersWriteIn> EP_MAP = new HashMap<>();

        @Override
        public boolean canHandle(Class<? extends CHVote<?, ?>> protocol) {
            return protocol == ch.openchvote.core.protocol.protocols.writein.WriteIn.class;
        }

        @Override
        public ElectionParametersWriteIn getElectionParameters(String eventId) {
            if (!EP_MAP.containsKey(eventId)) {
                var simulationConfiguration = SIMULATION_CONFIGURATIONS.get(eventId);
                var random = RandomFactory.getInstance(simulationConfiguration.getRandomFactoryMode());
                var t_max = simulationConfiguration.getMaxNumberOfElections();
                var n_max = simulationConfiguration.getMaxNumberOfCandidates();
                var k_max = simulationConfiguration.getMaxNumberOfSelections();
                var u_max = simulationConfiguration.getMaxNumberOfElectionGroups();
                var w_max = simulationConfiguration.getMaxNumberOfCountingCircles();
                var t = t_max == 0 ? 0 : random.nextInt(t_max) + 1;
                var u = u_max == 0 ? 0 : random.nextInt(u_max) + 1;
                var w = w_max == 0 ? 0 : random.nextInt(w_max) + 1;
                var N_E = VOTER_DESCRIPTIONS.get(eventId).getLength();
                var bold_n = get_bold_n(t, n_max, random);
                var bold_k = get_bold_k(t, k_max, bold_n, random);
                var bold_u = get_bold_u(t, u, random);
                var bold_w = get_bold_w(N_E, w, random);
                var bold_E = get_bold_E(N_E, u, simulationConfiguration.getProbCountingCircle(), simulationConfiguration.getProbEligibility(), random);
                var bold_z = get_bold_z(t, simulationConfiguration.getProbWriteIns(), random);
                var bold_v = get_bold_v(t, bold_n, bold_k, bold_z);
                EP_MAP.put(eventId, new ElectionParametersWriteIn(eventId, bold_n, bold_k, bold_u, bold_w, bold_E, bold_v, bold_z));
            }
            return EP_MAP.get(eventId);
        }

    }

    // private helper method for generating an election event description
    static private String get_D() {
        return "Election event: " + LocalDate.now();
    }

    // private helper method for generating election descriptions
    static private Vector<String> get_bold_d_E(int t) {
        var builder_bold_d_E = new Vector.Builder<String>(t);
        for (int j : IntSet.range(1, t)) {
            var ED_j = "Election-" + String.format(getFormatString(t), j);
            builder_bold_d_E.set(j, ED_j);
        }
        return builder_bold_d_E.build();
    }

    // private helper method for generating election group descriptions
    static private Vector<String> get_bold_d_G(int u) {
        var builder_bold_d_G = new Vector.Builder<String>(u);
        for (int k : IntSet.range(1, u)) {
            var GD_k = "ElectionGroup-" + String.format(getFormatString(u), k);
            builder_bold_d_G.set(k, GD_k);
        }
        return builder_bold_d_G.build();
    }

    // private helper method for generating candidate descriptions
    static private Vector<String> get_bold_d_A(int n) {
        var builder_bold_c = new Vector.Builder<String>(n);
        for (int i : IntSet.range(1, n)) {
            var AD_i = "Candidate-" + String.format(getFormatString(n), i);
            builder_bold_c.set(i, AD_i);
        }
        return builder_bold_c.build();
    }

    // private helper method for generating counting circle descriptions
    static private Vector<String> get_bold_d_C(int w) {
        var builder_bold_d_C = new Vector.Builder<String>(w);
        for (int i : IntSet.range(1, w)) {
            var CD_i = "CountingCircle-" + String.format(getFormatString(w), i);
            builder_bold_d_C.set(i, CD_i);
        }
        return builder_bold_d_C.build();
    }

    // private helper method for generating election groups
    static private IntVector get_bold_u(int t, int u, Random random) {
        var builder_bold_u = new IntVector.Builder(t);
        for (int j : IntSet.range(1, t)) {
            int u_j = random.nextInt(u) + 1;
            builder_bold_u.set(j, u_j);
        }
        return builder_bold_u.build().sort();
    }

    // private helper method for generating the number of candidates
    static private IntVector get_bold_n(int t, int n_max, Random random) {
        var builder_bold_n = new IntVector.Builder(t);
        for (int j : IntSet.range(1, t)) {
            var n_j = 2 + random.nextInt(n_max - 1);
            builder_bold_n.set(j, n_j);
        }
        return builder_bold_n.build();
    }

    // private helper method for generating the number of selections
    static private IntVector get_bold_k(int t, int k_max, IntVector bold_n, Random random) {
        var builder_bold_k = new IntVector.Builder(t);
        for (int j : IntSet.range(1, t)) {
            var k_j = Math.min(1 + random.nextInt(k_max), bold_n.getValue(j) - 1);
            builder_bold_k.set(j, k_j);
        }
        return builder_bold_k.build();
    }

    // private helper method for generating counting circles
    static private IntVector get_bold_w(int N_E, int w, Random random) {
        var builder_bold_w = new IntVector.Builder(N_E);
        for (int i : IntSet.range(1, N_E)) {
            int w_i = random.nextInt(w) + 1;
            builder_bold_w.set(i, w_i);
        }
        return builder_bold_w.build();
    }

    // private helper method for generating an eligibility matrix
    static private IntMatrix get_bold_E(int N_E, int u, double probCountingCircle, double probEligibility, Random random) {
        var builder_bold_E = new IntMatrix.Builder(N_E, u);
        for (int i : IntSet.range(1, N_E)) {
            for (int j : IntSet.range(1, u)) {
                int e_i_j = (random.nextDouble() <= probCountingCircle ? 1 : 0) * (random.nextDouble() <= probEligibility ? 1 : 0);
                builder_bold_E.set(i, j, e_i_j);
            }
        }
        return builder_bold_E.build();
    }

    // private helper method for generating write-in elections
    static private IntVector get_bold_z(int t, double probWriteIns, Random random) {
        var builder_bold_z = new IntVector.Builder(t);
        for (int j : IntSet.range(1, t)) {
            int z_j = random.nextDouble() <= probWriteIns ? 1 : 0;
            builder_bold_z.set(j, z_j);
        }
        return builder_bold_z.build();
    }

    // private helper method for generating write-in candidates
    static private IntVector get_bold_v(int t, IntVector bold_n, IntVector bold_k, IntVector bold_z) {
        int n = bold_n.sum();
        var builder_bold_v = new IntVector.Builder(n);
        int n_prime = 0;
        for (int j : IntSet.range(1, t)) {
            var n_j = bold_n.getValue(j);
            var k_j = bold_k.getValue(j);
            var z_j = bold_z.getValue(j);
            if (z_j == 1) {
                for (int i : IntSet.range(n_prime + 1, n_prime + n_j - k_j)) {
                    builder_bold_v.set(i, 0);
                }
                for (int i : IntSet.range(n_prime + n_j - k_j + 1, n_prime + n_j)) {
                    builder_bold_v.set(i, 1);
                }
            } else {
                for (int i : IntSet.range(n_prime + 1, n_prime + n_j)) {
                    builder_bold_v.set(i, 0);
                }
            }
            n_prime = n_prime + n_j;
        }
        return builder_bold_v.build();
    }

    // private helper method for formatting candidate and election descriptions
    static private String getFormatString(int x) {
        var digits = Math.max(Integer.toString(x).length(), 2);
        return "%0" + digits + "d";
    }


}
