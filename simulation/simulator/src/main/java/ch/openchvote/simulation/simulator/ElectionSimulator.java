/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.simulator;

import ch.openchvote.core.administrator.Administrator;
import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.model.SystemParties;
import ch.openchvote.core.electionauthority.ElectionAuthority;
import ch.openchvote.base.framework.communication.Publication;
import ch.openchvote.base.framework.communication.TestData;
import ch.openchvote.base.framework.party.Party;
import ch.openchvote.base.framework.protocol.Protocol;
import ch.openchvote.base.framework.synchronizer.Synchronizer;
import ch.openchvote.base.framework.synchronizer.collector.PublicationCollector;
import ch.openchvote.base.framework.synchronizer.collector.TestDataCollector;
import ch.openchvote.base.framework.synchronizer.monitor.EventServiceMonitor;
import ch.openchvote.core.printingauthority.PrintingAuthority;
import ch.openchvote.core.protocol.protocols.CHVote;
import ch.openchvote.simulation.services.SimpleLogger;
import ch.openchvote.simulation.simulator.random.RandomEventDefinitionService;
import ch.openchvote.simulation.simulator.random.RandomVotingStrategy;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.core.verifier.Verifier;
import ch.openchvote.simulation.voter.Voter;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Instances of this class create the necessary CHVote parties for simulating election events using different protocols.
 * Simulation properties such as the number of election authorities or the size and type of the elections can be
 * specified using a simulation configuration, but they remain fixed for a given instance of this class.
 */
public class ElectionSimulator {

    // singleton empty consumers and predicate
    static private final Consumer<String> DO_NOTHING_WITH_EVENT_ID = ignored -> {
    };
    static private final Consumer<List<Publication>> DO_NOTHING_WITH_PUBLICATIONS = ignored -> {
    };
    static private final BiPredicate<List<TestData>, List<Publication>> DO_NOTHING_WITH_TEST_DATA = (ignored1, ignored2) -> true;

    // election simulator id counter
    static private int ID_COUNTER = 0;

    // map for counting the number of previous and current election events for each protocol
    static private final Map<String, Integer> NUMBER_OF_EVENTS = new HashMap<>();

    // the election simulator's id
    private final int id;

    // simulation configuration
    private final SimulationConfiguration simulationConfiguration;

    // system parties and simulation coordinator
    private final List<Party> systemParties;
    private final List<Voter> voters;
    private final SystemParties systemPartyIds;
    private final Vector<String> voterDescriptions;
    private final SimulationCoordinator simulationCoordinator;

    // tasks, publication consumer, and test data predicate
    private Consumer<String> initializationTask; // consumes an eventId
    private Consumer<String> terminationTask; // consumes an eventId
    private Consumer<String> abortionTask; // consumes an eventId
    private Consumer<List<Publication>> publicationConsumer;
    private BiPredicate<List<TestData>, List<Publication>> testingPredicate;

    /**
     * The simplest possible  constructor for creating an election simulator with default parameters and an empty
     * election result consumer.
     */
    public ElectionSimulator() {
        this(new SimulationConfiguration());
    }

    /**
     * A simple constructor for creating an election simulator based on a given simulation configuration.
     *
     * @param simulationConfiguration The given simulation configuration
     */
    public ElectionSimulator(SimulationConfiguration simulationConfiguration) {

        // create unique election simulator id
        this.id = ID_COUNTER++;

        // set simulation configuration
        this.simulationConfiguration = simulationConfiguration;
        var numberOfAuthorities = this.simulationConfiguration.getNumberOfAuthorities();
        var numberOfVoters = this.simulationConfiguration.getNumberOfVoters();
        var loggerLevel = this.simulationConfiguration.getLoggerLevel();
        SimpleLogger.setLoggerLevel(loggerLevel);

        // initialize empty tasks, publication consumer, and test data predicate
        this.initializationTask = DO_NOTHING_WITH_EVENT_ID;
        this.terminationTask = DO_NOTHING_WITH_EVENT_ID;
        this.abortionTask = DO_NOTHING_WITH_EVENT_ID;
        this.publicationConsumer = DO_NOTHING_WITH_PUBLICATIONS;
        this.testingPredicate = DO_NOTHING_WITH_TEST_DATA;

        // create system parties
        this.systemParties = new ArrayList<>();
        var AD = this.createPartyId(Administrator.class);
        this.systemParties.add(new Administrator(AD));
        var PA = this.createPartyId(PrintingAuthority.class);
        this.systemParties.add(new PrintingAuthority(PA));
        var VF = this.createPartyId(Verifier.class);
        this.systemParties.add(new Verifier(VF));
        var bold_ea = this.createPartyIds(ElectionAuthority.class, numberOfAuthorities);
        bold_ea.forEach(electionAuthorityId -> this.systemParties.add(new ElectionAuthority(electionAuthorityId)));
        this.systemPartyIds = new SystemParties(AD, PA, bold_ea);

        // create voters
        this.voterDescriptions = this.createPartyIds(Voter.class, numberOfVoters);
        var probJavaScriptClient = this.simulationConfiguration.getProbJavaScriptClient();
        var randomVotingStrategy = new RandomVotingStrategy(this.simulationConfiguration);
        this.voters = this.createVoters(this.voterDescriptions, probJavaScriptClient, randomVotingStrategy);

        // create simulation coordinator
        var SC = this.createPartyId(SimulationCoordinator.class);
        this.simulationCoordinator = new SimulationCoordinator(SC);
    }

    /**
     * Combines the given initialization task with the election simulator's current initialization task.
     *
     * @param newTask The new initialization task
     */
    public void addInitializationTask(Consumer<String> newTask) {
        this.initializationTask = this.initializationTask.andThen(newTask);
    }

    /**
     * Combines the given termination task with the election simulator's current termination task.
     *
     * @param newTask The new termination task
     */
    public void addTerminationTask(Consumer<String> newTask) {
        this.terminationTask = this.terminationTask.andThen(newTask);
    }

    /**
     * Combines the given abortion task with the election simulator's current abort task.
     *
     * @param newTask The new abortion task
     */
    public void addAbortionTask(Consumer<String> newTask) {
        this.abortionTask = this.abortionTask.andThen(newTask);
    }

    /**
     * Combines a new publication consumer with the election simulator's current publication consumer.
     *
     * @param newConsumer The new publication consumer
     */
    public void addPublicationConsumer(Consumer<? super List<Publication>> newConsumer) {
        this.publicationConsumer = this.publicationConsumer.andThen(newConsumer);
    }

    /**
     * Combines the given testing predicate with the election simulator's current testing predicate.
     *
     * @param newPredicate The new test data predicate
     */
    public void addTestingPredicate(BiPredicate<? super List<TestData>, ? super List<Publication>> newPredicate) {
        var currentPredicate = this.testingPredicate;
        this.testingPredicate = (testData, publicationData) -> currentPredicate.test(testData, publicationData) && newPredicate.test(testData, publicationData);
    }

    /**
     * Simulates a single election event for the given protocol.
     *
     * @param protocol The given protocol
     */
    public void simulateEvent(Class<? extends CHVote<?, ?>> protocol) {
        this.simulateEvents(protocol, 1);
    }

    /**
     * Simulates multiple simultaneous election events for the given protocol.
     *
     * @param protocol       The given protocol
     * @param numberOfEvents The number of election events
     */
    public void simulateEvents(Class<? extends CHVote<?, ?>> protocol, int numberOfEvents) {
        this.simulateEvents(Collections.nCopies(numberOfEvents, protocol));
    }

    /**
     * Simulates multiple simultaneous election events for a given collection of protocols.
     *
     * @param protocols The given collection of protocols
     */
    public void simulateEvents(Collection<Class<? extends CHVote<?, ?>>> protocols) {

        // subscribe all parties to all services
        this.subscribeParties();

        // create and start separate threads
        var threads = protocols.stream()
                .map(protocol -> Thread.ofVirtual().start(() -> simulateSingleEvent(protocol)))
                .toList();

        // wait for all threads to finish
        for (var thread : threads) {
            try {
                thread.join(); //
            } catch (InterruptedException ignored) {
            }
        }

        // unsubscribe all parties from all services
        this.unsubscribeParties();
    }

    // private method to simulate a single election event for the given protocol
    private void simulateSingleEvent(Class<? extends CHVote<?, ?>> protocol) {

        // create event id
        var eventId = this.createEventId(protocol);

        // define synchronizer
        var numberOfAuthorities = this.simulationConfiguration.getNumberOfAuthorities();
        var publicationCollector = new PublicationCollector(eventId, numberOfAuthorities + 3); // s x PEP1, 1 x PAP1, 1 x PAP2, 1 x PVP1
        var initializationMonitor = new EventServiceMonitor.Initialize(eventId);
        var terminationMonitor = new EventServiceMonitor.Terminate(eventId);
        var abortionMonitor = new EventServiceMonitor.Abort(eventId);
        var combinedNotifier = publicationCollector.and(terminationMonitor).or(abortionMonitor);
        var synchronizer = combinedNotifier.toSynchronizer();

        // add monitor tasks
        initializationMonitor.addTask(this.initializationTask);
        terminationMonitor.addTask(this.terminationTask);
        abortionMonitor.addTask(this.abortionTask);

        // run the event and wait for the given synchronizer
        this.simulateSingleEvent(eventId, protocol, synchronizer);

        // consume data
        this.publicationConsumer.accept(publicationCollector.getData());
    }

    /**
     * Runs a single election event for testing purposes. A boolean value representing the test result is returned.
     *
     * @param protocol The given protocol
     * @return The test result
     */
    public boolean testEvent(Class<? extends CHVote<?, ?>> protocol) {
        return this.testEvents(protocol, 1);
    }

    /**
     * Runs multiple simultaneous election events for testing purposes. A boolean value representing the conjunction of
     * all individual test results is returned. The protocol and the number of election events can be specified.
     *
     * @param protocol       The given protocol
     * @param numberOfEvents The given number of election events
     * @return The combined test result
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean testEvents(Class<? extends CHVote<?, ?>> protocol, int numberOfEvents) {
        return this.testEvents(Collections.nCopies(numberOfEvents, protocol));
    }

    /**
     * Tests multiple simultaneous election events for a given collection of protocols. The number of voters in these
     * election events can be specified. A boolean value representing the conjunction of all individual test results is
     * returned.
     *
     * @param protocols The given collection of protocols
     * @return The combined test result
     */
    public boolean testEvents(Collection<Class<? extends CHVote<?, ?>>> protocols) {

        // subscribe all parties to all services
        this.subscribeParties();

        // use an atomic boolean for keeping track of the combined test result
        var testResult = new AtomicBoolean(true);

        // create and start separate threads
        var threads = protocols.stream()
                .map(protocol -> Thread.ofVirtual().start(() -> testResult.compareAndSet(true, testSingleEvent(protocol))))
                .toList();

        // wait for all threads to finish
        for (var thread : threads) {
            try {
                thread.join(); //
            } catch (InterruptedException ignored) {
            }
        }

        // unsubscribe all parties from all services
        this.unsubscribeParties();

        // return combined result
        return testResult.get();
    }

    // private method to test a single election event for the given protocol
    private boolean testSingleEvent(Class<? extends CHVote<?, ?>> protocol) {

        // create event id
        var eventId = this.createEventId(protocol);

        // define synchronizer
        var numberOfAuthorities = this.simulationConfiguration.getNumberOfAuthorities();
        var numberOfVoters = this.simulationConfiguration.getNumberOfVoters();
        var testDataCollector = new TestDataCollector(eventId, numberOfVoters); // N x TVT1|TVT2
        var publicationCollector = new PublicationCollector(eventId, numberOfAuthorities + 3); // s x PEP1, 1 x PAP1, 1 x PAP2, 1 x PVP1
        var initializationMonitor = new EventServiceMonitor.Initialize(eventId);
        var terminationMonitor = new EventServiceMonitor.Terminate(eventId);
        var abortionMonitor = new EventServiceMonitor.Abort(eventId);
        var combinedNotifier = testDataCollector.and(publicationCollector).or(abortionMonitor);
        var synchronizer = combinedNotifier.toSynchronizer();

        // add monitor tasks
        initializationMonitor.addTask(this.initializationTask);
        terminationMonitor.addTask(this.terminationTask);
        abortionMonitor.addTask(this.abortionTask);

        // run the event and wait for the given synchronizer
        this.simulateSingleEvent(eventId, protocol, synchronizer);

        // test if event has been aborted
        if (abortionMonitor.isDone()) {
            return false;
        }

        // test data and return test result
        return this.testingPredicate.test(testDataCollector.getData(), publicationCollector.getData());
    }

    @Override
    public String toString() {
        return "ElectionSimulator-" + this.id;
    }

    // private core method for conducting an election event
    private void simulateSingleEvent(String eventId, Class<? extends CHVote<?, ?>> protocol, Synchronizer synchronizer) {

        // initialize event definition service
        var protocolName = Protocol.getPrintName(protocol);
        RandomEventDefinitionService.registerProtocolName(eventId, protocolName);
        RandomEventDefinitionService.registerSystemParties(eventId, this.systemPartyIds);
        RandomEventDefinitionService.registerVoterDescriptions(eventId, this.voterDescriptions);
        RandomEventDefinitionService.registerSimulationConfiguration(eventId, this.simulationConfiguration);

        // register parties
        this.registerParties(eventId, protocol);

        // let the coordinator initialize event
        var protocolId = Protocol.getId(protocol);
        var securityLevel = this.simulationConfiguration.getSecurityLevel();
        this.simulationCoordinator.initializeEvent(eventId, protocolId, securityLevel);

        // wait for synchronizer to finish
        synchronizer.await();
    }

    // private method for subscribing all parties created by the election simulator to all services
    private void subscribeParties() {
        this.systemParties.forEach(Party::subscribeToServices);
        this.voters.forEach(Party::subscribeToServices);
        this.voters.forEach(voter -> voter.getVotingClient().subscribeToServices());
        this.voters.forEach(voter -> voter.getInspectionClient().subscribeToServices());
    }

    // private method for unsubscribing all parties created by the election simulator from all services
    private void unsubscribeParties() {
        this.systemParties.forEach(Party::unsubscribeFromServices);
        this.voters.forEach(Party::unsubscribeFromServices);
        this.voters.forEach(voter -> voter.getVotingClient().unsubscribeFromServices());
        this.voters.forEach(voter -> voter.getInspectionClient().unsubscribeFromServices());
    }

    // private method for registering all parties at the simulation coordinator
    private void registerParties(String eventId, Class<? extends Protocol> protocol) {
        this.systemParties.forEach(party -> this.registerParty(eventId, protocol, party));
        this.voters.forEach(voter -> this.registerParty(eventId, protocol, voter));
        this.voters.forEach(voter -> this.registerParty(eventId, protocol, voter.getVotingClient()));
        this.voters.forEach(voter -> this.registerParty(eventId, protocol, voter.getInspectionClient()));
    }

    // private method to register the given party to participate at the given event
    private void registerParty(String eventId, Class<? extends Protocol> protocol, Party party) {
        var partyId = party.getId();
        var partyClass = party.getClass();
        var assignedRole = Party.getAssignedRole(partyClass, protocol);
        this.simulationCoordinator.registerParty(partyId, eventId, assignedRole);
    }

    // private helper method for creating a new unique event id
    private synchronized String createEventId(Class<? extends CHVote<?, ?>> protocol) {
        var protocolId = Protocol.getId(protocol);
        int nr = NUMBER_OF_EVENTS.merge(protocolId, 1, Integer::sum);
        var protocolName = Protocol.getPrintName(protocol);
        return protocolName + "-" + nr;
    }

    // private helper method for creating a unique id for a specific party
    private String createPartyId(Class<?> partyClass) {
        return partyClass.getSimpleName() + (this.id == 0 ? "" : "-" + this.id);
    }

    // private helper method for creating a unique id for a specific party
    private Vector<String> createPartyIds(Class<?> partyClass, int n) {
        return IntStream.rangeClosed(1, n)
                .mapToObj(index -> this.createPartyId(partyClass) + "-" + index)
                .collect(Vector.getCollector());
    }

    // private helper method for creating the list of voters
    private List<Voter> createVoters(Vector<String> voterIds, double probJavaScriptClient, RandomVotingStrategy randomVotingStrategy) {
        var numberOfVoters = voterIds.getLength();
        var numberOfVotersJava = (int) (numberOfVoters * (1 - probJavaScriptClient));
        return Stream.concat(
                        IntStream.rangeClosed(1, numberOfVotersJava)
                                .mapToObj(voterIds::getValue)
                                .map(voterId -> new Voter(voterId, randomVotingStrategy, AlgorithmService.Language.JAVA)),
                        IntStream.rangeClosed(numberOfVotersJava + 1, numberOfVoters)
                                .mapToObj(voterIds::getValue)
                                .map(voterId -> new Voter(voterId, randomVotingStrategy, AlgorithmService.Language.JAVASCRIPT)))
                .toList();
    }

}
