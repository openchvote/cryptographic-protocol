/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import ch.openchvote.core.administrator.EventDefinitionService;
import ch.openchvote.simulation.simulator.random.RandomEventDefinitionService;

/**
 * This module implements the execution of the CHVote protocol.
 */
module ch.openchvote.simulation.simulator {

    requires ch.openchvote.core.administrator;
    requires ch.openchvote.core.electionauthority;
    requires ch.openchvote.core.printingauthority;
    requires ch.openchvote.core.protocol;
    requires ch.openchvote.core.verifier;
    requires ch.openchvote.simulation.voter;

    // needed for unit tests
    requires ch.openchvote.core.services;
    requires ch.openchvote.simulation.services;

    exports ch.openchvote.simulation.simulator;
    exports ch.openchvote.simulation.simulator.random;

    provides EventDefinitionService with RandomEventDefinitionService.Plain, RandomEventDefinitionService.WriteIn;

}
