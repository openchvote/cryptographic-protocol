/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.simulator;/*
 * Copyright (C) 2024 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */

import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.core.algorithms.protocols.plain.model.ElectionParametersPlain;
import ch.openchvote.core.algorithms.protocols.plain.model.ElectionResultPlain;
import ch.openchvote.core.algorithms.protocols.writein.model.ElectionParametersWriteIn;
import ch.openchvote.core.algorithms.protocols.writein.model.ElectionResultWriteIn;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteIn;
import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.base.framework.communication.Publication;
import ch.openchvote.base.framework.communication.TestData;
import ch.openchvote.base.framework.services.SerializationService;
import ch.openchvote.base.framework.services.Service;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.core.protocol.protocols.writein.content.publication.PAP1;
import ch.openchvote.core.protocol.protocols.writein.content.publication.PVP1;
import ch.openchvote.core.protocol.protocols.writein.content.testdata.TVT1;

import java.util.ArrayList;
import java.util.List;

/**
 * This class defines a number of static methods to verify if the election result obtained from the administrator
 * corresponds to the plaintext votes obtained from the voters. The main entry point is the method
 * {@link TestMethods#checkElectionResult}, which expects corresponding {@link TestData} and {@link Publication}
 * objects.
 */
public class TestMethods {

    static private final SerializationService.Content SERIALIZATION_SERVICE = Service.load(SerializationService.Content.class);

    /**
     * Checks if the election result included in the given list of publications corresponds to the plaintext votes
     * included in the test data.
     *
     * @param testDataList    The given list of test data
     * @param publicationList The given list of publications
     * @return {@code true}, if the election result corresponds to the plaintext votes, {@code false} otherwise
     */
    static public boolean checkElectionResult(List<TestData> testDataList, List<Publication> publicationList) {

        var dataPlain = new DataPlain();
        var dataWriteIn = new DataWriteIn();

        // sort deserialized test data
        for (var testData : testDataList) {

            // deserialize test data
            var contentId = testData.getContentId();
            var contentClass = Content.getClass(contentId);
            var content = SERIALIZATION_SERVICE.deserialize(testData.getContentString(), contentClass);

            // dispatch different publication types: plain
            if (content instanceof ch.openchvote.core.protocol.protocols.plain.content.testdata.TVT1 TVT1) {
                dataPlain.addVote(TVT1.get_v(), TVT1.get_bold_s());
            }

            // dispatch different publication types: writein
            if (content instanceof TVT1 TVT1) {
                dataWriteIn.addVote(TVT1.get_bold_v(), TVT1.get_bold_s(), TVT1.get_bold_s_prime());
            }
        }

        // sort deserialized publications
        for (var publication : publicationList) {

            // deserialize publication
            var contentId = publication.getContentId();
            var contentClass = Content.getClass(contentId);
            var content = SERIALIZATION_SERVICE.deserialize(publication.getContentString(), contentClass);

            // dispatch different publication types: plain
            if (content instanceof ch.openchvote.core.protocol.protocols.plain.content.publication.PAP1 PAP1) {
                dataPlain.set_ES(PAP1.get_ES());
                dataPlain.set_EP(PAP1.get_EP());
                dataPlain.set_ER(PAP1.get_ER());
            }
            if (content instanceof ch.openchvote.core.protocol.protocols.plain.content.publication.PVP1 PVP1) {
                dataPlain.set_v(PVP1.get_v());
            }

            // dispatch different publication types: writein
            if (content instanceof PAP1 PAP1) {
                dataWriteIn.set_ES(PAP1.get_ES());
                dataWriteIn.set_EP(PAP1.get_EP());
                dataWriteIn.set_ER(PAP1.get_ER());
            }
            if (content instanceof PVP1 PVP1) {
                dataWriteIn.set_v(PVP1.get_v());
            }
        }

        if (dataPlain.isAvailable()) {
            return dataPlain.v && dataPlain.ER.isEquivalent(dataPlain.getElectionResult());
        }

        if (dataWriteIn.isAvailable()) {
            return dataWriteIn.v && dataWriteIn.ER.isEquivalent(dataWriteIn.getElectionResult());
        }

        // something must have gone wrong
        return false;
    }

    static private class DataPlain {

        // from publications
        private EventSetup ES;
        private ElectionParametersPlain EP;
        private ElectionResultPlain ER;
        private boolean v;

        // from test data
        private final List<Integer> voterIds;
        private final List<IntVector> votes;

        public DataPlain() {
            this.v = false;
            this.voterIds = new ArrayList<>();
            this.votes = new ArrayList<>();
        }

        public boolean isAvailable() {
            return this.ES != null && this.EP != null && this.ER != null;
        }

        public void set_ES(EventSetup ES) {
            this.ES = ES;
        }

        public void set_EP(ElectionParametersPlain EP) {
            this.EP = EP;
        }

        public void set_ER(ElectionResultPlain ER) {
            this.ER = ER;
        }

        public void set_v(boolean v) {
            this.v = v;
        }

        public synchronized void addVote(Integer v, IntVector bold_s) {
            this.voterIds.add(v);
            this.votes.add(bold_s);
        }

        public ElectionResultPlain getElectionResult() {
            var bold_w = this.EP.get_bold_w();
            var bold_V = TestMethods.get_bold_V(this.votes, this.EP.get_bold_n());
            var bold_W = TestMethods.get_bold_W(this.voterIds, bold_w);
            return new ElectionResultPlain(bold_V, bold_W);
        }

    }

    static private class DataWriteIn {

        // from publications
        private EventSetup ES;
        private ElectionParametersWriteIn EP;
        private ElectionResultWriteIn ER;
        private boolean v;

        // from test data
        private final List<Integer> voterIds;
        private final List<IntVector> votes;
        private final List<Vector<WriteIn>> writeIns;

        public DataWriteIn() {
            this.v = false;
            this.voterIds = new ArrayList<>();
            this.votes = new ArrayList<>();
            this.writeIns = new ArrayList<>();
        }

        public boolean isAvailable() {
            return this.ES != null && this.EP != null && this.ER != null;
        }

        public void set_ES(EventSetup ES) {
            this.ES = ES;
        }

        public void set_EP(ElectionParametersWriteIn EP) {
            this.EP = EP;
        }

        public void set_ER(ElectionResultWriteIn ER) {
            this.ER = ER;
        }

        public void set_v(boolean v) {
            this.v = v;
        }

        public synchronized void addVote(Integer v, IntVector bold_s, Vector<WriteIn> bold_s_prime) {
            this.voterIds.add(v);
            this.votes.add(bold_s);
            this.writeIns.add(bold_s_prime);
        }

        public ElectionResultWriteIn getElectionResult() {
            var bold_u = this.EP.get_bold_u();
            var bold_n = this.EP.get_bold_n();
            var bold_k = this.EP.get_bold_k();
            var bold_w = this.EP.get_bold_w();
            var bold_E = this.EP.get_bold_E();
            var bold_z = this.EP.get_bold_z();
            var bold_V = TestMethods.get_bold_V(this.votes, bold_n);
            var bold_W = TestMethods.get_bold_W(this.voterIds, bold_w);
            var bold_S = TestMethods.get_bold_S(this.writeIns, bold_k, bold_u, bold_E, bold_z);
            var bold_T = TestMethods.get_bold_T(this.voterIds, bold_k, bold_u, bold_E, bold_z);
            return new ElectionResultWriteIn(bold_V, bold_W, bold_S, bold_T);
        }

    }

    static private IntMatrix get_bold_V(List<IntVector> votes, IntVector bold_n) {
        var n = bold_n.sum();
        var N = votes.size();
        var builder_bold_V = new IntMatrix.RowBuilder(N, n);
        for (var bold_s : votes) {
            var builder_bold_v_i = new IntVector.Builder(n);
            builder_bold_v_i.fill(0);
            for (int s : bold_s) {
                builder_bold_v_i.set(s, 1);
            }
            var bold_v_i = builder_bold_v_i.build();
            builder_bold_V.addRow(bold_v_i);
        }
        return builder_bold_V.build();
    }

    static private IntMatrix get_bold_W(List<Integer> voterIds, IntVector bold_w) {
        var w = bold_w.maxOrZero();
        var N = voterIds.size();
        var builder_bold_W = new IntMatrix.RowBuilder(N, w);
        for (var v : voterIds) {
            var builder_bold_w_i = new IntVector.Builder(w);
            builder_bold_w_i.fill(0);
            builder_bold_w_i.set(bold_w.getValue(v), 1);
            var bold_w_i = builder_bold_w_i.build();
            builder_bold_W.addRow(bold_w_i);
        }
        return builder_bold_W.build();
    }

    static private Matrix<WriteIn> get_bold_S(List<Vector<WriteIn>> writeIns, IntVector bold_k, IntVector bold_u, IntMatrix bold_E, IntVector bold_z) {
        var z_max = bold_E.select(bold_u).multiply(bold_z.times(bold_k)).maxOrZero();
        var N = writeIns.size();
        var builder_bold_S = new Matrix.RowBuilder<WriteIn>(N, z_max);
        for (var bold_s_prime : writeIns) {
            var builder_bold_s_i = new Vector.Builder<WriteIn>(z_max);
            builder_bold_s_i.fill(null);
            builder_bold_s_i.addAll(bold_s_prime);
            var bold_s_i = builder_bold_s_i.build();
            builder_bold_S.addRow(bold_s_i);
        }
        return builder_bold_S.build();
    }

    static private IntMatrix get_bold_T(List<Integer> voterIds, IntVector bold_k, IntVector bold_u, IntMatrix bold_E, IntVector bold_z) {
        var N = voterIds.size();
        var t = bold_k.getLength();
        var z_max = bold_E.select(bold_u).multiply(bold_z.times(bold_k)).maxOrZero();
        var builder_bold_T = new IntMatrix.RowBuilder(N, z_max);
        for (var v : voterIds) {
            var bold_e_hat_v = bold_E.getRow(v).expand(bold_u);
            var builder_bold_t_i = new IntVector.Builder(z_max);
            builder_bold_t_i.fill(0);
            var i = 1;
            var k_prime = 0;
            for (int l : IntSet.range(1, t)) {
                var k_l = bold_k.getValue(l);
                if (bold_z.getValue(l) == 1 && bold_e_hat_v.getValue(l) == 1) {
                    for (int k : IntSet.range(k_prime + 1, k_prime + k_l)) {
                        builder_bold_t_i.set(i, l);
                        i++;
                    }
                }
                k_prime = k_prime + k_l;
            }
            var bold_t_i = builder_bold_t_i.build();
            builder_bold_T.addRow(bold_t_i);
        }
        return builder_bold_T.build();
    }

}
