/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.simulator

import ch.openchvote.core.protocol.parameters.SecurityLevel
import ch.openchvote.core.protocol.parameters.UsabilityConfiguration
import ch.openchvote.core.protocol.protocols.plain.Plain
import ch.openchvote.core.protocol.protocols.writein.WriteIn
import ch.openchvote.base.utilities.tools.RandomFactory
import spock.lang.Specification
import spock.lang.Unroll

class ElectionSimulatorSpec extends Specification {

    @Unroll
    def "testRunEvents"() throws InterruptedException {
        given:
        def loggerLevel = System.Logger.Level.OFF
        def usabilityConfiguration = UsabilityConfiguration.TEST
        def simulatorConfiguration = new SimulationConfiguration(loggerLevel, randomFactoryMode, securityLevel, usabilityConfiguration, numAuthorities, numVoters, t_max, u_max, n_max, k_max, w_max, p_circle as double, p_eligibility as double, p_writeIn as double, p_participation as double, p_javaScript as double)

        when:
        // create election simulator based on config file
        def electionSimulator = new ElectionSimulator(simulatorConfiguration)

        // add progress bar output
        electionSimulator.addInitializationTask(eventId -> print(".."))
        electionSimulator.addTerminationTask(eventId -> print(".."))

        // add test predicate
        electionSimulator.addTestingPredicate(TestMethods::checkElectionResult)

        then:
        electionSimulator.testEvents(Plain.class, numEvents) // returns true/false
        electionSimulator.testEvents(WriteIn.class, numEvents) // returns true/false

        where:
        numAuthorities | numEvents | securityLevel         | numVoters | u_max | t_max | n_max | k_max | w_max | p_circle | p_eligibility | p_participation | p_writeIn | p_javaScript | randomFactoryMode
        // Edge cases: t=0 (no elections)
        1              | 0         | SecurityLevel.LEVEL_1 | 0         | 0     | 0     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        1              | 1         | SecurityLevel.LEVEL_1 | 0         | 0     | 0     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        1              | 0         | SecurityLevel.LEVEL_1 | 1         | 0     | 0     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        1              | 1         | SecurityLevel.LEVEL_1 | 1         | 0     | 0     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        2              | 2         | SecurityLevel.LEVEL_1 | 10        | 0     | 0     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        // Edge cases: N_E=0 (no voters)
        1              | 0         | SecurityLevel.LEVEL_1 | 0         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        1              | 1         | SecurityLevel.LEVEL_1 | 0         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        1              | 2         | SecurityLevel.LEVEL_1 | 0         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        2              | 0         | SecurityLevel.LEVEL_1 | 0         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        2              | 1         | SecurityLevel.LEVEL_1 | 0         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        2              | 2         | SecurityLevel.LEVEL_1 | 0         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        // Edge cases: N_E=1 (one voter, deterministic)
        1              | 0         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        1              | 1         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        1              | 2         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        2              | 0         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        2              | 1         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        2              | 2         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        // Edge cases: N_E=1 (one voter, random)
        1              | 0         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        1              | 1         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        1              | 2         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        2              | 0         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        2              | 1         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        2              | 2         | SecurityLevel.LEVEL_1 | 1         | 1     | 1     | 2     | 1     | 1     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        // Non-edge cases: 10 voters (small election, deterministic)
        1              | 1         | SecurityLevel.LEVEL_1 | 10        | 1     | 1     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        1              | 2         | SecurityLevel.LEVEL_1 | 10        | 1     | 1     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        2              | 1         | SecurityLevel.LEVEL_1 | 10        | 1     | 1     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        2              | 2         | SecurityLevel.LEVEL_1 | 10        | 1     | 1     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        // Non-edge cases: 10 voters (small election, random)
        1              | 1         | SecurityLevel.LEVEL_1 | 10        | 1     | 1     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        1              | 2         | SecurityLevel.LEVEL_1 | 10        | 1     | 1     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        2              | 1         | SecurityLevel.LEVEL_1 | 10        | 1     | 1     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        2              | 2         | SecurityLevel.LEVEL_1 | 10        | 1     | 1     | 2     | 1     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        // Non-edge cases: 10 voters (large election, deterministic)
        1              | 1         | SecurityLevel.LEVEL_1 | 10        | 1     | 2     | 5     | 3     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        1              | 2         | SecurityLevel.LEVEL_1 | 10        | 1     | 2     | 5     | 3     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        2              | 1         | SecurityLevel.LEVEL_1 | 10        | 2     | 3     | 5     | 3     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        2              | 2         | SecurityLevel.LEVEL_1 | 10        | 2     | 3     | 5     | 3     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.DETERMINISTIC
        // Non-edge cases: 10 voters (large election, random)
        1              | 1         | SecurityLevel.LEVEL_1 | 10        | 1     | 2     | 5     | 3     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        1              | 2         | SecurityLevel.LEVEL_1 | 10        | 1     | 2     | 5     | 3     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        2              | 1         | SecurityLevel.LEVEL_1 | 10        | 2     | 3     | 5     | 3     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        2              | 2         | SecurityLevel.LEVEL_1 | 10        | 2     | 3     | 5     | 3     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        // Non-edge cases: 10 voters (very large election, random)
        1              | 1         | SecurityLevel.LEVEL_1 | 10        | 5     | 10    | 8     | 5     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        1              | 2         | SecurityLevel.LEVEL_1 | 10        | 5     | 10    | 8     | 5     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        2              | 1         | SecurityLevel.LEVEL_1 | 10        | 5     | 10    | 8     | 5     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
        2              | 2         | SecurityLevel.LEVEL_1 | 10        | 5     | 10    | 8     | 5     | 4     | 0.75     | 0.8           | 0.8             | 0.5       | 0.2          | RandomFactory.Mode.RANDOM
    }

}