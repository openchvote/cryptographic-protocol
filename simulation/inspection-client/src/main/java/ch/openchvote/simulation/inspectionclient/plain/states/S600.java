/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.inspectionclient.plain.states;

import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Input;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.simulation.inspectionclient.InspectionClient;
import ch.openchvote.simulation.inspectionclient.plain.EventContext;
import ch.openchvote.core.protocol.phases.Inspection;
import ch.openchvote.core.protocol.protocols.plain.content.requestresponse.request.RIA1;
import ch.openchvote.core.protocol.protocols.plain.content.userinterface.input.UVI1;

@SuppressWarnings("MissingJavadoc")
@Phase(Inspection.class)
@Notify(Status.Type.READY)
public final class S600 extends State<InspectionClient, EventContext> {

    public S600(InspectionClient inspectionClient, EventContext eventContext) {
        super(inspectionClient, eventContext);
        this.registerInputHandler(UVI1.class, this::handleUVI1);
    }

    private void handleUVI1(Input input) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();

        // get input content
        var UVI1 = this.party.getContent(UVI1.class, input);

        // update public data
        publicData.setContent(UVI1);

        // select event data
        var AD = publicData.get_AD().get();
        var v = publicData.get_v().get();

        // send RIA1 to administrator
        this.party.sendRequest(eventId, AD, new RIA1(v));

        // update state
        this.party.updateState(this.eventContext, S610.class);

    }

}
