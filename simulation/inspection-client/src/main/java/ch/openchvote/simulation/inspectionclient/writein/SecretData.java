/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.inspectionclient.writein;

import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.base.utilities.tuples.NullTuple;

/**
 * Instances of this class represent the inspection client's secret data. In this particular case, the class inherits from
 * {@link NullTuple}, because inspection clients have no secret data.
 */
@SuppressWarnings("MissingJavadoc")
public final class SecretData extends NullTuple implements EventData {

    public SecretData() {
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private SecretData(SecretData secretData) {
        this();
    }

}
