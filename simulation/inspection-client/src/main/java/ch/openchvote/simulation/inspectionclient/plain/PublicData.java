/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.inspectionclient.plain;

import ch.openchvote.core.algorithms.protocols.common.model.VotingDescriptions;
import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.core.algorithms.protocols.plain.model.VotingParametersPlain;
import ch.openchvote.core.protocol.protocols.plain.content.requestresponse.response.RAI1;
import ch.openchvote.core.protocol.protocols.plain.content.requestresponse.response.REI1;
import ch.openchvote.core.protocol.protocols.plain.content.userinterface.input.UVI1;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.tuples.Septuple;
import ch.openchvote.base.utilities.tuples.Tuple;

/**
 * Instances of this class represent the inspection client's public data. The class inherits from the generic {@link Tuple}
 * subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects to store. 
 * Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class PublicData extends Septuple<
        // the administrator's id
        EventData.Data<String>, // AD
        // voter index
        EventData.Data<Integer>, // v
        // event definition
        EventData.Data<EventSetup>, // ES
        EventData.Data<VotingParametersPlain>, // VP_v
        EventData.Data<VotingDescriptions>, // VD_v
        // inspection code
        EventData.DataMap<ByteArray>, // bold_i
        EventData.Data<String>> // IC
        implements EventData {

    public PublicData() {
        this(new Data<>(), new Data<>(), new Data<>(), new Data<>(), new Data<>(), new DataMap<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private PublicData(Data<String> AD, Data<Integer> v, Data<EventSetup> ES, Data<VotingParametersPlain> VP_v, Data<VotingDescriptions> VD_v, DataMap<ByteArray> bold_i, Data<String> IC) {
        super(AD, v, ES, VP_v, VD_v, bold_i, IC);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private PublicData(PublicData publicData) {
        this(publicData.get_AD(), publicData.get_v(), publicData.get_ES(), publicData.get_VP_v(), publicData.get_VD_v(), publicData.get_bold_i(), publicData.get_IC());
    }

    public Data<String> get_AD() {
        return this.getFirst();
    }

    public Data<Integer> get_v() {
        return this.getSecond();
    }

    public Data<EventSetup> get_ES() {
        return this.getThird();
    }

    public Data<VotingParametersPlain> get_VP_v() {
        return this.getFourth();
    }

    public Data<VotingDescriptions> get_VD_v() {
        return this.getFifth();
    }

    public DataMap<ByteArray> get_bold_i() {
        return this.getSixth();
    }

    public Data<String> get_IC() {
        return this.getSeventh();
    }

    /**
     * Calling this method stores the given content of type {@link RAI1} into the inspection client's public data.
     *
     * @param content The given content
     */
    public void setContent(RAI1 content) {
        this.get_ES().set(content.get_ES());
        this.get_VP_v().set(content.get_VP());
        this.get_VD_v().set(content.get_VD());
    }

    /**
     * Calling this method stores the given content of type {@link REI1} into the inspection client's public data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, REI1 content) {
        this.get_bold_i().set(index, content.get_I());
    }

    /**
     * Calling this method stores the given content of type {@link UVI1} into the inspection client's public data.
     *
     * @param content The given content
     */
    public void setContent(UVI1 content) {
        this.get_AD().set(content.get_AD());
        this.get_v().set(content.get_v());
    }

}
