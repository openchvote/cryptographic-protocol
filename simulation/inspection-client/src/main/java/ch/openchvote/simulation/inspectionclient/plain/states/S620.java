/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.inspectionclient.plain.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Response;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.simulation.inspectionclient.InspectionClient;
import ch.openchvote.simulation.inspectionclient.plain.EventContext;
import ch.openchvote.simulation.inspectionclient.plain.states.error.A1;
import ch.openchvote.simulation.inspectionclient.plain.tasks.TI1;
import ch.openchvote.core.protocol.phases.Inspection;
import ch.openchvote.core.protocol.protocols.plain.content.requestresponse.response.REI1;
import ch.openchvote.core.protocol.protocols.plain.content.userinterface.output.UIV1;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Inspection.class)
public final class S620 extends State<InspectionClient, EventContext> {

    public S620(InspectionClient inspectionClient, EventContext eventContext) {
        super(inspectionClient, eventContext);
        this.registerResponseHandler(REI1.class, this::handleREI1);
    }

    private void handleREI1(Response response) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var j = ES.get_SP().getIndexOf(response.getResponderId());

        // select voting parameters and descriptions
        var VP_v = publicData.get_VP_v().get();
        var VD_v = publicData.get_VD_v().get();

        // get and check response content
        var aux = new Triple<>(ES, VP_v, VD_v);
        var REI1 = this.party.getAndCheckContent(REI1.class, response, aux, securityLevel);

        // update public data
        publicData.setContent(j, REI1);

        // run self activation
        var stateId = this.getId();
        this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

    }

    @SuppressWarnings("unused")
    @Override
    public void handleSelfActivation() {

        // get algorithm service from voting client
        var algorithmService = this.party.getAlgorithmService();

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();

        // check if all MEC4 messages are available
        if (TI1.isReady(publicData)) {
            try {
                // perform task
                TI1.run(publicData, algorithmService);

                // select event data
                var IC = publicData.get_IC().get();

                // send UIV1 to voter
                this.party.displayOutput(this.party.getVoter(), eventId, new UIV1(IC));

                // update state
                this.party.updateState(this.eventContext, FINAL.class);

            } catch (Algorithm.Exception exception) {
                // move to error state
                this.party.updateState(this.eventContext, A1.class);
            }
        }
    }

}
