/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.inspectionclient.plain.states;

import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Response;
import ch.openchvote.base.framework.exceptions.CommunicationException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.base.framework.protocol.Protocol;
import ch.openchvote.simulation.inspectionclient.InspectionClient;
import ch.openchvote.simulation.inspectionclient.plain.EventContext;
import ch.openchvote.core.protocol.phases.Inspection;
import ch.openchvote.core.protocol.protocols.plain.content.requestresponse.request.RIE1;
import ch.openchvote.core.protocol.protocols.plain.content.requestresponse.response.RAI1;

import static ch.openchvote.base.framework.exceptions.CommunicationException.Type.INCONSISTENT_EVENT_SETUP;

@SuppressWarnings("MissingJavadoc")
@Phase(Inspection.class)
public final class S610 extends State<InspectionClient, EventContext> {

    public S610(InspectionClient inspectionClient, EventContext eventContext) {
        super(inspectionClient, eventContext);
        this.registerResponseHandler(RAI1.class, this::handleRAI1);
    }

    private void handleRAI1(Response response) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var protocolId = this.eventContext.getProtocolId();
        var protocolName = Protocol.getPrintName(protocolId);
        var securityLevel = this.eventContext.getSecurityLevel();

        // get and check response content (no auxiliary data)
        var RAI1 = this.party.getAndCheckContent(RAI1.class, response, null, securityLevel);

        // check election setup consistency
        if (!RAI1.get_ES().checkConsistency(protocolName, securityLevel) || !RAI1.get_VP().checkConsistency(eventId)) {
            throw new CommunicationException(INCONSISTENT_EVENT_SETUP, response);
        }

        // update public data
        publicData.setContent(RAI1);

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SP = ES.get_SP();
        var bold_ea = SP.get_bold_ea();

        // select event data
        var v = publicData.get_v().get();

        // send RIE1 to all election authorities
        this.party.sendRequest(eventId, bold_ea, new RIE1(v));

        // update state
        this.party.updateState(this.eventContext, S620.class);

    }

}
