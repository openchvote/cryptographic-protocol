# Inspection Client

This module provides an implementation of the CHVote party *"Inspection Client"*. In
the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325), the role of the inspection client is described as
follows:

> The [...] inspection client is a machine used by some voter [...] to inspect the election participation in the
> aftermath of an
> election. Typically, this machine is either a desktop, notebook, or tablet computer with a network connection and
> enough computational power to perform cryptographic computations. The strict separation between the voter and
> the [...] inspection client is an important precondition for the protocol’s security concept.

The main purpose of this module is to provide an exemplary implementation of the inspection client in Java, which can be
used for simulating election events in a single JVM. Robust implementations of corresponding services can be connected
to this module by using
Java's `ServiceLoader.load()` functionality.

## Specification

### Phases and States

| Phase          | States           | Error States | Tasks                           |
|:---------------|:-----------------|:-------------|:--------------------------------|
| Initialization | –                | –            | –                               |
| Preparation    | –                | –            | –                               |
| Election       | –                | –            | –                               |
| Tallying       | –                | –            | –                               |
| Verification   | –                | –            | –                               |
| Inspection     | S600, S610, S620 | A1           | Inspect vote cast or abstention |
| Testing        | –                | –            | –                               |
| [none]         | FINAL            | –            | –                               |

### State Diagram

![StateDiagram](img/stateDiagram.svg)

### Communication

#### Services:

|                        | InspectionClient |
|:-----------------------|:-----------------|
| EventService           | Subscriber       |
| MailingService         | –                |
| MessagingService       | –                |
| PublicationService     | –                |
| RequestResponseService | Requester        |
| UserInterface          | Device           |
| TestingService         | –                |

#### Incoming Messages:

| State | Phase      | Protocol | Name | Type     | Sender             | Signed | Encrypted | Content                        |
|:-----:|:-----------|:--------:|:----:|:---------|:-------------------|:------:|:---------:|--------------------------------|
| S600  | Inspection |   7.12   | UVI1 | Input    | Voter              |   no   |    no     | Administrator id, voter index  |
| S610  | Inspection |   7.12   | RAI1 | Response | Administrator      |  yes   |    no     | Event setup, voting parameters |
| S620  | Inspection |   7.12   | REI1 | Response | Election authority |  yes   |    no     | Inspection                     |

#### Outgoing Messages:

| State | Phase      | Protocol | Name | Type    | Receiver           | Signed | Encrypted | Content         |
|:-----:|:-----------|:--------:|:----:|:--------|:-------------------|:------:|:---------:|:----------------|
| S500  | Inspection |   7.12   | RIA1 | Request | Administrator      |   no   |    no     | Voter index     |
| S510  | Inspection |   7.12   | RIE1 | Request | Election authority |   no   |    no     | Voter index     |
| S520  | Inspection |   7.12   | UIV1 | Output  | Voter              |   no   |    no     | Inspection code |

#### Coordination:

| Type           | State | Phase      | New State | New Phase  |
|:---------------|:-----:|:-----------|:---------:|:----------:|
| onInitialize() |   –   | –          |   S600    | Inspection |
| onStart()      | S600  | Inspection |   S600    | Inspection |
| onTerminate()  | FINAL | [none]     |     –     |     –      |

### Tasks

| Task | State | Phase      | Protocol | AlgorithmicException | TaskException |
|:----:|:-----:|:-----------|:--------:|:--------------------:|:-------------:|
| TI1  | S620  | Inspection |   7.12   |          A1          |       –       |

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of
this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts
is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as
follows:

```console
mvn clean install
-- a lot of output
```

## Maven Dependencies

The following dependency tree shows the Maven dependencies of this module.

```console
mvn dependency:tree -Dscope=compile -Dverbose=false
[INFO] --- dependency:3.6.0:tree (default-cli) @ inspection-client ---
[INFO] io.gitlab.openchvote:inspection-client:jar:2.4
[INFO] \- io.gitlab.openchvote:protocol:jar:2.4:compile
[INFO]    +- io.gitlab.openchvote:algorithms:jar:2.4:compile
[INFO]    |  \- io.gitlab.openchvote:utilities:jar:2.4:compile
[INFO]    |     \- com.verificatum:vmgj:jar:1.2.2:compile
[INFO]    \- io.gitlab.openchvote:framework:jar:2.4:compile
```

## Java Module Dependencies

The following module dependency diagram illustrates the Java module dependencies to other modules within this project:

![Java module dependencies](img/inspectionClient.svg)