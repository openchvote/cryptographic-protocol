/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.plain.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Response;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.core.protocol.protocols.plain.content.requestresponse.request.RCE2;
import ch.openchvote.core.protocol.protocols.plain.content.requestresponse.response.REC1;
import ch.openchvote.base.utilities.tuples.Triple;
import ch.openchvote.simulation.votingclient.VotingClient;
import ch.openchvote.simulation.votingclient.plain.EventContext;
import ch.openchvote.simulation.votingclient.plain.states.error.A2;
import ch.openchvote.simulation.votingclient.plain.states.error.A3;
import ch.openchvote.simulation.votingclient.plain.states.error.B2;
import ch.openchvote.simulation.votingclient.plain.tasks.TC2;
import ch.openchvote.simulation.votingclient.plain.tasks.TC3;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
public final class S330 extends State<VotingClient, EventContext> {

    public S330(VotingClient votingClient, EventContext eventContext) {
        super(votingClient, eventContext);
        this.registerResponseHandler(REC1.class, this::handleREC1);
    }

    private void handleREC1(Response response) {

        // get algorithm service from voting client
        var algorithmService = this.party.getAlgorithmService();

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var j = ES.get_SP().getIndexOf(response.getResponderId());

        // select voting parameters and descriptions
        var VP_v = publicData.get_VP_v().get();
        var VD_v = publicData.get_VD_v().get();

        // get and check response content
        var aux = new Triple<>(ES, VP_v, VD_v);
        var REC1 = this.party.getAndCheckContent(REC1.class, response, aux, securityLevel);

        // update public data
        publicData.setContent(j, REC1);

        try {
            // perform task
            TC2.run(j, publicData, algorithmService);

            // run self activation
            var stateId = this.getId();
            this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A2.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B2.class);
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void handleSelfActivation() {

        // get algorithm service from voting client
        var algorithmService = this.party.getAlgorithmService();

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SP = ES.get_SP();
        var bold_ea = SP.get_bold_ea();

        // check if all REC1 messages are available
        if (TC3.isReady(publicData)) {
            try {
                // perform task
                TC3.run(publicData, secretData, algorithmService);

                // select event data
                var v = publicData.get_v().get();
                var alpha = publicData.get_alpha().get();

                // send RCE2 to election authorities
                this.party.sendRequest(eventId, bold_ea, new RCE2(v, alpha));

                // update state
                this.party.updateState(this.eventContext, S340.class);

            } catch (Algorithm.Exception exception) {
                // move to error state
                this.party.updateState(this.eventContext, A3.class);
            }
        }
    }

}
