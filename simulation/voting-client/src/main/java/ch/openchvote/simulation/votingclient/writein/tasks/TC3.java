/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetPublicKey;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.GenBallot;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.GetPublicKeys;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.core.protocol.protocols.writein.UsabilityParameters;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.simulation.votingclient.writein.PublicData;
import ch.openchvote.simulation.votingclient.writein.SecretData;

@SuppressWarnings("MissingJavadoc")
public final class TC3 {

    static public boolean isReady(PublicData publicData) {
        var s = publicData.get_ES().get().get_SP().get_s();
        return publicData.get_bold_pk().arePresent(IntSet.range(0, s)) && publicData.get_bold_PK_prime().arePresent(IntSet.range(0, s));
    }

    static public void
    run(PublicData publicData, SecretData secretData, AlgorithmService algorithmService) {

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();
        var UC = ES.get_UC();

        // select voting parameters
        var VP_v = publicData.get_VP_v().get();

        // get security and usability parameters
        var securityParameters = new SecurityParameters(SL);
        var usabilityParameters = new UsabilityParameters(UC, SL);

        // select event data
        var X_v = secretData.get_X_v().get();
        var bold_s = secretData.get_bold_s().get();
        var bold_s_prime = secretData.get_bold_s_prime().get();
        var bold_pk = publicData.get_bold_pk().mapTo(Vector::of);
        var bold_PK_prime = publicData.get_bold_PK_prime().mapTo(Matrix::ofCols);

        // perform task
        var pk = algorithmService.run(GetPublicKey.class, securityParameters, bold_pk);
        var bold_pk_prime = algorithmService.run(GetPublicKeys.class, securityParameters, bold_PK_prime);
        var bold_n = VP_v.get_bold_n();
        var bold_k = VP_v.get_bold_k();
        var w_v = VP_v.get_w_v();
        var bold_e_hat_v = VP_v.get_bold_e_hat_v();
        var bold_v = VP_v.get_bold_v();
        var bold_z = VP_v.get_bold_z();
        var pair = algorithmService.run(GenBallot.class, securityParameters, usabilityParameters, X_v, bold_s, bold_s_prime, pk, bold_pk_prime, bold_n, bold_k, w_v, bold_e_hat_v, bold_v, bold_z);
        var alpha = pair.getFirst();
        var bold_r = pair.getSecond();

        // update event data
        publicData.get_pk().set(pk);
        publicData.get_bold_pk_prime().set(bold_pk_prime);
        publicData.get_alpha().set(alpha);
        secretData.get_bold_r().set(bold_r);
    }

}
