/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.writein;

import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteIn;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.input.UVC2;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.input.UVC3;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Quintuple;
import ch.openchvote.base.utilities.tuples.Tuple;

import java.math.BigInteger;

/**
 * Instances of this class represent the voting client's secret data. The class inherits from the generic {@link Tuple}
 * subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects to store.
 * Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class SecretData extends Quintuple<
        // ballot
        EventData.Data<String>, // X_v
        EventData.Data<IntVector>, // bold_s
        EventData.Data<Vector<WriteIn>>, // bold_s_prime
        EventData.Data<Vector<BigInteger>>, // bold_r
        // confirmation
        EventData.Data<String>> // Y_v
        implements EventData {

    public SecretData() {
        this(new Data<>(), new Data<>(), new Data<>(), new Data<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private SecretData(Data<String> X_v, Data<IntVector> bold_s, Data<Vector<WriteIn>> bold_s_prime, Data<Vector<BigInteger>> bold_r, Data<String> Y_v) {
        super(X_v, bold_s, bold_s_prime, bold_r, Y_v);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private SecretData(SecretData secretData) {
        this(secretData.get_X_v(), secretData.get_bold_s(), secretData.get_bold_s_prime(), secretData.get_bold_r(), secretData.get_Y_v());
    }

    public Data<String> get_X_v() {
        return this.getFirst();
    }

    public Data<IntVector> get_bold_s() {
        return this.getSecond();
    }

    public Data<Vector<WriteIn>> get_bold_s_prime() {
        return this.getThird();
    }

    public Data<Vector<BigInteger>> get_bold_r() {
        return this.getFourth();
    }

    public Data<String> get_Y_v() {
        return this.getFifth();
    }

    /**
     * Calling this method stores the given content of type {@link UVC2} into the election authority's secret data.
     *
     * @param content The given content
     */
    public void setContent(UVC2 content) {
        this.get_X_v().set(content.get_X());
        this.get_bold_s().set(content.get_bold_s());
        this.get_bold_s_prime().set(content.get_bold_s_prime());
    }

    /**
     * Calling this method stores the given content of type {@link UVC3} into the election authority's secret data.
     *
     * @param content The given content
     */
    public void setContent(UVC3 content) {
        this.get_Y_v().set(content.get_Y());
    }

}
