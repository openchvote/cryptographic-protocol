/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.writein.states;

import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Input;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.request.RCA1;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.input.UVC1;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.input.UVC4;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.simulation.votingclient.VotingClient;
import ch.openchvote.simulation.votingclient.writein.EventContext;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
@Notify(Status.Type.READY)
public final class S300 extends State<VotingClient, EventContext> {

    public S300(VotingClient votingClient, EventContext eventContext) {
        super(votingClient, eventContext);
        this.registerInputHandler(UVC1.class, this::handleUVC1);
        this.registerInputHandler(UVC4.class, this::handleUVC4);
    }

    private void handleUVC1(Input input) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();

        // get input content
        var UVC1 = this.party.getContent(UVC1.class, input);

        // update public data
        publicData.setContent(UVC1);

        // select event data
        var AD = publicData.get_AD().get();
        var v = publicData.get_v().get();

        // send RCA1 to administrator
        this.party.sendRequest(eventId, AD, new RCA1(v));

        // update state
        this.party.updateState(this.eventContext, S310.class);
    }

    @SuppressWarnings("unused")
    private void handleUVC4(Input input) {

        // update state
        this.party.updateState(this.eventContext, FINAL.class);
    }

}
