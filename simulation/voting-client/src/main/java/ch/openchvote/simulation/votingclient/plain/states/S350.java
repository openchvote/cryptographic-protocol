/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.plain.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Input;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.protocols.plain.content.requestresponse.request.RCE3;
import ch.openchvote.core.protocol.protocols.plain.content.userinterface.input.UVC3;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.simulation.votingclient.VotingClient;
import ch.openchvote.simulation.votingclient.plain.EventContext;
import ch.openchvote.simulation.votingclient.plain.states.error.A5;
import ch.openchvote.simulation.votingclient.plain.tasks.TC5;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
public final class S350 extends State<VotingClient, EventContext> {

    public S350(VotingClient votingClient, EventContext eventContext) {
        super(votingClient, eventContext);
        this.registerInputHandler(UVC3.class, this::handleUVC3);
    }

    private void handleUVC3(Input input) {

        // get algorithm service from voting client
        var algorithmService = this.party.getAlgorithmService();

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SP = ES.get_SP();
        var bold_ea = SP.get_bold_ea();

        // get input content
        var UVC3 = this.party.getContent(UVC3.class, input);

        // update secret data
        secretData.setContent(UVC3);

        try {
            // perform task
            TC5.run(publicData, secretData, algorithmService);

            // select event data
            var v = publicData.get_v().get();
            var gamma = publicData.get_gamma().get();

            // send RCE3 to election authorities
            this.party.sendRequest(eventId, bold_ea, new RCE3(v, gamma));

            // update state
            this.party.updateState(this.eventContext, S360.class);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A5.class);
        }
    }

}
