/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetPointMatrix;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetVerificationCodes;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.core.protocol.protocols.writein.UsabilityParameters;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.simulation.votingclient.writein.PublicData;
import ch.openchvote.simulation.votingclient.writein.SecretData;

@SuppressWarnings("MissingJavadoc")
public final class TC4 {

    static public boolean isReady(PublicData publicData) {
        var s = publicData.get_ES().get().get_SP().get_s();
        return publicData.get_bold_beta().arePresent(IntSet.range(1, s));
    }

    static public void
    run(PublicData publicData, SecretData secretData, AlgorithmService algorithmService) {

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();
        var UC = ES.get_UC();

        // get security and usability parameters
        var securityParameters = new SecurityParameters(SL);
        var usabilityParameters = new UsabilityParameters(UC, SL);

        // select event data
        var bold_n = publicData.get_VP_v().get().get_bold_n();
        var bold_s = secretData.get_bold_s().get();
        var bold_r = secretData.get_bold_r().get();

        // perform task
        var bold_beta = publicData.get_bold_beta().mapTo(Vector::of);
        var bold_P = algorithmService.run(GetPointMatrix.class, securityParameters, bold_n, bold_beta, bold_s, bold_r);
        var bold_vc = algorithmService.run(GetVerificationCodes.class, securityParameters, usabilityParameters, bold_s, bold_P);

        // update event data
        publicData.get_bold_P().set(bold_P);
        publicData.get_bold_vc().set(bold_vc);
    }

}
