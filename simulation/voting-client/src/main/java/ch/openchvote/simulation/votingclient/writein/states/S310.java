/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.writein.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Response;
import ch.openchvote.base.framework.exceptions.CommunicationException;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.base.framework.protocol.Protocol;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response.RAC1;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.output.UCV1;
import ch.openchvote.simulation.votingclient.VotingClient;
import ch.openchvote.simulation.votingclient.writein.EventContext;
import ch.openchvote.simulation.votingclient.writein.states.error.A1;
import ch.openchvote.simulation.votingclient.writein.states.error.B1;
import ch.openchvote.simulation.votingclient.writein.tasks.TC1;

import static ch.openchvote.base.framework.exceptions.CommunicationException.Type.INCONSISTENT_EVENT_SETUP;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
public final class S310 extends State<VotingClient, EventContext> {

    public S310(VotingClient votingClient, EventContext eventContext) {
        super(votingClient, eventContext);
        this.registerResponseHandler(RAC1.class, this::handleRAC1);
    }

    private void handleRAC1(Response response) {

        // get algorithm service from voting client
        var algorithmService = this.party.getAlgorithmService();

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var protocolId = this.eventContext.getProtocolId();
        var protocolName = Protocol.getPrintName(protocolId);
        var securityLevel = this.eventContext.getSecurityLevel();

        // get and check response content (no auxiliary data)
        var RAC1 = this.party.getAndCheckContent(RAC1.class, response, null, securityLevel);

        // check election setup consistency
        if (!RAC1.get_ES().checkConsistency(protocolName, securityLevel) || !RAC1.get_VP().checkConsistency(eventId)) {
            throw new CommunicationException(INCONSISTENT_EVENT_SETUP, response);
        }

        // update public data
        publicData.setContent(RAC1);

        // select voting parameters and descriptions
        var VP_v = publicData.get_VP_v().get();
        var VD_v = publicData.get_VD_v().get();

        try {
            // perform task
            TC1.run(publicData, algorithmService);

            // display UCV1 to voter
            this.party.displayOutput(this.party.getVoter(), eventId, new UCV1(VP_v, VD_v));

            // update state
            this.party.updateState(this.eventContext, S320.class);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A1.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B1.class);
        }
    }

}
