/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.writein.states;

import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Input;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.request.RCE1;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.input.UVC2;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.simulation.votingclient.VotingClient;
import ch.openchvote.simulation.votingclient.writein.EventContext;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
public final class S320 extends State<VotingClient, EventContext> {

    public S320(VotingClient votingClient, EventContext eventContext) {
        super(votingClient, eventContext);
        this.registerInputHandler(UVC2.class, this::handleUVC2);
    }

    private void handleUVC2(Input input) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SP = ES.get_SP();
        var bold_ea = SP.get_bold_ea();

        // get input content
        var UVC2 = this.party.getContent(UVC2.class, input);

        // update secret data
        secretData.setContent(UVC2);

        // select event data
        var v = publicData.get_v().get();

        // send RCE1 to election authorities
        this.party.sendRequest(eventId, bold_ea, new RCE1(v));

        // update state
        this.party.updateState(this.eventContext, S330.class);
    }

}
