/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.CheckKeyPairProof;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.simulation.votingclient.writein.PublicData;

@SuppressWarnings("MissingJavadoc")
public final class TC1 {

    static public void
    run(PublicData publicData, AlgorithmService algorithmService) {

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select event data
        var pi_0 = publicData.get_bold_pi().get(0);
        var pk_0 = publicData.get_bold_pk().get(0);
        var bold_pk_prime_0 = publicData.get_bold_PK_prime().get(0);

        // perform task
        if (!algorithmService.run(CheckKeyPairProof.class, securityParameters, pi_0, pk_0, bold_pk_prime_0)) {
            throw new TaskException(TaskException.Type.INVALID_ZKP_PROOF, TC1.class);
        }
    }

}
