/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GenConfirmation;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.core.protocol.protocols.writein.UsabilityParameters;
import ch.openchvote.simulation.votingclient.writein.PublicData;
import ch.openchvote.simulation.votingclient.writein.SecretData;

@SuppressWarnings("MissingJavadoc")
public final class TC5 {

    static public void
    run(PublicData publicData, SecretData secretData, AlgorithmService algorithmService) {

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();
        var UC = ES.get_UC();

        // get security and usability parameters
        var securityParameters = new SecurityParameters(SL);
        var usabilityParameters = new UsabilityParameters(UC, SL);

        // select event data
        var Y_v = secretData.get_Y_v().get();
        var bold_P = publicData.get_bold_P().get();

        // perform task
        var gamma = algorithmService.run(GenConfirmation.class, securityParameters, usabilityParameters, Y_v, bold_P);

        // update event data
        publicData.get_gamma().set(gamma);
    }

}
