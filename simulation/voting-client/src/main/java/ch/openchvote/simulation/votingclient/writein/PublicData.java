/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.writein;

import ch.openchvote.core.algorithms.protocols.common.model.*;
import ch.openchvote.core.algorithms.protocols.writein.model.Ballot;
import ch.openchvote.core.algorithms.protocols.writein.model.KeyPairProof;
import ch.openchvote.core.algorithms.protocols.writein.model.VotingParametersWriteIn;
import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response.RAC1;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response.REC1;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response.REC2;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response.REC3;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.input.UVC1;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Tuple;
import ch.openchvote.base.utilities.tuples.decuple.SeptenDecuple;

import java.math.BigInteger;

/**
 * Instances of this class represent the voting client's public data. The class inherits from the generic {@link Tuple}
 * subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects to store.
 * Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class PublicData extends SeptenDecuple<
        // the administrator's id
        EventData.Data<String>, // AD
        // voter index
        EventData.Data<Integer>, // v
        // event definition
        EventData.Data<EventSetup>, // ES
        EventData.Data<VotingParametersWriteIn>, // VP_v
        EventData.Data<VotingDescriptions>, // VD_v
        // public keys
        EventData.DataMap<BigInteger>, // bold_pk
        EventData.Data<BigInteger>, // pk
        EventData.DataMap<Vector<BigInteger>>, // bold_PK_prime
        EventData.Data<Vector<BigInteger>>, // bold_pk_prime
        EventData.DataMap<KeyPairProof>, // bold_pi
        // ballot, response, participation code
        EventData.Data<Ballot>, // alpha
        EventData.DataMap<Response>, // bold_beta
        EventData.Data<Matrix<Point>>, // bold_P
        EventData.Data<Vector<String>>, // bold_vc
        // confirmation, finalization, participation code
        EventData.Data<Confirmation>, // gamma
        EventData.DataMap<Finalization>, // bold_delta
        EventData.Data<String>> // PC
        implements EventData {

    public PublicData() {
        this(new Data<>(), new Data<>(), new Data<>(), new Data<>(), new Data<>(), new DataMap<>(), new Data<>(), new DataMap<>(), new Data<>(), new DataMap<>(), new Data<>(), new DataMap<>(), new Data<>(), new Data<>(), new Data<>(), new DataMap<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private PublicData(Data<String> AD, Data<Integer> v, Data<EventSetup> ES, Data<VotingParametersWriteIn> VP_v, Data<VotingDescriptions> VD_v, DataMap<BigInteger> bold_pk, Data<BigInteger> pk, DataMap<Vector<BigInteger>> bold_PK_prime, Data<Vector<BigInteger>> bold_pk_prime, DataMap<KeyPairProof> bold_pi, Data<Ballot> alpha, DataMap<Response> bold_beta, Data<Matrix<Point>> bold_P, Data<Vector<String>> bold_vc, Data<Confirmation> gamma, DataMap<Finalization> bold_delta, Data<String> PC) {
        super(AD, v, ES, VP_v, VD_v, bold_pk, pk, bold_PK_prime, bold_pk_prime, bold_pi, alpha, bold_beta, bold_P, bold_vc, gamma, bold_delta, PC);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private PublicData(PublicData publicData) {
        this(publicData.get_AD(), publicData.get_v(), publicData.get_ES(), publicData.get_VP_v(), publicData.get_VD_v(), publicData.get_bold_pk(), publicData.get_pk(), publicData.get_bold_PK_prime(), publicData.get_bold_pk_prime(), publicData.get_bold_pi(), publicData.get_alpha(), publicData.get_bold_beta(), publicData.get_bold_P(), publicData.get_bold_vc(), publicData.get_gamma(), publicData.get_bold_delta(), publicData.get_PC());
    }

    public Data<String> get_AD() {
        return this.getFirst();
    }

    public Data<Integer> get_v() {
        return this.getSecond();
    }

    public Data<EventSetup> get_ES() {
        return this.getThird();
    }

    public Data<VotingParametersWriteIn> get_VP_v() {
        return this.getFourth();
    }

    public Data<VotingDescriptions> get_VD_v() {
        return this.getFifth();
    }

    public DataMap<BigInteger> get_bold_pk() {
        return this.getSixth();
    }

    public Data<BigInteger> get_pk() {
        return this.getSeventh();
    }

    public DataMap<Vector<BigInteger>> get_bold_PK_prime() {
        return this.getEighth();
    }

    public Data<Vector<BigInteger>> get_bold_pk_prime() {
        return this.getNinth();
    }

    public DataMap<KeyPairProof> get_bold_pi() {
        return this.getTenth();
    }

    public Data<Ballot> get_alpha() {
        return this.getEleventh();
    }

    public DataMap<Response> get_bold_beta() {
        return this.getTwelfth();
    }

    public Data<Matrix<Point>> get_bold_P() {
        return this.getThirteenth();
    }

    public Data<Vector<String>> get_bold_vc() {
        return this.getFourteenth();
    }

    public Data<Confirmation> get_gamma() {
        return this.getFifteenth();
    }

    public DataMap<Finalization> get_bold_delta() {
        return this.getSixteenth();
    }

    public Data<String> get_PC() {
        return this.getSeventeenth();
    }

    /**
     * Calling this method stores the given content of type {@link RAC1} into the voting client's public data.
     *
     * @param content The given content
     */
    public void setContent(RAC1 content) {
        this.get_ES().set(content.get_ES());
        this.get_VP_v().set(content.get_VP());
        this.get_VD_v().set(content.get_VD());
        this.get_bold_pk().set(0, content.get_pk());
        this.get_bold_PK_prime().set(0, content.get_bold_pk_prime());
        this.get_bold_pi().set(0, content.get_pi());
    }

    /**
     * Calling this method stores the given content of type {@link REC1} into the voting client's public data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, REC1 content) {
        this.get_bold_pk().set(index, content.get_pk());
        this.get_bold_PK_prime().set(index, content.get_bold_pk_prime());
        this.get_bold_pi().set(index, content.get_pi());
    }

    /**
     * Calling this method stores the given content of type {@link REC2} into the voting client's public data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, REC2 content) {
        this.get_bold_beta().set(index, content.get_beta());
    }

    /**
     * Calling this method stores the given content of type {@link REC3} into the voting client's public data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, REC3 content) {
        this.get_bold_delta().set(index, content.get_delta());
    }

    /**
     * Calling this method stores the given content of type {@link UVC1} into the voting client's public data.
     *
     * @param content The given content
     */
    public void setContent(UVC1 content) {
        this.get_AD().set(content.get_AD());
        this.get_v().set(content.get_v());
    }

}
