/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.base.framework.annotations.party.Role;
import ch.openchvote.base.framework.interfaces.UserInterface;
import ch.openchvote.base.framework.party.Party;
import ch.openchvote.base.framework.services.RequestResponseService;
import ch.openchvote.simulation.votingclient.plain.PublicData;

/**
 * This class implements the 'Voting Client' party of the CHVote protocol. It is a direct subclass of {@link Party} with
 * an additional field {@link VotingClient#voter} for linking the voting client to the voter. The specific role of the
 * voting client in the protocol is implemented in the classes {@link PublicData} (plain protocol) and
 * {@link ch.openchvote.simulation.votingclient.writein.PublicData} (write-in protocol) and in corresponding state and task
 * classes.
 */
@Role(ch.openchvote.core.protocol.roles.VotingClient.class)
public final class VotingClient extends Party {

    // algorithm service for preferred language
    private final AlgorithmService algorithmService;

    // the voter using the voting client
    private final UserInterface.User voter;

    /**
     * Constructs a new instance of this class.
     *
     * @param id          The id of this voting client
     * @param language    The voting client's programming language for executing algorithms
     * @param voter       The voter using the voting client
     */
    public VotingClient(String id, AlgorithmService.Language language, UserInterface.User voter) {
        super(id);
        this.algorithmService = AlgorithmService.load(language);
        this.voter = voter;
    }

    @Override
    public void subscribeToServices() {
        super.subscribeToServices();

        // subscribe to request/response service as requester
        this.getService(RequestResponseService.Source.class).subscribe(this);
    }

    @Override
    public void unsubscribeFromServices() {
        super.unsubscribeFromServices();

        // unsubscribe from request/response service as requester
        this.getService(RequestResponseService.Source.class).unsubscribe(this);
    }

    /**
     * Return the voting client's algorithm service.
     *
     * @return The voting client's algorithm service
     */
    public AlgorithmService getAlgorithmService() {
        return this.algorithmService;
    }

    /**
     * Return the voter using the voting client.
     *
     * @return The voter using the voting client.
     */
    public UserInterface.User getVoter() {
        return this.voter;
    }

}
