/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.votingclient.writein.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Response;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response.REC2;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.output.UCV2;
import ch.openchvote.base.utilities.tuples.Triple;
import ch.openchvote.simulation.votingclient.VotingClient;
import ch.openchvote.simulation.votingclient.writein.EventContext;
import ch.openchvote.simulation.votingclient.writein.states.error.A4;
import ch.openchvote.simulation.votingclient.writein.tasks.TC4;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
public final class S340 extends State<VotingClient, EventContext> {

    public S340(VotingClient votingClient, EventContext eventContext) {
        super(votingClient, eventContext);
        this.registerResponseHandler(REC2.class, this::handleREC2);
    }

    private void handleREC2(Response response) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var j = ES.get_SP().getIndexOf(response.getResponderId());

        // select voting parameters and descriptions
        var VP_v = publicData.get_VP_v().get();
        var VD_v = publicData.get_VD_v().get();

        // get and check response content
        var aux = new Triple<>(ES, VP_v, VD_v);
        var REC2 = this.party.getAndCheckContent(REC2.class, response, aux, securityLevel);

        // update public data
        publicData.setContent(j, REC2);

        // run self activation
        var stateId = this.getId();
        this.party.selfActivate(eventId, stateId, State::handleSelfActivation);
    }

    @SuppressWarnings("unused")
    @Override
    public void handleSelfActivation() {

        // get algorithm service from voting client
        var algorithmService = this.party.getAlgorithmService();

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();

        // check if all REC2 messages are available
        if (TC4.isReady(publicData)) {
            try {
                // perform task
                TC4.run(publicData, secretData, algorithmService);

                // select event data
                var bold_vc = publicData.get_bold_vc().get();

                // display UCV2 to voter
                this.party.displayOutput(this.party.getVoter(), eventId, new UCV2(bold_vc));

                // update state
                this.party.updateState(this.eventContext, S350.class);

            } catch (Algorithm.Exception exception) {
                // move to error state
                this.party.updateState(this.eventContext, A4.class);
            }
        }
    }

}
