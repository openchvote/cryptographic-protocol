# Voting Client

This module provides an implementation of the CHVote party *"Voting Client"*. In
the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325), the role of the voting client is described as
follows:

> The voting client [...] is a machine used by the voter to submit a vote during the election period [...]. Typically,
> this machine is either a desktop, notebook, or tablet computer with a network connection and enough computational
> power to perform cryptographic computations. The strict separation between the voter and the voting [...] client is an
> important
> precondition for the protocol’s security concept.

The main purpose of this module is to provide an exemplary implementation of the voting client in Java, which can be
used for simulating election events in a single JVM. Robust implementations of corresponding services can be connected
to this module by using Java's
`ServiceLoader.load()` functionality.

## Specification

### Phases and States

| Phase          | States                                   | Error States                   | Task                  |
|:---------------|:-----------------------------------------|:-------------------------------|:----------------------|
| Initialization | –                                        | –                              | –                     |
| Preparation    | –                                        | –                              | –                     |
| Election       | S300, S310, S320, S330, S340, S350, S360 | A1, B1, A2, B2, A3, A4, A5, A6 | Cast and confirm vote |
| Tallying       | –                                        | –                              | –                     |
| Inspection     | –                                        | –                              | –                     |
| Verification   | –                                        | –                              | –                     |
| Testing        | –                                        | –                              | –                     |
| [none]         | FINAL                                    | –                              | –                     |

### State Diagram

![StateDiagram](img/stateDiagram.svg)

### Communication

#### Services:

|                        | VotingClient |
|:-----------------------|:-------------|
| EventService           | Subscriber   |
| MailingService         | –            |
| MessagingService       | –            |
| PublicationService     | –            |
| RequestResponseService | Requester    |
| UserInterface          | Device       |
| TestingService         | –            |

#### Incoming Messages:

| State | Phase    | Protocol | Name | Type     | Sender             | Signed | Encrypted | Content                                          |
|:-----:|:---------|:--------:|:----:|:---------|:-------------------|:------:|:---------:|--------------------------------------------------|
| S300  | Election |   7.5    | UVC1 | Input    | Voter              |   no   |    no     | Administrator id, voter index                    |
| S300  | Election |   7.5    | UVC4 | Input    | Voter              |   no   |    no     | –                                                |
| S310  | Election |   7.5    | RAC1 | Response | Administrator      |  yes   |    no     | Event setup, voting parameters, public key share |
| S320  | Election |   7.5    | UVC2 | Input    | Voter              |   no   |    no     | Voting code, selection                           |
| S330  | Election |   7.6    | REC1 | Response | Election authority |  yes   |    no     | Public key share                                 |
| S340  | Election |   7.6    | REC2 | Response | Election authority |  yes   |    no     | Response                                         |
| S350  | Election |   7.7    | UVC3 | Input    | Voter              |   no   |    no     | Confirmation code                                |
| S360  | Election |   7.7    | REC3 | Response | Election authority |  yes   |    no     | Finalization                                     |

#### Outgoing Messages:

| State | Phase    | Protocol | Name | Type    | Receiver           | Signed | Encrypted | Content                   |
|:-----:|:---------|:--------:|:----:|:--------|:-------------------|:------:|:---------:|:--------------------------|
| S300  | Election |   7.5    | RCA1 | Request | Administrator      |   no   |    no     | Voter index               |
| S310  | Election |   7.5    | UCV1 | Output  | Voter              |   no   |    no     | Voting parameters         |
| S320  | Election |   7.6    | RCE1 | Request | Election authority |   no   |    no     | Voter index               |
| S330  | Election |   7.6    | RCE2 | Request | Election authority |   no   |    no     | Voter index, ballot       |
| S340  | Election |   7.7    | UCV2 | Output  | Voter              |   no   |    no     | Verification codes        |
| S350  | Election |   7.7    | RCE3 | Request | Election authority |   no   |    no     | Voter index, confirmation |
| S360  | Election |   7.7    | UCV3 | Output  | Voter              |   no   |    no     | Participation code        |

#### Coordination:

| Type           | State | Phase    | New State | New Phase |
|:---------------|:-----:|:---------|:---------:|:----------|
| onInitialize() |   –   | –        |   S300    | Election  |
| onStart()      | S300  | Election |   S300    | Election  |
| onTerminate()  | FINAL | [none]   |     –     | –         |

### Tasks

| Task | State | Phase    | Protocol | AlgorithmicException | TaskException |
|:----:|:-----:|:---------|:--------:|:--------------------:|:-------------:|
| TC1  | S310  | Election |   7.5    |          A1          |      B1       |
| TC2  | S330  | Election |   7.6    |          A2          |      B2       |
| TC3  | S331  | Election |   7.6    |          A3          |               |
| TC4  | S340  | Election |   7.6    |          A4          |               |
| TC5  | S350  | Election |   7.7    |          A5          |               |
| TC6  | S360  | Election |   7.7    |          A6          |               |

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of
this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts
is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as
follows:

```console
mvn clean install
-- a lot of output
```

## Maven Dependencies

The following dependency tree shows the Maven dependencies of this module.

```console
mvn dependency:tree -Dscope=compile -Dverbose=false
[INFO] --- dependency:3.6.0:tree (default-cli) @ voting-client ---
[INFO] io.gitlab.openchvote:voting-client:jar:2.4
[INFO] \- io.gitlab.openchvote:protocol:jar:2.4:compile
[INFO]    +- io.gitlab.openchvote:algorithms:jar:2.4:compile
[INFO]    |  \- io.gitlab.openchvote:utilities:jar:2.4:compile
[INFO]    |     \- com.verificatum:vmgj:jar:1.2.2:compile
[INFO]    \- io.gitlab.openchvote:framework:jar:2.4:compile
```

## Java Module Dependencies

The following module dependency diagram illustrates the Java module dependencies to other modules within this project:

![Java module dependencies](img/votingClient.svg)
