# Voter

This module provides an implementation of the CHVote party *"Voter"*. In
the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325), the role of the voter is described as follows:

> The voters are the actual human users of the system. They are numbered with indices *i* =1,...,*N_E*. Prior to an
> election event, they receive then election card from the printing authority, which they can use to cast and confirm a
> vote during the election period using their voting client.

The intention of this Maven module is to provide Java code that simulates the voter's behavior according to the
protocol. As this is useful mainly for testing purposes, this module will not be part of a real system implementation.
In a real implementation, essentially the same tasks are executed by real voters.

## Specification

### Phases and States

| Phase          | States                             | Error States       | Task                            |
|:---------------|:-----------------------------------|:-------------------|:--------------------------------|
| Initialization | –                                  | –                  | –                               |
| Preparation    | S200                               | –                  | Receive election card           |
| Election       | S300, S310, S320, S330, S340, S350 | A1, A2, B2, A3, B3 | Cast and confirm vote           |
| Tallying       | –                                  | –                  | –                               |
| Verification   | –                                  | –                  | –                               |
| Inspection     | S600, S610                         | A4, B4             | Inspect vote cast or abstention |
| Testing        | S700                               | –                  | Send test votes                 |
| [none]         | FINAL                              | –                  | –                               |

### State Diagram

![stateDiagram](img/stateDiagram.svg)

### Communication

#### Services:

|                        | Voter      |
|:-----------------------|:-----------|
| EventService           | Subscriber |
| MailingService         | Subscriber |
| MessagingService       | –          |
| PublicationService     | –          |
| RequestResponseService | –          |
| UserInterface          | User       |
| TestingService         | Provider   |

#### Incoming Messages:

| State | Phase       | Protocol | Name | Type   | Sender             | Signed | Encrypted | Content            |
|:-----:|:------------|:--------:|:----:|:-------|:-------------------|:------:|:---------:|:-------------------|
| S200  | Preparation |   7.4    | MPV1 | Mail   | Printing authority |   no   |    no     | Election card      |
| S310  | Election    |   7.5    | UCV1 | Output | Voting client      |   no   |    no     | Voting parameters  |
| S330  | Election    |   7.7    | UCV2 | Output | Voting client      |   no   |    no     | Verification codes |
| S350  | Election    |   7.7    | UCV3 | Output | Voting client      |   no   |    no     | Participation code |
| S610  | Inspection  |   7.12   | UIV1 | Output | Inspection client  |   no   |    no     | Inspection code    |

#### Outgoing Messages:

| State | Phase      | Protocol | Name | Type  | Receiver          | Signed | Encrypted | Content                       |
|:-----:|:-----------|:--------:|:----:|:------|:------------------|:------:|:---------:|:------------------------------|
| S300  | Election   |   7.5    | UVC1 | Input | Voting client     |   no   |    no     | Administrator id, voter index |
| S300  | Election   |   7.5    | UVC4 | Input | Voting client     |   no   |    no     | –                             |
| S320  | Election   |   7.5    | UVC2 | Input | Voting client     |   no   |    no     | Voting code, selection        |
| S340  | Election   |   7.7    | UVC3 | Input | Voting client     |   no   |    no     | Confirmation code             |
| S600  | Inspection |   7.12   | UVI1 | Input | Inspection client |   no   |    no     | Administrator id, voter index |

#### Coordination:

| Type           | State | Phase       | New State | New Phase           |
|:---------------|:-----:|:------------|:---------:|:--------------------|
| onInitialize() |   –   | –           |   S200    | Preparation         |
| onStart()      | S200  | Preparation |   S200    | Preparation         | 
| onStart()      | S300  | Election    | S310/S600 | Election/Inspection | 
| onStart()      | S600  | Inspection  |   S610    | Inspection          | 
| onStart()      | S700  | Testing     |   FINAL   | [none]              |
| onTerminate()  | FINAL | [none]      |     –     | –                   |

### Tasks

| Task | State | Phase      | Protocol | AlgorithmicException | TaskException |
|:----:|:-----:|:-----------|:--------:|:--------------------:|:-------------:|
| TV1  | S320  | Election   |   7.5    |          A1          |       –       |
| TV2  | S340  | Election   |   7.7    |          A2          |      B2       |
| TV3  | S350  | Election   |   7.7    |          A3          |      B3       |
| TV4  | S610  | Inspection |   7.12   |          A4          |      B4       |

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of
this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts
is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as
follows:

```console
mvn clean install
-- a lot of output
```

## Maven Dependencies

The following dependency tree shows the Maven dependencies of this module.

```console
mvn dependency:tree -Dscope=compile -Dverbose=false
[INFO] --- dependency:3.6.0:tree (default-cli) @ voter ---
[INFO] io.gitlab.openchvote:voter:jar:2.4
[INFO] +- io.gitlab.openchvote:voting-client:jar:2.4:compile
[INFO] |  \- io.gitlab.openchvote:protocol:jar:2.4:compile
[INFO] |     +- io.gitlab.openchvote:algorithms:jar:2.4:compile
[INFO] |     |  \- io.gitlab.openchvote:utilities:jar:2.4:compile
[INFO] |     |     \- com.verificatum:vmgj:jar:1.2.2:compile
[INFO] |     \- io.gitlab.openchvote:framework:jar:2.4:compile
[INFO] \- io.gitlab.openchvote:inspection-client:jar:2.4:compile
```

## Java Module Dependencies

The following module dependency diagram illustrates the Java module dependencies to other modules within this project:

![Java module dependencies](img/voter.svg)