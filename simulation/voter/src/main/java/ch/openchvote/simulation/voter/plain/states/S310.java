/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.plain.states;

import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Output;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.protocols.plain.content.userinterface.output.UCV1;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.simulation.voter.Voter;
import ch.openchvote.simulation.voter.plain.EventContext;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
public final class S310 extends State<Voter, EventContext> {

    public S310(Voter voter, EventContext eventContext) {
        super(voter, eventContext);
        this.registerOutputHandler(UCV1.class, this::handleUCV1);
    }

    private void handleUCV1(Output output) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();

        // get output content
        var UCV1 = this.party.getContent(UCV1.class, output);

        // update public data
        publicData.setContent(UCV1);

        // update state
        this.party.updateState(this.eventContext, S320.class);

        // run self activation
        var stateId = this.getId();
        this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

    }

}
