/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.plain.states;

import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Testing;
import ch.openchvote.core.protocol.protocols.plain.content.testdata.TVT1;
import ch.openchvote.core.protocol.protocols.plain.content.testdata.TVT2;
import ch.openchvote.simulation.voter.Voter;
import ch.openchvote.simulation.voter.plain.EventContext;

@SuppressWarnings("MissingJavadoc")
@Phase(Testing.class)
@Notify(Status.Type.READY)
public final class S700 extends State<Voter, EventContext> {

    public S700(Voter voter, EventContext eventContext) {
        super(voter, eventContext);
    }

    @SuppressWarnings("unused")
    @Override
    public void handleStart() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var secretData = this.eventContext.getSecretData();

        // select event data
        var pb = secretData.get_pb().get();
        var EC_v = secretData.get_EC_v().get();
        var v = EC_v.get_v();

        // send test data
        if (pb == 1) {
            var bold_s = secretData.get_bold_s().get();
            this.party.sendTestData(eventId, new TVT1(v, bold_s));
        } else {
            this.party.sendTestData(eventId, new TVT2(v));
        }

        // update state
        this.party.updateState(this.eventContext, FINAL.class);

    }

}
