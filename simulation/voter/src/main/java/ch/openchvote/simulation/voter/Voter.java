/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.base.framework.annotations.party.Role;
import ch.openchvote.base.framework.party.Party;
import ch.openchvote.base.framework.services.MailingService;
import ch.openchvote.simulation.inspectionclient.InspectionClient;
import ch.openchvote.simulation.voter.plain.PublicData;
import ch.openchvote.simulation.votingclient.VotingClient;

/**
 * This class implements the 'Voter' party of the CHVote protocol. It is a direct subclass of {@link Party} with three
 * added fields of type {@link VotingClient}, {@link InspectionClient}, and {@link VotingStrategy}. The specific role of
 * the voter in the protocol is implemented in the classes {@link PublicData} (plain protocol)
 * and {@link ch.openchvote.simulation.voter.writein.PublicData} (write-in protocol) and in corresponding state and task classes.
 */
@Role(ch.openchvote.core.protocol.roles.Voter.class)
public final class Voter extends Party {

    // the voter's voting voting client
    private final VotingClient votingClient;
    // the voter's inspection client
    private final InspectionClient inspectionClient;
    // the voter's voting strategy
    private final VotingStrategy votingStrategy;

    /**
     * Constructs a new instance of this class.
     *
     * @param id             The id of this voter
     * @param votingStrategy The voter's voting strategy
     * @param language       The programming language for executing algorithms on the voter's devices
     */
    public Voter(String id, VotingStrategy votingStrategy, AlgorithmService.Language language) {
        super(id);
        this.votingStrategy = votingStrategy;

        // create voting and inspection clients
        var votingClientId = VotingClient.class.getSimpleName() + "<" + id + ">";
        var inspectionClientId = InspectionClient.class.getSimpleName() + "<" + id + ">";
        this.votingClient = new VotingClient(votingClientId, language, this);
        this.inspectionClient = new InspectionClient(inspectionClientId, language, this);
    }

    @Override
    public void subscribeToServices() {
        super.subscribeToServices();

        // subscribe to mailing service
        this.getService(MailingService.Target.class).subscribe(this);
    }

    @Override
    public void unsubscribeFromServices() {
        super.unsubscribeFromServices();

        // unsubscribe from mailing service
        this.getService(MailingService.Target.class).unsubscribe(this);
    }

    /**
     * Returns the voter's voting client.
     *
     * @return The voter's voting client
     */
    public VotingClient getVotingClient() {
        return this.votingClient;
    }

    /**
     * Returns the voter's inspection client.
     *
     * @return The voter's inspection client
     */
    public InspectionClient getInspectionClient() {
        return this.inspectionClient;
    }

    /**
     * Return the voter's voting strategy.
     *
     * @return The voter's voting strategy
     */
    public VotingStrategy getVotingStrategy() {
        return this.votingStrategy;
    }

}
