/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.plain.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.protocols.plain.content.userinterface.input.UVC3;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.simulation.voter.plain.states.error.A2;
import ch.openchvote.simulation.voter.plain.tasks.TV2;
import ch.openchvote.simulation.voter.Voter;
import ch.openchvote.simulation.voter.plain.EventContext;
import ch.openchvote.simulation.voter.plain.states.error.B2;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
public final class S340 extends State<Voter, EventContext> {

    public S340(Voter voter, EventContext eventContext) {
        super(voter, eventContext);
    }

    @SuppressWarnings("unused")
    @Override
    public void handleSelfActivation() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();

        try {
            // perform task
            TV2.run(publicData, secretData);

            // select event data
            var EC_v = secretData.get_EC_v().get();
            var Y_v = EC_v.get_Y();

            // enter UVC3 to voting client
            this.party.enterInput(this.party.getVotingClient(), eventId, new UVC3(Y_v));

            // update state
            this.party.updateState(this.eventContext, S350.class);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A2.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B2.class);
        }
    }

}
