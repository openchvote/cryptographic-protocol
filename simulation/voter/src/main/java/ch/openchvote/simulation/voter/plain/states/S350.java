/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.plain.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Output;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.protocols.plain.content.userinterface.output.UCV3;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.simulation.voter.plain.states.error.A3;
import ch.openchvote.simulation.voter.plain.states.error.B3;
import ch.openchvote.simulation.voter.plain.tasks.TV3;
import ch.openchvote.simulation.voter.Voter;
import ch.openchvote.simulation.voter.plain.EventContext;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
public final class S350 extends State<Voter, EventContext> {

    public S350(Voter voter, EventContext eventContext) {
        super(voter, eventContext);
        this.registerOutputHandler(UCV3.class, this::handleUCV3);
    }

    private void handleUCV3(Output output) {

        // decompose event context
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();

        // get output content
        var UCV3 = this.party.getContent(UCV3.class, output);

        // update public data
        publicData.setContent(UCV3);

        try {
            // perform task
            TV3.run(publicData, secretData);

            // update state
            this.party.updateState(this.eventContext, S360.class);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A3.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B3.class);
        }
    }

}
