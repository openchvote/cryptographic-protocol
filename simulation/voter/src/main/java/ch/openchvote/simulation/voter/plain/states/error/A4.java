/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.plain.states.error;

import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Inspection;
import ch.openchvote.simulation.voter.Voter;
import ch.openchvote.simulation.voter.plain.EventContext;

@SuppressWarnings("MissingJavadoc")
@Phase(Inspection.class)
@Notify(Status.Type.ERROR)
public final class A4 extends State<Voter, EventContext> {

    public A4(Voter voter, EventContext eventContext) {
        super(voter, eventContext);
    }

}
