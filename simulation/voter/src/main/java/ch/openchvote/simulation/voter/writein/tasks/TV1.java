/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.writein.tasks;

import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.simulation.voter.writein.SecretData;

@SuppressWarnings("MissingJavadoc")
public final class TV1 {

    static public String
    run(SecretData secretData) {

        // select event data
        var EC_v = secretData.get_EC_v().get();
        var S = secretData.get_S().get();

        // perform task
        var builder_bold_s = new IntVector.Builder(S.getSize().intValue());
        S.forEach(builder_bold_s::add);
        var bold_s = builder_bold_s.build().sort();
        var X_v = EC_v.get_X();

        // update event data
        secretData.get_bold_s().set(bold_s);

        // return data
        return X_v;
    }

}
