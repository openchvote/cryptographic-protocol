/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.writein.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Output;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Inspection;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.output.UIV1;
import ch.openchvote.simulation.voter.writein.tasks.TV4;
import ch.openchvote.simulation.voter.Voter;
import ch.openchvote.simulation.voter.writein.EventContext;
import ch.openchvote.simulation.voter.writein.states.error.A4;
import ch.openchvote.simulation.voter.writein.states.error.B4;

@SuppressWarnings("MissingJavadoc")
@Phase(Inspection.class)
public final class S610 extends State<Voter, EventContext> {

    public S610(Voter voter, EventContext eventContext) {
        super(voter, eventContext);
        this.registerOutputHandler(UIV1.class, this::handleUIV1);
    }

    private void handleUIV1(Output output) {

        // decompose event context
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();

        // get output content
        var UIV1 = this.party.getContent(UIV1.class, output);

        // update public data
        publicData.setContent(UIV1);

        try {
            // perform task
            TV4.run(publicData, secretData);

            // update state
            this.party.updateState(this.eventContext, S620.class);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A4.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B4.class);
        }
    }

}
