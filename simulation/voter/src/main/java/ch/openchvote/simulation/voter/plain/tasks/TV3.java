/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.plain.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.CheckParticipationCode;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.simulation.voter.plain.PublicData;
import ch.openchvote.simulation.voter.plain.SecretData;

@SuppressWarnings("MissingJavadoc")
public final class TV3 {

    static public void
    run(PublicData publicData, SecretData secretData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select event data
        var PC = publicData.get_PC().get();
        var EC_v = secretData.get_EC_v().get();

        // perform task
        var PC_v = EC_v.get_PC();
        if (!algorithmService.run(CheckParticipationCode.class, PC_v, PC)) {
            throw new TaskException(TaskException.Type.PARTICIPATION_CODE_MISMATCH, TV3.class);
        }
    }

}
