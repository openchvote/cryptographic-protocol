/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.writein;

import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionCard;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteIn;
import ch.openchvote.core.protocol.protocols.writein.content.mail.MPV1;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Quintuple;
import ch.openchvote.base.utilities.tuples.Tuple;

/**
 * Instances of this class represent the voter's secret data. The class inherits from the generic {@link Tuple}
 * subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects to store.
 * Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class SecretData extends Quintuple<
        // participation bit
        EventData.Data<Integer>, // pb
        // selections
        EventData.Data<IntSet>, // S
        EventData.Data<IntVector>, // bold_s
        EventData.Data<Vector<WriteIn>>, // bold_s_prime
        // election card
        EventData.Data<ElectionCard>> // EC_v
        implements EventData {

    public SecretData() {
        this(new Data<>(), new Data<>(), new Data<>(), new Data<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private SecretData(Data<Integer> pb, Data<IntSet> S, Data<IntVector> bold_s, Data<Vector<WriteIn>> bold_s_prime, Data<ElectionCard> EC_v) {
        super(pb, S, bold_s, bold_s_prime, EC_v);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private SecretData(SecretData secretData) {
        this(secretData.get_pb(), secretData.get_S(), secretData.get_bold_s(), secretData.get_bold_s_prime(), secretData.get_EC_v());
    }

    public Data<Integer> get_pb() {
        return this.getFirst();
    }

    public Data<IntSet> get_S() {
        return this.getSecond();
    }

    public Data<IntVector> get_bold_s() {
        return this.getThird();
    }

    public Data<Vector<WriteIn>> get_bold_s_prime() {
        return this.getFourth();
    }

    public Data<ElectionCard> get_EC_v() {
        return this.getFifth();
    }

    /**
     * Calling this method stores the given content of type {@link MPV1} into the voter's secret data.
     *
     * @param content The given content
     */
    public void setContent(MPV1 content) {
        this.get_EC_v().set(content.get_EC());
    }

}
