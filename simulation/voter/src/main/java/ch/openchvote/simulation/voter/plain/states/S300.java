/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.plain.states;

import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.protocols.plain.content.userinterface.input.UVC1;
import ch.openchvote.core.protocol.protocols.plain.content.userinterface.input.UVC4;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.simulation.voter.Voter;
import ch.openchvote.simulation.voter.plain.EventContext;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
@Notify(Status.Type.READY)
public final class S300 extends State<Voter, EventContext> {

    public S300(Voter voter, EventContext eventContext) {
        super(voter, eventContext);
    }

    @SuppressWarnings("unused")
    @Override
    public void handleStart() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var secretData = this.eventContext.getSecretData();

        // select event data
        var EC_v = secretData.get_EC_v().get();
        var AD = EC_v.get_AD();
        var v = EC_v.get_v();

        // define participation bit
        var pb = this.party.getVotingStrategy().getParticipationBit();
        secretData.get_pb().set(pb);

        if (pb == 1) { // vote

            // enter UVC1 to voting client
            this.party.enterInput(this.party.getVotingClient(), eventId, new UVC1(AD, v));

            // update state
            this.party.updateState(this.eventContext, S310.class);

        } else { // abstain

            // enter UVC4 to voting client
            this.party.enterInput(this.party.getVotingClient(), eventId, new UVC4());

            // update state
            this.party.updateState(this.eventContext, S360.class);

        }
    }

}

