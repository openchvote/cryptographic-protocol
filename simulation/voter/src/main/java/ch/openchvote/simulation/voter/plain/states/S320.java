/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.plain.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.core.protocol.protocols.plain.content.userinterface.input.UVC2;
import ch.openchvote.simulation.voter.plain.states.error.A1;
import ch.openchvote.simulation.voter.plain.tasks.TV1;
import ch.openchvote.simulation.voter.Voter;
import ch.openchvote.simulation.voter.plain.EventContext;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
public final class S320 extends State<Voter, EventContext> {

    public S320(Voter voter, EventContext eventContext) {
        super(voter, eventContext);
    }

    @SuppressWarnings("unused")
    @Override
    public void handleSelfActivation() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();

        // select voting parameters
        var VP_v = publicData.get_VP_v().get();

        // define selections
        var votingStrategy = this.party.getVotingStrategy();
        var S = votingStrategy.getSelection(VP_v);

        // update event data
        secretData.get_S().set(S);

        try {
            // perform task
            var X_v = TV1.run(secretData);

            // select event data
            var bold_s = secretData.get_bold_s().get();

            // enter UVC2 to voting client
            this.party.enterInput(this.party.getVotingClient(), eventId, new UVC2(X_v, bold_s));

            // update state
            this.party.updateState(this.eventContext, S330.class);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A1.class);
        }
    }

}
