/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter;

import ch.openchvote.core.algorithms.protocols.plain.model.VotingParametersPlain;
import ch.openchvote.core.algorithms.protocols.writein.model.VotingParametersWriteIn;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteIn;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;

/**
 * Instances of this class can be used by a {@link Voter} to select the candidates in a simulated election event.
 */
public interface VotingStrategy {

    /**
     * Determines a random participation bit, which determines whether the voter will participate in an election event
     * or not.
     *
     * @return The random participation bit
     */
    int getParticipationBit();

    /**
     * Returns a set of candidates in a plain election event.
     *
     * @param votingParameters The voter's voting parameters
     * @return The generated set of candidates
     */
    IntSet getSelection(VotingParametersPlain votingParameters);

    /**
     * Returns a set of candidates in a write-in election event.
     *
     * @param votingParameters The voter's voting parameters
     * @return The generated set of candidates
     */
    IntSet getSelection(VotingParametersWriteIn votingParameters);

    /**
     * Generates a random vector of write-in candidates for a given selection of candidates.
     *
     * @param S                The given selection of candidates
     * @param votingParameters The voter's voting parameters
     * @return The generated vector of write-in candidates
     */
    Vector<WriteIn> getWriteIns(IntSet S, VotingParametersWriteIn votingParameters);

}
