/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.voter.writein;

import ch.openchvote.core.algorithms.protocols.common.model.VotingDescriptions;
import ch.openchvote.core.algorithms.protocols.writein.model.VotingParametersWriteIn;
import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.output.UCV1;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.output.UCV2;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.output.UCV3;
import ch.openchvote.core.protocol.protocols.writein.content.userinterface.output.UIV1;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Quintuple;
import ch.openchvote.base.utilities.tuples.Tuple;

/**
 * Instances of this class represent the voter's public data. The class inherits from the generic {@link Tuple}
 * subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects to store.
 * Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class PublicData extends Quintuple<
        // voting parameters and descriptions
        EventData.Data<VotingParametersWriteIn>, // VP_v
        EventData.Data<VotingDescriptions>, // VD_v
        // verification, participation, inspection codes
        EventData.Data<Vector<String>>, // bold_vc
        EventData.Data<String>, // PC
        EventData.Data<String>> // IC
        implements EventData {

    public PublicData() {
        this(new Data<>(), new Data<>(), new Data<>(), new Data<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private PublicData(Data<VotingParametersWriteIn> VP_v, Data<VotingDescriptions> VD_v, Data<Vector<String>> bold_vc, Data<String> PC, Data<String> IC) {
        super(VP_v, VD_v, bold_vc, PC, IC);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private PublicData(PublicData publicData) {
        this(publicData.get_VP_v(), publicData.get_VD_v(), publicData.get_bold_vc(), publicData.get_PC(), publicData.get_IC());
    }

    public Data<VotingParametersWriteIn> get_VP_v() {
        return this.getFirst();
    }

    private Data<VotingDescriptions> get_VD_v() {
        return this.getSecond();
    }

    public Data<Vector<String>> get_bold_vc() {
        return this.getThird();
    }

    public Data<String> get_PC() {
        return this.getFourth();
    }

    public Data<String> get_IC() {
        return this.getFifth();
    }

    /**
     * Calling this method stores the given content of type {@link UCV1} into the voter's public data.
     *
     * @param content The given content
     */
    public void setContent(UCV1 content) {
        this.get_VP_v().set(content.get_VP());
        this.get_VD_v().set(content.get_VD());
    }

    /**
     * Calling this method stores the given content of type {@link UCV2} into the voter's public data.
     *
     * @param content The given content
     */
    public void setContent(UCV2 content) {
        this.get_bold_vc().set(content.get_bold_vc());
    }

    /**
     * Calling this method stores the given content of type {@link UCV3} into the voter's public data.
     *
     * @param content The given content
     */
    public void setContent(UCV3 content) {
        this.get_PC().set(content.get_PC());
    }

    /**
     * Calling this method stores the given content of type {@link UIV1} into the voter's public data.
     *
     * @param content The given content
     */
    public void setContent(UIV1 content) {
        this.get_IC().set(content.get_IC());
    }

}
