/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import ch.openchvote.base.framework.services.*;
import ch.openchvote.simulation.services.*;

/**
 * This module implements several simple services for the OpenCHVote framework.
 */
module ch.openchvote.simulation.services {

    requires transitive ch.openchvote.base.framework;

    // only necessary for logger
    exports ch.openchvote.simulation.services;

    // simple logger
    provides System.LoggerFinder with SimpleLogger.Finder;

    // general simple services
    provides KeystoreService with SimpleKeystoreService;
    provides CertificateService with SimpleCertificateService;
    provides PersistenceService with SimplePersistenceService;

    // simple communication services
    provides EventService.Source with SimpleEventService;
    provides EventService.Target with SimpleEventService;
    provides MailingService.Source with SimpleMailingService;
    provides MailingService.Target with SimpleMailingService;
    provides MessagingService.Source with SimpleMessagingService;
    provides MessagingService.Target with SimpleMessagingService;
    provides PublicationService.Source with SimplePublicationService;
    provides PublicationService.Target with SimplePublicationService;
    provides RequestResponseService.Source with SimpleRequestResponseService;
    provides RequestResponseService.Target with SimpleRequestResponseService;
    provides TestingService.Source with SimpleTestingService;
    provides TestingService.Target with SimpleTestingService;
    provides SelfActivationService with SimpleSelfActivationService;

}
