/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.services;

import ch.openchvote.base.framework.communication.Request;
import ch.openchvote.base.framework.communication.Response;
import ch.openchvote.base.framework.exceptions.CommunicationException;
import ch.openchvote.base.framework.services.RequestResponseService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static ch.openchvote.base.framework.exceptions.CommunicationException.Type.UNKNOWN_REQUESTER;
import static ch.openchvote.base.framework.exceptions.CommunicationException.Type.UNKNOWN_RESPONDER;

/**
 * This is a simple implementation of both the {@link RequestResponseService.Source} and
 * {@link RequestResponseService.Target} interfaces. It allows subscribed requesters created in the same JVM to submit
 * requests to subscribed responders, which will reply with a response. Otherwise, instances of this class behave like a
 * 'real' request-response services, which transmit the requests and responses over some communication channel.
 * Multiple instances of this class share two common {@link HashSet} instances of all subscribed requesters and
 * responders, respectively. In this way, users of this class can create their own instances of this class. All
 * operations are thread-safe.
 */
public final class SimpleRequestResponseService extends SimulationService implements RequestResponseService.Source, RequestResponseService.Target {

    // hashmap for storing the subscribed requesters (using their id as key)
    static private final Map<String, RequestResponseService.Requester> REQUESTERS = new HashMap<>();

    // hashmap for storing the subscribed responders (using their id as key)
    static private final Map<String, RequestResponseService.Responder> RESPONDERS = new HashMap<>();

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public SimpleRequestResponseService() {
    }

    @Override
    public void request(Request request) {
        var responderId = request.getResponderId();
        synchronized (RESPONDERS) {
            var responder = RESPONDERS.get(responderId);
            if (responder == null) {
                throw new CommunicationException(UNKNOWN_RESPONDER, request);
            }
            this.execute(() -> responder.onRequest(request));
        }
    }

    @Override
    public void subscribe(RequestResponseService.Requester requester) {
        synchronized (REQUESTERS) {
            REQUESTERS.put(requester.getId(), requester);
        }
    }

    @Override
    public void unsubscribe(RequestResponseService.Requester requester) {
        synchronized (REQUESTERS) {
            REQUESTERS.remove(requester.getId());
        }
    }

    @Override
    public void respond(Response response) {
        var requesterId = response.getRequesterId();
        synchronized (REQUESTERS) {
            var requester = REQUESTERS.get(requesterId);
            if (requester == null) {
                throw new CommunicationException(UNKNOWN_REQUESTER, response);
            }
            this.execute(() -> requester.onResponse(response));
        }
    }

    @Override
    public void subscribe(RequestResponseService.Responder responder) {
        synchronized (RESPONDERS) {
            RESPONDERS.put(responder.getId(), responder);
        }
    }

    @Override
    public void unsubscribe(RequestResponseService.Responder responder) {
        synchronized (RESPONDERS) {
            RESPONDERS.remove(responder.getId());
        }
    }

}
