/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.services;

import ch.openchvote.base.framework.communication.Mail;
import ch.openchvote.base.framework.exceptions.CommunicationException;
import ch.openchvote.base.framework.services.MailingService;

import java.util.HashMap;
import java.util.Map;

import static ch.openchvote.base.framework.exceptions.CommunicationException.Type.UNKNOWN_RECEIVER;

/**
 * This is a simple implementation of both the {@link MailingService.Source} and {@link MailingService.Target}
 * interfaces. It allows subscribers created in the same JVM to receive mails from a sender. Otherwise, instances
 * of this class behave like a 'real' mailing services, which transmit mails over some communication channel. Multiple
 * instances of this class share a common {@link HashMap} of all subscribers. In this way, users of this class can
 * create their own instances. All operations are thread-safe.
 */
public final class SimpleMailingService extends SimulationService implements MailingService.Source, MailingService.Target {

    // hashmap for storing the mailboxes of different receivers (using their id as key)
    static private final Map<String, MailingService.Subscriber> SUBSCRIBERS = new HashMap<>();

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public SimpleMailingService() {
    }

    @Override
    public void subscribe(MailingService.Subscriber subscriber) {
        synchronized (SUBSCRIBERS) {
            SUBSCRIBERS.put(subscriber.getId(), subscriber);
        }
    }

    @Override
    public void unsubscribe(MailingService.Subscriber subscriber) {
        synchronized (SUBSCRIBERS) {
            SUBSCRIBERS.remove(subscriber.getId());
        }
    }

    @Override
    public void deliver(Mail mail) {
        var receiverId = mail.getReceiverId();
        synchronized (SUBSCRIBERS) {
            var subscriber = SUBSCRIBERS.get(receiverId);
            if (subscriber == null) {
                throw new CommunicationException(UNKNOWN_RECEIVER, mail);
            }
            this.execute(() -> subscriber.onMail(mail));
        }
    }

}
