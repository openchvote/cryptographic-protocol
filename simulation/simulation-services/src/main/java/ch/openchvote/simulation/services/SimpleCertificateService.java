/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.services;

import ch.openchvote.base.framework.security.Certificate;
import ch.openchvote.base.framework.services.CertificateService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * This is a simple implementation of the {@link CertificateService} interface. The registered certificates are stored
 * in a common list which is available to all instances through a static variable. All operations are thread-safe.
 */
public final class SimpleCertificateService extends SimulationService implements CertificateService {

    // list of certificates issued by this certificate service
    static private final List<Certificate> CERTIFICATE_DIRECTORY = new ArrayList<>();

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public SimpleCertificateService() {
    }

    @Override
    public Certificate requestCertificate(String publicKey, String subject, Certificate.Type type, String algorithmName, String securityLevel) {
        var dateOfIssue = LocalDateTime.now();
        var certificate = Certificate.of(publicKey, subject, type, algorithmName, securityLevel, dateOfIssue);
        synchronized (CERTIFICATE_DIRECTORY) {
            CERTIFICATE_DIRECTORY.add(certificate);
        }
        return certificate;
    }

    @Override
    public Optional<Certificate> retrieveCertificate(String subject, Certificate.Type type, String algorithmName, String securityLevel) {
        synchronized (CERTIFICATE_DIRECTORY) {
            return CERTIFICATE_DIRECTORY.stream()
                    .filter(certificate -> certificate.getSubject().equals(subject))
                    .filter(certificate -> certificate.getType() == type)
                    .filter(certificate -> certificate.getAlgorithmName().equals(algorithmName))
                    .filter(certificate -> certificate.getSecurityLevel().equals(securityLevel))
                    .max(Comparator.comparing(Certificate::getDateOfIssue));
        }
    }

}