/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.services;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class is a simple implementation of the {@link System.Logger} interface, which allows printing the logger
 * messages to the standard output {@code System.out} or error output {@code System.err} using
 * {@link java.io.PrintStream#println}.
 */
@SuppressWarnings("ClassCanBeRecord")
public class SimpleLogger implements System.Logger {

    static private final Map<String, Level> LOGGER_LEVELS = new ConcurrentHashMap<>();
    static private final String LOGGER_CONFIG_FILE_NAME = "simplelogger.properties";
    static private final String DEFAULT_LOGGER_LEVEL_PROPERTY = "defaultLoggerLevel";
    static private final String DEFAULT_LOGGER_LEVEL_NAME = Level.INFO.getName();
    static private Level loggerLevel;

    // the logger's name
    private final String name;

    /**
     * Main constructor for this class. The provided {@link java.lang.System.Logger.Level} enum constant defines the
     * granularity of the displayed logging messages, i.e., only messages which {@link Level#getSeverity()} greater or
     * equal to the severity of the given {@link java.lang.System.Logger.Level} are displayed.
     *
     * @param name The given logger name
     */
    public SimpleLogger(String name) {
        this.name = name;
    }

    private Level getLoggerLevel() {
        if (LOGGER_LEVELS.containsKey(name)) {
            return LOGGER_LEVELS.get(name);
        }
        if (loggerLevel == null) {
            try (var inputStream = this.getClass().getResourceAsStream(LOGGER_CONFIG_FILE_NAME)) {
                var properties = new Properties();
                properties.load(inputStream);
                var property = properties.getProperty(DEFAULT_LOGGER_LEVEL_PROPERTY, DEFAULT_LOGGER_LEVEL_NAME);
                loggerLevel = Level.valueOf(property);
            } catch (IOException exception) {
                throw new RuntimeException("Configuration file not found: " + LOGGER_CONFIG_FILE_NAME);
            } catch (IllegalArgumentException exception) {
                throw new RuntimeException("Invalid logger level property: " + LOGGER_CONFIG_FILE_NAME);
            }
        }
        return loggerLevel;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean isLoggable(Level level) {
        return level.getSeverity() >= this.getLoggerLevel().getSeverity();
    }

    @Override
    public void log(Level level, ResourceBundle bundle, String message, Throwable thrown) {
        var numberOfSpaces = this.getMaxWidth() - level.getName().length() + 1;
        var formatString = "[%s]%" + numberOfSpaces + "s%s";
        if (this.isLoggable(level)) {
            var output = String.format(formatString, level, " ", message);
            switch (level) {
                case INFO, DEBUG, TRACE -> System.out.println(output);
                case WARNING, ERROR -> System.err.println(output);
            }
        }
    }

    @Override
    public void log(Level level, ResourceBundle bundle, String format, Object... params) {
        var message = MessageFormat.format(format, params);
        this.log(level, bundle, message, (Throwable) null);
    }

    @Override
    public void log(Level level, String message) {
        this.log(level, null, message, (Throwable) null);
    }

    /**
     * Sets the global logger level to the given value.
     *
     * @param level The given logger level
     */
    static public void setLoggerLevel(Level level) {
        loggerLevel = level;
    }

    /**
     * Sets the logger level for a logger with the given name to the given value.
     *
     * @param name  The given logger name
     * @param level The given logger level
     */
    public static void setLoggerLevel(String name, Level level) {
        LOGGER_LEVELS.put(name, level);
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    // private helper method to compute the maximal width of all logger levels
    private int getMaxWidth() {
        return Arrays.stream(Level.values())
                .filter(this::isLoggable)
                .mapToInt(level -> level.getName().length())
                .max()
                .getAsInt();
    }

    /**
     * This class is an implementation of the {@link System.LoggerFinder} interface. Its purpose is creating an instance
     * of the {@link SimpleLogger} class. It will be called via a call to {@link System.LoggerFinder#getLogger} method
     * from anywhere in an application not providing another logger implementation.
     */
    static public class Finder extends System.LoggerFinder {

        @Override
        public System.Logger getLogger(String name, Module module) {
            return new SimpleLogger(name);
        }

    }

}
