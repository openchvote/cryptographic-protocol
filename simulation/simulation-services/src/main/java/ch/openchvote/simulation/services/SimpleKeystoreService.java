/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.services;

import ch.openchvote.base.framework.security.KeyPair;
import ch.openchvote.base.framework.services.KeystoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This is a simple implementation of the {@link KeystoreService} interface. The key pairs are stored in a list. No
 * particular measures are taken for protecting the private keys. Instances of this class should therefore not be used
 * in real applications. All operations are thread-safe.
 */
public final class SimpleKeystoreService extends SimulationService implements KeystoreService {

    // an array list for storing the key pairs
    private final List<KeyPair> keyPairs = new ArrayList<>();

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public SimpleKeystoreService() {
    }

    @Override
    public synchronized void addEntry(KeyPair keyPair) {
        this.keyPairs.add(keyPair);
    }

    @Override
    public synchronized Optional<String> getPublicKey(String privateKey) {
        return this.keyPairs.stream().filter(k -> k.getPrivateKey().equals(privateKey)).map(KeyPair::getPublicKey).findFirst();
    }

    @Override
    public synchronized Optional<String> getPrivateKey(String publicKey) {
        return this.keyPairs.stream().filter(k -> k.getPublicKey().equals(publicKey)).map(KeyPair::getPrivateKey).findFirst();
    }

}
