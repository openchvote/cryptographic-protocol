/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.services;

import ch.openchvote.base.framework.communication.TestData;
import ch.openchvote.base.framework.services.TestingService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a simple implementation of both the {@link TestingService.Source} and {@link TestingService.Target}
 * interfaces. It allows subscribers created in the same JVM to receive test data from a test data provider. Otherwise,
 * instances of this class behave like a 'real' testing services, which transmit test data over some communication
 * channel. Multiple instances of this class share a common {@link HashMap} of all subscribers. In this way, users of
 * this class can create their own instances. All operations are thread-safe.
 */
public final class SimpleTestingService extends SimulationService implements TestingService.Source, TestingService.Target {

    // hashmap for storing the list of parties subscribed to a specific events id (using the event id as key)
    static private final Map<String, List<TestingService.Tester>> SUBSCRIBERS = new HashMap<>();

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public SimpleTestingService() {
    }

    @Override
    public void provide(TestData testData) {
        var eventId = testData.getEventId();
        synchronized (SUBSCRIBERS) {
            SUBSCRIBERS.getOrDefault(eventId, List.of()).forEach(subscriber -> this.execute(() -> subscriber.onTestData(testData)));
        }
    }

    @Override
    public void subscribe(TestingService.Tester subscriber, String eventId) {
        synchronized (SUBSCRIBERS) {
            SUBSCRIBERS.computeIfAbsent(eventId, k -> new ArrayList<>()).add(subscriber);
        }
    }

    @Override
    public void unsubscribe(TestingService.Tester subscriber, String eventId) {
        synchronized (SUBSCRIBERS) {
            SUBSCRIBERS.getOrDefault(eventId, List.of()).remove(subscriber);
        }
    }

}
