/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.services;

import ch.openchvote.base.framework.communication.Publication;
import ch.openchvote.base.framework.services.PublicationService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a simple implementation of both the {@link PublicationService.Source} and {@link PublicationService.Target}
 * interfaces. It allows subscribers created in the same JVM to receive publications from a sender. Otherwise, instances
 * of this class behave like a 'real' publication services, which transmit publications over some communication channel.
 * Multiple instances of this class share a common {@link ArrayList} and a {@link HashMap} of all subscribers. In this
 * way, users of this class can create their own instances. All operations are thread-safe.
 */
public final class SimplePublicationService extends SimulationService implements PublicationService.Source, PublicationService.Target {

    // hashmap for storing the list of parties subscribed to a specific event id (using the event id as key)
    static private final Map<String, List<PublicationService.Subscriber>> SUBSCRIBERS = new HashMap<>();
    // arraylist for storing the list of generally subscribed parties
    static private final List<PublicationService.Subscriber> GENERAL_SUBSCRIBERS = new ArrayList<>();

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public SimplePublicationService() {
    }

    @Override
    public void publish(Publication publication) {
        var eventId = publication.getEventId();
        var publisherId = publication.getPublisherId();
        synchronized (SUBSCRIBERS) {
            SUBSCRIBERS.getOrDefault(eventId, List.of()).stream()
                    .filter(subscriber -> !subscriber.getId().equals(publisherId)) // exclude sender
                    .forEach(subscriber -> this.execute(() -> subscriber.onPublication(publication)));
        }
        synchronized (GENERAL_SUBSCRIBERS) {
            GENERAL_SUBSCRIBERS.stream()
                    .filter(subscriber -> !subscriber.getId().equals(publisherId)) // exclude sender
                    .forEach(subscriber -> this.execute(() -> subscriber.onPublication(publication)));
        }
    }

    @Override
    public void subscribe(PublicationService.Subscriber subscriber) {
        synchronized (GENERAL_SUBSCRIBERS) {
            GENERAL_SUBSCRIBERS.add(subscriber);
        }
    }

    @Override
    public void subscribe(PublicationService.Subscriber subscriber, String eventId) {
        synchronized (SUBSCRIBERS) {
            SUBSCRIBERS.computeIfAbsent(eventId, k -> new ArrayList<>()).add(subscriber);
        }
    }

    @Override
    public void unsubscribe(PublicationService.Subscriber subscriber) {
        synchronized (GENERAL_SUBSCRIBERS) {
            GENERAL_SUBSCRIBERS.remove(subscriber);
        }
    }

    @Override
    public void unsubscribe(PublicationService.Subscriber subscriber, String eventId) {
        synchronized (SUBSCRIBERS) {
            SUBSCRIBERS.getOrDefault(eventId, List.of()).remove(subscriber);
        }
    }

}
