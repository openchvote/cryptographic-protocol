/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.services;

import ch.openchvote.base.framework.communication.Message;
import ch.openchvote.base.framework.exceptions.CommunicationException;
import ch.openchvote.base.framework.services.MessagingService;

import java.util.HashMap;
import java.util.Map;

import static ch.openchvote.base.framework.exceptions.CommunicationException.Type.UNKNOWN_RECEIVER;

/**
 * This is a simple implementation of the {@link MessagingService.Source} and {@link MessagingService.Target}
 * interfaces, which allows subscribers created in the same JVM to exchange messages over common memory. Otherwise,
 * instances of this class behave like a 'real' messaging service, which transmits the messages over a communication
 * channel. Multiple instances of this class share a common {@link HashMap} of all registered parties. In this way, parties
 * can create their own instances of this class. All operations are thread-safe.
 */
public final class SimpleMessagingService extends SimulationService implements MessagingService.Source, MessagingService.Target {

    // hashmap for storing the parties subscribed to the messaging service (using their id as key)
    static private final Map<String, MessagingService.Subscriber> SUBSCRIBERS = new HashMap<>();

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public SimpleMessagingService() {
    }

    @Override
    public void subscribe(MessagingService.Subscriber subscriber) {
        SUBSCRIBERS.put(subscriber.getId(), subscriber);
    }

    @Override
    public void unsubscribe(MessagingService.Subscriber subscriber) {
        SUBSCRIBERS.remove(subscriber.getId());
    }

    @Override
    public void send(Message message) {
        var receiverId = message.getReceiverId();
        synchronized (SUBSCRIBERS) {
            var subscriber = SUBSCRIBERS.get(receiverId);
            if (subscriber == null) {
                throw new CommunicationException(UNKNOWN_RECEIVER, message);
            }
            this.execute(() -> subscriber.onMessage(message));
        }
    }

}