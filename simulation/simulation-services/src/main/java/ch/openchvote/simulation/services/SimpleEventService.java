/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.services;

import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.services.EventService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This is a simple implementation of both the {@link EventService.Source} and {@link EventService.Target} interfaces.
 * It allows subscribers created in the same JVM to receive event messages from a coordinator and to reply with their
 * current status. Otherwise, instances of this class behave like a 'real' event services, which transmit the event
 * messages over some communication channel. Multiple instances of this class share a common {@link HashSet} and a
 * common {@link HashMap} of all subscribers and coordinators, respectively. All operations are thread-safe.
 */
public final class SimpleEventService extends SimulationService implements EventService.Source, EventService.Target {

    // hashmap for keeping track of the coordinator of a given event id
    static private final Map<String, EventService.Coordinator> COORDINATORS = new ConcurrentHashMap<>();

    // hashset for keeping track of all subscribers
    static private final Set<EventService.Subscriber> SUBSCRIBERS = new HashSet<>();

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public SimpleEventService() {
    }

    @Override
    public void initialize(EventService.Coordinator coordinator, String eventId, String protocolId, String securityLevel) {
        if (!COORDINATORS.containsKey(eventId)) {
            COORDINATORS.put(eventId, coordinator);
            synchronized (SUBSCRIBERS) {
                SUBSCRIBERS.forEach(subscriber -> this.execute(() -> subscriber.onInitialize(eventId, protocolId, securityLevel)));
            }
        }
    }

    @Override
    public void terminate(String eventId) {
        if (COORDINATORS.containsKey(eventId)) {
            synchronized (SUBSCRIBERS) {
                SUBSCRIBERS.forEach(subscriber -> this.execute(() -> subscriber.onTerminate(eventId)));
            }
            COORDINATORS.remove(eventId);
        }
    }

    @Override
    public void abort(String eventId) {
        if (COORDINATORS.containsKey(eventId)) {
            synchronized (SUBSCRIBERS) {
                SUBSCRIBERS.forEach(subscriber -> this.execute(() -> subscriber.onAbort(eventId)));
            }
        }
    }

    @Override
    public void start(String eventId, String phaseId) {
        if (COORDINATORS.containsKey(eventId)) {
            synchronized (SUBSCRIBERS) {
                SUBSCRIBERS.forEach(subscriber -> this.execute(() -> subscriber.onStart(eventId, phaseId)));
            }
        }
    }

    @Override
    public void stop(String eventId, String phaseId) {
        if (COORDINATORS.containsKey(eventId)) {
            synchronized (SUBSCRIBERS) {
                SUBSCRIBERS.forEach(subscriber -> this.execute(() -> subscriber.onStop(eventId, phaseId)));
            }
        }
    }

    @Override
    public void subscribe(EventService.Subscriber subscriber) {
        synchronized (SUBSCRIBERS) {
            SUBSCRIBERS.add(subscriber);
        }
    }

    @Override
    public void unsubscribe(EventService.Subscriber subscriber) {
        synchronized (SUBSCRIBERS) {
            SUBSCRIBERS.remove(subscriber);
        }
    }

    @Override
    public void notifyStatus(Status status) {
        var eventId = status.eventId();
        if (COORDINATORS.containsKey(eventId)) {
            var coordinator = COORDINATORS.get(eventId);
            this.execute(() -> coordinator.onStatus(status));
        }
    }

}
