/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.simulation.services;

import ch.openchvote.base.framework.annotations.service.Priority;
import ch.openchvote.base.framework.services.Service;

/**
 * Abstract base class for all simulation service classes in this package. Its main purpose is to provide a common
 * method {@link SimulationService#execute(Runnable)} to every service, which starts a new virtual thread for executing
 * a {@link Runnable} object. This is fundamental for creating an asynchronous simulation. Furthermore, the class is
 * annotated with the {@link Priority} value set to {@code -1}, which will usually make simple services the least
 * preferable choice for {@link java.util.ServiceLoader}.
 */
@Priority(-1)
public abstract class SimulationService implements Service {

    // method for executing a {@link Runnable} in a separate virtual thread
    protected void execute(Runnable runnable) {
        Thread.ofVirtual().start(runnable);
    }

}
