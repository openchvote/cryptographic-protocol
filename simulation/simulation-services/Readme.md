# Simple-Services

This module provides simple implementations of several infrastructure services to be used by the protocol parties. The
main intention behind these simple service implementations is to provide exemplary code for such components, which
can be used for simulations and for testing purposes in a single JVM. There are two types of such services, depending on
the number of parties involved.

## Overview of Services

### Services for individual parties

- <code>SimpleCertificateService</code>
- <code>SimpleKeystoreService</code>
- <code>SimpleLogger</code>
- <code>SimplePersistenceService</code>
- <code>SimpleSelfActivationService</code>code>

### Communication services between two or more parties

- <code>SimpleEventService</code>
- <code>SimpleMailingService</code>
- <code>SimpleMessagingService</code>
- <code>SimplePublicationService</code>
- <code>SimpleRequestResponseService</code>
- <code>SimpleTestingService</code>

These services are all fully functional and sufficiently efficient, but they do not provide the robustness and security
properties required in a real system.

## Installation of Maven Artefact

As this module depends on other modules of this project, these must be available in the local Maven repository. The POM
in the root of this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the
Maven artefacts is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as
follows:

```console
mvn clean install
-- a lot of output
```

## Maven Dependencies

The following dependency tree illustrates that this module has a single direct dependency
to `io.gitlab.openchvote:framework`, where corresponding service interfaces are defined:

```console
mvn dependency:tree -Dscope=compile -Dverbose=false
[INFO] --- dependency:3.6.0:tree (default-cli) @ simulation-services ---
[INFO] io.gitlab.openchvote:simulation-services:jar:2.4
[INFO] \- io.gitlab.openchvote:framework:jar:2.4:compile
```

## Java Module Dependencies

The following module dependency diagram illustrates the Java module dependencies to other modules within this project:

![Java module dependencies](img/services.svg)
