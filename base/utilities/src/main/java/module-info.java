/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
/**
 * This module provides a collection of utility classes for some mathematical and cryptographic primitives.
 */
module ch.openchvote.base.utilities {

    requires vmgj;

    exports ch.openchvote.base.utilities;
    exports ch.openchvote.base.utilities.algebra;
    exports ch.openchvote.base.utilities.crypto;
    exports ch.openchvote.base.utilities.sequence;
    exports ch.openchvote.base.utilities.serializer;
    exports ch.openchvote.base.utilities.set;
    exports ch.openchvote.base.utilities.tools;
    exports ch.openchvote.base.utilities.tuples;
    exports ch.openchvote.base.utilities.tuples.decuple;
    exports ch.openchvote.base.utilities.tuples.viguple;
}
