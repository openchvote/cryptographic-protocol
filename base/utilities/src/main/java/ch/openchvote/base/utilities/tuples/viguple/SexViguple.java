/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples.viguple;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.tuples.Tuple;

import java.util.stream.Stream;

import static ch.openchvote.base.utilities.UtilityException.Type.NULL_POINTER;

/**
 * This class implements a 26-tuple (sexviguple) of non-null values of different generic types.
 *
 * @param <V1>  The generic type of the first value
 * @param <V2>  The generic type of the second value
 * @param <V3>  The generic type of the third value
 * @param <V4>  The generic type of the fourth value
 * @param <V5>  The generic type of the fifth value
 * @param <V6>  The generic type of the sixth value
 * @param <V7>  The generic type of the seventh value
 * @param <V8>  The generic type of the eighth value
 * @param <V9>  The generic type of the ninth value
 * @param <V10> The generic type of the tenth value
 * @param <V11> The generic type of the eleventh value
 * @param <V12> The generic type of the twelfth value
 * @param <V13> The generic type of the thirteenth value
 * @param <V14> The generic type of the fourteenth value
 * @param <V15> The generic type of the fifteenth value
 * @param <V16> The generic type of the sixteenth value
 * @param <V17> The generic type of the seventeenth value
 * @param <V18> The generic type of the eighteenth value
 * @param <V19> The generic type of the nineteenth value
 * @param <V20> The generic type of the twentieth value
 * @param <V21> The generic type of the twenty-first value
 * @param <V22> The generic type of the twenty-second value
 * @param <V23> The generic type of the twenty-third value
 * @param <V24> The generic type of the twenty-fourth value
 * @param <V25> The generic type of the twenty-fifth value
 * @param <V26> The generic type of the twenty-sixth value
 */
@SuppressWarnings("unused")
public non-sealed class SexViguple<V1, V2, V3, V4, V5, V6, V7, V8, V9, V10, V11, V12, V13, V14, V15, V16, V17, V18, V19, V20, V21, V22, V23, V24, V25, V26> extends Tuple {

    private final V1 first;
    private final V2 second;
    private final V3 third;
    private final V4 fourth;
    private final V5 fifth;
    private final V6 sixth;
    private final V7 seventh;
    private final V8 eighth;
    private final V9 ninth;
    private final V10 tenth;
    private final V11 eleventh;
    private final V12 twelfth;
    private final V13 thirteenth;
    private final V14 fourteenth;
    private final V15 fifteenth;
    private final V16 sixteenth;
    private final V17 seventeenth;
    private final V18 eighteenth;
    private final V19 nineteenth;
    private final V20 twentieth;
    private final V21 twentyFirst;
    private final V22 twentySecond;
    private final V23 twentyThird;
    private final V24 twentyFourth;
    private final V25 twentyFifth;
    private final V26 twentySixth;

    /**
     * Constructs a new sexviguple for the given non-null values. An exception is thrown if one of the given values is
     * {@code null}.
     *
     * @param first        First value
     * @param second       Second value
     * @param third        Third value
     * @param fourth       Fourth value
     * @param fifth        Fifth value
     * @param sixth        Sixth value
     * @param seventh      Seventh value
     * @param eighth       Eighth value
     * @param ninth        Ninth value
     * @param tenth        Tenth value
     * @param eleventh     Eleventh value
     * @param twelfth      Twelfth value
     * @param thirteenth   Thirteenth value
     * @param fourteenth   Fourteenth value
     * @param fifteenth    Fifteenth value
     * @param sixteenth    Sixteenth value
     * @param seventeenth  Seventeenth value
     * @param eighteenth   Eighteenth value
     * @param nineteenth   Nineteenth value
     * @param twentieth    Twentieth value
     * @param twentyFirst  Twenty-first value
     * @param twentySecond Twenty-second value
     * @param twentyThird  Twenty-third value
     * @param twentyFourth Twenty-fourth value
     * @param twentyFifth  Twenty-fifth value
     * @param twentySixth  Twenty-sixth value
     */
    public SexViguple(V1 first, V2 second, V3 third, V4 fourth, V5 fifth, V6 sixth, V7 seventh, V8 eighth, V9 ninth, V10 tenth,
                      V11 eleventh, V12 twelfth, V13 thirteenth, V14 fourteenth, V15 fifteenth, V16 sixteenth, V17 seventeenth,
                      V18 eighteenth, V19 nineteenth, V20 twentieth, V21 twentyFirst, V22 twentySecond, V23 twentyThird,
                      V24 twentyFourth, V25 twentyFifth, V26 twentySixth) {
        if (first == null || second == null || third == null || fourth == null || fifth == null || sixth == null || seventh == null
                || eighth == null || ninth == null || tenth == null || eleventh == null || twelfth == null || thirteenth == null
                || fourteenth == null || fifteenth == null || sixteenth == null || seventeenth == null || eighteenth == null
                || nineteenth == null || twentieth == null || twentyFirst == null || twentySecond == null || twentyThird == null
                || twentyFourth == null || twentyFifth == null || twentySixth == null) {
            throw new UtilityException(NULL_POINTER, SexViguple.class, "Null value not allowed for tuples");
        }
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
        this.fifth = fifth;
        this.sixth = sixth;
        this.seventh = seventh;
        this.eighth = eighth;
        this.ninth = ninth;
        this.tenth = tenth;
        this.eleventh = eleventh;
        this.twelfth = twelfth;
        this.thirteenth = thirteenth;
        this.fourteenth = fourteenth;
        this.fifteenth = fifteenth;
        this.sixteenth = sixteenth;
        this.seventeenth = seventeenth;
        this.eighteenth = eighteenth;
        this.nineteenth = nineteenth;
        this.twentieth = twentieth;
        this.twentyFirst = twentyFirst;
        this.twentySecond = twentySecond;
        this.twentyThird = twentyThird;
        this.twentyFourth = twentyFourth;
        this.twentyFifth = twentyFifth;
        this.twentySixth = twentySixth;
    }

    /**
     * Returns the first element of the sexviguple.
     *
     * @return The first element
     */
    public final V1 getFirst() {
        return this.first;
    }

    /**
     * Returns the second element of the sexviguple.
     *
     * @return The second element
     */
    public final V2 getSecond() {
        return this.second;
    }

    /**
     * Returns the third element of the sexviguple.
     *
     * @return The third element
     */
    public final V3 getThird() {
        return this.third;
    }

    /**
     * Returns the fourth element of the sexviguple.
     *
     * @return The fourth element
     */
    public final V4 getFourth() {
        return this.fourth;
    }

    /**
     * Returns the fifth element of the sexviguple.
     *
     * @return The fifth element
     */
    public final V5 getFifth() {
        return this.fifth;
    }

    /**
     * Returns the sixth element of the sexviguple.
     *
     * @return The sixth element
     */
    public final V6 getSixth() {
        return this.sixth;
    }

    /**
     * Returns the seventh element of the sexviguple.
     *
     * @return The seventh element
     */
    public final V7 getSeventh() {
        return this.seventh;
    }

    /**
     * Returns the eighth element of the sexviguple.
     *
     * @return The eighth element
     */
    public final V8 getEighth() {
        return this.eighth;
    }

    /**
     * Returns the ninth element of the sexviguple.
     *
     * @return The ninth element
     */
    public final V9 getNinth() {
        return this.ninth;
    }

    /**
     * Returns the tenth element of the sexviguple.
     *
     * @return The tenth element
     */
    public final V10 getTenth() {
        return this.tenth;
    }

    /**
     * Returns the eleventh element of the sexviguple.
     *
     * @return The eleventh element
     */
    public final V11 getEleventh() {
        return this.eleventh;
    }

    /**
     * Returns the twelfth element of the sexviguple.
     *
     * @return The twelfth element
     */
    public final V12 getTwelfth() {
        return this.twelfth;
    }

    /**
     * Returns the thirteenth element of the sexviguple.
     *
     * @return The thirteenth element
     */
    public final V13 getThirteenth() {
        return this.thirteenth;
    }

    /**
     * Returns the fourteenth element of the sexviguple.
     *
     * @return The fourteenth element
     */
    public final V14 getFourteenth() {
        return this.fourteenth;
    }

    /**
     * Returns the fifteenth element of the sexviguple.
     *
     * @return The fifteenth element
     */
    public final V15 getFifteenth() {
        return this.fifteenth;
    }

    /**
     * Returns the sixteenth element of the sexviguple.
     *
     * @return The sixteenth element
     */
    public final V16 getSixteenth() {
        return this.sixteenth;
    }

    /**
     * Returns the seventeenth element of the sexviguple.
     *
     * @return The seventeenth element
     */
    public final V17 getSeventeenth() {
        return this.seventeenth;
    }

    /**
     * Returns the eighteenth element of the sexviguple.
     *
     * @return The eighteenth element
     */
    public final V18 getEighteenth() {
        return this.eighteenth;
    }

    /**
     * Returns the nineteenth element of the sexviguple.
     *
     * @return The nineteenth element
     */
    public final V19 getNineteenth() {
        return this.nineteenth;
    }

    /**
     * Returns the twentieth element of the sexviguple.
     *
     * @return The twentieth element
     */
    public final V20 getTwentieth() {
        return this.twentieth;
    }

    /**
     * Returns the twenty-first element of the sexviguple.
     *
     * @return The twenty-first element
     */
    public final V21 getTwentyFirst() {
        return this.twentyFirst;
    }

    /**
     * Returns the twenty-second element of the sexviguple.
     *
     * @return The twenty-second element
     */
    public final V22 getTwentySecond() {
        return this.twentySecond;
    }

    /**
     * Returns the twenty-third element of the sexviguple.
     *
     * @return The twenty-third element
     */
    public final V23 getTwentyThird() {
        return this.twentyThird;
    }

    /**
     * Returns the twenty-fourth element of the sexviguple.
     *
     * @return The twenty-fourth element
     */
    public final V24 getTwentyFourth() {
        return this.twentyFourth;
    }

    /**
     * Returns the twenty-fifth element of the sexviguple.
     *
     * @return The twenty-fifth element
     */
    public final V25 getTwentyFifth() {
        return this.twentyFifth;
    }

    /**
     * Returns the twenty-sixth element of the sexviguple.
     *
     * @return The twenty-sixth element
     */
    public final V26 getTwentySixth() {
        return this.twentySixth;
    }

    @Override
    public final Stream<Object> toStream() {
        return Stream.of(this.first, this.second, this.third, this.fourth, this.fifth, this.sixth, this.seventh, this.eighth,
                this.ninth, this.tenth, this.eleventh, this.twelfth, this.thirteenth, this.fourteenth, this.fifteenth,
                this.sixteenth, this.seventeenth, this.eighteenth, this.nineteenth, this.twentieth, this.twentyFirst, this.twentySecond, this.twentyThird,
                this.twentyFourth, this.twentyFifth, this.twentySixth);
    }

}