/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tools;

/**
 * Common interface for different builder implementations of a collection (sequence, set, vector, etc.) of values.
 *
 * @param <V> The generic type of the values
 * @param <C> The generic type of the collection to build
 */
public interface Builder<V, C> {

    /**
     * Adds a new value to the collection. The builder object itself is returned to allow pipeline notation when
     * multiple values are added to the collection.
     *
     * @param value The value to be added
     * @return The builder itself
     */
    @SuppressWarnings("UnusedReturnValue")
    Builder<V, C> add(V value);

    /**
     * Terminates the building process and returns the constructed collection of values.
     *
     * @return The constructed collection of values
     */
    C build();

    /**
     * Adds multiple values to the collection. The builder object itself is returned to allow pipeline notation.
     *
     * @param values An iterable collection of values to be added
     * @return The builder itself
     */
    default Builder<V, C> addAll(Iterable<V> values) {
        values.forEach(this::add);
        return this;
    }

}
