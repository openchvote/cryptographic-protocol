/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tools.Hashable;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static ch.openchvote.base.utilities.UtilityException.Type.*;

/**
 * Objects of this generic class represent immutable matrices of a fixed height {@code n} and width {@code m}. Their
 * elements are indexed by two indices from {@code minRowIndex} (usually {@code 1}) to {@code maxRowIndex} (usually
 * {@code n}) and {@code minColIndex} (usually {@code 1}) to {@code maxColIndex} (usually {@code m}). Since this class
 * is abstract, it does not offer public constructors. New instances of this class are created using the static member
 * classes {@link Matrix.Builder}, {@link Matrix.RowBuilder}, and {@link Matrix.ColBuilder}. During the building
 * process, values can be added to or placed into an initially empty matrix containing {@code null} values. After the
 * building process, accessing the values in the resulting matrix is restricted to read-only. The class implements the
 * {@link Hashable} and {@link Sequence} interfaces.
 *
 * @param <V> The generic type of the values stored in the matrix
 */
public abstract sealed class Matrix<V> extends Hashable implements Sequence<V> permits Matrix.NonSealed {

    // the purpose of this protected non-sealed inner class is to allow anonymous subclasses exclusively within this package
    static protected non-sealed abstract class NonSealed<V> extends Matrix<V> {

        // protected constructor for internal use in anonymous subclasses
        protected NonSealed(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            super(minRowIndex, maxRowIndex, minColIndex, maxColIndex);
        }

    }

    // the minimal/maximal row and column indices
    protected final int minRowIndex;
    protected final int maxRowIndex;
    protected final int minColIndex;
    protected final int maxColIndex;

    // protected constructor for internal use in subclasses
    protected Matrix(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
        this.minRowIndex = minRowIndex;
        this.maxRowIndex = maxRowIndex;
        this.minColIndex = minColIndex;
        this.maxColIndex = maxColIndex;
    }

    /**
     * Returns the value at the given row and column index. For an invalid indices, an exception is thrown.
     *
     * @param rowIndex The row index
     * @param colIndex The column index
     * @return The value at this row and column index
     */
    public abstract V getValue(int rowIndex, int colIndex);

    /**
     * Returns the minimal row index of this matrix.
     *
     * @return The minimal row index
     */
    public int getMinRowIndex() {
        return this.minRowIndex;
    }

    /**
     * Returns the maximal row index of this matrix.
     *
     * @return The maximal row index
     */
    public int getMaxRowIndex() {
        return this.maxRowIndex;
    }

    /**
     * Returns the minimal column index of this matrix.
     *
     * @return The minimal column index
     */
    public int getMinColIndex() {
        return this.minColIndex;
    }

    /**
     * Returns the maximal column index of this matrix.
     *
     * @return The maximal column index
     */
    public int getMaxColIndex() {
        return this.maxColIndex;
    }

    /**
     * Returns the set of the matrix's row indices.
     *
     * @return The set of row indices
     */
    public IntSet getRowIndices() {
        return IntSet.range(this.getMinRowIndex(), this.getMaxRowIndex());
    }

    /**
     * Returns the set of the matrix's column indices.
     *
     * @return The set of column indices
     */
    public IntSet getColIndices() {
        return IntSet.range(this.getMinColIndex(), this.getMaxColIndex());
    }

    /**
     * Returns the height of the matrix
     *
     * @return The height of the matrix
     */
    public int getHeight() {
        return this.maxRowIndex - this.minRowIndex + 1;
    }

    /**
     * Returns the width of the matrix
     *
     * @return The width of the matrix
     */
    public int getWidth() {
        return this.maxColIndex - this.minColIndex + 1;
    }

    /**
     * Returns a vector consisting of the matrix's row vectors.
     *
     * @return The vector of row vectors
     */
    public Vector<Vector<V>> getRows() {
        return new Vector.NonSealed<>(this.minRowIndex, this.maxRowIndex) {
            @Override
            public Vector<V> getValue(int index) {
                return Matrix.this.getRow(index);
            }
        };
    }

    /**
     * Returns a vector consisting of the matrix's column vectors.
     *
     * @return The vector of column vectors
     */
    public Vector<Vector<V>> getCols() {
        return new Vector.NonSealed<>(this.minColIndex, this.maxColIndex) {
            @Override
            public Vector<V> getValue(int index) {
                return Matrix.this.getCol(index);
            }
        };
    }

    /**
     * Returns the row vector at the given index.
     *
     * @param rowIndex The given row index
     * @return The row vector at the given index
     */
    public Vector<V> getRow(int rowIndex) {
        if (rowIndex < this.minRowIndex || rowIndex > this.maxRowIndex) {
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, Matrix.class, "RowIndex smaller than minRowIndex or larger than maxRowIndex");
        }
        return new Vector.NonSealed<>(this.minColIndex, this.maxColIndex) {
            @Override
            public V getValue(int index) {
                return Matrix.this.getValue(rowIndex, index);
            }
        };
    }

    /**
     * Returns the column vector at the given index.
     *
     * @param colIndex The given column index
     * @return The column vector at the given index
     */
    public Vector<V> getCol(int colIndex) {
        if (colIndex < this.minColIndex || colIndex > this.maxColIndex) {
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, Matrix.class, "ColIndex smaller than minColIndex or larger than maxColIndex");
        }
        return new Vector.NonSealed<>(this.minRowIndex, this.maxRowIndex) {
            @Override
            public V getValue(int index) {
                return Matrix.this.getValue(index, colIndex);
            }
        };
    }

    /**
     * Creates a new matrix by applying a function to each value of the matrix. Depending on the specified function, the
     * type {@code W} of the returned matrix may be different from the type {@code V} of the original matrix. The given
     * boolean value {@code applyToNull} determines whether the specified function is also applied to {@code null}
     * values. Otherwise, {@code null} values are mapped into {@code null} values. By returning a wrapper object which
     * performs the mapping lazily, this method runs ins constant time.
     *
     * @param function    The function that maps the values of the matrix
     * @param applyToNull Determines whether the function is applied to {@code null}
     * @param <W>         The type of the returned matrix
     * @return A matrix containing all mapped values
     */
    public <W> Matrix<W> map(Function<? super V, ? extends W> function, boolean applyToNull) {
        return new Matrix.NonSealed<>(this.minRowIndex, this.maxRowIndex, this.minColIndex, this.maxColIndex) {
            @Override
            public W getValue(int rowIndex, int colIndex) {
                var value = Matrix.this.getValue(rowIndex, colIndex);
                return value == null && !applyToNull ? null : function.apply(value);
            }
        };
    }

    /**
     * Returns the transposed matrix, in which the row and column indices and the height and width are swapped.
     *
     * @return The transposed matrix
     */
    public Matrix<V> transpose() {
        return new Matrix.NonSealed<>(this.minColIndex, this.maxColIndex, this.minRowIndex, this.maxRowIndex) {
            @Override
            public V getValue(int rowIndex, int colIndex) {
                return Matrix.this.getValue(colIndex, rowIndex);
            }

            @Override
            public Matrix<V> transpose() {
                return Matrix.this;
            }
        };
    }

    @Override
    public int getMinIndex() {
        return 0;
    }

    @Override
    public int getMaxIndex() {
        return this.getWidth() * this.getHeight() - 1;
    }

    @Override
    public V getValue(int index) {
        int rowIndex = index / this.getWidth() + this.minRowIndex;
        int colIndex = index % this.getWidth() + this.minColIndex;
        return this.getValue(rowIndex, colIndex);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object instanceof Matrix<?> other) {
            if (this.minRowIndex != other.getMinRowIndex()) return false;
            if (this.maxRowIndex != other.getMaxRowIndex()) return false;
            if (this.minColIndex != other.getMinColIndex()) return false;
            if (this.maxColIndex != other.getMaxColIndex()) return false;
            return this.getIndices().toIntStream().allMatch(index -> Objects.equals(this.getValue(index), other.getValue(index)));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int initValue = 0;
        initValue = 31 * initValue + this.minRowIndex;
        initValue = 31 * initValue + this.maxRowIndex;
        initValue = 31 * initValue + this.minColIndex;
        initValue = 31 * initValue + this.maxColIndex;
        return this.toStream().mapToInt(Objects::hashCode).reduce(initValue, (result, hash) -> 31 * result + hash);
    }

    @Override
    public String toString() {
        if (this.getHeight() == 0 || this.getWidth() == 0) {
            return "[]\n";
        }
        int maxLength = this.toStream().map(Objects::toString).mapToInt(String::length).max().orElse(0);
        var valueFormat = "%" + maxLength + "s";
        var stringBuilder = new StringBuilder();
        for (int i : IntSet.range(0, this.getHeight() - 1)) {
            String prefix, suffix;
            if (this.getHeight() == 1) {
                prefix = "[";
                suffix = "]";
            } else if (i == 0) {
                prefix = "⌈";
                suffix = "⌉";
            } else if (i == this.getHeight() - 1) {
                prefix = "⌊";
                suffix = "⌋";
            } else {
                prefix = "|";
                suffix = "|";
            }
            var row = this.getRow(this.minRowIndex + i).toStream().map(value -> String.format(valueFormat, value)).collect(Collectors.joining(" ", prefix, suffix));
            stringBuilder.append(row);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    /**
     * This static builder class is the main tool for constructing matrices from scratch. The height and width of the
     * matrix are defined at the beginning of the building process. All values are initially all set to {@code null}.
     * Values can be added either incrementally (row-wise from left to right) or in arbitrary order. At the end of the
     * building process, the matrix can be built exactly once. The builder class is threadsafe and the resulting matrix
     * is immutable.
     *
     * @param <V> The generic type of the matrix to build
     */
    static public class Builder<V> implements ch.openchvote.base.utilities.tools.Builder<V, Matrix<V>> {

        // flag that determines whether the matrix has already been built or not
        private boolean built;
        // the first and the last valid row indices
        private final int minRowIndex;
        private final int maxRowIndex;
        // the first and the last valid column indices
        private final int minColIndex;
        private final int maxColIndex;
        // an array for storing the added values during the building process
        private final V[][] values;
        // an internal index counter for adding values incrementally
        private int indexCounter;

        /**
         * Constructs a builder for a matrix of fixed {@code height} and {@code width}. Indexing starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width  The width of the matrix to construct
         */
        public Builder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1} and width
         * {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        @SuppressWarnings("unchecked")
        public Builder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1) {
                throw new UtilityException(INVALID_PARAMETERS, Matrix.Builder.class, "Invalid minimal or maximal indices");
            }
            this.built = false;
            this.minRowIndex = minRowIndex;
            this.maxRowIndex = maxRowIndex;
            this.minColIndex = minColIndex;
            this.maxColIndex = maxColIndex;
            this.values = (V[][]) new Object[maxRowIndex - minRowIndex + 1][maxColIndex - minColIndex + 1];
            this.indexCounter = 0;
        }

        /**
         * Fills up the matrix with a single value.
         *
         * @param value The value used for filling up the matrix
         * @return The matrix builder itself
         */
        public Matrix.Builder<V> fill(V value) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, Matrix.class);
                }
                for (int i : IntSet.range(this.minRowIndex, this.maxRowIndex)) {
                    for (int j : IntSet.range(this.minColIndex, this.maxColIndex)) {
                        this.set(i, j, value);
                    }
                }
                return this;
            }
        }

        /**
         * Sets the given value at the given row and column indices. The matrix builder object itself is returned to
         * allow pipeline notation when multiple values are added to the matrix.
         *
         * @param rowIndex The row index of the value to be added
         * @param colIndex The column index of the value to be added
         * @param value    The value to be added
         * @return The matrix builder itself
         */
        public Matrix.Builder<V> set(int rowIndex, int colIndex, V value) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, Matrix.class);
                }
                if (rowIndex < this.minRowIndex || rowIndex > this.maxRowIndex || colIndex < this.minColIndex || colIndex > this.maxColIndex) {
                    throw new UtilityException(INDEX_OUT_OF_BOUNDS, Matrix.Builder.class, "RowIndex smaller than minRowIndex or larger than maxRowIndex");
                }
                this.values[rowIndex - this.minRowIndex][colIndex - this.minColIndex] = value;
                return this;
            }
        }

        /**
         * Sets the next value in the matrix and increases the internal index counter by 1. In this way, values are
         * added row-wise from left to right. The matrix builder object itself is returned to allow pipeline notation
         * when multiple values are added to the matrix.
         *
         * @param value The value to be added
         * @return The matrix builder itself
         */
        @Override
        public Matrix.Builder<V> add(V value) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, Matrix.class);
                }
                int width = this.maxColIndex - this.minColIndex + 1;
                int currentRowIndex, currentColIndex;
                currentRowIndex = this.minRowIndex + this.indexCounter / width;
                currentColIndex = this.minColIndex + this.indexCounter % width;
                this.indexCounter = this.indexCounter + 1;
                return this.set(currentRowIndex, currentColIndex, value);
            }
        }

        @Override
        public Matrix<V> build() {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, Matrix.class);
                }
                this.built = true;
                return new Matrix.NonSealed<>(this.minRowIndex, this.maxRowIndex, this.minColIndex, this.maxColIndex) {
                    @Override
                    public V getValue(int rowIndex, int colIndex) {
                        if (rowIndex < Builder.this.minRowIndex || rowIndex > Builder.this.maxRowIndex || colIndex < Builder.this.minColIndex || colIndex > Builder.this.maxColIndex) {
                            throw new UtilityException(INDEX_OUT_OF_BOUNDS, Matrix.Builder.class, "ColIndex smaller than minColIndex or larger than maxColIndex");
                        }
                        return Builder.this.values[rowIndex - Builder.this.minRowIndex][colIndex - Builder.this.minColIndex];
                    }
                };
            }
        }

        /**
         * This special builder class can be used to construct matrices with indices from 0. As such it is a special
         * case of the general {@link Matrix.Builder} class and its main purpose is increased convenience.
         *
         * @param <V> The generic type of the matrix to build
         */
        static public class IndicesFromZero<V> extends Matrix.Builder<V> {

            /**
             * Constructs a builder for a matrix with row and column indices from 0 to {@code maxRowIndex} and
             * {@code maxColIndex}, respectively.
             *
             * @param maxRowIndex The maximal row index
             * @param maxColIndex The maximal col index
             */
            public IndicesFromZero(int maxRowIndex, int maxColIndex) {
                super(0, maxRowIndex, 0, maxColIndex);
            }

        }

    }

    /**
     * This static builder class can be used for constructing matrices from given row vectors. The height and width of
     * the matrix are defined at the beginning of the building process. All values are initially all set to
     * {@code null}. Rows can be added either incrementally or in arbitrary order. At the end of the building process,
     * the matrix can be built exactly once. The builder class is threadsafe and the resulting matrix is immutable.
     *
     * @param <V> The generic type of the matrix to build
     */
    static public class RowBuilder<V> {

        // uses a vector builder internally
        private final Vector.Builder<Vector<V>> vectorBuilder;
        private final int minColIndex;
        private final int maxColIndex;

        /**
         * Constructs a new row-wise matrix builder for a matrix of fixed {@code height} and {@code width}. Indexing of
         * both the rows and columns starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width  The width of the matrix to construct
         */
        public RowBuilder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a new row-wise matrix builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1} and
         * width {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        public RowBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1) {
                throw new UtilityException(INVALID_PARAMETERS, Matrix.RowBuilder.class, "Invalid minimal or maximal indices");
            }
            this.vectorBuilder = new Vector.Builder<>(minRowIndex, maxRowIndex);
            this.minColIndex = minColIndex;
            this.maxColIndex = maxColIndex;
        }

        /**
         * Sets the given row vector at the given index. The matrix builder object itself is returned to allow pipeline
         * notation when multiple rows are added to the matrix.
         *
         * @param rowIndex The row index of the row vector to be added
         * @param row      The row vector to be added
         * @return The matrix builder itself
         */
        public RowBuilder<V> setRow(int rowIndex, Vector<V> row) {
            if (row == null || row.getMinIndex() != this.minColIndex || row.getMaxIndex() != this.maxColIndex) {
                throw new UtilityException(INVALID_PARAMETERS, Matrix.RowBuilder.class, "minIndex or maxIndex are not compatible");
            }
            this.vectorBuilder.set(rowIndex, row);
            return this;
        }

        /**
         * Sets the next row vector in the matrix and increases the internal index counter by 1. In this way, row
         * vectors are added incrementally. The matrix builder object itself is returned to allow pipeline notation when
         * multiple row vectors are added to the matrix.
         *
         * @param row The row vector to be added
         * @return The matrix builder itself
         */
        public RowBuilder<V> addRow(Vector<V> row) {
            if (row == null || row.getMinIndex() != this.minColIndex || row.getMaxIndex() != this.maxColIndex) {
                throw new UtilityException(INVALID_PARAMETERS, Matrix.RowBuilder.class, "minIndex or maxIndex are not compatible");
            }
            this.vectorBuilder.add(row);
            return this;
        }

        /**
         * Adds multiple row vectors to the matrix. The builder object itself is returned to allow pipeline notation.
         *
         * @param rows An iterable collection of row vectors to be added
         * @return The builder itself
         */
        public RowBuilder<V> addRows(Iterable<Vector<V>> rows) {
            rows.forEach(this::addRow);
            return this;
        }


        /**
         * Terminates the building process and returns the constructed matrix.
         *
         * @return The constructed matrix
         */
        public Matrix<V> build() {
            Vector<Vector<V>> vector = this.vectorBuilder.build();
            return new Matrix.NonSealed<>(vector.getMinIndex(), vector.getMaxIndex(), this.minColIndex, this.maxColIndex) {
                @Override
                public V getValue(int rowIndex, int colIndex) {
                    var row = vector.getValue(rowIndex);
                    return row == null ? null : row.getValue(colIndex);
                }
            };
        }

    }

    /**
     * This static builder class can be used for constructing matrices from given column vectors. The height and width
     * of the matrix are defined at the beginning of the building process. All values are initially all set to
     * {@code null}. Rows can be added either incrementally or in arbitrary order. At the end of the building process,
     * the matrix can be built exactly once. The builder class is threadsafe and the resulting matrix is immutable.
     *
     * @param <V> The generic type of the matrix to build
     */
    static public class ColBuilder<V> {

        // uses a vector builder internally
        private final Vector.Builder<Vector<V>> vectorBuilder;
        private final int minRowIndex;
        private final int maxRowIndex;

        /**
         * Constructs a new column-wise matrix builder for a matrix of fixed {@code height} and {@code width}. Indexing
         * of both the rows and columns starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width  The width of the matrix to construct
         */
        public ColBuilder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a new column-wise matrix builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1}
         * and width {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        public ColBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1) {
                throw new UtilityException(INVALID_PARAMETERS, Matrix.ColBuilder.class, "Invalid minimal or maximal indices");
            }
            this.minRowIndex = minRowIndex;
            this.maxRowIndex = maxRowIndex;
            this.vectorBuilder = new Vector.Builder<>(minColIndex, maxColIndex);
        }

        /**
         * Sets the given column vector at the given index. The matrix builder object itself is returned to allow
         * pipeline notation when multiple rows are added to the matrix.
         *
         * @param colIndex The column index of the column vector to be added
         * @param col      The column vector to be added
         * @return The matrix builder itself
         */
        public ColBuilder<V> setCol(int colIndex, Vector<V> col) {
            if (col == null || col.getMinIndex() != this.minRowIndex || col.getMaxIndex() != this.maxRowIndex) {
                throw new UtilityException(INVALID_PARAMETERS, Matrix.ColBuilder.class, "minIndex or maxIndex are not compatible");
            }
            this.vectorBuilder.set(colIndex, col);
            return this;
        }

        /**
         * Sets the next column vector in the matrix and increases the internal index counter by 1. In this way, column
         * vectors are added incrementally. The matrix builder object itself is returned to allow pipeline notation when
         * multiple column vectors are added to the matrix.
         *
         * @param col The column vector to be added
         * @return The matrix builder itself
         */
        public ColBuilder<V> addCol(Vector<V> col) {
            if (col == null || col.getMinIndex() != this.minRowIndex || col.getMaxIndex() != this.maxRowIndex) {
                throw new UtilityException(INVALID_PARAMETERS, Matrix.ColBuilder.class, "minIndex or maxIndex are not compatible");
            }
            this.vectorBuilder.add(col);
            return this;
        }

        /**
         * Adds multiple column vectors to the matrix. The builder object itself is returned to allow pipeline
         * notation.
         *
         * @param cols An iterable collection of column vectors to be added
         * @return The builder itself
         */
        public ColBuilder<V> addCols(Iterable<Vector<V>> cols) {
            cols.forEach(this::addCol);
            return this;
        }


        /**
         * Terminates the building process and returns the constructed matrix.
         *
         * @return The constructed matrix
         */
        public Matrix<V> build() {
            Vector<Vector<V>> vector = this.vectorBuilder.build();
            return new Matrix.NonSealed<>(this.minRowIndex, this.maxRowIndex, vector.getMinIndex(), vector.getMaxIndex()) {
                @Override
                public V getValue(int rowIndex, int colIndex) {
                    var col = vector.getValue(colIndex);
                    return col == null ? null : col.getValue(rowIndex);
                }
            };
        }

    }

    /**
     * This static method allows constructing new matrices from a given vector of column vectors. The matrix indexing is
     * inherited from the vector indexing.
     *
     * @param columns The given vector of column vectors
     * @param <V>     The generic type of the values stored in the matrix
     * @return The constructed matrix
     */
    static public <V> Matrix<V> ofCols(Vector<Vector<V>> columns) {
        if (!columns.map(Vector::getMinIndex).isConstant() || !columns.map(Vector::getMaxIndex).isConstant()) {
            throw new UtilityException(INCOMPATIBLE_LENGTHS, Matrix.class);
        }
        int minRowIndex = columns.isEmpty() ? 1 : columns.getValue(columns.getMinIndex()).getMinIndex();
        int maxRowIndex = columns.isEmpty() ? 0 : columns.getValue(columns.getMinIndex()).getMaxIndex();
        int minColIndex = columns.getMinIndex();
        int maxColIndex = columns.getMaxIndex();
        var builder = new Matrix.ColBuilder<V>(minRowIndex, maxRowIndex, minColIndex, maxColIndex);
        columns.forEach(builder::addCol);
        return builder.build();
    }

    /**
     * This static method allows constructing new matrices from a given vector of row vectors. The matrix indexing is
     * inherited from the vector indexing.
     *
     * @param rows The given vector of row vectors
     * @param <V>  The generic type of the values stored in the matrix
     * @return The constructed matrix
     */
    static public <V> Matrix<V> ofRows(Vector<Vector<V>> rows) {
        if (!rows.map(Vector::getMinIndex).isConstant() || !rows.map(Vector::getMaxIndex).isConstant()) {
            throw new UtilityException(INCOMPATIBLE_LENGTHS, Matrix.class);
        }
        int minRowIndex = rows.getMinIndex();
        int maxRowIndex = rows.getMaxIndex();
        int minColIndex = rows.isEmpty() ? 1 : rows.getValue(rows.getMinIndex()).getMinIndex();
        int maxColIndex = rows.isEmpty() ? 0 : rows.getValue(rows.getMinIndex()).getMaxIndex();
        var builder = new Matrix.RowBuilder<V>(minRowIndex, maxRowIndex, minColIndex, maxColIndex);
        rows.forEach(builder::addRow);
        return builder.build();
    }

    /**
     * This static method allows constructing new matrices from a given set of key-value pairs, where the keys are of
     * type {@code Integer} and the values of type {@code Vector<V>}. Each of the vectors in the map defines a column
     * vector in the resulting matrix. The keys of the map are the column indices in the newly created matrix, i.e., the
     * indexing of the column vectors in the matrix goes from the smallest to the largest key in the map.
     *
     * @param entries The given map of column vectors
     * @param <V>     The generic type of the values stored in the matrix
     * @return The constructed matrix
     */
    static public <V> Matrix<V> ofCols(Set<Map.Entry<Integer, Vector<V>>> entries) {
        var columns = Vector.of(entries);
        return Matrix.ofCols(columns);
    }

    /**
     * This static method allows constructing new matrices from a given set of key-value pairs, where the keys are of
     * type {@code Integer} and the values of type {@code Vector<V>}. Each of the vectors in the map defines a row
     * vector in the resulting matrix. The keys of the map are the row indices in the newly created matrix, i.e., the
     * indexing of the column vectors in the matrix goes from the smallest to the largest key in the map.
     *
     * @param entries The given map of row vectors
     * @param <V>     The generic type of the values stored in the matrix
     * @return The constructed matrix
     */
    static public <V> Matrix<V> ofRows(Set<Map.Entry<Integer, Vector<V>>> entries) {
        var rows = Vector.of(entries);
        return Matrix.ofRows(rows);
    }

}
