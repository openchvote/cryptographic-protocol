/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.internal.MutableByteArray;
import ch.openchvote.base.utilities.tools.Math;

import java.util.Arrays;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

import static ch.openchvote.base.utilities.UtilityException.Type.*;

/**
 * Objects of this class represent immutable byte arrays of a fixed length n. Their elements are indexed from 0 to n-1.
 * The class provides methods for various operations on such byte arrays (skipping or truncating elements, bit-wise
 * logical operations, concatenation, etc.). All these methods are implemented to run in constant time. Since this class
 * is abstract, it does not offer public constructors. New instances of this class are created using the local
 * {@link Builder} class. During the building process, the bytes can be added to or placed into an initially empty byte
 * array consisting of 0s. After the building process, accessing the bytes in the resulting byte array is restricted to
 * read-only, i.e., instances of this class are immutable. This class implements the {@link IntSequence} interface.
 */
public abstract sealed class ByteArray implements IntSequence permits MutableByteArray, ByteArray.NonSealed {

    /**
     * The empty byte array of length 0.
     */
    static public final ByteArray EMPTY = new ByteArray.Builder(0).build();

    // the purpose of this protected non-sealed inner class is to allow anonymous subclasses exclusively within this package
    static protected non-sealed abstract class NonSealed extends ByteArray {

        // protected constructor for internal use in anonymous subclasses
        protected NonSealed(int length) {
            super(length);
        }

    }

    // the length of the byte array
    protected final int length;

    // protected constructor for internal use in subclasses
    protected ByteArray(int length) {
        this.length = length;
    }

    /**
     * Returns the byte at the given index. For an invalid index, an exception is thrown.
     *
     * @param index The index
     * @return The byte at this index
     */
    public abstract byte getByte(int index);

    /**
     * Creates and returns a new byte array by skipping the first {@code k<=n} bytes of the given byte array of length
     * {@code n}. The length of the returned byte array is {@code n-k}. An exception is thrown for an invalid
     * {@code k}.
     *
     * @param k The number of bytes to skip
     * @return The byte array obtained from skipping the first {@code k} bytes
     */
    public ByteArray skip(int k) {
        if (k < 0 || k > this.length) {
            throw new UtilityException(INVALID_PARAMETERS, ByteArray.class, "Value k not within range");
        }
        if (k == 0) {
            return this;
        }
        return new ByteArray.NonSealed(this.length - k) {
            @Override
            public byte getByte(int index) {
                if (index < 0 || index >= this.length) {
                    throw new UtilityException(INDEX_OUT_OF_BOUNDS, ByteArray.class, "Negative index or index larger than length");
                }
                return ByteArray.this.getByte(k + index);
            }
        };
    }

    /**
     * Creates and returns a new byte array, which contains the {@code k} first bytes of the given byte array. An
     * exception is thrown for an invalid {@code k}.
     *
     * @param k The length of the returned byte array
     * @return The byte array obtained from keeping the first {@code k} bytes
     */
    public ByteArray truncate(int k) {
        if (k < 0 || k > this.length) {
            throw new UtilityException(INVALID_PARAMETERS, ByteArray.class, "Value k not within range");
        }
        if (k == this.length) {
            return this;
        }
        return new ByteArray.NonSealed(k) {
            @Override
            public byte getByte(int index) {
                if (index < 0 || index >= this.length) {
                    throw new UtilityException(INDEX_OUT_OF_BOUNDS, ByteArray.class, "Negative index or index larger than length");
                }
                return ByteArray.this.getByte(index);
            }
        };
    }

    /**
     * Returns the concatenation of the given byte array with another byte array.
     *
     * @param byteArray2 The other byte array
     * @return The concatenated byte array
     */
    public ByteArray concatenate(ByteArray byteArray2) {
        var byteArray1 = this;
        if (byteArray1.length == 0)
            return byteArray2;
        if (byteArray2.length == 0)
            return byteArray1;
        return new ByteArray.NonSealed(byteArray1.length + byteArray2.length) {
            @Override
            public byte getByte(int index) {
                if (index < byteArray1.length)
                    return byteArray1.getByte(index);
                else
                    return byteArray2.getByte(index - byteArray1.length);
            }

            @Override
            protected void toByteArray(byte[] bytes, int offset) {
                byteArray1.toByteArray(bytes, offset);
                byteArray2.toByteArray(bytes, offset + byteArray1.length);
            }
        };
    }

    /**
     * Returns the byte array obtained from applying the logical AND-operator bit-wise to all bytes of the two byte
     * arrays. An exception is thrown if their lengths are incompatible.
     *
     * @param other The other byte array
     * @return The result from applying the AND-operator
     */
    public ByteArray and(ByteArray other) {
        return ByteArray.map(this, other, Math::and);
    }

    /**
     * Returns the byte array obtained from applying the logical OR-operator bit-wise to all bytes of the two byte
     * arrays. An exception is thrown if their lengths are incompatible.
     *
     * @param other The other byte array
     * @return The result from applying the OR-operator
     */
    public ByteArray or(ByteArray other) {
        return ByteArray.map(this, other, Math::or);
    }

    /**
     * Returns the byte array obtained from applying the logical XOR-operator bit-wise to all bytes of the two byte
     * arrays. An exception is thrown if their lengths are incompatible.
     *
     * @param other The other byte array
     * @return The result from applying the XOR-operator
     */
    public ByteArray xor(ByteArray other) {
        return ByteArray.map(this, other, Math::xor);
    }

    // private helper method for applying a binary operator bit-wise.
    static private ByteArray map(ByteArray byteArray1, ByteArray byteArray2, BinaryOperator<Byte> operator) {
        if (byteArray1.length != byteArray2.length) {
            throw new UtilityException(INCOMPATIBLE_LENGTHS, ByteArray.class);
        }
        return new ByteArray.NonSealed(byteArray1.length) {
            @Override
            public byte getByte(int index) {
                return operator.apply(byteArray1.getByte(index), byteArray2.getByte(index));
            }
        };
    }

    /**
     * Computes the concatenation of multiple byte arrays in the given order. The byte arrays are given as a vector.
     *
     * @param byteArrays The vector of byteArrays
     * @return The concatenation of the given byte arrays
     */
    static public ByteArray concatenate(Vector<ByteArray> byteArrays) {
        return byteArrays.toStream().reduce(ByteArray.EMPTY, ByteArray::concatenate);
    }

    /**
     * Returns the byte array obtained from applying the logical AND-operator bit-wise to all bytes of the given byte
     * arrays. The byte arrays are given as a vector. An exception is thrown if their lengths are incompatible with the
     * given length. If the given vector is empty, a byte array of the given length is returned (consisting of 1-bits).
     *
     * @param byteArrays The vector of byte arrays
     * @param length     The length of the byte arrays
     * @return The result from applying the AND-operator
     */
    static public ByteArray and(Vector<ByteArray> byteArrays, int length) {
        return byteArrays.toStream().reduce(ByteArray::and).orElse(ByteArray.of(Math.ONES, length));
    }

    /**
     * Returns the byte array obtained from applying the logical OR-operator bit-wise to all bytes of the given byte
     * arrays. The byte arrays are given as a vector. An exception is thrown if their lengths are incompatible with the
     * given length. If the given vector is empty, a byte array of the given length is returned (consisting of 0-bits).
     *
     * @param byteArrays The vector of byte arrays
     * @param length     The length of the byte arrays
     * @return The result from applying the OR-operator
     */
    static public ByteArray or(Vector<ByteArray> byteArrays, int length) {
        return byteArrays.toStream().reduce(ByteArray.of(Math.ZEROS, length), ByteArray::or);
    }

    /**
     * Returns the byte array obtained from applying the logical XOR-operator bit-wise to all bytes of the given byte
     * arrays. The byte arrays are given as a vector. An exception is thrown if their lengths are incompatible with the
     * given length. If the given vector is empty, a byte array of the given length is returned (consisting of 0-bits).
     *
     * @param byteArrays The vector of byte arrays
     * @param length     The length of the byte arrays
     * @return The result from applying the XOR-operator
     */
    static public ByteArray xor(Vector<ByteArray> byteArrays, int length) {
        return byteArrays.toStream().reduce(ByteArray.of(Math.ZEROS, length), ByteArray::xor);
    }

    /**
     * Creates and returns a new byte array with the same bytes, except for the byte at the specified index, which
     * receives a new value. An exception is thrown if the specified index is invalid. The new byte is given as an
     * {@code int} value.
     *
     * @param byteIndex The index of the changed byte
     * @param value     The new byte given as an {@code int} value
     * @return The new byte array with the byte changed at the specified index
     */
    public ByteArray setByte(int byteIndex, int value) {
        return this.setByte(byteIndex, Math.intToByte(value));
    }

    /**
     * Creates and returns a new byte array with the same bytes, except for the byte at the specified index, which
     * receives a new value. An exception is thrown if the specified index is invalid. The new byte is given as an
     * {@code int} value.
     *
     * @param byteIndex The index of the changed byte
     * @param value     The new byte
     * @return The new byte array with the byte changed at the specified index
     */
    public ByteArray setByte(int byteIndex, byte value) {
        if (byteIndex < 0 || byteIndex >= this.length) {
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, ByteArray.class, "Negative index or index larger than length");
        }
        if (value == this.getByte(byteIndex))
            return this;
        return new ByteArray.NonSealed(this.length) {
            @Override
            public byte getByte(int index) {
                return index == byteIndex ? value : ByteArray.this.getByte(index);
            }

            @Override
            protected void toByteArray(byte[] bytes, int offset) {
                ByteArray.this.toByteArray(bytes, offset);
                bytes[offset + byteIndex] = value;
            }
        };
    }

    /**
     * Returns a Java {@code byte[]} containing the bytes of the byte array in the same order.
     *
     * @return The resulting instance of {@code byte[]}
     */
    public byte[] toByteArray() {
        var bytes = new byte[this.length];
        this.toByteArray(bytes, 0);
        return bytes;
    }

    // protected helper method for transforming the byte array in a byte[]
    protected void toByteArray(byte[] bytes, int offset) {
        this.getIndices().forEach(index -> bytes[offset + index] = this.getByte(index));
    }

    @Override
    public int getMinIndex() {
        return 0;
    }

    @Override
    public int getMaxIndex() {
        return this.length - 1;
    }

    @Override
    public int getValue(int index) {
        return Math.byteToInt(this.getByte(index));
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object instanceof ByteArray other) {
            if (this.length == other.length) {
                return this.getIndices().toIntStream().allMatch(index -> this.getByte(index) == other.getByte(index));
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.toIntStream().reduce(this.length, (result, value) -> 31 * result + value);
    }

    @Override
    public String toString() {
        return this.toIntStream().mapToObj(b -> String.format("%02x", Math.byteToInt((byte) b)).toUpperCase()).collect(Collectors.joining());
    }

    /**
     * This class allows building new byte arrays of a given length. Initially, all bytes of the byte array are set to
     * 0. The bytes can then be set either in arbitrary order (using {@link Builder#set}) or in ascending order (using
     * {@link Builder#add}). When the building process is over, the construction of the byte array can be finalized
     * (using {@link Builder#build}). While the resulting byte array offers read-only access, the builder offers
     * write-only access. The builder itself is threadsafe.
     */
    static public class Builder implements ch.openchvote.base.utilities.tools.Builder<Byte, ByteArray> {

        // flag to indicate the end of the building process
        private boolean built;
        // array of bytes used during building process
        private final byte[] bytes;
        // an internal index counter for adding bytes incrementally
        private int indexCounter;

        /**
         * Constructs a new builder for a byte array of a given length. Returns an exception if the length is negative.
         *
         * @param length The length of the byte array
         */
        public Builder(int length) {
            if (length < 0) {
                throw new UtilityException(INVALID_LENGTH, ByteArray.Builder.class, "Negative length not allowed");
            }
            this.built = false;
            this.bytes = new byte[length];
            this.indexCounter = 0;
        }

        /**
         * Sets the byte at the specified index to {@code x}. This is a convenience method for setting bytes in the byte
         * array in form of {@code int} values. For an invalid value {@code x} or an invalid index, an exception is
         * thrown. The builder object is returned to enable pipeline notation.
         *
         * @param index The index
         * @param value The byte (given as {@code int}) to be stored at the index
         * @return The builder object
         */
        public ByteArray.Builder set(int index, int value) {
            return this.set(index, Math.intToByte(value));
        }

        /**
         * Sets the byte at the specified index to {@code b}. For an invalid index, an exception is thrown. The builder
         * object is returned to enable pipeline notation.
         *
         * @param index The index
         * @param value The byte to be stored at the index
         * @return The builder object
         */
        public ByteArray.Builder set(int index, byte value) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, ByteArray.Builder.class);
                }
                if (index < 0 || index >= this.bytes.length) {
                    throw new UtilityException(INDEX_OUT_OF_BOUNDS, ByteArray.Builder.class, "Negative index or index larger than length");
                }
                this.bytes[index] = value;
                return this;
            }
        }

        /**
         * Sets the next byte in the byte array to {@code x} and increases the internal index counter by 1. This is a
         * convenience method for adding bytes to the byte array in form of {@code int} values. The byte array builder
         * object itself is returned to allow pipeline notation when multiple bytes are added to the byte array.
         *
         * @param value The int to be added
         * @return The byte array builder itself
         */
        public ByteArray.Builder add(int value) {
            return this.add(Math.intToByte(value));
        }

        /**
         * Sets the next byte in the byte array to {@code b} and increases the internal index counter by 1. The byte
         * array builder object itself is returned to allow pipeline notation when multiple bytes are added to the byte
         * array.
         *
         * @param value The byte to be added
         * @return The byte array builder itself
         */
        public ByteArray.Builder add(byte value) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, ByteArray.Builder.class);
                }
                int currentIndex;
                currentIndex = this.indexCounter;
                this.indexCounter = this.indexCounter + 1;
                return this.set(currentIndex, value);
            }
        }

        @Override
        public ch.openchvote.base.utilities.tools.Builder<Byte, ByteArray> add(Byte value) {
            return this.add(value.byteValue());
        }

        /**
         * Terminates the building process and returns the constructed byte array.
         *
         * @return The constructed byte array
         */
        public ByteArray build() {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, ByteArray.Builder.class);
                }
                this.built = true;
                return new MutableByteArray(this.bytes);
            }
        }

    }

    /**
     * Returns a byte array consisting of a single byte given as integer.
     *
     * @param b The byte given as integer
     * @return The byte array consisting of the given integer
     */
    static public ByteArray of(int b) {
        return ByteArray.of(Math.intToByte(b));
    }

    /**
     * Returns a byte array consisting of a single given byte.
     *
     * @param value The given byte
     * @return The byte array consisting of the given byte
     */
    static public ByteArray of(byte value) {
        return ByteArray.of(value, 1);
    }

    /**
     * Returns a uniform byte array of a given length, where the byte is given as an integer.
     *
     * @param value  The byte given as integer
     * @param length The length of the resulting byte array
     * @return The uniform byte array of the given length
     */
    static public ByteArray of(int value, int length) {
        return ByteArray.of(Math.intToByte(value), length);
    }

    /**
     * Returns a uniform byte array of a given length.
     *
     * @param value  The given byte
     * @param length The length of the resulting byte array
     * @return The uniform byte array of the given length
     */
    static public ByteArray of(byte value, int length) {
        return new ByteArray.NonSealed(length) {
            @Override
            public byte getByte(int index) {
                return value;
            }
        };
    }

    /**
     * Static method to construct new byte arrays from a given varargs array of bytes. The given bytes are copied into
     * a new {@code byte[]} to ensure immutability of the returned byte array.
     *
     * @param bytes The given (array of) bytes
     * @return A new {@code ByteArray} instance from a given {@code byte[]}
     */
    static public ByteArray of(byte... bytes) {
        if (bytes == null) {
            throw new UtilityException(NULL_POINTER, ByteArray.class);
        }
        var copiedArray = Arrays.copyOf(bytes, bytes.length);
        return new MutableByteArray(copiedArray);
    }
    
}
