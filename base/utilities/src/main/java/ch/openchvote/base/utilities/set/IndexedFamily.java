/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.set;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.tools.Hashable;
import ch.openchvote.base.utilities.tools.Streamable;
import ch.openchvote.base.utilities.tuples.Pair;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ch.openchvote.base.utilities.UtilityException.Type.ALREADY_BUILT;
import static ch.openchvote.base.utilities.UtilityException.Type.NULL_POINTER;

/**
 * Informally, an indexed family is a set of elements E, each associated with an index from some index set I, such that
 * the indexing from I to E is unique , i.e., every index from the index set refers to exactly one element from the
 * given set of elements. Alternatively, one can think of an indexed family as an injective function with domain I and
 * co-domain E, or as a Java object of type {@code Map<Integer,E>} with a generic type {@code <E>} defining the set of
 * elements. Iterating through an indexed family always produces the same order with ascending indices. Instances of
 * this class are immutable.
 *
 * @param <E> The generic type of the elements
 */
public abstract sealed class IndexedFamily<E> extends Hashable implements FiniteSet<IndexedFamily.IndexedElement<E>, Long>, Streamable<IndexedFamily.IndexedElement<E>> permits IndexedFamily.NonSealed {

    // the purpose of this protected non-sealed inner class is to allow anonymous subclasses exclusively within this package
    static protected non-sealed abstract class NonSealed<V> extends IndexedFamily<V> {

    }

    // a sorted map for storing the key/element-associations of the indexed family in ascending index order
    protected abstract SortedMap<Integer, E> getSortedMap();

    /**
     * Checks if the indexed family contains a given index.
     *
     * @param index The given index
     * @return {@code true}, if the indexed family contains {@code index}, {@code false} otherwise
     */
    public boolean containsIndex(int index) {
        return this.getSortedMap().containsKey(index);
    }

    /**
     * If the indexed family contains the given index, the corresponding element is returned. If no entry for
     * {@code index} exists, the element {@code null} is returned.
     *
     * @param index The given index
     * @return The element associated with {@code index} in the list or {@code null}
     */
    public E getElement(int index) {
        return this.getSortedMap().get(index);
    }

    /**
     * Returns the set of all indices from of the indexed family.
     *
     * @return The stream of all indices
     */
    public IntSet getIndices() {
        return IntSet.of(this.getSortedMap().keySet());
    }

    /**
     * Returns a stream of all elements from of the indexed family in ascending index order.
     *
     * @return The stream of all elements
     */
    public Stream<E> getElements() {
        return this.getIndices().toIntStream().mapToObj(this::getElement);
    }

    @Override
    public boolean contains(IndexedElement<E> indexedElement) {
        var index = indexedElement.getIndex();
        var element = indexedElement.getElement();
        return this.containsIndex(index) && this.getElement(index).equals(element);
    }

    @Override
    public Long getSize() {
        return (long) this.getSortedMap().size();
    }

    @Override
    public Stream<IndexedElement<E>> toStream() {
        return this.getSortedMap().entrySet().stream().map(IndexedElement::new);
    }

    @Override
    public String toString() {
        return this.toStream().map(Objects::toString).collect(Collectors.joining(",", "[", "]"));
    }

    /**
     * Factory method for constructing a new IndexedFamily instance from an iterable collection of
     * {@link Map.Entry} objects.
     *
     * @param entries The given iterable collection of entries
     * @param <E>     The generic type of the elements
     * @return A new IndexedFamily instance
     */
    static public <E> IndexedFamily<E> of(Set<Map.Entry<Integer, E>> entries) {
        if (entries == null) {
            throw new UtilityException(NULL_POINTER, IndexedFamily.class);
        }
        var builder = new IndexedFamily.Builder<E>();
        entries.forEach(entry -> builder.addElement(entry.getKey(), entry.getValue()));
        return builder.build();
    }

    /**
     * This static builder class is the main tool for constructing indexed families incrementally from scratch. The size
     * of the indexed family under construction grows when new entries are added. At the end of the building process,
     * the indexed family can be built exactly once. The builder class is threadsafe and the resulting indexed family is
     * immutable.
     *
     * @param <E> The generic type of the elements
     */
    static public class Builder<E> implements ch.openchvote.base.utilities.tools.Builder<IndexedElement<E>, IndexedFamily<E>> {

        private final SortedMap<Integer, E> map;
        private boolean built;

        /**
         * Constructs a builder for an indexed family. The size of the indexed family grows automatically when entries
         * are added.
         */
        public Builder() {
            this.map = new TreeMap<>();
            this.built = false;
        }

        /**
         * Adds the given entry (index, element) to the indexed family. The builder object itself is returned to allow
         * pipeline notation when multiple entries are added to the indexed family.
         *
         * @param index   The given index of the entry to be added
         * @param element The given element of the entry to be added
         * @return The builder itself
         */
        public Builder<E> addElement(int index, E element) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, IndexedFamily.Builder.class);
                }
                this.map.put(index, element);
                return this;
            }
        }

        @Override
        public IndexedFamily.Builder<E> add(IndexedElement<E> indexedElement) {
            return this.addElement(indexedElement.getIndex(), indexedElement.getElement());
        }

        @Override
        public IndexedFamily<E> build() {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, IndexedFamily.Builder.class);
                }
                this.built = true;
                return new IndexedFamily.NonSealed<>() {
                    @Override
                    protected SortedMap<Integer, E> getSortedMap() {
                        return Builder.this.map;
                    }
                };
            }
        }

    }

    /**
     * Helper class to assign elements to indices.
     *
     * @param <E> The generic type of the elements
     */
    static public class IndexedElement<E> extends Pair<Integer, E> implements Comparable<IndexedElement<E>> {

        /**
         * Public constructor for creating assignments.
         *
         * @param index   The given index
         * @param element The given element
         */
        public IndexedElement(Integer index, E element) {
            super(index, element);
        }

        /**
         * Public constructor for creating assignments from an {@link Map.Entry} instance. This constructor serves as a
         * bridge between Java {@link Map} and {@link IndexedFamily} objects.
         *
         * @param entry The map entry
         */
        public IndexedElement(Map.Entry<Integer, E> entry) {
            this(entry.getKey(), entry.getValue());
        }

        /**
         * Returns the index of an assignment.
         *
         * @return The index of an assignment
         */
        public int getIndex() {
            return this.getFirst();
        }

        /**
         * Returns the element of an assignment.
         *
         * @return The element of an assignment
         */
        public E getElement() {
            return this.getSecond();
        }

        @Override
        public int compareTo(IndexedElement<E> other) {
            return this.getIndex() - other.getIndex();
        }
    }

}
