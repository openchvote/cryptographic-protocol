/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.crypto;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.internal.MutableByteArray;
import ch.openchvote.base.utilities.sequence.ByteArray;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static ch.openchvote.base.utilities.UtilityException.Type.*;

/**
 * This class provides an abstraction of the AES-GCM encryption scheme. It consists of two methods for encrypting
 * and decrypting arbitrary messages for a given secret key. The key length is 128 bits (16 bytes). To encrypt
 * messages using the GCM mode of operation, a fresh initialization vector (IV) must be specified for performing both
 * the encryption and decryption. The IV length is fixed to 96 Bits (12 bytes). All parameters (secret key, message,
 * initialization vector) are instances of the class {@link ByteArray}. Internally, existing Java security classes
 * {@link Cipher}, {@link SecretKeySpec}, and {@link GCMParameterSpec} are used to perform the AES encryption and
 * decryption.
 */
public final class AES implements BlockCipher {

    static private final int KEY_LENGTH = 16; // number of bytes
    static private final int TAG_LENGTH = 16; // number of bytes
    static private final int IV_LENGTH = 12; // number of bytes

    // package-private constructor to allow singleton instantiation in HashAlgorithm
    AES() {
    }

    @Override
    public int getKeyLength() {
        return KEY_LENGTH;
    }

    @Override
    public int getIVLength() {
        return IV_LENGTH;
    }

    @Override
    public ByteArray encrypt(ByteArray K, ByteArray IV, ByteArray M) {
        if (K.getLength() != KEY_LENGTH || IV.getLength() != IV_LENGTH) {
            throw new UtilityException(AES_ENCRYPTION_SETUP_ERROR, AES.class, "Key length or initialization vector length not compatible");
        }
        try {
            var keySpec = new SecretKeySpec(K.toByteArray(), "AES");
            var ivSpec = new GCMParameterSpec(TAG_LENGTH * Byte.SIZE, IV.toByteArray());
            var cipher = Cipher.getInstance("AES/GCM/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
            var bytes = cipher.doFinal(M.toByteArray());
            return new MutableByteArray(bytes);
        } catch (Exception exception) {
            throw new UtilityException(AES_ENCRYPTION_SETUP_ERROR, AES.class);
        }
    }

    @Override
    public ByteArray decrypt(ByteArray K, ByteArray IV, ByteArray C) {
        if (K.getLength() != KEY_LENGTH || IV.getLength() != IV_LENGTH) {
            throw new UtilityException(AES_DECRYPTION_SETUP_ERROR, AES.class, "Key length or initialization vector length not compatible");
        }
        try {
            var keySpec = new SecretKeySpec(K.toByteArray(), "AES");
            var ivSpec = new GCMParameterSpec(TAG_LENGTH * Byte.SIZE, IV.toByteArray());
            var cipher = Cipher.getInstance("AES/GCM/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            var bytes = cipher.doFinal(C.toByteArray());
            return new MutableByteArray(bytes);
        } catch (Exception exception) {
            throw new UtilityException(AES_DECRYPTION_SETUP_ERROR, AES.class);
        }
    }

}
