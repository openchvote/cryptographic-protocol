/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.serializer;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.*;
import ch.openchvote.base.utilities.sequence.internal.MutableByteArray;
import ch.openchvote.base.utilities.set.IndexedFamily;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Pair;
import ch.openchvote.base.utilities.tuples.Singleton;
import ch.openchvote.base.utilities.tuples.Triple;
import ch.openchvote.base.utilities.tuples.Tuple;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

import static ch.openchvote.base.utilities.UtilityException.Type.DESERIALIZATION_ERROR;
import static ch.openchvote.base.utilities.UtilityException.Type.SERIALIZATION_ERROR;

/**
 * The static methods of this helper class can be used to serialize arbitrary objects of various types into strings and
 * back. The supported types are: {@link java.lang.Boolean}, {@link java.lang.Integer}, {@link BigInteger},
 * {@link java.lang.String}, {@link ByteArray}, {@link IntVector}, {@link IntMatrix}, {@link IntSet}, {@link Vector},
 * {@link Matrix}, subclasses of {@link Tuple} ({@link Singleton}, {@link Pair}, {@link Triple}, etc.),
 * {@link IndexedFamily}, {@link Map}, {@link Map.Entry}, and {@link Iterable}. Nesting these types has no restrictions.
 * For a class implementing {@link Iterable}, a constructor must exist in this class, which accepts an iterable
 * collection of values, otherwise the deserialization will not work. This is the case for most classes of the Java
 * Collection Framework, for example for {@link ArrayList} or {@link HashSet}.
 */
public class Serializer {

    // private no-argument constructor to prevent the creation of instances of a utility class
    private Serializer() {
    }

    // special serialization characters and strings
    static private final char OPEN = '[';
    static private final char CLOSE = ']';
    static private final char DELIMITER = ',';
    static private final char ESCAPE = '\\';
    static private final String OPEN_STR = "" + OPEN;
    static private final String CLOSE_STR = "" + CLOSE;
    static private final String DELIMITER_STR = "" + DELIMITER;
    static private final String ESCAPE_OPEN_STR = "" + ESCAPE + OPEN;
    static private final String ESCAPE_CLOSE_STR = "" + ESCAPE + CLOSE;

    // some constants
    static private final String TRUE = "1";
    static private final String FALSE = "0";
    static private final String NULL = "*";
    static private final SubString NULL_SUBSTRING = new SubString(NULL);

    /**
     * Serializes the given object into a string. If the object consists of types that are not supported (see list of
     * supported types), an exception is thrown.
     *
     * @param object The object to serialize
     * @return The resulting serialization string
     * @throws UtilityException if the object cannot be serialized into a string
     */
    static public String serialize(Object object) throws UtilityException {
        var stringBuilder = new StringBuilder();
        Serializer.serialize(object, stringBuilder);
        return stringBuilder.toString();
    }

    /**
     * Deserializes the given string into an object of type {@code T}, which is structurally defined by the specified
     * type reference.
     *
     * @param string        The string to deserialize
     * @param typeReference The type reference for the deserialized object
     * @param <T>           The type of the deserialized object
     * @return The deserialized object
     * @throws UtilityException if the input string cannot be deserialized into an object of type {@code T}
     */
    @SuppressWarnings("unchecked")
    static public <T> T deserialize(String string, TypeReference<T> typeReference) throws UtilityException {
        return (T) Serializer.deserialize(new SubString(string), typeReference);
    }

    /**
     * Deserializes the given string into an object of given class of type {@code T}. The required type reference for
     * the deserialization process is read from the class's static field with the given name. For this method to work,
     * the static field must exist. Otherwise, an exception is thrown.
     *
     * @param string          The string to deserialize
     * @param clazz           The given class
     * @param staticFieldName The name of the class's static field
     * @param <T>             The type of the given class
     * @return The deserialized object
     * @throws UtilityException if the type reference cannot be read from the class's static field, or if the input
     *                          string cannot be deserialized into an object of type {@code T}
     */
    static public <T> T deserialize(String string, Class<T> clazz, String staticFieldName) throws UtilityException {
        try {
            var typeReference = Serializer.getTypeReference(clazz, staticFieldName);
            return Serializer.deserialize(string, typeReference);
        } catch (ReflectiveOperationException exception) {
            throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Static field missing", staticFieldName, clazz);
        }
    }

    // internal deserialization method, which passes a StringBuilder object through the recursion
    static private void serialize(Object object, StringBuilder stringBuilder) {
        switch (object) {
            case null -> serializeNull(stringBuilder);
            // primitive Java types
            case Boolean booleanValue -> serializeBoolean(booleanValue, stringBuilder);
            case Integer integerValue -> serializeInteger(integerValue, stringBuilder);
            case Double doubleValue -> serializeDouble(doubleValue, stringBuilder);
            // non-primitive Java types
            case String string -> serializeString(stringBuilder, string);
            case BigInteger bigInteger -> serializeBigInteger(bigInteger, stringBuilder);
            // utilities types
            case ByteArray byteArray -> serializeByteArray(byteArray, stringBuilder);
            case IntVector intVector -> serializeIntVector(intVector, stringBuilder);
            case IntMatrix intMatrix -> serializeIntMatrix(intMatrix, stringBuilder);
            case IntSet intSet -> serializeIntSet(intSet, stringBuilder);
            case Vector<?> vector -> serializeVector(vector, stringBuilder);
            case Matrix<?> matrix -> serializeMatrix(matrix, stringBuilder);
            case Tuple tuple -> serializeTuple(tuple, stringBuilder);
            case IndexedFamily<?> indexedFamily -> serializeIndexedFamily(indexedFamily, stringBuilder);
            // other Java types
            case Map<?, ?> map -> serializeMap(map, stringBuilder);
            case Map.Entry<?, ?> mapEntry -> serializeMapEntry(mapEntry, stringBuilder);
            case Iterable<?> iterable -> serializeIterable(iterable, stringBuilder);
            // unsupported
            default -> throw new UtilityException(SERIALIZATION_ERROR, Serializer.class, "Type not supported", object);
        }
    }

    static private Object deserialize(SubString subString, TypeReference<?> typeReference) {
        if (subString.equals(NULL_SUBSTRING))
            return null;
        // primitive Java types
        if (typeReference.isSubtype(Boolean.class))
            return deserializeBoolean(subString, typeReference);
        if (typeReference.isSubtype(Integer.class))
            return deserializeInteger(subString, typeReference);
        if (typeReference.isSubtype(Double.class))
            return deserializeDouble(subString, typeReference);
        // Java types
        if (typeReference.isSubtype(String.class))
            return deserializeString(subString, typeReference);
        if (typeReference.isSubtype(BigInteger.class))
            return deserializeBigInteger(subString, typeReference);
        // utilities types
        if (typeReference.isSubtype(ByteArray.class))
            return deserializeByteArray(subString, typeReference);
        if (typeReference.isSubtype(IntVector.class))
            return deserializeIntVector(subString, typeReference);
        if (typeReference.isSubtype(IntMatrix.class))
            return deserializeIntMatrix(subString, typeReference);
        if (typeReference.isSubtype(IntSet.class))
            return deserializeIntSet(subString, typeReference);
        if (typeReference.isSubtype(Vector.class))
            return deserializeVector(subString, typeReference);
        if (typeReference.isSubtype(Matrix.class))
            return deserializeMatrix(subString, typeReference);
        if (typeReference.isSubtype(Tuple.class))
            return deserializeTuple(subString, typeReference);
        if (typeReference.isSubtype(IndexedFamily.class))
            return deserializeIndexedFamily(subString, typeReference);
        // other Java types
        if (typeReference.isSubtype(Map.class))
            return deserializeMap(subString, typeReference);
        if (typeReference.isSubtype(Map.Entry.class))
            return deserializeMapEntry(subString, typeReference);
        if (typeReference.isSubtype(Iterable.class))
            return deserializeIterable(subString, typeReference);
        // unsupported
        throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Unsupported type", subString, typeReference);
    }

    static private void serializeNull(StringBuilder stringBuilder) {
        stringBuilder.append(NULL);
    }

    static private void serializeBoolean(Boolean booleanValue, StringBuilder stringBuilder) {
        stringBuilder.append(booleanValue ? TRUE : FALSE);
    }

    static private void serializeInteger(Integer integerValue, StringBuilder stringBuilder) {
        stringBuilder.append(integerValue);
    }

    static private void serializeDouble(Double doubleValue, StringBuilder stringBuilder) {
        stringBuilder.append(doubleValue);
    }

    static private void serializeBigInteger(BigInteger bigInteger, StringBuilder stringBuilder) {
        stringBuilder.append(bigInteger.toString(16));
    }

    static private void serializeByteArray(ByteArray byteArray, StringBuilder stringBuilder) {
        stringBuilder.append(OPEN_STR).append(byteArray.toString()).append(CLOSE_STR);
    }

    static private void serializeString(StringBuilder stringBuilder, String string) {
        stringBuilder.append(OPEN_STR).append(addEscaping(string)).append(CLOSE_STR);
    }

    static private void serializeIntVector(IntVector intVector, StringBuilder stringBuilder) {
        stringBuilder.append(OPEN_STR);
        serializeInteger(intVector.getMinIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeInteger(intVector.getMaxIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeIterable(intVector, stringBuilder);
        stringBuilder.append(CLOSE_STR);
    }

    static private void serializeIntMatrix(IntMatrix intMatrix, StringBuilder stringBuilder) {
        stringBuilder.append(OPEN_STR);
        serializeInteger(intMatrix.getMinRowIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeInteger(intMatrix.getMaxRowIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeInteger(intMatrix.getMinColIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeInteger(intMatrix.getMaxColIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeIterable(intMatrix, stringBuilder);
        stringBuilder.append(CLOSE_STR);
    }

    static private void serializeIntSet(IntSet intSet, StringBuilder stringBuilder) {
        serializeIterable(intSet, stringBuilder); // delegate to Iterable
    }

    static private void serializeVector(Vector<?> vector, StringBuilder stringBuilder) {
        stringBuilder.append(OPEN_STR);
        serializeInteger(vector.getMinIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeInteger(vector.getMaxIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeIterable(vector, stringBuilder);
        stringBuilder.append(CLOSE_STR);
    }

    static private void serializeMatrix(Matrix<?> matrix, StringBuilder stringBuilder) {
        stringBuilder.append(OPEN_STR);
        serializeInteger(matrix.getMinRowIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeInteger(matrix.getMaxRowIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeInteger(matrix.getMinColIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeInteger(matrix.getMaxColIndex(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        serializeIterable(matrix, stringBuilder);
        stringBuilder.append(CLOSE_STR);
    }

    static private void serializeTuple(Tuple tuple, StringBuilder stringBuilder) {
        serializeIterable(tuple, stringBuilder);  // delegate to Iterable
    }

    static private void serializeIndexedFamily(IndexedFamily<?> indexedFamily, StringBuilder stringBuilder) {
        serializeIterable(indexedFamily, stringBuilder);  // delegate to Iterable
    }

    static private void serializeMap(Map<?, ?> map, StringBuilder stringBuilder) {
        serializeIterable(map.entrySet(), stringBuilder);  // delegate to Iterable
    }

    static private void serializeMapEntry(Map.Entry<?, ?> mapEntry, StringBuilder stringBuilder) {
        stringBuilder.append(OPEN_STR);
        Serializer.serialize(mapEntry.getKey(), stringBuilder);
        stringBuilder.append(DELIMITER_STR);
        Serializer.serialize(mapEntry.getValue(), stringBuilder);
        stringBuilder.append(CLOSE_STR);
    }

    // this method covers multiple classes implementing Iterable such as ArrayList, HashSet, etc.
    static private void serializeIterable(Iterable<?> iterable, StringBuilder stringBuilder) {
        stringBuilder.append(OPEN_STR);
        int oldLength = stringBuilder.length();
        for (var value : iterable) {
            Serializer.serialize(value, stringBuilder);
            stringBuilder.append(DELIMITER_STR);
        }
        int newLength = stringBuilder.length() - 1; // without last comma
        stringBuilder.setLength(Math.max(oldLength, newLength));
        stringBuilder.append(CLOSE_STR);
    }

    static private Boolean deserializeBoolean(SubString subString, TypeReference<?> typeReference) {
        var booleanValue = subString.toString();
        if (booleanValue.equals(TRUE)) {
            return true;
        }
        if (booleanValue.equals(FALSE)) {
            return false;
        }
        throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not a boolean", subString, typeReference);
    }

    static private Integer deserializeInteger(SubString subString) {
        return deserializeInteger(subString, new TypeReference<Integer>() {
        });
    }

    static private Integer deserializeInteger(SubString subString, TypeReference<?> typeReference) {
        var integerValue = subString.toString();
        try {
            return Integer.valueOf(integerValue);
        } catch (NumberFormatException exception) {
            throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not an integer", subString, typeReference);
        }
    }

    static private Double deserializeDouble(SubString subString, TypeReference<?> typeReference) {
        var doubleValue = subString.toString();
        try {
            return Double.valueOf(doubleValue);
        } catch (NumberFormatException exception) {
            throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not a double", subString, typeReference);
        }
    }

    static private BigInteger deserializeBigInteger(SubString subString, TypeReference<?> typeReference) {
        var bigInteger = subString.toString();
        try {
            return new BigInteger(bigInteger, 16);
        } catch (NumberFormatException exception) {
            throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not a bigInteger", subString, typeReference);
        }
    }

    static private ByteArray deserializeByteArray(SubString subString, TypeReference<?> typeReference) {
        var byteArray = subString.toString();
        var length = byteArray.length();
        if (length < 2 || byteArray.charAt(0) != OPEN || byteArray.charAt(length - 1) != CLOSE) {
            throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not a byte array", subString, typeReference);
        }
        try {
            byte[] bytes = hexStringToBytes(byteArray.substring(1, length - 1));
            return new MutableByteArray(bytes);
        } catch (NumberFormatException exception) {
            throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not a byte array", subString, typeReference);
        }
    }

    static private String deserializeString(SubString subString, TypeReference<?> typeReference) {
        var string = subString.toString();
        var length = string.length();
        if (length < 2 || string.charAt(0) != OPEN || string.charAt(length - 1) != CLOSE) {
            throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not an string", subString, typeReference);
        }
        return removeEscaping(string.substring(1, length - 1));
    }

    static private IntVector deserializeIntVector(SubString subString, TypeReference<?> typeReference) {
        var subStrings = subString.split(OPEN, CLOSE, DELIMITER, ESCAPE);
        if (subStrings.size() == 3) {
            int minIndex = deserializeInteger(subStrings.get(0));
            int maxIndex = deserializeInteger(subStrings.get(1));
            int length = maxIndex - minIndex + 1;
            var integers = subStrings.get(2).split(OPEN, CLOSE, DELIMITER, ESCAPE);
            if (integers.size() == length) {
                var builder = new IntVector.Builder(minIndex, maxIndex);
                for (var integer : integers) {
                    builder.add(deserializeInteger(integer));
                }
                return builder.build();
            }
        }
        throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not an intVector", subString, typeReference);
    }

    static private IntMatrix deserializeIntMatrix(SubString subString, TypeReference<?> typeReference) {
        var subStrings = subString.split(OPEN, CLOSE, DELIMITER, ESCAPE);
        if (subStrings.size() == 5) {
            int minRowIndex = deserializeInteger(subStrings.get(0));
            int maxRowIndex = deserializeInteger(subStrings.get(1));
            int minColIndex = deserializeInteger(subStrings.get(2));
            int maxColIndex = deserializeInteger(subStrings.get(3));
            int height = maxRowIndex - minRowIndex + 1;
            int width = maxColIndex - minColIndex + 1;
            var integers = subStrings.get(4).split(OPEN, CLOSE, DELIMITER, ESCAPE);
            if (integers.size() == height * width) {
                var builder = new IntMatrix.Builder(minRowIndex, maxRowIndex, minColIndex, maxColIndex);
                for (var integer : integers) {
                    builder.add(deserializeInteger(integer));
                }
                return builder.build();
            }
        }
        throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not an intMatrix", subString, typeReference);
    }

    static private IntSet deserializeIntSet(SubString subString, TypeReference<?> ignoredTypeReference) {
        var integers = subString.split(OPEN, CLOSE, DELIMITER, ESCAPE);
        var builder = new IntSet.Builder();
        for (var integer : integers) {
            builder.add(deserializeInteger(integer));
        }
        return builder.build();
    }

    static private Vector<?> deserializeVector(SubString subString, TypeReference<?> typeReference) {
        var subStrings = subString.split(OPEN, CLOSE, DELIMITER, ESCAPE);
        if (subStrings.size() == 3) {
            int minIndex = deserializeInteger(subStrings.get(0));
            int maxIndex = deserializeInteger(subStrings.get(1));
            int length = maxIndex - minIndex + 1;
            var values = subStrings.get(2).split(OPEN, CLOSE, DELIMITER, ESCAPE);
            if (values.size() == length) {
                var builder = new Vector.Builder<>(minIndex, maxIndex);
                var typeParameter = typeReference.getTypeParameters().getFirst(); // a vector has a single type parameter
                for (var value : values) {
                    builder.add(Serializer.deserialize(value, typeParameter));
                }
                return builder.build();
            }
        }
        throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not a vector", subString, typeReference);
    }

    static private Matrix<?> deserializeMatrix(SubString subString, TypeReference<?> typeReference) {
        var subStrings = subString.split(OPEN, CLOSE, DELIMITER, ESCAPE);
        if (subStrings.size() == 5) {
            int minRowIndex = deserializeInteger(subStrings.get(0));
            int maxRowIndex = deserializeInteger(subStrings.get(1));
            int minColIndex = deserializeInteger(subStrings.get(2));
            int maxColIndex = deserializeInteger(subStrings.get(3));
            int height = maxRowIndex - minRowIndex + 1;
            int width = maxColIndex - minColIndex + 1;
            var values = subStrings.get(4).split(OPEN, CLOSE, DELIMITER, ESCAPE);
            if (values.size() == height * width) {
                var builder = new Matrix.Builder<>(minRowIndex, maxRowIndex, minColIndex, maxColIndex);
                var typeParameter = typeReference.getTypeParameters().getFirst(); // a matrix has a single type parameter
                for (var value : values) {
                    builder.add(Serializer.deserialize(value, typeParameter));
                }
                return builder.build();
            }
        }
        throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not a matrix", subString, typeReference);
    }

    static private Tuple deserializeTuple(SubString subString, TypeReference<?> typeReference) {
        var tupleTypeReference = typeReference.getTypeReference(Tuple.class).getSubclass();
        if (tupleTypeReference.isPresent()) {
            var typeParameters = tupleTypeReference.get().getTypeParameters();
            try {
                var subStrings = subString.split(OPEN, CLOSE, DELIMITER, ESCAPE);
                var length = subStrings.size();
                if (length == typeParameters.size()) {
                    var values = new Object[length];
                    for (int i : IntSet.range(0, length - 1)) {
                        values[i] = deserialize(subStrings.get(i), typeParameters.get(i));
                    }
                    return (Tuple) Reflection.getInstance(typeReference.getTypeClass(), values);
                }
            } catch (ReflectiveOperationException ignored) {
            }
        }
        throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not a tuple", subString, typeReference);
    }

    static private IndexedFamily<?> deserializeIndexedFamily(SubString subString, TypeReference<?> typeReference) {
        // an indexed family has a single type parameter
        var bindings = subString.split(OPEN, CLOSE, DELIMITER, ESCAPE);
        var builder = new IndexedFamily.Builder<>();
        var typeParameter = typeReference.getTypeParameters().getFirst(); // an indexed family has a single type parameter
        for (var binding : bindings) {
            var indexAndElement = binding.split(OPEN, CLOSE, DELIMITER, ESCAPE);
            if (indexAndElement.size() != 2) {
                throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not a binding", binding, typeReference);
            }
            var index = deserializeInteger(indexAndElement.get(0));
            var element = Serializer.deserialize(indexAndElement.get(1), typeParameter);
            builder.addElement(index, element);
        }
        return builder.build();
    }

    @SuppressWarnings("unchecked")
    static private Map<?, ?> deserializeMap(SubString subString, TypeReference<?> typeReference) {
        try {
            var map = (Map<Object, Object>) Reflection.getInstance(typeReference.getTypeClass());
            subString.split(OPEN, CLOSE, DELIMITER, ESCAPE).stream()
                    .map(entryString -> deserializeMapEntry(entryString, typeReference))
                    .forEach(entry -> map.put(entry.getKey(), entry.getValue()));
            return map;
        } catch (ReflectiveOperationException exception) {
            throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not a map", subString, typeReference);
        }
    }

    @SuppressWarnings("DataFlowIssue")
    static private Map.Entry<Object, Object> deserializeMapEntry(SubString subString, TypeReference<?> typeReference) {
        var typeParameters = typeReference.getTypeParameters();
        var mapEntry = subString.split(OPEN, CLOSE, DELIMITER, ESCAPE);
        if (mapEntry.size() == 2) {
            var key = deserialize(mapEntry.get(0), typeParameters.get(0)); // one for the key
            var value = deserialize(mapEntry.get(1), typeParameters.get(1)); // one for the value
            return Map.entry(key, value);
        }
        throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not a map entry", subString, typeReference);
    }

    // this method covers multiple classes implementing Iterable such as ArrayList, HashSet, etc.
    static private Iterable<?> deserializeIterable(SubString subString, TypeReference<?> typeReference) {
        try {
            var subStrings = subString.split(OPEN, CLOSE, DELIMITER, ESCAPE);
            var values = new ArrayList<>();
            var typeParameter = typeReference.getTypeReference(Iterable.class).getTypeParameters().getFirst(); // an iterable has a single type parameter
            for (var string : subStrings) {
                values.add(Serializer.deserialize(string, typeParameter));
            }
            return (Iterable<?>) Reflection.getInstance(typeReference.getTypeClass(), values);
        } catch (ReflectiveOperationException exception) {
            throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Substring is not iterable", subString, typeReference);
        }
    }

    static private byte[] hexStringToBytes(String str) {
        if (str.length() % 2 != 0) {
            throw new UtilityException(DESERIALIZATION_ERROR, Serializer.class, "Length of hex string needs two be a multiple of 2");
        }
        int length = str.length() / 2;
        byte[] bytes = new byte[length];
        for (int i : IntSet.range(0, length - 1)) {
            bytes[i] = (byte) Integer.parseInt(str.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

    static private String addEscaping(String str) {
        return str.replace(OPEN_STR, ESCAPE_OPEN_STR).replace(CLOSE_STR, ESCAPE_CLOSE_STR);
    }

    static private String removeEscaping(String str) {
        return str.replace(ESCAPE_OPEN_STR, OPEN_STR).replace(ESCAPE_CLOSE_STR, CLOSE_STR);
    }

    @SuppressWarnings("unchecked")
    static private <T> TypeReference<T> getTypeReference(Class<T> serializableClass, String staticFieldName) throws ReflectiveOperationException {
        var declaredField = serializableClass.getDeclaredField(staticFieldName);
        return (TypeReference<T>) declaredField.get(null);
    }

}
