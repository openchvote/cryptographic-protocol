/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples;

import ch.openchvote.base.utilities.UtilityException;

import java.util.stream.Stream;

import static ch.openchvote.base.utilities.UtilityException.Type.NULL_POINTER;

/**
 * This class implements a triple of non-null values of different generic types.
 *
 * @param <V1> The generic type of the first value
 * @param <V2> The generic type of the second value
 * @param <V3> The generic type of the third value
 */
@SuppressWarnings("unused")
public non-sealed class Triple<V1, V2, V3> extends Tuple {

    private final V1 first;
    private final V2 second;
    private final V3 third;

    /**
     * Constructs a new triple for the given non-null values. An exception is thrown if one of the given values is
     * {@code null}.
     *
     * @param first  First value
     * @param second Second value
     * @param third  Third value
     */
    public Triple(V1 first, V2 second, V3 third) {
        if (first == null || second == null || third == null) {
            throw new UtilityException(NULL_POINTER, Triple.class, "Null value not allowed for tuples");
        }
        this.first = first;
        this.second = second;
        this.third = third;
    }

    /**
     * Returns the first element of the triple.
     *
     * @return The first element
     */
    public final V1 getFirst() {
        return this.first;
    }

    /**
     * Returns the second element of the triple.
     *
     * @return The second element
     */
    public final V2 getSecond() {
        return this.second;
    }

    /**
     * Returns the third element of the triple.
     *
     * @return The third element
     */
    public final V3 getThird() {
        return this.third;
    }

    @Override
    public final Stream<Object> toStream() {
        return Stream.builder()
                .add(this.first)
                .add(this.second)
                .add(this.third)
                .build();
    }

}
