/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples;

import ch.openchvote.base.utilities.UtilityException;

import java.util.stream.Stream;

import static ch.openchvote.base.utilities.UtilityException.Type.NULL_POINTER;

/**
 * This class implements a singleton of a non-null value of a generic type.
 *
 * @param <V1> The generic type of the value
 */
@SuppressWarnings("unused")
public non-sealed class Singleton<V1> extends Tuple {

    private final V1 first;

    /**
     * Constructs a new singleton for the given non-null value. An exception is thrown if the given values is
     * {@code null}.
     *
     * @param first First value
     */
    public Singleton(V1 first) {
        if (first == null) {
            throw new UtilityException(NULL_POINTER, Singleton.class, "Null value not allowed for tuples");
        }
        this.first = first;
    }

    /**
     * Returns the first element of the singleton.
     *
     * @return The first element
     */
    public final V1 getFirst() {
        return this.first;
    }

    @Override
    public final Stream<Object> toStream() {
        return Stream.builder()
                .add(this.first)
                .build();
    }

}
