/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tools;

import ch.openchvote.base.utilities.sequence.Vector;

import java.util.Iterator;
import java.util.List;
import java.util.function.IntFunction;
import java.util.stream.Stream;

/**
 * Implementing this interface allows a collection of elements to be transformed into a stream of elements. Based of
 * this stream, the interface offers default methods to transform the collection into a list or an array. By
 * implementing the {@code Iterable} interface, they can also be used in for-each-loops. All implementations of this
 * interface are expected to define the ordering of the elements in the stream in an unambiguous manner.
 *
 *
 * @param <T> The type of the elements in the streamable collection
 **/
@FunctionalInterface
public interface Streamable<T> extends Iterable<T> {

    /**
     * Return a stream containing all the elements of the streamable collection in some order.
     *
     * @return A stream of all elements
     */
    Stream<T> toStream();

    /**
     * Returns a vector containing all elements of the streamable collection in some order.
     *
     * @return A vector of all elements in the same order
     */
    default Vector<T> toVector() {
        return this.toStream().collect(Vector.getCollector());
    }

    /**
     * Returns a list containing all elements of the streamable collection in some order.
     *
     * @return A list of all elements in the same order
     */
    default List<T> toList() {
        return this.toStream().toList();
    }

    /**
     * Returns an array containing all elements of the streamable collection in some order. Type safety of the returned
     * array can not be guaranteed under all circumstances.
     *
     * @return An array of all elements in the same order
     */
    @SuppressWarnings("unchecked")
    default T[] toArray() {
        return (T[]) this.toStream().toArray();
    }

    /**
     * Returns an array containing all elements of the streamable collection in some order. The additional argument
     * {@code generator} of type {@code IntFunction<T[]>} is used to create an array of the right type, similar to the
     * argument {@code generator} in the method {@link Stream#toArray(IntFunction)}. Calling this method is the safest
     * and therefore preferred way of converting a streamable object to an array.
     *
     * @param generator An argument of type {@code IntFunction<T[]>} such as {@code String[]::new} to help the compiler
     *                  constructing an array of the right type.
     * @return An array of all elements in the same order
     */
    default T[] toArray(IntFunction<T[]> generator) {
        return this.toStream().toArray(generator);
    }

    @Override
    default Iterator<T> iterator() {
        return this.toStream().iterator();
    }

}
