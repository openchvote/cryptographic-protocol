/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence.internal;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.Vector;

import static ch.openchvote.base.utilities.UtilityException.Type.INDEX_OUT_OF_BOUNDS;
import static ch.openchvote.base.utilities.UtilityException.Type.INVALID_PARAMETERS;

/**
 * The purpose of this class is to construct instances of {@link Vector} from a given array of values without copying
 * them. This class is for internal use only within this module, i.e., the enclosing package is not exported from this
 * module.
 *
 * @param <V> The generic type of the values stored in the vector
 */
public final class MutableVector<V> extends Vector<V> {

    // array for storing the values of the vector
    private final V[] values;

    /**
     * Constructs a new vector from a given array of values, which is assumed to be safe to use without copying its
     * values. Therefore, this method must be used with maximal care. To minimize misuse, this class is located in the
     * package {@link ch.openchvote.base.utilities.sequence.internal}, which is not exported from this module. The indexing
     * in the newly created vector starts from 1.
     *
     * @param values The given array of values
     */
    public MutableVector(V[] values) {
        this(values, 1, values.length);
    }

    /**
     * Constructs a new vector from a given array of values, like the main constructor
     * {@link MutableVector#MutableVector(V[])}, but for indices starting from {@code minIndex} up to {@code maxIndex}.
     *
     * @param values   The given array of values
     * @param minIndex The minimal index of the new vector
     * @param maxIndex The maximal index of the new vector
     */
    public MutableVector(V[] values, int minIndex, int maxIndex) {
        super(minIndex, maxIndex);
        if (values == null || this.getLength() > values.length) {
            throw new UtilityException(INVALID_PARAMETERS, MutableVector.class, "Invalid length or null argument");
        }
        this.values = values;
    }

    @Override
    public V getValue(int index) {
        if (index < this.minIndex || index > this.maxIndex) {
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, MutableVector.class, "Index smaller than minIndex or larger than maxIndex");
        }
        return this.values == null ? null : this.values[index - this.minIndex];
    }

}
