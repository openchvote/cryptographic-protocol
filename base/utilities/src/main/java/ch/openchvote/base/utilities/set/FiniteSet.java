/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.set;

import ch.openchvote.base.utilities.algebra.GG;
import ch.openchvote.base.utilities.algebra.ZZ;
import ch.openchvote.base.utilities.algebra.ZZPlus;
import ch.openchvote.base.utilities.tools.Streamable;

/**
 * This interface that inherits from {@link Set} represents sets with a finite amount of elements. The elements must
 * have a natural order, which means that their actual type must implement the {@link Comparable} interface. In this,
 * iterating through a finite set can be implemented to always produce the same element order.
 *
 * @param <T> The generic type of the elements contained in the finite set
 * @param <S> The generic type of the size elements (Integer, Long, BigInteger, etc.)
 */
public sealed interface FiniteSet<T extends Comparable<T>, S extends Number> extends Set<T>, Streamable<T> permits GG, ZZ, ZZPlus, Alphabet, IndexedFamily, IntSet {

    /**
     * Returns the size (number of elements) of the finite set.
     *
     * @return The size of the finite set
     */
    S getSize();

    /**
     * Checks if the finite set is empty.
     *
     * @return {@code true}, if the finite set is empty, {@code false} otherwise
     */
    default boolean isEmpty() {
        return this.getSize().longValue() == 0L;
    }

}
