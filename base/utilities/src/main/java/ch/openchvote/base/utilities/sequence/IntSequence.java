/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence;

import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tools.IntBiPredicate;
import ch.openchvote.base.utilities.tools.IntStreamable;

import java.util.OptionalInt;
import java.util.stream.IntStream;

/**
 * Representation of a sequence of {@code int} values that can be enumerated over a range of integer indices.
 */
public sealed interface IntSequence extends IntStreamable permits IntVector, IntMatrix, ByteArray {

    /**
     * Returns the smallest possible index of the sequence.
     *
     * @return the smallest possible index
     */
    int getMinIndex();

    /**
     * Returns the largest possible index of the sequence.
     *
     * @return the largest possible index
     */
    int getMaxIndex();

    /**
     * Returns the value of the sequence at the given index. For an invalid index, an exception is thrown.
     *
     * @param index The index
     * @return The value at this index
     */
    int getValue(int index);

    /**
     * Returns the length of the sequence.
     *
     * @return The length of the sequence
     */
    default int getLength() {
        return this.getMaxIndex() - this.getMinIndex() + 1;
    }

    /**
     * Returns the sequence's set of indices.
     *
     * @return The set of indices
     */
    default IntSet getIndices() {
        return IntSet.range(this.getMinIndex(), this.getMaxIndex());
    }

    /**
     * Returns a stream containing all values of the sequence in ascending index order.
     *
     * @return The stream of values in ascending index order
     */
    default IntStream toIntStream() {
        return this.getIndices().toIntStream().map(this::getValue);
    }

    /**
     * Checks if the int sequence contains the given value.
     *
     * @param value The given value
     * @return {@code true}, if the int sequence contains the given value, {@code false} otherwise
     */
    default boolean contains(int value) {
        return this.toIntStream().anyMatch(v -> v == value);
    }

    /**
     * Checks if applying the given predicate pairwise to all adjacent values of the int sequence returns {@code true}
     * for all pairs of values. This can be useful for example for testing if a sequence is sorted, or if all values of
     * the sequence are equal.
     *
     * @param predicate The given predicate
     * @return {@code true}, if applying the given predicate pairwise to all adjacent values always returns
     * {@code true}, otherwise {@code false}
     */
    default boolean allMatch(IntBiPredicate predicate) {
        return this.getIndices().toIntStream().skip(1).allMatch(index -> predicate.test(this.getValue(index - 1), this.getValue(index)));
    }

    /**
     * Checks if applying the given predicate pairwise to all adjacent values of the int sequence returns {@code true}
     * for at least one pair of values.
     *
     * @param predicate The given predicate
     * @return {@code true}, if applying the given predicate pairwise to all adjacent values returns {@code true} for at
     * least one pair of values, otherwise {@code false}
     */
    default boolean anyMatch(IntBiPredicate predicate) {
        return this.getIndices().toIntStream().skip(1).anyMatch(index -> predicate.test(this.getValue(index - 1), this.getValue(index)));
    }

    /**
     * Computes the sum of all values in the sequence.
     *
     * @return The sum of all values
     */
    default int sum() {
        return this.toIntStream().sum();
    }

    /**
     * Returns an optional containing the smallest value of the sequence.
     *
     * @return An optional containing the smallest value
     */
    default OptionalInt min() {
        return this.toIntStream().min();
    }

    /**
     * Returns an optional containing the largest value of the sequence.
     *
     * @return An optional containing the largest value
     */
    default OptionalInt max() {
        return this.toIntStream().max();
    }

    /**
     * Returns the smallest value of the sequence, or 0 if the sequence is empty.
     *
     * @return The smallest value of the sequence or 0
     */
    default int minOrZero() {
        return this.min().orElse(0);
    }

    /**
     * Returns the largest value of the sequence, or 0 if the sequence is empty.
     *
     * @return The largest value of the sequence or 0
     */
    default int maxOrZero() {
        return this.max().orElse(0);
    }

}
