/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.algebra;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.FiniteSet;
import ch.openchvote.base.utilities.tools.Math;
import ch.openchvote.base.utilities.tools.Streamable;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import static ch.openchvote.base.utilities.UtilityException.Type.INVALID_PARAMETERS;
import static ch.openchvote.base.utilities.UtilityException.Type.NULL_POINTER;

/**
 * This class represents a {@code q}-order subgroup of integers modulo {@code p}, where {@code p} and {@code q} are
 * distinct primes of the form {@code p=kq+1} for {@code k>=2}. The elements of the group are represented by objects of
 * type {@link BigInteger}. The group is a {@link FiniteSet} with a size of {@code q}. Instances of this class are
 * immutable.
 */
public final class GG implements FiniteSet<BigInteger, BigInteger>, Streamable<BigInteger> {

    // a cache of previously constructed instances
    static private final Map<Pair<BigInteger, BigInteger>, GG> CACHE = new ConcurrentHashMap<>();

    // the modulus
    private final BigInteger p;
    // the group order
    private final BigInteger q;
    // the co-factor
    private final BigInteger k;
    // the corresponding ring of integers (used internally to perform the modular operations)
    private final ZZ ZZ_p;

    // private constructor used by factory method
    private GG(BigInteger p, BigInteger q) {
        this.p = p;
        this.q = q;
        this.k = this.p.subtract(BigInteger.ONE).divide(this.q);
        this.ZZ_p = ZZ.of(p);
    }

    /**
     * Returns the {@code q}-order subgroup of integers modulo {@code p}, where {@code p} and {@code q} are distinct
     * primes of the form {@code p=kq+1} for {@code k>=2}.
     *
     * @param p The given prime modulo
     * @param q The prime order of the subgroup
     * @return The {@code q}-order subgroup of integers modulo {@code p}
     */
    static public GG of(BigInteger p, BigInteger q) {
        if (p == null || q == null) {
            throw new UtilityException(NULL_POINTER, GG.class);
        }
        return CACHE.computeIfAbsent(new Pair<>(p, q), key -> {
            if (p.equals(q) || !Math.isProbablePrime(p) || !Math.isProbablePrime(q) || !Math.divides(q, p.subtract(BigInteger.ONE))) {
                throw new UtilityException(INVALID_PARAMETERS, GG.class, "Values p and q must be distinct primed and q must divide p-1");
            }
            return new GG(p, q);
        });
    }

    /**
     * Computes the modular product of two integers {@code x} and {@code y}.
     *
     * @param x The first integer
     * @param y The second integer
     * @return The modular product {@code x * y mod n}
     */
    public BigInteger multiply(BigInteger x, BigInteger y) {
        return this.ZZ_p.multiply(x, y);
    }

    /**
     * Computes {@code x} to the power of {@code y} modulo {@code p}.
     *
     * @param x The base
     * @param y The exponent
     * @return {@code x} to the power of {@code y} modulo {@code p}
     */
    public BigInteger pow(BigInteger x, BigInteger y) {
        return this.ZZ_p.pow(x, y);
    }

    /**
     * Computes the multiplicative inverse of {@code x} modulo {@code p}.
     *
     * @param x The given integer
     * @return The multiplicative inverse of {@code x} modulo {@code p}
     */
    public BigInteger invert(BigInteger x) {
        return this.ZZ_p.invert(x);
    }

    /**
     * Computes the modular division of two integers {@code x} and {@code y}.
     *
     * @param x The first integer
     * @param y The second integer
     * @return The modular division {@code x / y mod n}
     */
    public BigInteger divide(BigInteger x, BigInteger y) {
        return this.ZZ_p.divide(x, y);
    }

    /**
     * Computes the modular product of a vector of integers. Returns 1 if the vector is empty.
     *
     * @param bold_x The vector of integers
     * @return The modular product of the given vector of integers
     */
    public BigInteger prod(Vector<BigInteger> bold_x) {
        return this.ZZ_p.prod(bold_x);
    }

    /**
     * Computes the modular product of the modular powers obtained from combining the values of two equally long vectors
     * of bases and exponents. Returns 1 if the vectors are empty. Tries to delegate the computation to the VMGJ
     * library.
     *
     * @param bold_x The first vector of integers
     * @param bold_y The second vector of integers
     * @return The modular product of powers
     */
    public BigInteger prodPow(Vector<BigInteger> bold_x, Vector<BigInteger> bold_y) {
        return this.ZZ_p.prodPow(bold_x, bold_y);
    }

    @Override
    public boolean contains(BigInteger x) {
        return x != null && x.signum() > 0 && x.compareTo(this.p) < 0 && this.ZZ_p.pow(x, this.q).equals(BigInteger.ONE);
    }

    @Override
    public BigInteger getSize() {
        return this.q;
    }

    @Override
    public Stream<BigInteger> toStream() {
        var generator = BigInteger.TWO.modPow(this.k, this.p);
        return Stream.iterate(BigInteger.ZERO, e -> e.compareTo(this.q) < 0, e -> e.add(BigInteger.ONE)).map(e -> this.pow(generator, e));
    }

}
