/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples.decuple;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.tuples.Tuple;

import java.util.stream.Stream;

import static ch.openchvote.base.utilities.UtilityException.Type.NULL_POINTER;

/**
 * This class implements a 12-tuple (duodecuple) of non-null values of different generic types.
 *
 * @param <V1>  The generic type of the first value
 * @param <V2>  The generic type of the second value
 * @param <V3>  The generic type of the third value
 * @param <V4>  The generic type of the fourth value
 * @param <V5>  The generic type of the fifth value
 * @param <V6>  The generic type of the sixth value
 * @param <V7>  The generic type of the seventh value
 * @param <V8>  The generic type of the eighth value
 * @param <V9>  The generic type of the ninth value
 * @param <V10> The generic type of the tenth value
 * @param <V11> The generic type of the eleventh value
 * @param <V12> The generic type of the twelfth value
 */
@SuppressWarnings("unused")
public non-sealed class DuoDecuple<V1, V2, V3, V4, V5, V6, V7, V8, V9, V10, V11, V12> extends Tuple {

    private final V1 first;
    private final V2 second;
    private final V3 third;
    private final V4 fourth;
    private final V5 fifth;
    private final V6 sixth;
    private final V7 seventh;
    private final V8 eighth;
    private final V9 ninth;
    private final V10 tenth;
    private final V11 eleventh;
    private final V12 twelfth;

    /**
     * Constructs a new duodecuple for the given non-null values. An exception is thrown if one of the given values is
     * {@code null}.
     *
     * @param first    First value
     * @param second   Second value
     * @param third    Third value
     * @param fourth   Fourth value
     * @param fifth    Fifth value
     * @param sixth    Sixth value
     * @param seventh  Seventh value
     * @param eighth   Eighth value
     * @param ninth    Ninth value
     * @param tenth    Tenth value
     * @param eleventh Eleventh value
     * @param twelfth  Twelfth value
     */
    public DuoDecuple(V1 first, V2 second, V3 third, V4 fourth, V5 fifth, V6 sixth, V7 seventh, V8 eighth, V9 ninth, V10 tenth, V11 eleventh, V12 twelfth) {
        if (first == null || second == null || third == null || fourth == null || fifth == null || sixth == null || seventh == null || eighth == null || ninth == null || tenth == null || eleventh == null || twelfth == null) {
            throw new UtilityException(NULL_POINTER, DuoDecuple.class, "Null value not allowed for tuples");
        }
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
        this.fifth = fifth;
        this.sixth = sixth;
        this.seventh = seventh;
        this.eighth = eighth;
        this.ninth = ninth;
        this.tenth = tenth;
        this.eleventh = eleventh;
        this.twelfth = twelfth;
    }

    /**
     * Returns the first element of the duodecuple.
     *
     * @return The first element
     */
    public final V1 getFirst() {
        return this.first;
    }

    /**
     * Returns the second element of the duodecuple.
     *
     * @return The second element
     */
    public final V2 getSecond() {
        return this.second;
    }

    /**
     * Returns the third element of the duodecuple.
     *
     * @return The third element
     */
    public final V3 getThird() {
        return this.third;
    }

    /**
     * Returns the fourth element of the duodecuple.
     *
     * @return The fourth element
     */
    public final V4 getFourth() {
        return this.fourth;
    }

    /**
     * Returns the fifth element of the duodecuple.
     *
     * @return The fifth element
     */
    public final V5 getFifth() {
        return this.fifth;
    }

    /**
     * Returns the sixth element of the duodecuple.
     *
     * @return The sixth element
     */
    public final V6 getSixth() {
        return this.sixth;
    }

    /**
     * Returns the seventh element of the duodecuple.
     *
     * @return The seventh element
     */
    public final V7 getSeventh() {
        return this.seventh;
    }

    /**
     * Returns the eighth element of the duodecuple.
     *
     * @return The eighth element
     */
    public final V8 getEighth() {
        return this.eighth;
    }

    /**
     * Returns the ninth element of the duodecuple.
     *
     * @return The ninth element
     */
    public final V9 getNinth() {
        return this.ninth;
    }

    /**
     * Returns the tenth element of the duodecuple.
     *
     * @return The tenth element
     */
    public final V10 getTenth() {
        return this.tenth;
    }

    /**
     * Returns the eleventh element of the duodecuple.
     *
     * @return The eleventh element
     */
    public final V11 getEleventh() {
        return this.eleventh;
    }

    /**
     * Returns the twelfth element of the duodecuple.
     *
     * @return The twelfth element
     */
    public final V12 getTwelfth() {
        return this.twelfth;
    }

    @Override
    public final Stream<Object> toStream() {
        return Stream.builder()
                .add(this.first)
                .add(this.second)
                .add(this.third)
                .add(this.fourth)
                .add(this.fifth)
                .add(this.sixth)
                .add(this.seventh)
                .add(this.eighth)
                .add(this.ninth)
                .add(this.tenth)
                .add(this.eleventh)
                .add(this.twelfth)
                .build();
    }

}
