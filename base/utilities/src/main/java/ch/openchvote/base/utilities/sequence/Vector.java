/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.internal.MutableVector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tools.Hashable;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ch.openchvote.base.utilities.UtilityException.Type.*;

/**
 * Objects of this generic class represent immutable vectors of a fixed length {@code n}. Their elements are indexed
 * from {@code minIndex} (usually {@code 1}) to {@code maxIndex} (usually {@code n}). Since this class is abstract, it
 * does not offer public constructors. New instances of this class are created using the static member classes
 * {@link Builder} or {@link Builder.IndicesFromZero}. During the building process, values can be added to or placed
 * into an initially empty vector containing {@code null} values. After the building process, accessing the values in
 * the resulting vector is restricted to read-only. The class implements the {@link Hashable} and {@link Sequence}
 * interfaces.
 *
 * @param <V> The generic type of the values stored in the vector
 */
public abstract sealed class Vector<V> extends Hashable implements Sequence<V> permits MutableVector, Vector.NonSealed {

    // the purpose of this protected non-sealed inner class is to allow anonymous subclasses exclusively within this package
    static protected non-sealed abstract class NonSealed<V> extends Vector<V> {

        // protected constructor for internal use in anonymous subclasses
        protected NonSealed(int minIndex, int maxIndex) {
            super(minIndex, maxIndex);
        }

    }

    // the minimal/maximal indices
    protected final int minIndex;
    protected final int maxIndex;

    // protected constructor for internal use in subclasses
    protected Vector(int minIndex, int maxIndex) {
        this.minIndex = minIndex;
        this.maxIndex = maxIndex;
    }

    /**
     * Returns the minimal index of this vector.
     *
     * @return The minimal index
     */
    public int getMinIndex() {
        return this.minIndex;
    }

    /**
     * Returns the maximal index of this vector.
     *
     * @return The maximal index
     */
    public int getMaxIndex() {
        return this.maxIndex;
    }

    /**
     * Creates a new vector by applying a function to each value of the vector. Depending on the specified function, the
     * type {@code W} of the returned vector may be different from the type {@code V} of the original vector.
     * {@code null} values are mapped into {@code null} values. By returning a wrapper object which performs the mapping
     * lazily, this method runs ins constant time.
     *
     * @param function The function that maps the values of the vector
     * @param <W>      The type of the returned vector
     * @return A vector containing all mapped values
     */
    public <W> Vector<W> map(Function<? super V, ? extends W> function) {
        return this.map(function, false);
    }

    /**
     * Creates a new vector by applying a function to each value of the vector. Depending on the specified function, the
     * type {@code W} of the returned vector may be different from the type {@code V} of the original vector. The given
     * boolean value {@code applyToNull} determines whether the specified function is also applied to {@code null}
     * values. Otherwise, {@code null} values are mapped into {@code null} values. By returning a wrapper object which
     * performs the mapping lazily, this method runs ins constant time.
     *
     * @param function    The function that maps the values of the vector
     * @param applyToNull Determines whether the function is applied to {@code null}
     * @param <W>         The type of the returned vector
     * @return A vector containing all mapped values
     */
    public <W> Vector<W> map(Function<? super V, ? extends W> function, boolean applyToNull) {
        return new Vector.NonSealed<>(this.minIndex, this.maxIndex) {
            @Override
            public W getValue(int index) {
                var value = Vector.this.getValue(index);
                return value == null && !applyToNull ? null : function.apply(value);
            }
        };
    }

    /**
     * Creates a new {@code IntVector} by applying a {@code ToIntFunction} to each value of the vector. {@code null}
     * values are mapped into 0. By returning a wrapper object which performs the mapping lazily, this method runs ins
     * constant time.
     *
     * @param function The function that maps the values of the vector
     * @return An integer vector containing all mapped values
     */
    public IntVector mapToInt(ToIntFunction<? super V> function) {
        return new IntVector.NonSealed(this.minIndex, this.maxIndex) {
            @Override
            public int getValue(int index) {
                var value = Vector.this.getValue(index);
                return value == null ? 0 : function.applyAsInt(value);
            }
        };
    }

    /**
     * Creates a new vector by applying a binary function pairwise to the values from this vector of type {@code V} and
     * from another vector of type {@code W}. The given function maps into values of type {@code R}. By returning a
     * wrapper object which performs the mapping lazily, this method runs ins constant time. I
     *
     * @param other      The other vector
     * @param biFunction The binary function that maps the values of the vector
     * @param <W>        The type of the other vector
     * @param <R>        The type of the returned vector
     * @return A vector containing all mapped values
     */
    public <W, R> Vector<R> map(Vector<W> other, BiFunction<V, W, R> biFunction) {
        if (this.minIndex != other.minIndex || this.maxIndex != other.maxIndex) {
            throw new UtilityException(INVALID_PARAMETERS, Vector.class, "MinIndex or maxIndex are not compatible");
        }
        return new Vector.NonSealed<>(this.minIndex, this.maxIndex) {
            @Override
            public R getValue(int index) {
                return biFunction.apply(Vector.this.getValue(index), other.getValue(index));
            }
        };
    }

    /**
     * Creates a new vector by selecting the values specified by the given {@code indexSet}. By taking the given indices
     * in ascending order, the order of the values in the returned vector corresponds to the order in the original
     * vector. The length of the returned vector is equal to the size of {@code indexSet}, and indexing starts at 1.
     *
     * @param indexSet The set of indices of the values to be selected
     * @return A new vector containing the selected values
     */
    public Vector<V> select(IntSet indexSet) {
        if (indexSet == null) {
            throw new UtilityException(NULL_POINTER, Vector.class);
        }
        var indexArray = this.getIndices().toIntStream().filter(indexSet::contains).toArray();
        return new Vector.NonSealed<>(1, indexArray.length) {
            @Override
            public V getValue(int index) {
                return Vector.this.getValue(indexArray[index - 1]);
            }
        };
    }

    /**
     * Creates a new vector by selecting the values specified by the given {@code indexVector}. The length of the
     * resulting vector is therefore equal to the length of the index vector, and indexing starts at 1.
     *
     * @param indexVector The given index vector
     * @return A new vector containing the selected values
     */
    public Vector<V> select(IntVector indexVector) {
        if (indexVector == null)
            throw new UtilityException(NULL_POINTER, Vector.class);
        return indexVector.mapToObj(this::getValue);
    }

    /**
     * Returns a new vector that is identical to the given vector, except for the given new value which is placed at the
     * given index {@code i}.
     *
     * @param i     The index for the new value
     * @param value The new value
     * @return A vector in which the value at the given index has been replaced by a new value
     */
    public Vector<V> replace(int i, V value) {
        if (i < this.minIndex || i > this.maxIndex) {
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, Vector.class, "Index smaller than minIndex or larger than maxIndex");
        }
        return new Vector.NonSealed<>(this.minIndex, this.maxIndex) {
            @Override
            public V getValue(int index) {
                return i == index ? value : Vector.this.getValue(index);
            }
        };
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object instanceof Vector<?> other) {
            if (this.minIndex != other.minIndex) return false;
            if (this.maxIndex != other.maxIndex) return false;
            return this.getIndices().toIntStream().allMatch(index -> Objects.equals(this.getValue(index), other.getValue(index)));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int initValue = 0;
        initValue = 31 * initValue + this.minIndex;
        initValue = 31 * initValue + this.maxIndex;
        return this.toStream().mapToInt(Objects::hashCode).reduce(initValue, (result, hash) -> 31 * result + hash);
    }

    @Override
    public String toString() {
        return this.toStream().map(Objects::toString).collect(Collectors.joining(",", "[", "]"));
    }

    /**
     * This static builder class is the main tool for constructing vectors from scratch. If the length of the vector to
     * construct is defined at the beginning of the building process, then the values are initially all set to
     * {@code null}. If the length is not specified, then the vector grows when new values are added. In both cases,
     * values can be added either incrementally or in arbitrary order. At the end of the building process, the vector
     * can be built exactly once. The builder class is threadsafe and the resulting vector is immutable.
     *
     * @param <V> The generic type of the vector to build
     */
    static public class Builder<V> implements ch.openchvote.base.utilities.tools.Builder<V, Vector<V>> {

        // flag that determines whether the vector has already been built or not
        private boolean built;
        // flag that determines whether the vector length is fixed or not during building process
        private final boolean growable;
        // the first and the last valid indices
        private int minIndex;
        private int maxIndex;
        // an array for storing the added values during the building process (non-growable mode)
        private final V[] values;
        // a map for storing the added values during the building process (growable mode)
        private final SortedMap<Integer, V> valueMap;
        // an internal index counter for adding values incrementally starting from minIndex
        private int nextIndex;

        /**
         * Constructs a builder for a vector with undetermined length. The length of the vector grows automatically to
         * the necessary length when values are added. Indexing starts at 1.
         */
        public Builder() {
            this.built = false;
            this.growable = true;
            this.minIndex = 1;
            this.maxIndex = 0;
            this.nextIndex = 1;
            this.values = null; // not needed in growable mode
            this.valueMap = new TreeMap<>();
        }

        /**
         * Constructs a builder for a vector of fixed {@code length}. Indexing starts from 1.
         *
         * @param length The length of the vector to construct
         */
        public Builder(int length) {
            this(1, length);
        }

        /**
         * Constructs a builder for a vector of fixed length {@code maxIndex-minIndex+1}. Indexing starts from
         * {@code minIndex} and goes up {@code maxIndex}.
         *
         * @param minIndex The minimal index
         * @param maxIndex The maximal index
         */
        @SuppressWarnings("unchecked")
        public Builder(int minIndex, int maxIndex) {
            if (minIndex < 0 || minIndex > maxIndex + 1) {
                throw new UtilityException(INVALID_PARAMETERS, Vector.Builder.class, "Invalid minimal or maximal indices");
            }
            this.built = false;
            this.growable = false;
            this.minIndex = minIndex;
            this.maxIndex = maxIndex;
            this.values = (V[]) new Object[maxIndex - minIndex + 1];
            this.valueMap = null; // not needed in the non-growable mode
            this.nextIndex = minIndex;
        }

        /**
         * Fills up the vector with a single value. In case of a vector with undetermined length, the current length
         * remains unchanged.
         *
         * @param value The value used for filling up the vector
         * @return The vector builder itself
         */
        public Vector.Builder<V> fill(V value) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, Vector.Builder.class);
                }
                for (int i : IntSet.range(this.minIndex, this.maxIndex)) {
                    this.set(i, value);
                }
                return this;
            }
        }

        /**
         * Sets the given value at the given index. If the vector length is undetermined, increases the length if
         * necessary. The vector builder object itself is returned to allow pipeline notation when multiple values are
         * added to the vector.
         *
         * @param index The index of the value to be added
         * @param value The value to be added
         * @return The vector builder itself
         */
        @SuppressWarnings("all")
        public Vector.Builder<V> set(int index, V value) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, Vector.Builder.class);
                }
                if (this.growable) {
                    this.valueMap.put(index, value);
                    this.minIndex = this.valueMap.firstKey();
                    this.maxIndex = this.valueMap.lastKey();
                } else {
                    this.values[index - this.minIndex] = value;
                }
                return this;
            }
        }

        /**
         * Sets the next value in the vector and increases the internal index counter by 1. If the vector length is
         * undetermined, increases the length if necessary. The vector builder object itself is returned to allow
         * pipeline notation when multiple values are added to the vector.
         *
         * @param value The value to be added
         * @return The vector builder itself
         */
        @Override
        @SuppressWarnings("all")
        public Vector.Builder<V> add(V value) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, Vector.Builder.class);
                }
                this.set(this.nextIndex, value);
                this.nextIndex++;
                return this;
            }
        }

        @Override
        @SuppressWarnings("all")
        public Vector<V> build() {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, Vector.Builder.class);
                }
                this.built = true;
                if (this.growable) {
                    return new Vector.NonSealed<>(Builder.this.minIndex, Builder.this.maxIndex) {

                        @Override
                        public V getValue(int index) {
                            return Builder.this.valueMap.get(index);
                        }
                    };
                }
                return new MutableVector<>(this.values, this.minIndex, this.maxIndex);
            }
        }

        /**
         * This special builder class can be used to construct vectors with indices from 0. As such it is a special case
         * of the general {@link Vector.Builder} class and its main purpose is increased convenience.
         *
         * @param <V> The generic type of the vector to build
         */
        static public class IndicesFromZero<V> extends Builder<V> {

            /**
             * Constructs a builder for a vector with indices from 0 to {@code maxIndex}.
             *
             * @param maxIndex The maximal index
             */
            public IndicesFromZero(int maxIndex) {
                super(0, maxIndex);
            }

        }

    }

    /**
     * Returns a {@link Collector} instance for building vectors using the {@link Stream#collect(Collector)} method. The
     * returned collector can not be used in parallel stream processing, because it does not provide a proper combiner.
     *
     * @param <T> The generic type of the vector to be built
     * @return A collector for building vectors
     */
    static public <T> Collector<T, Builder<T>, Vector<T>> getCollector() {
        return Collector.of(Vector.Builder::new, Vector.Builder::add, (l, r) -> null, Vector.Builder::build);
    }

    /**
     * Returns a new vector containing the same values sorted relative to the natural ordering of the given type.
     *
     * @param <V>    The generic type of the values
     * @param vector The input vector to be sorted
     * @return The sorted vector
     */
    static public <V extends Comparable<V>> Vector<V> sort(Vector<V> vector) {
        if (vector == null) {
            throw new UtilityException(NULL_POINTER, Vector.class);
        }
        var builder = new Vector.Builder<V>(vector.getMinIndex(), vector.getMaxIndex());
        vector.toStream().sorted().forEach(builder::add);
        return builder.build();
    }

    /**
     * This static method allows constructing new vectors from a given array of values of type {@code V}. The indexing
     * in the newly created vector starts from 1.
     *
     * @param <V>    The generic type of the values
     * @param values The given array of values
     * @return The constructed vector
     */
    @SafeVarargs
    static public <V> Vector<V> of(V... values) {
        if (values == null) {
            throw new UtilityException(NULL_POINTER, Vector.class);
        }
        var builder = new Vector.Builder<V>(values.length); // non-growable vector builder
        Arrays.stream(values).forEach(builder::add);
        return builder.build();
    }

    /**
     * This static method allows constructing new vectors from a given iterable collection of values of type {@code V}.
     * The indexing in the newly created vector starts from 1 and is defined by the iteration order.
     *
     * @param <V>    The generic type of the values
     * @param values The given iterable collection of values
     * @return The constructed vector
     */
    static public <V> Vector<V> of(Iterable<V> values) {
        if (values == null) {
            throw new UtilityException(NULL_POINTER, Vector.class);
        }
        var builder = new Vector.Builder<V>(); // growable vector builder
        values.forEach(builder::add);
        return builder.build();
    }

    /**
     * This static method allows constructing new vectors from a given set of key-value pairs, where the keys are of
     * type {@code Integer} and the values of type {@code V}. The keys of the map are the indices of the newly created
     * vector, i.e., the indexing of the vector goes from the smallest to the largest key in the map. Keys in between
     * with no entry in the map are interpreted as {@code null} values.
     *
     * @param <V>     The generic type of the values
     * @param entries The given map of (index,value)-pairs
     * @return The constructed vector
     */
    static public <V> Vector<V> of(Set<Map.Entry<Integer, V>> entries) {
        if (entries == null) {
            throw new UtilityException(NULL_POINTER, Vector.class);
        }
        var builder = new Vector.Builder<V>(); // growable vector builder
        entries.forEach(entry -> builder.set(entry.getKey(), entry.getValue()));
        return builder.build();
    }

}
