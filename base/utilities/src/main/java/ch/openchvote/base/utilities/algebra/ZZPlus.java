/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.algebra;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.FiniteSet;
import ch.openchvote.base.utilities.tools.Math;
import ch.openchvote.base.utilities.tools.Streamable;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import static ch.openchvote.base.utilities.UtilityException.Type.INVALID_PARAMETERS;
import static ch.openchvote.base.utilities.UtilityException.Type.NULL_POINTER;

/**
 * This class represents the multiplicative group of absolute values modulo {@code p}, where {@code p=2q+1} is a safe
 * prime such that {@code q} is also prime. The elements of the group are represented by objects of type
 * {@link BigInteger}. The group is a {@link FiniteSet} with a size of {@code q}. Instances of this class are
 * immutable.
 *
 * @see "Rolf Haenni & IlonaStarýKořánová, 'An Alternative Group for Applications of ElGamal in Cryptographic
 * Protocols', E-Vote-ID'23, 8th International Joint Conference, Luxembourg, 2023"
 */
public final class ZZPlus implements FiniteSet<BigInteger, BigInteger>, Streamable<BigInteger> {

    // a cache of previously constructed instances
    static private final Map<BigInteger, ZZPlus> CACHE = new ConcurrentHashMap<>();

    // the group order
    private final BigInteger q;
    // the corresponding ring of integers (used internally to perform the modular operations)
    private final ZZ ZZ_p;

    // private constructor used by factory method
    private ZZPlus(BigInteger p) {
        this.q = Math.div2(p);
        this.ZZ_p = ZZ.of(p);
        // the modulus p needs not to be stored, since it is contained in ZZ_p
    }

    /**
     * Returns the group of positive integers modulo {@code p}, where {@code p} is a safe prime of the form
     * {@code p=2q+1} such that {@code q} is also prime.
     *
     * @param p The given safe prime modulo
     * @return The group of positive integers modulo {@code p}
     */
    static public ZZPlus of(BigInteger p) {
        if (p == null) {
            throw new UtilityException(NULL_POINTER, ZZPlus.class);
        }
        return CACHE.computeIfAbsent(p, key -> {
            if (!Math.isProbableSafePrime(p)) {
                throw new UtilityException(INVALID_PARAMETERS, ZZPlus.class, "Value p must be a safe prime");
            }
            return new ZZPlus(p);
        });
    }

    /**
     * Maps a given integer into the multiplicative group of absolute values modulo {@code p}.
     *
     * @param x The given integer
     * @return The corresponding element of the group
     */
    public BigInteger mapToGroup(BigInteger x) {
        return this.abs(this.ZZ_p.mapToRing(x));
    }

    /**
     * Computes the absolute value of the modular product of two integers {@code x} and {@code y}.
     *
     * @param x The first integer
     * @param y The second integer
     * @return The absolute value of the modular product {@code x * y mod p}
     */
    public BigInteger multiply(BigInteger x, BigInteger y) {
        return this.abs(this.ZZ_p.multiply(x, y));
    }

    /**
     * Computes the absolute value of the modular product of three integers {@code x}, {@code y}, and  {@code z}.
     *
     * @param x The first integer
     * @param y The second integer
     * @param z The thirds integer
     * @return The absolute value of the modular product {@code x * y * z mod p}
     */
    public BigInteger multiply(BigInteger x, BigInteger y, BigInteger z) {
        return this.abs(this.ZZ_p.multiply(x, y, z));
    }

    /**
     * Computes the absolute value of {@code x} to the power of {@code y} modulo {@code p}.
     *
     * @param x The base
     * @param y The exponent
     * @return The absolute value of {@code x} to the power of {@code y} modulo {@code p}
     */
    public BigInteger pow(BigInteger x, BigInteger y) {
        return this.abs(this.ZZ_p.pow(x, y));
    }

    /**
     * Computes the absolute value of the multiplicative inverse of {@code x} modulo {@code p}.
     *
     * @param x The given integer
     * @return The absolute value of the multiplicative inverse of {@code x} modulo {@code p}
     */
    public BigInteger invert(BigInteger x) {
        return this.abs(this.ZZ_p.invert(x));
    }

    /**
     * Computes the absolute value of the modular division of two integers {@code x} and {@code y}.
     *
     * @param x The first integer
     * @param y The second integer
     * @return The absolute value of the modular division {@code x / y mod p}
     */
    public BigInteger divide(BigInteger x, BigInteger y) {
        return this.abs(this.ZZ_p.divide(x, y));
    }

    /**
     * Computes the absolute value of the modular product of a vector of integers. Returns 1 if the vector is empty.
     *
     * @param bold_x The vector of integers
     * @return The absolute value of the modular product of the given vector of integers
     */
    public BigInteger prod(Vector<BigInteger> bold_x) {
        return this.abs(this.ZZ_p.prod(bold_x));
    }

    /**
     * Computes the absolute value of the modular product of powers obtained from combining the values of two equally
     * long vectors of bases and exponents. Returns 1 if the vectors are empty. Tries to delegate the computation to the
     * VMGJ library.
     *
     * @param bold_x The first vector of integers
     * @param bold_y The second vector of integers
     * @return The absolute value of the modular product of powers
     */
    public BigInteger prodPow(Vector<BigInteger> bold_x, Vector<BigInteger> bold_y) {
        return this.abs(this.ZZ_p.prodPow(bold_x, bold_y));
    }

    // private helper method to compute the absolute value for inputs 0 <= x < p
    private BigInteger abs(BigInteger x) {
        if (x.compareTo(this.q) <= 0) {
            return x;
        } else {
            return this.ZZ_p.minus(x);
        }
    }

    @Override
    public boolean contains(BigInteger x) {
        return x != null && x.signum() == 1 && x.compareTo(this.q) <= 0;
    }

    @Override
    public BigInteger getSize() {
        return this.q;
    }

    @Override
    public Stream<BigInteger> toStream() {
        return Stream.iterate(BigInteger.ONE, x -> x.compareTo(this.q) <= 0, x -> x.add(BigInteger.ONE));
    }

}
