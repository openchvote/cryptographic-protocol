/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tools;

import ch.openchvote.base.utilities.UtilityException;

import java.util.Random;

import static ch.openchvote.base.utilities.UtilityException.Type.INVALID_PARAMETERS;

/**
 * This class provides a factory for instances of the Java class {@link Random}. Various modes from the enum class
 * {@link Mode} are supported. Using {@link Mode#RANDOM}, regular {@link Random} objects are returned. Using
 * {@link Mode#DETERMINISTIC}, the returned {@link Random} objects are seeded wit a series of deterministic
 * values starting from 1. Using {@link Mode#IDENTICAL}, the returned {@link Random} objects are all seeded with
 * the same value {@link RandomFactory#DEFAULT_SEED}. Using {@link Mode#MIN} or
 * {@link Mode#MAX}, the returned {@link Random} will always return the smallest respectively the
 * largest value from the range of possible values.
 */
public final class RandomFactory {

    // private no-argument constructor to prevent the creation of instances of a utility class
    private RandomFactory() {
    }

    static private final long DEFAULT_SEED = 0;
    static private long currentSeed = 0;

    /**
     * Return a new instance of the Java class {@link Random}. Its behaviour is determined by the specified mode of the
     * enum class {@link Mode}.
     *
     * @param mode The mode of the returned {@link Random} object
     * @return The constructed {@link Random} object
     */
    static public Random getInstance(Mode mode) {
        return switch (mode) {
            case RANDOM -> new Random();
            case DETERMINISTIC -> {
                currentSeed++;
                yield new Random(currentSeed);
            }
            case IDENTICAL -> new Random(DEFAULT_SEED);
            case MIN -> new Random() {
                @Override
                public double nextDouble() {
                    return 0.0;
                }

                @Override
                public int nextInt(int bound) {
                    if (bound <= 0) {
                        throw new UtilityException(INVALID_PARAMETERS, RandomFactory.class, "Bound must be positive");
                    }
                    return 0;
                }
                // other methods derived from Random are not needed
            };
            case MAX -> new Random() {
                @Override
                public double nextDouble() {
                    return 1.0;
                }

                @Override
                public int nextInt(int bound) {
                    if (bound <= 0) {
                        throw new UtilityException(INVALID_PARAMETERS, RandomFactory.class, "Bound must be positive");
                    }
                    return bound - 1;
                }
                // other methods derived from Random are not needed
            };
            case AVERAGE -> new Random() {
                @Override
                public double nextDouble() {
                    return 0.5;
                }

                @Override
                public int nextInt(int bound) {
                    if (bound <= 0) {
                        throw new UtilityException(INVALID_PARAMETERS, RandomFactory.class, "Bound must be positive");
                    }
                    return bound / 2;
                }
                // other methods derived from Random are not needed
            };
        };
    }

    /**
     * This enum class provides several modes of operation for the {@link Random} objects returned by a
     * {@link RandomFactory}.
     */
    public enum Mode {
        /**
         * For generating random values.
         */
        RANDOM,
        /**
         * For generating randomly looking values in a deterministic way.
         */
        DETERMINISTIC,
        /**
         * For generating identical values.
         */
        IDENTICAL,
        /**
         * For generating constant values (the smallest in the given order).
         */
        MIN,
        /**
         * For generating constant values (the largest in the given order).
         */
        MAX,
        /**
         * For generating constant values (the average of all values).
         */
        AVERAGE
    }
}
