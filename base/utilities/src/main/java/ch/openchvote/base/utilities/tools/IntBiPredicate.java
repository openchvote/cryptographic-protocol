/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tools;

import java.util.function.BiPredicate;

/**
 * Represents a predicate (boolean-valued function) of two {@code int}-valued arguments. This is the
 * {@code int}-consuming primitive type specialization of {@link BiPredicate}. This is a functional interface whose
 * functional method is {@link #test(int, int)}.
 *
 * @see BiPredicate
 */
@FunctionalInterface
public interface IntBiPredicate {

    /**
     * Predicate for testing the strictly smaller relation.
     */
    IntBiPredicate SMALLER = (x, y) -> x < y;

    /**
     * Predicate for testing the smaller or equal relation.
     */
    IntBiPredicate SMALLER_OR_EQUAL = (x, y) -> x <= y;

    /**
     * Predicate for testing equality.
     */
    IntBiPredicate EQUAL = (x, y) -> x == y;

    /**
     * Predicate for testing inequality.
     */
    IntBiPredicate NOT_EQUAL = (x, y) -> x != y;

    /**
     * Predicate for testing the strictly larger relation.
     */
    IntBiPredicate LARGER = (x, y) -> x > y;

    /**
     * Predicate for testing the larger or equal relation.
     */
    IntBiPredicate LARGER_OR_EQUAL = (x, y) -> x >= y;

    /**
     * Evaluates this predicate on the given argument.
     *
     * @param value1 The first input argument
     * @param value2 the second input argument
     * @return {@code true}, if the input arguments match the predicate, otherwise {@code false}
     */
    boolean test(int value1, int value2);

}
