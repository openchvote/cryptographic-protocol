/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence;

import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tools.Streamable;

import java.util.stream.Stream;

/**
 * Representation of a collection of values of type {@code V} that can be enumerated over a range of integer indices.
 *
 * @param <V> The generic type of the sequence
 */
public sealed interface Sequence<V> extends Streamable<V> permits Vector, Matrix, SubString {

    /**
     * Returns the smallest possible index of the sequence.
     *
     * @return the smallest possible index
     */
    int getMinIndex();

    /**
     * Returns the largest possible index of the sequence.
     *
     * @return the largest possible index
     */
    int getMaxIndex();

    /**
     * Returns the value of the sequence at the given index. For an invalid index, an exception is thrown.
     *
     * @param index The index
     * @return The value at this index
     */
    V getValue(int index);

    /**
     * Returns the length of the sequence.
     *
     * @return The length of the sequence
     */
    default int getLength() {
        return this.getMaxIndex() - this.getMinIndex() + 1;
    }

    /**
     * Checks if the sequence is empty.
     *
     * @return {@code true}, if the sequence is empty, {@code false} otherwise
     */
    default boolean isEmpty() {
        return this.getLength() == 0;
    }

    /**
     * Returns the sequence's set of indices.
     *
     * @return The set of indices
     */
    default IntSet getIndices() {
        return IntSet.range(this.getMinIndex(), this.getMaxIndex());
    }

    /**
     * Returns a stream containing all values of the sequence in ascending index order.
     *
     * @return The stream of values in ascending index order
     */
    @Override
    default Stream<V> toStream() {
        return this.getIndices().toIntStream().mapToObj(this::getValue);
    }

    /**
     * Checks if all values of the sequence are identical.
     *
     * @return {@code true}, if all values are identical, {@code false}, otherwise
     */
    default boolean isConstant() {
        return this.toStream().distinct().count() <= 1;
    }

}
