/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tools;

import java.util.Iterator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Implementing this interface allows a collection of {@code int} values to be transformed into a {@code IntStream} of
 * {@code int} values. Based of this stream, the interface offers a default method to transform the collection
 * into an array. By implementing the {@code Iterable} interface, they can also be used in for-each-loops.
 **/
public interface IntStreamable extends Streamable<Integer> {

    /**
     * Returns a stream containing all the integer of the streamable collection of integers in some order.
     *
     * @return A stream of all integers in some order
     */
    IntStream toIntStream();

    /**
     * Returns an array containing all values of the streamable collection in some order.
     *
     * @return The array of all elements in the same order
     */
    default int[] toIntArray() {
        return this.toIntStream().toArray();
    }

    @Override
    default Stream<Integer> toStream() {
        return this.toIntStream().boxed();
    }

    @Override
    default Iterator<Integer> iterator() {
        return this.toIntStream().iterator();
    }

}
