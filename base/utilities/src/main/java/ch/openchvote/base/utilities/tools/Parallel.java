/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tools;

import java.util.function.Consumer;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * The purpose of this class is to provide alternatives to standard Java loops based on parallel streams. The static
 * methods of this class should be called whenever parallel execution of some code may result in significant
 * performance benefits.
 */
public final class Parallel {

    // private no-argument constructor to prevent the creation of instances of a utility class
    private Parallel() {
    }

    /**
     * Parallel execution of a standard for-loop with indices starting from {@code startIndex} to {@code endIndex}. The
     * parameter {@code consumer} defines the code to be executed for each index in the specified range.
     *
     * @param startIndex The first index of the loop
     * @param endIndex   The last index of the loop
     * @param consumer   The task to be executed at each step of the iteration
     */
    static public void forLoop(int startIndex, int endIndex, Consumer<Integer> consumer) {
        forEachLoop(IntStream.rangeClosed(startIndex, endIndex).boxed(), consumer);
    }

    /**
     * Parallel execution of a for-each-loop over the elements of an iterable object. The parameter {@code consumer}
     * defines the code to be executed for each value of the iterable object.
     *
     * @param iterable The iterable object
     * @param consumer The task to be executed for each value of the iterable object
     * @param <V>      The generic type of the values stored in the iterable object
     */
    static public <V> void forEachLoop(Iterable<V> iterable, Consumer<V> consumer) {
        forEachLoop(StreamSupport.stream(iterable.spliterator(), true), consumer);
    }

    /**
     * Parallel execution of a for-each-loop over the elements of a stream. The parameter {@code consumer} defines the
     * code to be executed for each value of the stream.
     *
     * @param stream   The stream
     * @param consumer The task to be executed for each value of the stream
     * @param <V>      The generic type of the values stored in the iterable object
     */
    static public <V> void forEachLoop(Stream<V> stream, Consumer<V> consumer) {
        stream.parallel().forEach(consumer);
    }
}
