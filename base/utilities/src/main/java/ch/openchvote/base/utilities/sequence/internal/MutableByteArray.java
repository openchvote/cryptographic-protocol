/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence.internal;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.ByteArray;

import static ch.openchvote.base.utilities.UtilityException.Type.INDEX_OUT_OF_BOUNDS;

/**
 * The purpose of this class is to construct instances of {@link ByteArray} from a given {@code byte[]} without copying
 * its values. This class is for internal use only within this module, i.e., the enclosing package is not exported from
 * this module.
 */
public final class MutableByteArray extends ByteArray {

    // array for storing the values of the byte array
    private final byte[] bytes;

    /**
     * Constructs a new byte array from a given array of bytes, which is assumed to be safe to use without copying its
     * values. Therefore, this method must be used with maximal care. To minimize misuse, this class is located in the
     * package {@link ch.openchvote.base.utilities.sequence.internal}, which is not exported from this module. The indexing
     * in the newly created byte array starts from 1.
     *
     * @param bytes The given array of bytes
     */
    public MutableByteArray(byte[] bytes) {
        super(bytes.length);
        this.bytes = bytes;
    }

    @Override
    public byte getByte(int index) {
        if (index < 0 || index >= this.length) {
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, MutableByteArray.class, "Negative index or index larger than length");
        }
        return this.bytes[index];
    }

    @Override
    protected void toByteArray(byte[] newBytes, int offset) {
        System.arraycopy(this.bytes, 0, newBytes, offset, this.length);
    }

}
