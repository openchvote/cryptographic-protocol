/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence.internal;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.IntVector;

import static ch.openchvote.base.utilities.UtilityException.Type.INDEX_OUT_OF_BOUNDS;
import static ch.openchvote.base.utilities.UtilityException.Type.INVALID_PARAMETERS;

/**
 * The purpose of this class is to construct instances of {@link IntVector} from a given {@code int[]} without copying
 * its values. This class is for internal use only within this module, i.e., the enclosing package is not exported from
 * this module.
 */
public final class MutableIntVector extends IntVector {

    // array for storing the values of the int array
    private final int[] ints;

    /**
     * Constructs a new vector from a given array of integers, which is assumed to be safe to use without copying its
     * values. Therefore, this method must be used with maximal care. To minimize misuse, this class is located in the
     * package {@link ch.openchvote.base.utilities.sequence.internal}, which is not exported from this module. The indexing in the
     * newly created vector starts from 1.
     *
     * @param ints The given (array of) integers
     */
    public MutableIntVector(int[] ints) {
        this(ints, 1, ints.length);
    }

    /**
     * Constructs a new int vector from a given array of ints, like the main constructor
     * {@link MutableIntVector#MutableIntVector(int[])}, but for indices starting from {@code minIndex} up to
     * {@code maxIndex}.
     *
     * @param ints     The given array of ints
     * @param minIndex The minimal index of the new int vector
     * @param maxIndex The maximal index of the new int vector
     */
    public MutableIntVector(int[] ints, int minIndex, int maxIndex) {
        super(minIndex, maxIndex);
        if (ints == null || this.getLength() > ints.length) {
            throw new UtilityException(INVALID_PARAMETERS, MutableIntVector.class, "Invalid length or null argument");
        }
        this.ints = ints;
    }

    @Override
    public int getValue(int index) {
        if (index < this.minIndex || index > this.maxIndex) {
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, MutableIntVector.class, "Index smaller than minIndex or larger than maxIndex");
        }
        return this.ints[index - this.minIndex];
    }

}
