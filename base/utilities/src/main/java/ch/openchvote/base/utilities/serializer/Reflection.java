/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.serializer;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * This class provides some additional convenience methods for functionalities not provided directly by the standard
 * Java Reflection library. They are particularly useful for deserialization,
 */
public final class Reflection {

    // private no-argument constructor to prevent the creation of instances of a utility class
    private Reflection() {
    }

    /**
     * Returns a suitable constructor of the given class for being called with the given array of arguments. An
     * exception is thrown if no such constructor exists.
     *
     * @param clazz     The given class
     * @param arguments The given arguments
     * @param <T>       The generic type of the specified class
     * @return A suitable constructor for the given arguments
     * @throws ReflectiveOperationException if no constructor exists with the specified number and types of arguments
     */
    @SuppressWarnings("unchecked")
    static public <T> Constructor<T> getConstructor(Class<T> clazz, Object... arguments) throws ReflectiveOperationException {
        var argumentTypes = Arrays.stream(arguments).map(Object::getClass).toArray(Class<?>[]::new);
        var constructor = Arrays.stream(clazz.getDeclaredConstructors())
                .filter(c -> c.getParameterCount() == arguments.length)
                .filter(c -> Reflection.areAssignableFrom(c.getParameterTypes(), argumentTypes))
                .findFirst()
                .orElseThrow(ReflectiveOperationException::new);
        constructor.setAccessible(true);
        return (Constructor<T>) constructor;
    }

    /**
     * Constructs a new instance of the specified class for a given array of arguments. A constructor that matches with
     * the number of arguments is selected and invoked using the {@link Constructor#newInstance(Object...)} method. An
     * exception is thrown if something goes wrong.
     *
     * @param clazz     The given class
     * @param arguments The given array of arguments
     * @param <T>       The generic type of the specified class
     * @return The constructed instances of the given class
     * @throws ReflectiveOperationException if no constructor exists with the specified number and types of arguments
     */
    static public <T> T getInstance(Class<T> clazz, Object... arguments) throws ReflectiveOperationException {
        var constructor = Reflection.getConstructor(clazz, arguments);
        return constructor.newInstance(arguments);
    }

    // tests if all classes from both arrays are mutually assignable from
    static private boolean areAssignableFrom(Class<?>[] classes1, Class<?>[] classes2) {
        return classes1.length == classes2.length &&
                IntStream.range(0, classes1.length).allMatch(i -> classes1[i].isAssignableFrom(classes2[i]));
    }

}
