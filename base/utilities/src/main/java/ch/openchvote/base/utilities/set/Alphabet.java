/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.set;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.tools.Streamable;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static ch.openchvote.base.utilities.UtilityException.Type.*;

/**
 * This class implements the concept of an alphabet, which consists of an ordered finite set of characters. The position
 * of the characters in the order is called rank. The rank of the first character in an alphabet of size n is 0 and the
 * rank of the last element is n-1. By implementing the {@link FiniteSet} interface, an alphabet also inherits the
 * properties of a finite set (of characters).
 */
public sealed class Alphabet implements FiniteSet<Character, Integer>, Streamable<Character> permits Alphabet.UCSAlphabet {

    /**
     * Static factory method to obtain arbitrary alphabets. The characters of the alphabet are given by a string
     * in which each character is contained at most once. The positions of the characters in the string determine their
     * ranks. Internally, all characters are stored in a HashMap, which enables computing their ranks efficiently. The
     * minimal size of an alphabet is 2.
     *
     * @param characters The set of characters given as a string
     * @return The resulting alphabet
     */
    static public Alphabet of(String characters) {
        return new Alphabet(characters);
    }

    static private Alphabet of() {
        return new UCSAlphabet();
    }

    /**
     * UCS alphabet (all Java Unicode characters)
     */
    static public final Alphabet UCS = Alphabet.of();

    /**
     * Binary alphabet (digits 0 and 1)
     */
    static public final Alphabet BASE2 = Alphabet.of("01");

    /**
     * Octal alphabet (digits 0 to 7)
     */
    static public final Alphabet BASE_8 = Alphabet.of("01234567");

    /**
     * Decimal alphabet (digits 0 to 9)
     */
    static public final Alphabet BASE_10 = Alphabet.of("0123456789");

    /**
     * Hexadecimal alphabet (digits 0 to 9, letters A to E)
     */
    static public final Alphabet BASE_16 = Alphabet.of("0123456789ABCDEF");

    /**
     * Fail-safe 32-character alphabet (letters A to Z, digits 2 to 7)
     */
    static public final Alphabet BASE_32 = Alphabet.of("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567");

    /**
     * 64-character alphabet (letters A to Z, a to z, digits 0 to 9, symbols = and /)
     */
    static public final Alphabet BASE_64 = Alphabet.of("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789=/");

    /**
     * 64-character alphabet for URLs (letters A to Z, a to z, digits 0 to 9, symbols - and _), see §5 of RFC4648
     */
    static public final Alphabet BASE_64_URL = Alphabet.of("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_");

    /**
     * Upper-case letters alphabet (digits A to Z)
     */
    static public final Alphabet LATIN_26 = Alphabet.of("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

    /**
     * Upper-case and lower-case letters alphabet (digits A to Z, a to z)
     */
    static public final Alphabet LATIN_52 = Alphabet.of("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");

    /**
     * Extended Latin alphabet with characters used in different Latin languages
     */
    static public final Alphabet LATIN_EXTENDED = Alphabet.of("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽž -'");

    /**
     * Fail-safe alphanumeric alphabet (digits A to Z, 0 to 9, without I, O, 0, 1)
     */
    static public final Alphabet ALPHANUMERIC_32 = Alphabet.of("ABCDEFGHJKLMNPQRSTUVWXYZ23456789");

    /**
     * Alphanumeric alphabet (digits A to Z, 0 to 9)
     */
    static public final Alphabet ALPHANUMERIC_36 = Alphabet.of("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");

    /**
     * Fail-safe alphanumeric alphabet (digits A to Z, a to z, 0 to 9, without I, O, l, 0, 1)
     */
    static public final Alphabet ALPHANUMERIC_57 = Alphabet.of("ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz23456789");

    /**
     * Alphanumeric alphabet (digits A to Z, a to z, 0 to 9)
     */
    static public final Alphabet ALPHANUMERIC_62 = Alphabet.of("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

    private Map<Character, Integer> charMap;
    private String characters;

    // private no-argument constructor used by Alphabet.UCS subclass
    private Alphabet() {
    }

    // private constructor used by factory method
    Alphabet(String characters) {
        if (characters == null || characters.length() < 2) {
            throw new UtilityException(INVALID_LENGTH, Alphabet.class, "Alphabets of size smaller than 2 are not allowed");
        }
        this.charMap = new HashMap<>(characters.length());
        for (int index : IntSet.range(0, characters.length() - 1)) {
            this.charMap.put(characters.charAt(index), index);
        }
        if (this.charMap.size() != characters.length()) {
            throw new UtilityException(DUPLICATE_CHARACTER, Alphabet.class);
        }
        this.characters = characters;
    }

    /**
     * Returns all characters as a string in ascending rank order.
     *
     * @return All characters of the alphabet
     */
    public String getCharacters() {
        return this.characters;
    }

    /**
     * Returns the rank of the character in the alphabet. If the character is not an element of the alphabet, an
     * exception is thrown.
     *
     * @param c The character
     * @return The rank of the character
     */
    public int getRank(char c) {
        if (!this.charMap.containsKey(c)) {
            throw new UtilityException(UNKNOWN_CHARACTER, Alphabet.class);
        }
        return this.charMap.get(c);
    }

    /**
     * Returns the character with the given rank in the alphabet. For invalid ranks, an exception is thrown.
     *
     * @param rank The rank
     * @return The character with the rank
     */
    public char getChar(int rank) {
        if (rank < 0 || rank >= this.characters.length()) {
            throw new UtilityException(RANK_OUT_OF_BOUNDS, Alphabet.class, "Negative rank or rank larger than length");
        }
        return this.characters.charAt(rank);
    }

    /**
     * Adds a new character to an alphabet. The returned alphabet is a new object, which has been created without
     * modifying the given alphabet.
     *
     * @param c The character to be added
     * @return The extended alphabet
     */
    public Alphabet addCharacter(char c) {
        if (this.contains(c)) {
            return this;
        } else {
            return Alphabet.of(this.characters + c);
        }
    }

    /**
     * Checks if all characters from a given string are elements of the alphabet. This method complements the {@link
     * Set#containsAll} methods from the {@link Set} interface.
     *
     * @param s The string
     * @return {@code true}, if all characters from {@code s} are in the alphabet, {@code false} otherwise
     */
    public boolean containsAll(String s) {
        return this.containsAll(s.chars().mapToObj(c -> (char) c));
    }

    @Override
    public boolean contains(Character c) {
        return this.charMap.containsKey(c);
    }

    @Override
    public Integer getSize() {
        return this.characters.length();
    }

    @Override
    public Stream<Character> toStream() {
        return IntStream.range(0, this.characters.length()).mapToObj(this.characters::charAt);
    }

    // subclass for the special case of the UCS alphabet
    static protected final class UCSAlphabet extends Alphabet {

        @Override
        public String getCharacters() {
            throw new UtilityException(UNSUPPORTED_OPERATION, Alphabet.class, "Exact UCS character set unknown");
        }

        @Override
        public Integer getSize() {
            throw new UtilityException(UNSUPPORTED_OPERATION, Alphabet.class, "Size of UCS alphabet unknown");
        }

        @Override
        public boolean contains(Character c) {
            return true;
        }

        @Override
        public int getRank(char c) {
            return c;
        }

        @Override
        public char getChar(int rank) {
            return (char) rank;
        }

    }

}
