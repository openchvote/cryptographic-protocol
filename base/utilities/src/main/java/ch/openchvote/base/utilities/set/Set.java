/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.set;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.*;
import ch.openchvote.base.utilities.tuples.*;
import ch.openchvote.base.utilities.tuples.decuple.*;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.stream.Stream;

import static ch.openchvote.base.utilities.UtilityException.Type.*;

/**
 * This is a functional interface for implementing set membership tests in a simple and consistent way for different
 * types of sets. Classes implementing this interface are therefore not to be understood as data structures such as
 * {@link java.util.HashSet} or {@link java.util.TreeSet}. Based on the basic membership test implemented by
 * {@link Set#contains}, the interface offers default methods for performing membership tests over all elements from a
 * given stream, vector, matrix, or array.
 *
 * @param <T> The generic type of the elements contained in the set
 */
@FunctionalInterface
public interface Set<T> {

    /**
     * Performs the membership test on a single element {@code x} of the given generic type. Returns {@code true}, if
     * {@code x} is a member of the set, and {@code false} otherwise.
     *
     * @param x The given object
     * @return {@code true}, if {@code x} is a member of the set, {@code false} otherwise
     */
    boolean contains(T x);

    /**
     * Performs the membership test on all elements of a given stream. Returns {@code true}, if all element are members
     * of the set. If the stream itself is {@code null}, the membership test returns {@code false}.
     *
     * @param stream The given stream of elements
     * @return {@code true}, if all elements are members of the set, {@code false} otherwise
     */
    default boolean containsAll(Stream<? extends T> stream) {
        return stream != null && stream.allMatch(this::contains);
    }

    /**
     * Performs the membership test on all elements of a given iterable collection. Returns {@code true}, if all
     * elements are members of the set. If the iterable collection itself is {@code null}, the membership test returns
     * {@code false}.
     *
     * @param elements The given iterable collection of elements
     * @return {@code true}, if all elements are members of the set, {@code false} otherwise
     */
    default boolean containsAll(Iterable<? extends T> elements) {
        if (elements == null) {
            return false;
        }
        for (var element : elements) {
            if (!this.contains(element)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Extends the given set with the element {@code null}.
     *
     * @return The set extended with {@code null}
     */
    default Set<T> orNull() {
        return x -> x == null || Set.this.contains(x);
    }

    /**
     * The set of non-negative integers {0,1,2,...}.
     */
    Set<BigInteger> NN = x -> x != null && x.signum() >= 0;

    /**
     * The set of positive integers {1,2,3,...}.
     */
    Set<BigInteger> NN_plus = x -> x != null && x.signum() > 0;

    /**
     * Returns the universal set of all elements of a given type.
     *
     * @param <E>          The generic type of the elements of a given type.
     * @param ignoredElementClass The class representing the type of the elements
     * @return The universal set of all elements
     */
    static <E> Set<E> universal(Class<E> ignoredElementClass) {
        return element -> true;
    }

    /**
     * Returns the set of all permutations of length {@code n}. Permutations are represented by integer vectors
     * containing the integers {@code 1,...,n} in permuted order.
     *
     * @param n The given length
     * @return The set of permutations of length {@code n}
     */
    static Set<IntVector> Psi(int n) {
        if (n < 0) {
            throw new UtilityException(INVALID_LENGTH, Set.class, "Negative length not allowed");
        }
        return phi -> phi != null
                && phi.getLength() == n
                && phi.toIntStream().allMatch(x -> 1 <= x && x <= n)
                && phi.toIntStream().distinct().count() == n;
    }

    /**
     * Returns the set of all byte arrays of length {@code length}.
     *
     * @param length The given length
     * @return The set of byte arrays of length {@code length}
     */
    static Set<ByteArray> B(int length) {
        if (length < 0) {
            throw new UtilityException(INVALID_LENGTH, Set.class, "Negative length not allowed");
        }
        return byteArray -> byteArray != null && byteArray.getLength() == length;
    }

    /**
     * The set of all byte arrays of arbitrary length.
     */
    Set<ByteArray> B_star = Objects::nonNull;

    /**
     * The set of all UCS strings.
     */
    Set<String> UCS_star = string -> string != null && Alphabet.UCS.containsAll(string);

    /**
     * Returns the set of all UCS strings of length {@code n}.
     *
     * @param length The given length
     * @return The set of strings of length {@code n}
     */
    static Set<String> UCS(int length) {
        return Set.String(Alphabet.UCS, length);
    }

    /**
     * Returns the set of all strings of arbitrary length with characters from the given alphabet.
     *
     * @param alphabet The given alphabet
     * @return The set of strings of with characters from the given alphabet
     */
    static Set<String> String(Alphabet alphabet) {
        if (alphabet == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return string -> string != null && alphabet.containsAll(string);
    }

    /**
     * Returns the set of all strings of length {@code length} with characters from the given alphabet.
     *
     * @param alphabet The given alphabet
     * @param length   The given length
     * @return The set of strings of length {@code length} with characters from the given alphabet
     */
    static Set<String> String(Alphabet alphabet, int length) {
        if (alphabet == null || length < 0) {
            throw new UtilityException(INVALID_PARAMETERS, Set.class, "Invalid length or null argument");
        }
        return string -> string != null
                && string.length() == length
                && alphabet.containsAll(string);
    }

    /**
     * Returns the set of all strings of length greater or equal to {@code minLength} and smaller or equal to
     * {@code maxLength} with characters from the given alphabet.
     *
     * @param alphabet  The given alphabet
     * @param minLength The given minimal length
     * @param maxLength The given maximal length
     * @return The set of strings of length {@code minLength <= n <= maxLength} with characters from the given alphabet
     */
    static Set<String> String(Alphabet alphabet, int minLength, int maxLength) {
        if (alphabet == null || minLength < 0 || maxLength < 0) {
            throw new UtilityException(INVALID_PARAMETERS, Set.class, "Invalid lengths or null argument");
        }
        return string -> string != null
                && string.length() >= minLength
                && string.length() <= maxLength
                && alphabet.containsAll(string);
    }

    /**
     * The set of all byte arrays representing a valid UTF8 encoding.
     */
    Set<ByteArray> UTF8 = B -> {
        var decoder = StandardCharsets.UTF_8.newDecoder();
        decoder.onMalformedInput(CodingErrorAction.REPORT);
        decoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        try {
            decoder.decode(ByteBuffer.wrap(B.toByteArray()));
            return true;
        } catch (CharacterCodingException exception) {
            return false;
        }
    };

    /**
     * Returns the set of all vectors with elements from the given set.
     *
     * @param set The given set
     * @param <T> The generic type of both the vector and the set
     * @return The set of all vectors with elements from the given set
     */
    static <T> Set<Vector<? extends T>> Vector(Set<T> set) {
        if (set == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return vector -> vector.toStream().allMatch(set::contains);
    }

    /**
     * Returns the set of all vectors of length {@code length} with elements from the given set.
     *
     * @param set    The given set
     * @param length The length of the vectors
     * @param <T>    The generic type of both the vectors and the set
     * @return The set of all vectors of length {@code length} with elements from the given set
     */
    static <T> Set<Vector<? extends T>> Vector(Set<T> set, int length) {
        if (set == null || length < 0) {
            throw new UtilityException(INVALID_PARAMETERS, Set.class, "Invalid length or null argument");
        }
        return vector -> (vector.getLength() == length) && vector.toStream().allMatch(set::contains);
    }

    /**
     * Returns the set of all vectors of length between {@code minLength} and {@code maxLength} and with elements from
     * the given set.
     *
     * @param set       The given set
     * @param minLength The minimal length of the vectors
     * @param maxLength The maximal length of the vectors
     * @param <T>       The generic type of both the vectors and the set
     * @return The set of all vectors of length between {@code minLength} and {@code maxLength} and with elements from
     * the given set
     */
    static <T> Set<Vector<? extends T>> Vector(Set<T> set, int minLength, int maxLength) {
        if (set == null || minLength < 0 || minLength > maxLength) {
            throw new UtilityException(INVALID_PARAMETERS, Set.class, "Invalid length or null argument");
        }
        return vector -> (vector.getLength() >= minLength) && (vector.getLength() <= maxLength) && vector.toStream().allMatch(set::contains);
    }

    /**
     * Returns the set of all matrices of the given height and width with elements from the given set.
     *
     * @param set    The given set
     * @param height The height of the matrices
     * @param width  The width of the matrices
     * @param <T>    The generic type of both the matrices and the set
     * @return The set of all matrices of the given height and width with elements from the given set
     */
    static <T> Set<Matrix<? extends T>> Matrix(Set<T> set, int height, int width) {
        if (set == null || height < 0 || width < 0) {
            throw new UtilityException(INVALID_HEIGHT_OR_WIDTH, Set.class);
        }
        return matrix -> (matrix.getHeight() == height) && (matrix.getWidth() == width) && matrix.toStream().allMatch(set::contains);
    }

    /**
     * Returns the set of all matrices of height and width between the given limits and with elements from the given
     * set.
     *
     * @param set       The given set
     * @param minHeight The minimal height of the matrices
     * @param maxHeight The maximal height of the matrices
     * @param minWidth  The minimal width of the matrices
     * @param maxWidth  The maximal width of the matrices
     * @param <T>       The generic type of both the matrices and the set
     * @return The set of all matrices of height and width between the given limits with elements from the given set
     */
    static <T> Set<Matrix<? extends T>> Matrix(Set<T> set, int minHeight, int maxHeight, int minWidth, int maxWidth) {
        if (set == null || minHeight < 0 || minHeight > maxHeight || minWidth < 0 || minWidth > maxWidth) {
            throw new UtilityException(INVALID_HEIGHT_OR_WIDTH, Set.class);
        }
        return matrix -> (matrix.getHeight() >= minHeight) && (matrix.getHeight() <= maxHeight)
                && (matrix.getWidth() >= minWidth) && (matrix.getWidth() <= maxWidth) && matrix.toStream().allMatch(set::contains);
    }

    /**
     * Returns the set of all {@code int} vectors of length {@code length} with elements from the given {@code int}
     * set.
     *
     * @param set    The given {@code int} set
     * @param length The length of the {@code int} vectors
     * @return The set of all {@code int} vectors of length {@code length} with elements from the given {@code int} set
     */
    static Set<IntVector> IntVector(IntSet set, int length) {
        if (set == null || length < 0) {
            throw new UtilityException(INVALID_PARAMETERS, Set.class, "Invalid length or null argument");
        }
        return vector -> (vector.getLength() == length) && set.containsAll(vector);
    }

    /**
     * Returns the set of all {@code int} matrices of the given height and width with elements from the given
     * {@code int} set.
     *
     * @param set    The given {@code int} set
     * @param height The height of the matrices
     * @param width  The width of the matrices
     * @return The set of all {@code int} matrices of the given height and width with elements from the given
     * {@code int} set
     */
    static Set<IntMatrix> IntMatrix(IntSet set, int height, int width) {
        if (set == null || height < 0 || width < 0) {
            throw new UtilityException(INVALID_HEIGHT_OR_WIDTH, Set.class);
        }
        return matrix -> (matrix.getHeight() == height) && (matrix.getWidth() == width) && set.containsAll(matrix);
    }

    /**
     * Returns the set of all indexed families with indices from {@code indexSet} and elements from {@code elementSet}.
     *
     * @param indexSet   The given index set
     * @param elementSet The given element set
     * @param <E>        The generic type of the elements
     * @return The set of all indexed families
     */
    static <E> Set<IndexedFamily<? extends E>> IndexedFamily(IntSet indexSet, Set<E> elementSet) {
        if (indexSet == null || elementSet == null) {
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, Set.class);
        }
        return indexedFamily -> indexedFamily.getSize().longValue() == indexedFamily.getIndices().getSize().longValue() // no identical indices
                && indexSet.containsAll(indexedFamily.getIndices())
                && elementSet.containsAll(indexedFamily.getElements());
    }

    /**
     * Creates the Cartesian product of two input sets.
     *
     * @param set1 The first set
     * @param set2 The second set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @return The Cartesian product of two sets
     */
    @SuppressWarnings("unused")
    static <T1, T2> Set<Pair<? extends T1, ? extends T2>> Pair(Set<T1> set1, Set<T2> set2) {
        if (set1 == null || set2 == null) {
            throw new UtilityException(NULL_POINTER, Singleton.class);
        }
        return pair -> set1.contains(pair.getFirst()) && set2.contains(pair.getSecond());
    }

    /**
     * Creates the Cartesian product of three input sets.
     *
     * @param set1 The first set
     * @param set2 The second set
     * @param set3 The third set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @param <T3> The generic type of the third set
     * @return The Cartesian product of three sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3> Set<Triple<? extends T1, ? extends T2, ? extends T3>> Triple(Set<T1> set1, Set<T2> set2, Set<T3> set3) {
        if (set1 == null || set2 == null || set3 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return triple -> set1.contains(triple.getFirst())
                && set2.contains(triple.getSecond())
                && set3.contains(triple.getThird());
    }

    /**
     * Creates the Cartesian product of four input sets.
     *
     * @param set1 The first set
     * @param set2 The second set
     * @param set3 The third set
     * @param set4 The fourth set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @param <T3> The generic type of the third set
     * @param <T4> The generic type of the fourth set
     * @return The Cartesian product of four sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3, T4> Set<Quadruple<? extends T1, ? extends T2, ? extends T3, ? extends T4>> Quadruple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4) {
        if (set1 == null || set2 == null || set3 == null || set4 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return quadruple -> set1.contains(quadruple.getFirst())
                && set2.contains(quadruple.getSecond())
                && set3.contains(quadruple.getThird())
                && set4.contains(quadruple.getFourth());
    }

    /**
     * Creates the Cartesian product of five input sets.
     *
     * @param set1 The first set
     * @param set2 The second set
     * @param set3 The third set
     * @param set4 The fourth set
     * @param set5 The fifth set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @param <T3> The generic type of the third set
     * @param <T4> The generic type of the fourth set
     * @param <T5> The generic type of the fifth set
     * @return The Cartesian product of five sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3, T4, T5> Set<Quintuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5>> Quintuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return quintuple -> set1.contains(quintuple.getFirst())
                && set2.contains(quintuple.getSecond())
                && set3.contains(quintuple.getThird())
                && set4.contains(quintuple.getFourth())
                && set5.contains(quintuple.getFifth());
    }

    /**
     * Creates the Cartesian product of six input sets.
     *
     * @param set1 The first set
     * @param set2 The second set
     * @param set3 The third set
     * @param set4 The fourth set
     * @param set5 The fifth set
     * @param set6 The sixth set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @param <T3> The generic type of the third set
     * @param <T4> The generic type of the fourth set
     * @param <T5> The generic type of the fifth set
     * @param <T6> The generic type of the sixth set
     * @return The Cartesian product of six sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3, T4, T5, T6> Set<Sextuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5, ? extends T6>> Sextuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5, Set<T6> set6) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null || set6 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return sextuple -> set1.contains(sextuple.getFirst())
                && set2.contains(sextuple.getSecond())
                && set3.contains(sextuple.getThird())
                && set4.contains(sextuple.getFourth())
                && set5.contains(sextuple.getFifth())
                && set6.contains(sextuple.getSixth());
    }

    /**
     * Creates the Cartesian product of seven input sets.
     *
     * @param set1 The first set
     * @param set2 The second set
     * @param set3 The third set
     * @param set4 The fourth set
     * @param set5 The fifth set
     * @param set6 The sixth set
     * @param set7 The seventh set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @param <T3> The generic type of the third set
     * @param <T4> The generic type of the fourth set
     * @param <T5> The generic type of the fifth set
     * @param <T6> The generic type of the sixth set
     * @param <T7> The generic type of the seventh set
     * @return The Cartesian product of seven sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3, T4, T5, T6, T7> Set<Septuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5, ? extends T6, ? extends T7>> Septuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5, Set<T6> set6, Set<T7> set7) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null || set6 == null || set7 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return septuple -> set1.contains(septuple.getFirst())
                && set2.contains(septuple.getSecond())
                && set3.contains(septuple.getThird())
                && set4.contains(septuple.getFourth())
                && set5.contains(septuple.getFifth())
                && set6.contains(septuple.getSixth())
                && set7.contains(septuple.getSeventh());
    }

    /**
     * Creates the Cartesian product of eight input sets.
     *
     * @param set1 The first set
     * @param set2 The second set
     * @param set3 The third set
     * @param set4 The fourth set
     * @param set5 The fifth set
     * @param set6 The sixth set
     * @param set7 The seventh set
     * @param set8 The eight set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @param <T3> The generic type of the third set
     * @param <T4> The generic type of the fourth set
     * @param <T5> The generic type of the fifth set
     * @param <T6> The generic type of the sixth set
     * @param <T7> The generic type of the seventh set
     * @param <T8> The generic type of the eight set
     * @return The Cartesian product of eight sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3, T4, T5, T6, T7, T8> Set<Octuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5, ? extends T6, ? extends T7, ? extends T8>> Octuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5, Set<T6> set6, Set<T7> set7, Set<T8> set8) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null || set6 == null || set7 == null || set8 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return octuple -> set1.contains(octuple.getFirst())
                && set2.contains(octuple.getSecond())
                && set3.contains(octuple.getThird())
                && set4.contains(octuple.getFourth())
                && set5.contains(octuple.getFifth())
                && set6.contains(octuple.getSixth())
                && set7.contains(octuple.getSeventh())
                && set8.contains(octuple.getEighth());
    }

    /**
     * Creates the Cartesian product of nine input sets.
     *
     * @param set1 The first set
     * @param set2 The second set
     * @param set3 The third set
     * @param set4 The fourth set
     * @param set5 The fifth set
     * @param set6 The sixth set
     * @param set7 The seventh set
     * @param set8 The eight set
     * @param set9 The ninth set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @param <T3> The generic type of the third set
     * @param <T4> The generic type of the fourth set
     * @param <T5> The generic type of the fifth set
     * @param <T6> The generic type of the sixth set
     * @param <T7> The generic type of the seventh set
     * @param <T8> The generic type of the eight set
     * @param <T9> The generic type of the ninth set
     * @return The Cartesian product of nine sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3, T4, T5, T6, T7, T8, T9> Set<Nonuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5, ? extends T6, ? extends T7, ? extends T8, ? extends T9>> Nonuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5, Set<T6> set6, Set<T7> set7, Set<T8> set8, Set<T9> set9) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null || set6 == null || set7 == null || set8 == null || set9 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return nonuple -> set1.contains(nonuple.getFirst())
                && set2.contains(nonuple.getSecond())
                && set3.contains(nonuple.getThird())
                && set4.contains(nonuple.getFourth())
                && set5.contains(nonuple.getFifth())
                && set6.contains(nonuple.getSixth())
                && set7.contains(nonuple.getSeventh())
                && set8.contains(nonuple.getEighth())
                && set9.contains(nonuple.getNinth());
    }

    /**
     * Creates the Cartesian product of ten input sets.
     *
     * @param set1  The first set
     * @param set2  The second set
     * @param set3  The third set
     * @param set4  The fourth set
     * @param set5  The fifth set
     * @param set6  The sixth set
     * @param set7  The seventh set
     * @param set8  The eight set
     * @param set9  The ninth set
     * @param set10 The tenth set
     * @param <T1>  The generic type of the first set
     * @param <T2>  The generic type of the second set
     * @param <T3>  The generic type of the third set
     * @param <T4>  The generic type of the fourth set
     * @param <T5>  The generic type of the fifth set
     * @param <T6>  The generic type of the sixth set
     * @param <T7>  The generic type of the seventh set
     * @param <T8>  The generic type of the eight set
     * @param <T9>  The generic type of the ninth set
     * @param <T10> The generic type of the tenth set
     * @return The Cartesian product of ten sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> Set<Decuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5, ? extends T6, ? extends T7, ? extends T8, ? extends T9, ? extends T10>> Decuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5, Set<T6> set6, Set<T7> set7, Set<T8> set8, Set<T9> set9, Set<T10> set10) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null || set6 == null || set7 == null || set8 == null || set9 == null || set10 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return decuple -> set1.contains(decuple.getFirst())
                && set2.contains(decuple.getSecond())
                && set3.contains(decuple.getThird())
                && set4.contains(decuple.getFourth())
                && set5.contains(decuple.getFifth())
                && set6.contains(decuple.getSixth())
                && set7.contains(decuple.getSeventh())
                && set8.contains(decuple.getEighth())
                && set9.contains(decuple.getNinth())
                && set10.contains(decuple.getTenth());
    }

    /**
     * Creates the Cartesian product of eleven input sets.
     *
     * @param set1  The first set
     * @param set2  The second set
     * @param set3  The third set
     * @param set4  The fourth set
     * @param set5  The fifth set
     * @param set6  The sixth set
     * @param set7  The seventh set
     * @param set8  The eight set
     * @param set9  The ninth set
     * @param set10 The tenth set
     * @param set11 The eleventh set
     * @param <T1>  The generic type of the first set
     * @param <T2>  The generic type of the second set
     * @param <T3>  The generic type of the third set
     * @param <T4>  The generic type of the fourth set
     * @param <T5>  The generic type of the fifth set
     * @param <T6>  The generic type of the sixth set
     * @param <T7>  The generic type of the seventh set
     * @param <T8>  The generic type of the eight set
     * @param <T9>  The generic type of the ninth set
     * @param <T10> The generic type of the tenth set
     * @param <T11> The generic type of the eleventh set
     * @return The Cartesian product of eleven sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> Set<UnDecuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5, ? extends T6, ? extends T7, ? extends T8, ? extends T9, ? extends T10, ? extends T11>> UnDecuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5, Set<T6> set6, Set<T7> set7, Set<T8> set8, Set<T9> set9, Set<T10> set10, Set<T11> set11) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null || set6 == null || set7 == null || set8 == null || set9 == null || set10 == null || set11 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return unDecuple -> set1.contains(unDecuple.getFirst())
                && set2.contains(unDecuple.getSecond())
                && set3.contains(unDecuple.getThird())
                && set4.contains(unDecuple.getFourth())
                && set5.contains(unDecuple.getFifth())
                && set6.contains(unDecuple.getSixth())
                && set7.contains(unDecuple.getSeventh())
                && set8.contains(unDecuple.getEighth())
                && set9.contains(unDecuple.getNinth())
                && set10.contains(unDecuple.getTenth())
                && set11.contains(unDecuple.getEleventh());
    }

    /**
     * Creates the Cartesian product of twelve input sets.
     *
     * @param set1  The first set
     * @param set2  The second set
     * @param set3  The third set
     * @param set4  The fourth set
     * @param set5  The fifth set
     * @param set6  The sixth set
     * @param set7  The seventh set
     * @param set8  The eight set
     * @param set9  The ninth set
     * @param set10 The tenth set
     * @param set11 The eleventh set
     * @param set12 The twelfth set
     * @param <T1>  The generic type of the first set
     * @param <T2>  The generic type of the second set
     * @param <T3>  The generic type of the third set
     * @param <T4>  The generic type of the fourth set
     * @param <T5>  The generic type of the fifth set
     * @param <T6>  The generic type of the sixth set
     * @param <T7>  The generic type of the seventh set
     * @param <T8>  The generic type of the eight set
     * @param <T9>  The generic type of the ninth set
     * @param <T10> The generic type of the tenth set
     * @param <T11> The generic type of the eleventh set
     * @param <T12> The generic type of the twelfth set
     * @return The Cartesian product of twelve sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> Set<DuoDecuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5, ? extends T6, ? extends T7, ? extends T8, ? extends T9, ? extends T10, ? extends T11, ? extends T12>> DuoDecuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5, Set<T6> set6, Set<T7> set7, Set<T8> set8, Set<T9> set9, Set<T10> set10, Set<T11> set11, Set<T12> set12) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null || set6 == null || set7 == null || set8 == null || set9 == null || set10 == null || set11 == null || set12 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return duoDecuple -> set1.contains(duoDecuple.getFirst())
                && set2.contains(duoDecuple.getSecond())
                && set3.contains(duoDecuple.getThird())
                && set4.contains(duoDecuple.getFourth())
                && set5.contains(duoDecuple.getFifth())
                && set6.contains(duoDecuple.getSixth())
                && set7.contains(duoDecuple.getSeventh())
                && set8.contains(duoDecuple.getEighth())
                && set9.contains(duoDecuple.getNinth())
                && set10.contains(duoDecuple.getTenth())
                && set11.contains(duoDecuple.getEleventh())
                && set12.contains(duoDecuple.getTwelfth());
    }

    /**
     * Creates the Cartesian product of thirteen input sets.
     *
     * @param set1  The first set
     * @param set2  The second set
     * @param set3  The third set
     * @param set4  The fourth set
     * @param set5  The fifth set
     * @param set6  The sixth set
     * @param set7  The seventh set
     * @param set8  The eight set
     * @param set9  The ninth set
     * @param set10 The tenth set
     * @param set11 The eleventh set
     * @param set12 The twelfth set
     * @param set13 The thirteenth set
     * @param <T1>  The generic type of the first set
     * @param <T2>  The generic type of the second set
     * @param <T3>  The generic type of the third set
     * @param <T4>  The generic type of the fourth set
     * @param <T5>  The generic type of the fifth set
     * @param <T6>  The generic type of the sixth set
     * @param <T7>  The generic type of the seventh set
     * @param <T8>  The generic type of the eight set
     * @param <T9>  The generic type of the ninth set
     * @param <T10> The generic type of the tenth set
     * @param <T11> The generic type of the eleventh set
     * @param <T12> The generic type of the twelfth set
     * @param <T13> The generic type of the thirteenth set
     * @return The Cartesian product of thirteen sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> Set<TreDecuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5, ? extends T6, ? extends T7, ? extends T8, ? extends T9, ? extends T10, ? extends T11, ? extends T12, ? extends T13>> TreDecuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5, Set<T6> set6, Set<T7> set7, Set<T8> set8, Set<T9> set9, Set<T10> set10, Set<T11> set11, Set<T12> set12, Set<T13> set13) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null || set6 == null || set7 == null || set8 == null || set9 == null || set10 == null || set11 == null || set12 == null || set13 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return treDecuple -> set1.contains(treDecuple.getFirst())
                && set2.contains(treDecuple.getSecond())
                && set3.contains(treDecuple.getThird())
                && set4.contains(treDecuple.getFourth())
                && set5.contains(treDecuple.getFifth())
                && set6.contains(treDecuple.getSixth())
                && set7.contains(treDecuple.getSeventh())
                && set8.contains(treDecuple.getEighth())
                && set9.contains(treDecuple.getNinth())
                && set10.contains(treDecuple.getTenth())
                && set11.contains(treDecuple.getEleventh())
                && set12.contains(treDecuple.getTwelfth())
                && set13.contains(treDecuple.getThirteenth());
    }

    /**
     * Creates the Cartesian product of fourteen input sets.
     *
     * @param set1  The first set
     * @param set2  The second set
     * @param set3  The third set
     * @param set4  The fourth set
     * @param set5  The fifth set
     * @param set6  The sixth set
     * @param set7  The seventh set
     * @param set8  The eight set
     * @param set9  The ninth set
     * @param set10 The tenth set
     * @param set11 The eleventh set
     * @param set12 The twelfth set
     * @param set13 The thirteenth set
     * @param set14 The fourteenth set
     * @param <T1>  The generic type of the first set
     * @param <T2>  The generic type of the second set
     * @param <T3>  The generic type of the third set
     * @param <T4>  The generic type of the fourth set
     * @param <T5>  The generic type of the fifth set
     * @param <T6>  The generic type of the sixth set
     * @param <T7>  The generic type of the seventh set
     * @param <T8>  The generic type of the eight set
     * @param <T9>  The generic type of the ninth set
     * @param <T10> The generic type of the tenth set
     * @param <T11> The generic type of the eleventh set
     * @param <T12> The generic type of the twelfth set
     * @param <T13> The generic type of the thirteenth set
     * @param <T14> The generic type of the fourteenth set
     * @return The Cartesian product of thirteen sets
     */
    @SuppressWarnings("unused")
    static <T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> Set<QuattuorDecuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5, ? extends T6, ? extends T7, ? extends T8, ? extends T9, ? extends T10, ? extends T11, ? extends T12, ? extends T13, ? extends T14>> QuattuorDecuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5, Set<T6> set6, Set<T7> set7, Set<T8> set8, Set<T9> set9, Set<T10> set10, Set<T11> set11, Set<T12> set12, Set<T13> set13, Set<T14> set14) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null || set6 == null || set7 == null || set8 == null || set9 == null || set10 == null || set11 == null || set12 == null || set13 == null || set14 == null) {
            throw new UtilityException(NULL_POINTER, Set.class);
        }
        return treDecuple -> set1.contains(treDecuple.getFirst())
                && set2.contains(treDecuple.getSecond())
                && set3.contains(treDecuple.getThird())
                && set4.contains(treDecuple.getFourth())
                && set5.contains(treDecuple.getFifth())
                && set6.contains(treDecuple.getSixth())
                && set7.contains(treDecuple.getSeventh())
                && set8.contains(treDecuple.getEighth())
                && set9.contains(treDecuple.getNinth())
                && set10.contains(treDecuple.getTenth())
                && set11.contains(treDecuple.getEleventh())
                && set12.contains(treDecuple.getTwelfth())
                && set13.contains(treDecuple.getThirteenth())
                && set14.contains(treDecuple.getFourteenth());
    }

}
