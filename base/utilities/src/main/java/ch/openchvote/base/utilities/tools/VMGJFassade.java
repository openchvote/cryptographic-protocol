/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tools;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.tuples.Pair;
import com.verificatum.vmgj.FpowmTab;
import com.verificatum.vmgj.VMG;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static ch.openchvote.base.utilities.UtilityException.Type.LIBRARY_ERROR;
import static java.lang.System.Logger.Level.INFO;

/**
 * This is an adapter class to the "Verificatum Multiplicative Groups library for Java" (VMGJ) by Douglas Wikstroem. It
 * is inspired by the {@code BigIntegerArithmetic} class in <a href="https://gitlab.com/chvote2">CHVote 2.0</a>). The
 * library path might be needed to be set explicitly as VM option, e.g. {@code -Djava.library.path=/usr/local/lib/}.
 */
public final class VMGJFassade {

    // private no-argument constructor to prevent the creation of instances of a utility class
    private VMGJFassade() {
    }

    // default size of the precomputation table used in fixed-based exponentiation algorithms
    static private final int DEFAULT_NUMBER_OF_BLOCKS = 18;

    // precomputation table
    static private final Map<Pair<BigInteger, BigInteger>, FpowmTab> PRECOMPUTATION_TABLES = new ConcurrentHashMap<>();

    static private final System.Logger LOGGER = System.getLogger(VMGJFassade.class.getSimpleName());
    static private final UnsatisfiedLinkError LOAD_ERROR;

    // static class initialization block for checking the availability of the VMGJ library
    static {
        UnsatisfiedLinkError loadError = null;
        try {
            VMGJFassade.checkLoaded();
        } catch (UnsatisfiedLinkError error) {
            loadError = error;
            LOGGER.log(INFO, "System    : LibVMGJ is not available (computations will be slower)");
        }
        LOAD_ERROR = loadError;
    }

    /**
     * Returns the result from computing {@code b^e mod n}. The computation is delegated to the VMGJ library.
     *
     * @param base     The base
     * @param exponent The exponent
     * @param modulus  The modulus
     * @return The result of computing {@code b^e mod n}
     */
    static public BigInteger modPow(BigInteger base, BigInteger exponent, BigInteger modulus) {
        var table = PRECOMPUTATION_TABLES.get(new Pair<>(base, modulus));
        if (table == null) {
            return VMG.powm(base, exponent, modulus);
        } else {
            return table.fpowm(exponent);
        }
    }

    /**
     * Computes the product exponentiation {@code prod_i base_i^exp_i mod n} relative to two arrays of bases and
     * exponents. The computation is delegated to the VMGJ library.
     *
     * @param bases     The bases
     * @param exponents The exponents
     * @param modulus   The modulus
     * @return The product exponentiation
     */
    static public BigInteger modProdPow(BigInteger[] bases, BigInteger[] exponents, BigInteger modulus) {
        return VMG.spowm(bases, exponents, modulus);
    }

    /**
     * Generates the precomputation table for a given base and modulus. The optimal size of the table is derived from
     * the order of the generated subgroup and from a default number of blocks.
     *
     * @param base       The given base
     * @param modulus    The given modulus
     * @param groupOrder The order of the generated subgroup
     */
    static public void precomputeTable(BigInteger base, BigInteger modulus, BigInteger groupOrder) {
        VMGJFassade.computePrecomputationTable(base, modulus, groupOrder, DEFAULT_NUMBER_OF_BLOCKS);
    }

    /**
     * Releases the precomputation table for a given base and modulus. The optimal size of the table is derived from the
     * order of the generated subgroup and the expected number of exponentiations.
     *
     * @param base       The given base
     * @param modulus    The given modulus
     * @param groupOrder The order of the generated subgroup
     * @param N          The expected number of exponentiations
     */
    static public void precomputeTable(BigInteger base, BigInteger modulus, BigInteger groupOrder, int N) {
        VMGJFassade.computePrecomputationTable(base, modulus, groupOrder, getOptimalNumberOfBlocks(groupOrder, N));
    }

    /**
     * Frees the precomputation table for a given base and modulus from memory.
     *
     * @param base    The given base
     * @param modulus The given modulus
     */
    static public void releasePrecomputationTable(BigInteger base, BigInteger modulus) {
        synchronized (PRECOMPUTATION_TABLES) {
            var table = PRECOMPUTATION_TABLES.remove(new Pair<>(base, modulus));
            if (table != null) {
                table.free();
            }
        }
    }

    /**
     * Checks if the VMGJ library is loaded.
     *
     * @return {@code true} if the VMGJ library is loaded, {@code false} otherwise
     */
    static public boolean isLoaded() {
        return LOAD_ERROR == null;
    }

    // determines the optimal number of blocks based on the exponent's bit length and the expected number of modexps
    // according to the following table:
    //       N: 224 2047/3071
    //     100:   9    12
    //    1000:  12    15
    //   10000:  15    18
    //  100000:  18    21
    // 1000000:  21    24
    static private int getOptimalNumberOfBlocks(BigInteger groupOrder, int N) {
        int bitLength = groupOrder.bitLength();
        int numberOfBlocks = 0;
        int[][] lookupTable = {{9, 12}, {12, 15}, {15, 18}, {18, 21}, {21, 24}};
        for (int i = 0; i < lookupTable.length; i++) {
            if (i == lookupTable.length - 1 || N < Math.intPowerOfTen(i + 3)) {
                numberOfBlocks = lookupTable[i][bitLength <= 256 ? 0 : 1];
                break;
            }
        }
        return numberOfBlocks;
    }

    // private method for generating the precomputation table using the VMGJ library
    static private void computePrecomputationTable(BigInteger base, BigInteger modulus, BigInteger groupOrder, int numberOfBlocks) {
        if (VMGJFassade.isLoaded()) {
            var key = new Pair<>(base, modulus);
            synchronized (PRECOMPUTATION_TABLES) {
                if (!PRECOMPUTATION_TABLES.containsKey(key)) {
                    var table = new FpowmTab(base, modulus, numberOfBlocks, groupOrder.bitLength());
                    PRECOMPUTATION_TABLES.put(key, table);
                }
            }
        }
    }

    // inspired by com.squareup.jnagmp.Gmp;
    static private void checkLoaded() {
        if (LOAD_ERROR != null) {
            throw LOAD_ERROR;
        }
        // make a test call 2^3 mod 5 = 3, sometimes the error won't occur until you try the native method
        var two = BigInteger.valueOf(2);
        var three = BigInteger.valueOf(3);
        var five = BigInteger.valueOf(5);
        var answer = VMG.powm(two, three, five);
        if (!three.equals(answer)) {
            throw new UtilityException(LIBRARY_ERROR, VMGJFassade.class, "LibVMGJ is loaded but powm returned the wrong answer");
        }
    }

}
