/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tools;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;

import java.math.BigInteger;
import java.util.stream.IntStream;

import static ch.openchvote.base.utilities.UtilityException.Type.INCOMPATIBLE_LENGTHS;

/**
 * This class provides some additional methods for mathematical functions not included in standard Java libraries. The
 * methods deal with integer values of type {@code byte}, {@code int} or {@link BigInteger}.
 */
public final class Math {

    // private no-argument constructor to prevent the creation of instances of a utility class
    private Math() {
    }

    /**
     * A byte consisting of 0-bits.
     */
    static public final byte ZEROS = Math.intToByte(0x00);

    /**
     * A byte consisting of 1-bits.
     */
    static public final byte ONES = Math.intToByte(0xFF);

    /**
     * Converts a given value {@code 0<=x<=255} of type {@code int} into a value {@code -128<=b<=127} of type {@code
     * byte}. For an invalid input, the return value is not specified.
     *
     * @param x The given value of type {@code int}
     * @return The converted value of type {@code byte}
     */
    static public byte intToByte(int x) {
        return (byte) x;
    }

    /**
     * Converts a given value {@code -128<=b<=127} of type {@code byte} into a value {@code 0<=x<=255} of type {@code
     * int}. This method implements the inverse functions of {@code intToByte}.
     *
     * @param b The given value of type {@code byte}
     * @return The converted value of type {@code int}
     */
    static public int byteToInt(byte b) {
        return b < 0 ? b + 256 : b;
    }

    /**
     * Computes the bit-wise logical AND-operation of two bytes.
     *
     * @param b1 The first byte
     * @param b2 The second byte
     * @return The result of applying the bit-wise logical AND-operation
     */
    static public byte and(byte b1, byte b2) {
        return (byte) (b1 & b2);
    }

    /**
     * Computes the bit-wise logical OR-operation of two bytes.
     *
     * @param b1 The first byte
     * @param b2 The second byte
     * @return The result of applying the bit-wise logical OR-operation
     */
    static public byte or(byte b1, byte b2) {
        return (byte) (b1 | b2);
    }

    /**
     * Computes the bit-wise logical XOR-operation of two bytes.
     *
     * @param b1 The first byte
     * @param b2 The second byte
     * @return The result of applying the bit-wise logical XOR-operation
     */
    static public byte xor(byte b1, byte b2) {
        return (byte) (b1 ^ b2);
    }

    /**
     * Computes the fraction {@code x/y} rounded up to the next integer, where {@code x>=0} and {@code y>=1} are the
     * given input values of type {@code int}. For invalid inputs, the return value is not specified.
     *
     * @param x The numerator
     * @param y The denominator
     * @return The fraction {@code x/y} rounded up to the next integer
     */
    @SuppressWarnings("SuspiciousNameCombination")
    static public int ceilDiv(int x, int y) {
        if (divides(y, x)) {
            return x / y;
        } else {
            return x / y + 1;
        }
    }

    /**
     * Computes the number of necessary bits to represent {@code x>=0} of type {@code int}. For negative inputs, the
     * return value is not specified.
     *
     * @param x The given value
     * @return The number of necessary bits to represent {@code x>=0}
     */
    static public int bitLength(int x) {
        return Integer.SIZE - Integer.numberOfLeadingZeros(java.lang.Math.abs(x));
    }

    /**
     * Checks if {@code x>0} is an integer divisor of {@code y>=0}. For invalid inputs, the return value is not
     * specified.
     *
     * @param x The first integer
     * @param y The second integer
     * @return {@code true}, if {@code x} is an integer divisor of {@code y}, {@code false} otherwise
     */
    static public boolean divides(int x, int y) {
        return y % x == 0;
    }

    /**
     * Computes {@code 2^y} for a given exponent {@code 0<=y<=30}. For an invalid input, the return value is not
     * specified.
     *
     * @param y The given exponent
     * @return The {@code y}-th power of 2
     */
    static public int intPowerOfTwo(int y) {
        return 1 << y;
    }

    /**
     * Computes {@code 10^y} for a given exponent {@code 0<=y<=9}. For an invalid input, the return value is not
     * specified.
     *
     * @param y The given exponent
     * @return The {@code y}-th power of 10
     */
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    static public int intPowerOfTen(int y) {
        return IntStream.iterate(1, x -> x * 10).skip(y).findFirst().getAsInt();
    }

    /**
     * Computes the sum of products of all integer pairs from two input vectors of type {@code IntVector}. An
     * exception is thrown for input vectors of different length.
     *
     * @param vector1 The first vector of integer values
     * @param vector2 The second vector of integer values
     * @return The sum of products all integer pairs
     */
    static public int intSumProd(IntVector vector1, IntVector vector2) {
        if (vector1.getLength() != vector2.getLength()) {
            throw new UtilityException(INCOMPATIBLE_LENGTHS, Math.class);
        }
        return IntStream.rangeClosed(1, vector1.getLength()).map(index -> vector1.getValue(index) * vector2.getValue(index)).sum();
    }

    /**
     * Computes the fraction {@code x/y} rounded up to the next integer, where {@code x>=0} and {@code y>=1} are the
     * given input values of type {@code BigInteger}. For invalid inputs, the return value is not specified.
     *
     * @param x The numerator
     * @param y The denominator
     * @return The fraction {@code x/y} rounded up to the next integer
     */
    @SuppressWarnings("SuspiciousNameCombination")
    static public BigInteger ceilDiv(BigInteger x, BigInteger y) {
        if (divides(y, x)) {
            return x.divide(y);
        } else {
            return x.divide(y).add(BigInteger.ONE);
        }
    }

    /**
     * Computes the number of necessary bytes to represent the absolute value of {@code x} in the ordinary
     * binary representation.
     *
     * @param x The given value
     * @return The number of necessary bytes to represent the absolute value of {@code x}
     */
    static public int byteLength(BigInteger x) {
        return ceilDiv(x.abs().bitLength(), Byte.SIZE);
    }

    /**
     * Computes the logarithm of {@code x} to the base {@code b} rounded up to the next integer. This
     * method is a special case of {@link Math#ceilLog(BigInteger, BigInteger)} for a base of type {@code long}.
     *
     * @param x The given integer
     * @param b The base
     * @return The logarithm of {@code x} to the base {@code b} rounded up to the next integer
     */
    static public int ceilLog(BigInteger x, long b) {
        return ceilLog(x, BigInteger.valueOf(b));
    }

    /**
     * Computes the logarithm of {@code x} to the base {@code b} rounded up to the next integer, where {@code x>=1} and
     * {@code b>=2} are the given integer values. For invalid inputs, the return value is not specified.
     *
     * @param x The given integer
     * @param b The base
     * @return The logarithm of {@code x} to the base {@code b} rounded up to the next integer
     */
    static public int ceilLog(BigInteger x, BigInteger b) {
        int result = 0;
        var z = BigInteger.ONE;
        while (z.compareTo(x) < 0) {
            result++;
            z = z.multiply(b);
        }
        return result;
    }

    /**
     * Computes the logarithm of {@code x} to the base {@code b} rounded down to the next integer. This
     * method is a special case of {@link Math#floorLog(BigInteger, BigInteger)} for a base of type {@code long}.
     *
     * @param x The given integer
     * @param b The base
     * @return The logarithm of {@code x} to the base {@code b} rounded down to the next integer
     */
    static public int floorLog(BigInteger x, long b) {
        return floorLog(x, BigInteger.valueOf(b));
    }

    /**
     * Computes the logarithm of {@code x} to the base {@code b} rounded down to the next integer, where {@code x>=1}
     * and {@code b>=2} are the given integer values. For invalid inputs, the output is not specified.
     *
     * @param x The given integer
     * @param b The base
     * @return The logarithm of {@code x} to the base {@code b} rounded down to the next integer
     */
    static public int floorLog(BigInteger x, BigInteger b) {
        int result = 0;
        var z = b;
        while (z.compareTo(x) <= 0) {
            result++;
            z = z.multiply(b);
        }
        return result;
    }

    /**
     * Checks if {@code x>0} is an integer divisor of {@code y>=0}. For invalid inputs, the return value is not
     * specified.
     *
     * @param x The first integer
     * @param y The second integer
     * @return {@code true}, if {@code x} is an integer divisor of {@code y}, {@code false} otherwise
     */
    static public boolean divides(BigInteger x, BigInteger y) {
        return y.mod(x).equals(BigInteger.ZERO);
    }

    /**
     * Computes {@code 2^y} for a given exponent {@code 0<=y}. For an invalid input, the return value is not
     * specified.
     *
     * @param y The given exponent
     * @return The {@code y}-th power of 2
     */
    static public BigInteger powerOfTwo(int y) {
        return BigInteger.ONE.shiftLeft(y);
    }

    /**
     * Computes the sum of all integer values from a given vector of type {@code Vector<BigInteger>}. The behaviour of
     * this method is unspecified for vectors containing {@code null} values.
     *
     * @param vector The given vector of integer values
     * @return The sum all integer values
     */
    static public BigInteger sum(Vector<BigInteger> vector) {
        return vector.toStream().reduce(BigInteger.ZERO, BigInteger::add);
    }

    /**
     * Computes the product of all integer values from a given vector of type {@code Vector<BigInteger>}. The behaviour
     * of this method is unspecified for vectors containing {@code null} values.
     *
     * @param vector The given vector of integer values
     * @return The product all integer values
     */
    static public BigInteger prod(Vector<BigInteger> vector) {
        return vector.toStream().reduce(BigInteger.ONE, BigInteger::multiply);
    }

    /**
     * Computes the Jacobi symbol {@code J(x,n)} of a positive integer {@code x} and an odd positive integer {@code n}
     * satisfying {@code 1<=x<=n}. The method implements the algorithm as described in Eric Bach and Jeffrey Shallit's
     * book on "Algorithmic Number Theory". The behaviour of this method and its return value are unspecified for
     * invalid input values.
     * <br/>
     * Reference: Eric Bach and Jeffrey Shallit, "Algorithmic Number Theory", "Vol.1: Efficient Algorithms", p.113
     *
     * @param x A positive integer
     * @param n An odd positive integer
     * @return The Jacobi symbol of {@code x} and {@code n}
     */
    static public int jacobiSymbol(BigInteger x, BigInteger n) {
        int result = 1;
        BigInteger z;
        while (x.signum() != 0) {
            while (!x.testBit(0)) {
                x = Math.div2(x);
                if (n.testBit(0) && n.testBit(1) == !n.testBit(2)) {
                    result = -result;
                }
            }
            z = x;
            x = n;
            n = z;
            if (x.testBit(0) && x.testBit(1) && n.testBit(0) && n.testBit(1)) {
                result = -result;
            }
            x = x.mod(n);
        }
        return n.equals(BigInteger.ONE) ? result : 0;
    }

    /**
     * Computes {@code x mod 256}. Returns the result as a value of type {@code int}.
     *
     * @param x The input value
     * @return {@code x} modulo 256
     */
    static public int mod256(BigInteger x) {
        return x.intValue() % 256;
    }

    /**
     * Computes {@code x/256}.
     *
     * @param x The input value
     * @return {@code x} divided by 256
     */
    static public BigInteger div256(BigInteger x) {
        return x.shiftRight(Byte.SIZE);
    }

    /**
     * Computes {@code 256*x}.
     *
     * @param x The input value
     * @return {@code x} times 256
     */
    static public BigInteger mult256(BigInteger x) {
        return x.shiftLeft(Byte.SIZE);
    }

    /**
     * Computes {@code x/2}.
     *
     * @param x The input value
     * @return {@code x} divided by 2
     */
    static public BigInteger div2(BigInteger x) {
        return x.shiftRight(1);
    }

    /**
     * Computes {@code 2*x}.
     *
     * @param x The input value
     * @return {@code x} times 2
     */
    static public BigInteger mult2(BigInteger x) {
        return x.shiftLeft(1);
    }

    /**
     * Checks if the given candidate is probably a prime with a failure probability of at most {@code 1/2^128}.
     *
     * @param x The given prime candidate
     * @return {@code true}, if the candidate is probably prime, {@code false} otherwise
     */
    static public boolean isProbablePrime(BigInteger x) {
        return x.signum() == 1 && x.isProbablePrime(128); // we set the certainty to 128 bits
    }

    /**
     * Checks if the given candidate is probably a safe prime with a failure probability of at most {@code 1/2^128}.
     *
     * @param x The given safe prime candidate
     * @return {@code true}, if the candidate is probably a safe prime, {@code false} otherwise
     */
    static public boolean isProbableSafePrime(BigInteger x) {
        return isProbablePrime(x) && isProbablePrime(div2(x));
    }

    /**
     * Checks if two given integers {@code x} and {@code y} are coprime, i.e., if their greatest common divisor is 1.
     *
     * @param x The first integer
     * @param y The second integer
     * @return {@code true}, if {@code x} and {@code y} are coprime, {@code false} otherwise
     */
    static public boolean isCoPrime(BigInteger x, BigInteger y) {
        return x.gcd(y).equals(BigInteger.ONE);
    }

}

