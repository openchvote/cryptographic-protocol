/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tools;

import ch.openchvote.base.utilities.crypto.HashAlgorithm;
import ch.openchvote.base.utilities.sequence.*;
import ch.openchvote.base.utilities.set.IndexedFamily;
import ch.openchvote.base.utilities.tuples.Tuple;

import java.util.HashMap;
import java.util.Map;

/**
 * This is a base class for other classes providing a method for hashing the object into a byte array. It implements a
 * cache for previously computed hash values. Writing to and reading from the cache is not done automatically. It's the
 * responsibility of the developer using these classes to decide if hash values are cached or not, using the methods
 * {@link Hashable#getHashValue(HashAlgorithm)} and {@link Hashable#putHashValue(HashAlgorithm, ByteArray)}.
 */
public abstract sealed class Hashable permits IntMatrix, IntVector, Matrix, Vector, IndexedFamily, Tuple {

    // the internal map for caching the hash values
    private final Map<HashAlgorithm, ByteArray> CACHE = new HashMap<>();

    /**
     * Checks if the cache contains a hash value for the given hash algorithm.
     *
     * @param hashAlgorithm The hash algorithm
     * @return {@code true}, if the cache contains a hash value, {@code false} otherwise
     */
    public boolean hasHashValue(HashAlgorithm hashAlgorithm) {
        return this.CACHE.containsKey(hashAlgorithm);
    }

    /**
     * Recovers a hash value from the cache. If no hash value exists in the cache for the specified hash algorithm, this
     * method return {@code null}.
     *
     * @param hashAlgorithm The hash algorithm
     * @return The object's cached hash value or {@code null}
     */
    public ByteArray getHashValue(HashAlgorithm hashAlgorithm) {
        return this.CACHE.get(hashAlgorithm);
    }

    /**
     * Adds a hash value to the cache, but only it no hash value is already present for the specified hash algorithm.
     * Otherwise, the new hash value is ignored.
     *
     * @param hashAlgorithm The hash algorithm
     * @param hashValue     The hash value add to the cache
     */
    public void putHashValue(HashAlgorithm hashAlgorithm, ByteArray hashValue) {
        this.CACHE.putIfAbsent(hashAlgorithm, hashValue);
    }

}
