/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities;

/**
 * This class is used for throwing specific runtime exceptions that could occur while using the classes from this
 * module. The different types of utility exceptions are represented by the values of an internal enum class. As runtime
 * exceptions, they are indicators for possible programming bugs.
 */
public final class UtilityException extends RuntimeException {

    @SuppressWarnings("MissingJavadoc")
    public enum Type {
        AES_ENCRYPTION_SETUP_ERROR,
        AES_DECRYPTION_SETUP_ERROR,
        ALREADY_BUILT,
        DESERIALIZATION_ERROR,
        DUPLICATE_CHARACTER,
        HEALTH_TEST_FAILURE,
        HEALTH_TEST_THREADING_PROBLEM,
        INCOMPATIBLE_LENGTHS,
        INDEX_OUT_OF_BOUNDS,
        INVALID_HEIGHT_OR_WIDTH,
        INVALID_LENGTH,
        INVALID_PARAMETERS,
        INVALID_SPECIAL_CHARACTERS,
        LIBRARY_ERROR,
        NULL_POINTER,
        RANK_OUT_OF_BOUNDS,
        SERIALIZATION_ERROR,
        SHA3_SETUP_ERROR,
        TYPE_REFERENCE_ERROR,
        UNBALANCED_PARENTHESES,
        UNKNOWN_CHARACTER,
        UNSUPPORTED_OPERATION
    }

    // the exception type
    private final Type type;

    // the utility class causing this exception
    private final Class<?> utilityClass;

    // a user-defined comment string
    private final String comment;

    // an array ob objects causing the exception
    private final Object[] objects;

    /**
     * Constructs a new utility exception based on the given parameters.
     *
     * @param type         The type of the exception
     * @param utilityClass The utility class causing the exception
     * @param objects      An array of objects causing the exception
     */
    public UtilityException(Type type, Class<?> utilityClass, Object... objects) {
        this(type, utilityClass, "", objects);
    }

    /**
     * Constructs a new utility exception based on the given parameters.
     *
     * @param type         The type of the exception
     * @param utilityClass The utility class causing the exception
     * @param comment      The given comment string
     * @param objects      An array of objects causing the exception
     */
    public UtilityException(Type type, Class<?> utilityClass, String comment, Object... objects) {
        super(type.name() + ": " + comment + "(" + utilityClass.getSimpleName() + ")");
        this.type = type;
        this.utilityClass = utilityClass;
        this.comment = comment;
        this.objects = objects;
    }

    /**
     * Returns the type of the exception.
     *
     * @return The type of the exception
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Returns the utility class causing this exception.
     *
     * @return The utility class causing this exception
     */
    public Class<?> getUtilityClass() {
        return this.utilityClass;
    }

    /**
     * Returns the comment string defined for this exception.
     *
     * @return The comment string
     */
    public String getComment() {
        return this.comment;
    }

    /**
     * Returns the array of objects causing the exception.
     *
     * @return The objects causing the exception
     */
    public Object[] getObjects() {
        return this.objects;
    }

}
