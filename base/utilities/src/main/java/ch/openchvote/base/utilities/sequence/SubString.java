/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.set.IntSet;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static ch.openchvote.base.utilities.UtilityException.Type.*;

/**
 * Objects of this class represent substrings of existing Java string. Instead of copying the characters of the
 * substring, they keep a reference to the parent string and two indices that indicate the start and the end position of
 * the substring. The purpose of this class is to avoid copying substrings from large strings.
 */
public final class SubString implements Sequence<Character> {

    private final String string; // parent string
    private final int from; // index of first character
    private final int to; // index of last character + 1

    /**
     * Constructs a substring object that is identical to the given parent string.
     *
     * @param string The given parent string
     */
    public SubString(String string) {
        this(string, 0, string.length());
    }

    /**
     * Constructs a substring object based on the characters of the given parent string. The substring begins at the
     * specified index {@code from} and extends to the character at index {@code to - 1}. Thus, the length of the
     * substring is {@code to - from}.
     *
     * @param string The given parent string
     * @param from   The index of the first substring character
     * @param to     The index of the last substring character plus 1
     */
    public SubString(String string, int from, int to) {
        if (string == null || from < 0 || to > string.length() || to < from) {
            throw new UtilityException(INVALID_PARAMETERS, SubString.class, "Incompatible index bounds or null argument");
        }
        this.string = string;
        this.from = from;
        this.to = to;
    }

    /**
     * Returns the character at the specified index. An index ranges from {@code 0} to {@code length() - 1}.
     *
     * @param index The index of the character
     * @return The character at the specified index of this substring
     */
    public char charAt(int index) {
        if (index < 0 || index > this.getLength()) {
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, SubString.class, "Negative index or index larger than length");
        }
        return this.string.charAt(this.from + index);
    }

    /**
     * Returns a new substring of an existing substring. The new substring begins at the specified index {@code from}
     * and extends to the character at index {@code to - 1}. Thus, the length of the new substring is
     * {@code to - from}.
     *
     * @param from The index of the first substring character
     * @param to   The index of the last substring character plus 1
     * @return The specified substring
     */
    public SubString substring(int from, int to) {
        return new SubString(this.string, this.from + from, this.from + to);
    }

    @Override
    public int getMinIndex() {
        return 0;
    }

    @Override
    public int getMaxIndex() {
        return this.to - this.from - 1;
    }

    @Override
    public Character getValue(int index) {
        return this.charAt(index);
    }

    @Override
    public String toString() {
        return this.string.substring(this.from, this.to);
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof SubString other) {
            if (this.getLength() == other.getLength()) {
                return IntStream.range(0, this.getLength()).allMatch(index -> this.charAt(index) == other.charAt(index));
            }
        }
        return false;
    }

    /**
     * This method can be used to split a string representation of a tree with balanced opening and closing parentheses
     * defined by {@code open} and {@code close} into a list of top-level children defined by {@code delimiter}. For
     * example, {@code "[[1,2],[x,y,[A,B,C]],[]]"} is split into three substrings {@code "[1,2]"},
     * {@code "[x,y,[A,B,C]]"}, and {@code ""}, for {@code open='['}, {@code close=']'}, and {@code delimiter=','}. The
     * {@code escape} character must prefix any special character appearing in the string
     *
     * @param open The opening parenthesis
     * @param close The closing parentheses
     * @param delimiter The delimiter character
     * @param escape The escape character
     * @return The list of substrings obtained from splitting the top-level elements of tree representation
     */
    public List<SubString> split(char open, char close, char delimiter, char escape) {
        if (open == close || open == delimiter || open == escape || close == delimiter || close == escape || delimiter == escape) {
            throw new UtilityException(INVALID_SPECIAL_CHARACTERS, SubString.class, "Identical special characters");
        }
        List<SubString> result = new ArrayList<>();
        int length = this.getLength();
        if (length < 2 || this.charAt(0) != open || this.charAt(length - 1) != close) {
            throw new UtilityException(INVALID_SPECIAL_CHARACTERS, SubString.class, "Missing opening or closing character");
        }
        if (length > 2) {
            int counter = 0;
            int i = 1;
            for (int j : IntSet.range(1, length - 1)) {
                if (counter < 0) {
                    throw new UtilityException(INVALID_SPECIAL_CHARACTERS, SubString.class, "Missing or wrongly ordered opening or closing characters");
                }
                char c = this.charAt(j);
                if (c == delimiter || j == length - 1) {
                    if (counter == 0) {
                        result.add(this.substring(i, j));
                        i = j + 1;
                    }
                } else {
                    if (c == open && this.charAt(j - 1) != escape)
                        counter++;
                    if (c == close && this.charAt(j - 1) != escape)
                        counter--;
                }
            }
            if (counter < 0) {
                throw new UtilityException(UNBALANCED_PARENTHESES, SubString.class);
            }
        }
        return result;
    }

}
