/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tools;

/**
 * Common interface for different builder implementations of a collection (sequence, set, vector, etc.) of integer
 * values.
 *
 * @param <C> The generic type of the collection to build
 */
public interface IntBuilder<C> {

    /**
     * Adds a new integer value to the collection. The builder object itself is returned to allow pipeline notation when
     * multiple integer values are added to the collection.
     *
     * @param value The integer value to be added
     * @return The builder itself
     */
    @SuppressWarnings("UnusedReturnValue")
    IntBuilder<C> add(int value);

    /**
     * Terminates the building process and returns the constructed collection of integer values.
     *
     * @return The constructed collection of integer values
     */
    C build();

    /**
     * Adds multiple integer values to the collection. The builder object itself is returned to allow pipeline notation.
     *
     * @param values An iterable collection of integer values to be added
     * @return The builder itself
     */
    default IntBuilder<C> addAll(Iterable<Integer> values) {
        values.forEach(this::add);
        return this;
    }

}
