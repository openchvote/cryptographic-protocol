/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.crypto;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.internal.MutableByteArray;
import ch.openchvote.base.utilities.sequence.ByteArray;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static ch.openchvote.base.utilities.UtilityException.Type.SHA3_SETUP_ERROR;

/**
 * This class provides an abstraction of the SHA3-256 hash algorithm. It consists of a methods for
 * computing hash values of arbitrary input messages, which are given as instances of the class {@link ByteArray}.
 * Internally, the existing Java security class {@link MessageDigest} is used to perform the actual hash value
 * computation.
 */
public final class SHA3 implements HashAlgorithm {

    static private final int HASH_LENGTH = 32; // number of bytes

    // package-private constructor to allow singleton instantiation in HashAlgorithm
    SHA3() {
    }

    @Override
    public int getLength() {
        return HASH_LENGTH;
    }

    @Override
    public ByteArray hash(ByteArray message) {
        try {
            byte[] bytes = MessageDigest.getInstance("SHA3-256").digest(message.toByteArray());
            return new MutableByteArray(bytes);
        } catch (NoSuchAlgorithmException exception) {
            throw new UtilityException(SHA3_SETUP_ERROR, SHA3.class);
        }
    }

}
