/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.crypto;

import ch.openchvote.base.utilities.sequence.ByteArray;

/**
 * This interface defines the functionality of a block cipher, which operates on byte arrays. Instantiations of this
 * interface can be used for encrypting and decrypting messages with a symmetric key.
 */
public interface BlockCipher {

    /**
     * The default AES-GCM instantiation of this interface.
     */
    BlockCipher AES = new AES();

    /**
     * Returns the encryption scheme's key length (number of bytes).
     *
     * @return The key length
     */
    int getKeyLength();

    /**
     * Returns the length of the encryption scheme's initialization vector (number of bytes).
     *
     * @return The length of the initialization vector
     */
    int getIVLength();

    /**
     * Encrypts the message using the given initialization vector and secret key. Both the message and the resulting
     * ciphertext are instances of the class {@link ByteArray}.
     *
     * @param K  The secret key
     * @param IV The initialization vector
     * @param M  The message to encrypt
     * @return The encrypted message
     */
    ByteArray encrypt(ByteArray K, ByteArray IV, ByteArray M);

    /**
     * Decrypts a ciphertext using the given initialization vector and the secret key. Both the ciphertext and the
     * resulting decrypted message are instances of the class {@link ByteArray}. This method is the inverse of
     * {@link BlockCipher#encrypt}, which means that {@code decrypt(K,IV,encrypt(K,IV,M))} must be equal to {@code M}
     * for all {@code K}, {@code IV}, and {@code M}.
     *
     * @param K  The secret key
     * @param IV The Initialization vector
     * @param C  The ciphertext to decrypt
     * @return The decrypted message
     */
    ByteArray decrypt(ByteArray K, ByteArray IV, ByteArray C);

}
