/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.crypto;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.internal.MutableByteArray;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.set.IntSet;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

import static ch.openchvote.base.utilities.UtilityException.Type.*;

/**
 * This class implements a static method {@link Entropy#getEntropy(int)}} for extracting so-called CPU jitter
 * entropy. The purpose of calling this method is to complement the JVM's default entropy source used for seeding and
 * reseeding instances of the PRNG class {@link SecureRandom}. The other private methods of this class implement some
 * techniques proposed in the NIST Special Publication SP.800-90B.
 *
 * @see <a href="https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-90B.pdf">NIST SP.800-90B</a>
 * @see <a href="https://www.chronox.de/jent/doc/CPU-Jitter-NPTRNG.pdf">CPU-Jitter-NPTRNG</a>
 * @see <a href="http://lkml.iu.edu/hypermail/linux/kernel/1909.3/03718.html">Linux kernel</a>
 */
public final class Entropy {

    // jitter entropy constants
    static private final double ESTIMATED_ENTROPY_PER_SAMPLE = 1.0 / 16; // = 1/16 bit
    static private final long TIME_PRECISION_NANOS = 1000; // this value is observed on current JVMs
    static private final int SLEEPING_TIME_MILLIS = 1;
    static private final int THREAD_POOL_SIZE = 256;

    // health test constants
    static private final int HEALTH_TEST_SAMPLE_SIZE = 1024; // see NIST SP800-90B, p.24, Item 4
    static private final int MINUS_LOG_ALPHA = 20;
    static private final double ALPHA = Math.pow(2.0, -MINUS_LOG_ALPHA); // allowed false positives probability, see NIST SP800-90B, Subsection 4.4
    static private final int WINDOW_SIZE = 512; // recommended by NIST for non-binary samples

    // hash algorithm used to condition the extracted entropy bits to 256 bits
    static private final HashAlgorithm HASH_ALGORITHM = HashAlgorithm.SHA3;

    // the start-up health test in this static block checks if the noise source is able to provide the assumed entropy
    static {
        if (!healthTest(HEALTH_TEST_SAMPLE_SIZE)) {
            throw new UtilityException(HEALTH_TEST_FAILURE, Entropy.class);
        }
    }

    /**
     * This method extracts jitter entropy that can be used for complementing the default seeding or reseeding
     * mechanisms of existing pseudorandom number generators. It extracts the entropy from a series of samples. Each
     * sample results from a time measurement in microseconds.
     *
     * @param bitsOfEntropy The requested minimum amount of entropy in bits
     * @return The resulting entropy conditioned as a byte array of length 32 bytes (256 bits).
     */
    static public ByteArray getEntropy(int bitsOfEntropy) {
        if (bitsOfEntropy < 0 || bitsOfEntropy > HASH_ALGORITHM.getLength() * Byte.SIZE) {
            throw new UtilityException(INVALID_PARAMETERS, Entropy.class, "Invalid number of entropy bits");
        }
        int numberOfSamplesRequested = (int) Math.ceil(bitsOfEntropy / ESTIMATED_ENTROPY_PER_SAMPLE);
        var samples = Entropy.getNoise(numberOfSamplesRequested);
        var byteBuffer = ByteBuffer.allocate(numberOfSamplesRequested * Integer.BYTES);
        for (int sample : samples) {
            byteBuffer.putInt(sample);
        }
        var byteArray = new MutableByteArray(byteBuffer.array());
        return HASH_ALGORITHM.hash(byteArray);
    }

    // This method provides noise of a true entropy source. It does so by sampling the 'jitter' entropy of the high
    // precision timing of starting a Thread, {@link Thread#sleep(long)}, {@link Thread#yield()} and in memory
    // fetching with probable cash miss. As the time precision for these actions are non-deterministic to several
    // 1000 CPU cycles, the exact timing carries some true entropy (by ignorance due to hidden parameters). As a
    // pessimistic approach this algorithm assumes some fraction bit of true entropy per run. The execution time of
    // this method depends on the numberOfSamplesRequested and the CPU-Threads available. The method only provides
    // one synchronization 'mutex', so synchronization happens more often and more non-deterministic. Per requested
    // sample, a Thread is created and executed. The thread does a sleep of some milliseconds. The thread then does
    // a memory-lookup that is most probably not cached. The time that is required for all these actions is
    // accumulated and is used as a sample.
    static private List<Integer> getNoise(int numberOfSamplesRequested) {

        var executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        var futureList = new ArrayList<Future<Long>>();  // list of futures containing the raw samples of type long
        var mutex = new Object(); // object on which to synchronize

        // initialize shared data structure used to provoke time skewing and additional thread synchronization
        var sharedList = new ArrayList<Long>();
        for (long x = 0; x < TIME_PRECISION_NANOS; x++) {
            sharedList.add(x);
        }

        // add tasks to executor service
        for (int i : IntSet.range(1, numberOfSamplesRequested)) {
            Future<Long> future = executorService.submit(new Callable<>() {
                private final Instant start = Instant.now();
                private Instant finish = null;

                @SuppressWarnings("BusyWait")
                @Override
                public Long call() {
                    long randomNanos = 0;
                    while (this.finish == null) {
                        try {
                            // generate jitter entropy by putting the thread to sleep
                            Thread.sleep(SLEEPING_TIME_MILLIS);

                            // provoke time-skewing and thread synchronizations by reading/writing the common list
                            for (int i = 0; i < sharedList.size(); i++) {
                                synchronized (mutex) {
                                    long x = sharedList.getFirst();
                                    sharedList.removeFirst();
                                    sharedList.add(x);
                                    randomNanos = (randomNanos + x) % TIME_PRECISION_NANOS;
                                }
                                // hint the processor to yield the current thread
                                Thread.yield();
                            }
                            this.finish = Instant.now();
                        } catch (InterruptedException exception) {
                            // if sleeping gets interrupted, try again
                        }
                    }
                    // compute sample and add it to list
                    long duration = Duration.between(this.start, this.finish).toNanos();
                    return duration + randomNanos;
                }
            });
            futureList.add(future);
        }

        // apply von Neumann unbiasing to raw samples
        var unbiasedSamples = new ArrayList<Integer>();
        for (var future : futureList) {
            try {
                long sample = future.get();
                int unbiasedSample = vonNeumannUnbiasing(sample);
                unbiasedSamples.add(unbiasedSample);
            } catch (InterruptedException | ExecutionException exception) {
                throw new UtilityException(HEALTH_TEST_THREADING_PROBLEM, Entropy.class);
            }
        }

        // stop executor service and return result
        executorService.shutdownNow();
        return unbiasedSamples;
    }

    // performs the von Neumann unbiasing on a single sample
    static private int vonNeumannUnbiasing(long sample) {
        long result = 0;
        int x = 1;
        while (sample > 0) {
            long b1 = sample % 2; // last significant bit
            sample = sample / 2;
            long b2 = sample % 2; // second last significant bit
            sample = sample / 2;
            if (b1 != b2) {
                // process pair of unequal bits: 10 => 1, 01 => 0
                result = result + x * b1;
                x = 2 * x;
            }
        }
        return (int) result;
    }

    // executes the three health test proposed by Stephan Mueller
    @SuppressWarnings("SameParameterValue")
    static private boolean healthTest(int numberOfSamplesRequested) {
        List<Integer> samples = getNoise(numberOfSamplesRequested);
        return stuckTest(samples) && repetitionCountTest(samples) && adaptiveProportionTest(samples);
    }

    /**
     * An implementation of the stuck test from NIST SP800-90B, Subsection 4.4.1. Checks if the 1st, 2nd, and 3rd
     * derivations are non-zero.
     *
     * @param samples A list of sample values
     * @return {@code true}, if the test succeeds, {@code false} otherwise
     */
    static public boolean stuckTest(List<Integer> samples) {
        var firstDerivation = IntStream.range(1, samples.size()).mapToObj(index -> samples.get(index) - samples.get(index - 1)).toList();
        if (firstDerivation.stream().allMatch(x -> x == 0)) {
            return false;
        }
        var secondDerivation = IntStream.range(1, firstDerivation.size()).mapToObj(index -> firstDerivation.get(index) - firstDerivation.get(index - 1)).toList();
        if (secondDerivation.stream().allMatch(x -> x == 0)) {
            return false;
        }
        var thirdDerivation = IntStream.range(1, secondDerivation.size()).mapToObj(index -> secondDerivation.get(index) - secondDerivation.get(index - 1)).toList();
        return !thirdDerivation.stream().allMatch(x -> x == 0);
    }

    /**
     * An implementation of the repetition count test from NIST SP800-90B, Subsection 4.4.1.
     *
     * @param samples A list of sample values
     * @return {@code true}, if the test succeeds, {@code false} otherwise
     */
    static public boolean repetitionCountTest(List<Integer> samples) {
        if (samples.isEmpty()) {
            return false;
        }
        int C = 1 + (int) Math.ceil(MINUS_LOG_ALPHA / ESTIMATED_ENTROPY_PER_SAMPLE);
        long A = samples.getFirst();
        int B = 1;
        for (int i = 1; i < samples.size(); i++) {
            int X = samples.get(i);
            if (X == A) {
                B++;
                if (B >= C) {
                    return false;
                }
            } else {
                A = X;
                B = 1;
            }
        }
        return true;
    }

    /**
     * An implementation of the adaptive proportion test from NIST SP800-90B, Subsection 4.4.2.
     *
     * @param samples A list of sample values
     * @return {@code true}, if the test succeeds, {@code false} otherwise
     */
    static public boolean adaptiveProportionTest(List<Integer> samples) {
        if (samples.isEmpty()) {
            return false;
        }
        int C = 1 + critBinom(WINDOW_SIZE, Math.pow(2.0, -ESTIMATED_ENTROPY_PER_SAMPLE), 1 - ALPHA);
        for (int i = 0; i < samples.size(); i++) {
            int A = samples.get(i);
            int B = 1;
            for (int j = 1; j < Math.min(WINDOW_SIZE, samples.size() - i); j++) {
                if (A == samples.get(i + j)) {
                    B++;
                    if (B >= C) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    // returns the smallest value for which the cumulative binomial distribution is greater than or equal to alpha (this
    // method is called in the adaptive proportion test to compute the cutoff value C), see Table 2 of NIST SP800-90B.
    @SuppressWarnings("SameParameterValue")
    static private int critBinom(int W, double p, double alpha) {
        var bigDecimalAlpha = BigDecimal.valueOf(alpha);
        var cumulatedValue = BigDecimal.ZERO;
        for (int k = 0; k <= W; k++) {
            cumulatedValue = cumulatedValue.add(binomialDistribution(W, k, p));
            if (cumulatedValue.compareTo(bigDecimalAlpha) >= 0)
                return k;
        }
        throw new UtilityException(INVALID_PARAMETERS, Entropy.class, "No value k found");
    }

    // computes the binomial distribution relative to n, k, and p
    static private BigDecimal binomialDistribution(int n, int k, double p) {
        var P = BigDecimal.valueOf(p);
        return new BigDecimal(binomialCoefficient(n, k)).multiply(P.pow(k)).multiply(BigDecimal.ONE.subtract(P).pow(n - k));
    }

    // computes the binomial coefficient relative to n and k
    static private BigInteger binomialCoefficient(int n, int k) {
        k = Math.min(k, n - k);
        var result = BigInteger.ONE;
        for (int i = 0; i < k; i++) {
            result = result.multiply(BigInteger.valueOf(n - i)).divide(BigInteger.valueOf(i + 1));
        }
        return result;
    }

}
