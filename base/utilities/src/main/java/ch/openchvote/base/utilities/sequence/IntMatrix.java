/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence;

import ch.openchvote.base.utilities.tools.IntBuilder;
import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tools.Hashable;
import ch.openchvote.base.utilities.tools.Math;

import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;

import static ch.openchvote.base.utilities.UtilityException.Type.*;

/**
 * Objects of this class represent immutable matrices of {@code int} values of a fixed height {@code n} and width
 * {@code m}. Their elements are indexed by two indices from {@code minRowIndex} (usually {@code 1}) to
 * {@code maxRowIndex} (usually {@code n}) and {@code minColIndex} (usually {@code 1}) to {@code maxColIndex} (usually
 * {@code m}). Since this class is abstract, it does not offer public constructors. New instances of this class are
 * created using the static member classes {@link IntMatrix.Builder}, {@link IntMatrix.RowBuilder}, and
 * {@link IntMatrix.ColBuilder}. During the building process, values can be added to or placed into an initially empty
 * matrix containing zeros. After the building process, accessing the values in the resulting vector is restricted to
 * read-only. The class implements the {@link IntSequence} interface.
 */
public abstract sealed class IntMatrix extends Hashable implements IntSequence permits IntMatrix.NonSealed {

    // the purpose of this protected non-sealed inner class is to allow anonymous subclasses exclusively within this package
    static protected non-sealed abstract class NonSealed extends IntMatrix {

        // protected constructor for internal use in anonymous subclasses
        protected NonSealed(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            super(minRowIndex, maxRowIndex, minColIndex, maxColIndex);
        }

    }

    /**
     * Returns the {@code int} value at the given row and column index. For an invalid indices, an exception is thrown.
     *
     * @param rowIndex The row index
     * @param colIndex The column index
     * @return The {@code int} value at this row and column index
     */
    public abstract int getValue(int rowIndex, int colIndex);

    // the minimal/maximal row and column indices
    protected final int minRowIndex;
    protected final int maxRowIndex;
    protected final int minColIndex;
    protected final int maxColIndex;

    // protected constructor for internal use in subclasses
    protected IntMatrix(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
        this.minRowIndex = minRowIndex;
        this.maxRowIndex = maxRowIndex;
        this.minColIndex = minColIndex;
        this.maxColIndex = maxColIndex;
    }

    /**
     * Returns the minimal row index of this matrix.
     *
     * @return The minimal row index
     */
    public int getMinRowIndex() {
        return this.minRowIndex;
    }

    /**
     * Returns the maximal row index of this matrix.
     *
     * @return The maximal row index
     */
    public int getMaxRowIndex() {
        return this.maxRowIndex;
    }

    /**
     * Returns the minimal column index of this matrix.
     *
     * @return The minimal column index
     */
    public int getMinColIndex() {
        return this.minColIndex;
    }

    /**
     * Returns the maximal column index of this matrix.
     *
     * @return The maximal column index
     */
    public int getMaxColIndex() {
        return this.maxColIndex;
    }

    /**
     * Returns the set of the matrix's row indices.
     *
     * @return The set of row indices
     */
    public IntSet getRowIndices() {
        return IntSet.range(this.getMinRowIndex(), this.getMaxRowIndex());
    }

    /**
     * Returns the set of the matrix's column indices.
     *
     * @return The set of column indices
     */
    public IntSet getColIndices() {
        return IntSet.range(this.getMinColIndex(), this.getMaxColIndex());
    }

    /**
     * Returns the height of the matrix
     *
     * @return The height of the matrix
     */
    public int getHeight() {
        return this.maxRowIndex - this.minRowIndex + 1;
    }

    /**
     * Returns the width of the matrix
     *
     * @return The width of the matrix
     */
    public int getWidth() {
        return this.maxColIndex - this.minColIndex + 1;
    }

    /**
     * Returns a vector consisting of the matrix's row vectors.
     *
     * @return The vector of row vectors
     */
    public Vector<IntVector> getRows() {
        return new Vector.NonSealed<>(this.minRowIndex, this.maxRowIndex) {
            @Override
            public IntVector getValue(int index) {
                return IntMatrix.this.getRow(index);
            }
        };
    }

    /**
     * Returns a vector consisting of the matrix's column vectors.
     *
     * @return The vector of column vectors
     */
    public Vector<IntVector> getCols() {
        return new Vector.NonSealed<>(this.minColIndex, this.maxColIndex) {
            @Override
            public IntVector getValue(int index) {
                return IntMatrix.this.getCol(index);
            }
        };
    }

    /**
     * Returns the row vector at the given index.
     *
     * @param rowIndex The given row index
     * @return The row vector at the given index
     */
    public IntVector getRow(int rowIndex) {
        if (rowIndex < this.minRowIndex || rowIndex > this.maxRowIndex) {
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, IntMatrix.class, "Index smaller than minIndex or larger than maxIndex");
        }
        return new IntVector.NonSealed(this.minColIndex, this.maxColIndex) {
            @Override
            public int getValue(int index) {
                return IntMatrix.this.getValue(rowIndex, index);
            }
        };
    }

    /**
     * Returns the column vector at the given index.
     *
     * @param colIndex The given column index
     * @return The column vector at the given index
     */
    public IntVector getCol(int colIndex) {
        if (colIndex < this.minColIndex || colIndex > this.maxColIndex) {
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, IntMatrix.class, "Index smaller than minIndex or larger than maxIndex");
        }
        return new IntVector.NonSealed(this.minRowIndex, this.maxRowIndex) {
            @Override
            public int getValue(int index) {
                return IntMatrix.this.getValue(index, colIndex);
            }
        };
    }

    /**
     * Creates a new {@code int} matrix by applying a function to each {@code int} value of the matrix. By returning a
     * wrapper object which performs the mapping lazily, this method runs ins constant time.
     *
     * @param function The function that maps the values of the matrix
     * @return A matrix containing all mapped values
     */
    public IntMatrix map(IntUnaryOperator function) {
        return new IntMatrix.NonSealed(this.minRowIndex, this.maxRowIndex, this.minColIndex, this.maxColIndex) {
            @Override
            public int getValue(int rowIndex, int colIndex) {
                return function.applyAsInt(IntMatrix.this.getValue(rowIndex, colIndex));
            }
        };
    }

    /**
     * Creates a new matrix by applying a function to each value of the matrix. The type {@code W} of the returned
     * matrix depends on the specified function. By returning a wrapper object which performs the mapping lazily, this
     * method runs ins constant time.
     *
     * @param function The function that maps the values of the matrix
     * @param <W>      The type of the returned matrix
     * @return A matrix containing all mapped values
     */

    public <W> Matrix<W> map(IntFunction<? extends W> function) {
        return new Matrix.NonSealed<>(this.minRowIndex, this.maxRowIndex, this.minColIndex, this.maxColIndex) {
            @Override
            public W getValue(int rowIndex, int colIndex) {
                return function.apply(IntMatrix.this.getValue(rowIndex, colIndex));
            }
        };
    }

    /**
     * Returns the transposed matrix, in which the row and column indices and the height and width are swapped.
     *
     * @return The transposed matrix
     */
    public IntMatrix transpose() {
        return new IntMatrix.NonSealed(this.minColIndex, this.maxColIndex, this.minRowIndex, this.maxRowIndex) {
            @Override
            public int getValue(int rowIndex, int colIndex) {
                return IntMatrix.this.getValue(colIndex, rowIndex);
            }

            @Override
            public IntMatrix transpose() {
                return IntMatrix.this;
            }
        };
    }

    /**
     * Computes and returns the matrix product {@code C=AB} of two int matrices A={@code this} and B={@code other}. The
     * result is a new matrix with the same amount of rows as A and the same amount of columns as B. For this operation
     * to work, the number of columns of A must be equal to the number of rows of B. Otherwise, an exception is thrown.
     *
     * @param other The second matrix
     * @return The matrix product of the two matrices
     */
    public IntMatrix multiply(IntMatrix other) {
        if (other == null || this.minColIndex != other.getMinRowIndex() || this.maxColIndex != other.getMaxRowIndex())
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, IntMatrix.class, this, other);
        return new IntMatrix.NonSealed(this.minRowIndex, this.maxRowIndex, other.minColIndex, other.maxColIndex) {
            @Override
            public int getValue(int rowIndex, int colIndex) {
                return Math.intSumProd(IntMatrix.this.getRow(rowIndex), other.getCol(colIndex));
            }
        };
    }

    /**
     * Computes the matrix-vector multiplication {@code Ax} of matrix A={@code this} and x={@code vector}. The result is
     * a new vector of the same size. For this operation to work, the number of columns of A must be equal to the length
     * of x. Otherwise, an exception is thrown.
     *
     * @param vector The given vector
     * @return The matrix-vector multiplication
     */
    public IntVector multiply(IntVector vector) {
        if (vector == null || this.minColIndex != vector.getMinIndex() || this.maxColIndex != vector.getMaxIndex())
            throw new UtilityException(INDEX_OUT_OF_BOUNDS, IntMatrix.class, this, vector);
        return new IntVector.NonSealed(this.minRowIndex, this.maxRowIndex) {
            @Override
            public int getValue(int index) {
                return Math.intSumProd(IntMatrix.this.getRow(index), vector);
            }
        };
    }

    /**
     * Applies the selection operation as defined by {@link Vector#select(IntVector)} to each row of the matrix. The
     * result is therefore a matrix with the same height, but with the width equal to the length of the given index
     * vector.
     *
     * @param indexVector The given index vector
     * @return The resulting matrix of selected values
     */
    public IntMatrix select(IntVector indexVector) {
        if (indexVector == null)
            throw new UtilityException(NULL_POINTER, IntMatrix.class);
        return new IntMatrix.NonSealed(this.minRowIndex, this.maxRowIndex, indexVector.getMinIndex(), indexVector.getMaxIndex()) {
            @Override
            public int getValue(int rowIndex, int colIndex) {
                return IntMatrix.this.getValue(rowIndex, indexVector.getValue(colIndex));
            }
        };
    }

    /**
     * Returns a new int matrix containing the same rows sorted in ascending order.
     *
     * @return The sorted matrix
     */
    public IntMatrix rowSort() {
        var builder = new RowBuilder(this.minRowIndex, this.maxRowIndex, this.minColIndex, this.maxColIndex);
        this.getRows().toStream().sorted().forEach(builder::addRow);
        return builder.build();
    }

    /**
     * Returns a new int matrix containing the same columns sorted in ascending order.
     *
     * @return The sorted matrix
     */
    public IntMatrix colSort() {
        var builder = new ColBuilder(this.minRowIndex, this.maxRowIndex, this.minColIndex, this.maxColIndex);
        this.getCols().toStream().sorted().forEach(builder::addCol);
        return builder.build();
    }

    @Override
    public int getMinIndex() {
        return 0;
    }

    @Override
    public int getMaxIndex() {
        return this.getWidth() * this.getHeight() - 1;
    }

    @Override
    public int getValue(int index) {
        int rowIndex = index / this.getWidth() + this.minRowIndex;
        int colIndex = index % this.getWidth() + this.minColIndex;
        return this.getValue(rowIndex, colIndex);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object instanceof IntMatrix other) {
            if (this.minRowIndex != other.minRowIndex) return false;
            if (this.maxRowIndex != other.maxRowIndex) return false;
            if (this.minColIndex != other.minColIndex) return false;
            if (this.maxColIndex != other.maxColIndex) return false;
            return this.getIndices().toIntStream().allMatch(index -> this.getValue(index) == other.getValue(index));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int initValue = 0;
        initValue = 31 * initValue + this.minRowIndex;
        initValue = 31 * initValue + this.maxRowIndex;
        initValue = 31 * initValue + this.minColIndex;
        initValue = 31 * initValue + this.maxColIndex;
        return this.toIntStream().reduce(initValue, (result, hash) -> 31 * result + hash);
    }

    @Override
    public String toString() {
        if (this.getHeight() == 0 || this.getWidth() == 0) {
            return "[]\n";
        }
        int maxLength = this.toIntStream().mapToObj(Integer::toString).mapToInt(String::length).max().orElse(0);
        var valueFormat = "%" + maxLength + "s";
        var stringBuilder = new StringBuilder();
        for (int i : IntSet.range(0, this.getHeight() - 1)) {
            String prefix, suffix;
            if (this.getHeight() == 1) {
                prefix = "[";
                suffix = "]";
            } else if (i == 0) {
                prefix = "⌈";
                suffix = "⌉";
            } else if (i == this.getHeight() - 1) {
                prefix = "⌊";
                suffix = "⌋";
            } else {
                prefix = "|";
                suffix = "|";
            }
            var row = this.getRow(this.getMinRowIndex() + i).toIntStream()
                    .mapToObj(value -> String.format(valueFormat, value))
                    .collect(Collectors.joining(" ", prefix, suffix));
            stringBuilder.append(row);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    /**
     * This static builder class is the main tool for constructing {@code int} matrices from scratch. The height and
     * width of the matrix are defined at the beginning of the building process. All values are initially all set to
     * {@code 0}. Values can be added either incrementally (row-wise from left to right) or in arbitrary order. At the
     * end of the building process, the matrix can be built exactly once. The builder class is threadsafe and the
     * resulting matrix is immutable.
     */
    static public class Builder implements IntBuilder<IntMatrix> {

        // flag that determines whether the matrix has already been built or not
        private boolean built;
        // the first and the last valid row indices
        private final int minRowIndex;
        private final int maxRowIndex;
        // the first and the last valid column indices
        private final int minColIndex;
        private final int maxColIndex;
        // an array for storing the added values during the building process
        private final int[][] values;
        // an internal index counter for adding values incrementally
        private int indexCounter;

        /**
         * Constructs a matrix builder for a matrix of fixed {@code height} and {@code width}. Indexing starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width  The width of the matrix to construct
         */
        public Builder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a matrix builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1} and width
         * {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        public Builder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1) {
                throw new UtilityException(INVALID_PARAMETERS, IntMatrix.Builder.class, "Invalid minimal or maximal indices");
            }
            this.built = false;
            this.minRowIndex = minRowIndex;
            this.maxRowIndex = maxRowIndex;
            this.minColIndex = minColIndex;
            this.maxColIndex = maxColIndex;
            this.values = new int[maxRowIndex - minRowIndex + 1][maxColIndex - minColIndex + 1];
            this.indexCounter = 0;
        }

        /**
         * Fills up the matrix with a single value.
         *
         * @param value The value used for filling up the matrix
         * @return The matrix builder itself
         */
        public IntMatrix.Builder fill(int value) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, IntMatrix.Builder.class);
                }
                for (int i : IntSet.range(this.minRowIndex, this.maxRowIndex)) {
                    for (int j : IntSet.range(this.minColIndex, this.maxColIndex)) {
                        this.set(i, j, value);
                    }
                }
                return this;
            }
        }

        /**
         * Sets the given value at the given row and column indices. The matrix builder object itself is returned to
         * allow pipeline notation when multiple values are added to the matrix.
         *
         * @param rowIndex The row index of the value to be added
         * @param colIndex The column index of the value to be added
         * @param value    The value to be added
         * @return The matrix builder itself
         */
        public IntMatrix.Builder set(int rowIndex, int colIndex, int value) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, IntMatrix.Builder.class);
                }
                if (rowIndex < this.minRowIndex || rowIndex > this.maxRowIndex || colIndex < this.minColIndex || colIndex > this.maxColIndex) {
                    throw new UtilityException(INDEX_OUT_OF_BOUNDS, IntMatrix.Builder.class, "Index smaller than minIndex or larger than maxIndex");
                }
                this.values[rowIndex - this.minRowIndex][colIndex - this.minColIndex] = value;
                return this;
            }
        }

        /**
         * Sets the next value in the matrix and increases the internal index counter by 1. In this way, values are
         * added row-wise from left to right. The matrix builder object itself is returned to allow pipeline notation
         * when multiple values are added to the matrix.
         *
         * @param value The value to be added
         * @return The matrix builder itself
         */
        public IntMatrix.Builder add(int value) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, IntMatrix.Builder.class);
                }
                int width = this.maxColIndex - this.minColIndex + 1;
                int currentRowIndex, currentColIndex;
                currentRowIndex = this.minRowIndex + this.indexCounter / width;
                currentColIndex = this.minColIndex + this.indexCounter % width;
                this.indexCounter = this.indexCounter + 1;
                return this.set(currentRowIndex, currentColIndex, value);
            }
        }

        @Override
        public IntMatrix build() {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, IntMatrix.Builder.class);
                }
                this.built = true;
                return new IntMatrix.NonSealed(this.minRowIndex, this.maxRowIndex, this.minColIndex, this.maxColIndex) {
                    @Override
                    public int getValue(int rowIndex, int colIndex) {
                        if (rowIndex < Builder.this.minRowIndex || rowIndex > Builder.this.maxRowIndex || colIndex < Builder.this.minColIndex || colIndex > Builder.this.maxColIndex) {
                            throw new UtilityException(INDEX_OUT_OF_BOUNDS, IntMatrix.class, "Index smaller than minIndex or larger than maxIndex");
                        }
                        return Builder.this.values[rowIndex - Builder.this.minRowIndex][colIndex - Builder.this.minColIndex];
                    }
                };
            }
        }

    }

    /**
     * This static builder class can be used for constructing {@code int} matrices from given row vectors. The height
     * and width of the matrix are defined at the beginning of the building process. All values are initially all set to
     * {@code 0}. Rows can be added either incrementally or in arbitrary order. At the end of the building process, the
     * matrix can be built exactly once. The builder class is threadsafe and the resulting matrix is immutable.
     */
    static public class RowBuilder {

        // uses a vector builder internally
        private final Vector.Builder<IntVector> vectorBuilder;
        private final int minColIndex;
        private final int maxColIndex;

        /**
         * Constructs a new row-wise matrix builder for a matrix of fixed {@code height} and {@code width}. Indexing of
         * both the rows and columns starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width  The width of the matrix to construct
         */
        public RowBuilder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a new row-wise matrix builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1} and
         * width {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        public RowBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1) {
                throw new UtilityException(INVALID_PARAMETERS, IntMatrix.RowBuilder.class, "Invalid minimal or maximal indices");
            }
            this.vectorBuilder = new Vector.Builder<>(minRowIndex, maxRowIndex);
            this.minColIndex = minColIndex;
            this.maxColIndex = maxColIndex;
        }

        /**
         * Sets the given row vector at the given index. The matrix builder object itself is returned to allow pipeline
         * notation when multiple rows are added to the matrix.
         *
         * @param rowIndex The row index of the row vector to be added
         * @param row      The row vector to be added
         * @return The matrix builder itself
         */
        public RowBuilder setRow(int rowIndex, IntVector row) {
            if (row == null || row.getMinIndex() != this.minColIndex || row.getMaxIndex() != this.maxColIndex) {
                throw new UtilityException(INVALID_PARAMETERS, IntMatrix.RowBuilder.class, "Invalid minimal or maximal indices");
            }
            this.vectorBuilder.set(rowIndex, row);
            return this;
        }

        /**
         * Sets the next row vector in the matrix and increases the internal index counter by 1. In this way, row
         * vectors are added incrementally. The matrix builder object itself is returned to allow pipeline notation when
         * multiple row vectors are added to the matrix.
         *
         * @param row The row vector to be added
         * @return The matrix builder itself
         */
        public RowBuilder addRow(IntVector row) {
            if (row == null || row.getMinIndex() != this.minColIndex || row.getMaxIndex() != this.maxColIndex) {
                throw new UtilityException(INVALID_PARAMETERS, IntMatrix.RowBuilder.class, "Invalid minimal or maximal indices");
            }
            this.vectorBuilder.add(row);
            return this;
        }

        /**
         * Terminates the building process and returns the constructed matrix.
         *
         * @return The constructed matrix
         */
        public IntMatrix build() {
            Vector<IntVector> vector = this.vectorBuilder.build();
            return new IntMatrix.NonSealed(vector.getMinIndex(), vector.getMaxIndex(), this.minColIndex, this.maxColIndex) {
                @Override
                public int getValue(int rowIndex, int colIndex) {
                    var row = vector.getValue(rowIndex);
                    return row == null ? 0 : row.getValue(colIndex);
                }
            };
        }

    }

    /**
     * This static builder class can be used for constructing {@code int} matrices from given column vectors. The height
     * and width of the matrix are defined at the beginning of the building process. All values are initially all set to
     * {@code 0}. Columns can be added either incrementally or in arbitrary order. At the end of the building process,
     * the matrix can be built exactly once. The builder class is threadsafe and the resulting matrix is immutable.
     */
    static public class ColBuilder {

        // uses a vector builder internally
        private final Vector.Builder<IntVector> vectorBuilder;
        private final int minRowIndex;
        private final int maxRowIndex;

        /**
         * Constructs a new column-wise matrix builder for a matrix of fixed {@code height} and {@code width}. Indexing
         * of both the rows and columns starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width  The width of the matrix to construct
         */
        public ColBuilder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a new column-wise matrix builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1}
         * and width {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        public ColBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1) {
                throw new UtilityException(INVALID_PARAMETERS, IntMatrix.ColBuilder.class, "Invalid minimal or maximal indices");
            }
            this.minRowIndex = minRowIndex;
            this.maxRowIndex = maxRowIndex;
            this.vectorBuilder = new Vector.Builder<>(minColIndex, maxColIndex);
        }

        /**
         * Sets the given column vector at the given index. The matrix builder object itself is returned to allow
         * pipeline notation when multiple rows are added to the matrix.
         *
         * @param colIndex The column index of the column vector to be added
         * @param col      The column vector to be added
         * @return The matrix builder itself
         */
        public ColBuilder setCol(int colIndex, IntVector col) {
            if (col == null || col.getMinIndex() != this.minRowIndex || col.getMaxIndex() != this.maxRowIndex) {
                throw new UtilityException(INVALID_PARAMETERS, IntMatrix.ColBuilder.class, "Invalid minimal or maximal indices");
            }
            this.vectorBuilder.set(colIndex, col);
            return this;
        }

        /**
         * Sets the next column vector in the matrix and increases the internal index counter by 1. In this way, column
         * vectors are added incrementally. The matrix builder object itself is returned to allow pipeline notation when
         * multiple column vectors are added to the matrix.
         *
         * @param col The column vector to be added
         * @return The matrix builder itself
         */
        public ColBuilder addCol(IntVector col) {
            if (col == null || col.getMinIndex() != this.minRowIndex || col.getMaxIndex() != this.maxRowIndex) {
                throw new UtilityException(INVALID_PARAMETERS, IntMatrix.ColBuilder.class, "Invalid minimal or maximal indices");
            }
            this.vectorBuilder.add(col);
            return this;
        }

        /**
         * Terminates the building process and returns the constructed matrix.
         *
         * @return The constructed matrix
         */
        public IntMatrix build() {
            Vector<IntVector> vector = this.vectorBuilder.build();
            return new IntMatrix.NonSealed(this.minRowIndex, this.maxRowIndex, vector.getMinIndex(), vector.getMaxIndex()) {
                @Override
                public int getValue(int rowIndex, int colIndex) {
                    var col = vector.getValue(colIndex);
                    return col == null ? 0 : col.getValue(rowIndex);
                }
            };
        }

    }

}
