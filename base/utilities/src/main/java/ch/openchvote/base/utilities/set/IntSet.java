/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.set;

import ch.openchvote.base.utilities.tools.IntBuilder;
import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.sequence.IntSequence;
import ch.openchvote.base.utilities.tools.IntStreamable;

import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static ch.openchvote.base.utilities.UtilityException.Type.ALREADY_BUILT;
import static ch.openchvote.base.utilities.UtilityException.Type.INVALID_PARAMETERS;

/**
 * This class implements the generic interface {@link FiniteSet} for elements of type {@code int}. The main purpose of
 * this interface is to avoid unnecessary boxing/unboxing operations between the primitive type {@code int} and the
 * wrapper class {@code Integer} while conducting membership tests. Since this class is abstract, it does not offer
 * public constructors. Iterating through the element of an integer set must always produce the same ascending order.
 */
public abstract sealed class IntSet implements FiniteSet<Integer, Long>, IntStreamable permits IntSet.NonSealed {

    // the purpose of this protected non-sealed inner class is to allow anonymous subclasses exclusively within this package
    static protected non-sealed abstract class NonSealed extends IntSet {

    }

    // defines the maximum number of processed elements when calling toString()
    static private final int TO_STRING_LIMIT = 100;

    /**
     * Performs the membership test on a single element {@code x} of type {@code int}. Returns {@code true}, if
     * {@code x} is a member of the set, and {@code false} otherwise.
     *
     * @param x The given integer
     * @return {@code true} if {@code x} is a member of the set
     */
    public abstract boolean contains(int x);

    /**
     * Computes the union of this set and the other set.
     *
     * @param other The other set
     * @return The union of this and the other set
     */
    public IntSet union(IntSet other) {
        return new IntSet.NonSealed() {
            @Override
            public boolean contains(int x) {
                return IntSet.this.contains(x) || other.contains(x);
            }

            @Override
            public IntStream toIntStream() {
                return IntStream.concat(IntSet.this.toIntStream(), other.toIntStream()).distinct().sorted();
            }

            @Override
            public Long getSize() {
                return this.toIntStream().count();
            }
        };
    }

    /**
     * Computes the intersection of this set and the other set.
     *
     * @param other The other set
     * @return The union of this and the other set
     */
    public IntSet intersection(IntSet other) {
        return new IntSet.NonSealed() {
            @Override
            public boolean contains(int x) {
                return IntSet.this.contains(x) && other.contains(x);
            }

            @Override
            public IntStream toIntStream() {
                return IntSet.this.toIntStream().filter(other::contains); // already sorted before filtering
            }

            @Override
            public Long getSize() {
                return this.toIntStream().count();
            }
        };
    }

    /**
     * Computes the difference of this set and the other set.
     *
     * @param other The other set
     * @return The difference of this and the other set
     */
    public IntSet difference(IntSet other) {
        return new IntSet.NonSealed() {
            @Override
            public boolean contains(int x) {
                return IntSet.this.contains(x) && !other.contains(x);
            }

            @Override
            public IntStream toIntStream() {
                return IntSet.this.toIntStream().filter(x -> !other.contains(x)); // already sorted before filtering
            }

            @Override
            public Long getSize() {
                return this.toIntStream().count();
            }
        };
    }

    /**
     * Performs the membership test on all elements of a given integer stream. Returns {@code true}, if all elements are
     * members of this set. If the stream itself is {@code null}, the membership test returns {@code false}.
     *
     * @param stream The given integer stream
     * @return {@code true} if all elements are members of this set, {@code false} otherwise
     */
    public boolean containsAll(IntStream stream) {
        return stream != null && stream.allMatch(this::contains);
    }

    /**
     * Performs the membership test on all objects of a given IntSequence. Returns {@code true}, if all elements are
     * members of this set. If the IntSequence itself is {@code null}, the membership test returns {@code false}.
     *
     * @param sequence The given sequence
     * @return {@code true} if all elements are members of this set, {@code false} otherwise
     */
    public boolean containsAll(IntSequence sequence) {
        return sequence != null && sequence.toIntStream().allMatch(this::contains);
    }

    /**
     * Performs the membership test on all objects of a given set. Returns {@code true}, if all elements of the given
     * are set are members of this set. If the given set itself is {@code null}, the membership test returns
     * {@code false}.
     *
     * @param set The given set
     * @return {@code true} if all elements are members of this set, {@code false} otherwise
     */
    public boolean containsAll(IntSet set) {
        return set != null && this.containsAll(set.toIntStream());
    }

    /**
     * Returns a stream containing all the integers of the set in ascending order.
     *
     * @return A stream of all integers in ascending order
     */
    @Override
    public abstract IntStream toIntStream();

    @Override
    public boolean contains(Integer x) {
        return x != null && this.contains(x.intValue());
    }

    @Override
    public String toString() {
        var strStream = this.toIntStream().mapToObj(Integer::toString);
        if (this.getSize() > TO_STRING_LIMIT) {
            strStream = Stream.concat(strStream.limit(TO_STRING_LIMIT), Stream.of("..."));
        }
        return strStream.collect(Collectors.joining(",", "{", "}"));
    }

    /**
     * Creates an integer set containing the integers from the specified range. The range itself is specified by its
     * lower and upper bounds, i.e., it corresponds to the set {lb,...,up}.
     *
     * @param lb The lower bound of the integer range
     * @param ub The upper bound of the integer range
     * @return The set of integers within the given range
     */
    static public IntSet range(int lb, int ub) {
        return new IntSet.NonSealed() {
            @Override
            public boolean contains(int x) {
                return lb <= x && x <= ub;
            }

            @Override
            public IntStream toIntStream() {
                return IntStream.rangeClosed(lb, ub); // automatically sorted
            }

            @Override
            public Long getSize() {
                return (long) ub - lb + 1;
            }
        };
    }

    /**
     * The set of non-negative integers {0,1,2,...}. The finite domain of the {@code int} data type limits this set to
     * integers smaller or equal to {@code Integer.MAX_VALUE}.
     */
    static public final IntSet NN = IntSet.range(0, Integer.MAX_VALUE);

    /**
     * The set of positive integers {1,2,3,...}. The finite domain of the {@code int} data type limits this set to
     * integers smaller or equal to {@code Integer.MAX_VALUE}.
     */
    static public final IntSet NN_plus = IntSet.range(1, Integer.MAX_VALUE);

    /**
     * Creates the set {1,...,n} of positive integers smaller or equal to {@code n}.
     *
     * @param n The upper bound of the range
     * @return The set of positive integers smaller or equal to {@code n}
     */
    static public IntSet NN_plus(int n) {
        if (n < 0) {
            throw new UtilityException(INVALID_PARAMETERS, IntSet.class, "Value n must be non-negative");
        }
        return IntSet.range(1, n);
    }

    /**
     * Returns the set {0,...,n-1} of integers modulo {@code n}.
     *
     * @param n The modulo
     * @return The set of integers modulo {@code n}
     */
    static public IntSet ZZ(int n) {
        if (n < 0) {
            throw new UtilityException(INVALID_PARAMETERS, IntSet.class, "Value n must be non-negative");
        }
        return IntSet.range(0, n - 1);
    }

    /**
     * The set of integers {0,1}.
     */
    static public final IntSet BB = IntSet.range(0, 1);

    /**
     * Creates the set of all integers contained in the given varargs parameter {@code integers}.
     *
     * @param integers An array of integers
     * @return The set of integers contained in {@code integers}
     */
    static public IntSet of(Integer... integers) {
        var set = java.util.Set.of(integers);
        return new IntSet.NonSealed() {
            @Override
            public boolean contains(int x) {
                return set.contains(x);
            }

            @Override
            public IntStream toIntStream() {
                return set.stream().mapToInt(Integer::intValue).sorted();
            }

            @Override
            public Long getSize() {
                return (long) set.size();
            }
        };
    }

    /**
     * Creates the set of all integers contained in the given {@code java.util.Set} of integers.
     *
     * @param integers A set of integers
     * @return The set of integers contained in {@code integers}
     */
    static public IntSet of(java.util.Set<Integer> integers) {
        var intArray = integers.toArray(new Integer[0]);
        return IntSet.of(intArray);
    }

    /**
     * This class allows building an integer set by adding individual integers to an initially empty set. When the
     * building process is over, the construction of the integer set can be finalized (using
     * {@link IntSet.Builder#build}). While the resulting integer set offers read-only access, the builder offers
     * write-only access. The builder itself is threadsafe.
     */
    static public class Builder implements IntBuilder<IntSet> {

        private final java.util.Set<Integer> set;
        private boolean built;

        /**
         * Constructs a new builder for an integer set.
         */
        public Builder() {
            this.set = new HashSet<>();
            this.built = false;
        }

        @Override
        public IntSet.Builder add(int element) {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, IntSet.Builder.class);
                }
                this.set.add(element);
                return this;
            }
        }

        @Override
        public IntSet build() {
            synchronized (this) {
                if (this.built) {
                    throw new UtilityException(ALREADY_BUILT, IntSet.Builder.class);
                }
                this.built = true;
                return IntSet.of(this.set);
            }
        }

    }

}