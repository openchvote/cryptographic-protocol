/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples;

import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.tools.Hashable;
import ch.openchvote.base.utilities.tools.Streamable;
import ch.openchvote.base.utilities.tuples.decuple.*;
import ch.openchvote.base.utilities.tuples.viguple.*;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This abstract class is the base class for implementing tuples (pairs, triples, etc.) of different lengths. Tuples are
 * immutable objects.
 */
public abstract sealed class Tuple extends Hashable implements Streamable<Object> permits NullTuple, Singleton, Pair, Triple, Quadruple, Quintuple,
        Sextuple, Septuple, Octuple, Nonuple, Decuple, UnDecuple, DuoDecuple, TreDecuple, QuattuorDecuple, QuinDecuple,
        SexDecuple, SeptenDecuple, OctoDecuple, NovemDecuple, Viguple, UnViguple, DuoViguple, TreViguple, QuattuorViguple,
        QuinViguple, SexViguple, SeptenViguple, OctoViguple, NovemViguple {

    /**
     * Constructs a tuple with all generic types set to {@link Object}. The type of the tuple depends on the the given
     * number of arguments.
     *
     * @param objects The given objects
     * @return A tuple containing all given objects
     */
    public static Tuple of(Object... objects) {
        if (objects == null) {
            throw new UtilityException(UtilityException.Type.NULL_POINTER, Tuple.class);
        }
        return switch (objects.length) {
            case 0 -> new NullTuple();
            case 1 -> new Singleton<>(objects[0]);
            case 2 -> new Pair<>(objects[0], objects[1]);
            case 3 -> new Triple<>(objects[0], objects[1], objects[2]);
            case 4 -> new Quadruple<>(objects[0], objects[1], objects[2], objects[3]);
            case 5 -> new Quintuple<>(objects[0], objects[1], objects[2], objects[3], objects[4]);
            case 6 -> new Sextuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5]);
            case 7 ->
                    new Septuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6]);
            case 8 ->
                    new Octuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7]);
            case 9 ->
                    new Nonuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8]);
            case 10 ->
                    new Decuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9]);
            case 11 ->
                    new UnDecuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10]);
            case 12 ->
                    new DuoDecuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11]);
            case 13 ->
                    new TreDecuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12]);
            case 14 ->
                    new QuattuorDecuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13]);
            case 15 ->
                    new QuinDecuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14]);
            case 16 ->
                    new SexDecuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15]);
            case 17 ->
                    new SeptenDecuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16]);
            case 18 ->
                    new OctoDecuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17]);
            case 19 ->
                    new NovemDecuple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17], objects[18]);
            case 20 ->
                    new Viguple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17], objects[18], objects[19]);
            case 21 ->
                    new UnViguple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17], objects[18], objects[19], objects[20]);
            case 22 ->
                    new DuoViguple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17], objects[18], objects[19], objects[20], objects[21]);
            case 23 ->
                    new TreViguple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17], objects[18], objects[19], objects[20], objects[21], objects[22]);
            case 24 ->
                    new QuattuorViguple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17], objects[18], objects[19], objects[20], objects[21], objects[22], objects[23]);
            case 25 ->
                    new QuinViguple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17], objects[18], objects[19], objects[20], objects[21], objects[22], objects[23], objects[24]);
            case 26 ->
                    new SexViguple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17], objects[18], objects[19], objects[20], objects[21], objects[22], objects[23], objects[24], objects[25]);
            case 27 ->
                    new SeptenViguple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17], objects[18], objects[19], objects[20], objects[21], objects[22], objects[23], objects[24], objects[25], objects[26]);
            case 28 ->
                    new OctoViguple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17], objects[18], objects[19], objects[20], objects[21], objects[22], objects[23], objects[24], objects[25], objects[26], objects[27]);
            case 29 ->
                    new NovemViguple<>(objects[0], objects[1], objects[2], objects[3], objects[4], objects[5], objects[6], objects[7], objects[8], objects[9], objects[10], objects[11], objects[12], objects[13], objects[14], objects[15], objects[16], objects[17], objects[18], objects[19], objects[20], objects[21], objects[22], objects[23], objects[24], objects[25], objects[26], objects[27], objects[28]);
            default -> throw new UtilityException(UtilityException.Type.UNSUPPORTED_OPERATION, Tuple.class);
        };
    }

    @Override
    public abstract Stream<Object> toStream();

    @Override
    public String toString() {
        var prefix = "";
        if (!this.getClass().getSuperclass().equals(Tuple.class)) {
            prefix = this.getClass().getSimpleName();
        }
        return prefix + this.toStream().map(Object::toString).collect(Collectors.joining(",", "(", ")"));
    }

    @Override
    public int hashCode() {
        return this.toStream()
                .mapToInt(Object::hashCode)
                .reduce(1, (result, hash) -> 31 * result + hash);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object instanceof Tuple other) {
            return this.toVector().equals(other.toVector());
        }
        return false;
    }

}
