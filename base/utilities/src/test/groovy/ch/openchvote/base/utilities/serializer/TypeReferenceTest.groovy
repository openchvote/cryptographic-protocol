/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.serializer

import ch.openchvote.base.utilities.sequence.IntMatrix
import ch.openchvote.base.utilities.tools.Hashable
import ch.openchvote.base.utilities.tuples.Pair
import ch.openchvote.base.utilities.tuples.Triple
import ch.openchvote.base.utilities.sequence.Vector
import spock.lang.Specification

class TypeReferenceTest extends Specification {

    def "getTypeClass should return the type's actual class"() {
        given:
        def typeRef = new TypeReference<String>() {}

        when:
        def typeClass = typeRef.getTypeClass()

        then:
        typeClass == String.class
    }

    def "isInterface should correctly identify if type is an interface"() {
        given:
        def typeRef1 = new TypeReference<List<String>>() {}
        def typeRef2 = new TypeReference<String>() {}

        expect:
        typeRef1.isInterface()
        !typeRef2.isInterface()
    }

    def "isSubtype should correctly determine if a type is a subtype of another"() {
        given:
        def typeRef = new TypeReference<ArrayList<String>>() {}

        expect:
        typeRef.isSubtype(List.class)
        typeRef.isSubtype(Collection.class)
        !typeRef.isSubtype(String.class)
    }

    def "getSupertypes should return a list of supertypes"() {
        given:
        def typeRef = new TypeReference<ArrayList<String>>() {}

        when:
        def supertypes = typeRef.getSupertypes()

        then:
        supertypes.size() == 13
        supertypes*.getTypeClass().toSet() == [Collection.class, List.class, Serializable.class, Iterable.class, RandomAccess, Collection.class, AbstractList.class, ArrayList.class, List.class, Object, Cloneable, SequencedCollection.class, AbstractCollection.class].toSet()
    }

    def "getTypeParameters should return type parameters for a parameterized type"() {
        given:
        def typeRef = new TypeReference<Map<String, Integer>>() {}

        when:
        def typeParams = typeRef.getTypeParameters()

        then:
        typeParams.size() == 2
        typeParams*.getTypeClass() == [String.class, Integer.class]
    }

    def "getTypeReference should return a TypeReference for a specified class if it's a subtype"() {
        given:
        def typeRef = new TypeReference<ArrayList<String>>() {}

        expect:
        typeRef.getTypeReference(Object.class).getTypeClass() == Object.class
        typeRef.getTypeReference(List.class).getTypeClass() == List.class
    }

    def "general test"() {
        given:
        def typeRef = new TypeReference<Vector<Triple<Map<IntMatrix, Pair<Integer, String>>, BigInteger, Iterable<Object>>>>() {}

        expect:
        typeRef.getTypeClass() == Vector.class
        typeRef.getSuperclass().get().getTypeClass() == Hashable.class
        typeRef.getSuperclass().get().getSuperclass().get().getTypeClass() == Object.class
        typeRef.getTypeParameters().size() == 1
        typeRef.getTypeParameters().getFirst().getTypeClass() == Triple.class
        typeRef.getTypeParameters().getFirst().getTypeParameters().size() == 3
        typeRef.getTypeParameters().getFirst().getTypeParameters().get(0).getTypeClass() == Map.class
        typeRef.getTypeParameters().getFirst().getTypeParameters().get(1).getTypeClass() == BigInteger.class
        typeRef.getTypeParameters().getFirst().getTypeParameters().get(2).getTypeClass() == Iterable.class
        typeRef.getTypeParameters().getFirst().getTypeParameters().get(0).getTypeParameters().size() == 2
        typeRef.getTypeParameters().getFirst().getTypeParameters().get(0).getTypeParameters().get(0).getTypeClass() == IntMatrix.class
        typeRef.getTypeParameters().getFirst().getTypeParameters().get(0).getTypeParameters().get(1).getTypeClass() == Pair.class
        typeRef.getTypeParameters().getFirst().getTypeParameters().get(0).getTypeParameters().get(1).getTypeParameters().size() == 2
        typeRef.getTypeParameters().getFirst().getTypeParameters().get(0).getTypeParameters().get(1).getTypeParameters().get(0).getTypeClass() == Integer.class
        typeRef.getTypeParameters().getFirst().getTypeParameters().get(0).getTypeParameters().get(1).getTypeParameters().get(1).getTypeClass() == String.class
        typeRef.getTypeParameters().getFirst().getTypeParameters().get(2).getTypeParameters().get(0).getTypeClass() == Object.class
    }

}