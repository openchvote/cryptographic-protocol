/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Specification

class SingletonSpec extends Specification {
    def "Constructor throws UtilityException for case: #caseDesc"() {
        when:
        new Singleton(first)

        then:
        thrown(UtilityException)

        where:
        caseDesc        | first
        "first is null" | null
    }

    def "Test constructor and getter methods for the elements"() {
        given:
        def singleton = new Singleton(first)

        expect:
        singleton.getFirst() == first

        where:
        first                 | _
        "first"               | _
        1                     | _
        BigInteger.valueOf(1) | _
    }

    @SuppressWarnings('ChangeToOperator')
    def "equals should return true if all pairs of elements of two pairs are the same and false otherwise"() {
        given:
        def singleton1 = new Singleton(elems1.get(0))
        def singleton2 = new Singleton(elems2.get(0))

        expect:
        //noinspection ChangeToOperator
        singleton1.equals(singleton2) == expected
        (singleton1.hashCode() == singleton2.hashCode()) == expected

        where:
        elems1    | elems2    | expected
        ["first"] | ["first"] | true
        ["wrong"] | ["first"] | false
    }

    def "toString()"() {
        given:
        def singleton = new Singleton(elems1.get(0))

        expect:
        singleton.toString() == expected

        where:
        elems1    | expected
        ["first"] | "(first)"
        [1]       | "(1)"
    }

    def "toStream() should return a Stream containing all elements in the pair in the correct order"() {
        given:
        def singleton = new Singleton(elems1.get(0))

        expect:
        singleton.toStream().toList() == expected

        where:
        elems1    | expected
        ["first"] | ["first"]
        [1]       | [1]
    }
}
