/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence

import ch.openchvote.base.utilities.tools.IntBiPredicate
import spock.lang.Specification

import java.util.function.IntFunction
import java.util.function.IntUnaryOperator
class IntMatrixSpec extends Specification {
    def "Builder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex): building a new IntMatrix<V> using the Builder<V> class"() {
        given: "build rows IntVector"
        def rowsBuilder = new Vector.Builder<IntVector>(minRowIndex, maxRowIndex)
        for (def i = 0; i < inputRows.size(); i++) {
            def rowBuilder = new IntVector.Builder(minColIndex, maxColIndex)
            for (def x : inputRows.get(i)) {
                rowBuilder.add(x)
            }
            def row = rowBuilder.build()
            rowsBuilder.add(row)
        }
        def rows = rowsBuilder.build()

        and: "build columns IntVector"
        def columnsBuilder = new Vector.Builder<IntVector>(minColIndex, maxColIndex)
        for (def i = 0; i < inputColumns.size(); i++) {
            def colBuilder = new IntVector.Builder(minRowIndex, maxRowIndex)
            for (def x : inputColumns.get(i)) {
                colBuilder.add(x)
            }
            def column = colBuilder.build()
            columnsBuilder.add(column)
        }
        def columns = columnsBuilder.build()

        and: "build IntMatrix"
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()
        def builder = new IntMatrix.Builder(minRowIndex, maxRowIndex, minColIndex, maxColIndex)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + minRowIndex, j + minColIndex, inputMatrix.get(i).get(j))
            }
        }
        def intMatrix = builder.build()

        expect: "IntMatrix has expected dimensions"
        intMatrix.getMinRowIndex() == minRowIndex
        intMatrix.getMaxRowIndex() == maxRowIndex
        intMatrix.getHeight() == height

        intMatrix.getMinColIndex() == minColIndex
        intMatrix.getMaxColIndex() == maxColIndex
        intMatrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert intMatrix.getValue(minRowIndex + i, minColIndex + j) == inputMatrix.get(i).get(j)
            }
        }

        and: "rows are as expected"
        for (def i = 0; i < height; i++) {
            assert intMatrix.getRow(minRowIndex + i) == rows.getValue(minRowIndex + i)
        }
        intMatrix.getRows() == rows

        and: "columns are as expected"
        for (def i = 0; i < width; i++) {
            assert intMatrix.getCol(minColIndex + i) == columns.getValue(minColIndex + i)
        }
        intMatrix.getCols() == columns

        where:
        minRowIndex | maxRowIndex | minColIndex | maxColIndex | inputMatrix        | inputRows                                                | inputColumns
        1           | 3           | 1           | 3           | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33]]               | [[11, 21, 31], [12, 22, 32], [13, 23, 33]]
        3           | 5           | 4           | 7           | [[11, 12, 13, 14],
                                                                 [21, 22, 23, 24],
                                                                 [31, 32, 33, 34]] | [[11, 12, 13, 14], [21, 22, 23, 24], [31, 32, 33, 34]]   | [[11, 21, 31], [12, 22, 32], [13, 23, 33], [14, 24, 34]]
        7           | 10          | 9           | 11          | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33],
                                                                 [41, 42, 43]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33], [41, 42, 43]] | [[11, 21, 31, 41], [12, 22, 32, 42], [13, 23, 33, 43]]
    }

    def "Builder<V>: building a new Matrix<V> using the Builder<V> class and fill method"() {
        given:

        def builder = new IntMatrix.Builder(height, width)
        builder.fill(value)
        def matrix = builder.build()

        expect:

        and: "Matrix has expected dimensions"
        matrix.getMinRowIndex() == 1
        matrix.getMaxRowIndex() == height
        matrix.getHeight() == height

        matrix.getMinColIndex() == 1
        matrix.getMaxColIndex() == width
        matrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert matrix.getValue(1 + i, 1 + j) == expected.get(i).get(j)
            }
        }

        where:
        value | height | width | expected
        1     | 3      | 3     | [[1, 1, 1],
                                  [1, 1, 1],
                                  [1, 1, 1]]
        26    | 3      | 4     | [[26, 26, 26, 26],
                                  [26, 26, 26, 26],
                                  [26, 26, 26, 26]]
        1291  | 4      | 3     | [[1291, 1291, 1291],
                                  [1291, 1291, 1291],
                                  [1291, 1291, 1291],
                                  [1291, 1291, 1291]]
    }

    def "RowBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex): building a new IntMatrix<V> using the RowBuilder<V> class"() {
        given:
        def rows
        def columns
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build rows IntVector"
        def rowsBuilder = new Vector.Builder<IntVector>(minRowIndex, maxRowIndex)
        for (def i = 0; i < inputRows.size(); i++) {
            def rowBuilder = new IntVector.Builder(minColIndex, maxColIndex)
            for (def x : inputRows.get(i)) {
                rowBuilder.add(x)
            }
            def row = rowBuilder.build()
            rowsBuilder.add(row)
        }
        rows = rowsBuilder.build()

        and: "build columns IntVector"
        def columnsBuilder = new Vector.Builder<IntVector>(minColIndex, maxColIndex)
        for (def i = 0; i < inputColumns.size(); i++) {
            def colBuilder = new IntVector.Builder(minRowIndex, maxRowIndex)
            for (def x : inputColumns.get(i)) {
                colBuilder.add(x)
            }
            def column = colBuilder.build()
            columnsBuilder.add(column)
        }
        columns = columnsBuilder.build()

        and: "build IntMatrix"
        def builder = new IntMatrix.RowBuilder(minRowIndex, maxRowIndex, minColIndex, maxColIndex)
        for (def i = minRowIndex; i <= maxRowIndex; i++) {
            builder.setRow(i, rows.getValue(i))
        }
        def intMatrix = builder.build()

        expect: "IntMatrix has expected dimensions"
        intMatrix.getMinRowIndex() == minRowIndex
        intMatrix.getMaxRowIndex() == maxRowIndex
        intMatrix.getHeight() == height

        intMatrix.getMinColIndex() == minColIndex
        intMatrix.getMaxColIndex() == maxColIndex
        intMatrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert intMatrix.getValue(minRowIndex + i, minColIndex + j) == inputMatrix.get(i).get(j)
            }
        }

        and: "rows are as expected"
        for (def i = 0; i < height; i++) {
            assert intMatrix.getRow(minRowIndex + i) == rows.getValue(minRowIndex + i)
        }
        intMatrix.getRows() == rows

        and: "columns are as expected"
        for (def i = 0; i < width; i++) {
            assert intMatrix.getCol(minColIndex + i) == columns.getValue(minColIndex + i)
        }
        intMatrix.getCols() == columns

        where:
        minRowIndex | maxRowIndex | minColIndex | maxColIndex | inputMatrix        | inputRows                                                | inputColumns
        1           | 3           | 1           | 3           | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33]]               | [[11, 21, 31], [12, 22, 32], [13, 23, 33]]
        3           | 5           | 4           | 7           | [[11, 12, 13, 14],
                                                                 [21, 22, 23, 24],
                                                                 [31, 32, 33, 34]] | [[11, 12, 13, 14], [21, 22, 23, 24], [31, 32, 33, 34]]   | [[11, 21, 31], [12, 22, 32], [13, 23, 33], [14, 24, 34]]
        7           | 10          | 9           | 11          | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33],
                                                                 [41, 42, 43]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33], [41, 42, 43]] | [[11, 21, 31, 41], [12, 22, 32, 42], [13, 23, 33, 43]]
    }

    def "ColBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex): building a new IntMatrix<V> using the ColBuilder<V> class"() {
        given:
        def rows
        def columns
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build rows IntVector"
        def rowsBuilder = new Vector.Builder<IntVector>(minRowIndex, maxRowIndex)
        for (def i = 0; i < inputRows.size(); i++) {
            def rowBuilder = new IntVector.Builder(minColIndex, maxColIndex)
            for (def x : inputRows.get(i)) {
                rowBuilder.add(x)
            }
            def row = rowBuilder.build()
            rowsBuilder.add(row)
        }
        rows = rowsBuilder.build()

        and: "build columns IntVector"
        def columnsBuilder = new Vector.Builder<IntVector>(minColIndex, maxColIndex)
        for (def i = 0; i < inputColumns.size(); i++) {
            def colBuilder = new IntVector.Builder(minRowIndex, maxRowIndex)
            for (def x : inputColumns.get(i)) {
                colBuilder.add(x)
            }
            def column = colBuilder.build()
            columnsBuilder.add(column)
        }
        columns = columnsBuilder.build()

        and: "build IntMatrix"
        def builder = new IntMatrix.ColBuilder(minRowIndex, maxRowIndex, minColIndex, maxColIndex)
        for (def i = minColIndex; i <= maxColIndex; i++) {
            builder.setCol(i, columns.getValue(i))
        }
        def intMatrix = builder.build()

        expect: "IntMatrix has expected dimensions"
        intMatrix.getMinRowIndex() == minRowIndex
        intMatrix.getMaxRowIndex() == maxRowIndex
        intMatrix.getHeight() == height

        intMatrix.getMinColIndex() == minColIndex
        intMatrix.getMaxColIndex() == maxColIndex
        intMatrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert intMatrix.getValue(minRowIndex + i, minColIndex + j) == inputMatrix.get(i).get(j)
            }
        }

        and: "rows are as expected"
        for (def i = 0; i < height; i++) {
            assert intMatrix.getRow(minRowIndex + i) == rows.getValue(minRowIndex + i)
        }
        intMatrix.getRows() == rows

        and: "columns are as expected"
        for (def i = 0; i < width; i++) {
            assert intMatrix.getCol(minColIndex + i) == columns.getValue(minColIndex + i)
        }
        intMatrix.getCols() == columns

        where:
        minRowIndex | maxRowIndex | minColIndex | maxColIndex | inputMatrix        | inputRows                                                | inputColumns
        1           | 3           | 1           | 3           | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33]]               | [[11, 21, 31], [12, 22, 32], [13, 23, 33]]
        3           | 5           | 4           | 7           | [[11, 12, 13, 14],
                                                                 [21, 22, 23, 24],
                                                                 [31, 32, 33, 34]] | [[11, 12, 13, 14], [21, 22, 23, 24], [31, 32, 33, 34]]   | [[11, 21, 31], [12, 22, 32], [13, 23, 33], [14, 24, 34]]
        7           | 10          | 9           | 11          | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33],
                                                                 [41, 42, 43]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33], [41, 42, 43]] | [[11, 21, 31, 41], [12, 22, 32, 42], [13, 23, 33, 43]]
    }

    def "map(IntFunction function): map should apply function to each cell"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build IntMatrix"
        def builder = new IntMatrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def intMatrix = builder.build()

        and: "build expected IntMatrix"
        def eBuilder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                eBuilder.set(i + 1, j + 1, eInputMatrix.get(i).get(j))
            }
        }
        def expected = eBuilder.build()

        expect:
        intMatrix.map(function as IntFunction) == expected
        intMatrix.map(function as IntFunction).hashCode() == expected.hashCode()

        where:
        inputMatrix    | function       | eInputMatrix
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]] | { x -> x * 2 } | [[22, 24, 26],
                                           [42, 44, 46],
                                           [62, 64, 66]]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]] | { x -> x }     | [[11, 12, 13],
                                           [21, 22, 23],
                                           [31, 32, 33],
                                           [41, 42, 43]]
    }

    def "map(IntUnaryOperator function): map should apply function to each cell"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build IntMatrix"
        def builder = new IntMatrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def intMatrix = builder.build()

        def eBuilder = new IntMatrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                eBuilder.set(i + 1, j + 1, eInputMatrix.get(i).get(j))
            }
        }
        def expected = eBuilder.build()

        expect:
        intMatrix.map(function as IntUnaryOperator) == expected
        intMatrix.map(function as IntUnaryOperator).hashCode() == expected.hashCode()

        where:
        inputMatrix    | function       | eInputMatrix
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]] | { x -> x * 2 } | [[22, 24, 26],
                                           [42, 44, 46],
                                           [62, 64, 66]]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]] | { x -> x }     | [[11, 12, 13],
                                           [21, 22, 23],
                                           [31, 32, 33],
                                           [41, 42, 43]]
    }

    def "transpose(): should return the transposed IntMatrix"() {
        given: "build IntMatrix"
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()
        def builder = new IntMatrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def intMatrix = builder.build()

        and: "build expected IntMatrix"
        def eHeight = eInputMatrix.size()
        def eWidth = eInputMatrix.get(0).size()
        def eBuilder = new IntMatrix.Builder(eHeight, eWidth)
        for (def i = 0; i < eHeight; i++) {
            for (def j = 0; j < eWidth; j++) {
                eBuilder.set(i + 1, j + 1, eInputMatrix.get(i).get(j))
            }
        }
        def expected = eBuilder.build()

        expect:
        intMatrix.transpose() == expected
        intMatrix.transpose().hashCode() == expected.hashCode()

        where:
        inputMatrix        | eInputMatrix
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]     | [[11, 21, 31],
                              [12, 22, 32],
                              [13, 23, 33]]
        [[11, 12, 13, 14],
         [21, 22, 23, 24],
         [31, 32, 33, 34]] | [[11, 21, 31],
                              [12, 22, 32],
                              [13, 23, 33],
                              [14, 24, 34]]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]     | [[11, 21, 31, 41],
                              [12, 22, 32, 42],
                              [13, 23, 33, 43]]
    }

    def "allMatch(): returns true if all values of the IntMatrix are equal"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build IntMatrix"
        def builder = new IntMatrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def intMatrix = builder.build()

        expect:
        intMatrix.allMatch(IntBiPredicate.EQUAL) == expected

        where:
        inputMatrix        | expected
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]     | false
        [[11, 12, 13, 14],
         [21, 22, 23, 24],
         [31, 32, 33, 34]] | false
        [[0, 0, 0],
         [0, 0, 0],
         [0, 0, 0]]        | true
        [[1, 1, 1],
         [1, 1, 1],
         [1, 1, 1]]        | true
    }

    def "toStream(): should return the elements of the IntMatrix in row-wise order"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build IntMatrix"
        def builder = new IntMatrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def intMatrix = builder.build()

        expect:
        intMatrix.toIntStream().boxed().toList() == expected

        where:
        inputMatrix        | expected
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]     | [11, 12, 13, 21, 22, 23, 31, 32, 33]
        [[11, 12, 13, 14],
         [21, 22, 23, 24],
         [31, 32, 33, 34]] | [11, 12, 13, 14, 21, 22, 23, 24, 31, 32, 33, 34]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]     | [11, 12, 13, 21, 22, 23, 31, 32, 33, 41, 42, 43]
    }

    def "iterator(): should go over the elements of the IntMatrix in row-wise order"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build IntMatrix"
        def builder = new IntMatrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def intMatrix = builder.build()

        expect:
        def itr = intMatrix.iterator()

        for (def i = 0; i < expected.size(); i++) {
            assert itr.hasNext()
            assert itr.next() == expected.get(i)
        }
        !itr.hasNext()

        where:
        inputMatrix        | expected
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]     | [11, 12, 13, 21, 22, 23, 31, 32, 33]
        [[11, 12, 13, 14],
         [21, 22, 23, 24],
         [31, 32, 33, 34]] | [11, 12, 13, 14, 21, 22, 23, 24, 31, 32, 33, 34]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]     | [11, 12, 13, 21, 22, 23, 31, 32, 33, 41, 42, 43]
    }

    def "toString()"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build IntMatrix"
        def builder = new IntMatrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def intMatrix = builder.build()

        expect:
        intMatrix.toString() == expected

        where:
        inputMatrix        | expected
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]     | "⌈11 12 13⌉\n" +
                "|21 22 23|\n" +
                "⌊31 32 33⌋\n"
        [[11, 12, 13, 14],
         [21, 22, 23, 24],
         [31, 32, 33, 34]] | "⌈11 12 13 14⌉\n" +
                "|21 22 23 24|\n" +
                "⌊31 32 33 34⌋\n"
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]     | "⌈11 12 13⌉\n" +
                "|21 22 23|\n" +
                "|31 32 33|\n" +
                "⌊41 42 43⌋\n"
    }

}
