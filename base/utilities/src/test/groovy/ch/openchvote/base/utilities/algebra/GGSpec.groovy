/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.algebra

import spock.lang.Specification
import spock.lang.Unroll

class GGSpec extends Specification {

    def static final p = BigInteger.valueOf(29)
    def static final q = BigInteger.valueOf(7)
    def static final GGp = GG.of(p, q)

    @Unroll
    def "contains(T x) #x"() {
        expect:
        set.contains(x == null ? null : BigInteger.valueOf(x)) == expected

        where:
        set | x    | expected
        GGp | null | false
        GGp | -1   | false
        GGp | -0   | false
        GGp | 1    | true
        GGp | 2    | false
        GGp | 3    | false
        GGp | 4    | false
        GGp | 5    | false
        GGp | 6    | false
        GGp | 7    | true
        GGp | 8    | false
        GGp | 9    | false
        GGp | 10   | false
        GGp | 11   | false
        GGp | 12   | false
        GGp | 13   | false
        GGp | 14   | false
        GGp | 15   | false
        GGp | 16   | true
        GGp | 17   | false
        GGp | 18   | false
        GGp | 19   | false
        GGp | 20   | true
        GGp | 21   | false
        GGp | 22   | false
        GGp | 23   | true
        GGp | 24   | true
        GGp | 25   | true
        GGp | 26   | false
        GGp | 27   | false
        GGp | 28   | false
        GGp | 29   | false
    }

    @Unroll
    def "multiply(T x, T y) #x * #y"() {

        expect:
        GGp.multiply(BigInteger.valueOf(x), BigInteger.valueOf(y)) == BigInteger.valueOf(expected)

        where:
        x  | y  | expected
        1  | 1  | 1
        1  | 7  | 7
        1  | 16 | 16
        1  | 20 | 20
        1  | 23 | 23
        1  | 24 | 24
        1  | 25 | 25
        7  | 1  | 7
        7  | 7  | 20
        7  | 16 | 25
        7  | 20 | 24
        7  | 23 | 16
        7  | 24 | 23
        7  | 25 | 1
        16 | 1  | 16
        16 | 7  | 25
        16 | 16 | 24
        16 | 20 | 1
        16 | 23 | 20
        16 | 24 | 7
        16 | 25 | 23
        20 | 1  | 20
        20 | 7  | 24
        20 | 16 | 1
        20 | 20 | 23
        20 | 23 | 25
        20 | 24 | 16
        20 | 25 | 7
        23 | 1  | 23
        23 | 7  | 16
        23 | 16 | 20
        23 | 20 | 25
        23 | 23 | 7
        23 | 24 | 1
        23 | 25 | 24
        24 | 1  | 24
        24 | 7  | 23
        24 | 16 | 7
        24 | 20 | 16
        24 | 23 | 1
        24 | 24 | 25
        24 | 25 | 20
        25 | 1  | 25
        25 | 7  | 1
        25 | 16 | 23
        25 | 20 | 7
        25 | 23 | 24
        25 | 24 | 20
        25 | 25 | 16
    }

    @Unroll
    def "pow(T x, T y) #x^#y"() {

        expect:
        GGp.pow(BigInteger.valueOf(x), BigInteger.valueOf(y)) == BigInteger.valueOf(expected)

        where:
        x  | y | expected
        1  | 0 | 1
        1  | 1 | 1
        1  | 2 | 1
        1  | 3 | 1
        1  | 4 | 1
        1  | 5 | 1
        1  | 6 | 1
        7  | 0 | 1
        7  | 1 | 7
        7  | 2 | 20
        7  | 3 | 24
        7  | 4 | 23
        7  | 5 | 16
        7  | 6 | 25
        16 | 0 | 1
        16 | 1 | 16
        16 | 2 | 24
        16 | 3 | 7
        16 | 4 | 25
        16 | 5 | 23
        16 | 6 | 20
        20 | 0 | 1
        20 | 1 | 20
        20 | 2 | 23
        20 | 3 | 25
        20 | 4 | 7
        20 | 5 | 24
        20 | 6 | 16
        23 | 0 | 1
        23 | 1 | 23
        23 | 2 | 7
        23 | 3 | 16
        23 | 4 | 20
        23 | 5 | 25
        23 | 6 | 24
        24 | 0 | 1
        24 | 1 | 24
        24 | 2 | 25
        24 | 3 | 20
        24 | 4 | 16
        24 | 5 | 7
        24 | 6 | 23
        25 | 0 | 1
        25 | 1 | 25
        25 | 2 | 16
        25 | 3 | 23
        25 | 4 | 24
        25 | 5 | 20
        25 | 6 | 7
    }

    @Unroll
    def "invert(T x) #x^-1"() {

        expect:
        GGp.invert(BigInteger.valueOf(x)) == BigInteger.valueOf(expected)

        where:
        x  | expected
        1  | 1
        7  | 25
        16 | 20
        20 | 16
        23 | 24
        24 | 23
        25 | 7
    }

    @Unroll
    def "divide(T x, T y) #x / #y"() {

        expect:
        GGp.divide(BigInteger.valueOf(x), BigInteger.valueOf(y)) == BigInteger.valueOf(expected)

        where:
        x  | y  | expected
        1  | 1  | 1
        1  | 7  | 25
        1  | 16 | 20
        1  | 20 | 16
        1  | 23 | 24
        1  | 24 | 23
        1  | 25 | 7
        7  | 1  | 7
        7  | 7  | 1
        7  | 16 | 24
        7  | 20 | 25
        7  | 23 | 23
        7  | 24 | 16
        7  | 25 | 20
        16 | 1  | 16
        16 | 7  | 23
        16 | 16 | 1
        16 | 20 | 24
        16 | 23 | 7
        16 | 24 | 20
        16 | 25 | 25
        20 | 1  | 20
        20 | 7  | 7
        20 | 16 | 23
        20 | 20 | 1
        20 | 23 | 16
        20 | 24 | 25
        20 | 25 | 24
        23 | 1  | 23
        23 | 7  | 24
        23 | 16 | 25
        23 | 20 | 20
        23 | 23 | 1
        23 | 24 | 7
        23 | 25 | 16
        24 | 1  | 24
        24 | 7  | 20
        24 | 16 | 16
        24 | 20 | 7
        24 | 23 | 25
        24 | 24 | 1
        24 | 25 | 23
        25 | 1  | 25
        25 | 7  | 16
        25 | 16 | 7
        25 | 20 | 23
        25 | 23 | 20
        25 | 24 | 24
        25 | 25 | 1
    }
}
