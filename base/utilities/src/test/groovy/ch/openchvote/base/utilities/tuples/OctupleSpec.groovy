/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Shared
import spock.lang.Specification

class OctupleSpec extends Specification {

    @Shared
    def default_First
    @Shared
    def default_Second
    @Shared
    def default_Third
    @Shared
    def default_Fourth
    @Shared
    def default_Fifth
    @Shared
    def default_Sixth
    @Shared
    def default_Seventh
    @Shared
    def default_Eighth

    def setupSpec() {
        default_First = String.valueOf(1)
        default_Second = String.valueOf(2)
        default_Third = String.valueOf(3)
        default_Fourth = String.valueOf(4)
        default_Fifth = String.valueOf(5)
        default_Sixth = String.valueOf(6)
        default_Seventh = String.valueOf(7)
        default_Eighth = String.valueOf(8)
    }

    def "Constructor throws UtilityException for case: #caseDesc"() {
        when:
        new Octuple(first, second, third, fourth, fifth, sixth, seventh, eighth)

        then:
        thrown(UtilityException)

        where:
        caseDesc          | first         | second         | third         | fourth         | fifth         | sixth         | seventh         | eighth
        "first is null"   | null          | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth
        "second is null"  | default_First | null           | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth
        "third is null"   | default_First | default_Second | null          | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth
        "fourth is null"  | default_First | default_Second | default_Third | null           | default_Fifth | default_Sixth | default_Seventh | default_Eighth
        "fifth is null"   | default_First | default_Second | default_Third | default_Fourth | null          | default_Sixth | default_Seventh | default_Eighth
        "sixth is null"   | default_First | default_Second | default_Third | default_Fourth | default_Fifth | null          | default_Seventh | default_Eighth
        "seventh is null" | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | null            | default_Eighth
        "eighth is null"  | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | null
    }

    def "Test constructor and getter methods for the elements"() {
        given:
        def octuple = new Octuple(first, second, third, fourth, fifth, sixth, seventh, eighth)

        expect:
        octuple.getFirst() == first
        octuple.getSecond() == second
        octuple.getThird() == third
        octuple.getFourth() == fourth
        octuple.getFifth() == fifth
        octuple.getSixth() == sixth
        octuple.getSeventh() == seventh
        octuple.getEighth() == eighth

        where:
        first                 | second                | third                 | fourth                | fifth                 | sixth                 | seventh               | eighth
        "first"               | "second"              | "third"               | "fourth"              | "fifth"               | "sixth"               | "seventh"             | "eighth"
        1                     | 2                     | 3                     | 4                     | 5                     | 6                     | 7                     | 8
        BigInteger.valueOf(1) | BigInteger.valueOf(2) | BigInteger.valueOf(3) | BigInteger.valueOf(4) | BigInteger.valueOf(5) | BigInteger.valueOf(6) | BigInteger.valueOf(7) | BigInteger.valueOf(8)
    }

    @SuppressWarnings('ChangeToOperator')
    def "equals should return true if all pairs of elements of two octuples are the same and false otherwise"() {
        given:
        def octuple1 = new Octuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7))
        def octuple2 = new Octuple(elems2.get(0), elems2.get(1), elems2.get(2), elems2.get(3), elems2.get(4), elems2.get(5), elems2.get(6), elems2.get(7))

        expect:
        //noinspection ChangeToOperator
        octuple1.equals(octuple2) == expected
        (octuple1.hashCode() == octuple2.hashCode()) == expected

        where:
        elems1                                                                        | elems2                                                                        | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | true
        ["wrong", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | false
        ["first", "wrong", "third", "fourth", "fifth", "sixth", "seventh", "eighth"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | false
        ["first", "second", "wrong", "fourth", "fifth", "sixth", "seventh", "eighth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | false
        ["first", "second", "third", "wrong", "fifth", "sixth", "seventh", "eighth"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | false
        ["first", "second", "third", "fourth", "wrong", "sixth", "seventh", "eighth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | false
        ["first", "second", "third", "fourth", "fifth", "wrong", "seventh", "eighth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "wrong", "eighth"]   | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "wrong"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | false
    }

    def "toString() "() {
        given:
        def octuple = new Octuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7))

        expect:
        octuple.toString() == expected

        where:
        elems1                                                                        | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | "(first,second,third,fourth,fifth,sixth,seventh,eighth)"
        [1, 2, 3, 4, 5, 6, 7, 8]                                                      | "(1,2,3,4,5,6,7,8)"
    }

    def "toStream() should return a Stream containing all elements in the octuple in the correct order"() {
        given:
        def octuple = new Octuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7))

        expect:
        octuple.toStream().toList() == expected

        where:
        elems1                                                                        | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"]
        [1, 2, 3, 4, 5, 6, 7, 8]                                                      | [1, 2, 3, 4, 5, 6, 7, 8]
    }
}
