/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Shared
import spock.lang.Specification

class PairSpec extends Specification {

    @Shared
    def default_First
    @Shared
    def default_Second

    def setupSpec() {
        default_First = String.valueOf(1)
        default_Second = String.valueOf(2)
    }

    def "Constructor throws UtilityException for case: #caseDesc"() {
        when:
        new Pair(first, second)

        then:
        thrown(UtilityException)

        where:
        caseDesc         | first         | second
        "first is null"  | null          | default_Second
        "second is null" | default_First | null
    }

    def "Test constructor and getter methods for the elements"() {
        given:
        def pair = new Pair(first, second)

        expect:
        pair.getFirst() == first
        pair.getSecond() == second

        where:
        first                 | second
        "first"               | "second"
        1                     | 2
        BigInteger.valueOf(1) | BigInteger.valueOf(2)
    }

    @SuppressWarnings('ChangeToOperator')
    def "equals should return true if all pairs of elements of two pairs are the same and false otherwise"() {
        given:
        def pair1 = new Pair(elems1.get(0), elems1.get(1))
        def pair2 = new Pair(elems2.get(0), elems2.get(1))

        expect:
        //noinspection ChangeToOperator
        pair1.equals(pair2) == expected
        (pair1.hashCode() == pair2.hashCode()) == expected

        where:
        elems1              | elems2              | expected
        ["first", "second"] | ["first", "second"] | true
        ["wrong", "second"] | ["first", "second"] | false
        ["first", "wrong"]  | ["first", "second"] | false
    }

    def "toString()"() {
        given:
        def pair = new Pair(elems1.get(0), elems1.get(1))

        expect:
        pair.toString() == expected

        where:
        elems1              | expected
        ["first", "second"] | "(first,second)"
        [1, 2]              | "(1,2)"
    }

    def "toStream() should return a Stream containing all elements in the pair in the correct order"() {
        given:
        def pair = new Pair(elems1.get(0), elems1.get(1))

        expect:
        pair.toStream().toList() == expected

        where:
        elems1              | expected
        ["first", "second"] | ["first", "second"]
        [1, 2]              | [1, 2]
    }

}
