/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.crypto

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Specification
import spock.lang.Unroll

class EntropySpec extends Specification {

    @Unroll
    def "getEntropy(int bitsOfEntropy) throws UtilityException for invalid bitsOfEntropy"() {
        when:
        Entropy.getEntropy(value)

        then:
        thrown(UtilityException)

        where:
        descCase              | value
        "value is below zero" | -1
        "value is above 256"  | 264
    }

    def "getEntropy(int bitsOfEntropy) always delivers 256 bits of entropy"() {
        given:
        def random = new Random()

        when:
        def bitsOfEntropy = random.nextInt(256)
        def entropy = Entropy.getEntropy(bitsOfEntropy)

        then:
        entropy.getLength() == 32

        where:
        i << (1..10)
    }

    def "getNoise(int numberOfSamplesRequested) always returns the the  requested number of samples"() {
        given:
        def random = new Random()

        when:
        def numberOfSamplesRequested = random.nextInt(256)
        def samples = Entropy.getNoise(numberOfSamplesRequested)

        then:
        samples.size() == numberOfSamplesRequested

        where:
        i << (1..10)
    }

    def "vonNeumannUnbiasing(long value) works as defined"() {
        expect:
        Entropy.vonNeumannUnbiasing(x) == expected

        where:
        x     | expected
        85    | 15
        170   | 0
        201   | 1
        19270 | 22
    }

    def "stuckTest(List<Integer> samples) correctly identifies samples where the 1st, 2nd, and 3rd derivations are non-zero"() {
        expect:
        Entropy.stuckTest(samples) == expected

        where:
        samples                          | expected
        [2, 2, 2, 2, 2, 2, 2]            | false
        [2, 3, 4, 5, 6, 7, 8]            | false
        [2, 3, 5, 8, 12, 17]             | false
        [14, 48, 108, 224, 450, 864]     | true
        [840, 144504, 3866912, 46875450] | true
        [1, 8, 27, 64, 125]              | true
    }

    def "repetitionCountTest(List<Integer> samples) returns true for constant function"() {
        given:
        def samples = new ArrayList<Integer>()
        for (int i = 0; i < 2001; i++) {
            samples.add(1)
        }

        expect:
        !Entropy.repetitionCountTest(samples)
    }

    def "repetitionCountTest(List<Integer> samples) returns false for periodic function"() {
        given:
        def samples = new ArrayList<Integer>()
        for (int i = 0; i < 6003; i++) {
            if (i % 3 == 0) {
                samples.add(1)
            } else {
                samples.add(0)
            }
        }

        expect:
        Entropy.repetitionCountTest(samples)
    }

    def "adaptiveProportionTest(List<Integer> samples) returns false for constant function"() {
        given:
        def samples = new ArrayList<Integer>()
        for (int i = 0; i < 512; i++) {
            samples.add(1)
        }

        expect:
        !Entropy.adaptiveProportionTest(samples)
    }

    def "adaptiveProportionTest(List<Integer> samples) returns false for periodic function"() {
        given:
        def samples = new ArrayList<Integer>()
        for (int i = 0; i < 1000; i++) {
            if (i % 200 == 0) {
                samples.add(1)
            } else {
                samples.add(0)
            }
        }

        expect:
        !Entropy.adaptiveProportionTest(samples)
    }

    def "adaptiveProportionTest(List<Integer> samples) returns false for linear function"() {
        given:
        def samples = new ArrayList<Integer>()
        for (int i = 0; i < 1000; i++) {
            samples.add(i)
        }

        expect:
        Entropy.adaptiveProportionTest(samples)
    }

    def "critBinom(int n, double p, double alpha) returns correct Cutoff point C"() {
        expect:
        (int) Math.ceil(Entropy.critBinom(n, Math.pow(2, (-p)), 1 - alpha)) + 1 == expected

        where:
        n    | p   | alpha            | expected
        1024 | 0.2 | Math.pow(2, -20) | 941
        1024 | 0.4 | Math.pow(2, -20) | 840
        1024 | 0.6 | Math.pow(2, -20) | 748
        1024 | 0.8 | Math.pow(2, -20) | 664
        1024 | 1.0 | Math.pow(2, -20) | 589
        512  | 0.5 | Math.pow(2, -20) | 410
        512  | 1.0 | Math.pow(2, -20) | 311
        512  | 2.0 | Math.pow(2, -20) | 177
        512  | 4.0 | Math.pow(2, -20) | 62
        512  | 8.0 | Math.pow(2, -20) | 13
    }
}
