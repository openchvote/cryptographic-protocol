/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.crypto

import ch.openchvote.base.utilities.sequence.ByteArray
import spock.lang.Specification

import java.nio.charset.StandardCharsets

class SHA3Spec extends Specification {

    def toByteArray(String S) {
        return ByteArray.of(S.getBytes(StandardCharsets.UTF_8))
    }
    def "hash(ByteArray B) should return the sha3 hash of B"() {
        given:
        def b = toByteArray(bString)

        expect:
        HashAlgorithm.SHA3.hash(b).toString() == eString

        where:
        l  | bString            | eString
        32 | "hello"            | "3338BE694F50C5F338814986CDF0686453A888B84F424D792AF4B9202398F392"
        32 | "this is a secret" | "AC2A903543E7673C9E441498FF3FB7D7D193D2AD5F29C870694410F0766D6699"
    }

}
