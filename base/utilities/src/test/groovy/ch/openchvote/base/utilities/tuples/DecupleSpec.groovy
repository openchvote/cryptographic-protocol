/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples


import ch.openchvote.base.utilities.UtilityException
import ch.openchvote.base.utilities.tuples.decuple.Decuple
import spock.lang.Shared
import spock.lang.Specification

class DecupleSpec extends Specification {
    @Shared
    def default_First
    @Shared
    def default_Second
    @Shared
    def default_Third
    @Shared
    def default_Fourth
    @Shared
    def default_Fifth
    @Shared
    def default_Sixth
    @Shared
    def default_Seventh
    @Shared
    def default_Eighth
    @Shared
    def default_Ninth
    @Shared
    def default_Tenth
    def setupSpec() {
        default_First = String.valueOf(1)
        default_Second = String.valueOf(2)
        default_Third = String.valueOf(3)
        default_Fourth = String.valueOf(4)
        default_Fifth = String.valueOf(5)
        default_Sixth = String.valueOf(6)
        default_Seventh = String.valueOf(7)
        default_Eighth = String.valueOf(8)
        default_Ninth = String.valueOf(9)
        default_Tenth = String.valueOf(10)
    }

    def "Constructor throws UtilityException for case: #caseDesc"() {
        when:
        new Decuple(first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth)

        then:
        thrown(UtilityException)

        where:
        caseDesc          | first         | second         | third         | fourth         | fifth         | sixth         | seventh         | eighth         | ninth         | tenth
        "first is null"   | null          | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth | default_Tenth
        "second is null"  | default_First | null           | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth | default_Tenth
        "third is null"   | default_First | default_Second | null          | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth | default_Tenth
        "fourth is null"  | default_First | default_Second | default_Third | null           | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth | default_Tenth
        "fifth is null"   | default_First | default_Second | default_Third | default_Fourth | null          | default_Sixth | default_Seventh | default_Eighth | default_Ninth | default_Tenth
        "sixth is null"   | default_First | default_Second | default_Third | default_Fourth | default_Fifth | null          | default_Seventh | default_Eighth | default_Ninth | default_Tenth
        "seventh is null" | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | null            | default_Eighth | default_Ninth | default_Tenth
        "eighth is null"  | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | null           | default_Ninth | default_Tenth
        "ninth is null"   | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | null          | default_Tenth
        "tenth is null"   | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth | null
    }

    def "Test constructor and getter methods for the elements"() {
        given:
        def Decuple = new Decuple(first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth)

        expect:
        Decuple.getFirst() == first
        Decuple.getSecond() == second
        Decuple.getThird() == third
        Decuple.getFourth() == fourth
        Decuple.getFifth() == fifth
        Decuple.getSixth() == sixth
        Decuple.getSeventh() == seventh
        Decuple.getEighth() == eighth
        Decuple.getNinth() == ninth
        Decuple.getTenth() == tenth

        where:
        first                 | second                | third                 | fourth                | fifth                 | sixth                 | seventh               | eighth                | ninth                 | tenth
        "first"               | "second"              | "third"               | "fourth"              | "fifth"               | "sixth"               | "seventh"             | "eighth"              | "ninth"               | "tenth"
        1                     | 2                     | 3                     | 4                     | 5                     | 6                     | 7                     | 8                     | 9                     | 10
        BigInteger.valueOf(1) | BigInteger.valueOf(2) | BigInteger.valueOf(3) | BigInteger.valueOf(4) | BigInteger.valueOf(5) | BigInteger.valueOf(6) | BigInteger.valueOf(7) | BigInteger.valueOf(8) | BigInteger.valueOf(9) | BigInteger.valueOf(10)
    }

    @SuppressWarnings('ChangeToOperator')
    def "equals should return true if all pairs of elements of two decuples are the same and false otherwise"() {
        given:
        def decuple1 = new Decuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8), elems1.get(9))
        def decuple2 = new Decuple(elems2.get(0), elems2.get(1), elems2.get(2), elems2.get(3), elems2.get(4), elems2.get(5), elems2.get(6), elems2.get(7), elems2.get(8), elems2.get(9))

        expect:
        //noinspection ChangeToOperator
        decuple1.equals(decuple2) == expected
        (decuple1.hashCode() == decuple2.hashCode()) == expected

        where:
        elems1                                                                                          | elems2                                                                                          | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | true
        ["wrong", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | false
        ["first", "wrong", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | false
        ["first", "second", "wrong", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | false
        ["first", "second", "third", "wrong", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | false
        ["first", "second", "third", "fourth", "wrong", "sixth", "seventh", "eighth", "ninth", "tenth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | false
        ["first", "second", "third", "fourth", "fifth", "wrong", "seventh", "eighth", "ninth", "tenth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "wrong", "eighth", "ninth", "tenth"]   | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "wrong", "ninth", "tenth"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "wrong", "tenth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "wrong"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | false
    }

    def "toString()"() {
        given:
        def decuple = new Decuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8), elems1.get(9))

        expect:
        decuple.toString() == expected

        where:
        elems1                                                                                          | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | "(first,second,third,fourth,fifth,sixth,seventh,eighth,ninth,tenth)"
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]                                                                 | "(1,2,3,4,5,6,7,8,9,10)"
    }

    def "toStream() should return a Stream containing all elements in the decuple in the correct order"() {
        given:
        def decuple = new Decuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8), elems1.get(9))

        expect:
        decuple.toStream().toList() == expected

        where:
        elems1                                                                                          | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"]
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]                                                                 | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    }
}
