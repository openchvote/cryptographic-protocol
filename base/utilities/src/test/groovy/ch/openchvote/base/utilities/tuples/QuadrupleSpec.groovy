/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Shared
import spock.lang.Specification

class QuadrupleSpec extends Specification {
    @Shared
    def default_First
    @Shared
    def default_Second
    @Shared
    def default_Third
    @Shared
    def default_Fourth

    def setupSpec() {
        default_First = String.valueOf(1)
        default_Second = String.valueOf(2)
        default_Third = String.valueOf(3)
        default_Fourth = String.valueOf(4)
    }

    def "Constructor throws UtilityException for case: #caseDesc"() {
        when:
        new Quadruple(first, second, third, fourth)

        then:
        thrown(UtilityException)

        where:
        caseDesc         | first         | second         | third         | fourth
        "first is null"  | null          | default_Second | default_Third | default_Fourth
        "second is null" | default_First | null           | default_Third | default_Fourth
        "third is null"  | default_First | default_Second | null          | default_Fourth
        "fourth is null" | default_First | default_Second | default_Third | null
    }

    def "Test constructor and getter methods for the elements"() {
        given:
        def quadruple = new Quadruple(first, second, third, fourth)

        expect:
        quadruple.getFirst() == first
        quadruple.getSecond() == second
        quadruple.getThird() == third
        quadruple.getFourth() == fourth

        where:
        first                 | second                | third                 | fourth
        "first"               | "second"              | "third"               | "fourth"
        1                     | 2                     | 3                     | 4
        BigInteger.valueOf(1) | BigInteger.valueOf(2) | BigInteger.valueOf(3) | BigInteger.valueOf(4)
    }

    @SuppressWarnings('ChangeToOperator')
    def "equals should return true if all pairs of elements of two quadruples are the same and false otherwise"() {
        given:
        def quadruple1 = new Quadruple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3))
        def quadruple2 = new Quadruple(elems2.get(0), elems2.get(1), elems2.get(2), elems2.get(3))

        expect:
        //noinspection ChangeToOperator
        quadruple1.equals(quadruple2) == expected
        (quadruple1.hashCode() == quadruple2.hashCode()) == expected

        where:
        elems1                                 | elems2                                 | expected
        ["first", "second", "third", "fourth"] | ["first", "second", "third", "fourth"] | true
        ["wrong", "second", "third", "fourth"] | ["first", "second", "third", "fourth"] | false
        ["first", "wrong", "third", "fourth"]  | ["first", "second", "third", "fourth"] | false
        ["first", "second", "wrong", "fourth"] | ["first", "second", "third", "fourth"] | false
        ["first", "second", "third", "wrong"]  | ["first", "second", "third", "fourth"] | false
    }

    def "toString()"() {
        given:
        def quadruple = new Quadruple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3))

        expect:
        quadruple.toString() == expected

        where:
        elems1                                 | expected
        ["first", "second", "third", "fourth"] | "(first,second,third,fourth)"
        [1, 2, 3, 4]                           | "(1,2,3,4)"
    }

    def "toStream() should return a Stream containing all elements in the quadruple in the correct order"() {
        given:
        def quadruple = new Quadruple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3))

        expect:
        quadruple.toStream().toList() == expected

        where:
        elems1                                 | expected
        ["first", "second", "third", "fourth"] | ["first", "second", "third", "fourth"]
        [1, 2, 3, 4]                           | [1, 2, 3, 4]
    }
}
