/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Shared
import spock.lang.Specification

class QuintupleSpec extends Specification {
    @Shared
    def default_First
    @Shared
    def default_Second
    @Shared
    def default_Third
    @Shared
    def default_Fourth
    @Shared
    def default_Fifth

    def setupSpec() {
        default_First = String.valueOf(1)
        default_Second = String.valueOf(2)
        default_Third = String.valueOf(3)
        default_Fourth = String.valueOf(4)
        default_Fifth = String.valueOf(5)
    }

    def "Constructor throws UtilityException for case: #caseDesc"() {
        when:
        new Quintuple(first, second, third, fourth, fifth)

        then:
        thrown(UtilityException)

        where:
        caseDesc         | first         | second         | third         | fourth         | fifth
        "first is null"  | null          | default_Second | default_Third | default_Fourth | default_Fifth
        "second is null" | default_First | null           | default_Third | default_Fourth | default_Fifth
        "third is null"  | default_First | default_Second | null          | default_Fourth | default_Fifth
        "fourth is null" | default_First | default_Second | default_Third | null           | default_Fifth
        "fifth is null"  | default_First | default_Second | default_Third | default_Fourth | null
    }

    def "Test constructor and getter methods for the elements"() {
        given:
        def quintuple = new Quintuple(first, second, third, fourth, fifth)

        expect:
        quintuple.getFirst() == first
        quintuple.getSecond() == second
        quintuple.getThird() == third
        quintuple.getFourth() == fourth
        quintuple.getFifth() == fifth

        where:
        first                 | second                | third                 | fourth                | fifth
        "first"               | "second"              | "third"               | "fourth"              | "fifth"
        1                     | 2                     | 3                     | 4                     | 5
        BigInteger.valueOf(1) | BigInteger.valueOf(2) | BigInteger.valueOf(3) | BigInteger.valueOf(4) | BigInteger.valueOf(5)
    }

    @SuppressWarnings('ChangeToOperator')
    def "equals should return true if all pairs of elements of two quintuples are the same and false otherwise"() {
        given:
        def quintuple1 = new Quintuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4))
        def quintuple2 = new Quintuple(elems2.get(0), elems2.get(1), elems2.get(2), elems2.get(3), elems2.get(4))

        expect:
        //noinspection ChangeToOperator
        quintuple1.equals(quintuple2) == expected
        (quintuple1.hashCode() == quintuple2.hashCode()) == expected

        where:
        elems1                                          | elems2                                          | expected
        ["first", "second", "third", "fourth", "fifth"] | ["first", "second", "third", "fourth", "fifth"] | true
        ["wrong", "second", "third", "fourth", "fifth"] | ["first", "second", "third", "fourth", "fifth"] | false
        ["first", "wrong", "third", "fourth", "fifth"]  | ["first", "second", "third", "fourth", "fifth"] | false
        ["first", "second", "wrong", "fourth", "fifth"] | ["first", "second", "third", "fourth", "fifth"] | false
        ["first", "second", "third", "wrong", "fifth"]  | ["first", "second", "third", "fourth", "fifth"] | false
        ["first", "second", "third", "fourth", "wrong"] | ["first", "second", "third", "fourth", "fifth"] | false
    }

    def "toString()"() {
        given:
        def quintuple = new Quintuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4))

        expect:
        quintuple.toString() == expected

        where:
        elems1                                          | expected
        ["first", "second", "third", "fourth", "fifth"] | "(first,second,third,fourth,fifth)"
        [1, 2, 3, 4, 5]                                 | "(1,2,3,4,5)"
    }

    def "toStream() should return a Stream containing all elements in the quintuple in the correct order"() {
        given:
        def quintuple = new Quintuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4))

        expect:
        quintuple.toStream().toList() == expected

        where:
        elems1                                          | expected
        ["first", "second", "third", "fourth", "fifth"] | ["first", "second", "third", "fourth", "fifth"]
        [1, 2, 3, 4, 5]                                 | [1, 2, 3, 4, 5]
    }
}