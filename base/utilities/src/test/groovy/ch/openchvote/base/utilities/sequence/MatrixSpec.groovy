/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence
import spock.lang.Specification

class MatrixSpec extends Specification {

    def "Builder<V>: building a new Matrix<V> using the Builder<V> class and setValue method"() {
        given:
        def rows
        def columns
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build rows vector"
        def rowsBuilder = new Vector.Builder<Vector>(height)
        for (def i = 0; i < inputRows.size(); i++) {
            def row = Vector.of(inputRows.get(i).toArray())
            rowsBuilder.add(row)
        }
        rows = rowsBuilder.build()

        and: "build columns vector"
        def columnsBuilder = new Vector.Builder<Vector>(width)
        for (def i = 0; i < inputColumns.size(); i++) {
            def column = Vector.of(inputColumns.get(i).toArray())
            columnsBuilder.add(column)
        }
        columns = columnsBuilder.build()

        and: "build Matrix"
        def builder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def matrix = builder.build()

        expect: "Matrix has expected dimensions"
        matrix.getMinRowIndex() == 1
        matrix.getMaxRowIndex() == height
        matrix.getHeight() == height

        matrix.getMinColIndex() == 1
        matrix.getMaxColIndex() == width
        matrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert matrix.getValue(1 + i, 1 + j) == inputMatrix.get(i).get(j)
            }
        }

        and: "rows are as expected"
        for (def i = 0; i < height; i++) {
            assert matrix.getRow(1 + i) == rows.getValue(1 + i)
        }
        matrix.getRows() == rows

        and: "columns are as expected"
        for (def i = 0; i < width; i++) {
            assert matrix.getCol(1 + i) == columns.getValue(1 + i)
        }
        matrix.getCols() == columns

        where:
        inputMatrix        | inputRows                                                | inputColumns
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33]]               | [[11, 21, 31], [12, 22, 32], [13, 23, 33]]
        [[11, 12, 13, 14],
         [21, 22, 23, 24],
         [31, 32, 33, 34]] | [[11, 12, 13, 14], [21, 22, 23, 24], [31, 32, 33, 34]]   | [[11, 21, 31], [12, 22, 32], [13, 23, 33], [14, 24, 34]]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33], [41, 42, 43]] | [[11, 21, 31, 41], [12, 22, 32, 42], [13, 23, 33, 43]]
    }

    def "Builder<V>: building a new Matrix<V> using the Builder<V> class and addValue method"() {
        given:
        def rows
        def columns
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build rows vector"
        def rowsBuilder = new Vector.Builder<Vector>(height)
        for (def i = 0; i < inputRows.size(); i++) {
            def row = Vector.of(inputRows.get(i).toArray())
            rowsBuilder.add(row)
        }
        rows = rowsBuilder.build()

        and: "build columns vector"
        def columnsBuilder = new Vector.Builder<Vector>(width)
        for (def i = 0; i < inputColumns.size(); i++) {
            def column = Vector.of(inputColumns.get(i).toArray())
            columnsBuilder.add(column)
        }
        columns = columnsBuilder.build()

        and: "build Matrix"
        def builder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.add(inputMatrix.get(i).get(j))
            }
        }
        def matrix = builder.build()

        expect: "Matrix has expected dimensions"
        matrix.getMinRowIndex() == 1
        matrix.getMaxRowIndex() == height
        matrix.getHeight() == height

        matrix.getMinColIndex() == 1
        matrix.getMaxColIndex() == width
        matrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert matrix.getValue(1 + i, 1 + j) == inputMatrix.get(i).get(j)
            }
        }

        and: "rows are as expected"
        for (def i = 0; i < height; i++) {
            assert matrix.getRow(1 + i) == rows.getValue(1 + i)
        }
        matrix.getRows() == rows

        and: "columns are as expected"
        for (def i = 0; i < width; i++) {
            assert matrix.getCol(1 + i) == columns.getValue(1 + i)
        }
        matrix.getCols() == columns

        where:
        inputMatrix        | inputRows                                                | inputColumns
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33]]               | [[11, 21, 31],
                                                                                         [12, 22, 32],
                                                                                         [13, 23, 33]]
        [[11, 12, 13, 14],
         [21, 22, 23, 24],
         [31, 32, 33, 34]] | [[11, 12, 13, 14], [21, 22, 23, 24], [31, 32, 33, 34]]   | [[11, 21, 31],
                                                                                         [12, 22, 32],
                                                                                         [13, 23, 33],
                                                                                         [14, 24, 34]]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33], [41, 42, 43]] | [[11, 21, 31, 41],
                                                                                         [12, 22, 32, 42],
                                                                                         [13, 23, 33, 43]]
    }

    def "Builder<V>: building a new Matrix<V> using the Builder<V> class and fill method"() {

        given: "build Matrix"

        def builder = new Matrix.Builder(height, width)
        builder.fill(value)
        def matrix = builder.build()

        expect:

        and: "Matrix has expected dimensions"
        matrix.getMinRowIndex() == 1
        matrix.getMaxRowIndex() == height
        matrix.getHeight() == height

        matrix.getMinColIndex() == 1
        matrix.getMaxColIndex() == width
        matrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert matrix.getValue(1 + i, 1 + j) == expected.get(i).get(j)
            }
        }

        where:
        value | height | width | expected
        1     | 3      | 3     | [[1, 1, 1],
                                  [1, 1, 1],
                                  [1, 1, 1]]
        26    | 3      | 4     | [[26, 26, 26, 26],
                                  [26, 26, 26, 26],
                                  [26, 26, 26, 26]]
        1291  | 4      | 3     | [[1291, 1291, 1291],
                                  [1291, 1291, 1291],
                                  [1291, 1291, 1291],
                                  [1291, 1291, 1291]]
    }

    def "Builder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex): building a new Matrix<V> using the Builder<V> class"() {
        given:
        def rows
        def columns
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build rows vector"
        def rowsBuilder = new Vector.Builder<Vector>(minRowIndex, maxRowIndex)
        for (def i = 0; i < inputRows.size(); i++) {
            def rowBuilder = new Vector.Builder(minColIndex, maxColIndex)
            for (def x : inputRows.get(i)) {
                rowBuilder.add(x)
            }
            def row = rowBuilder.build()
            rowsBuilder.add(row)
        }
        rows = rowsBuilder.build()

        and: "build columns vector"
        def columnsBuilder = new Vector.Builder<Vector>(minColIndex, maxColIndex)
        for (def i = 0; i < inputColumns.size(); i++) {
            def colBuilder = new Vector.Builder(minRowIndex, maxRowIndex)
            for (def x : inputColumns.get(i)) {
                colBuilder.add(x)
            }
            def column = colBuilder.build()
            columnsBuilder.add(column)
        }
        columns = columnsBuilder.build()

        and: "build Matrix"
        def builder = new Matrix.Builder(minRowIndex, maxRowIndex, minColIndex, maxColIndex)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + minRowIndex, j + minColIndex, inputMatrix.get(i).get(j))
            }
        }
        def matrix = builder.build()

        expect: "Matrix has expected dimensions"
        matrix.getMinRowIndex() == minRowIndex
        matrix.getMaxRowIndex() == maxRowIndex
        matrix.getHeight() == height

        matrix.getMinColIndex() == minColIndex
        matrix.getMaxColIndex() == maxColIndex
        matrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert matrix.getValue(minRowIndex + i, minColIndex + j) == inputMatrix.get(i).get(j)
            }
        }

        and: "rows are as expected"
        for (def i = 0; i < height; i++) {
            assert matrix.getRow(minRowIndex + i) == rows.getValue(minRowIndex + i)
        }
        matrix.getRows() == rows

        and: "columns are as expected"
        for (def i = 0; i < width; i++) {
            assert matrix.getCol(minColIndex + i) == columns.getValue(minColIndex + i)
        }
        matrix.getCols() == columns

        where:
        minRowIndex | maxRowIndex | minColIndex | maxColIndex | inputMatrix        | inputRows                                                | inputColumns
        1           | 3           | 1           | 3           | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33]]               | [[11, 21, 31], [12, 22, 32], [13, 23, 33]]
        3           | 5           | 4           | 7           | [[11, 12, 13, 14],
                                                                 [21, 22, 23, 24],
                                                                 [31, 32, 33, 34]] | [[11, 12, 13, 14], [21, 22, 23, 24], [31, 32, 33, 34]]   | [[11, 21, 31], [12, 22, 32], [13, 23, 33], [14, 24, 34]]
        7           | 10          | 9           | 11          | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33],
                                                                 [41, 42, 43]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33], [41, 42, 43]] | [[11, 21, 31, 41], [12, 22, 32, 42], [13, 23, 33, 43]]
    }

    def "RowBuilder(int height, int width): building a new Matrix<V> using the RowBuilder<V> class"() {
        given:
        def rows
        def columns
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build rows vector"
        def rowsBuilder = new Vector.Builder<Vector>(height)
        for (def i = 0; i < inputRows.size(); i++) {
            def row = Vector.of(inputRows.get(i).toArray())
            rowsBuilder.add(row)
        }
        rows = rowsBuilder.build()

        and: "build columns vector"
        def columnsBuilder = new Vector.Builder<Vector>(width)
        for (def i = 0; i < inputColumns.size(); i++) {
            def column = Vector.of(inputColumns.get(i).toArray())
            columnsBuilder.add(column)
        }
        columns = columnsBuilder.build()

        and: "build Matrix"
        def builder = new Matrix.RowBuilder(height, width)
        for (def i = 1; i <= height; i++) {
            builder.addRow(rows.getValue(i))
        }
        def matrix = builder.build()

        expect: "Matrix has expected dimensions"
        matrix.getMinRowIndex() == 1
        matrix.getMaxRowIndex() == height
        matrix.getHeight() == height

        matrix.getMinColIndex() == 1
        matrix.getMaxColIndex() == width
        matrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert matrix.getValue(1 + i, 1 + j) == inputMatrix.get(i).get(j)
            }
        }

        and: "rows are as expected"
        for (def i = 0; i < height; i++) {
            assert matrix.getRow(1 + i) == rows.getValue(1 + i)
        }
        matrix.getRows() == rows

        and: "columns are as expected"
        for (def i = 0; i < width; i++) {
            assert matrix.getCol(1 + i) == columns.getValue(1 + i)
        }
        matrix.getCols() == columns

        where:
        inputMatrix        | inputRows                                                | inputColumns
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33]]               | [[11, 21, 31], [12, 22, 32], [13, 23, 33]]
        [[11, 12, 13, 14],
         [21, 22, 23, 24],
         [31, 32, 33, 34]] | [[11, 12, 13, 14], [21, 22, 23, 24], [31, 32, 33, 34]]   | [[11, 21, 31], [12, 22, 32], [13, 23, 33], [14, 24, 34]]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33], [41, 42, 43]] | [[11, 21, 31, 41], [12, 22, 32, 42], [13, 23, 33, 43]]
    }

    def "RowBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex): building a new Matrix<V> using the RowBuilder<V> class"() {
        given:
        def rows
        def columns
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build rows vector"
        def rowsBuilder = new Vector.Builder<Vector>(minRowIndex, maxRowIndex)
        for (def i = 0; i < inputRows.size(); i++) {
            def rowBuilder = new Vector.Builder(minColIndex, maxColIndex)
            for (def x : inputRows.get(i)) {
                rowBuilder.add(x)
            }
            def row = rowBuilder.build()
            rowsBuilder.add(row)
        }
        rows = rowsBuilder.build()

        and: "build columns vector"
        def columnsBuilder = new Vector.Builder<Vector>(minColIndex, maxColIndex)
        for (def i = 0; i < inputColumns.size(); i++) {
            def colBuilder = new Vector.Builder(minRowIndex, maxRowIndex)
            for (def x : inputColumns.get(i)) {
                colBuilder.add(x)
            }
            def column = colBuilder.build()
            columnsBuilder.add(column)
        }
        columns = columnsBuilder.build()

        and: "build Matrix"
        def builder = new Matrix.RowBuilder(minRowIndex, maxRowIndex, minColIndex, maxColIndex)
        for (def i = minRowIndex; i <= maxRowIndex; i++) {
            builder.setRow(i, rows.getValue(i))
        }
        def matrix = builder.build()

        expect: "Matrix has expected dimensions"
        matrix.getMinRowIndex() == minRowIndex
        matrix.getMaxRowIndex() == maxRowIndex
        matrix.getHeight() == height

        matrix.getMinColIndex() == minColIndex
        matrix.getMaxColIndex() == maxColIndex
        matrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert matrix.getValue(minRowIndex + i, minColIndex + j) == inputMatrix.get(i).get(j)
            }
        }

        and: "rows are as expected"
        for (def i = 0; i < height; i++) {
            assert matrix.getRow(minRowIndex + i) == rows.getValue(minRowIndex + i)
        }
        matrix.getRows() == rows

        and: "columns are as expected"
        for (def i = 0; i < width; i++) {
            assert matrix.getCol(minColIndex + i) == columns.getValue(minColIndex + i)
        }
        matrix.getCols() == columns

        where:
        minRowIndex | maxRowIndex | minColIndex | maxColIndex | inputMatrix        | inputRows                                                | inputColumns
        1           | 3           | 1           | 3           | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33]]               | [[11, 21, 31], [12, 22, 32], [13, 23, 33]]
        3           | 5           | 4           | 7           | [[11, 12, 13, 14],
                                                                 [21, 22, 23, 24],
                                                                 [31, 32, 33, 34]] | [[11, 12, 13, 14], [21, 22, 23, 24], [31, 32, 33, 34]]   | [[11, 21, 31], [12, 22, 32], [13, 23, 33], [14, 24, 34]]
        7           | 10          | 9           | 11          | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33],
                                                                 [41, 42, 43]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33], [41, 42, 43]] | [[11, 21, 31, 41], [12, 22, 32, 42], [13, 23, 33, 43]]
    }

    def "ColBuilder(int height, int width): building a new Matrix<V> using the ColBuilder<V> class"() {
        given:
        def rows
        def columns
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build rows vector"
        def rowsBuilder = new Vector.Builder<Vector>(height)
        for (def i = 0; i < inputRows.size(); i++) {
            def row = Vector.of(inputRows.get(i).toArray())
            rowsBuilder.add(row)
        }
        rows = rowsBuilder.build()

        and: "build columns vector"
        def columnsBuilder = new Vector.Builder<Vector>(width)
        for (def i = 0; i < inputColumns.size(); i++) {
            def column = Vector.of(inputColumns.get(i).toArray())
            columnsBuilder.add(column)
        }
        columns = columnsBuilder.build()

        and: "build Matrix"
        def builder = new Matrix.ColBuilder(height, width)
        for (def i = 1; i <= width; i++) {
            builder.addCol(columns.getValue(i))
        }
        def matrix = builder.build()

        expect: "Matrix has expected dimensions"
        matrix.getMinRowIndex() == 1
        matrix.getMaxRowIndex() == height
        matrix.getHeight() == height

        matrix.getMinColIndex() == 1
        matrix.getMaxColIndex() == width
        matrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert matrix.getValue(1 + i, 1 + j) == inputMatrix.get(i).get(j)
            }
        }

        and: "rows are as expected"
        for (def i = 0; i < height; i++) {
            assert matrix.getRow(1 + i) == rows.getValue(1 + i)
        }
        matrix.getRows() == rows

        and: "columns are as expected"
        for (def i = 0; i < width; i++) {
            assert matrix.getCol(1 + i) == columns.getValue(1 + i)
        }
        matrix.getCols() == columns

        where:
        inputMatrix        | inputRows                                                | inputColumns
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33]]               | [[11, 21, 31], [12, 22, 32], [13, 23, 33]]
        [[11, 12, 13, 14],
         [21, 22, 23, 24],
         [31, 32, 33, 34]] | [[11, 12, 13, 14], [21, 22, 23, 24], [31, 32, 33, 34]]   | [[11, 21, 31], [12, 22, 32], [13, 23, 33], [14, 24, 34]]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33], [41, 42, 43]] | [[11, 21, 31, 41], [12, 22, 32, 42], [13, 23, 33, 43]]
    }

    def "ColBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex): building a new Matrix<V> using the ColBuilder<V> class"() {
        given:
        def rows
        def columns
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build rows vector"
        def rowsBuilder = new Vector.Builder<Vector>(minRowIndex, maxRowIndex)
        for (def i = 0; i < inputRows.size(); i++) {
            def rowBuilder = new Vector.Builder(minColIndex, maxColIndex)
            for (def x : inputRows.get(i)) {
                rowBuilder.add(x)
            }
            def row = rowBuilder.build()
            rowsBuilder.add(row)
        }
        rows = rowsBuilder.build()

        and: "build columns vector"
        def columnsBuilder = new Vector.Builder<Vector>(minColIndex, maxColIndex)
        for (def i = 0; i < inputColumns.size(); i++) {
            def colBuilder = new Vector.Builder(minRowIndex, maxRowIndex)
            for (def x : inputColumns.get(i)) {
                colBuilder.add(x)
            }
            def column = colBuilder.build()
            columnsBuilder.add(column)
        }
        columns = columnsBuilder.build()

        and: "build Matrix"
        def builder = new Matrix.ColBuilder(minRowIndex, maxRowIndex, minColIndex, maxColIndex)
        for (def i = minColIndex; i <= maxColIndex; i++) {
            builder.setCol(i, columns.getValue(i))
        }
        def matrix = builder.build()

        expect: "Matrix has expected dimensions"
        matrix.getMinRowIndex() == minRowIndex
        matrix.getMaxRowIndex() == maxRowIndex
        matrix.getHeight() == height

        matrix.getMinColIndex() == minColIndex
        matrix.getMaxColIndex() == maxColIndex
        matrix.getWidth() == width

        and: "each cell contains expected value"
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                assert matrix.getValue(minRowIndex + i, minColIndex + j) == inputMatrix.get(i).get(j)
            }
        }

        and: "rows are as expected"
        for (def i = 0; i < height; i++) {
            assert matrix.getRow(minRowIndex + i) == rows.getValue(minRowIndex + i)
        }
        matrix.getRows() == rows

        and: "columns are as expected"
        for (def i = 0; i < width; i++) {
            assert matrix.getCol(minColIndex + i) == columns.getValue(minColIndex + i)
        }
        matrix.getCols() == columns

        where:
        minRowIndex | maxRowIndex | minColIndex | maxColIndex | inputMatrix        | inputRows                                                | inputColumns
        1           | 3           | 1           | 3           | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33]]               | [[11, 21, 31], [12, 22, 32], [13, 23, 33]]
        3           | 5           | 4           | 7           | [[11, 12, 13, 14],
                                                                 [21, 22, 23, 24],
                                                                 [31, 32, 33, 34]] | [[11, 12, 13, 14], [21, 22, 23, 24], [31, 32, 33, 34]]   | [[11, 21, 31], [12, 22, 32], [13, 23, 33], [14, 24, 34]]
        7           | 10          | 9           | 11          | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33],
                                                                 [41, 42, 43]]     | [[11, 12, 13], [21, 22, 23], [31, 32, 33], [41, 42, 43]] | [[11, 21, 31, 41], [12, 22, 32, 42], [13, 23, 33, 43]]
    }

    def "map(Function<? super V, ? extends W> function, boolean applyToNull): map should apply function to each cell"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build Matrix"
        def builder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def matrix = builder.build()

        def eBuilder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                eBuilder.set(i + 1, j + 1, eInputMatrix.get(i).get(j))
            }
        }
        def expected = eBuilder.build()

        expect:
        matrix.map(function, false) == expected
        matrix.map(function, false).hashCode() == expected.hashCode()

        where:
        inputMatrix                | function                      | eInputMatrix
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]             | { x -> x * 2 }                | [[22, 24, 26],
                                                                      [42, 44, 46],
                                                                      [62, 64, 66]]
        [["11", "12", "13", "14"],
         ["21", "22", "23", "24"],
         ["31", "32", null, "34"]] | { x -> x.replace("1", "11") } | [["1111", "112", "113", "114"],
                                                                      ["211", "22", "23", "24"],
                                                                      ["311", "32", null, "34"]]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]             | { x -> x }                    | [[11, 12, 13],
                                                                      [21, 22, 23],
                                                                      [31, 32, 33],
                                                                      [41, 42, 43]]
    }

    def "transpose(): should return the transposed matrix"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()
        def eHeight = eInputMatrix.size()
        def eWidth = eInputMatrix.get(0).size()

        and: "build Matrix"
        def builder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def matrix = builder.build()

        def eBuilder = new Matrix.Builder(eHeight, eWidth)
        for (def i = 0; i < eHeight; i++) {
            for (def j = 0; j < eWidth; j++) {
                eBuilder.set(i + 1, j + 1, eInputMatrix.get(i).get(j))
            }
        }
        def expected = eBuilder.build()
        def result = matrix.transpose()

        expect:
        expected == result

        where:
        inputMatrix                | eInputMatrix
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]             | [[11, 21, 31],
                                      [12, 22, 32],
                                      [13, 23, 33]]
        [["11", "12", "13", "14"],
         ["21", "22", "23", "24"],
         ["31", "32", null, "34"]] | [["11", "21", "31"],
                                      ["12", "22", "32"],
                                      ["13", "23", null],
                                      ["14", "24", "34"]]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]             | [[11, 21, 31, 41],
                                      [12, 22, 32, 42],
                                      [13, 23, 33, 43]]
    }

    def "isUniform(): returns if the matrix is uniform"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build Matrix"
        def builder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def matrix = builder.build()

        expect:
        matrix.isConstant() == expected

        where:
        inputMatrix                | expected
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]             | false
        [["11", "12", "13", "14"],
         ["21", "22", "23", "24"],
         ["31", "32", null, "34"]] | false
        [[null, null, null],
         [null, null, null],
         [null, null, null],
         [null, null, null]]       | true
        [[1, 1, 1],
         [1, 1, 1],
         [1, 1, 1]]                | true
    }

    def "toStream(): should return the elements of the matrix in row-wise order"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build Matrix"
        def builder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def matrix = builder.build()

        expect:
        matrix.toStream().toArray() == expected.toArray()

        where:
        inputMatrix                | expected
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]             | [11, 12, 13, 21, 22, 23, 31, 32, 33]
        [["11", "12", "13", "14"],
         ["21", "22", "23", "24"],
         ["31", "32", null, "34"]] | ["11", "12", "13", "14", "21", "22", "23", "24", "31", "32", null, "34"]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]             | [11, 12, 13, 21, 22, 23, 31, 32, 33, 41, 42, 43]
    }

    def "iterator(): should go over the elements of the matrix in row-wise order"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build Matrix"
        def builder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def matrix = builder.build()

        expect:
        def itr = matrix.iterator()
        for (def i = 0; i < expected.size(); i++) {
            assert itr.hasNext()
            assert itr.next() == expected.get(i)
        }
        assert !itr.hasNext()

        where:
        inputMatrix                | expected
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]             | [11, 12, 13, 21, 22, 23, 31, 32, 33]
        [["11", "12", "13", "14"],
         ["21", "22", "23", "24"],
         ["31", "32", null, "34"]] | ["11", "12", "13", "14", "21", "22", "23", "24", "31", "32", null, "34"]
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]             | [11, 12, 13, 21, 22, 23, 31, 32, 33, 41, 42, 43]
    }

    def "toString()"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()

        and: "build Matrix"
        def builder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def matrix = builder.build()

        expect:
        matrix.toString() == expected

        where:
        inputMatrix                | expected
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33]]             | "⌈11 12 13⌉\n" +
                "|21 22 23|\n" +
                "⌊31 32 33⌋\n"
        [["11", "12", "13", "14"],
         ["21", "22", "23", "24"],
         ["31", "32", "33", "34"]] | "⌈11 12 13 14⌉\n" +
                "|21 22 23 24|\n" +
                "⌊31 32 33 34⌋\n"
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]             | "⌈11 12 13⌉\n" +
                "|21 22 23|\n" +
                "|31 32 33|\n" +
                "⌊41 42 43⌋\n"
    }

}
