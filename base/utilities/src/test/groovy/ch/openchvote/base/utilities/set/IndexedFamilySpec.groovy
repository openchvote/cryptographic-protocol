/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.set
import spock.lang.Specification

class IndexedFamilySpec extends Specification {

    def "Combined Test of build and getter methods"() {
        given: "an empty List"
        def indexedFamily0 = new IndexedFamily.Builder<String>().build()

        expect:
        indexedFamily0.getSize() == 0
        !indexedFamily0.containsIndex(1)
        indexedFamily0.getElement(1) == null
        indexedFamily0.getIndices().size() == 0
        indexedFamily0.getElements().count() == 0
        !indexedFamily0.iterator().hasNext()

        when: "inserting the first element"
        def val1 = "first"
        def indexedFamily1 = new IndexedFamily.Builder<String>().addElement(1, val1).build()

        then:
        indexedFamily1.getSize() == 1
        indexedFamily1.containsIndex(1)
        !indexedFamily1.containsIndex(2)
        indexedFamily1.getElement(1) == val1
        indexedFamily1.getElement(2) == null
        indexedFamily1.getIndices().toIntArray() == [1].toArray() as int[]
        indexedFamily1.getElements().toArray() == [val1].toArray()

        and: "iterator has one new element"
        def iter = indexedFamily1.iterator()
        iter.hasNext()
        def pair1 = iter.next()
        pair1.getIndex() == 1
        pair1.getElement() == val1
        !iter.hasNext()

        when: "inserting 5 new elements with 5 new keys"
        def val2 = "second"
        def val3 = "third"
        def val4 = "fourth"
        def val5 = "fifth"
        def indexedFamily5 = new IndexedFamily.Builder<String>().addElement(1, val1).addElement(2, val2).addElement(3, val3).addElement(4, val4).addElement(5, val5).build()

        then:
        indexedFamily5.getSize() == 5
        indexedFamily5.containsIndex(1)
        indexedFamily5.containsIndex(2)
        indexedFamily5.containsIndex(3)
        indexedFamily5.containsIndex(4)
        indexedFamily5.containsIndex(5)
        !indexedFamily5.containsIndex(6)
        indexedFamily5.getElement(1) == val1
        indexedFamily5.getElement(2) == val2
        indexedFamily5.getElement(3) == val3
        indexedFamily5.getElement(4) == val4
        indexedFamily5.getElement(5) == val5
        indexedFamily5.getElement(6) == null
        indexedFamily5.getIndices().toIntArray() == [1, 2, 3, 4, 5].toArray() as int[]
        indexedFamily5.getElements().toArray() == [val1, val2, val3, val4, val5].toArray()

        and: "iterator should travers the 5  elements in correct order"
        def iter2 = indexedFamily5.iterator()
        def values2 = [val1, val2, val3, val4, val5]
        for (int i = 1; i <= 5; i++) {
            assert iter2.hasNext()
            def pair = iter2.next()
            assert pair.getIndex() == i
            assert pair.getElement() == values2.get(i - 1)
        }
        !iter2.hasNext()
    }
}
