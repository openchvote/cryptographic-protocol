/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
//file:noinspection GroovyUnusedAssignment
package ch.openchvote.base.utilities.serializer


import ch.openchvote.base.utilities.sequence.*
import ch.openchvote.base.utilities.set.IntSet
import ch.openchvote.base.utilities.tuples.*
import ch.openchvote.base.utilities.tuples.decuple.Decuple
import ch.openchvote.base.utilities.tuples.decuple.UnDecuple

/*
 * Copyright (C) 2024 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */

import spock.lang.Specification

class SerializerSpec extends Specification {

    def "null = deserialize(serialize(null)) should hold"() {
        given:
        def typeReference = new TypeReference<Integer>() {}

        expect:
        null == Serializer.deserialize(Serializer.serialize(null), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Integer x"() {
        given:
        def typeReference = new TypeReference<Integer>() {}

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        x                     | _
        Integer.MAX_VALUE     | _
        Integer.MIN_VALUE     | _
        Integer.valueOf(0)    | _
        Integer.valueOf(-237) | _
        Integer.valueOf(2857) | _
    }

    def "x = deserialize(serialize(x)) should hold for all Double x"() {
        given:
        def typeReference = new TypeReference<Double>() {}

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        x                     | _
        Double.MAX_VALUE      | _
        Double.MIN_VALUE      | _
        Double.valueOf(0)     | _
        Double.valueOf(-237)  | _
        Double.valueOf(2857)  | _
        Double.valueOf(1.234) | _
        Double.valueOf(-23.4) | _
    }

    def "x = deserialize(serialize(x)) should hold for all BigInteger x"() {
        given:
        def typeReference = new TypeReference<BigInteger>() {}

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        x                                                 | _
        BigInteger.TWO.pow(1024)                          | _
        BigInteger.TWO.pow(1024) * BigInteger.valueOf(-1) | _
        BigInteger.valueOf(0)                             | _
        BigInteger.valueOf(-237)                          | _
        BigInteger.valueOf(2857)                          | _
    }

    def "x = deserialize(serialize(x)) should hold for all ByteArray x"() {
        given:
        def typeReference = new TypeReference<ByteArray>() {}

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        x                                                      | _
        ByteArray.of([] as byte[])                             | _
        ByteArray.of([0xEA] as byte[])                         | _
        ByteArray.of([0x00, 0x00, 0x00] as byte[])             | _
        ByteArray.of([0xFF, 0xFF, 0xFF, 0xFF, 0xFF] as byte[]) | _
    }

    def "x = deserialize(serialize(x)) should hold for all Boolean x"() {
        given:
        def typeReference = new TypeReference<Boolean>() {}

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        x     | _
        true  | _
        false | _
    }

    def "x = deserialize(serialize(x)) should hold for all String x"() {
        given:
        def typeReference = new TypeReference<String>() {}

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        x                                                                                                                                                                                                                                                                                                                                                                                                                                               | _
        ""                                                                                                                                                                                                                                                                                                                                                                                                                                              | _
        "Hello World"                                                                                                                                                                                                                                                                                                                                                                                                                                   | _
        "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." | _
        "def NewSerializer = new NewSerializer<String>() {}\n \n  expect:\n \n  x == NewSerializer.deserialize(NewSerializer.serialize(x))"                                                                                                                                                                                                                                                                                                             | _
    }

    def "x = deserialize(serialize(x)) should hold for all IntVector x"() {
        given:
        def typeReference = new TypeReference<IntVector>() {}
        def xBuilder = new IntVector.Builder(input.size())
        input.each {
            xBuilder.add(it)
        }
        def x = xBuilder.build()

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        input             | _
        []                | _
        [0, 1, 0]         | _
        [728, 5763, 7462] | _
    }

    def "x = deserialize(serialize(x)) should hold for all IntVector x with indexes not beginning with 1"() {
        given:
        def typeReference = new TypeReference<IntVector>() {}
        def xBuilder = new IntVector.Builder(minIndex, maxIndex)
        input.each {
            xBuilder.add(it)
        }
        def x = xBuilder.build()

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        input             | minIndex | maxIndex
        [0]               | 0        | 0
        [0, 1, 0]         | 2        | 4
        [728, 5763, 7462] | 26       | 28
    }

    def "x = deserialize(serialize(x)) should hold for all Vector x"() {
        given:
        def typeReference = new TypeReference<Vector<String>>() {}
        def xBuilder = new Vector.Builder(input.size())
        input.each {
            xBuilder.add(it)
        }
        def x = xBuilder.build()

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        input                   | _
        []                      | _
        ["0", null, "0"]        | _
        ["728", "5763", "7462"] | _
    }

    def "x = deserialize(serialize(x)) should hold for all Vector x with indexes not beginning with 1"() {
        given:
        def typeReference = new TypeReference<Vector<BigInteger>>() {}
        def xBuilder = new Vector.Builder(minIndex, maxIndex)
        input.each {
            xBuilder.add(it)
        }
        def x = xBuilder.build()

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        input                                             | minIndex | maxIndex
        [BigInteger.valueOf(1736539)]                     | 0        | 0
        [BigInteger.ONE, BigInteger.ZERO, BigInteger.ONE] | 2        | 4
        [null, null, null]                                | 26       | 28
    }

    def "x = deserialize(serialize(x)) should hold for all IntMatrix x"() {
        given:
        def typeReference = new TypeReference<IntMatrix>() {}
        def height = inputMatrix.size()
        def width = (height == 0) ? 0 : inputMatrix.get(0).size()
        def xBuilder = new IntMatrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                xBuilder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def x = xBuilder.build()

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        inputMatrix        | _
        []                 | _
        [[11, 12, 13, 14],
         [21, 22, 23, 24],
         [31, 32, 33, 34]] | _
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]     | _
    }

    def "x = deserialize(serialize(x)) should hold for all IntMatrix x with different indexes"() {
        given:
        def typeReference = new TypeReference<IntMatrix>() {}
        def height = inputMatrix.size()
        def width = (height == 0) ? 0 : inputMatrix.get(0).size()
        def xBuilder = new IntMatrix.Builder(minRowIndex, maxRowIndex, minColIndex, maxColIndex)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                xBuilder.add(inputMatrix.get(i).get(j))
            }
        }
        def x = xBuilder.build()

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        minRowIndex | maxRowIndex | minColIndex | maxColIndex | inputMatrix
        1           | 3           | 1           | 3           | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33]]
        3           | 9           | 4           | 4           | [[11, 12, 13, 14, 15, 16, 17]]
        7           | 7           | 9           | 13          | [[11],
                                                                 [21],
                                                                 [31],
                                                                 [41],
                                                                 [51]]
    }

    def "x = deserialize(serialize(x)) should hold for all Matrix x"() {
        given:
        def typeReference = new TypeReference<Matrix<String>>() {}
        def height = inputMatrix.size()
        def width = (height == 0) ? 0 : inputMatrix.get(0).size()
        def xBuilder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                xBuilder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def x = xBuilder.build()

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        inputMatrix                | _
        []                         | _
        [["11", "12", "13", "14"],
         ["21", null, "23", "24"],
         ["31", "32", null, "34"]] | _
        [["11", "12", "13"],
         ["21", "22", "23"],
         ["31", "32", "33"],
         ["41", "42", "43"]]       | _
    }

    def "x = deserialize(serialize(x)) should hold for all Matrix x with different indexes"() {
        given:
        def typeReference = new TypeReference<Matrix<BigInteger>>() {}
        def height = maxColIndex - minColIndex
        def width = maxRowIndex - minRowIndex
        def xBuilder = new Matrix.Builder(minRowIndex, maxRowIndex, minColIndex, maxColIndex)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                xBuilder.add(inputMatrix.get(i).get(j))
            }
        }
        def x = xBuilder.build()

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)

        where:
        minRowIndex | maxRowIndex | minColIndex | maxColIndex | inputMatrix
        1           | 3           | 1           | 3           | [[BigInteger.valueOf(11), BigInteger.valueOf(12), BigInteger.valueOf(13)],
                                                                 [BigInteger.valueOf(21), BigInteger.valueOf(22), BigInteger.valueOf(23)],
                                                                 [BigInteger.valueOf(31), BigInteger.valueOf(32), BigInteger.valueOf(33)]]
        3           | 9           | 4           | 4           | [[null, null, null, null, null, null, null]]
        7           | 7           | 9           | 13          | [[BigInteger.valueOf(11)],
                                                                 [BigInteger.valueOf(21)],
                                                                 [BigInteger.valueOf(31)],
                                                                 [BigInteger.valueOf(41)],
                                                                 [BigInteger.valueOf(51)]]
    }

    def "x = deserialize(serialize(x)) should hold for all IntSet s"() {
        given:
        def typeReference = new TypeReference<IntSet>() {}
        def s1 = IntSet.of(input as Integer[])
        def s2 = Serializer.deserialize(Serializer.serialize(s1), typeReference)

        expect:
        s1.containsAll(s2)
        s2.containsAll(s1)

        where:
        input             | _
        []                | _
        [0]               | _
        [0, 1]            | _
        [728, 5763, 7462] | _
    }


    def "x = deserialize(serialize(x)) should hold for all NullTuple x"() {
        given:
        def typeReference = new TypeReference<NullTuple>() {}
        def x = new NullTuple()

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Singleton x"() {
        given:
        def typeReference = new TypeReference<Singleton<String>>() {}
        def x = new Singleton("first")

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Pair x"() {
        given:
        def typeReference = new TypeReference<Pair<String, Integer>>() {}
        def x = new Pair("first", 2)

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Triple x"() {
        given:
        def typeReference = new TypeReference<Triple<String, Integer, BigInteger>>() {}
        def x = new Triple("first", 2, BigInteger.valueOf(3))

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Quadruple x"() {
        given:
        def typeReference = new TypeReference<Quadruple<String, Integer, BigInteger, Boolean>>() {}
        def x = new Quadruple("first", 2, BigInteger.valueOf(3), true)

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Quintuple x"() {
        given:
        def typeReference = new TypeReference<Quintuple<String, Integer, BigInteger, Boolean, Singleton<Integer>>>() {}
        def x = new Quintuple("first", 2, BigInteger.valueOf(3), true, new Singleton<Integer>(5))

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Sextuple x"() {
        given:
        def typeReference = new TypeReference<Sextuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>>>() {
        }
        def x = new Sextuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build()
        )

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Septuple x"() {
        given:
        def typeReference = new TypeReference<Septuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>, Integer>>() {
        }
        def x = new Septuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build(),
                7
        )

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Octuple x"() {
        given:
        def typeReference = new TypeReference<Octuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>, Integer, String>>() {
        }
        def x = new Octuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build(),
                7,
                "eight"
        )

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Nonuple x"() {
        given:
        def typeReference = new TypeReference<Nonuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>, Integer, String, BigInteger>>() {
        }
        def x = new Nonuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build(),
                7,
                "eight",
                BigInteger.valueOf(9)
        )

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Decuple x"() {
        given:
        def typeReference = new TypeReference<Decuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>, Integer, String, BigInteger, Pair<String, String>>>() {
        }
        def x = new Decuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build(),
                7,
                "eight",
                BigInteger.valueOf(9),
                new Pair("first", "second")
        )

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for all Undecuple x"() {
        given:
        def typeReference = new TypeReference<UnDecuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>, Integer, String, BigInteger, Pair<String, String>, String>>() {
        }
        def x = new UnDecuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build(),
                7,
                "eight",
                BigInteger.valueOf(9),
                new Pair("first", "second"),
                "last"
        )

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for an ArrayList x"() {
        given:
        def typeReference = new TypeReference<ArrayList<String>>() {}
        def x = new ArrayList<String>()
        x.add("s1")
        x.add("s2")
        x.add("s3")

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    def "x = deserialize(serialize(x)) should hold for an HashMap x"() {
        given:
        def typeReference = new TypeReference<HashMap<String, Pair<Integer, Boolean>>>() {}
        def x = new HashMap<String, Pair<Integer, Boolean>>()
        x.put("s1", new Pair(1, true))
        x.put("s2", new Pair(2, true))
        x.put("s3", new Pair(3, false))

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
        Serializer.deserialize(Serializer.serialize(x), typeReference).getClass() == HashMap.class
    }

    def "x = deserialize(serialize(x)) should hold for an TreeMap x"() {
        given:
        def typeReference = new TypeReference<TreeMap<String, Pair<Integer, Boolean>>>() {}
        def x = new TreeMap<String, Pair<Integer, Boolean>>()
        x.put("s1", new Pair(1, true))
        x.put("s2", new Pair(2, true))
        x.put("s3", new Pair(3, false))

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
        Serializer.deserialize(Serializer.serialize(x), typeReference).getClass() == TreeMap.class
    }

    def "x = deserialize(serialize(x)) should hold for a NonGenericClass x"() {
        given:
        def map = new HashMap<BigInteger, IntMatrix>()
        def builder1 = new IntMatrix.Builder(2, 3)
        builder1.set(1, 1, 1).set(1, 2, 2).set(2, 3, 3)
        def intMatrix1 = builder1.build()
        def builder2 = new IntMatrix.Builder(2, 3)
        builder2.set(2, 1, 1).set(2, 2, 2).set(1, 3, 3)
        def intMatrix2 = builder2.build()
        map.put(BigInteger.ONE, intMatrix1)
        map.put(BigInteger.TWO, intMatrix2)
        def x = new NonGenericClass(Vector.of(1, 2, 3), new Pair("x", 4), map)

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), NonGenericClass.class, "TYPE_REFERENCE")
    }

    static final class NonGenericClass extends Triple<Vector<Integer>, Pair<String, Integer>, HashMap<BigInteger, IntMatrix>> {

        static public final TypeReference<NonGenericClass> TYPE_REFERENCE = new TypeReference<NonGenericClass>() {
        }

        NonGenericClass(Vector<Integer> first, Pair<String, Integer> second, HashMap<BigInteger, IntMatrix> third) {
            super(first, second, third)
        }
    }

    def "x = deserialize(serialize(x)) should hold for a GenericClass x"() {
        given:
        def map = new HashMap<BigInteger, IntMatrix>()
        def builder1 = new IntMatrix.Builder(2, 3)
        builder1.set(1, 1, 1).set(1, 2, 2).set(2, 3, 3)
        def intMatrix1 = builder1.build()
        def builder2 = new IntMatrix.Builder(2, 3)
        builder2.set(2, 1, 1).set(2, 2, 2).set(1, 3, 3)
        def intMatrix2 = builder2.build()
        map.put(BigInteger.ONE, intMatrix1)
        map.put(BigInteger.TWO, intMatrix2)
        def typeReference = new TypeReference<GenericClass<Boolean>>() {}
        def x = new GenericClass(Vector.of(1, 2, 3), true, map)

        expect:
        x == Serializer.deserialize(Serializer.serialize(x), typeReference)
    }

    static final class GenericClass<T> extends Triple<Vector<Integer>, T, HashMap<BigInteger, IntMatrix>> {

        GenericClass(Vector<Integer> first, T second, HashMap<BigInteger, IntMatrix> third) {
            super(first, second, third)
        }
    }

}
