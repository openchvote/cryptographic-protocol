/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.set

import ch.openchvote.base.utilities.sequence.IntMatrix
import ch.openchvote.base.utilities.sequence.IntVector
import spock.lang.Specification

import java.util.stream.IntStream

class IntSetSpec extends Specification {

    def "range(int lb, int ub)"() {
        expect:
        set.contains(x) == expectedContains
        set.toIntStream().sorted().toArray() == expectedToStream as int[]
        set.getSize() == size

        where:
        set                  | x   | expectedContains | expectedToStream             | size
        IntSet.range(4, 7)   | 4   | true             | [4, 5, 6, 7]                 | 4
        IntSet.range(4, 7)   | 7   | true             | [4, 5, 6, 7]                 | 4
        IntSet.range(88, 94) | 400 | false            | [88, 89, 90, 91, 92, 93, 94] | 7
        IntSet.range(88, 94) | 87  | false            | [88, 89, 90, 91, 92, 93, 94] | 7
        IntSet.range(1, 3)   | 4   | false            | [1, 2, 3]                    | 3
        IntSet.range(1, 3)   | 2   | true             | [1, 2, 3]                    | 3
    }

    def "of(Integer... values): should create a Set of the given values"() {
        given:
        def intSet = IntSet.of(set as Integer[])

        expect:
        intSet.contains(x) == expectedContains
        intSet.toIntStream().sorted().toArray() == expectedToStream as int[]

        where:
        set               | x  | expectedContains | expectedToStream
        [1, 2, 3]         | 1  | true             | [1, 2, 3]
        [1, 2, 3]         | 3  | true             | [1, 2, 3]
        [1, 2, 3]         | 0  | false            | [1, 2, 3]
        [1, 2, 3]         | 5  | false            | [1, 2, 3]
        [32, 56, 87, 123] | 87 | true             | [32, 56, 87, 123]
        [32, 56, 87, 123] | 56 | true             | [32, 56, 87, 123]
        [32, 56, 87, 123] | 33 | false            | [32, 56, 87, 123]
        [32, 56, 87, 123] | 67 | false            | [32, 56, 87, 123]
    }

    def "of(java.util.Set<Integer> values): should create a Set of the given values"() {
        given:
        def selectionSet = new TreeSet<Integer>(set)
        def intSet = IntSet.of(selectionSet)

        expect:
        intSet.contains(x) == expectedContains
        intSet.toIntStream().sorted().toArray() == expectedToStream as int[]

        where:
        set               | x  | expectedContains | expectedToStream
        [1, 2, 3]         | 1  | true             | [1, 2, 3]
        [1, 2, 3]         | 3  | true             | [1, 2, 3]
        [1, 2, 3]         | 0  | false            | [1, 2, 3]
        [1, 2, 3]         | 5  | false            | [1, 2, 3]
        [32, 56, 87, 123] | 87 | true             | [32, 56, 87, 123]
        [32, 56, 87, 123] | 56 | true             | [32, 56, 87, 123]
        [32, 56, 87, 123] | 33 | false            | [32, 56, 87, 123]
        [32, 56, 87, 123] | 67 | false            | [32, 56, 87, 123]
    }

    def "contains(int x): should return if the given int value #x is in the Set #set or not"() {
        expect:
        set.contains(x) == expected

        where:
        set                 | x      | expected
        IntSet.range(4, 92) | 3      | false
        IntSet.range(4, 92) | 4      | true
        IntSet.range(4, 92) | 56     | true
        IntSet.range(4, 92) | 92     | true
        IntSet.range(4, 92) | 93     | false
        IntSet.range(4, 92) | 400    | false
        IntSet.NN           | 0      | true
        IntSet.NN           | 1      | true
        IntSet.NN           | 732659 | true
        IntSet.NN           | -1     | false
        IntSet.NN           | -38798 | false
        IntSet.NN_plus      | 0      | false
        IntSet.NN_plus      | 1      | true
        IntSet.NN_plus      | 897329 | true
        IntSet.NN           | -1     | false
        IntSet.NN_plus      | -32987 | false
        IntSet.NN_plus(7)   | 0      | false
        IntSet.NN_plus(7)   | 1      | true
        IntSet.NN_plus(7)   | 6      | true
        IntSet.NN_plus(7)   | 7      | true
        IntSet.NN_plus(7)   | 8      | false
        IntSet.NN_plus(7)   | -1     | false
        IntSet.ZZ(7)        | 0      | true
        IntSet.ZZ(7)        | 1      | true
        IntSet.ZZ(7)        | 6      | true
        IntSet.ZZ(7)        | 7      | false
        IntSet.ZZ(7)        | 8      | false
        IntSet.ZZ(7)        | -1     | false
        IntSet.BB           | 0      | true
        IntSet.BB           | 1      | true
        IntSet.BB           | -1     | false
        IntSet.BB           | 23     | false
    }

    def "containsAll(IntStream stream) should return if the given stream of int values #x are all in the Set #set or not"() {
        given:
        def stream = Arrays.stream(x as int[])

        expect:
        set.containsAll(stream as IntStream) == expected

        where:
        set                 | x                                                  | expected
        IntSet.range(4, 92) | [4, 5, 8, 92]                                      | true
        IntSet.range(4, 92) | [5, 23, 43, 56, 78]                                | true
        IntSet.range(4, 92) | [1, 2, 3, 4, 5]                                    | false
        IntSet.range(4, 92) | [5, 56, 93, 99]                                    | false
        IntSet.NN           | [0, 1, 2, 3, 4]                                    | true
        IntSet.NN           | [732659, 92334, 983212, Integer.MAX_VALUE]         | true
        IntSet.NN           | [-1, 0, 1, 2, 3, 4]                                | false
        IntSet.NN           | [732659, 92334, 983212, -38798, Integer.MAX_VALUE] | false
        IntSet.NN_plus      | [1, 234, 1293, 1726, 123]                          | true
        IntSet.NN_plus      | [897329, Integer.MAX_VALUE, 1498, 90213]           | true
        IntSet.NN_plus      | [1, 234, 1293, 1726, 123, 0]                       | false
        IntSet.NN_plus      | [897329, Integer.MAX_VALUE, -32987, 1498, 90213]   | false
        IntSet.NN_plus(7)   | [1, 1, 3, 4, 5, 6, 1]                              | true
        IntSet.NN_plus(7)   | [7]                                                | true
        IntSet.NN_plus(7)   | [1, 3, 4, 0, 0, 0]                                 | false
        IntSet.NN_plus(7)   | [1, 2, 43, 5, 13]                                  | false
        IntSet.BB           | [0, 0, 0, 0, 1, 1, 0]                              | true
        IntSet.BB           | []                                                 | true
        IntSet.BB           | [1, 1, 0, 2, -1]                                   | false
        IntSet.BB           | [0, 1, 2, 0]                                       | false
    }

    def "containsAll(IntVector vector) should return if the given vector of int values #x are all in the Set #set or not"() {
        given:
        def vector = IntVector.of(x as int[])

        expect:
        set.containsAll(vector) == expected

        where:
        set                 | x                                                  | expected
        IntSet.range(4, 92) | [4, 5, 8, 92]                                      | true
        IntSet.range(4, 92) | [5, 23, 43, 56, 78]                                | true
        IntSet.range(4, 92) | [1, 2, 3, 4, 5]                                    | false
        IntSet.range(4, 92) | [5, 56, 93, 99]                                    | false
        IntSet.NN           | [0, 1, 2, 3, 4]                                    | true
        IntSet.NN           | [732659, 92334, 983212, Integer.MAX_VALUE]         | true
        IntSet.NN           | [-1, 0, 1, 2, 3, 4]                                | false
        IntSet.NN           | [732659, 92334, 983212, -38798, Integer.MAX_VALUE] | false
        IntSet.NN_plus      | [1, 234, 1293, 1726, 123]                          | true
        IntSet.NN_plus      | [897329, Integer.MAX_VALUE, 1498, 90213]           | true
        IntSet.NN_plus      | [1, 234, 1293, 1726, 123, 0]                       | false
        IntSet.NN_plus      | [897329, Integer.MAX_VALUE, -32987, 1498, 90213]   | false
        IntSet.NN_plus(7)   | [1, 1, 3, 4, 5, 6, 1]                              | true
        IntSet.NN_plus(7)   | [7]                                                | true
        IntSet.NN_plus(7)   | [1, 3, 4, 0, 0, 0]                                 | false
        IntSet.NN_plus(7)   | [1, 2, 43, 5, 13]                                  | false
        IntSet.BB           | [0, 0, 0, 0, 1, 1, 0]                              | true
        IntSet.BB           | []                                                 | true
        IntSet.BB           | [1, 1, 0, 2, -1]                                   | false
        IntSet.BB           | [0, 1, 2, 0]                                       | false
    }

    def "containsAll(IntMatrix matrix) should return if the given matrix of int values #x are all in the Set #set or not"() {
        given: "build IntMatrix"
        def height = x.size()
        def width = x.get(0).size()

        and:
        def builder = new IntMatrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, x.get(i).get(j))
            }
        }
        def intMatrix = builder.build()

        expect:
        set.containsAll(intMatrix) == expected

        where:
        set                 | x                                                    | expected
        IntSet.range(4, 92) | [[4, 5, 8, 92],
                               [5, 23, 43, 56]]                                    | true
        IntSet.range(4, 92) | [[1, 2, 4, 5],
                               [5, 56, 93, 99]]                                    | false
        IntSet.NN           | [[0, 1, 2, 3],
                               [732659, 92334, 983212, Integer.MAX_VALUE]]         | true
        IntSet.NN           | [[-1, 0, 1, 2, 3],
                               [732659, 92334, 983212, -38798, Integer.MAX_VALUE]] | false
    }

    def "contains(Integer x): should return if the given int value #x is in the Set #set or not"() {
        expect:
        set.contains(x) == expected

        where:
        set                 | x                       | expected
        IntSet.range(4, 92) | Integer.valueOf(4)      | true
        IntSet.range(4, 92) | Integer.valueOf(92)     | true
        IntSet.range(4, 92) | Integer.valueOf(400)    | false
        IntSet.range(4, 92) | Integer.valueOf(3)      | false
        IntSet.range(4, 92) | Integer.valueOf(93)     | false
        IntSet.range(4, 92) | Integer.valueOf(56)     | true
        IntSet.NN           | Integer.valueOf(0)      | true
        IntSet.NN           | Integer.valueOf(732659) | true
        IntSet.NN           | Integer.valueOf(-1)     | false
        IntSet.NN           | Integer.valueOf(-38798) | false
        IntSet.NN_plus      | Integer.valueOf(1)      | true
        IntSet.NN_plus      | Integer.valueOf(897329) | true
        IntSet.NN_plus      | Integer.valueOf(0)      | false
        IntSet.NN_plus      | Integer.valueOf(-32987) | false
        IntSet.NN_plus(7)   | Integer.valueOf(1)      | true
        IntSet.NN_plus(7)   | Integer.valueOf(7)      | true
        IntSet.NN_plus(7)   | Integer.valueOf(0)      | false
        IntSet.NN_plus(7)   | Integer.valueOf(8)      | false
        IntSet.BB           | Integer.valueOf(0)      | true
        IntSet.BB           | Integer.valueOf(1)      | true
        IntSet.BB           | Integer.valueOf(-1)     | false
        IntSet.BB           | Integer.valueOf(23)     | false
    }

    def "size()"() {
        expect:
        set.getSize() == size

        where:
        set                  | size
        IntSet.of()          | 0
        IntSet.of(1)         | 1
        IntSet.of(1, 2)      | 2
        IntSet.range(0, 0)   | 1
        IntSet.range(0, 1)   | 2
        IntSet.range(0, 100) | 101
        IntSet.ZZ(0)         | 0
        IntSet.ZZ(1)         | 1
        IntSet.ZZ(100)       | 100
        IntSet.NN_plus(0)    | 0
        IntSet.NN_plus(1)    | 1
        IntSet.NN_plus(100)  | 100
        IntSet.NN_plus       | Integer.MAX_VALUE
        IntSet.NN            | Integer.MAX_VALUE + 1L
    }

}
