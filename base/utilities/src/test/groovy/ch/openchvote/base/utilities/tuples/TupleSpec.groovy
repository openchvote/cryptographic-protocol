/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples


import ch.openchvote.base.utilities.sequence.Vector
import ch.openchvote.base.utilities.tuples.decuple.Decuple
import ch.openchvote.base.utilities.tuples.decuple.UnDecuple
import spock.lang.Specification

class TupleSpec extends Specification {
    /**
     * Returns a vector of type {@code Object} containing all elements of the tuple.
     *
     * @return A vector of type {@code Object} containing all elements of the tuple
     */
    //public Vector<Object> toVector()

    def "Test if NullTuples are correctly converted to Vectors"() {
        given:
        def tuple = new NullTuple()
        def tupleVector = tuple.toVector()
        def vector = Vector.of()

        expect:
        tupleVector == vector
    }

    def "Test if Singletons are correctly converted to Vectors"() {
        given:
        def tuple = new Singleton(elems1.get(0))
        def tupleVector = tuple.toVector()
        def vector = Vector.of(expected.get(0) as Object)

        expect:
        tupleVector == vector

        where:
        elems1    | expected
        ["first"] | ["first"]
        [1]       | [1]
    }

    def "Test if Pairs are correctly converted to Vectors"() {
        given:
        def tuple = new Pair(elems1.get(0), elems1.get(1))
        def tupleVector = tuple.toVector()
        def vector = Vector.of(elems1.get(0) as Object, elems1.get(1))

        expect:
        tupleVector == vector

        where:
        elems1              | expected
        ["first", "second"] | ["first", "second"]
        [1, 2]              | [1, 2]
    }

    def "Test if Triple are correctly converted to Vectors"() {
        given:
        def tuple = new Triple(elems1.get(0), elems1.get(1), elems1.get(2))
        def tupleVector = tuple.toVector()
        def vector = Vector.of(elems1.get(0) as Object, elems1.get(1), elems1.get(2))

        expect:
        tupleVector == vector

        where:
        elems1                       | expected
        ["first", "second", "third"] | ["first", "second", "third"]
        [1, 2, 3]                    | [1, 2, 3]
    }

    def "Test if Quadruple are correctly converted to Vectors"() {
        given:
        def tuple = new Quadruple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3))
        def tupleVector = tuple.toVector()
        def vector = Vector.of(elems1.get(0) as Object, elems1.get(1), elems1.get(2), elems1.get(3))

        expect:
        tupleVector == vector

        where:
        elems1                                 | expected
        ["first", "second", "third", "fourth"] | ["first", "second", "third", "fourth"]
        [1, 2, 3, 4]                           | [1, 2, 3, 4]
    }

    def "Test if Quintuple are correctly converted to Vectors"() {
        given:
        def tuple = new Quintuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4))
        def tupleVector = tuple.toVector()
        def vector = Vector.of(elems1.get(0) as Object, elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4))

        expect:
        tupleVector == vector

        where:
        elems1                                          | expected
        ["first", "second", "third", "fourth", "fifth"] | ["first", "second", "third", "fourth", "fifth"]
        [1, 2, 3, 4, 5]                                 | [1, 2, 3, 4, 5]
    }

    def "Test if Sextuple are correctly converted to Vectors"() {
        given:
        def tuple = new Sextuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5))
        def tupleVector = tuple.toVector()
        def vector = Vector.of(elems1.get(0) as Object, elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5))

        expect:
        tupleVector == vector

        where:
        elems1                                                   | expected
        ["first", "second", "third", "fourth", "fifth", "sixth"] | ["first", "second", "third", "fourth", "fifth", "sixth"]
        [1, 2, 3, 4, 5, 6]                                       | [1, 2, 3, 4, 5, 6]
    }

    def "Test if Septuple are correctly converted to Vectors"() {
        given:
        def tuple = new Septuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6))
        def tupleVector = tuple.toVector()
        def vector = Vector.of(elems1.get(0) as Object, elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6))

        expect:
        tupleVector == vector

        where:
        elems1                                                              | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"]
        [1, 2, 3, 4, 5, 6, 7]                                               | [1, 2, 3, 4, 5, 6, 7]
    }

    def "Test if Octuple are correctly converted to Vectors"() {
        given:
        def tuple = new Octuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7))
        def tupleVector = tuple.toVector()
        def vector = Vector.of(elems1.get(0) as Object, elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7))

        expect:
        tupleVector == vector

        where:
        elems1                                                                        | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"]
        [1, 2, 3, 4, 5, 6, 7, 8]                                                      | [1, 2, 3, 4, 5, 6, 7, 8]
    }

    def "Test if Nonuple are correctly converted to Vectors"() {
        given:
        def tuple = new Nonuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8))
        def tupleVector = tuple.toVector()
        def vector = Vector.of(elems1.get(0) as Object, elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8))

        expect:
        tupleVector == vector

        where:
        elems1                                                                                 | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"]
        [1, 2, 3, 4, 5, 6, 7, 8, 9]                                                            | [1, 2, 3, 4, 5, 6, 7, 8, 9]
    }

    def "Test if Dectuple are correctly converted to Vectors"() {
        given:
        def tuple = new Decuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8), elems1.get(9))
        def tupleVector = tuple.toVector()
        def vector = Vector.of(elems1.get(0) as Object, elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8), elems1.get(9))

        expect:
        tupleVector == vector

        where:
        elems1                                                                                          | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"]
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]                                                                 | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    }

    def "Test if Undecuple are correctly converted to Vectors"() {
        given:
        def tuple = new UnDecuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8), elems1.get(9), elems1.get(10))
        def tupleVector = tuple.toVector()
        def vector = Vector.of(elems1.get(0) as Object, elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8), elems1.get(9), elems1.get(10))

        expect:
        tupleVector == vector

        where:
        elems1                                                                                                      | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"]
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]                                                                         | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    }

}
