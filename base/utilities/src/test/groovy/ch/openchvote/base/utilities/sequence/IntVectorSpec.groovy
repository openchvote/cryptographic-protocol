/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence

import ch.openchvote.base.utilities.set.IntSet
import ch.openchvote.base.utilities.tools.IntBiPredicate
import spock.lang.Specification
import spock.lang.Unroll

import java.util.function.IntFunction
import java.util.function.IntUnaryOperator

class IntVectorSpec extends Specification {

    def "Creating new vector with Builder()"() {
        when:
        def addVectorBuilder = new IntVector.Builder()
        input.each {
            addVectorBuilder.add(it)
        }
        def addVector = addVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert addVector.getValue(i + 1) == input.get(i)
        }
        addVector.getLength() == input.size()
        addVector.getMinIndex() == 1
        addVector.getMaxIndex() == input.size()

        when:
        def setVectorBuilder = new IntVector.Builder()
        for (int i = 0; i < input.size(); i++) {
            setVectorBuilder.set(i + 1, input.get(i))
        }
        def setVector = setVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert setVector.getValue(i + 1) == input.get(i)
        }
        setVector.getLength() == input.size()
        setVector.getMinIndex() == 1
        setVector.getMaxIndex() == input.size()

        where:
        input             | _
        [1, 2, 3, 4]      | _
        [0, 1, 0]         | _
        [728, 5763, 7462] | _
    }

    def "Creating new vector with Builder(int length) and fill(T value)"() {
        when:
        def addVectorBuilder = new IntVector.Builder(length)

        addVectorBuilder.fill(input)
        def addVector = addVectorBuilder.build()

        then:
        for (int i = 0; i < length; i++) {
            assert addVector.getValue(i + 1) == expected.get(i)
        }
        addVector.getLength() == length
        addVector.getMinIndex() == 1
        addVector.getMaxIndex() == length

        where:
        input | length | expected
        1     | 4      | [1, 1, 1, 1]
        2     | 3      | [2, 2, 2]
        4728  | 2      | [4728, 4728]
        0     | 1      | [0]
    }

    def "Creating new vector with Builder(int length)"() {
        when:
        def addVectorBuilder = new IntVector.Builder(input.size())
        input.each {
            addVectorBuilder.add(it)
        }
        def addVector = addVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert addVector.getValue(i + 1) == input.get(i)
        }
        addVector.getLength() == input.size()
        addVector.getMinIndex() == 1
        addVector.getMaxIndex() == input.size()

        when:
        def setVectorBuilder = new IntVector.Builder(input.size())
        for (int i = 0; i < input.size(); i++) {
            setVectorBuilder.set(i + 1, input.get(i))
        }
        def setVector = setVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert setVector.getValue(i + 1) == input.get(i)
        }
        setVector.getLength() == input.size()
        setVector.getMinIndex() == 1
        setVector.getMaxIndex() == input.size()

        where:
        input             | _
        [1, 2, 3, 4]      | _
        [0, 1, 0]         | _
        [728, 5763, 7462] | _
    }

    def "Creating new vector with Builder(int minIndex, int maxIndex) and fill(T value)"() {
        when:
        def addVectorBuilder = new IntVector.Builder(minIndex, maxIndex)

        addVectorBuilder.fill(input)
        def addVector = addVectorBuilder.build()

        then:
        for (int i = 0; i < length; i++) {
            assert addVector.getValue(i + minIndex) == expected.get(i)
        }
        addVector.getLength() == length
        addVector.getMinIndex() == minIndex
        addVector.getMaxIndex() == maxIndex

        where:
        input | length | minIndex | maxIndex | expected
        1     | 4      | 4        | 7        | [1, 1, 1, 1]
        2     | 3      | 8        | 10       | [2, 2, 2]
        4728  | 2      | 41       | 42       | [4728, 4728]
        0     | 1      | 1        | 1        | [0]
    }

    def "Creating new vector with Builder(int minIndex, int maxIndex)"() {
        when:
        def addVectorBuilder = new IntVector.Builder(minIndex, maxIndex)
        input.each {
            addVectorBuilder.add(it)
        }
        def addVector = addVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert addVector.getValue(i + minIndex) == input.get(i)
        }
        addVector.getLength() == input.size()
        addVector.getMinIndex() == minIndex
        addVector.getMaxIndex() == maxIndex

        when:
        def setVectorBuilder = new IntVector.Builder(minIndex, maxIndex)
        for (int i = 0; i < input.size(); i++) {
            setVectorBuilder.set(i + minIndex, input.get(i))
        }
        def setVector = setVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert setVector.getValue(i + minIndex) == input.get(i)
        }
        setVector.getLength() == input.size()
        setVector.getMinIndex() == minIndex
        setVector.getMaxIndex() == maxIndex

        where:
        input             | minIndex | maxIndex
        [1, 2, 3, 4]      | 4        | 7
        [0, 1, 0]         | 2        | 4
        [728, 5763, 7462] | 26       | 28
    }

    def "Creating new vector with of(V[] values)"() {
        when:
        def vector = IntVector.of(input as int[])

        then:
        for (int i = 0; i < input.size(); i++) {
            assert vector.getValue(i + 1) == input.get(i)
        }
        vector.getLength() == input.size()
        vector.getMinIndex() == 1
        vector.getMaxIndex() == input.size()

        where:
        input             | _
        [1, 2, 3, 4]      | _
        [0, 1, 0]         | _
        [728, 9475, 4623] | _
    }

    @Unroll
    def "map(IntFunction<? extends W> function) should apply the given function too each element of the vector"() {
        def vector = IntVector.of(input as int[])
        def expected = Vector.of(eInput.toArray())
        expect:
        vector.mapToObj(function as IntFunction) == expected
        vector.mapToObj(function as IntFunction).hashCode() == expected.hashCode()

        where:
        input              | function       | eInput
        [1, 2, 3, 4, 5]    | { n -> n * 2 } | [2, 4, 6, 8, 10]
        [123, 456, 0, 789] | { n -> n * 2 } | [246, 912, 0, 1578]

    }

    @Unroll
    def "map(IntUnaryOperator function) should apply the given function too each element of the vector"() {
        def vector = IntVector.of(input as int[])
        def expected = IntVector.of(eInput as int[])
        expect:
        vector.map(function as IntUnaryOperator) == expected
        vector.map(function as IntUnaryOperator).hashCode() == expected.hashCode()

        where:
        input              | function       | eInput
        [1, 2, 3, 4, 5]    | { n -> n * 2 } | [2, 4, 6, 8, 10]
        [123, 456, 0, 789] | { n -> n + 1 } | [124, 457, 1, 790]

    }

    def "select(IntSet indexSet) should construct a new vector by selecting the values for the given indices in the IntSet"() {
        def vector = IntVector.of(input as int[])
        def expected = IntVector.of(eInput as int[])
        def selectionSet = IntSet.of(selection as Integer[])
        expect:
        vector.select(selectionSet) == expected
        vector.select(selectionSet).hashCode() == expected.hashCode()

        where:
        input              | selection | eInput
        [1, 2, 3, 4, 5]    | [2, 4]    | [2, 4]
        [123, 456, 0, 789] | [4, 2, 1] | [123, 456, 789]
    }

    def "allMatch() should return true if all the elements of a vector are the same"() {
        def vector = IntVector.of(input as int[])

        expect:
        vector.allMatch(IntBiPredicate.EQUAL) == expected

        where:
        input           | expected
        [1, 2, 3, 4, 5] | false
        [123, 123, 123] | true
        [1, 1, 1, 1]    | true
        [0, 0, 0, 1]    | false
    }

    def "toStream()"() {
        def vector = IntVector.of(input as int[])

        expect:
        vector.toList() == expected

        where:

        input           | expected
        [1, 2, 3, 4, 5] | [1, 2, 3, 4, 5]
        [123, 456, 789] | [123, 456, 789]
        [1, 1, 1, 1]    | [1, 1, 1, 1]
    }

    def "int[] toArray()"() {
        def vector = IntVector.of(input as int[])

        expect:
        vector.toList() == expected

        where:

        input           | expected
        [1, 2, 3, 4, 5] | [1, 2, 3, 4, 5]
        [123, 456, 789] | [123, 456, 789]
        [1, 1, 1, 1]    | [1, 1, 1, 1]
    }

    def "iterator()"() {
        def vector = IntVector.of(input as int[])

        expect:
        def itr = vector.iterator()
        for (def i = 0; i < expected.size(); i++) {
            assert itr.hasNext()
            assert itr.next() == expected.get(i)
        }
        !itr.hasNext()

        where:

        input           | expected
        [1, 2, 3, 4, 5] | [1, 2, 3, 4, 5]
        [123, 456, 789] | [123, 456, 789]
        [1, 1, 1, 1]    | [1, 1, 1, 1]
    }

    def "toString()"() {
        def vector = IntVector.of(input as int[])

        expect:
        vector.toString() == expected

        where:

        input           | expected
        [1, 2, 3, 4, 5] | "[1,2,3,4,5]"
        [123, 456, 789] | "[123,456,789]"
        [1, 1, 1, 1]    | "[1,1,1,1]"

    }

    def "sort(IntVector vector))"() {
        def vector = IntVector.of(input as int[])
        def expected = IntVector.of(eInput as int[])
        expect:
        vector.sort() == expected

        where:

        input                      | eInput
        [1, 2, 3, 4, 5]            | [1, 2, 3, 4, 5]
        [1243, 456, 91]            | [91, 456, 1243]
        [9, 8, 7, 6, 1]            | [1, 6, 7, 8, 9]
        [22, 22, 45, 1, 22, 45, 1] | [1, 1, 22, 22, 22, 45, 45]
    }

    def "allMatch(IntBiPredicate predicate)"() {
        def vector = IntVector.of(input as int[])

        expect:
        vector.allMatch((s1, s2) -> s1 < s2) == expected

        where:

        input                            | expected
        []                               | true
        [1]                              | true
        [1, 2, 3, 4, 5]                  | true
        [1243, 456, 91]                  | false
        [1, 7, 7, 8, 9]                  | false
        [1, 45, 225, 322, 463, 465, 545] | true
    }

    def "allMatch(IntVector intVector1, IntVector intVector2, IntBiPredicate predicate)"() {
        def vector1 = IntVector.of(input1 as int[])
        def vector2 = IntVector.of(input2 as int[])
        expect:
        IntVector.allMatch(vector1, vector2, IntBiPredicate.SMALLER) == expected

        where:

        input1                     | input2                       | expected
        []                         | []                           | true
        [1]                        | [0]                          | false
        [1]                        | [1]                          | false
        [1]                        | [2]                          | true
        [1, 2, 3, 4, 5]            | [6, 6, 6, 6, 6]              | true
        [1, 6, 7, 8, 9]            | [2, 7, 8, 9, 10]             | true
        [1, 1, 22, 22, 22, 45, 45] | [23, 23, 23, 23, 23, 23, 23] | false
    }
}
