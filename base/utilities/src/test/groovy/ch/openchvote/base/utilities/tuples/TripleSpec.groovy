/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Shared
import spock.lang.Specification

class TripleSpec extends Specification {

    @Shared
    def default_First
    @Shared
    def default_Second
    @Shared
    def default_Third

    def setupSpec() {
        default_First = String.valueOf(1)
        default_Second = String.valueOf(2)
        default_Third = String.valueOf(3)
    }

    def "Constructor throws UtilityException for case: #caseDesc"() {
        when:
        new Triple(first, second, third)

        then:
        thrown(UtilityException)

        where:
        caseDesc         | first         | second         | third
        "first is null"  | null          | default_Second | default_Third
        "second is null" | default_First | null           | default_Third
        "third is null"  | default_First | default_Second | null
    }

    def "Test constructor and getter methods for the elements"() {
        given:
        def triple = new Triple(first, second, third)

        expect:
        triple.getFirst() == first
        triple.getSecond() == second
        triple.getThird() == third

        where:
        first                 | second                | third
        "first"               | "second"              | "third"
        1                     | 2                     | 3
        BigInteger.valueOf(1) | BigInteger.valueOf(2) | BigInteger.valueOf(3)
    }

    @SuppressWarnings('ChangeToOperator')
    def "equals should return true if all pairs of elements of two triples are the same and false otherwise"() {
        given:
        def triple1 = new Triple(elems1.get(0), elems1.get(1), elems1.get(2))
        def triple2 = new Triple(elems2.get(0), elems2.get(1), elems2.get(2))

        expect:
        //noinspection ChangeToOperator
        triple1.equals(triple2) == expected
        (triple1.hashCode() == triple2.hashCode()) == expected

        where:
        elems1                       | elems2                       | expected
        ["first", "second", "third"] | ["first", "second", "third"] | true
        ["wrong", "second", "third"] | ["first", "second", "third"] | false
        ["first", "wrong", "third"]  | ["first", "second", "third"] | false
        ["first", "second", "wrong"] | ["first", "second", "third"] | false
    }

    def "toString()"() {
        given:
        def triple = new Triple(elems1.get(0), elems1.get(1), elems1.get(2))

        expect:
        triple.toString() == expected

        where:
        elems1                       | expected
        ["first", "second", "third"] | "(first,second,third)"
        [1, 2, 3]                    | "(1,2,3)"
    }

    def "toStream() should return a Stream containing all elements in the triple in the correct order"() {
        given:
        def triple = new Triple(elems1.get(0), elems1.get(1), elems1.get(2))

        expect:
        triple.toStream().toList() == expected

        where:
        elems1                       | expected
        ["first", "second", "third"] | ["first", "second", "third"]
        [1, 2, 3]                    | [1, 2, 3]
    }

}
