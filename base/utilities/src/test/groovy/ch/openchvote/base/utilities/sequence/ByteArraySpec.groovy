/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Specification
import spock.lang.Unroll

class ByteArraySpec extends Specification {

    @SuppressWarnings('GroovyUnusedAssignment')
    def "Builder(): throws UtilityException for negative #x"() {
        when: "constructing a Builder with a negative #x"
        def builder = new ByteArray.Builder(x)

        then:
        thrown(UtilityException)

        where:
        x   | _
        -1  | _
        -25 | _
    }

    def "Builder(): constructs a ByteArray with #x bytes and all bytes are zero"() {
        given: "constructing a Builder with #x"
        def builder = new ByteArray.Builder(x)
        def expected = ByteArray.of(eBytes as byte[])

        expect: "a ByteArray with #x bytes and all bytes are zero"
        builder.build() == expected

        where:
        x | eBytes
        0 | []
        1 | [0x00]
        2 | [0x00, 0x00]
        3 | [0x00, 0x00, 0x00]
        4 | [0x00, 0x00, 0x00, 0x00]
        5 | [0x00, 0x00, 0x00, 0x00, 0x00]
    }

    @Unroll
    def "setByte(int index, int x): should throw an UtilityException for invalid indices and invalid "() {
        given: "constructing a Builder with #x"
        def builder = new ByteArray.Builder(length)

        when: "setByte is called for invalid index"
        builder.set(index, x)

        then: "UtilityException is thrown"
        thrown(UtilityException)

        where:
        length | index | x
        4      | -1    | 12
        4      | 5     | 12
    }

    @Unroll
    def "setByte(int index, int x): should change the byte as #index to #x"() {
        given: "constructing a Builder with #x"
        def builder = new ByteArray.Builder(length)
        def expected = ByteArray.of(eBytes as byte[])

        when: "setting byte at #index to #x"
        builder.set(index, x)

        then: "resulting Array should equal expected"
        builder.build() == expected

        where:
        length | index | x   | eBytes
        4      | 0     | 0   | [0x00, 0x00, 0x00, 0x00]
        4      | 1     | 12  | [0x00, 0x0C, 0x00, 0x00]
        4      | 2     | 36  | [0x00, 0x00, 0x24, 0x00]
        4      | 3     | 255 | [0x00, 0x00, 0x00, 0xFF]

    }

    @Unroll
    def "setByte(int index, byte b): should throw an UtilityException for invalid indices"() {
        given: "constructing a Builder with #b"
        def builder = new ByteArray.Builder(length)

        when: "setByte is called for invalid index"
        builder.set(index, b)

        then: "UtilityException is thrown"
        thrown(UtilityException)

        where:
        length | index | b
        4      | -1    | 0x00
        4      | 5     | 0x00
    }

    @Unroll
    def "setByte(int index, byte b): should change the byte as #index to #b"() {
        given: "constructing a Builder with #b"
        def builder = new ByteArray.Builder(length)
        def expected = ByteArray.of(eBytes as byte[])

        when: "setting byte at #index to #b"
        builder.set(index, b)

        then: "resulting Array should equal expected"
        builder.build() == expected

        where:
        length | index | b    | eBytes
        4      | 0     | 0x00 | [0x00, 0x00, 0x00, 0x00]
        4      | 1     | 0x0C | [0x00, 0x0C, 0x00, 0x00]
        4      | 2     | 0x24 | [0x00, 0x00, 0x24, 0x00]
        4      | 3     | 0xFF | [0x00, 0x00, 0x00, 0xFF]
    }

    @Unroll
    def "addByte(int x): should add byte with value #x at the end of the bytearray"() {
        given: "constructing a Builder with #x"
        def builder = new ByteArray.Builder(length)
        def expected = ByteArray.of(eBytes as byte[])

        when: "add byte #x"
        builder.add(x)

        then: "resulting Array should equal expected"
        builder.build() == expected

        where:
        length | x   | eBytes
        1      | 0   | [0x00]
        2      | 12  | [0x0C, 0x00]
        3      | 36  | [0x24, 0x00, 0x00]
        4      | 255 | [0xFF, 0x00, 0x00, 0x00]
    }

    @Unroll
    def "addByte(byte b): should add byte with value #b at the end of the bytearray"() {
        given: "constructing a Builder with #b"
        def builder = new ByteArray.Builder(length)
        def expected = ByteArray.of(eBytes as byte[])

        when: "add byte  #b"
        builder.add(b)

        then: "resulting Array should equal expected"
        builder.build() == expected

        where:
        length | b    | eBytes
        1      | 0x00 | [0x00]
        2      | 0x0C | [0x0C, 0x00]
        3      | 0x24 | [0x24, 0x00, 0x00]
        4      | 0xFF | [0xFF, 0x00, 0x00, 0x00]
    }

    def "of()  throws UtilityException for null input"() {
        when:
        ByteArray.of(null)

        then:
        thrown(UtilityException)
    }

    def "getLength(): the length should be equal to the size of the byte[] used to create it"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])

        expect: "length of the ByteArray should be #expected"
        B.getLength() == expected

        where:
        bytes                          | expected
        []                             | 0
        [0x00]                         | 1
        [0x00, 0x00]                   | 2
        [0x00, 0x00, 0x00]             | 3
        [0x00, 0x00, 0x00, 0x00]       | 4
        [0x00, 0x00, 0x00, 0x00, 0x00] | 5
        [0xFF]                         | 1
        [0xFF, 0xFF]                   | 2
        [0xFF, 0xFF, 0xFF]             | 3
        [0xFF, 0xFF, 0xFF, 0xFF]       | 4
        [0xFF, 0xFF, 0xFF, 0xFF, 0xFF] | 5
    }

    def "getByte(): should throw an UtilityException for invalid indices"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])

        when: "getByte is called for invalid index"
        B.getByte(index)

        then: "UtilityException is thrown"
        thrown(UtilityException)

        where:
        bytes                          | index
        [0x04, 0x03, 0x02, 0x01, 0x00] | -1
        [0x04, 0x03, 0x02, 0x01, 0x00] | 5
        [0x04, 0x03, 0x02, 0x01, 0x00] | 26
    }

    def "getByte(): should return #expected for #index"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])

        expect: "byte at #index is equal to #expected"
        B.getByte(index) == (byte) expected

        where:
        bytes                          | index | expected
        [0x04, 0x03, 0x02, 0x01, 0x00] | 0     | 0x04
        [0x04, 0x03, 0x02, 0x01, 0x00] | 1     | 0x03
        [0x04, 0x03, 0x02, 0x01, 0x00] | 2     | 0x02
        [0x04, 0x03, 0x02, 0x01, 0x00] | 3     | 0x01
        [0x04, 0x03, 0x02, 0x01, 0x00] | 4     | 0x00
    }

    def "getInteger(): should throw an UtilityException for invalid indices"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])

        when: "getInteger is called for invalid index"
        B.getValue(index)

        then: "UtilityException is thrown"
        thrown(UtilityException)

        where:
        bytes                          | index
        [0x04, 0x03, 0x02, 0x01, 0x00] | -1
        [0x04, 0x03, 0x02, 0x01, 0x00] | 5
        [0x04, 0x03, 0x02, 0x01, 0x00] | 26
    }

    def "getInteger(): should return #expected for index #index"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])

        expect: "integer value for byte at #index is equal to #expected"
        B.getValue(index) == expected

        where:
        bytes                          | index | expected
        [0x04, 0x03, 0x02, 0x01, 0x00] | 0     | 4
        [0x04, 0x03, 0x02, 0x01, 0x00] | 1     | 3
        [0x04, 0x03, 0x02, 0x01, 0x00] | 2     | 2
        [0x04, 0x03, 0x02, 0x01, 0x00] | 3     | 1
        [0x04, 0x03, 0x02, 0x01, 0x00] | 4     | 0
    }

    def "skip(): should throw an UtilityException for invalid k"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])

        when: "skip is called for invalid #k"
        B.skip(k)

        then: "UtilityException is thrown"
        thrown(UtilityException)

        where:
        bytes                          | k
        [0x04, 0x03, 0x02, 0x01, 0x00] | -1
        [0x04, 0x03, 0x02, 0x01, 0x00] | 6
        [0x04, 0x03, 0x02, 0x01, 0x00] | 26
    }

    @Unroll
    def "skip():should return the last n - k bytes of the input ByteArray"() {
        given: "a ByteArray from a byte[]"
        def b = ByteArray.of(bytes as byte[])
        def expected = ByteArray.of(eBytes as byte[])

        expect: "to return the last n - k bytes of the array"
        b.skip(k) == expected
        b.skip(k).hashCode() == expected.hashCode()
        where:
        bytes                          | k | eBytes
        [0x04, 0x03, 0x02, 0x01, 0x00] | 0 | [0x04, 0x03, 0x02, 0x01, 0x00]
        [0x04, 0x03, 0x02, 0x01, 0x00] | 1 | [0x03, 0x02, 0x01, 0x00]
        [0x04, 0x03, 0x02, 0x01, 0x00] | 2 | [0x02, 0x01, 0x00]
        [0x04, 0x03, 0x02, 0x01, 0x00] | 3 | [0x01, 0x00]
        [0x04, 0x03, 0x02, 0x01, 0x00] | 4 | [0x00]
        [0x04, 0x03, 0x02, 0x01, 0x00] | 5 | []
    }

    def "truncate(): should throw an UtilityException for invalid k"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])

        when: "truncate is called for invalid #k"
        B.truncate(k)

        then: "UtilityException is thrown"
        thrown(UtilityException)

        where:
        bytes                          | k
        [0x04, 0x03, 0x02, 0x01, 0x00] | -1
        [0x04, 0x03, 0x02, 0x01, 0x00] | 6
        [0x04, 0x03, 0x02, 0x01, 0x00] | 26
    }

    @Unroll
    def "truncate(): should return the first k bytes of the input ByteArray"() {
        given: "a ByteArray from a byte[]"
        def b = ByteArray.of(bytes as byte[])
        def expected = ByteArray.of(eBytes as byte[])

        expect: "to return the first k bytes of the array"
        b.truncate(k) == expected
        b.truncate(k).hashCode() == expected.hashCode()

        where:
        bytes                          | k | eBytes
        [0x04, 0x03, 0x02, 0x01, 0x00] | 0 | []
        [0x04, 0x03, 0x02, 0x01, 0x00] | 1 | [0x04]
        [0x04, 0x03, 0x02, 0x01, 0x00] | 2 | [0x04, 0x03]
        [0x04, 0x03, 0x02, 0x01, 0x00] | 3 | [0x04, 0x03, 0x02]
        [0x04, 0x03, 0x02, 0x01, 0x00] | 4 | [0x04, 0x03, 0x02, 0x01]
        [0x04, 0x03, 0x02, 0x01, 0x00] | 5 | [0x04, 0x03, 0x02, 0x01, 0x00]
    }

    @Unroll
    def "concatenate(): should combine two ByteArrays together"() {
        given: "two ByteArrays from a byte[]"
        def a = ByteArray.of(aBytes as byte[])
        def b = ByteArray.of(bBytes as byte[])
        def expected = ByteArray.of(eBytes as byte[])

        expect: "to return the a ByteArray containing all bytes from a followed by all bytes of b"
        a.concatenate(b) == expected
        a.concatenate(b).hashCode() == expected.hashCode()

        where:
        aBytes                               | bBytes                               | eBytes
        []                                   | []                                   | []
        [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F] | []                                   | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        []                                   | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F] | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [0x0A, 0x0B, 0x0C]                   | [0x0D, 0x0E, 0x0F]                   | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [0x04, 0x03, 0x02, 0x01]             | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F] | [0x04, 0x03, 0x02, 0x01, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [0x26, 0x04]                         | [0x02]                               | [0x26, 0x04, 0x02]
    }

    @Unroll
    def "concatenate(Vector<ByteArray> byteArrays): should combine a Vector of ByteArrays together"() {
        given: "a Vector of ByteArrays"
        def expected = ByteArray.of(eBytes as byte[])
        def vBuilder = new Vector.Builder<ByteArray>(vBytes.size())
        vBytes.each {
            def byteArray = ByteArray.of(it as byte[])
            vBuilder.add(byteArray)
        }
        def v = vBuilder.build()

        expect: "to return the one ByteArray containing all bytes of the first Array followed by all bytes of the second array and so on..."
        ByteArray.concatenate(v) == expected
        ByteArray.concatenate(v).hashCode() == expected.hashCode()

        where:
        vBytes                                                                 | eBytes
        [[], [], [], []]                                                       | []
        [[0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F], [], [], []]                     | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [[], [0x0A], [0x0B, 0x0C], [0x0D], [0x0E, 0x0F]]                       | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [[0x0A, 0x0B, 0x0C], [0x0D, 0x0E], [0x0F]]                             | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [[0x04, 0x03, 0x02, 0x01], [0x0A, 0x0B, 0x0C], [], [0x0D, 0x0E, 0x0F]] | [0x04, 0x03, 0x02, 0x01, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [[0x26, 0x04]]                                                         | [0x26, 0x04]
        []                                                                     | []
    }

    def "and(): should throw a UtilityException if the ByteArrays dont have the same length"() {
        given: "two ByteArrays from a byte[]"
        def a = ByteArray.of(aBytes as byte[])
        def b = ByteArray.of(bBytes as byte[])

        when:
        //noinspection ChangeToOperator
        a.and(b)

        then: "should throw a UtilityException"
        thrown(UtilityException)

        where:
        aBytes                               | bBytes
        [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F] | []
        []                                   | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [0x0A, 0x0B, 0x0C]                   | [0x0D, 0x0E]
        [0x04, 0x03, 0x02, 0x01]             | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [0x26, 0x04]                         | [0x02]
    }

    @Unroll
    def "and(): should return the bitwise and of two ByteArrays"() {
        given: "two ByteArrays from a byte[]"
        def a = ByteArray.of(aBytes as byte[])
        def b = ByteArray.of(bBytes as byte[])
        def expected = ByteArray.of(eBytes as byte[])

        expect: "to return the bitwise and of ByteArrays #a and #b"
        //noinspection ChangeToOperator
        a.and(b) == expected
        //noinspection ChangeToOperator
        a.and(b).hashCode() == expected.hashCode()

        where:
        aBytes                   | bBytes                   | eBytes
        []                       | []                       | []
        [0x3A]                   | [0x5B]                   | [0x1A]
        [0x4F, 0xAD]             | [0x53, 0x7D]             | [0x43, 0x2D]
        [0x9E, 0x62, 0x8C]       | [0x5B, 0xB7, 0x93]       | [0x1A, 0x22, 0x80]
        [0x00, 0x00, 0x00, 0x00] | [0x3A, 0x67, 0x9C, 0xFE] | [0x00, 0x00, 0x00, 0x00]

    }

    @Unroll
    def "and(Vector<ByteArray> byteArrays): should throw a UtilityException if the ByteArrays dont have the same length"() {
        given: "a Vector of ByteArrays"
        def vBuilder = new Vector.Builder<ByteArray>(vBytes.size())
        vBytes.each {
            def byteArray = ByteArray.of(it as byte[])
            vBuilder.add(byteArray)
        }
        def v = vBuilder.build()

        when:
        ByteArray.and(v, length)

        then: "should throw a UtilityException"
        thrown(UtilityException)

        where:

        vBytes                                                                 | length
        [[0x0A, 0x0B], [], [], []]                                             | 2
        [[], [], [0x0A, 0x0B, 0x0C], []]                                       | 2
        [[], [0x0A], [0x0B, 0x0C], [0x0D], [0x0E, 0x0F]]                       | 2
        [[0x0A, 0x0B, 0x0C], [0x0D, 0x0E], [0x0F]]                             | 2
        [[0x04, 0x03, 0x02, 0x01], [0x0A, 0x0B, 0x0C], [], [0x0D, 0x0E, 0x0F]] | 2

    }

    @Unroll

    def "and(Vector<ByteArray> byteArrays): should return the bitwise and of two ByteArrays"() {
        given: "a Vector of ByteArrays"
        def expected = ByteArray.of(eBytes as byte[])
        def vBuilder = new Vector.Builder<ByteArray>(vBytes.size())
        vBytes.each {
            def byteArray = ByteArray.of(it as byte[])
            vBuilder.add(byteArray)
        }
        def v = vBuilder.build()

        expect: "to return the one ByteArray containing the bitwise and combination all ByteArrays in the vector"
        ByteArray.and(v, length) == expected
        ByteArray.and(v, length).hashCode() == expected.hashCode()

        where:
        vBytes                                                       | eBytes             | length
        [[], [], [], []]                                             | []                 | 0
        [[0x3A], [0x5B]]                                             | [0x1A]             | 1
        [[0x4F, 0xAD], [0x53, 0x7D], [0x0E, 0x0F]]                   | [0x02, 0x0D]       | 2
        [[0x9E, 0x62, 0x8C], [0x5B, 0xB7, 0x93], [0x95, 0x6D, 0xB1]] | [0x10, 0x20, 0x80] | 3
        [[0x26, 0x04]]                                               | [0x26, 0x04]       | 2
        []                                                           | []                 | 0

    }

    def "or(): should throw a UtilityException if the ByteArrays dont have the same length"() {
        given: "two ByteArrays from a byte[]"
        def a = ByteArray.of(aBytes as byte[])
        def b = ByteArray.of(bBytes as byte[])

        when:
        //noinspection ChangeToOperator
        a.or(b)

        then: "should throw a UtilityException"
        thrown(UtilityException)

        where:
        aBytes                               | bBytes
        [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F] | []
        []                                   | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [0x0A, 0x0B, 0x0C]                   | [0x0D, 0x0E]
        [0x04, 0x03, 0x02, 0x01]             | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [0x26, 0x04]                         | [0x02]
    }

    @Unroll
    def "or(): should return the bitwise OR of two ByteArrays"() {
        given: "two ByteArrays from a byte[]"
        def a = ByteArray.of(aBytes as byte[])
        def b = ByteArray.of(bBytes as byte[])
        def expected = ByteArray.of(eBytes as byte[])

        expect: "to return the bitwise and of ByteArrays #a and #b"
        //noinspection ChangeToOperator
        a.or(b) == expected
        //noinspection ChangeToOperator
        a.or(b).hashCode() == expected.hashCode()

        where:
        aBytes                   | bBytes                   | eBytes
        []                       | []                       | []
        [0x3A]                   | [0x5B]                   | [0x7B]
        [0x4F, 0xAD]             | [0x53, 0x7D]             | [0x5F, 0xFD]
        [0x9E, 0x62, 0x8C]       | [0x5B, 0xB7, 0x93]       | [0xDF, 0xF7, 0x9F]
        [0x00, 0x00, 0x00, 0x00] | [0x3A, 0x67, 0x9C, 0xFE] | [0x3A, 0x67, 0x9C, 0xFE]

    }

    @Unroll
    def "or(Vector<ByteArray> byteArrays): should throw a UtilityException if the ByteArrays dont have the same length"() {
        given: "a Vector of ByteArrays"
        def vBuilder = new Vector.Builder<ByteArray>(vBytes.size())
        vBytes.each {
            def byteArray = ByteArray.of(it as byte[])
            vBuilder.add(byteArray)
        }
        def v = vBuilder.build()

        when:
        ByteArray.or(v, length)

        then: "should throw a UtilityException"
        thrown(UtilityException)

        where:

        vBytes                                                                 | length
        [[0x0A, 0x0B], [], [], []]                                             | 2
        [[], [], [0x0A, 0x0B, 0x0C], []]                                       | 2
        [[], [0x0A], [0x0B, 0x0C], [0x0D], [0x0E, 0x0F]]                       | 2
        [[0x0A, 0x0B, 0x0C], [0x0D, 0x0E], [0x0F]]                             | 2
        [[0x04, 0x03, 0x02, 0x01], [0x0A, 0x0B, 0x0C], [], [0x0D, 0x0E, 0x0F]] | 2

    }

    @Unroll

    def "or(Vector<ByteArray> byteArrays): should return the bitwise OR of two ByteArrays"() {
        given: "a Vector of ByteArrays"
        def expected = ByteArray.of(eBytes as byte[])
        def vBuilder = new Vector.Builder<ByteArray>(vBytes.size())
        vBytes.each {
            def byteArray = ByteArray.of(it as byte[])
            vBuilder.add(byteArray)
        }
        def v = vBuilder.build()

        expect: "to return the one ByteArray containing the bitwise and combination all ByteArrays in the vector"
        ByteArray.or(v, length) == expected
        ByteArray.or(v, length).hashCode() == expected.hashCode()

        where:
        vBytes                                                       | eBytes             | length
        [[], [], [], []]                                             | []                 | 0
        [[0x3A], [0x5B]]                                             | [0x7B]             | 1
        [[0x4F, 0xAD], [0x53, 0x7D], [0x0E, 0x0F]]                   | [0x5F, 0xFF]       | 2
        [[0x9E, 0x62, 0x8C], [0x5B, 0xB7, 0x93], [0x95, 0x6D, 0xB1]] | [0xDF, 0xFF, 0xBF] | 3
        [[0x26, 0x04]]                                               | [0x26, 0x04]       | 2
        []                                                           | []                 | 0
    }

    def "xor(): should throw a UtilityException if the ByteArrays dont have the same length"() {
        given: "two ByteArrays from a byte[]"
        def a = ByteArray.of(aBytes as byte[])
        def b = ByteArray.of(bBytes as byte[])

        when:
        //noinspection ChangeToOperator
        a.xor(b)

        then: "should throw a UtilityException"
        thrown(UtilityException)

        where:
        aBytes                               | bBytes
        [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F] | []
        []                                   | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [0x0A, 0x0B, 0x0C]                   | [0x0D, 0x0E]
        [0x04, 0x03, 0x02, 0x01]             | [0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]
        [0x26, 0x04]                         | [0x02]
    }

    @Unroll
    def "xor(): should return the bitwise XOR of two ByteArrays"() {
        given: "two ByteArrays from a byte[]"
        def a = ByteArray.of(aBytes as byte[])
        def b = ByteArray.of(bBytes as byte[])
        def expected = ByteArray.of(eBytes as byte[])

        expect: "to return the bitwise and of ByteArrays #a and #b"
        //noinspection ChangeToOperator
        a.xor(b) == expected
        //noinspection ChangeToOperator
        a.xor(b).hashCode() == expected.hashCode()

        where:
        aBytes                   | bBytes                   | eBytes
        []                       | []                       | []
        [0x3A]                   | [0x5B]                   | [0x61]
        [0x4F, 0xAD]             | [0x53, 0x7D]             | [0x1C, 0xD0]
        [0x9E, 0x62, 0x8C]       | [0x5B, 0xB7, 0x93]       | [0xC5, 0xD5, 0x1F]
        [0x00, 0x00, 0x00, 0x00] | [0x3A, 0x67, 0x9C, 0xFE] | [0x3A, 0x67, 0x9C, 0xFE]

    }

    @Unroll
    def "xor(Vector<ByteArray> byteArrays): should throw a UtilityException if the ByteArrays dont have the same length"() {
        given: "a Vector of ByteArrays"
        def vBuilder = new Vector.Builder<ByteArray>(vBytes.size())
        vBytes.each {
            def byteArray = ByteArray.of(it as byte[])
            vBuilder.add(byteArray)
        }
        def v = vBuilder.build()

        when:
        ByteArray.xor(v, length)

        then: "should throw a UtilityException"
        thrown(UtilityException)

        where:

        vBytes                                                                 | length
        [[0x0A, 0x0B], [], [], []]                                             | 2
        [[], [], [0x0A, 0x0B, 0x0C], []]                                       | 2
        [[], [0x0A], [0x0B, 0x0C], [0x0D], [0x0E, 0x0F]]                       | 2
        [[0x0A, 0x0B, 0x0C], [0x0D, 0x0E], [0x0F]]                             | 2
        [[0x04, 0x03, 0x02, 0x01], [0x0A, 0x0B, 0x0C], [], [0x0D, 0x0E, 0x0F]] | 2
    }

    @Unroll

    def "xor(Vector<ByteArray> byteArrays): should return the bitwise XOR of two ByteArrays"() {
        given: "a Vector of ByteArrays"
        def expected = ByteArray.of(eBytes as byte[])
        def vBuilder = new Vector.Builder<ByteArray>(vBytes.size())
        vBytes.each {
            def byteArray = ByteArray.of(it as byte[])
            vBuilder.add(byteArray)
        }
        def v = vBuilder.build()

        expect: "to return the one ByteArray containing the bitwise and combination all ByteArrays in the vector"
        ByteArray.xor(v, length) == expected
        ByteArray.xor(v, length).hashCode() == expected.hashCode()

        where:
        vBytes                                                       | eBytes             | length
        [[], [], [], []]                                             | []                 | 0
        [[0x3A], [0x5B]]                                             | [0x61]             | 1
        [[0x4F, 0xAD], [0x53, 0x7D], [0x0E, 0x0F]]                   | [0x12, 0xDF]       | 2
        [[0x9E, 0x62, 0x8C], [0x5B, 0xB7, 0x93], [0x95, 0x6D, 0xB1]] | [0x50, 0xB8, 0xAE] | 3
        [[0x26, 0x04]]                                               | [0x26, 0x04]       | 2
        []                                                           | []                 | 0

    }

    @Unroll
    def "setByte(int byteIndex, int x): should throw an UtilityException for invalid indices or invalid values"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])

        when: "setByte is called for invalid index"
        B.setByte(index, x)

        then: "UtilityException is thrown"
        thrown(UtilityException)

        where:
        bytes                          | x | index
        [0x04, 0x03, 0x02, 0x01, 0x00] | 0 | -1
        [0x04, 0x03, 0x02, 0x01, 0x00] | 0 | 5
        [0x04, 0x03, 0x02, 0x01, 0x00] | 0 | 26

    }

    def "setByte(int byteIndex, int x): should replace the byte at #index in the #bytes with #x\""() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])
        def expected = ByteArray.of(ebytes as byte[])

        expect: "the result to be starting Array with the byte at #index changed to #x"
        B.setByte(index, x) == expected
        B.setByte(index, x).hashCode() == expected.hashCode()

        where:
        bytes                          | x   | index | ebytes
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0   | 0     | [0x00, 0x03, 0x02, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0   | 4     | [0x04, 0x03, 0x02, 0x01, 0x00]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0   | 2     | [0x04, 0x03, 0x00, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 255 | 0     | [0xFF, 0x03, 0x02, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 255 | 4     | [0x04, 0x03, 0x02, 0x01, 0xFF]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 255 | 2     | [0x04, 0x03, 0xFF, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 26  | 0     | [0x1A, 0x03, 0x02, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 26  | 4     | [0x04, 0x03, 0x02, 0x01, 0x1A]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 26  | 2     | [0x04, 0x03, 0x1A, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 4   | 0     | [0x04, 0x03, 0x02, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 6   | 4     | [0x04, 0x03, 0x02, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 2   | 2     | [0x04, 0x03, 0x02, 0x01, 0x06]
    }

    def "setByte(int byteIndex, byte b): should throw an UtilityException for invalid indices"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])

        when: "setByte is called for invalid index"
        B.setByte(index, x)

        then: "UtilityException is thrown"
        thrown(UtilityException)

        where:
        bytes                          | x    | index
        [0x04, 0x03, 0x02, 0x01, 0x00] | 0x00 | -1
        [0x04, 0x03, 0x02, 0x01, 0x00] | 0x00 | 5
        [0x04, 0x03, 0x02, 0x01, 0x00] | 0x00 | 26

    }

    def "setByte(int byteIndex, byte b): should replace the byte at #index in the #bytes with #x"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])
        def expected = ByteArray.of(ebytes as byte[])

        expect: "the result to be starting Array with the byte at #index changed to #x"
        B.setByte(index, x) == expected
        B.setByte(index, x).hashCode() == expected.hashCode()

        where:
        bytes                          | x    | index | ebytes
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0x00 | 0     | [0x00, 0x03, 0x02, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0x00 | 4     | [0x04, 0x03, 0x02, 0x01, 0x00]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0x00 | 2     | [0x04, 0x03, 0x00, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0xFF | 0     | [0xFF, 0x03, 0x02, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0xFF | 4     | [0x04, 0x03, 0x02, 0x01, 0xFF]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0xFF | 2     | [0x04, 0x03, 0xFF, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0x1A | 0     | [0x1A, 0x03, 0x02, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0x1A | 4     | [0x04, 0x03, 0x02, 0x01, 0x1A]
        [0x04, 0x03, 0x02, 0x01, 0x06] | 0x1A | 2     | [0x04, 0x03, 0x1A, 0x01, 0x06]
    }

    def "toByteArray(): should return the ByteArray as a Java byte[]"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])
        expect: "the result to be starting Array with the byte at #index changed to #x"
        B.toByteArray() == (byte[]) expected

        where:
        bytes                          | expected
        []                             | []
        [0x01]                         | [0x01]
        [0x01, 0x06]                   | [0x01, 0x06]
        [0x02, 0x01, 0x06]             | [0x02, 0x01, 0x06]
        [0x03, 0x02, 0x01, 0x06]       | [0x03, 0x02, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | [0x04, 0x03, 0x02, 0x01, 0x06]

    }

    @Unroll
    def "toStream(): should return an Stream of integers based on the bytes in the ByteArray"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])

        when:
        def BStream = B.toIntStream()

        then:
        BStream.boxed().toList() == expected

        where:
        bytes                          | expected
        []                             | []
        [0x01]                         | [1]
        [0x01, 0x06]                   | [1, 6]
        [0xFF, 0x01, 0x06]             | [255, 1, 6]
        [0x00, 0xFF, 0x01, 0x06]       | [0, 255, 1, 6]
        [0x4A, 0x00, 0xFF, 0x01, 0x06] | [74, 0, 255, 1, 6]
    }

    def "iterator() should go through the bytes in the correct order"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])
        expect: "the iterator should go over the array and return the bytes in the order given at construction"
        def iter = B.iterator()
        for (int i = 0; i < expected.size(); i++) {
            assert iter.hasNext()
            assert (byte) iter.next() == expected.get(i)
        }
        !iter.hasNext()

        where:
        bytes                          | expected
        []                             | []
        [0x01]                         | [0x01]
        [0x01, 0x06]                   | [0x01, 0x06]
        [0x02, 0x01, 0x06]             | [0x02, 0x01, 0x06]
        [0x03, 0x02, 0x01, 0x06]       | [0x03, 0x02, 0x01, 0x06]
        [0x04, 0x03, 0x02, 0x01, 0x06] | [0x04, 0x03, 0x02, 0x01, 0x06]

    }

    def "toString()"() {
        given: "a ByteArray from a byte[]"
        def B = ByteArray.of(bytes as byte[])

        expect: "toString method should return a Sting representation of the array in the expected format"
        B.toString() == expected

        where:
        bytes                          | expected
        []                             | ""
        [0x01]                         | "01"
        [0x12, 0x34]                   | "1234"
        [0x56, 0x78, 0x9A]             | "56789A"
        [0xBC, 0xDE, 0xFF, 0xED]       | "BCDEFFED"
        [0xCB, 0xA9, 0x87, 0x65, 0x43] | "CBA9876543"

    }

}
