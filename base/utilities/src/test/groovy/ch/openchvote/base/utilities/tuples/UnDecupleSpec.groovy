/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples


import ch.openchvote.base.utilities.UtilityException
import ch.openchvote.base.utilities.tuples.decuple.UnDecuple
import spock.lang.Shared
import spock.lang.Specification

class UnDecupleSpec extends Specification {

    @Shared
    def default_First
    @Shared
    def default_Second
    @Shared
    def default_Third
    @Shared
    def default_Fourth
    @Shared
    def default_Fifth
    @Shared
    def default_Sixth
    @Shared
    def default_Seventh
    @Shared
    def default_Eighth
    @Shared
    def default_Ninth
    @Shared
    def default_Tenth
    @Shared
    def default_Eleventh

    def setupSpec() {
        default_First = String.valueOf(1)
        default_Second = String.valueOf(2)
        default_Third = String.valueOf(3)
        default_Fourth = String.valueOf(4)
        default_Fifth = String.valueOf(5)
        default_Sixth = String.valueOf(6)
        default_Seventh = String.valueOf(7)
        default_Eighth = String.valueOf(8)
        default_Ninth = String.valueOf(9)
        default_Tenth = String.valueOf(10)
        default_Eleventh = String.valueOf(11)
    }

    def "Constructor throws UtilityException for case: #caseDesc"() {
        when:
        new UnDecuple(first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth, eleventh)

        then:
        thrown(UtilityException)

        where:
        caseDesc           | first         | second         | third         | fourth         | fifth         | sixth         | seventh         | eighth         | ninth         | tenth         | eleventh
        "first is null"    | null          | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth | default_Tenth | default_Eleventh
        "second is null"   | default_First | null           | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth | default_Tenth | default_Eleventh
        "third is null"    | default_First | default_Second | null          | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth | default_Tenth | default_Eleventh
        "fourth is null"   | default_First | default_Second | default_Third | null           | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth | default_Tenth | default_Eleventh
        "fifth is null"    | default_First | default_Second | default_Third | default_Fourth | null          | default_Sixth | default_Seventh | default_Eighth | default_Ninth | default_Tenth | default_Eleventh
        "sixth is null"    | default_First | default_Second | default_Third | default_Fourth | default_Fifth | null          | default_Seventh | default_Eighth | default_Ninth | default_Tenth | default_Eleventh
        "seventh is null"  | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | null            | default_Eighth | default_Ninth | default_Tenth | default_Eleventh
        "eighth is null"   | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | null           | default_Ninth | default_Tenth | default_Eleventh
        "ninth is null"    | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | null          | default_Tenth | default_Eleventh
        "tenth is null"    | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth | null          | default_Eleventh
        "eleventh is null" | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth | default_Tenth | null
    }

    def "Test constructor and getter methods for the elements"() {
        given:
        def undecuple = new UnDecuple(first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth, eleventh)

        expect:
        undecuple.getFirst() == first
        undecuple.getSecond() == second
        undecuple.getThird() == third
        undecuple.getFourth() == fourth
        undecuple.getFifth() == fifth
        undecuple.getSixth() == sixth
        undecuple.getSeventh() == seventh
        undecuple.getEighth() == eighth
        undecuple.getNinth() == ninth
        undecuple.getTenth() == tenth
        undecuple.getEleventh() == eleventh

        where:
        first                 | second                | third                 | fourth                | fifth                 | sixth                 | seventh               | eighth                | ninth                 | tenth                  | eleventh
        "first"               | "second"              | "third"               | "fourth"              | "fifth"               | "sixth"               | "seventh"             | "eighth"              | "ninth"               | "tenth"                | "eleventh"
        1                     | 2                     | 3                     | 4                     | 5                     | 6                     | 7                     | 8                     | 9                     | 10                     | 11
        BigInteger.valueOf(1) | BigInteger.valueOf(2) | BigInteger.valueOf(3) | BigInteger.valueOf(4) | BigInteger.valueOf(5) | BigInteger.valueOf(6) | BigInteger.valueOf(7) | BigInteger.valueOf(8) | BigInteger.valueOf(9) | BigInteger.valueOf(10) | BigInteger.valueOf(11)
    }

    @SuppressWarnings('ChangeToOperator')
    def "equals should return true if all pairs of elements of two undecuples are the same and false otherwise"() {
        given:
        def undecuple1 = new UnDecuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8), elems1.get(9), elems1.get(10))
        def undecuple2 = new UnDecuple(elems2.get(0), elems2.get(1), elems2.get(2), elems2.get(3), elems2.get(4), elems2.get(5), elems2.get(6), elems2.get(7), elems2.get(8), elems2.get(9), elems2.get(10))

        expect:
        //noinspection ChangeToOperator
        undecuple1.equals(undecuple2) == expected
        (undecuple1.hashCode() == undecuple2.hashCode()) == expected

        where:
        elems1                                                                                                      | elems2                                                                                                      | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | true
        ["wrong", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | false
        ["first", "wrong", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | false
        ["first", "second", "wrong", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | false
        ["first", "second", "third", "wrong", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | false
        ["first", "second", "third", "fourth", "wrong", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | false
        ["first", "second", "third", "fourth", "fifth", "wrong", "seventh", "eighth", "ninth", "tenth", "eleventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "wrong", "eighth", "ninth", "tenth", "eleventh"]   | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "wrong", "ninth", "tenth", "eleventh"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "wrong", "tenth", "eleventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "wrong", "eleventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "wrong"]    | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | false
    }

    def "toString()"() {
        given:
        def undecuple = new UnDecuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8), elems1.get(9), elems1.get(10))

        expect:
        undecuple.toString() == expected

        where:
        elems1                                                                                                      | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | "(first,second,third,fourth,fifth,sixth,seventh,eighth,ninth,tenth,eleventh)"
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]                                                                         | "(1,2,3,4,5,6,7,8,9,10,11)"
    }

    def "toStream() should return a Stream containing all elements in the undecuple in the correct order"() {
        given:
        def undecuple = new UnDecuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8), elems1.get(9), elems1.get(10))

        expect:
        undecuple.toStream().toList() == expected

        where:
        elems1                                                                                                      | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh"]
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]                                                                         | [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    }
}
