/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.crypto

import ch.openchvote.base.utilities.UtilityException
import ch.openchvote.base.utilities.sequence.ByteArray
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import java.nio.charset.StandardCharsets

class AESSpec extends Specification {

    @Shared
    def default_secretKey
    @Shared
    def default_iv
    @Shared
    def default_message
    @Shared
    def default_cipher

    def stringToByteArray(String S) {
        return ByteArray.of(S.getBytes(StandardCharsets.UTF_8))
    }

    @SuppressWarnings('ChangeToOperator')
    def bigIntegerToByteArray(BigInteger x) {
        int n = (int) Math.ceil(x.bitLength() / 8.0)
        byte[] array = new byte[n]
        for (int i = 1; i <= n; i++) {
            //noinspection ChangeToOperator
            array[n - i] = x.mod(BigInteger.valueOf(256)).byteValue()
            x = x.divide(BigInteger.valueOf(256))
        }
        return ByteArray.of(array)
    }

    def setupSpec() {
        default_secretKey = bigIntegerToByteArray(BigInteger.valueOf(255))
        default_iv = bigIntegerToByteArray(BigInteger.valueOf(127))
        default_message = stringToByteArray("hello")
        default_cipher = stringToByteArray("encrypted")
    }

    @Unroll
    def "encrypt() throws UtilityException in case: #caseDesc"() {
        when: "running encrypt under the condition that #caseDesc"
        BlockCipher.AES.encrypt(secretKey, iv, message as ByteArray)

        then: "a UtilityException is thrown"
        thrown(UtilityException)

        where:
        caseDesc              | secretKey                                       | iv                                             | message
        "secretKey too small" | bigIntegerToByteArray(BigInteger.valueOf(142))  | default_iv                                     | default_message
        "secretKey too large" | bigIntegerToByteArray(BigInteger.valueOf(1024)) | default_iv                                     | default_message
        "iv too small"        | default_secretKey                               | bigIntegerToByteArray(BigInteger.valueOf(26))  | default_message
        "iv too large"        | default_secretKey                               | bigIntegerToByteArray(BigInteger.valueOf(260)) | default_message
    }

    @Unroll
    def "compare encrypt() to python3 pycryptodome function"() {
        given: " a  secret key, an iv and a message"
        def secretKey = bigIntegerToByteArray(new BigInteger(secretKeyHexString, 16))
        def iv = bigIntegerToByteArray(new BigInteger(ivHexString, 16))
        def message = stringToByteArray(messageString)
        def expected = bigIntegerToByteArray(new BigInteger(expectedHexString, 16))
        def aes = BlockCipher.AES

        expect: "encrypt(key,iv,message) to return the same ciphertext as openssl aes-256-cbc"
        aes.encrypt(secretKey, iv, message) == expected

        where:
        secretKeyHexString                 | ivHexString                | messageString      | expectedHexString
        "4e61b39525cee2dca4055e5ab5c2064a" | "cbe1f356e4e70acdcbe1f356" | "hello"            | "cd4f303f36e592d13982f4c34e190f995fd34c35c8"
        "4e61b39525cee2dca4055e5ab5c2064a" | "cbe1f356e4e70acdcbe1f357" | "this is a secret" | "5411eb93aa031908cb44c9edbdcc1e37be4ed5bb607cee6f2dbec092bd600628"
        "17cd60d23221c440c78080589f5f0c54" | "dcf20467f5f81bdedcf20467" | "hello"            | "70f8cff5ec65dbc11a09a2498e5108e24ff50338f6"
        "17cd60d23221c440c78080589f5f0c54" | "dcf20467f5f81bdedcf20463" | "this is a secret" | "33a0969620110672cf056d8ae7943a8fbe67fec3a464d3d4428caa1ec80de69e"
    }

    @Unroll
    def "decrypt() throws UtilityException in case: #caseDesc"() {
        when: "running decrypt under the condition that #caseDesc"
        BlockCipher.AES.decrypt(secretKey, iv, cipher as ByteArray)

        then: "a UtilityException is thrown"
        thrown(UtilityException)

        where:
        caseDesc              | secretKey                                       | iv                                             | cipher
        "secretKey too small" | bigIntegerToByteArray(BigInteger.valueOf(142))  | default_iv                                     | default_cipher
        "secretKey too large" | bigIntegerToByteArray(BigInteger.valueOf(1024)) | default_iv                                     | default_cipher
        "iv too small"        | default_secretKey                               | bigIntegerToByteArray(BigInteger.valueOf(26))  | default_cipher
        "iv too large"        | default_secretKey                               | bigIntegerToByteArray(BigInteger.valueOf(260)) | default_cipher
    }

    @Unroll
    def "test if decrypt() reverses python3 pycryptodome function"() {
        given: "given secret key, iv and ciphertext produced by aes-256-cbc"
        def secretKey = bigIntegerToByteArray(new BigInteger(secretKeyHexString, 16))
        def iv = bigIntegerToByteArray(new BigInteger(ivHexString, 16))
        def cipher = bigIntegerToByteArray(new BigInteger(cipherHexString, 16))
        def expected = stringToByteArray(eString)

        expect: "decrypt to return the same message"
        ByteArray result = BlockCipher.AES.decrypt(secretKey, iv, cipher)
        result == expected

        where:
        secretKeyHexString                 | ivHexString                | cipherHexString                                                    | eString
        "4e61b39525cee2dca4055e5ab5c2064a" | "cbe1f356e4e70acdcbe1f356" | "cd4f303f36e592d13982f4c34e190f995fd34c35c8"                       | "hello"
        "4e61b39525cee2dca4055e5ab5c2064a" | "cbe1f356e4e70acdcbe1f357" | "5411eb93aa031908cb44c9edbdcc1e37be4ed5bb607cee6f2dbec092bd600628" | "this is a secret"
        "17cd60d23221c440c78080589f5f0c54" | "dcf20467f5f81bdedcf20467" | "70f8cff5ec65dbc11a09a2498e5108e24ff50338f6"                       | "hello"
        "17cd60d23221c440c78080589f5f0c54" | "dcf20467f5f81bdedcf20463" | "33a0969620110672cf056d8ae7943a8fbe67fec3a464d3d4428caa1ec80de69e" | "this is a secret"
    }

    @Unroll
    def "decrypt() should reverse encrypt()"() {
        given: " a  secret key, an iv and a message"
        def secretKey = bigIntegerToByteArray(new BigInteger(secretKeyHexString, 16))
        def iv = bigIntegerToByteArray(new BigInteger(ivHexString, 16))
        def message = stringToByteArray(messageString)

        expect: " decrypt(key, iv,  encrypt(key,iv,message)) to return the original message"
        BlockCipher.AES.decrypt(secretKey, iv, BlockCipher.AES.encrypt(secretKey, iv, message)) == message

        where:
        secretKeyHexString                 | ivHexString                | messageString
        "4e61b39525cee2dca4055e5ab5c2064a" | "cbe1f356e4e70acdcbe1f356" | "hello"
        "4e61b39525cee2dca4055e5ab5c2064a" | "cbe1f356e4e70acdcbe1f357" | "this is a secret"
        "17cd60d23221c440c78080589f5f0c54" | "dcf20467f5f81bdedcf20467" | "hello"
        "17cd60d23221c440c78080589f5f0c54" | "dcf20467f5f81bdedcf20468" | "this is a secret"
    }

}
