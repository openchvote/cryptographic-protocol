/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.set

import ch.openchvote.base.utilities.UtilityException
import ch.openchvote.base.utilities.sequence.*
import ch.openchvote.base.utilities.tuples.Pair
import ch.openchvote.base.utilities.tuples.Quadruple
import ch.openchvote.base.utilities.tuples.Quintuple
import ch.openchvote.base.utilities.tuples.Triple
import spock.lang.Specification
import spock.lang.Unroll

import java.nio.charset.StandardCharsets

class SetSpec extends Specification {

    def stringToUTF8ByteArray(String S) {
        return ByteArray.of(S.getBytes(StandardCharsets.UTF_8))
    }

    def stringToUTF16ByteArray(String S) {
        return ByteArray.of(S.getBytes(StandardCharsets.UTF_16))
    }

    @Unroll
    def "contains(T x) #x"() {
        expect:
        set.contains(x) == expected

        where:
        set                                                                       | x                                                                           | expected
        Set.NN                                                                    | BigInteger.valueOf(0)                                                       | true
        Set.NN                                                                    | BigInteger.valueOf(Integer.MAX_VALUE)                                       | true
        Set.NN                                                                    | BigInteger.valueOf(-1)                                                      | false
        Set.NN                                                                    | BigInteger.valueOf(-98264)                                                  | false
        Set.NN_plus                                                               | BigInteger.ONE                                                              | true
        Set.NN_plus                                                               | BigInteger.valueOf(982374)                                                  | true
        Set.NN_plus                                                               | BigInteger.valueOf(0)                                                       | false
        Set.NN_plus                                                               | BigInteger.valueOf(-928374)                                                 | false
        Set.B(5)                                                                  | ByteArray.of([0x01, 0x02, 0x03, 0x04, 0x05] as byte[])                      | true
        Set.B(5)                                                                  | ByteArray.of([0xFF, 0xFF, 0xFF, 0xFF, 0xFF] as byte[])                      | true
        Set.B(5)                                                                  | ByteArray.of([0x01, 0x02, 0x03, 0x04] as byte[])                            | false
        Set.B(5)                                                                  | ByteArray.of([0x01, 0x02, 0x03, 0x04, 0x05, 0x06] as byte[])                | false
        Set.B_star                                                                | ByteArray.of([0x01, 0x02, 0x03, 0x04, 0x05] as byte[])                      | true
        Set.B_star                                                                | ByteArray.of([0xFF, 0xFF, 0xFF, 0xFF, 0xFF] as byte[])                      | true
        Set.B_star                                                                | ByteArray.of([0x01, 0x02, 0x03, 0x04] as byte[])                            | true
        Set.B_star                                                                | null                                                                        | false
        Set.UCS(5)                                                                | "12345"                                                                     | true
        Set.UCS(5)                                                                | "zzzzz"                                                                     | true
        Set.UCS(5)                                                                | "1234"                                                                      | false
        Set.UCS(5)                                                                | "123456"                                                                    | false
        Set.UCS_star                                                              | "12345"                                                                     | true
        Set.UCS_star                                                              | "zzzzz"                                                                     | true
        Set.UCS_star                                                              | null                                                                        | false
        Set.String(Alphabet.LATIN_52)                                             | "abcde"                                                                     | true
        Set.String(Alphabet.LATIN_52)                                             | "vwxyz"                                                                     | true
        Set.String(Alphabet.LATIN_52)                                             | "abc34"                                                                     | false
        Set.String(Alphabet.LATIN_52)                                             | "abcd5"                                                                     | false
        Set.String(Alphabet.LATIN_52, 5)                                          | "abcde"                                                                     | true
        Set.String(Alphabet.LATIN_52, 5)                                          | "vwxyz"                                                                     | true
        Set.String(Alphabet.LATIN_52, 5)                                          | "abc"                                                                       | false
        Set.String(Alphabet.LATIN_52, 5)                                          | "abcd5"                                                                     | false
        Set.String(Alphabet.LATIN_52, 3, 5)                                       | "abcde"                                                                     | true
        Set.String(Alphabet.LATIN_52, 3, 5)                                       | "vwxyz"                                                                     | true
        Set.String(Alphabet.LATIN_52, 3, 5)                                       | "ab"                                                                        | false
        Set.String(Alphabet.LATIN_52, 3, 5)                                       | "abcd5"                                                                     | false
        Set.Vector(Set.UCS(5))                                                    | (new Vector.Builder<String>(3)).fill("Lorem").build()                       | true
        Set.Vector(Set.UCS(5))                                                    | (new Vector.Builder<String>(1)).fill("Lorem Ipsum").build()                 | false
        Set.Vector(Set.UCS(5), 5)                                                 | (new Vector.Builder<String>(5)).fill("Lorem").build()                       | true
        Set.Vector(Set.UCS(5), 5)                                                 | (new Vector.Builder<String>(5)).fill("Lorem Ipsum").build()                 | false
        Set.Matrix(Set.UCS(5), 5, 3)                                              | (new Matrix.Builder<String>(5, 3)).fill("Lorem").build()                    | true
        Set.Matrix(Set.UCS(5), 5, 3)                                              | (new Matrix.Builder<String>(5, 3)).fill("Lorem Ipsum").build()              | false
        Set.IntVector(IntSet.NN, 5)                                               | (new IntVector.Builder(5)).fill(647).build()                                | true
        Set.IntVector(IntSet.NN, 5)                                               | (new IntVector.Builder(3)).fill(4).build()                                  | false
        Set.IntMatrix(IntSet.NN, 5, 3)                                            | (new IntMatrix.Builder(5, 3)).fill(34).build()                              | true
        Set.IntMatrix(IntSet.NN, 5, 3)                                            | (new IntMatrix.Builder(3, 5)).fill(0).build()                               | false
        Set.Pair(Set.UCS(5), Set.UCS(5))                                          | new Pair<>("Lorem", "Ipsum")                                                | true
        Set.Pair(Set.UCS(5), Set.UCS(5))                                          | new Pair<>("Hello", "World!")                                               | false
        Set.Triple(Set.UCS(5), Set.NN, Set.UCS(5))                                | new Triple<>("Lorem", BigInteger.valueOf(24), "Ipsum")                      | true
        Set.Triple(Set.UCS(5), Set.NN, Set.UCS(5))                                | new Triple<>("Hello", BigInteger.valueOf(-1), "World!")                     | false
        Set.Quadruple(Set.UCS(5), Set.NN, Set.UCS(5), Set.UCS_star)               | new Quadruple<>("Lorem", BigInteger.valueOf(24), "Ipsum", "dolor")          | true
        Set.Quadruple(Set.UCS(5), Set.NN, Set.UCS(5), Set.UCS_star)               | new Quadruple<>("Hello", BigInteger.valueOf(-1), "World!", "Bye!")          | false
        Set.Quintuple(Set.UCS(5), Set.NN, Set.UCS(5), Set.UCS_star, Set.UCS_star) | new Quintuple<>("Lorem", BigInteger.valueOf(24), "Ipsum", "dolor", "sit")   | true
        Set.Quintuple(Set.UCS(5), Set.NN, Set.UCS(5), Set.UCS_star, Set.UCS_star) | new Quintuple<>("Hello", BigInteger.valueOf(-1), "World!", ",Good", "Bye!") | false
    }

    @Unroll
    def "contains(T x) for Psi set #x"() {
        given:
        def permutationBuilder = new IntVector.Builder(x.size())
        x.each {
            permutationBuilder.add(it)
        }
        def permutation = permutationBuilder.build()

        expect:
        set.contains(permutation) == expected

        where:
        set        | x               | expected
        Set.Psi(5) | [1, 2, 3, 4, 5] | true
        Set.Psi(5) | [5, 2, 3, 1, 4] | true
        Set.Psi(5) | [5, 2, 3, 1]    | false
        Set.Psi(5) | [5, 2, 3, 1, 5] | false
    }

    @Unroll
    def "contains(T x) for UTF8 set #x"() {
        given:
        def byteArrayUTF8 = stringToUTF8ByteArray(x)
        def byteArrayASCII = stringToUTF16ByteArray(x)
        expect:
        set.contains(byteArrayUTF8)
        !set.contains(byteArrayASCII)
        where:
        set      | x
        Set.UTF8 | "hello"
        Set.UTF8 | "world"

    }

    @Unroll
    def "containsAll(Stream<T> stream) #x"() {
        given:

        def stream = Arrays.stream(x.toArray())
        expect:
        set.containsAll(stream) == expected

        where:
        set                              | x                                                                                                                | expected
        Set.NN                           | []                                                                                                               | true
        Set.NN                           | [BigInteger.valueOf(0), BigInteger.valueOf(876213), BigInteger.valueOf(32984)]                                   | true
        Set.NN                           | [BigInteger.valueOf(Integer.MAX_VALUE), BigInteger.valueOf(21368), BigInteger.valueOf(9287)]                     | true
        Set.NN                           | [BigInteger.valueOf(-1), BigInteger.valueOf(13897), BigInteger.valueOf(0)]                                       | false
        Set.NN                           | [BigInteger.valueOf(123), BigInteger.valueOf(-98264)]                                                            | false
        Set.NN_plus                      | []                                                                                                               | true
        Set.NN_plus                      | [BigInteger.valueOf(21412), BigInteger.ONE, BigInteger.valueOf(324987)]                                          | true
        Set.NN_plus                      | [BigInteger.valueOf(324), BigInteger.valueOf(526873), BigInteger.valueOf(982374)]                                | true
        Set.NN_plus                      | [BigInteger.valueOf(213), BigInteger.valueOf(4879), BigInteger.valueOf(0)]                                       | false
        Set.NN_plus                      | [BigInteger.valueOf(0), BigInteger.valueOf(-928374), BigInteger.valueOf(0)]                                      | false
        Set.B(5)                         | []                                                                                                               | true
        Set.B(5)                         | [ByteArray.of([0x01, 0x02, 0x03, 0x04, 0x05] as byte[]), ByteArray.of([0xA4, 0x7F, 0xCE, 0x3D, 0x32] as byte[])] | true
        Set.B(5)                         | []                                                                                                               | true
        Set.B(5)                         | [ByteArray.of([0x01, 0x02, 0x03, 0x04] as byte[]), ByteArray.of([0x01, 0x02, 0x03, 0x04, 0x05] as byte[])]       | false
        Set.B(5)                         | [ByteArray.of([0x01, 0x02, 0x03, 0x04, 0x05, 0x06] as byte[])]                                                   | false
        Set.UCS(5)                       | []                                                                                                               | true
        Set.UCS(5)                       | ["12345", "12345", "12345"]                                                                                      | true
        Set.UCS(5)                       | ["zzzzz", "Z5DEQ", "Fe4T7"]                                                                                      | true
        Set.UCS(5)                       | ["1234", "12345", "jde749d"]                                                                                     | false
        Set.UCS(5)                       | ["123456", "15f"]                                                                                                | false
        Set.String(Alphabet.LATIN_52, 5) | []                                                                                                               | true
        Set.String(Alphabet.LATIN_52, 5) | ["abcde", "uehte", "eWtZd"]                                                                                      | true
        Set.String(Alphabet.LATIN_52, 5) | ["vwxyz", "irhts", "eurht"]                                                                                      | true
        Set.String(Alphabet.LATIN_52, 5) | ["djebt", "heurh", "abc"]                                                                                        | false
        Set.String(Alphabet.LATIN_52, 5) | ["abcd5", "e8gh4", "ehwk"]                                                                                       | false
    }

    @Unroll
    def "containsAll(Stream<T> stream) for Psi set #x"() {
        given:
        def permutationList = new LinkedList<IntVector>()
        x.each { array ->
            def permutationBuilder = new IntVector.Builder(array.size())
            array.each {
                permutationBuilder.add(it)
            }
            def permutation = permutationBuilder.build()
            permutationList.add(permutation)
        }
        def stream = permutationList.stream()
        expect:
        set.containsAll(stream) == expected

        where:
        set        | x                                                                | expected
        Set.Psi(5) | [[1, 2, 3, 4, 5], [1, 2, 3, 4, 5], [2, 4, 1, 3, 5]]              | true
        Set.Psi(5) | [[5, 2, 3, 1, 4], [2, 5, 1, 3, 4], [5, 1, 2, 4, 3]]              | true
        Set.Psi(5) | [[35, 47, 19, 371, 21], [5, 2, 3, 1], [1, 83, 485, 1287, 78]]    | false
        Set.Psi(5) | [[1, 2, 3, 4, 5], [2398, 9457, 2648, 1983, 35], [5, 2, 3, 1, 5]] | false

    }

    @Unroll
    def "containsAll(Stream<T> stream) for UTF8 set #x"() {
        given:
        def UTF8List = new ArrayList<ByteArray>()
        def UTF16List = new ArrayList<ByteArray>()

        x.each { s ->
            UTF8List.add(stringToUTF8ByteArray(s))
            UTF16List.add(stringToUTF16ByteArray(s))
        }

        expect:
        set.containsAll(UTF8List.stream())
        !set.containsAll(UTF16List.stream())
        where:
        set      | x
        Set.UTF8 | ["hello", "world"]
        Set.UTF8 | ["lorem", "ipsum"]

    }

    @Unroll
    def "contains(Collection<T> values) #x"() {
        given:
        def collection = new ArrayList()
        x.each {
            collection.add(it)
        }

        expect:
        set.containsAll(collection) == expected

        where:
        set                              | x                                                                                                                | expected
        Set.NN                           | []                                                                                                               | true
        Set.NN                           | [BigInteger.valueOf(0), BigInteger.valueOf(876213), BigInteger.valueOf(32984)]                                   | true
        Set.NN                           | [BigInteger.valueOf(Integer.MAX_VALUE), BigInteger.valueOf(21368), BigInteger.valueOf(9287)]                     | true
        Set.NN                           | [BigInteger.valueOf(-1), BigInteger.valueOf(13897), BigInteger.valueOf(0)]                                       | false
        Set.NN                           | [BigInteger.valueOf(123), BigInteger.valueOf(-98264)]                                                            | false
        Set.NN_plus                      | []                                                                                                               | true
        Set.NN_plus                      | [BigInteger.valueOf(21412), BigInteger.ONE, BigInteger.valueOf(324987)]                                          | true
        Set.NN_plus                      | [BigInteger.valueOf(324), BigInteger.valueOf(526873), BigInteger.valueOf(982374)]                                | true
        Set.NN_plus                      | [BigInteger.valueOf(213), BigInteger.valueOf(4879), BigInteger.valueOf(0)]                                       | false
        Set.NN_plus                      | [BigInteger.valueOf(0), BigInteger.valueOf(-928374), BigInteger.valueOf(0)]                                      | false
        Set.B(5)                         | []                                                                                                               | true
        Set.B(5)                         | [ByteArray.of([0x01, 0x02, 0x03, 0x04, 0x05] as byte[]), ByteArray.of([0xA4, 0x7F, 0xCE, 0x3D, 0x32] as byte[])] | true
        Set.B(5)                         | []                                                                                                               | true
        Set.B(5)                         | [ByteArray.of([0x01, 0x02, 0x03, 0x04] as byte[]), ByteArray.of([0x01, 0x02, 0x03, 0x04, 0x05] as byte[])]       | false
        Set.B(5)                         | [ByteArray.of([0x01, 0x02, 0x03, 0x04, 0x05, 0x06] as byte[])]                                                   | false
        Set.UCS(5)                       | []                                                                                                               | true
        Set.UCS(5)                       | ["12345", "12345", "12345"]                                                                                      | true
        Set.UCS(5)                       | ["zzzzz", "Z5DEQ", "Fe4T7"]                                                                                      | true
        Set.UCS(5)                       | ["1234", "12345", "jde749d"]                                                                                     | false
        Set.UCS(5)                       | ["123456", "15f"]                                                                                                | false
        Set.String(Alphabet.LATIN_52, 5) | []                                                                                                               | true
        Set.String(Alphabet.LATIN_52, 5) | ["abcde", "uehte", "eWtZd"]                                                                                      | true
        Set.String(Alphabet.LATIN_52, 5) | ["vwxyz", "irhts", "eurht"]                                                                                      | true
        Set.String(Alphabet.LATIN_52, 5) | ["djebt", "heurh", "abc"]                                                                                        | false
        Set.String(Alphabet.LATIN_52, 5) | ["abcd5", "e8gh4", "ehwk"]                                                                                       | false
    }

    @Unroll
    def "containsAll(Collection<T> values) for Psi set #x"() {

        given:
        def collection = new ArrayList()
        x.each { array ->
            def permutationBuilder = new IntVector.Builder(array.size())
            array.each {
                permutationBuilder.add(it)
            }
            def permutation = permutationBuilder.build()
            collection.add(permutation)
        }

        expect:
        set.containsAll(collection) == expected
        where:
        set        | x                                                                | expected
        Set.Psi(5) | [[1, 2, 3, 4, 5], [1, 2, 3, 4, 5], [2, 4, 1, 3, 5]]              | true
        Set.Psi(5) | [[5, 2, 3, 1, 4], [2, 5, 1, 3, 4], [5, 1, 2, 4, 3]]              | true
        Set.Psi(5) | [[35, 47, 19, 371, 21], [5, 2, 3, 1], [1, 83, 485, 1287, 78]]    | false
        Set.Psi(5) | [[1, 2, 3, 4, 5], [2398, 9457, 2648, 1983, 35], [5, 2, 3, 1, 5]] | false

    }

    def "containsAll(Collection<T> values) for UTF8 set #x"() {
        given:
        def UTF8List = new ArrayList<ByteArray>()
        def UTF16List = new ArrayList<ByteArray>()

        x.each { s ->
            UTF8List.add(stringToUTF8ByteArray(s))
            UTF16List.add(stringToUTF16ByteArray(s))
        }

        expect:
        set.containsAll(UTF8List)
        !set.containsAll(UTF16List)
        where:
        set      | x
        Set.UTF8 | ["hello", "world"]
        Set.UTF8 | ["lorem", "ipsum"]

    }

    def "orNull()"() {
        expect:
        !set.contains(null)
        set.orNull().contains(null)
        where:
        set                              | _
        Set.NN                           | _
        Set.NN_plus                      | _
        Set.B(5)                         | _
        Set.UCS(5)                       | _
        Set.String(Alphabet.LATIN_52, 5) | _

    }

    def "String(Alphabet alphabet, int n): should throw UtilityException for alphabet = null "() {
        when:
        Set.String(alphabet, n)

        then:
        thrown(UtilityException)

        where:
        alphabet | n
        null     | 5
    }
}
