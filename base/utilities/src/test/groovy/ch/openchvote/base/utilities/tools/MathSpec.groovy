/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tools

import ch.openchvote.base.utilities.UtilityException
import ch.openchvote.base.utilities.sequence.IntVector
import ch.openchvote.base.utilities.sequence.Vector
import spock.lang.Specification

class MathSpec extends Specification {

    def "intToByte(int x) should convert a int value into a corresponding byte value"() {
        expect:
        Math.intToByte(x) == expected as byte

        where:
        x   | expected
        0   | 0x00
        18  | 0x12
        45  | 0x2D
        123 | 0x7B
        255 | 0xFF
    }

    def "byteToInt(byte b) should convert a byte value into a corresponding int value"() {
        expect:
        Math.byteToInt(b as byte) == expected

        where:
        b    | expected
        0x00 | 0
        0x12 | 18
        0x2D | 45
        0x7B | 123
        0xFF | 255
    }

    def "and(byte b1, byte b2) should return the result of bit-wise AND operation of two bytes"() {
        expect:
        Math.and(b1 as byte, b2 as byte) == expected as byte

        where:
        b1   | b2   | expected
        0x00 | 0xFF | 0x00
        0x12 | 0xE3 | 0x02
        0x2D | 0x5A | 0x08
        0xED | 0xF5 | 0xE5
        0xFF | 0x57 | 0x57
    }

    def "or(byte b1, byte b2) should return the result of bit-wise OR operation of two bytes"() {
        expect:
        Math.or(b1 as byte, b2 as byte) == expected as byte

        where:
        b1   | b2   | expected
        0x00 | 0xFF | 0xFF
        0x12 | 0xE3 | 0xF3
        0x2D | 0x5A | 0x7F
        0xED | 0xF5 | 0xFD
        0xFF | 0x57 | 0xFF
    }

    def "xor(byte b1, byte b2) should return the result of bit-wise XOR operation of two bytes"() {
        expect:
        Math.xor(b1 as byte, b2 as byte) == expected as byte

        where:
        b1   | b2   | expected
        0x00 | 0xFF | 0xFF
        0x12 | 0xE3 | 0xF1
        0x2D | 0x5A | 0x77
        0xED | 0xF5 | 0x18
        0xFF | 0x57 | 0xA8
    }

    def "ceilDiv(int x, int y) should return the rounded up fraction of two int values"() {
        expect:
        Math.ceilDiv(x, y) == expected

        where:
        x    | y    | expected
        0    | 125  | 0
        4    | 4    | 1
        458  | 122  | 4
        771  | 81   | 10
        9674 | 3579 | 3
    }

    def "bitLength(int x) should return the the number of necessary bits for the given int value"() {
        expect:
        Math.bitLength(x) == expected

        where:
        x    | expected
        0    | 0
        4    | 3
        458  | 9
        771  | 10
        9674 | 14
    }

    def "divides(int x, int y) should return whether y is dividable by x or not"() {
        expect:
        Math.divides(x, y) == expected

        where:
        x    | y     | expected
        1    | 0     | true
        4    | 4     | true
        458  | 122   | false
        771  | 62451 | true
        9674 | 3225  | false
    }

    def "powerOfTwo(int y) should return 2^y"() {
        expect:
        Math.intPowerOfTwo(y) == expected

        where:
        y  | expected
        1  | 2
        4  | 16
        15 | 32768
        23 | 8388608
        30 | 1073741824
    }

    def "powerOfTen(int y) should return 10^y"() {
        expect:
        Math.intPowerOfTen(y) == expected

        where:
        y | expected
        1 | 10
        4 | 10000
        5 | 100000
        7 | 10000000
        9 | 1000000000
    }

    def "intSumProd(IntVector vector1, IntVector vector2) should a UtilityException if vectors are not the same size"() {
        given:
        def vector1Builder = new IntVector.Builder(x.size())
        x.each {
            vector1Builder.add(it)
        }
        def vector1 = vector1Builder.build()
        def vector2Builder = new IntVector.Builder(y.size())
        y.each {
            vector2Builder.add(it)
        }
        def vector2 = vector2Builder.build()

        when:
        Math.intSumProd(vector1, vector2)

        then:
        thrown(UtilityException)

        where:
        x                       | y
        [1, 2, 3, 4, 5]         | [1, 2, 3, 4]
        [379, 26387]            | [1, 2, 3]
        [32468, 123, 4857, 274] | [3]
        [123]                   | []
    }

    def "intSumProd(IntVector vector1, IntVector vector2) should return the sum of the products of each value pair of the two vectors"() {
        given:
        def vector1Builder = new IntVector.Builder(x.size())
        x.each {
            vector1Builder.add(it)
        }
        def vector1 = vector1Builder.build()
        def vector2Builder = new IntVector.Builder(y.size())
        y.each {
            vector2Builder.add(it)
        }
        def vector2 = vector2Builder.build()

        expect:
        Math.intSumProd(vector1, vector2) == expected

        where:
        x                  | y               | expected
        [1, 2, 3, 4, 5]    | [1, 2, 3, 4, 5] | 55
        [379, 26387]       | [1, 2]          | 53153
        [32468, 123, 4857] | [3, 274, 4]     | 150534
        []                 | []              | 0
    }

    def "ceilDiv(BigInteger x, BigInteger y) should return the rounded up fraction of two int values"() {
        expect:
        Math.ceilDiv(x, y) == expected

        where:
        x                        | y                        | expected
        BigInteger.valueOf(0)    | BigInteger.valueOf(125)  | BigInteger.valueOf(0)
        BigInteger.valueOf(3)    | BigInteger.valueOf(4)    | BigInteger.valueOf(1)
        BigInteger.valueOf(4)    | BigInteger.valueOf(4)    | BigInteger.valueOf(1)
        BigInteger.valueOf(5)    | BigInteger.valueOf(4)    | BigInteger.valueOf(2)
        BigInteger.valueOf(458)  | BigInteger.valueOf(122)  | BigInteger.valueOf(4)
        BigInteger.valueOf(771)  | BigInteger.valueOf(81)   | BigInteger.valueOf(10)
        BigInteger.valueOf(9674) | BigInteger.valueOf(3579) | BigInteger.valueOf(3)
    }

    def "byteLength(BigInteger x) should return the number of bytes to represent x"() {
        expect:
        Math.byteLength(x) == expected

        where:
        x                          | expected
        BigInteger.valueOf(0)      | 0
        BigInteger.valueOf(1)      | 1
        BigInteger.valueOf(2)      | 1
        BigInteger.valueOf(3)      | 1
        BigInteger.valueOf(-1)     | 1
        BigInteger.valueOf(-2)     | 1
        BigInteger.valueOf(-3)     | 1
        BigInteger.valueOf(254)    | 1
        BigInteger.valueOf(255)    | 1
        BigInteger.valueOf(256)    | 2
        BigInteger.valueOf(257)    | 2
        BigInteger.valueOf(-254)   | 1
        BigInteger.valueOf(-255)   | 1
        BigInteger.valueOf(-256)   | 2
        BigInteger.valueOf(-257)   | 2
        BigInteger.valueOf(65534)  | 2
        BigInteger.valueOf(65535)  | 2
        BigInteger.valueOf(65536)  | 3
        BigInteger.valueOf(65537)  | 3
        BigInteger.valueOf(-65534) | 2
        BigInteger.valueOf(-65535) | 2
        BigInteger.valueOf(-65536) | 3
        BigInteger.valueOf(-65537) | 3
    }

    def "ceilLog(BigInteger x, int b) should return the rounded up base b logarithm of x"() {
        expect:
        Math.ceilLog(x, b) == expected

        where:
        x                        | b  | expected
        BigInteger.valueOf(1)    | 2  | 0
        BigInteger.valueOf(4)    | 3  | 2
        BigInteger.valueOf(458)  | 4  | 5
        BigInteger.valueOf(711)  | 5  | 5
        BigInteger.valueOf(9674) | 10 | 4
    }

    def "floorLog(BigInteger x, int b) should return the rounded down base b logarithm of x"() {
        expect:
        Math.floorLog(x, b) == expected

        where:
        x                        | b  | expected
        BigInteger.valueOf(1)    | 2  | 0
        BigInteger.valueOf(4)    | 3  | 1
        BigInteger.valueOf(458)  | 4  | 4
        BigInteger.valueOf(711)  | 5  | 4
        BigInteger.valueOf(9674) | 10 | 3
    }

    def "divides(BigInteger x, BigInteger y) should return whether y is dividable by x or not"() {
        expect:
        Math.divides(x, y) == expected

        where:
        x                        | y                         | expected
        BigInteger.valueOf(1)    | BigInteger.valueOf(0)     | true
        BigInteger.valueOf(4)    | BigInteger.valueOf(4)     | true
        BigInteger.valueOf(458)  | BigInteger.valueOf(122)   | false
        BigInteger.valueOf(771)  | BigInteger.valueOf(62451) | true
        BigInteger.valueOf(9674) | BigInteger.valueOf(3225)  | false
    }

    def "sum(Vector<BigInteger> vector) should return the sum of all integer in the vector"() {
        given:
        def vectorBuilder = new Vector.Builder(x.size())
        x.each {
            vectorBuilder.add(it)
        }
        def vector = vectorBuilder.build()

        expect:
        Math.sum(vector) == expected

        where:
        x                                                                                                                   | expected
        [BigInteger.valueOf(1), BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(4), BigInteger.valueOf(5)] | BigInteger.valueOf(15)
        [BigInteger.valueOf(379), BigInteger.valueOf(26387), BigInteger.valueOf(12)]                                        | BigInteger.valueOf(26778)
        [BigInteger.valueOf(32468), BigInteger.valueOf(123), BigInteger.valueOf(4857), BigInteger.valueOf(274)]             | BigInteger.valueOf(37722)
        [BigInteger.valueOf(123)]                                                                                           | BigInteger.valueOf(123)
        []                                                                                                                  | BigInteger.valueOf(0)
    }

    def "prod(Vector<BigInteger> vector) should return the product of all integer in the vector"() {
        given:
        def vectorBuilder = new Vector.Builder(x.size())
        x.each {
            vectorBuilder.add(it)
        }
        def vector = vectorBuilder.build()

        expect:
        Math.prod(vector) == expected

        where:
        x                                                                                                                   | expected
        [BigInteger.valueOf(1), BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(4), BigInteger.valueOf(5)] | BigInteger.valueOf(120)
        [BigInteger.valueOf(379), BigInteger.valueOf(26387), BigInteger.valueOf(12)]                                        | BigInteger.valueOf(120008076)
        [BigInteger.valueOf(32468), BigInteger.valueOf(123), BigInteger.valueOf(4857), BigInteger.valueOf(274)]             | BigInteger.valueOf(5314706855352)
        [BigInteger.valueOf(123)]                                                                                           | BigInteger.valueOf(123)
        []                                                                                                                  | BigInteger.valueOf(1)
    }

    def "jacobiSymbol(BigInteger x, BigInteger n) should return the Jacobi symbol of the two values"() {
        expect:
        Math.jacobiSymbol(x, y) == expected

        where:
        x                        | y                        | expected
        BigInteger.valueOf(3)    | BigInteger.valueOf(1)    | 1
        BigInteger.valueOf(5)    | BigInteger.valueOf(5)    | 0
        BigInteger.valueOf(8)    | BigInteger.valueOf(3)    | -1
        BigInteger.valueOf(771)  | BigInteger.valueOf(51)   | 0
        BigInteger.valueOf(9674) | BigInteger.valueOf(3225) | 1
    }

    def "mod256(BigInteger x)  should compute x mod 256"() {
        expect:
        Math.mod256(x) == expected

        where:
        x                        | expected
        BigInteger.valueOf(1)    | 1
        BigInteger.valueOf(255)  | 255
        BigInteger.valueOf(826)  | 58
        BigInteger.valueOf(771)  | 3
        BigInteger.valueOf(9674) | 202
    }

    def "div256(BigInteger x) should compute x / 256"() {
        expect:
        Math.div256(x) == expected

        where:
        x                        | expected
        BigInteger.valueOf(1)    | BigInteger.valueOf(0)
        BigInteger.valueOf(255)  | BigInteger.valueOf(0)
        BigInteger.valueOf(826)  | BigInteger.valueOf(3)
        BigInteger.valueOf(771)  | BigInteger.valueOf(3)
        BigInteger.valueOf(9674) | BigInteger.valueOf(37)
    }

    def "mult256(BigInteger x) should compute x * 256"() {
        expect:
        Math.mult256(x) == expected

        where:
        x                        | expected
        BigInteger.valueOf(1)    | BigInteger.valueOf(256)
        BigInteger.valueOf(255)  | BigInteger.valueOf(65280)
        BigInteger.valueOf(826)  | BigInteger.valueOf(211456)
        BigInteger.valueOf(771)  | BigInteger.valueOf(197376)
        BigInteger.valueOf(9674) | BigInteger.valueOf(2476544)
    }

    def "div2(BigInteger x) should compute x / 2"() {
        expect:
        Math.div2(x) == expected

        where:
        x                        | expected
        BigInteger.valueOf(1)    | BigInteger.valueOf(0)
        BigInteger.valueOf(255)  | BigInteger.valueOf(127)
        BigInteger.valueOf(826)  | BigInteger.valueOf(413)
        BigInteger.valueOf(771)  | BigInteger.valueOf(385)
        BigInteger.valueOf(9674) | BigInteger.valueOf(4837)
    }

    def "mult2(BigInteger x) should compute x * 2"() {
        expect:
        Math.mult2(x) == expected

        where:
        x                        | expected
        BigInteger.valueOf(1)    | BigInteger.valueOf(2)
        BigInteger.valueOf(255)  | BigInteger.valueOf(510)
        BigInteger.valueOf(826)  | BigInteger.valueOf(1652)
        BigInteger.valueOf(771)  | BigInteger.valueOf(1542)
        BigInteger.valueOf(9674) | BigInteger.valueOf(19348)
    }
}
