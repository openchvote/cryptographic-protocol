/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence
import spock.lang.Specification

class SubStringSpec extends Specification {

    def "SubString(String s)"() {
        when:
        def subString = new SubString(s)

        then:
        for (int i = 0; i < subString.getLength(); i++) {
            subString.charAt(i) == s[i].charAt(0)
        }
        subString.toString() == s

        where:
        s             | _
        "Hello World" | _
        "Test"        | _
        "lorem ipsum" | _
        "CHVote"      | _
    }

    def "SubString(String s, int to, int from)"() {
        when:
        def subString = new SubString(s, from, to)

        then:
        for (int i = 0; i < subString.getLength(); i++) {
            subString.charAt(i) == expected[i].charAt(0)
        }
        subString.toString() == expected

        where:
        s             | from | to | expected
        "Hello World" | 6    | 11 | "World"
        "Test"        | 1    | 4  | "est"
        "lorem ipsum" | 6    | 8  | "ip"
        "CHVote"      | 0    | 2  | "CH"
    }

    def "subString(int to, int from)"() {
        when:
        def subString = new SubString(s)
        def expected = new SubString(eString)
        subString = subString.substring(from, to)

        then:
        subString == expected
        subString.hashCode() == expected.hashCode()

        where:
        s             | from | to | eString
        "Hello World" | 6    | 11 | "World"
        "Test"        | 1    | 4  | "est"
        "lorem ipsum" | 6    | 8  | "ip"
        "CHVote"      | 0    | 2  | "CH"
    }
}
