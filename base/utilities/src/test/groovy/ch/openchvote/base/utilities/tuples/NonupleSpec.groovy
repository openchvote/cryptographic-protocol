/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Shared
import spock.lang.Specification

class NonupleSpec extends Specification {

    @Shared
    def default_First
    @Shared
    def default_Second
    @Shared
    def default_Third
    @Shared
    def default_Fourth
    @Shared
    def default_Fifth
    @Shared
    def default_Sixth
    @Shared
    def default_Seventh
    @Shared
    def default_Eighth
    @Shared
    def default_Ninth

    def setupSpec() {
        default_First = String.valueOf(1)
        default_Second = String.valueOf(2)
        default_Third = String.valueOf(3)
        default_Fourth = String.valueOf(4)
        default_Fifth = String.valueOf(5)
        default_Sixth = String.valueOf(6)
        default_Seventh = String.valueOf(7)
        default_Eighth = String.valueOf(8)
        default_Ninth = String.valueOf(9)
    }

    def "Constructor throws UtilityException for case: #caseDesc"() {
        when:
        new Nonuple(first, second, third, fourth, fifth, sixth, seventh, eighth, ninth)

        then:
        thrown(UtilityException)

        where:
        caseDesc          | first         | second         | third         | fourth         | fifth         | sixth         | seventh         | eighth         | ninth
        "first is null"   | null          | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth
        "second is null"  | default_First | null           | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth
        "third is null"   | default_First | default_Second | null          | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth
        "fourth is null"  | default_First | default_Second | default_Third | null           | default_Fifth | default_Sixth | default_Seventh | default_Eighth | default_Ninth
        "fifth is null"   | default_First | default_Second | default_Third | default_Fourth | null          | default_Sixth | default_Seventh | default_Eighth | default_Ninth
        "sixth is null"   | default_First | default_Second | default_Third | default_Fourth | default_Fifth | null          | default_Seventh | default_Eighth | default_Ninth
        "seventh is null" | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | null            | default_Eighth | default_Ninth
        "eighth is null"  | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | null           | default_Ninth
        "ninth is null"   | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh | default_Eighth | null
    }

    def "Test constructor and getter methods for the elements"() {
        given:
        def nonuple = new Nonuple(first, second, third, fourth, fifth, sixth, seventh, eighth, ninth)

        expect:
        nonuple.getFirst() == first
        nonuple.getSecond() == second
        nonuple.getThird() == third
        nonuple.getFourth() == fourth
        nonuple.getFifth() == fifth
        nonuple.getSixth() == sixth
        nonuple.getSeventh() == seventh
        nonuple.getEighth() == eighth
        nonuple.getNinth() == ninth

        where:
        first                 | second                | third                 | fourth                | fifth                 | sixth                 | seventh               | eighth                | ninth
        "first"               | "second"              | "third"               | "fourth"              | "fifth"               | "sixth"               | "seventh"             | "eighth"              | "ninth"
        1                     | 2                     | 3                     | 4                     | 5                     | 6                     | 7                     | 8                     | 9
        BigInteger.valueOf(1) | BigInteger.valueOf(2) | BigInteger.valueOf(3) | BigInteger.valueOf(4) | BigInteger.valueOf(5) | BigInteger.valueOf(6) | BigInteger.valueOf(7) | BigInteger.valueOf(8) | BigInteger.valueOf(9)
    }

    @SuppressWarnings('ChangeToOperator')
    def "equals should return true if all pairs of elements of two nonuples are the same and false otherwise"() {
        given:
        def nonuple1 = new Nonuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8))
        def nonuple2 = new Nonuple(elems2.get(0), elems2.get(1), elems2.get(2), elems2.get(3), elems2.get(4), elems2.get(5), elems2.get(6), elems2.get(7), elems2.get(8))

        expect:
        //noinspection ChangeToOperator
        nonuple1.equals(nonuple2) == expected
        (nonuple1.hashCode() == nonuple2.hashCode()) == expected

        where:
        elems1                                                                                 | elems2                                                                                 | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | true
        ["wrong", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | false
        ["first", "wrong", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | false
        ["first", "second", "wrong", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | false
        ["first", "second", "third", "wrong", "fifth", "sixth", "seventh", "eighth", "ninth"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | false
        ["first", "second", "third", "fourth", "wrong", "sixth", "seventh", "eighth", "ninth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | false
        ["first", "second", "third", "fourth", "fifth", "wrong", "seventh", "eighth", "ninth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "wrong", "eighth", "ninth"]   | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "wrong", "ninth"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "wrong"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | false
    }

    def "toString()"() {
        given:
        def nonuple = new Nonuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8))

        expect:
        nonuple.toString() == expected

        where:
        elems1                                                                                 | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | "(first,second,third,fourth,fifth,sixth,seventh,eighth,ninth)"
        [1, 2, 3, 4, 5, 6, 7, 8, 9]                                                            | "(1,2,3,4,5,6,7,8,9)"
    }

    def "toStream() should return a Stream containing all elements in the nonuple in the correct order"() {
        given:
        def nonuple = new Nonuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8))

        expect:
        nonuple.toStream().toList() == expected

        where:
        elems1                                                                                 | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"]
        [1, 2, 3, 4, 5, 6, 7, 8, 9]                                                            | [1, 2, 3, 4, 5, 6, 7, 8, 9]
    }
}
