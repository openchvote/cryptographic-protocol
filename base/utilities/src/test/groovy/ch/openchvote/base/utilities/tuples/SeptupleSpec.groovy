/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Shared
import spock.lang.Specification

class SeptupleSpec extends Specification {

    @Shared
    def default_First
    @Shared
    def default_Second
    @Shared
    def default_Third
    @Shared
    def default_Fourth
    @Shared
    def default_Fifth
    @Shared
    def default_Sixth
    @Shared
    def default_Seventh

    def setupSpec() {
        default_First = String.valueOf(1)
        default_Second = String.valueOf(2)
        default_Third = String.valueOf(3)
        default_Fourth = String.valueOf(4)
        default_Fifth = String.valueOf(5)
        default_Sixth = String.valueOf(6)
        default_Seventh = String.valueOf(7)
    }

    def "Constructor throws UtilityException for case: #caseDesc"() {
        when:
        new Septuple(first, second, third, fourth, fifth, sixth, seventh)

        then:
        thrown(UtilityException)

        where:
        caseDesc          | first         | second         | third         | fourth         | fifth         | sixth         | seventh
        "first is null"   | null          | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh
        "second is null"  | default_First | null           | default_Third | default_Fourth | default_Fifth | default_Sixth | default_Seventh
        "third is null"   | default_First | default_Second | null          | default_Fourth | default_Fifth | default_Sixth | default_Seventh
        "fourth is null"  | default_First | default_Second | default_Third | null           | default_Fifth | default_Sixth | default_Seventh
        "fifth is null"   | default_First | default_Second | default_Third | default_Fourth | null          | default_Sixth | default_Seventh
        "sixth is null"   | default_First | default_Second | default_Third | default_Fourth | default_Fifth | null          | default_Seventh
        "seventh is null" | default_First | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth | null
    }

    def "Test constructor and getter methods for the elements"() {
        given:
        def septuple = new Septuple(first, second, third, fourth, fifth, sixth, seventh)

        expect:
        septuple.getFirst() == first
        septuple.getSecond() == second
        septuple.getThird() == third
        septuple.getFourth() == fourth
        septuple.getFifth() == fifth
        septuple.getSixth() == sixth
        septuple.getSeventh() == seventh

        where:
        first                 | second                | third                 | fourth                | fifth                 | sixth                 | seventh
        "first"               | "second"              | "third"               | "fourth"              | "fifth"               | "sixth"               | "seventh"
        1                     | 2                     | 3                     | 4                     | 5                     | 6                     | 7
        BigInteger.valueOf(1) | BigInteger.valueOf(2) | BigInteger.valueOf(3) | BigInteger.valueOf(4) | BigInteger.valueOf(5) | BigInteger.valueOf(6) | BigInteger.valueOf(7)
    }

    @SuppressWarnings('ChangeToOperator')
    def "equals should return true if all pairs of elements of two septuples are the same and false otherwise"() {
        given:
        def septuple1 = new Septuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6))
        def septuple2 = new Septuple(elems2.get(0), elems2.get(1), elems2.get(2), elems2.get(3), elems2.get(4), elems2.get(5), elems2.get(6))

        expect:
        //noinspection ChangeToOperator
        septuple1.equals(septuple2) == expected
        (septuple1.hashCode() == septuple2.hashCode()) == expected

        where:
        elems1                                                              | elems2                                                              | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | true
        ["wrong", "second", "third", "fourth", "fifth", "sixth", "seventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | false
        ["first", "wrong", "third", "fourth", "fifth", "sixth", "seventh"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | false
        ["first", "second", "wrong", "fourth", "fifth", "sixth", "seventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | false
        ["first", "second", "third", "wrong", "fifth", "sixth", "seventh"]  | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | false
        ["first", "second", "third", "fourth", "wrong", "sixth", "seventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | false
        ["first", "second", "third", "fourth", "fifth", "wrong", "seventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | false
        ["first", "second", "third", "fourth", "fifth", "sixth", "wrong"]   | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | false
    }

    def "toString()"() {
        given:
        def septuple = new Septuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6))

        expect:
        septuple.toString() == expected

        where:
        elems1                                                              | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | "(first,second,third,fourth,fifth,sixth,seventh)"
        [1, 2, 3, 4, 5, 6, 7]                                               | "(1,2,3,4,5,6,7)"
    }

    def "toStream() should return a Stream containing all elements in the septuple in the correct order"() {
        given:
        def septuple = new Septuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6))

        expect:
        septuple.toStream().toList() == expected

        where:
        elems1                                                              | expected
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"]
        [1, 2, 3, 4, 5, 6, 7]                                               | [1, 2, 3, 4, 5, 6, 7]
    }

}

