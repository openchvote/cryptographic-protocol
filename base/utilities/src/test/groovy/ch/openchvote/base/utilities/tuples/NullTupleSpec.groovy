/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples

import spock.lang.Specification

class NullTupleSpec extends Specification {
    def "equals should return true if all pairs of elements of two pairs are the same and false otherwise"() {
        given:
        def nullTuple1 = new NullTuple()
        def nullTuple2 = new NullTuple()

        expect:
        nullTuple1 == nullTuple2
        nullTuple1.hashCode() == nullTuple2.hashCode()
    }

    def "toString()"() {
        given:
        def nullTuple = new NullTuple()

        expect:
        nullTuple.toString() == "()"
    }

    def "toStream() should return a Stream containing all elements in the pair in the correct order"() {
        given:
        def nullTuple = new NullTuple()

        expect:
        nullTuple.toStream().toList() == []
    }

}
