/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.algebra
import ch.openchvote.base.utilities.sequence.Vector
import spock.lang.Specification
import spock.lang.Unroll

class ZZSpec extends Specification {

    @Unroll
    def "contains(T x) #x"() {
        expect:
        ZZ.of(BigInteger.valueOf(n)).contains(x == null ? null : BigInteger.valueOf(x)) == expected

        where:
        n   | x    | expected
        1   | null | false
        1   | -1   | false
        1   | 0    | true
        1   | 1    | false
        2   | null | false
        2   | -1   | false
        2   | 0    | true
        2   | 1    | true
        2   | 2    | false
        10  | null | false
        10  | -1   | false
        10  | 0    | true
        10  | 1    | true
        10  | 9    | true
        10  | 10   | false
        167 | null | false
        167 | -1   | false
        167 | 0    | true
        167 | 1    | true
        167 | 166  | true
        167 | 167  | false
    }

    def "pow(BigInteger x, BigInteger y, BigInteger n) should return x^y mod n"() {

        expect:
        ZZ.of(BigInteger.valueOf(n)).pow(BigInteger.valueOf(x), BigInteger.valueOf(y)) == BigInteger.valueOf(expected)

        where:
        x   | y  | n   | expected
        124 | 0  | 167 | 1
        124 | 1  | 167 | 124
        34  | 74 | 167 | 97
        84  | 91 | 167 | 152
        74  | 0  | 503 | 1
        429 | 1  | 503 | 429
        496 | 74 | 503 | 131
        397 | 91 | 503 | 271
    }

    def "invert(BigInteger x, BigInteger n) should return the inverse to x based on modulo n"() {
        expect:
        ZZ.of(BigInteger.valueOf(n)).invert(BigInteger.valueOf(x)) == BigInteger.valueOf(expected)

        where:
        x   | n   | expected
        1   | 167 | 1
        14  | 167 | 12
        42  | 167 | 4
        67  | 167 | 5
        1   | 503 | 1
        4   | 503 | 126
        151 | 503 | 10
        397 | 503 | 242
    }

    def "add(BigInteger x, BigInteger y, BigInteger n) should return x+y mod n"() {
        expect:
        ZZ.of(BigInteger.valueOf(n)).add(BigInteger.valueOf(x), BigInteger.valueOf(y)) == BigInteger.valueOf(expected)

        where:
        x   | y  | n   | expected
        124 | 0  | 167 | 124
        124 | 1  | 167 | 125
        34  | 74 | 167 | 108
        84  | 91 | 167 | 8
        74  | 0  | 503 | 74
        429 | 1  | 503 | 430
        496 | 74 | 503 | 67
        397 | 91 | 503 | 488
    }

    def "subtract(BigInteger x, BigInteger y, BigInteger n) should return x-y mod n"() {
        expect:
        ZZ.of(BigInteger.valueOf(n)).subtract(BigInteger.valueOf(x), BigInteger.valueOf(y)) == BigInteger.valueOf(expected)

        where:
        x   | y   | n   | expected
        124 | 0   | 167 | 124
        124 | 1   | 167 | 123
        34  | 74  | 167 | 127
        91  | 84  | 167 | 7
        74  | 0   | 503 | 74
        429 | 1   | 503 | 428
        496 | 74  | 503 | 422
        97  | 391 | 503 | 209
    }

    @SuppressWarnings('ChangeToOperator')
    def "minus(BigInteger n, BigInteger x) should return n-x mod n"() {
        expect:
        //noinspection ChangeToOperator
        ZZ.of(BigInteger.valueOf(n)).minus(BigInteger.valueOf(x)) == BigInteger.valueOf(expected)

        where:
        n   | x  | expected
        124 | 0  | 0
        124 | 1  | 123
        34  | 74 | 28
        84  | 91 | 77
        74  | 0  | 0
        429 | 1  | 428
        496 | 74 | 422
        397 | 91 | 306
    }

    def "multiply(BigInteger x, BigInteger y, BigInteger n) should return x*y mod n"() {
        expect:
        ZZ.of(BigInteger.valueOf(n)).multiply(BigInteger.valueOf(x), BigInteger.valueOf(y)) == BigInteger.valueOf(expected)

        where:
        x   | y  | n   | expected
        124 | 0  | 167 | 0
        124 | 1  | 167 | 124
        34  | 74 | 167 | 11
        9   | 8  | 167 | 72
        74  | 0  | 503 | 0
        429 | 1  | 503 | 429
        496 | 74 | 503 | 488
        97  | 3  | 503 | 291
    }

    def "divide(BigInteger x, BigInteger y, BigInteger n) should return x*(y^-1) mod n"() {
        expect:
        ZZ.of(BigInteger.valueOf(n)).divide(BigInteger.valueOf(x), BigInteger.valueOf(y)) == BigInteger.valueOf(expected)

        where:
        x   | y   | n   | expected
        124 | 1   | 167 | 124
        124 | 14  | 167 | 152
        34  | 42  | 167 | 136
        9   | 67  | 167 | 45
        74  | 1   | 503 | 74
        429 | 4   | 503 | 233
        496 | 151 | 503 | 433
        97  | 397 | 503 | 336
    }

    def "sum(Vector<BigInteger> x_bold, BigInteger n) should return the sum of all numbers in x-bold mod n"() {
        given:
        def vectorBuilder = new Vector.Builder(x_bold.size())
        x_bold.each {
            vectorBuilder.add(BigInteger.valueOf(it))
        }
        def vector = vectorBuilder.build()

        expect:
        ZZ.of(BigInteger.valueOf(n)).sum(vector) == BigInteger.valueOf(expected)

        where:
        x_bold                  | n   | expected
        [1, 2, 3, 4, 5]         | 167 | 15
        [379, 26387, 12]        | 167 | 58
        [32468, 123, 4857, 274] | 167 | 147
        [123]                   | 167 | 123
        []                      | 167 | 0
    }

    def "sumProd(Vector<BigInteger> x_bold, Vector<BigInteger> y_bold, BigInteger n)"() {
        given:
        def vector1Builder = new Vector.Builder(x.size())
        x.each {
            vector1Builder.add(BigInteger.valueOf(it))
        }
        def vector1 = vector1Builder.build()
        def vector2Builder = new Vector.Builder(y.size())
        y.each {
            vector2Builder.add(BigInteger.valueOf(it))
        }
        def vector2 = vector2Builder.build()

        expect:
        ZZ.of(BigInteger.valueOf(n)).sumProd(vector1, vector2) == BigInteger.valueOf(expected)

        where:
        x                  | y               | n   | expected
        [1, 2, 3, 4, 5]    | [1, 2, 3, 4, 5] | 167 | 55
        [379, 26387]       | [1, 2]          | 167 | 47
        [32468, 123, 4857] | [3, 274, 4]     | 167 | 67
        []                 | []              | 167 | 0
    }

    def "prod(Vector<BigInteger> x_bold, BigInteger n) should return the product of all numbers in x-bold mod n"() {
        given:
        def vectorBuilder = new Vector.Builder(x_bold.size())
        x_bold.each {
            vectorBuilder.add(BigInteger.valueOf(it))
        }
        def vector = vectorBuilder.build()

        expect:
        ZZ.of(BigInteger.valueOf(n)).prod(vector) == BigInteger.valueOf(expected)

        where:
        x_bold                  | n   | expected
        [1, 2, 3, 4, 5]         | 167 | 120
        [379, 26387, 12]        | 167 | 39
        [32468, 123, 4857, 274] | 167 | 36
        [123]                   | 167 | 123
        []                      | 167 | 1
    }

    def "prodPow(Vector<BigInteger> x_bold, Vector<BigInteger> y_bold, BigInteger n)"() {
        given:
        def vector1Builder = new Vector.Builder(x.size())
        x.each {
            vector1Builder.add(BigInteger.valueOf(it))
        }
        def vector1 = vector1Builder.build()
        def vector2Builder = new Vector.Builder(y.size())
        y.each {
            vector2Builder.add(BigInteger.valueOf(it))
        }
        def vector2 = vector2Builder.build()

        expect:
        ZZ.of(BigInteger.valueOf(n)).prodPow(vector1, vector2) == BigInteger.valueOf(expected)

        where:
        x                  | y               | n   | expected
        [1, 2, 3, 4, 5]    | [1, 2, 3, 4, 5] | 167 | 45
        [379, 26387]       | [1, 2]          | 167 | 45
        [32468, 123, 4857] | [3, 274, 4]     | 167 | 103
        []                 | []              | 167 | 1
    }
}
