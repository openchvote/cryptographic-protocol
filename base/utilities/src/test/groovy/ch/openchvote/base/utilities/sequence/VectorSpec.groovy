/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.sequence

import ch.openchvote.base.utilities.set.IntSet
import ch.openchvote.base.utilities.tools.Math
import spock.lang.Specification
import spock.lang.Unroll

class VectorSpec extends Specification {
    def "Creating new vector with Builder()"() {
        when:
        def addVectorBuilder = new Vector.Builder()
        input.each {
            addVectorBuilder.add(it)
        }
        def addVector = addVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert addVector.getValue(i + 1) == input.get(i)
        }
        addVector.getLength() == input.size()
        addVector.getMinIndex() == 1
        addVector.getMaxIndex() == input.size()

        when:
        def setVectorBuilder = new Vector.Builder()
        for (int i = 0; i < input.size(); i++) {
            setVectorBuilder.set(i + 1, input.get(i))
        }
        def setVector = setVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert setVector.getValue(i + 1) == input.get(i)
        }
        setVector.getLength() == input.size()
        setVector.getMinIndex() == 1
        setVector.getMaxIndex() == input.size()

        where:
        input                                                        | _
        [1, 2, 3, 4]                                                 | _
        ["one", "two", "three"]                                      | _
        [BigInteger.valueOf(64728), null, BigInteger.valueOf(74623)] | _
        [null, null, BigInteger.valueOf(74623)]                      | _
    }

    def "Creating new vector with Builder(int length)"() {
        when:
        def addVectorBuilder = new Vector.Builder(input.size())
        input.each {
            addVectorBuilder.add(it)
        }
        def addVector = addVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert addVector.getValue(i + 1) == input.get(i)
        }
        addVector.getLength() == input.size()
        addVector.getMinIndex() == 1
        addVector.getMaxIndex() == input.size()

        when:
        def setVectorBuilder = new Vector.Builder(input.size())
        for (int i = 0; i < input.size(); i++) {
            setVectorBuilder.set(i + 1, input.get(i))
        }
        def setVector = setVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert setVector.getValue(i + 1) == input.get(i)
        }
        setVector.getLength() == input.size()
        setVector.getMinIndex() == 1
        setVector.getMaxIndex() == input.size()

        where:
        input                                                        | _
        [1, 2, 3, 4]                                                 | _
        ["one", "two", "three"]                                      | _
        [BigInteger.valueOf(64728), null, BigInteger.valueOf(74623)] | _
        [null, null, BigInteger.valueOf(74623)]                      | _
    }

    def "Creating new vector with Builder(int length) and fill(T value)"() {
        when:
        def addVectorBuilder = new Vector.Builder(length)

        addVectorBuilder.fill(input)
        def addVector = addVectorBuilder.build()

        then:
        for (int i = 0; i < length; i++) {
            assert addVector.getValue(i + 1) == expected.get(i)
        }
        addVector.getLength() == length
        addVector.getMinIndex() == 1
        addVector.getMaxIndex() == length

        where:
        input                     | length | expected
        1                         | 4      | [1, 1, 1, 1]
        "two"                     | 3      | ["two", "two", "two"]
        BigInteger.valueOf(64728) | 2      | [BigInteger.valueOf(64728), BigInteger.valueOf(64728)]
        null                      | 1      | [null]
    }

    def "Creating new vector with Builder(int minIndex, int maxIndex)"() {
        when:
        def addVectorBuilder = new Vector.Builder(minIndex, maxIndex)
        input.each {
            addVectorBuilder.add(it)
        }
        def addVector = addVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert addVector.getValue(i + minIndex) == input.get(i)
        }
        addVector.getLength() == input.size()
        addVector.getMinIndex() == minIndex
        addVector.getMaxIndex() == maxIndex

        when:
        def setVectorBuilder = new Vector.Builder(minIndex, maxIndex)
        for (int i = 0; i < input.size(); i++) {
            setVectorBuilder.set(i + minIndex, input.get(i))
        }
        def setVector = setVectorBuilder.build()

        then:
        for (int i = 0; i < input.size(); i++) {
            assert setVector.getValue(i + minIndex) == input.get(i)
        }
        setVector.getLength() == input.size()
        setVector.getMinIndex() == minIndex
        setVector.getMaxIndex() == maxIndex

        where:
        input                                                        | minIndex | maxIndex
        [1, 2, 3, 4]                                                 | 4        | 7
        ["one", "two", "three"]                                      | 2        | 4
        [BigInteger.valueOf(64728), null, BigInteger.valueOf(74623)] | 26       | 28
        [null, null, BigInteger.valueOf(74623)]                      | 23       | 25
    }

    def "Creating new vector with Builder(int minIndex, int maxIndex) and fill(T value)"() {
        when:
        def addVectorBuilder = new Vector.Builder(minIndex, maxIndex)
        addVectorBuilder.fill(input)
        def addVector = addVectorBuilder.build()

        then:
        for (int i = 0; i < length; i++) {
            assert addVector.getValue(i + minIndex) == expected.get(i)
        }
        addVector.getLength() == length
        addVector.getMinIndex() == minIndex
        addVector.getMaxIndex() == maxIndex

        where:
        input                     | length | minIndex | maxIndex | expected
        1                         | 4      | 4        | 7        | [1, 1, 1, 1]
        "two"                     | 3      | 8        | 10       | ["two", "two", "two"]
        BigInteger.valueOf(64728) | 2      | 41       | 42       | [BigInteger.valueOf(64728), BigInteger.valueOf(64728)]
        null                      | 1      | 1        | 1        | [null]
    }

    def "Creating new vector with of(V[] values)"() {
        when:
        def vector = Vector.of(input.toArray())

        then:
        for (int i = 0; i < input.size(); i++) {
            assert vector.getValue(i + 1) == input.get(i)
        }
        vector.getLength() == input.size()
        vector.getMinIndex() == 1
        vector.getMaxIndex() == input.size()

        where:
        input                                                                               | _
        [1, 2, 3, 4]                                                                        | _
        ["one", "two", "three"]                                                             | _
        [BigInteger.valueOf(64728), BigInteger.valueOf(9475763), BigInteger.valueOf(74623)] | _
    }

    @SuppressWarnings('GroovyAssignabilityCheck')
    @Unroll
    def "map(Function<? super V, ? extends W> function) should apply the given function too each element of the vector"() {
        def vector = Vector.of(input)
        def expected = Vector.of(eInput)
        expect:
        vector.map(function) == expected
        vector.map(function).hashCode() == expected.hashCode()

        where:
        input                                                                             | function                                    | eInput
        [1, 2, 3, 4, 5]                                                                   | { n -> Math.intPowerOfTwo(n) }        | [2, 4, 8, 16, 32]
        [BigInteger.valueOf(123), BigInteger.valueOf(456), null, BigInteger.valueOf(789)] | { n -> n.add(BigInteger.ONE) } | [BigInteger.valueOf(124), BigInteger.valueOf(457), null, BigInteger.valueOf(790)]
        ["test", null, "null", "hello", "lorem"]                                          | { s -> s.replace("e", "") }        | ["tst", null, "null", "hllo", "lorm"]
    }

    @SuppressWarnings('GroovyAssignabilityCheck')
    @Unroll
    def "map(Function<? super V, ? extends W> function, W nullValue) should apply the given function too each element of the vector and insert nullValue for null"() {
        def vector = Vector.of(input.toArray())
        def expected = Vector.of(eInput.toArray())
        def result = vector.map(function, false)
        expect:
        expected == result

        where:
        input                                                                             | function                                    | eInput
        [1, 2, 3, 4, 5]                                                                   | { n -> Math.intPowerOfTwo(n) }        | [2, 4, 8, 16, 32]
        [BigInteger.valueOf(123), BigInteger.valueOf(456), null, BigInteger.valueOf(789)] | { n -> n.add(BigInteger.ONE) } | [BigInteger.valueOf(124), BigInteger.valueOf(457), null, BigInteger.valueOf(790)]
        ["test", null, "null", "hello", "lorem"]                                          | { s -> s.replace("e", "") }        | ["tst", null, "null", "hllo", "lorm"]
    }

    def "select(IntSet indexSet) should construct a new vector by selecting the values for the given indices in the IntSet"() {
        def vector = Vector.of(input.toArray())
        def expected = Vector.of(eInput.toArray())
        def selectionSet = IntSet.of(selection as Integer[])
        def result = vector.select(selectionSet)
        expect:
        expected == result

        where:
        input                                                                             | selection | eInput
        [1, 2, 3, 4, 5]                                                                   | [2, 4]    | [2, 4]
        [BigInteger.valueOf(123), BigInteger.valueOf(456), null, BigInteger.valueOf(789)] | [4, 2, 1] | [BigInteger.valueOf(123), BigInteger.valueOf(456), BigInteger.valueOf(789)]
        ["test", null, "null", "hello", "lorem"]                                          | [4]       | ["hello"]
    }

    def "isUniform() should return if all the elements of a vector are the same"() {
        def vector = Vector.of(input.toArray())

        expect:
        vector.isConstant() == expected

        where:
        input                                                                       | expected
        [1, 2, 3, 4, 5]                                                             | false
        [BigInteger.valueOf(123), BigInteger.valueOf(123), BigInteger.valueOf(123)] | true
        ["test", null, "null", "hello", "lorem"]                                    | false
        [1, 1, 1, 1]                                                                | true
        [null, null, null]                                                          | true
    }

    def "toStream()"() {
        def vector = Vector.of(input.toArray())

        expect:
        vector.toStream().toArray() == expected as Object[]

        where:

        input                                                                       | expected
        [1, 2, 3, 4, 5]                                                             | [1, 2, 3, 4, 5]
        [BigInteger.valueOf(123), BigInteger.valueOf(456), BigInteger.valueOf(789)] | [BigInteger.valueOf(123), BigInteger.valueOf(456), BigInteger.valueOf(789)]
        ["test", null, "null", "hello", "lorem"]                                    | ["test", null, "null", "hello", "lorem"]
        [1, 1, 1, 1]                                                                | [1, 1, 1, 1]
        [null, null, null]                                                          | [null, null, null]
    }

    def "V[] toArray()"() {
        def vector = Vector.of(input.toArray())

        expect:
        vector.toArray() == expected as Object[]

        where:

        input                                                                       | expected
        [1, 2, 3, 4, 5]                                                             | [1, 2, 3, 4, 5]
        [BigInteger.valueOf(123), BigInteger.valueOf(456), BigInteger.valueOf(789)] | [BigInteger.valueOf(123), BigInteger.valueOf(456), BigInteger.valueOf(789)]
        ["test", null, "null", "hello", "lorem"]                                    | ["test", null, "null", "hello", "lorem"]
        [1, 1, 1, 1]                                                                | [1, 1, 1, 1]
        [null, null, null]                                                          | [null, null, null]
    }

    def "iterator()"() {
        def vector = Vector.of(input.toArray())

        expect:
        def itr = vector.iterator()
        for (def i = 0; i < expected.size(); i++) {
            assert itr.hasNext()
            assert itr.next() == expected.get(i)
        }
        !itr.hasNext()

        where:

        input                                                                       | expected
        [1, 2, 3, 4, 5]                                                             | [1, 2, 3, 4, 5]
        [BigInteger.valueOf(123), BigInteger.valueOf(456), BigInteger.valueOf(789)] | [BigInteger.valueOf(123), BigInteger.valueOf(456), BigInteger.valueOf(789)]
        ["test", null, "null", "hello", "lorem"]                                    | ["test", null, "null", "hello", "lorem"]
        [1, 1, 1, 1]                                                                | [1, 1, 1, 1]
        [null, null, null]                                                          | [null, null, null]
    }

    def "toString()"() {
        def vector = Vector.of(input.toArray())

        expect:
        vector.toString() == expected

        where:

        input                                                                       | expected
        [1, 2, 3, 4, 5]                                                             | "[1,2,3,4,5]"
        [BigInteger.valueOf(123), BigInteger.valueOf(456), BigInteger.valueOf(789)] | "[123,456,789]"
        ["test", null, "null", "hello", "lorem"]                                    | "[test,null,null,hello,lorem]"
        [1, 1, 1, 1]                                                                | "[1,1,1,1]"
        [null, null, null]                                                          | "[null,null,null]"
    }

    def "sort(util.Vector<V> vector)"() {
        def vector = Vector.of((List<Comparable>) eInput)
        def expected = Vector.of(eInput)
        expect:
        Vector.sort(vector) == expected

        where:

        input                                                                       | eInput
        [1, 2, 3, 4, 5]                                                             | [1, 2, 3, 4, 5]
        [BigInteger.valueOf(1243), BigInteger.valueOf(456), BigInteger.valueOf(91)] | [BigInteger.valueOf(91), BigInteger.valueOf(456), BigInteger.valueOf(1243)]
        [9, 8, 7, 6, 1]                                                             | [1, 6, 7, 8, 9]
        [22, 22, 45, 1, 22, 45, 1]                                                  | [1, 1, 22, 22, 22, 45, 45]
    }

}
