/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.tuples

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Shared
import spock.lang.Specification

class SextupleSpec extends Specification {

    @Shared
    def default_First
    @Shared
    def default_Second
    @Shared
    def default_Third
    @Shared
    def default_Fourth
    @Shared
    def default_Fifth
    @Shared
    def default_Sixth

    def setupSpec() {
        default_First = String.valueOf(1)
        default_Second = String.valueOf(2)
        default_Third = String.valueOf(3)
        default_Fourth = String.valueOf(4)
        default_Fifth = String.valueOf(5)
        default_Sixth = String.valueOf(6)
    }

    def "Constructor throws UtilityException for case: #caseDesc"() {
        when:
        new Sextuple(first, second, third, fourth, fifth, sixth)

        then:
        thrown(UtilityException)

        where:
        caseDesc         | first         | second         | third         | fourth         | fifth         | sixth
        "first is null"  | null          | default_Second | default_Third | default_Fourth | default_Fifth | default_Sixth
        "second is null" | default_First | null           | default_Third | default_Fourth | default_Fifth | default_Sixth
        "third is null"  | default_First | default_Second | null          | default_Fourth | default_Fifth | default_Sixth
        "fourth is null" | default_First | default_Second | default_Third | null           | default_Fifth | default_Sixth
        "fifth is null"  | default_First | default_Second | default_Third | default_Fourth | null          | default_Sixth
        "sixth is null"  | default_First | default_Second | default_Third | default_Fourth | default_Fifth | null
    }

    def "Test constructor and getter methods for the elements"() {
        given:
        def sextuple = new Sextuple(first, second, third, fourth, fifth, sixth)

        expect:
        sextuple.getFirst() == first
        sextuple.getSecond() == second
        sextuple.getThird() == third
        sextuple.getFourth() == fourth
        sextuple.getFifth() == fifth
        sextuple.getSixth() == sixth

        where:
        first                 | second                | third                 | fourth                | fifth                 | sixth
        "first"               | "second"              | "third"               | "fourth"              | "fifth"               | "sixth"
        1                     | 2                     | 3                     | 4                     | 5                     | 6
        BigInteger.valueOf(1) | BigInteger.valueOf(2) | BigInteger.valueOf(3) | BigInteger.valueOf(4) | BigInteger.valueOf(5) | BigInteger.valueOf(6)
    }

    @SuppressWarnings('ChangeToOperator')
    def "equals should return true if all pairs of elements of two sextuples are the same and false otherwise"() {
        given:
        def sextuple1 = new Sextuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5))
        def sextuple2 = new Sextuple(elems2.get(0), elems2.get(1), elems2.get(2), elems2.get(3), elems2.get(4), elems2.get(5))

        expect:
        //noinspection ChangeToOperator
        sextuple1.equals(sextuple2) == expected
        (sextuple1.hashCode() == sextuple2.hashCode()) == expected

        where:
        elems1                                                   | elems2                                                   | expected
        ["first", "second", "third", "fourth", "fifth", "sixth"] | ["first", "second", "third", "fourth", "fifth", "sixth"] | true
        ["wrong", "second", "third", "fourth", "fifth", "sixth"] | ["first", "second", "third", "fourth", "fifth", "sixth"] | false
        ["first", "wrong", "third", "fourth", "fifth", "sixth"]  | ["first", "second", "third", "fourth", "fifth", "sixth"] | false
        ["first", "second", "wrong", "fourth", "fifth", "sixth"] | ["first", "second", "third", "fourth", "fifth", "sixth"] | false
        ["first", "second", "third", "wrong", "fifth", "sixth"]  | ["first", "second", "third", "fourth", "fifth", "sixth"] | false
        ["first", "second", "third", "fourth", "wrong", "sixth"] | ["first", "second", "third", "fourth", "fifth", "sixth"] | false
        ["first", "second", "third", "fourth", "fifth", "wrong"] | ["first", "second", "third", "fourth", "fifth", "sixth"] | false
    }

    def "toString()"() {
        given:
        def sextuple = new Sextuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5))

        expect:
        sextuple.toString() == expected

        where:
        elems1                                                   | expected
        ["first", "second", "third", "fourth", "fifth", "sixth"] | "(first,second,third,fourth,fifth,sixth)"
        [1, 2, 3, 4, 5, 6]                                       | "(1,2,3,4,5,6)"
    }

    def "toStream() should return a Stream containing all elements in the sextuple in the correct order"() {
        given:
        def sextuple = new Sextuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5))

        expect:
        sextuple.toStream().toList() == expected

        where:
        elems1                                                   | expected
        ["first", "second", "third", "fourth", "fifth", "sixth"] | ["first", "second", "third", "fourth", "fifth", "sixth"]
        [1, 2, 3, 4, 5, 6]                                       | [1, 2, 3, 4, 5, 6]
    }

}
