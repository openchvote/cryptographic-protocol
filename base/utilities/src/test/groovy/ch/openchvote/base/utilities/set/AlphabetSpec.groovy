/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.utilities.set

import ch.openchvote.base.utilities.UtilityException
import spock.lang.Specification
import spock.lang.Unroll

class AlphabetSpec extends Specification {

    @Unroll
    def "Constructor(String s) test constructor with input '#a'"() {
        when: "creating a new Alphabet with input a"
        def A = new Alphabet(a)

        then: "length of the alphabet should be equal to the length of a"
        A.size == a.length()

        and: "rank of each character should be the same as its index in a"
        for (String c : a) {
            A.getRank(c.charAt(0)) == a.indexOf(c.toString())
        }

        and: "A.getCharacters should be equal to a"
        A.getCharacters() == a

        where:
        a                            | _
        "01"                         | _
        "01234567"                   | _
        "0123456789"                 | _
        "0123456789ABCDEF"           | _
        "abcdefghijklmnopqrstuvwxyz" | _
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | _
        "+#%&/[]!?"                  | _
    }

    @SuppressWarnings('GroovyUnusedAssignment')
    def "Constructor(String S) throws UtilityException case: #a"() {
        when: "creating a new Alphabet with input a"
        def A = new Alphabet(a)

        then: "should result in UtilityException"
        thrown(UtilityException)

        where:
        a              | _
        ""             | _
        "AA"           | _
        "012345678910" | _
        null           | _
    }

    def "String getCharacters(): returns the same String as used while creating the alphabet"() {
        given: " a new Alphabet with input a"
        def A = new Alphabet(a)

        expect: "A.getCharacters should be equal to a"
        A.getCharacters() == a

        where:
        a                            | _
        "01"                         | _
        "01234567"                   | _
        "0123456789"                 | _
        "0123456789ABCDEF"           | _
        "abcdefghijklmnopqrstuvwxyz" | _
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | _
        "+#%&/[]!?"                  | _
    }

    def "getSize(): should return the same length as the length of the string that was used for creating the alphabet "() {
        given: "a new Alphabet with input a"
        def A = new Alphabet(a)

        expect: "length of the alphabet should be equal to the length of a"
        A.size == expected

        where:
        a                            | expected
        "01"                         | 2
        "01234567"                   | 8
        "0123456789"                 | 10
        "0123456789ABCDEF"           | 16
        "abcdefghijklmnopqrstuvwxyz" | 26
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | 26
        "+#%&/[]!?"                  | 9
    }

    @Unroll
    def "contains(Character c): alphabet #s should contain #elements and not contain #notElements"() {
        given: "a new Alphabet with input s"
        def A = new Alphabet(s)

        expect: "A should contain all characters in #elements"
        for (String c : elements) {
            assert A.contains(c.charAt(0))
        }

        and: "A should not contain any characters in #notElements"
        for (String c : notElements) {
            assert !A.contains(c.charAt(0))
        }

        where:
        s                            | elements                                                                         | notElements
        "01"                         | '0'..'1'                                                                         | '2'..'9'
        "01234567"                   | '0'..'7'                                                                         | '8'..'9'
        "0123456789"                 | '0'..'9'                                                                         | 'a'..'z'
        "0123456789ABCDEF"           | ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'] | 'G'..'Z'
        "abcdefghijklmnopqrstuvwxyz" | 'a'..'z'                                                                         | '0'..'9'
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | 'A'..'Z'                                                                         | '0'..'9'
        "+#%&/[]!?"                  | ['+', '#', '%', '&', '/', '[', ']', '!', '?']                                    | '0'..'9'
    }

    def "containsAll(String s) should return #expected for string  #s and alphabet #a"() {
        given: "a new Alphabet with input a"
        def A = new Alphabet(a)

        expect: "containsAll should return #expected for input #s"
        A.containsAll(s) == expected

        where:
        a                            | s          | expected
        "01"                         | "101"      | true
        "01234567"                   | "123"      | true
        "0123456789"                 | "123"      | true
        "0123456789ABCDEF"           | "123ABC"   | true
        "abcdefghijklmnopqrstuvwxyz" | "abcd"     | true
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | "ABCD"     | true
        "+#%&/[]!?"                  | "!?"       | true
        "01"                         | "123ABC!?" | false
        "01234567"                   | "123ABC!?" | false
        "0123456789"                 | "123ABC!?" | false
        "0123456789ABCDEF"           | "123ABC!?" | false
        "abcdefghijklmnopqrstuvwxyz" | "123ABC!?" | false
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | "123ABC!?" | false
        "+#%&/[]!?"                  | "123ABC!?" | false
    }

    def "getRank(Character c) returns rank #expected for char #c and Alphabet #a"() {
        given: "creating a new Alphabet with input a"
        def A = new Alphabet(a)

        expect: "containsAll should return #expected for input #s"
        A.getRank(c as char) == expected

        where:
        a                            | c   | expected
        "01"                         | "0" | 0
        "01234567"                   | "1" | 1
        "0123456789"                 | "3" | 3
        "0123456789ABCDEF"           | "A" | 10
        "abcdefghijklmnopqrstuvwxyz" | "b" | 1
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | "Z" | 25
        "+#%&/[]!?"                  | "!" | 7
    }
    @Unroll
    def "getRank(Character c) throws UtilityException for characters not in A"() {
        given: "creating a new Alphabet with input a"
        def A = new Alphabet(a)

        when: "running getRank for a character outside the Alphabet"
        A.getRank(c as char)

        then: "getRank throws an UtilityException"
        thrown(UtilityException)

        where:
        a                            | c
        "01"                         | "2"
        "01234567"                   | "9"
        "0123456789"                 | "A"
        "0123456789ABCDEF"           | "G"
        "abcdefghijklmnopqrstuvwxyz" | "0"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | "0"
        "+#%&/[]!?"                  | "0"
    }

    def "getChar(int rank) returns char #expected for rank #rank "() {
        given: "creating a new Alphabet with input a"
        def A = new Alphabet(a)

        expect: "getCar should return #expected for given rank"
        A.getChar(rank) == expected as char

        where:
        a                            | rank | expected
        "01"                         | 0    | "0"
        "01234567"                   | 1    | "1"
        "0123456789"                 | 3    | "3"
        "0123456789ABCDEF"           | 10   | "A"
        "abcdefghijklmnopqrstuvwxyz" | 1    | "b"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | 25   | "Z"
        "+#%&/[]!?"                  | 7    | "!"
    }

    def "getChar(int rank) throws UtilityException for rank out of bound"() {
        given: "creating a new Alphabet with input a"
        def A = new Alphabet(a)

        when:
        A.getChar(rank)

        then:
        thrown(UtilityException)

        where:
        a                            | rank
        "01"                         | -1
        "01234567"                   | -1
        "0123456789"                 | -1
        "0123456789ABCDEF"           | -1
        "abcdefghijklmnopqrstuvwxyz" | -1
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | -1
        "+#%&/[]!?"                  | -1
        "01"                         | 2
        "01234567"                   | 8
        "0123456789"                 | 10
        "0123456789ABCDEF"           | 16
        "abcdefghijklmnopqrstuvwxyz" | 26
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | 26
        "+#%&/[]!?"                  | 9
    }

    def "addCharacter(Char c) should return add the character c to alphabet #a"() {
        given: "a new Alphabet with input a"
        def A = new Alphabet(a)
        A = A.addCharacter(c as char)

        expect: " alphabet to contain added character"
        A.contains(c as char)

        where:
        a                            | c
        "01"                         | '2'
        "01234567"                   | '9'
        "0123456789"                 | '0'
        "0123456789ABCDEF"           | 'G'
        "abcdefghijklmnopqrstuvwxyz" | '0'
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" | '?'
        "+#%&/[]!?"                  | 'R'
    }

    def "getCharacters() throws UtilityException for UCS alphabet"() {
        given:
        def ucs = Alphabet.UCS

        when:
        ucs.getCharacters()

        then:
        thrown(UtilityException)
    }

    def "getSize() throws UtilityException for UCS alphabet"() {
        given:
        def ucs = Alphabet.UCS

        when:
        ucs.getSize()

        then:
        thrown(UtilityException)
    }

    def "contains() for UCS alphabet"() {
        given:
        def ucs = Alphabet.UCS

        expect:
        ucs.contains(x as char)

        where:
        x   | _
        "1" | _
        "A" | _
        "Z" | _
        "!" | _
        ":" | _
    }

    def "getRank() for UCS alphabet"() {
        given:
        def ucs = Alphabet.UCS

        expect:
        ucs.getRank(x as char) == x as char

        where:
        x   | _
        "1" | _
        "A" | _
        "Z" | _
        "!" | _
        ":" | _
    }

    def "getChar() for UCS alphabet"() {
        given:
        def ucs = Alphabet.UCS

        expect:
        ucs.getChar((x as char) as int) == x as char

        where:
        x   | _
        "1" | _
        "A" | _
        "Z" | _
        "!" | _
        ":" | _
    }

}
