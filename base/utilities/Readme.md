# Utilities

This module provides a collection of utility classes, which implement various mathematical concepts such as vectors,
matrices, tuples, sets, or modular arithmetic. It also offers convenient wrapper classes for some cryptographic
primitives.

The goal of these utility classes is to increase the convenience of programming mathematical and cryptographic
algorithms in Java. Many of the available classes are generic and can be nested, which offers strong type safety
even for complex mathematical objects like:

```
var Matrix<Pair<BigInteger, ByteArray>> someMatrix = ...;
var Triple<BigInteger, Vector<String>, Pair<Integer, ByteArray>> someTriple  = ...;
var Pair<Matrix<BigInteger>, Matrix<Triple<String, Integer, ByteArray>>> somePairOfMatrices = ...;
:
```

Providing these classes is crucial for implementing the CHVote pseudocode algorithms as closely as possible to
their description in the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325) document.

## Installation of Maven Artefact

This module can be built as follows from its root:

```console 
mvn clean install
-- a lot of output
```

## Maven Dependencies

As shown in the following dependency tree, this module is independent of any other module of the OpenCHVote
project. However, there is an optional dependency to
the [Verificatum Multiplicative Groups Library for Java (VMGJ)](https://github.com/verificatum/verificatum-vmgj), for
which [this license](LICENSE_VMGLJ.md) applies. At runtime, a wrapper tries to resolve the dependency to VMGJ. No harm
occurs if the dependency can not be resolved, but a warning is displayed in that case. Without VMGJ, some computations
are
less efficient.

```console
mvn dependency:tree -Dscope=compile -Dverbose=false
[INFO] --- dependency:3.6.0:tree (default-cli) @ utilities ---
[INFO] io.gitlab.openchvote:utilities:jar:2.4
[INFO] \- com.verificatum:vmgj:jar:1.2.2:compile
```

An additional test dependency exists to the `org.spockframework:spock-core` testing framework, which
itself has dependencies to `org.apache.groovy:groovy` and `org.junit.platform:junit-platform-engine`.
