/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.party.EventContext;

/**
 * A party participating in a protocol requires a persistence service to persist the current {@link EventContext} of a
 * given election event. Usually, for making the transitions from one state to another atomic, changes to an event
 * context should be synchronized. Loading and saving an event context should therefore be embraced by locking and
 * unlocking the event. Classes implementing this interface provide methods for exactly this purpose.
 */
public interface PersistenceService extends Service {

    /**
     * Checks if an event context is available for a given event id.
     *
     * @param eventId The event id
     * @return {@code true}, if an event context is available for the given event id, {@code false} otherwise.
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    boolean hasContext(String eventId);

    /**
     * Returns the persisted event context of an election event.
     *
     * @param eventId The event id
     * @return The persisted event context
     */
    EventContext<?, ?> loadContext(String eventId);

    /**
     * Persists the event context of an election event.
     *
     * @param eventContext The event context to be saved
     */
    void saveContext(EventContext<?, ?> eventContext);

    /**
     * Locks the event context of an election event for allowing changes to its context.
     *
     * @param eventId The event id
     */
    void lockContext(String eventId);

    /**
     * Unlocks the event context of an election event.
     *
     * @param eventId The event id
     */
    void unlockContext(String eventId);

    /**
     * Seals the event context of an election event to disallow further updates using
     * {@link PersistenceService#saveContext(EventContext)}.
     *
     * @param eventId The event id
     */
    void sealContext(String eventId);

}
