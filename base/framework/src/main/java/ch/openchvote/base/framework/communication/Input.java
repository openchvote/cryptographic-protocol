/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.communication;

import ch.openchvote.base.framework.interfaces.UserInterface;

/**
 * Instances of this class represent the user input to be transmitted over the user's {@link UserInterface}. All fields
 * are of type {@link String}. The class has no methods other than the getter methods for accessing the fields.
 */
public final class Input extends Communication {

    /**
     * Constructs a new input object from the given parameters.
     *
     * @param eventId       The given event id
     * @param contentId     The input's content id
     * @param contentString The input's content string
     */
    public Input(String eventId, String contentId, String contentString) {
        super(eventId, contentId, contentString);
    }

    @Override
    public String toString() {
        return String.format("Input={eventID=%s, contentId=%s}", this.eventId, this.contentId);
    }

}
