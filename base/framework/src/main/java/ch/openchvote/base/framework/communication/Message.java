/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.communication;

import ch.openchvote.base.framework.services.MessagingService;
import ch.openchvote.base.framework.interfaces.Identifiable;

import java.util.UUID;

/**
 * Instances of this class represent the signed messages to be transmitted over the {@link MessagingService}. All fields
 * are of type {@link String}. The class has no methods other than the getter methods for accessing the fields.
 */
public final class Message extends Communication.Signed implements Identifiable {

    private final String receiverId;
    private final String id;

    /**
     * Constructs a new message object from the given parameters.
     *
     * @param eventId       The given event id
     * @param senderId      The given sender id
     * @param receiverId    The given receiver id
     * @param contentId     The message's content id
     * @param contentString The message's content string
     * @param signature     The message's signature
     */
    public Message(String eventId, String senderId, String receiverId, String contentId, String contentString, String signature) {
        super(eventId, contentId, contentString, senderId, signature);
        this.receiverId = receiverId;
        this.id = UUID.randomUUID().toString();
    }

    /**
     * Returns the message's sender id.
     *
     * @return The sender id
     */
    public String getSenderId() {
        return super.getSignerId();
    }

    /**
     * Returns the message's receiver id.
     *
     * @return The receiver id
     */
    public String getReceiverId() {
        return this.receiverId;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String toString() {
        var shortId = this.id.substring(0, 4) + "..." + this.id.substring(this.id.length() - 4);
        return String.format("Message={id=%s, eventId=%s, senderId=%s, receiverId=%s, contentId=%s}", shortId, this.eventId, this.getSignerId(), this.receiverId, this.contentId);
    }

}
