/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.exceptions.SerializerException;
import ch.openchvote.base.framework.party.EventData;

/**
 * This is the parent interface for two specific serialization services {@link SerializationService.Content} and
 * {@link SerializationService.EventContext}. It defines the generic type of the objects to serialize and two methods
 * for conducting the serialization and deserialization.
 *
 * @param <T> The generic type of the objects to serialize
 */
public interface SerializationService<T> extends Service {

    /**
     * Serializes the given object and returns the resulting string that represents the object.
     *
     * @param object The object to serialize
     * @return The serialized object
     */
    String serialize(T object);

    /**
     * Deserializes the given string representation and returns the resulting object. The type of the resulting object
     * is given by its class object of type {@code <S>}. The method's generic type parameter {@code <S>} must be equal
     * to or a subtype of the interface's generic type parameter {@code <T>}.
     *
     * @param <S>           The generic type of the object to deserialize
     * @param contentString The serialized object
     * @param clazz         The class of the object to deserialize
     * @return The deserialized object
     * @throws SerializerException if the deserialization process encounters a problem
     */
    <S extends T> S deserialize(String contentString, Class<S> clazz) throws SerializerException;

    /**
     * This interface defines a specific serialization service for objects of type
     * {@link ch.openchvote.base.framework.communication.Content}.
     */
    interface Content extends SerializationService<ch.openchvote.base.framework.communication.Content> {

    }

    /**
     * This interface defines a specific serialization service for objects of type
     * {@link ch.openchvote.base.framework.party.EventContext}.
     */
    interface EventContext extends SerializationService<ch.openchvote.base.framework.party.EventContext<? extends EventData, ? extends EventData>> {

    }

}
