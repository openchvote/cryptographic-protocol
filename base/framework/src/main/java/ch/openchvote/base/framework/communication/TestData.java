/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.communication;

/**
 * Test data objects are published by the parties involved in a protocol execution to provide additional input to some
 * test code, for example using JUnit or other testing frameworks.
 */
public final class TestData extends Communication {

    private final String publisherId;

    /**
     * Constructs a new test data object from the given parameters.
     *
     * @param eventId       The given event id
     * @param publisherId   The publisher's id
     * @param contentId     The test data's content id
     * @param contentString The test data's content string
     */
    public TestData(String eventId, String publisherId, String contentId, String contentString) {
        super(eventId, contentId, contentString);
        this.publisherId = publisherId;
    }

    /**
     * Returns the publisher id of the test data.
     *
     * @return The test data's publisher id.
     */
    public String getPublisherId() {
        return this.publisherId;
    }

    @Override
    public String toString() {
        return String.format("TestData={eventId=%s, publisherId=%s, contentId=%s}", this.eventId, this.getPublisherId(), this.contentId);
    }

}
