/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.exceptions.SerializerException;
import ch.openchvote.base.framework.security.KeyGenerator;

/**
 * This service interface defines the basic functionality of an asymmetric encryption scheme. All parameters of the two
 * methods {@link EncryptionService#encrypt(String, String, String)} and
 * {@link EncryptionService#decrypt(String, String, String)} are strings representations of corresponding objects.
 */
public interface EncryptionService extends Service {

    /**
     * Returns the encryption scheme's key generator.
     *
     * @return The encryption scheme's key generator
     */
    KeyGenerator getKeyGenerator();

    /**
     * Returns the name of the encryption algorithm.
     *
     * @return The name of the encryption algorithm
     */
    String getAlgorithmName();

    /**
     * Encrypts a given plaintext with the receiver's public encryption key. The given security level defines the
     * strength of the encryption, for example by using it to select corresponding cryptographic parameters. The
     * plaintext and the resulting ciphertext are strings, as well as the public key.
     *
     * @param plaintext     The plaintext to encrypt
     * @param encryptionKey The public encryption key
     * @param securityLevel The security level of the encryption
     * @return The resulting ciphertext
     * @throws SerializerException if the serializer fails
     */
    String encrypt(String plaintext, String encryptionKey, String securityLevel) throws SerializerException;

    /**
     * Decrypts a given ciphertext with the receiver's private decryption key into the original plaintext. This
     * operation is the inverse of {@link EncryptionService#encrypt(String, String, String)}.
     *
     * @param ciphertext    The ciphertext to decrypt
     * @param decryptionKey The private decryption key
     * @param securityLevel The security level of the encryption
     * @return The resulting plaintext
     * @throws SerializerException if the serializer fails
     */
    String decrypt(String ciphertext, String decryptionKey, String securityLevel) throws SerializerException;

}
