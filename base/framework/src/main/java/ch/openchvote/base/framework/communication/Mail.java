/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.communication;

import ch.openchvote.base.framework.services.MailingService;

/**
 * Instances of this class represent the mails to be transmitted over the {@link MailingService}. All fields are of type
 * {@link String}. The class has no methods other than the getter methods for accessing the fields.
 */
public final class Mail extends Communication {

    private final String senderId;
    private final String receiverId;

    /**
     * Constructs a new mail object from the given parameters.
     *
     * @param eventId       The given event id
     * @param senderId      The given sender id
     * @param receiverId    The given receiver id
     * @param contentId     The mail's content id
     * @param contentString The mail's content string
     */
    public Mail(String eventId, String senderId, String receiverId, String contentId, String contentString) {
        super(eventId, contentId, contentString);
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    /**
     * Returns the sender id.
     *
     * @return The sender id
     */
    public String getSenderId() {
        return this.senderId;
    }

    /**
     * Returns the receiver id.
     *
     * @return The receiver id
     */
    public String getReceiverId() {
        return this.receiverId;
    }

    @Override
    public String toString() {
        return String.format("Mail={eventID=%s, senderId=%s, receiverId=%s, contentId=%s}", this.eventId, this.senderId, this.receiverId, this.contentId);
    }

}
