/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.security.Certificate;
import ch.openchvote.base.framework.security.KeyPair;

import java.util.Optional;

/**
 * This interface defines the basic functionality of a keystore, which can be used for managing key pairs.
 */
public interface KeystoreService extends Service {

    /**
     * Add a new entry to the keystore.
     *
     * @param keyPair The new entry
     */
    void addEntry(KeyPair keyPair);

    /**
     * Returns the public key for a given private key.
     *
     * @param privateKey The given private key
     * @return The corresponding public key
     */
    Optional<String> getPublicKey(String privateKey);

    /**
     * Returns the private key for a given public key.
     *
     * @param publicKey The given public key
     * @return The corresponding private key
     */
    Optional<String> getPrivateKey(String publicKey);

    /**
     * Returns the private key for a given certificate.
     *
     * @param certificate The given certificate
     * @return The corresponding private key
     */
    default Optional<String> getPrivateKey(Certificate certificate) {
        return this.getPrivateKey(certificate.getPublicKey());
    }

}
