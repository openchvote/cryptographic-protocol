/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.communication;

/**
 * Status objects are used by the parties involved in a protocol execution to inform the event coordinator about their
 * current status. It consists of three strings: the event id, the id of the party submitting the status, and the id of
 * the current phase.
 *
 * @param eventId  The event id
 * @param senderId The sender id
 * @param phaseId  The phase id
 * @param type     The status type
 */
public record Status(String eventId, String senderId, String phaseId, Type type) {

    @SuppressWarnings("MissingJavadoc")
    public enum Type {READY, DONE, ERROR, FINAL}

    @Override
    public String toString() {
        return String.format("Status={eventId=%s, senderId=%s, phaseId=%s, type=%s}", this.eventId, this.senderId, this.phaseId, this.type);
    }

}
