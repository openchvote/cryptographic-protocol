/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.exceptions;

/**
 * This class is used for throwing specific exceptions related to an election event. The different types of event
 * exceptions are represented as an internal enum class.
 */
public final class SerializerException extends TypedException {

    /**
     * This enum type defines different types of framework exceptions.
     */
    @SuppressWarnings("MissingJavadoc")
    public enum Type implements TypedException.Type {
        SERIALIZATION,
        DESERIALIZATION;

        @Override
        public String getId() {
            return this.name().toUpperCase();
        }
    }

    /**
     * Creates an instance of this exception for the given parameters.
     *
     * @param type    The type of the exception
     * @param objects The objects causing the exception
     */
    public SerializerException(Type type, Object... objects) {
        super(type, objects);
    }

    /**
     * Creates an instance of this exception for the given parameters.
     *
     * @param type    The type of the exception
     * @param cause   The origin of the exception
     * @param objects The objects causing the exception
     */
    public SerializerException(Type type, Throwable cause, Object... objects) {
        super(type, cause, objects);
    }

}
