/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.communication;

import ch.openchvote.base.framework.services.RequestResponseService;

/**
 * Instances of this class represent the signed responses to be transmitted over the {@link RequestResponseService}. All
 * fields are of type {@link String}. The class has no methods other than the getter methods for accessing the fields.
 */
public final class Response extends Communication.Signed {

    private final String requestId;
    private final String requesterId;

    /**
     * Constructs a new response object from the given parameters.
     *
     * @param eventId       The given event id
     * @param responderId   The given responder id
     * @param requesterId   The given requester id
     * @param requestId     The response's request id
     * @param contentId     The response's content id
     * @param contentString The response's content string
     * @param signature     The response's signature
     */
    public Response(String eventId, String responderId, String requesterId, String requestId, String contentId, String contentString, String signature) {
        super(eventId, contentId, contentString, responderId, signature);
        this.requesterId = requesterId;
        this.requestId = requestId;
    }

    /**
     * Returns the responder id.
     *
     * @return The responder id
     */
    public String getResponderId() {
        return super.getSignerId();
    }

    /**
     * Returns the request id.
     *
     * @return The request id
     */
    public String getRequestId() {
        return this.requestId;
    }

    /**
     * Returns the requester id.
     *
     * @return The requester id
     */
    public String getRequesterId() {
        return this.requesterId;
    }

    @Override
    public String toString() {
        return String.format("Response={eventID=%s, requestId=%s, responderId=%s, requesterId=%s, contentId=%s}", this.eventId, this.requestId, this.getResponderId(), this.requesterId, this.contentId);
    }

}
