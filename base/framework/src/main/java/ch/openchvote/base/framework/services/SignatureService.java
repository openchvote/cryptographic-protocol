/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.base.framework.exceptions.SerializerException;
import ch.openchvote.base.framework.security.KeyGenerator;

/**
 * This interface defines the basic functionality of a signature scheme by providing two methods for signing a given
 * {@link Content} and for verifying the resulting signature. The resulting signature is represented by a string.
 */
public interface SignatureService extends Service {

    /**
     * Returns the signature scheme's key generator.
     *
     * @return The signature scheme's key generator
     */
    KeyGenerator getKeyGenerator();

    /**
     * Returns the name of the signature algorithm.
     *
     * @return The name of the signature algorithm
     */
    String getAlgorithmName();

    /**
     * Signs the given message content using the sender's private signature key. The given security level defines the
     * strength of the signature, for example by using it to select corresponding cryptographic parameters. Two
     * additional elements {@code contentName} and {@code domain} are linked into the signature for making the signature
     * dependent on the message type and on some auxiliary data.
     *
     * @param content       The content to sign
     * @param auxiliaryData Auxiliary data to include in the signature (can be {@code null})
     * @param signatureKey  The sender's private signature key
     * @param securityLevel The security level of the signature
     * @return The generated signature
     * @throws SerializerException if the serializer fails
     */
    String sign(Content content, Object auxiliaryData, String signatureKey, String securityLevel) throws SerializerException;

    /**
     * Verifies a given signature using the sender's public verification key. This operation is the inverse of
     * {@link SignatureService#sign(Content, Object, String, String)}.
     *
     * @param signature       The given signature
     * @param content         The signed content
     * @param auxiliaryData   Auxiliary data included in the signature (can be {@code null})
     * @param verificationKey The sender's public verification key
     * @param securityLevel   The security level of the signature
     * @return {@code true}, if the signature is valid, {@code false} otherwise
     * @throws SerializerException if the serializer fails
     */
    boolean verify(String signature, Content content, Object auxiliaryData, String verificationKey, String securityLevel) throws SerializerException;

}
