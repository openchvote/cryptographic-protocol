/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.party;

import ch.openchvote.base.framework.annotations.party.EncryptionKeys;
import ch.openchvote.base.framework.annotations.party.Role;
import ch.openchvote.base.framework.annotations.party.SignatureKeys;
import ch.openchvote.base.framework.annotations.protocol.Roles;
import ch.openchvote.base.framework.exceptions.CommunicationException;
import ch.openchvote.base.framework.exceptions.EventContextException;
import ch.openchvote.base.framework.exceptions.FrameworkException;
import ch.openchvote.base.framework.exceptions.StateException;
import ch.openchvote.base.framework.interfaces.UserInterface;
import ch.openchvote.base.framework.protocol.Phase;
import ch.openchvote.base.framework.protocol.Protocol;
import ch.openchvote.base.framework.security.Certificate;
import ch.openchvote.base.framework.communication.*;
import ch.openchvote.base.framework.services.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static java.lang.System.Logger.Level.TRACE;
import static java.lang.System.Logger.Level.WARNING;

/**
 * This is a base class for the parties participating in a protocol. It provides a method for loading service objects
 * when they are needed. Once loaded, these service objects are stored in a map to avoid reloading them multiple times.
 * Some of the service are needed to establish communication links to other parties. These services must be compatible
 * across all implementations used by the parties involved. Services that are needed locally, for example for storing
 * cryptographic keys or for persisting the data accumulated during a protocol run, can be implemented individually.
 * This class provides a collection of convenience methods for using these services. In addition to these convenience
 * methods, the class implements the methods that are required by the different communication services for receiving the
 * transmitted messages. These methods are defined in corresponding interfaces that are implemented by this class.
 */
public abstract class Party implements EventService.Subscriber, MailingService.Subscriber, MessagingService.Subscriber, RequestResponseService.Requester, RequestResponseService.Responder, UserInterface.User, UserInterface.Client, PublicationService.Subscriber, SelfActivationService.Consumer {

    // the party's unique id
    private final String id;

    // the party's logger
    private final System.Logger logger;

    // a map of service objects
    protected final Map<String, Service> services;

    // protected constructor for internal use in subclasses
    protected Party(String id) {
        this.id = id;
        this.services = new HashMap<>();
        this.logger = System.getLogger(this.getPrintName()); // use party name as logger name

        // if necessary generate signature and encryption keys and request certificates for all assigned roles
        this.generateSignatureKeyPair();
        this.generateEncryptionKeyPairs();
    }

    /**
     * Returns a service object for a given service class. These service objects are stored in a map to avoid loading
     * them multiple times.
     *
     * @param service The given service class
     * @param <S>     The generic type of the returned service
     * @return The selected service
     */
    @SuppressWarnings("unchecked")
    public synchronized <S extends Service> S getService(Class<S> service) {
        var serviceId = Service.getId(service);
        return (S) this.services.computeIfAbsent(serviceId, ignored -> Service.load(service));
    }

    /**
     * Calling this method update's the party's current state. The party's event context, which stores the current
     * state, and the class representing the new state are given as arguments. If updating the state leads to a phase
     * transition, a corresponding status notification is sent using the party's {@link EventService}.
     *
     * @param eventContext The party's event context
     * @param stateClass   The class of the new state
     */
    public void updateState(EventContext<?, ?> eventContext, Class<? extends State<?, ?>> stateClass) {

        // get event id, state id, phase ids
        var eventId = eventContext.getEventId();
        var stateId = State.getId(stateClass);

        // update context
        eventContext.setCurrentState(stateId);

        // send status notifications
        this.notifyStatus(eventId, stateId);

        // make logger entry
        var stateName = State.getPrintName(stateClass);
        this.log(TRACE, eventId, "New state reached", stateName);
    }

    /**
     * Sends a {@link Mail} to the given receiver using the party's {@link MailingService}. The content of the mail is
     * given as an object of type {@link Content}.
     *
     * @param eventId    The given event id
     * @param receiverId The given receiver id
     * @param content    The content of the mail
     */
    public void deliverMail(String eventId, String receiverId, Content content) {
        var contentId = content.getId();
        var contentString = this.serializeContent(content);

        // send mail
        var mail = new Mail(eventId, this.id, receiverId, contentId, contentString);
        this.getService(MailingService.Source.class).deliver(mail);

        // make logger entry
        var contentName = content.getPrintName();
        this.log(TRACE, eventId, "Mail sent", contentName, receiverId);
    }

    /**
     * Sends a signed {@link Message} to multiple receivers using the party's {@link MessagingService}. The content of
     * the message is given as an object of type {@link Content}.
     *
     * @param eventId       The given event id
     * @param receiverIds   An iterable collection of receivers ids
     * @param content       The given message content
     * @param auxiliaryData Auxiliary data to include in the signature
     * @param securityLevel The security level used for signing the messages
     */
    public void sendMessage(String eventId, Iterable<String> receiverIds, Content content, Object auxiliaryData, String securityLevel) {
        receiverIds.forEach(receiverId -> {
            if (!this.id.equals(receiverId)) {
                this.sendMessage(eventId, receiverId, content, auxiliaryData, securityLevel);
            }
        });
    }

    /**
     * Sends a signed {@link Message} to a single receiver using the party's {@link MessagingService}. The content of
     * the message is given as an object of type {@link Content}.
     *
     * @param eventId       The given event id
     * @param receiverId    The given receiver id
     * @param content       The given message content
     * @param auxiliaryData Auxiliary data to include in the signature
     * @param securityLevel The security level used for signing the message
     */
    public void sendMessage(String eventId, String receiverId, Content content, Object auxiliaryData, String securityLevel) {
        var contentId = content.getId();
        var contentString = Content.isEncrypted(contentId)
                ? this.encrypt(content, receiverId, securityLevel)
                : this.serializeContent(content);
        var signature = this.sign(content, auxiliaryData, securityLevel);
        var message = new Message(eventId, this.id, receiverId, contentId, contentString, signature);
        this.getService(MessagingService.Source.class).send(message);

        // make logger entry
        var contentName = content.getPrintName();
        this.log(TRACE, eventId, "Message sent", contentName, receiverId);
    }

    /**
     * Sends a signed {@link Publication} to all subscribers of the party's {@link PublicationService}. The content of
     * the publication is given as an object of type {@link Content}.
     *
     * @param eventId       The given event id
     * @param content       The given publication content
     * @param auxiliaryData Auxiliary data to include in the signature
     * @param securityLevel The security level used for signing the message
     */
    public void sendPublication(String eventId, Content content, Object auxiliaryData, String securityLevel) {
        var contentId = content.getId();
        var contentString = this.serializeContent(content);
        var signature = this.sign(content, auxiliaryData, securityLevel);

        // send publication
        var publication = new Publication(eventId, this.id, contentId, contentString, signature);
        this.getService(PublicationService.Source.class).publish(publication);

        // make logger entry
        var contentName = content.getPrintName();
        this.log(TRACE, eventId, "Publication sent", contentName);
    }

    /**
     * Sends a {@link Request} to multiple responders using the party's {@link RequestResponseService}. The content of
     * the request is given as an object of type {@link Content}.
     *
     * @param eventId      The given event id
     * @param responderIds An iterable collection of receiver ids
     * @param content      The given request content
     */
    public void sendRequest(String eventId, Iterable<String> responderIds, Content content) {
        responderIds.forEach(receiverId -> {
            if (!this.id.equals(receiverId)) {
                this.sendRequest(eventId, receiverId, content);
            }
        });
    }

    /**
     * Sends a {@link Request} to a single responder using the party's {@link RequestResponseService}. The content of
     * the request is given as an object of type {@link Content}.
     *
     * @param eventId     The given event id
     * @param responderId The given responder id
     * @param content     The given request content
     */
    public void sendRequest(String eventId, String responderId, Content content) {
        var contentId = content.getId();
        var contentString = this.serializeContent(content);

        // send request
        var request = new Request(eventId, this.id, responderId, contentId, contentString);
        this.getService(RequestResponseService.Source.class).request(request);

        // make logger entry
        var contentName = content.getPrintName();
        this.log(TRACE, eventId, "Request sent", contentName, responderId);
    }

    /**
     * Sends a signed {@link Response} to a single requester using the party's {@link RequestResponseService}. Usually,
     * this happens in response to receiving a {@link Request}. The id of that request is included in the response to
     * allow the proper assignment of the response. The content of the response is given as an object of type
     * {@link Content}.
     *
     * @param eventId       The given event id
     * @param requestId     The id of the request received
     * @param requesterId   The given requester id
     * @param content       The given response content
     * @param auxiliaryData Auxiliary data to include in the signature
     * @param securityLevel The security level used for signing the message
     */
    public void sendResponse(String eventId, String requestId, String requesterId, Content content, Object auxiliaryData, String securityLevel) {
        var contentId = content.getId();
        var contentString = this.serializeContent(content);
        var signature = this.sign(content, auxiliaryData, securityLevel);

        // send response
        var response = new Response(eventId, this.id, requesterId, requestId, contentId, contentString, signature);
        this.getService(RequestResponseService.Target.class).respond(response);

        // make logger entry
        var contentName = content.getPrintName();
        this.log(TRACE, eventId, "Response sent", contentName, requesterId);
    }

    /**
     * Sends some {@link TestData} to all subscribers of the party's {@link TestingService}. The content of the test
     * data is given as an object of type {@link Content}.
     *
     * @param eventId The given event id
     * @param content The given publication content
     */
    public void sendTestData(String eventId, Content content) {
        var contentId = content.getId();
        var contentString = this.serializeContent(content);

        // send test data
        var testData = new TestData(eventId, this.id, contentId, contentString);
        this.getService(TestingService.Source.class).provide(testData);

        // make logger entry
        var contentName = content.getPrintName();
        this.log(TRACE, eventId, "Test data sent", contentName);
    }

    /**
     * Transmits some user {@link Input} to an attached client device using the party's {@link UserInterface}. The
     * content of the user input is given as an object of type {@link Content}.
     *
     * @param client  The attached client device
     * @param eventId The given event id
     * @param content The given input content
     */
    public void enterInput(UserInterface.Client client, String eventId, Content content) {
        var contentId = content.getId();
        var contentString = this.serializeContent(content);

        // submit input
        var input = new Input(eventId, contentId, contentString);
        UserInterface.Source userInterfaceSource = () -> client;
        userInterfaceSource.enter(input);
    }

    /**
     * Displays some {@link Output} to a user using the party's {@link UserInterface}. The content of the output is
     * given as an object of type {@link Content}.
     *
     * @param user    The given user
     * @param eventId The given event id
     * @param content The given input content
     */
    public void displayOutput(UserInterface.User user, String eventId, Content content) {
        var contentId = content.getId();
        var contentString = this.serializeContent(content);

        // submit output
        var output = new Output(eventId, contentId, contentString);
        UserInterface.Target userInterfaceTarget = () -> user;
        userInterfaceTarget.display(output);
    }

    /**
     * Triggers the party's self-activation for the given activation handler using the party's
     * {@link SelfActivationService}.
     *
     * @param eventId           The given event id
     * @param stateId           The given state id
     * @param activationHandler The given activation handler
     */
    public void selfActivate(String eventId, String stateId, Consumer<State<?, ?>> activationHandler) {

        // make logger entry
        var stateName = State.getPrintName(stateId);
        this.log(TRACE, eventId, "Self activation triggered", stateName);

        // handle self activation
        this.getService(SelfActivationService.class).selfActivate(this, eventId, activationHandler);
    }

    /**
     * Extracts and returns the content of the given communication. In the simplest case, it is only necessary to
     * perform the deserialization of the content string, but for an encrypted communication, the content string needs
     * to be decrypted beforehand using the party's {@link EncryptionService}. The content class is given as an
     * additional argument to provide the correct return type. Additionally, the validity of the signature included in
     * the given signed communication is checked using the party's {@link SignatureService}. If checking the signature
     * fails, an exception is thrown.
     *
     * @param contentClass        The given content class
     * @param signedCommunication The given signed communication
     * @param auxiliaryData       Auxiliary data included in the signature
     * @param securityLevel       The given security level
     * @param <T>                 The generic type parameter specifying the type of the returned content object
     * @return The deserialized content
     * @throws CommunicationException if the signature is invalid
     */
    public <T extends Content> T getAndCheckContent(Class<T> contentClass, Communication.Signed signedCommunication, Object auxiliaryData, String securityLevel) throws CommunicationException {
        var content = this.getContent(contentClass, signedCommunication, securityLevel);
        this.checkContent(content, signedCommunication, auxiliaryData, securityLevel);
        return content;
    }

    /**
     * x Extracts and returns the content of the given communication by deserializing the content string included in the
     * communication. The content class is given as an additional argument to provide the correct return type.
     *
     * @param contentClass  The given content class
     * @param communication The given communication
     * @param <T>           The generic type parameter specifying the type of the returned content object
     * @return The content extracted from the given communication
     */
    public <T extends Content> T getContent(Class<T> contentClass, Communication communication) {
        var contentId = communication.getContentId();
        if (contentId.equals(Content.getId(contentClass))) {
            var contentString = communication.getContentString();
            return this.deserializeContent(contentClass, contentString);
        }
        throw new FrameworkException(FrameworkException.Type.INVALID_CONTENT_ID, communication, contentClass);
    }

    /**
     * Returns a string representation of the given event context. The serialization is delegated to the party's event
     * context serialization service.
     *
     * @param eventContext The given event context
     * @return A string representation of the given event context
     */
    public String serializeEventContext(EventContext<?, ?> eventContext) {
        var serializerService = this.getService(SerializationService.EventContext.class);
        return serializerService.serialize(eventContext);
    }

    /**
     * Generic method for reconstructing the event context from a given string representation generated from
     * {@link Party#serializeEventContext(EventContext)}. The deserialization is delegated to the party's event context
     * serialization service. The type of the resulting event context is determined based on the given protocol id. An
     * exception is thrown if the deserialization fails for any reason.
     *
     * @param protocolId         The given protocol id
     * @param eventContextString The given string representation of the event context
     * @param <T>                The generic type of the returned event context
     * @param <PD>               The generic type of the event context's public data
     * @param <SD>               The generic type of the event context's secret data
     * @return The deserialized event context
     */
    @SuppressWarnings("unchecked")
    public <T extends EventContext<PD, SD>, PD extends EventData, SD extends EventData> T deserializeEventContext(String protocolId, String eventContextString) {
        var eventContextId = this.getEventContextId(protocolId);
        var eventContextClass = (Class<T>) EventContext.getClass(eventContextId);
        var serializerService = this.getService(SerializationService.EventContext.class);
        return serializerService.deserialize(eventContextString, eventContextClass);
    }

    @Override
    public void subscribeToServices() {
        // subscribe to the event service
        this.getService(EventService.Target.class).subscribe(this);
    }

    @Override
    public void unsubscribeFromServices() {
        // unsubscribe from event service
        this.getService(EventService.Target.class).unsubscribe(this);
    }

    @Override
    public void onInitialize(String eventId, String protocolId, String securityLevel) {
        var persistenceService = this.getService(PersistenceService.class);
        if (!persistenceService.hasContext(eventId)) {

            // create and persist context
            var eventContext = this.createEventContext(eventId, protocolId, securityLevel);
            persistenceService.saveContext(eventContext);

            // make logger entry
            this.log(TRACE, eventId, "Event initialized");

            // notify status
            var stateId = eventContext.getCurrentStateId();
            this.notifyStatus(eventId, stateId);

        } else {
            this.log(WARNING, eventId, "Event already initialized");
        }

    }

    @Override
    public void onTerminate(String eventId) {

        // seal context
        this.getService(PersistenceService.class).sealContext(eventId);

        // make logger entry
        this.log(TRACE, eventId, "Event terminated");
    }

    @Override
    public void onAbort(String eventId) {
        // make logger entry
        this.log(TRACE, eventId, "Event error");
    }

    @Override
    public void onStart(String eventId, String phaseId) {
        this.handleActivation(eventId, state -> {
            var stateId = state.getId();
            var phase = State.getAssignedPhase(stateId);
            if (phaseId.equals(Phase.getId(phase))) { // otherwise ignore onStop

                // make logger entry
                var phaseName = Phase.getPrintName(phaseId);
                this.log(TRACE, eventId, "Phase started", phaseName);

                // handle start
                state.handleStart();
            }
        });
    }

    @Override
    public void onStop(String eventId, String phaseId) {
        this.handleActivation(eventId, state -> {
            var stateId = state.getId();
            var phase = State.getAssignedPhase(stateId);
            if (phaseId.equals(Phase.getId(phase))) { // otherwise ignore onStop

                // make logger entry
                var phaseName = Phase.getPrintName(phaseId);
                this.log(TRACE, eventId, "Phase stopped", phaseName);

                // handle start
                state.handleStop();
            }
        });
    }

    @Override
    public void onMail(Mail mail) {

        // make logger entry
        var eventId = mail.getEventId();
        var senderId = mail.getSenderId();
        var contentId = mail.getContentId();
        var contentName = Content.getPrintName(contentId);
        this.log(TRACE, eventId, "Mail received", contentName, senderId);

        // handle activation
        this.handleActivation(eventId, state -> state.handleMail(mail));
    }

    @Override
    public void onMessage(Message message) {

        // preparation
        var persistenceService = this.getService(PersistenceService.class);
        var eventId = message.getEventId();

        // make logger entry
        var senderId = message.getSenderId();
        var contentId = message.getContentId();
        var contentName = Content.getPrintName(contentId);
        this.log(TRACE, eventId, "Message received", contentName, senderId);

        try {

            // lock event and load context
            if (!persistenceService.hasContext(eventId)) {
                throw new EventContextException(EventContextException.Type.UNKNOWN_EVENT_ID, eventId);
            }
            persistenceService.lockContext(eventId);
            var eventContext = persistenceService.loadContext(eventId);

            // get current state id
            var stateId = eventContext.getCurrentStateId();

            // check if the current state is active (non-final)
            if (State.isActive(stateId)) {

                // create current state
                var state = State.createState(stateId, this, eventContext);

                // check if correct message receiver
                if (!this.id.equals(message.getReceiverId())) {
                    throw new CommunicationException(CommunicationException.Type.WRONG_RECEIVER, message);
                }

                // handle message
                state.handleMessage(message);

                // save context
                persistenceService.saveContext(eventContext);

            } else {

                // unlock context
                persistenceService.unlockContext(eventId);
            }

        } catch (CommunicationException | EventContextException | StateException exception) {
            // make logger entry
            this.log(WARNING, eventId, "Handling failed", exception);
        }

        // unlock event
        persistenceService.unlockContext(eventId);
    }

    @Override
    public void onRequest(Request request) {

        // make logger entry
        var eventId = request.getEventId();
        var requesterId = request.getRequesterId();
        var contentId = request.getContentId();
        var contentName = Content.getPrintName(contentId);
        this.log(TRACE, eventId, "Request received", contentName, requesterId);

        // handle activation
        this.handleActivation(eventId, state -> state.handleRequest(request));
    }

    @Override
    public void onResponse(Response response) {

        // make logger entry
        var eventId = response.getEventId();
        var responderId = response.getResponderId();
        var contentId = response.getContentId();
        var contentName = Content.getPrintName(contentId);
        this.log(TRACE, eventId, "Response received", contentName, responderId);

        // handle activation
        this.handleActivation(eventId, state -> state.handleResponse(response));
    }

    @Override
    public void onInput(Input input) {

        // make logger entry
        var eventId = input.getEventId();
        var contentId = input.getContentId();
        var contentName = Content.getPrintName(contentId);
        this.log(TRACE, eventId, "Input entered", contentName);

        // handle activation
        this.handleActivation(eventId, state -> state.handleInput(input));
    }

    @Override
    public void onOutput(Output output) {

        // make logger entry
        var eventId = output.getEventId();
        var contentId = output.getContentId();
        var contentName = Content.getPrintName(contentId);
        this.log(TRACE, eventId, "Output displayed", contentName);

        // handle activation
        this.handleActivation(eventId, state -> state.handleOutput(output));
    }

    @Override
    public void onPublication(Publication publication) {

        // make logger entry
        var eventId = publication.getEventId();
        var contentId = publication.getContentId();
        var contentName = Content.getPrintName(contentId);
        this.log(TRACE, eventId, "Publication received", contentName);

        // handle activation
        this.handleActivation(eventId, state -> state.handlePublication(publication));
    }

    @Override
    public void onSelfActivation(String eventId, Consumer<State<?, ?>> activationHandler) {
        this.handleActivation(eventId, activationHandler);
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return this.id;
    }

    // private method for submitting all necessary status notifications
    private void notifyStatus(String eventId, String stateId) {

        // get phase and status types assigned to state class
        var phase = State.getAssignedPhase(stateId);
        var statusTypes = State.getAssignedStatusTypes(stateId);

        // send notifications
        var phaseId = Phase.getId(phase);
        for (var statusType : statusTypes) {

            // send notification
            var status = new Status(eventId, this.id, phaseId, statusType);
            this.getService(EventService.Target.class).notifyStatus(status);

            // make logger entry
            var phaseName = Phase.getPrintName(phaseId);
            this.log(TRACE, eventId, "Status notified", statusType, phaseName);
        }
    }

    // private helper method to eventually decrypt and deserialize the content
    private <T extends Content> T getContent(Class<T> contentClass, Communication communication, String securityLevel) {
        var contentId = communication.getContentId();
        if (contentId.equals(Content.getId(contentClass))) {
            var contentString = communication.getContentString();
            if (Content.isEncrypted(contentId)) {
                contentString = this.decrypt(communication.getContentString(), securityLevel);
            }
            return this.deserializeContent(contentClass, contentString);
        }
        throw new FrameworkException(FrameworkException.Type.INVALID_CONTENT_ID, communication, contentClass);
    }

    // private helper method to check the validity of the signature included in the given signed communication
    private void checkContent(Content content, Communication.Signed signedCommunication, Object auxiliaryData, String securityLevel) throws CommunicationException {
        var signerId = signedCommunication.getSignerId();
        var signature = signedCommunication.getSignature();
        if (!this.verify(signature, content, auxiliaryData, signerId, securityLevel)) {
            throw new CommunicationException(CommunicationException.Type.INVALID_SIGNATURE, signedCommunication);
        }
    }

    // private method for generating encryption key pairs for all encryption schemes necessary to fulfill all roles
    private void generateEncryptionKeyPairs() {
        if (this.getClass().isAnnotationPresent(EncryptionKeys.class)) {
            var encryptionService = this.getService(EncryptionService.class);
            var keyGenerator = encryptionService.getKeyGenerator();
            var algorithmName = encryptionService.getAlgorithmName();
            for (var securityLevel : keyGenerator.getSecurityLevels()) {
                var encryptionKeyPair = keyGenerator.generateKeyPair(securityLevel);
                this.getService(KeystoreService.class).addEntry(encryptionKeyPair);
                var encryptionKey = encryptionKeyPair.getPublicKey();
                this.getService(CertificateService.class).requestCertificate(encryptionKey, this.id, Certificate.Type.ENCRYPTION, algorithmName, securityLevel);
            }
        }
    }

    // private method for encrypting a message content
    private String encrypt(Content content, String receiverId, String securityLevel) {
        var contentString = this.serializeContent(content);
        var encryptionService = this.getService(EncryptionService.class);
        var algorithmName = encryptionService.getAlgorithmName();
        var encryptionKey = this.getPublicKey(receiverId, Certificate.Type.ENCRYPTION, algorithmName, securityLevel);
        return encryptionService.encrypt(contentString, encryptionKey, securityLevel);
    }

    // private method for decrypting s message content
    @SuppressWarnings("UnnecessaryLocalVariable")
    private String decrypt(String encryptedContentString, String securityLevel) {
        var encryptionService = this.getService(EncryptionService.class);
        var algorithmName = encryptionService.getAlgorithmName();
        var decryptionKey = this.getPrivateKey(Certificate.Type.ENCRYPTION, algorithmName, securityLevel);
        var contentString = encryptionService.decrypt(encryptedContentString, decryptionKey, securityLevel);
        return contentString;
    }

    // private method for generating signature key pairs for all signature schemes necessary to fulfill all roles
    private void generateSignatureKeyPair() {
        if (this.getClass().isAnnotationPresent(SignatureKeys.class)) {
            var signatureService = this.getService(SignatureService.class);
            var keyGenerator = signatureService.getKeyGenerator();
            var algorithmName = signatureService.getAlgorithmName();
            for (var securityLevel : keyGenerator.getSecurityLevels()) {
                var signatureKeyPair = keyGenerator.generateKeyPair(securityLevel);
                this.getService(KeystoreService.class).addEntry(signatureKeyPair);
                var verificationKey = signatureKeyPair.getPublicKey();
                this.getService(CertificateService.class).requestCertificate(verificationKey, this.id, Certificate.Type.SIGNATURE, algorithmName, securityLevel);
            }
        }
    }

    // private method for signing a message content
    private String sign(Content content, Object auxiliaryData, String securityLevel) {
        var signatureService = this.getService(SignatureService.class);
        var algorithmName = signatureService.getAlgorithmName();
        var signatureKey = this.getPrivateKey(Certificate.Type.SIGNATURE, algorithmName, securityLevel);
        return signatureService.sign(content, auxiliaryData, signatureKey, securityLevel);
    }

    // private method for verifying the signature of a message content
    private boolean verify(String signature, Content content, Object auxiliaryData, String senderId, String securityLevel) {
        var signatureService = this.getService(SignatureService.class);
        var algorithmName = signatureService.getAlgorithmName();
        var verificationKey = this.getPublicKey(senderId, Certificate.Type.SIGNATURE, algorithmName, securityLevel);
        return signatureService.verify(signature, content, auxiliaryData, verificationKey, securityLevel);
    }

    // private method for selecting a private key
    private String getPrivateKey(Certificate.Type type, String algorithmName, String securityLevel) {
        var publicKey = this.getPublicKey(this.id, type, algorithmName, securityLevel);
        return this.getService(KeystoreService.class).getPrivateKey(publicKey).orElseThrow(() -> new FrameworkException(FrameworkException.Type.MISSING_PRIVATE_KEY, publicKey));
    }

    // private method for selecting a private key
    private String getPublicKey(String subject, Certificate.Type type, String algorithmName, String securityLevel) {
        var certificate = this.getService(CertificateService.class).retrieveCertificate(subject, type, algorithmName, securityLevel).orElseThrow(() -> new CommunicationException(CommunicationException.Type.UNKNOWN_CERTIFICATE));
        return certificate.getPublicKey();
    }

    // protected method for serializing a content using the content serializer service
    protected String serializeContent(Content content) {
        var serializerService = this.getService(SerializationService.Content.class);
        return serializerService.serialize(content);
    }

    // protected method for deserializing a content using the content serializer service
    protected <T extends Content> T deserializeContent(Class<T> contentClass, String contentString) {
        var serializerService = this.getService(SerializationService.Content.class);
        return serializerService.deserialize(contentString, contentClass);
    }

    // creates a new EventContext for a given eventId and protocolId (a corresponding subclass must exist at the right location in the party's package structure
    private EventContext<?, ?> createEventContext(String eventId, String protocolId, String securityLevel) {
        var eventContextId = this.getEventContextId(protocolId);
        var eventContextClass = EventContext.getClass(eventContextId);
        try {
            var constructor = eventContextClass.getDeclaredConstructor(String.class, String.class, String.class);
            constructor.setAccessible(true);
            return constructor.newInstance(eventId, protocolId, securityLevel);
        } catch (ReflectiveOperationException exception) {
            throw new FrameworkException(FrameworkException.Type.MISSING_CONSTRUCTOR, exception, eventId);
        }
    }

    /**
     * Returns the event context id for a given protocol id. This is a default implementation, which can be overridden
     * in subclasses. The default implementation assumes that an event context class called {@code EventContext} is
     * located in a sub-package with a name identical to the protocol's print name in lower-case. Without overriding
     * this method and without providing such an event context class in such a sub-package, the initialization of new
     * events will fail.
     *
     * @param protocolId The given protocol id
     * @return The corresponding event id
     */
    public String getEventContextId(String protocolId) {
        var packageName = Protocol.getPrintName(protocolId).toLowerCase();
        return this.getClass().getPackageName() + "." + packageName + "." + EventContext.class.getSimpleName();
    }

    // private method for handling different kinds of activations
    private void handleActivation(String eventId, Consumer<State<?, ?>> handler) {
        var persistenceService = this.getService(PersistenceService.class);

        try {
            // lock event and load context
            if (!persistenceService.hasContext(eventId)) {
                throw new EventContextException(EventContextException.Type.UNKNOWN_EVENT_ID, eventId);
            }
            persistenceService.lockContext(eventId);
            var eventContext = persistenceService.loadContext(eventId);

            // get current state id
            var stateId = eventContext.getCurrentStateId();

            // check if the current state is active (non-final)
            if (State.isActive(stateId)) {

                // create current state
                var state = State.createState(stateId, this, eventContext);

                // handle message
                handler.accept(state);

                // save context
                persistenceService.saveContext(eventContext);

            } else {

                // unlock context
                persistenceService.unlockContext(eventId);
            }

        } catch (CommunicationException | EventContextException | StateException exception) {
            // make logger entry
            this.log(WARNING, eventId, "Handling failed", exception);
        }

        // unlock event
        persistenceService.unlockContext(eventId);
    }

    // private methods for making consistent log entries
    private void log(System.Logger.Level level, String eventId, String message, Object... logObjects) {
        var objectsString = "";
        if (logObjects != null && logObjects.length > 0) {
            objectsString = Arrays.stream(logObjects).map(Object::toString).collect(Collectors.joining(", ", "(", ")"));
        }
        var logString = String.format("%-11s %-28s : %s %s", eventId, this.id, message, objectsString);
        this.logger.log(level, logString);
    }

    /**
     * Returns the common role assigned to both the given party class using the
     * {@link Role} annotation and the given protocol using the {@link Roles}
     * annotation. If less or more than one common role exist, an exception is thrown.
     *
     * @param partyClass The given party class
     * @param protocol   The given protocol
     * @return The role assigned to the party in the given protocol
     * @throws FrameworkException if less or more than one common role exist
     */
    static public Class<? extends ch.openchvote.base.framework.protocol.Role> getAssignedRole(Class<? extends Party> partyClass, Class<? extends Protocol> protocol) {
        var assignedRoles = Party.getAssignedRoles(partyClass);
        var protocolRoles = Protocol.getAssignedRoles(protocol);
        var roles = assignedRoles.stream().filter(protocolRoles::contains).toList();
        if (roles.size() == 1) {
            return roles.getFirst();
        }
        throw new FrameworkException(FrameworkException.Type.INVALID_NUMBER_OF_ROLES_ASSIGNED);
    }

    /**
     * Returns the set of roles assigned to the given party class using the
     * {@link Role} annotation.
     *
     * @param partyClass The given party class
     * @return The set of assigned roles
     */
    static public Set<? extends Class<? extends ch.openchvote.base.framework.protocol.Role>> getAssignedRoles(Class<? extends Party> partyClass) {
        // Case 1: there are multiple @Role annotations
        var roleAnnotationsClass = Role.Annotations.class;
        if (partyClass.isAnnotationPresent(roleAnnotationsClass)) {
            return Arrays.stream(partyClass.getAnnotation(roleAnnotationsClass).value()).map(Role::value).collect(Collectors.toSet());
        }
        // Case 2: there is exactly one @Role annotation
        var roleAnnotationClass = Role.class;
        if (partyClass.isAnnotationPresent(roleAnnotationClass)) {
            return Set.of(partyClass.getAnnotation(roleAnnotationClass).value());
        }
        // Case 3: there are no @Role annotations
        return Set.of();
    }

}