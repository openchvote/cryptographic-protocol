/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.synchronizer;

import java.util.HashSet;
import java.util.Set;

/**
 * This abstract class serves as a base class for concrete notifier implementations. A notifier is an object that keeps
 * track of a list of subscribers, which are informed by the notifier when a specific event occurs. The abstract class
 * also provides static methods for combining individual notifiers in different ways. A combined notifier bundles the
 * notifications of its child notifiers and informs its subscribers when
 */
public abstract class Notifier {

    private final Set<Subscriber> subscribers;

    protected Notifier() {
        this.subscribers = new HashSet<>();
    }

    /**
     * Adds the given subscriber to the notifier's list of subscribers.
     *
     * @param subscriber The given subscriber
     */
    public synchronized void subscribe(Subscriber subscriber) {
        this.subscribers.add(subscriber);
    }

    /**
     * Checks if the specific event has occurred and all subscribers have been notifier.
     *
     * @return {@code true}, if the specific event has occurred, {@code else} otherwise
     */
    public abstract boolean isDone();

    // sends a notification to the every subscriber from the notifier's list of subscribers.
    protected synchronized void notifySubscribers() { // method name `notify` not allowed
        this.subscribers.forEach(subscriber -> subscriber.onNotification(this));
    }

    /**
     * Returns a {@link Synchronizer} that blocks the current thread until its receives a notification from the
     * notifier. This is a convenience method that can be used as part of a pipeline notation instead of calling the
     * constructor.
     *
     * @return The newly constructed synchronizer
     */
    public Synchronizer toSynchronizer() {
        return new Synchronizer(this);
    }

    /**
     * Creates a combined notifier that correspond to an AND-composition of this notifier and the given notifier. This
     * is a convenience method that can be used as part of a pipeline notation instead of calling the static method
     * {@link Notifier#ofAll(Notifier...)} with two arguments.
     *
     * @param other The given notifier
     * @return The AND-composition of this and other
     */
    public Notifier and(Notifier other) {
        return Notifier.ofAll(this, other);
    }

    /**
     * Creates a combined notifier that correspond to an OR-composition of this notifier and the given notifier. This is
     * a convenience method that can be used as part of a pipeline notation instead of calling the static method
     * {@link Notifier#ofAny(Notifier...)} with two arguments.
     *
     * @param other The given notifier
     * @return The AND-composition of this and other
     */
    public Notifier or(Notifier other) {
        return Notifier.ofAny(this, other);
    }

    /**
     * Returns a combined notifier that notifies its subscribers when all the given child notifiers have sent a
     * notification. This corresponds to a conjunctive composition, which can be used to build more complex compositions
     * (in combination with {@link Notifier#ofAny(Notifier...)}).
     *
     * @param notifiers The given child notifiers
     * @return The combined notifier
     */
    static public Notifier ofAll(Notifier... notifiers) {
        return new ComposedNotifier(notifiers) {
            @Override
            public synchronized void onNotification(Notifier notifier) {
                if (this.childern.contains(notifier)) {
                    if (this.isDone()) {
                        this.notifySubscribers();
                    }
                }
            }

            @Override
            public boolean isDone() {
                return this.childern.stream().allMatch(Notifier::isDone);
            }
        };
    }

    /**
     * Returns a combined notifier that notifies its subscribers when any of the given child notifiers has sent a
     * notification.  This corresponds to a disjunctive composition, which can be used to build more complex
     * compositions (in combination with {@link Notifier#ofAll(Notifier...)}).
     *
     * @param notifiers The given child notifiers
     * @return The combined notifier
     */
    static public Notifier ofAny(Notifier... notifiers) {
        return new ComposedNotifier(notifiers) {
            @Override
            public synchronized void onNotification(Notifier notifier) {
                if (this.childern.contains(notifier)) {
                    if (this.isDone()) {
                        this.notifySubscribers();
                    }
                }
            }

            @Override
            public boolean isDone() {
                return this.childern.stream().anyMatch(Notifier::isDone);
            }
        };
    }

    // private base class for the two anonymous subclasses needed in ofAll and ofAny
    static private abstract class ComposedNotifier extends Notifier implements Subscriber {

        protected final Set<Notifier> childern;

        protected ComposedNotifier(Notifier... children) {
            super();
            this.childern = new HashSet<>();
            for (var notifier : children) {
                notifier.subscribe(this);
                this.childern.add(notifier);
            }
        }

    }

}
