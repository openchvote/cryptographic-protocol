/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.communication.TestData;

/**
 * The purpose of this interface is to let parties involved in the execution of a protocol share parts of their
 * application data with some test code. For this to work, the test code needs to define a service consumer of type
 * {@link Tester} that implements the {@link Tester#onTestData(TestData)} method. To receive the test data for a given
 * event id, a subscription must be established beforehand. Subscriptions are always relative to one or multiple event
 * ids.
 */
public interface TestingService {

    /**
     * This sub-interface defines a method for providing some {@link TestData} to the subscribed testers. Calling this
     * method will transfer the provided test data to the subscribed testers by calling their method
     * {@link Tester#onTestData(TestData)}.
     */
    interface Source extends Service {

        /**
         * Provides some test data to the subscribed testers.
         *
         * @param testData Some test data
         */
        void provide(TestData testData);

    }

    /**
     * This sub-interface can be used by service consumers to subscribe to the testing service, or to unsubscribe from
     * it. Such testers with a subscription for a given event id will receive all provided test data over corresponding
     * calls to the method {@link Tester#onTestData(TestData)}.
     */
    interface Target extends Service {

        /**
         * Subscribes the given tester to the testing service for a given event id. Nothing happens if the subscription
         * already exists.
         *
         * @param tester  The given tester
         * @param eventId The given event id
         */
        void subscribe(Tester tester, String eventId);

        /**
         * Unsubscribes the given tester from the testing service for a given event id. Nothing happens if the
         * subscription does not exist.
         *
         * @param tester  The given tester
         * @param eventId The given event id
         */
        void unsubscribe(Tester tester, String eventId);

        /**
         * Subscribes the given tester to the testing service for multiple event ids. Event ids of already existing
         * subscriptions are ignored.
         *
         * @param tester   The given tester
         * @param eventIds The given event ids
         */
        default void subscribe(Tester tester, Iterable<String> eventIds) {
            eventIds.forEach(eventId -> this.subscribe(tester, eventId));
        }

        /**
         * Unsubscribes the given tester from the testing service for multiple event ids. Non-existing event ids are
         * ignored.
         *
         * @param tester   The given tester
         * @param eventIds The given event ids
         */
        default void unsubscribe(Tester tester, Iterable<String> eventIds) {
            eventIds.forEach(eventId -> this.unsubscribe(tester, eventId));
        }

    }

    /**
     * Testing service consumers must implement this sub-interface to receive the test data provided for the subscribed
     * event ids.
     */
    interface Tester extends Service.Subscriber {

        /**
         * Transmits the test data from the provider to the tester.
         *
         * @param testData The provided test data
         */
        void onTestData(TestData testData);

    }

}
