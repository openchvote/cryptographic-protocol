/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.communication;

import ch.openchvote.base.framework.annotations.content.Encrypted;
import ch.openchvote.base.framework.annotations.content.Signed;
import ch.openchvote.base.framework.exceptions.FrameworkException;
import ch.openchvote.base.framework.interfaces.Identifiable;
import ch.openchvote.base.framework.services.SerializationService;

import static ch.openchvote.base.framework.exceptions.FrameworkException.Type.CONTENT_CLASS_NOT_FOUND;

/**
 * This interface defines a common type for various content classes. Specific content classes can be defined for the
 * framework's different communication services. Each content class defines the exact structure of the information to be
 * transmitted using these services. For the transmission, content objects are serialized using a serialization service
 * of type {@link SerializationService.Content}. Default methods are provided for the methods inherited from
 * {@link Identifiable}. The id of a content corresponds to the fully qualified class name, and the print name of the
 * content to the class name.
 */
public interface Content extends Identifiable {

    @Override
    default String getId() {
        return Content.getId(this.getClass());
    }

    @Override
    default String getPrintName() {
        return Content.getPrintName(this.getClass());
    }

    /**
     * Returns the id of a given content class. The id corresponds to the content class's fully qualified name (the name
     * of the class prefixed with the package name), which is obtained by calling {@link Class#getName()}.
     *
     * @param contentClass The given content class
     * @return The id of a given content class
     */
    static String getId(Class<? extends Content> contentClass) {
        return contentClass.getName();
    }

    /**
     * Returns the content class for a given content id. For this method to work, the id must correspond to the fully
     * qualified name of the content class. An exception is thrown if no such class exists.
     *
     * @param contentId The given content id
     * @return The content class for the given content id
     * @throws FrameworkException if no content class exists for the given content id
     */
    @SuppressWarnings("unchecked")
    static Class<? extends Content> getClass(String contentId) {
        try {
            return (Class<? extends Content>) Class.forName(contentId);
        } catch (ClassNotFoundException exception) {
            throw new FrameworkException(CONTENT_CLASS_NOT_FOUND, exception, contentId);
        }
    }

    /**
     * Returns the print name of the content class for a given content id. The name is obtained by calling
     * {@link Class#getSimpleName()}.
     *
     * @param contentId The given content id
     * @return The print name of the corresponding content class
     */
    static String getPrintName(String contentId) {
        var contentClass = Content.getClass(contentId);
        return Content.getPrintName(contentClass);
    }

    /**
     * Returns the print name of a given content class, which is obtained by calling {@link Class#getSimpleName()}.
     *
     * @param contentClass The given content class
     * @return The print name of the given content class
     */
    static String getPrintName(Class<? extends Content> contentClass) {
        return contentClass.getSimpleName();
    }

    /**
     * Checks if the content class for the given content id is annotated with {@link Signed}.
     *
     * @param contentId The given content id
     * @return {@code true}, if the content class is annotated with {@link Signed}, {@code false} otherwise
     */
    static boolean isSigned(String contentId) {
        return Content.getClass(contentId).isAnnotationPresent(Signed.class);
    }

    /**
     * Checks if the content class for the given content id is annotated with {@link Encrypted}.
     *
     * @param contentId The given content id
     * @return {@code true}, if the content class is annotated with {@link Encrypted}, {@code false} otherwise
     */
    static boolean isEncrypted(String contentId) {
        return Content.getClass(contentId).isAnnotationPresent(Encrypted.class);
    }

}
