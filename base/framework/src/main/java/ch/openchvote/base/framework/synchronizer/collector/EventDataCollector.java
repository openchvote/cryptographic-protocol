/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.synchronizer.collector;

import ch.openchvote.base.framework.services.Service;
import ch.openchvote.base.framework.synchronizer.CountDownNotifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This is an abstract base class for implementing different event data collectors. An event data collector listens to
 * one or multiple public communication channels during the execution of a protocol event and collects the received
 * event data up to a certain amount. When that amount of data is reached, a notification is sent to every subscriber of
 * the collector's list of subscribers. By extending {@link CountDownNotifier}, the notification functionality is
 * inherited. This class is thread-safe.
 *
 * @param <D> The generic type of the data to be collected
 */
public abstract class EventDataCollector<D> extends CountDownNotifier implements Service.Subscriber {

    protected final String eventId;
    protected final List<D> data;

    // general constructor for initializing the synchronizer
    protected EventDataCollector(String eventId, int maxSize) {
        super(maxSize);
        this.eventId = eventId;
        this.data = new ArrayList<>();
        this.loadServices();
        this.subscribeToServices();
    }

    /**
     * Returns the current list of collected event data as an unmodifiable list.
     *
     * @return The current list of collected event data
     */
    public synchronized List<D> getData() {
        return Collections.unmodifiableList(this.data);
    }

    // protected method to collect data, count down the counter, and if the counter is 0, invoke the data mapper and release the latch
    protected synchronized void onData(String eventId, D data) {
        if (eventId.equals(this.eventId)) {
            if (!this.isDone()) {
                this.data.add(data);
                this.countDown();
                if (this.isDone()) {
                    this.unsubscribeFromServices();
                }
            }
        }
    }

    // abstract protected method for loading the necessary services (this method is automatically called by the constructor)
    protected abstract void loadServices();

}
