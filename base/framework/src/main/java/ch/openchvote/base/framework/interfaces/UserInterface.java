/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.interfaces;

import ch.openchvote.base.framework.communication.Input;
import ch.openchvote.base.framework.communication.Output;

/**
 * This interface provides an abstraction of a user-client relationship between two parties. It provides two
 * sub-interfaces {@link User} and {@link Client} that the two involved parties need to implement. The interface allows
 * the user to "enter" an {@link Input} message, which results in transmitting the input to the client. Upon receiving
 * such an input, the client responds to the user by "displaying" an {@link Output} in an asynchronous manner.
 * Corresponding methods {@link Source#enter(Input)} and {@link Target#display(Output)} exist the sub-interfaces
 * {@link Source} and {@link Target}. For implementations of this interface, it can be assumed that the user knows the
 * client and the client knows the user, i.e., only corresponding getter methods {@link Source#getClient()} and
 * {@link Target#getUser()} need to be implemented.
 */
public interface UserInterface {

    /**
     * This sub-interface defines the methods available to the user of a user interface.
     */
    @FunctionalInterface
    interface Source {

        /**
         * Returns the client assigned to user interface.
         *
         * @return The assigned client
         */
        Client getClient();

        /**
         * Transmits some user input to the client.
         *
         * @param input The given user input
         */
        default void enter(Input input) {
            this.getClient().onInput(input);
        }

    }

    /**
     * This sub-interface defines the methods available to the client of a user interface.
     */
    @FunctionalInterface
    interface Target {

        /**
         * Returns the user assigned to user interface.
         *
         * @return The assigned user
         */
        User getUser();

        /**
         * Transmits some client output back to the user.
         *
         * @param output The given client output
         */
        default void display(Output output) {
            this.getUser().onOutput(output);
        }

    }

    /**
     * This interface defines the method that a user assigned to a user interface must implement. Upon receiving an
     * output message from the client, this method is called for transmitting the output to the user.
     */
    interface User {

        /**
         * Transmits an output message from the client to the user.
         *
         * @param output The given client output
         */
        void onOutput(Output output);

    }

    /**
     * This interface defines the method that a client assigned to a user interface must implement. Upon receiving an
     * input message from the user, this method is called for transmitting the input to the client.
     */
    interface Client {

        /**
         * Transmits an input message from the user to the client.
         *
         * @param input The given user input
         */
        void onInput(Input input);

    }

}
