/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.annotations.protocol;

import ch.openchvote.base.framework.protocol.Protocol;
import ch.openchvote.base.framework.protocol.Role;

import java.lang.annotation.*;

/**
 * This annotation can be used in a given protocol to specify the roles that are assigned to the parties in the
 * protocol. Classes inheriting from {@link Role} and from {@link Protocol} are in a one-to-many relationship.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Roles {

    /**
     * Returns an array of all the roles assigned to the parties in the protocol.
     *
     * @return An array of all roles
     */
    Class<? extends Role>[] value() default {};

}
