/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.synchronizer.monitor;

import ch.openchvote.base.framework.protocol.Phase;
import ch.openchvote.base.framework.services.EventService;
import ch.openchvote.base.framework.services.Service;
import ch.openchvote.base.framework.synchronizer.CountDownNotifier;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * This is an abstract base class for implementing different event service monitors. An event service monitor listens to
 * an event service during the execution of a protocol event for the service coordinator's coordination instructions. If
 * a certain coordination instruction is received, a notification is sent to every subscriber of the monitor's list of
 * subscribers. By extending {@link CountDownNotifier}, the notification functionality is inherited. This class is
 * thread-safe.
 */
public abstract class EventServiceMonitor extends CountDownNotifier implements EventService.Subscriber {

    // used for creating unique ids
    static private int ID_COUNTER = 1;

    // singleton empty consumers
    static private final Consumer<String> DO_NOTHING_STRING = (ignored) -> {
    };
    static private final BiConsumer<String, String> DO_NOTHING_STRING_STRING = (ignored1, ignored2) -> {
    };

    private final String eventId; // always needed
    private final String phaseId; // only needed in EventServiceMonitor.Start and EventServiceMonitor.Stop

    // the monitor's publication service
    private final EventService.Target eventServiceTarget;

    // the tasks to execute when a coordination instruction is received from the event service's coordinator
    protected Consumer<String> onInitialize;
    protected Consumer<String> onTerminate;
    protected Consumer<String> onAbort;
    protected BiConsumer<String, String> onStart;
    protected BiConsumer<String, String> onStop;

    // the task to execute when the monitor receives the coordination instruction it is waiting for
    private BiConsumer<String, String> onDoneTask;

    // sets all tasks to DO_NOTHING (to them be overridden in corresponding subclasses)
    private EventServiceMonitor(String eventId, String phaseId) {
        super(1); // set counter to 1
        this.eventId = eventId;
        this.phaseId = phaseId;
        this.onInitialize = DO_NOTHING_STRING;
        this.onTerminate = DO_NOTHING_STRING;
        this.onAbort = DO_NOTHING_STRING;
        this.onStart = DO_NOTHING_STRING_STRING;
        this.onStop = DO_NOTHING_STRING_STRING;
        this.eventServiceTarget = Service.load(EventService.Target.class);
        this.onDoneTask = DO_NOTHING_STRING_STRING;
        this.subscribeToServices();
    }

    /**
     * Returns the monitor's event id.
     *
     * @return The event id
     */
    public String getEventId() {
        return this.eventId;
    }

    /**
     * Returns the monitor's phase id.
     *
     * @return The phase id
     */
    public String getPhaseId() {
        return this.phaseId;
    }

    /**
     * Adds a task to the monitor. When the monitor receives the coordination instruction it is waiting for, the
     * combination of all added tasks is executed. In this version of the method, the phase id is ignored.
     *
     * @param newTask The new task to execute
     */
    public void addTask(Consumer<String> newTask) {
        this.addTask((eventId, phaseId) -> newTask.accept(eventId));
    }

    /**
     * Adds a task to the monitor. When the monitor receives the coordination instruction it is waiting for, the
     * combination of all added tasks is executed.
     *
     * @param newTask The new task to execute
     */
    public void addTask(BiConsumer<String, String> newTask) {
        this.onDoneTask = this.onDoneTask.andThen(newTask);
    }

    @Override
    public void onInitialize(String eventId, String protocolId, String securityLevel) {
        this.onInitialize.accept(eventId);
    }

    @Override
    public void onTerminate(String eventId) {
        this.onTerminate.accept(eventId);
    }

    @Override
    public void onAbort(String eventId) {
        this.onAbort.accept(eventId);
    }

    @Override
    public void onStart(String eventId, String phaseId) {
        this.onStart.accept(eventId, phaseId);
    }

    @Override
    public void onStop(String eventId, String phaseId) {
        this.onStop.accept(eventId, phaseId);
    }

    @Override
    public void subscribeToServices() {
        this.eventServiceTarget.subscribe(this);
    }

    @Override
    public void unsubscribeFromServices() {
        this.eventServiceTarget.unsubscribe(this);
    }

    @Override
    public String getId() {
        return this.getClass().getSimpleName() + "-" + ID_COUNTER++;
    }

    // protected methods to be called by at least one of the interface method of EventService.Subscriber
    protected synchronized void onInstruction(String eventId) {
        if (eventId.equals(this.eventId)) {
            if (!this.isDone()) {
                this.countDown();
                this.onDoneTask.accept(eventId, phaseId);
                this.unsubscribeFromServices();
            }
        }
    }

    protected synchronized void onInstruction(String eventId, String phaseId) {
        if (eventId.equals(this.eventId) && phaseId.equals(this.phaseId)) {
            if (!this.isDone()) {
                this.countDown();
                this.onDoneTask.accept(eventId, phaseId);
                this.unsubscribeFromServices();
            }
        }
    }

    /**
     * Instances of this subclass listen to {@link EventService.Subscriber#onInitialize}. If this particular
     * coordination instruction is received, a notification is sent to every subscriber of the monitor's list of
     * subscribers.
     */
    static public class Initialize extends EventServiceMonitor {

        /**
         * Creates a new monitor instance for this particular coordination instruction.
         *
         * @param eventId The given event id
         */
        public Initialize(String eventId) {
            super(eventId, null);
            this.onInitialize = this::onInstruction;
        }
    }

    /**
     * Instances of this subclass listen to {@link EventService.Subscriber#onTerminate}}. If this particular
     * coordination instruction is received, a notification is sent to every subscriber of the monitor's list of
     * subscribers.
     */
    static public class Terminate extends EventServiceMonitor {

        /**
         * Creates a new monitor instance for this particular coordination instruction.
         *
         * @param eventId The given event id
         */
        public Terminate(String eventId) {
            super(eventId, null);
            this.onTerminate = this::onInstruction;
        }
    }

    /**
     * Instances of this subclass listen to {@link EventService.Subscriber#onAbort}}. If this particular coordination
     * instruction is received, a notification is sent to every subscriber of the monitor's list of subscribers.
     */
    static public class Abort extends EventServiceMonitor {

        /**
         * Creates a new monitor instance for this particular coordination instruction.
         *
         * @param eventId The given event id
         */
        public Abort(String eventId) {
            super(eventId, null);
            this.onAbort = this::onInstruction;
        }
    }

    /**
     * Instances of this subclass listen to {@link EventService.Subscriber#onStart}}. If this particular coordination
     * instruction is received, a notification is sent to every subscriber of the monitor's list of subscribers.
     */
    static public class Start extends EventServiceMonitor {

        /**
         * Creates a new monitor instance for this particular coordination instruction.
         *
         * @param eventId The given event id
         * @param phase   The given phase
         */
        public Start(String eventId, Class<? extends Phase> phase) {
            super(eventId, Phase.getId(phase));
            this.onStart = this::onInstruction;
        }
    }

    /**
     * Instances of this subclass listen to {@link EventService.Subscriber#onStop}}. If this particular coordination
     * instruction is received, a notification is sent to every subscriber of the monitor's list of subscribers.
     */
    static public class Stop extends EventServiceMonitor {

        /**
         * Creates a new monitor instance for this particular coordination instruction.
         *
         * @param eventId The given event id
         * @param phase   The given phase
         */
        public Stop(String eventId, Class<? extends Phase> phase) {
            super(eventId, Phase.getId(phase));
            this.onStop = this::onInstruction;
        }
    }

}
