/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.security;

import ch.openchvote.base.framework.exceptions.SerializerException;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

/**
 * This interface models the basic functionality of a certificate as a container for assigning a public key to a
 * subject. Both the subject and the public key are given as strings. The current version of this interface ignores
 * several important aspects of 'real' certificates, for example the signature of the issuing CA or the expiration
 * date.
 */
public interface Certificate {

    /**
     * Defines two different types of certificates.
     */
    enum Type {
        /**
         * Used for signature key.
         */
        SIGNATURE,
        /**
         * Used for encryption keys.
         */
        ENCRYPTION
    }

    /**
     * Returns the certificate's type.
     *
     * @return The certificate's type
     */
    Type getType();

    /**
     * Returns the certificate's subject.
     *
     * @return The certificate's subject
     */
    String getSubject();

    /**
     * Returns the certificate's public key.
     *
     * @return The certificate's public key
     */
    String getPublicKey();

    /**
     * Returns the certificate's security level.
     *
     * @return The certificate's security level
     */
    String getSecurityLevel();

    /**
     * Returns the certificate's date of issue.
     *
     * @return The certificate's date of issue
     */
    LocalDateTime getDateOfIssue();

    /**
     * Returns the name of the cryptographic algorithm.
     *
     * @return The name of the cryptographic algorithm
     */
    String getAlgorithmName();

    /**
     * Checks the validity of a certificate.
     *
     * @return {@code true}, if the certificate is valid, {@code false} otherwise
     */
    @SuppressWarnings("SameReturnValue")
    boolean checkValidity();

    /**
     * Default implementation of a simple serialization method.
     *
     * @return The serialized certificate.
     */
    default String serialize() {
        return String.format("%s,%s,%s,%s,%s,%s", this.getPublicKey(), this.getSubject(), this.getType(), this.getAlgorithmName(), this.getSecurityLevel(), this.getDateOfIssue());
    }

    /**
     * Static deserialization method for obtaining a certificate from the given serialization string. If the string is
     * not a valid certificate serialization, an exception is thrown.
     *
     * @param string The given certificate serialization string
     * @return The deserialized certificate
     * @throws SerializerException if the string is not a valid certificate serialization
     */
    static Certificate deserialize(String string) throws SerializerException {
        var values = string.split(",");
        if (values.length != 6) {
            throw new SerializerException(SerializerException.Type.DESERIALIZATION, string);
        }
        try {
            var publicKey = values[0];
            var subject = values[1];
            var type = Certificate.Type.valueOf(values[2]);
            var algorithmName = values[3];
            var securityLevel = values[4];
            var dateOfIssue = LocalDateTime.parse(values[5]);
            return Certificate.of(publicKey, subject, type, algorithmName, securityLevel, dateOfIssue);
        } catch (IllegalArgumentException | IndexOutOfBoundsException | DateTimeParseException exception) {
            throw new SerializerException(SerializerException.Type.DESERIALIZATION, exception, string);
        }
    }

    /**
     * Static method for creating a certificate from the given certificate components.
     *
     * @param publicKey     The given public key
     * @param subject       The given subject
     * @param type          The given certificate type
     * @param algorithmName The given algorithm name
     * @param securityLevel The given security level
     * @param dateOfIssue   The given date of issue
     * @return The resulting certificate
     */
    static Certificate of(String publicKey, String subject, Certificate.Type type, String algorithmName, String securityLevel, LocalDateTime dateOfIssue) {
        return new Certificate() {
            @Override
            public String getPublicKey() {
                return publicKey;
            }

            @Override
            public String getSubject() {
                return subject;
            }

            @Override
            public Type getType() {
                return type;
            }

            @Override
            public String getAlgorithmName() {
                return algorithmName;
            }

            @Override
            public String getSecurityLevel() {
                return securityLevel;
            }

            @Override
            public LocalDateTime getDateOfIssue() {
                return dateOfIssue;
            }

            @Override
            public boolean checkValidity() {
                // TODO checking the certificate's validity
                return true;
            }
        };
    }

}
