/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.communication.Request;
import ch.openchvote.base.framework.communication.Response;

/**
 * This service interface defines the asynchronous communication between a {@link Requester} and a {@link Responder}.
 * Initially, it is always the requester who sends a {@link Request} to the responder with the expectation of getting a
 * {@link Response} in an asynchronous manner. Requests and responses objects are connected by carrying the same unique
 * request id. For using this service, both the requester and the responder must be subscribed. Requests are unsigned,
 * whereas responses are signed.
 */
public interface RequestResponseService {

    /**
     * This sub-interface defines the requester's part of the service needed for submitting requests to the responder.
     * It also includes methods for subscribing to the service and for unsubscribing from the service as a requester.
     */
    interface Source extends Service {

        /**
         * Sends the given request to the intended responder.
         *
         * @param request The given request
         */
        void request(Request request);

        /**
         * Subscribes the given requester to the service. Nothing happens if the requester is already subscribed.
         *
         * @param requester The given subscriber
         */
        void subscribe(Requester requester);

        /**
         * Unsubscribes the given requester from the service. Nothing happens if currently the requester is not
         * subscribed to the service.
         *
         * @param requester The given requester
         */
        void unsubscribe(Requester requester);

    }

    /**
     * This sub-interface defines the responder's part of the service needed for submitting signed responses to the
     * requester. It also includes methods for subscribing to the service and for unsubscribing from the service as a
     * responder.
     */
    interface Target extends Service {

        /**
         * Sends the given response to the requester. The response objects carries the id from the received request
         * objects.
         *
         * @param response The given response
         */
        void respond(Response response);

        /**
         * Subscribes the given responder to the service. Nothing happens if the responder is already subscribed.
         *
         * @param responder The given subscriber
         */
        void subscribe(Responder responder);

        /**
         * Unsubscribes the given responder from the service. Nothing happens if currently the responder is not
         * subscribed to the service.
         *
         * @param responder The given responder
         */
        void unsubscribe(Responder responder);

    }

    /**
     * This sub-interface defines the method that a requester must provide for receiving responses.
     */
    interface Requester extends Service.Subscriber {

        /**
         * This method is called upon receiving an incoming response from the responder.
         *
         * @param response The received response
         */
        void onResponse(Response response);

    }

    /**
     * This sub-interface defines the method that a responder must provide for receiving requests.
     */
    interface Responder extends Service.Subscriber {

        /**
         * This method is called upon receiving an incoming request from the requester.
         *
         * @param request The received request
         */
        void onRequest(Request request);

    }

}
