/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.interfaces.Identifiable;

/**
 * The purpose of an event service is to allow a {@link Coordinator} to instruct the subscribed parties to initialize a
 * new event, start and stop the current phase of a running event, and run or terminate an event. The coordinator
 * expects from the subscribed parties to receive {@link Status} notifications about their current state and phase.
 */
public interface EventService {

    /**
     * This sub-interface defines the coordinator's part of the event service needed for instructing the subscribed
     * parties to initialize a new event, start and stop the current phase of a running event, or terminate an event.
     */
    interface Source extends Service {

        /**
         * Instructs the subscribed parties to initialize a new event. The new event is specified by the given event id,
         * protocol id, and security level. The instruction is ignored by the service, if the given event id is already
         * registered.
         *
         * @param coordinator   The coordinator responsible for initializing the new event
         * @param eventId       The id of the event to initialize
         * @param protocolId    The corresponding protocol id
         * @param securityLevel The corresponding security level
         */
        void initialize(Coordinator coordinator, String eventId, String protocolId, String securityLevel);

        /**
         * Instructs the subscribed parties to terminate the given event. The instruction is ignored by the service, if
         * the given event id is unknown.
         *
         * @param eventId The id of the event to terminate
         */
        void terminate(String eventId);

        /**
         * Instructs the subscribed parties to abort due to an error that has occurred during the execution of the
         * event. The instruction is ignored by the service, if the given event id is unknown.
         *
         * @param eventId The id of the event wit an errow
         */
        void abort(String eventId);


        /**
         * Instructs the subscribed parties to start a specific phase in the given event. The instruction is ignored by
         * the service, if the given event id is unknown.
         *
         * @param eventId The given event id
         * @param phaseId The given phase id
         */
        void start(String eventId, String phaseId);

        /**
         * Instructs the subscribed parties to stop a specific phase in the given event. The instruction is ignored by
         * the service, if the given event id is unknown.
         *
         * @param eventId The given event id
         * @param phaseId The given phase id
         */
        void stop(String eventId, String phaseId);

    }

    /**
     * This sub-interface defines the subscriber's part of the event service needed for notifying the coordinator about
     * the subscriber's current status. It provides methods for subscribing and unsubscribing.
     */
    interface Target extends Service {

        /**
         * Notifies the coordinator about the subscriber's current status.
         *
         * @param status The given status
         */
        void notifyStatus(Status status);

        /**
         * Subscribes the given subscriber to the event service. Nothing happens if the subscriber is already
         * subscribed.
         *
         * @param subscriber The given subscriber
         */
        void subscribe(EventService.Subscriber subscriber);

        /**
         * Unsubscribes the given subscriber from the event service. Nothing happens if currently the subscriber is not
         * subscribed to the event service.
         *
         * @param subscriber The given subscriber
         */
        void unsubscribe(EventService.Subscriber subscriber);

    }

    /**
     * This sub-interface defines the method that a coordinator must provide for receiving status notification from the
     * subscribers.
     */
    interface Coordinator extends Identifiable {

        /**
         * This method is called upon receiving an incoming status notification.
         *
         * @param status The received status
         */
        void onStatus(Status status);

    }

    /**
     * This sub-interface defines the methods that a subscriber must provide for receiving instructions from the
     * coordinator of an event.
     */
    interface Subscriber extends Service.Subscriber {

        /**
         * This method is called upon receiving the coordinator's instruction to initialize a new event for the given
         * event id, protocol id, and security level.
         *
         * @param eventId       The given event id
         * @param protocolId    The given protocol id
         * @param securityLevel The given security level
         */
        void onInitialize(String eventId, String protocolId, String securityLevel);

        /**
         * This method is called upon receiving the coordinator's instruction to terminate the given event.
         *
         * @param eventId The id of the event to terminate
         */
        void onTerminate(String eventId);

        /**
         * This method is called upon receiving the coordinator's notification about an error in the given event.
         *
         * @param eventId The id of the event with an error
         */
        void onAbort(String eventId);

        /**
         * This method is called upon receiving the coordinator's instruction to start a specific phase in the given
         * event.
         *
         * @param eventId The given event id
         * @param phaseId The given phase id
         */
        void onStart(String eventId, String phaseId);

        /**
         * This method is called upon receiving the coordinator's instruction to stop a specific phase in the given
         * event.
         *
         * @param eventId The given event id
         * @param phaseId The given phase id
         */
        void onStop(String eventId, String phaseId);

    }

}
