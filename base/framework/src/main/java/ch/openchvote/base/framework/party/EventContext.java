/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.party;

import ch.openchvote.base.framework.exceptions.FrameworkException;

import java.util.Iterator;
import java.util.List;

/**
 * Instances of this class represent a party's state related to an election event. It stores the protocol id, the id of
 * the current {@link State}, the public and secret {@link EventData}, and all successfully handled messages. Both the
 * event data and the list of messages accumulate more and more information during the execution of the election event.
 *
 * @param <PD> The generic type of the public event data class
 * @param <SD> The generic type of the secret event data class
 */
public abstract class EventContext<PD extends EventData, SD extends EventData> implements Iterable<Object> {

    private final String eventId;
    private final String protocolId;
    private final String securityLevel;
    protected String currentStateId;
    private final EventData publicData;
    private final EventData secretData;

    // protected constructor for initializing all fields (used for reflection)
    protected EventContext(String eventId, String protocolId, String securityLevel, String initialStateId, EventData publicData, EventData secretData) {
        this.eventId = eventId;
        this.protocolId = protocolId;
        this.securityLevel = securityLevel;
        this.currentStateId = initialStateId;
        this.publicData = publicData;
        this.secretData = secretData;
    }

    // protected copy constructor for internal use
    protected EventContext(EventContext<PD, SD> eventContext) {
        this(eventContext.eventId, eventContext.protocolId, eventContext.securityLevel, eventContext.currentStateId, eventContext.publicData.getCopy(), eventContext.secretData.getCopy());
    }

    /**
     * Returns the event id.
     *
     * @return The event id
     */
    public String getEventId() {
        return this.eventId;
    }

    /**
     * Returns the protocol id.
     *
     * @return The protocol id
     */
    public String getProtocolId() {
        return this.protocolId;
    }

    /**
     * Returns the security level.
     *
     * @return The security level
     */
    public String getSecurityLevel() {
        return this.securityLevel;
    }

    /**
     * Returns the id of the current state.
     *
     * @return The current state id
     */
    public String getCurrentStateId() {
        return this.currentStateId;
    }

    /**
     * Updates the current state of the event context.
     *
     * @param stateId The id of the new state
     */
    public void setCurrentState(String stateId) {
        this.currentStateId = stateId;
    }

    /**
     * Returns the public event data.
     *
     * @return The public event data
     */
    @SuppressWarnings("unchecked")
    public PD getPublicData() {
        return (PD) this.publicData;
    }

    /**
     * Returns the secret event data.
     *
     * @return The secret event data
     */
    @SuppressWarnings("unchecked")
    public SD getSecretData() {
        return (SD) this.secretData;
    }

    /**
     * Returns a copy of this event context. This method can be used for implementing a simple persistence service.
     *
     * @return A copy of this event context
     */
    public EventContext<?, ?> getCopy() {
        var eventContextClass = this.getClass();
        try {
            var constructor = eventContextClass.getDeclaredConstructor(eventContextClass);
            constructor.setAccessible(true);
            return constructor.newInstance(this);
        } catch (ReflectiveOperationException exception) {
            throw new FrameworkException(FrameworkException.Type.MISSING_CONSTRUCTOR, exception, eventContextClass);
        }
    }

    @Override
    public String toString() {
        return String.format("%s[state=%s]", this.getClass().getSimpleName(), this.currentStateId);
    }

    @Override
    public Iterator<Object> iterator() {
        return List.of(this.eventId, this.protocolId, this.currentStateId, this.publicData, this.secretData).iterator();
    }

    /**
     * Returns the event context class for a given event context id. For this method to work, the id must correspond to
     * the fully qualified name of the event context class. An exception is thrown if no such class exists.
     *
     * @param eventContextId The given event context id
     * @return The corresponding event context class
     * @throws FrameworkException if no event context class exists for the given event context id
     */
    @SuppressWarnings("unchecked")
    static public Class<? extends EventContext<?, ?>> getClass(String eventContextId) {
        try {
            return (Class<? extends EventContext<?, ?>>) Class.forName(eventContextId);
        } catch (ClassNotFoundException exception) {
            throw new FrameworkException(FrameworkException.Type.EVENT_CONTEXT_CLASS_NOT_FOUND, exception, eventContextId);
        }
    }

}
