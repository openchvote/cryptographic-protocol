/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.security;

/**
 * Represents the concept of a key pair consisting of a private and a public key.
 */
public interface KeyPair {

    /**
     * Returns the private key.
     *
     * @return The private key
     */
    String getPrivateKey();

    /**
     * Returns the public key.
     *
     * @return The public key
     */
    String getPublicKey();

    /**
     * Returns the associated security level.
     *
     * @return The associated security level
     */
    String getSecurityLevel();

}
