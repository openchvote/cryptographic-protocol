/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.communication;

import ch.openchvote.base.framework.services.PublicationService;

/**
 * Instances of this class represent the signed publications to be transmitted over the {@link PublicationService}. All
 * fields are of type {@link String}. The class has no methods other than the getter methods for accessing the fields.
 */
public final class Publication extends Communication.Signed {

    /**
     * Constructs a new publication object from the given parameters.
     *
     * @param eventId       The given event id
     * @param publisherId   The given publisher id
     * @param contentId     The publication's content id
     * @param contentString The publication's content string
     * @param signature     The publication's signature
     */
    public Publication(String eventId, String publisherId, String contentId, String contentString, String signature) {
        super(eventId, contentId, contentString, publisherId, signature);
    }

    /**
     * Returns the id of the publication's publisher.
     *
     * @return The publisher id
     */
    public String getPublisherId() {
        return super.getSignerId();
    }

    @Override
    public String toString() {
        return String.format("Publication={eventId=%s, publisherId=%s, contentId=%s}", this.eventId, this.getPublisherId(), this.contentId);
    }

}
