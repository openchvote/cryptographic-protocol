/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.synchronizer.collector;

import ch.openchvote.base.framework.communication.TestData;
import ch.openchvote.base.framework.services.Service;
import ch.openchvote.base.framework.services.TestingService;

/**
 * This class implements a synchronizer that listens to a {@link TestingService} for a given event. It collects all the
 * test data up to a certain amount. By instantiating the generic result type to {@code Boolean}, the expected result
 * will be either {@code true} or {@code false}, depending on whether the tests performed on the test data succeed or
 * not. When the specified number of test data is reached, the predicate is applied to the list of collected test data
 * and the result will be set accordingly.
 */
public class TestDataCollector extends EventDataCollector<TestData> implements TestingService.Tester {

    // used for creating unique ids
    static private int ID_COUNTER = 1;

    // the collector's testing service
    private TestingService.Target testingServiceTarget;

    /**
     * This is the general constructor of this class. It defines the event id and the number of expected test data.
     *
     * @param eventId          The given event id
     * @param numberOfTestData The expected number of test data
     */
    public TestDataCollector(String eventId, int numberOfTestData) {
        super(eventId, numberOfTestData);
    }

    @Override
    public void onTestData(TestData testData) {
        var eventId = testData.getEventId();
        this.onData(eventId, testData);
    }

    @Override
    protected void loadServices() {
        this.testingServiceTarget = Service.load(TestingService.Target.class);
    }

    @Override
    public void subscribeToServices() {
        this.testingServiceTarget.subscribe(this, this.eventId);
    }

    @Override
    public void unsubscribeFromServices() {
        this.testingServiceTarget.unsubscribe(this, this.eventId);
    }

    @Override
    public String getId() {
        return this.getClass().getSimpleName() + "-" + ID_COUNTER++;
    }

}
