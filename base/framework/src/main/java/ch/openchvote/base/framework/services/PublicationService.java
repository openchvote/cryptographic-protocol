/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.communication.Publication;

/**
 * This service interface can be used for sending some data related to an event to all parties subscribed to this
 * service. For subscribing to this service, an event id must be specified, i.e., subscribers will only receive the
 * publications for that particular event. Publishing event data using this service requires the publisher to include
 * the data in an instance of type {@link Publication}.
 */
public interface PublicationService {

    /**
     * This sub-interface defines the part of the publication service allows parties ti submit a publication. It
     * consists of a single method {@link Source#publish(Publication)}, which can be called by anyone. This method is
     * responsible for sending the publication to all subscribers of the event id specified by the publication.
     */
    interface Source extends Service {

        /**
         * Sends the given publication to all subscribers of the specified event id.
         *
         * @param publication The given publication
         */
        void publish(Publication publication);

    }

    /**
     * This sub-interface defines the part of the publication service that the subscribers use for maintaining their
     * subscriptions.
     */
    interface Target extends Service {

        /**
         * Subscribes the given subscriber to this service. If a subscription already exists for the given event id,
         * calling this method has no effect.
         *
         * @param subscriber The given subscriber
         */
        void subscribe(PublicationService.Subscriber subscriber);

        /**
         * Subscribes the given subscriber to this service for the given event id. If a subscription already exists for
         * the given event id, calling this method has no effect.
         *
         * @param subscriber The given subscriber
         * @param eventId    The given event id
         */
        void subscribe(PublicationService.Subscriber subscriber, String eventId);

        /**
         * Subscribes the given subscriber to this service for a given iterable collection of event ids. Existing event
         * ids are ignored.
         *
         * @param subscriber The given subscriber
         * @param eventIds   The given event ids
         */
        default void subscribe(PublicationService.Subscriber subscriber, Iterable<String> eventIds) {
            eventIds.forEach(eventId -> this.subscribe(subscriber, eventId));
        }

        /**
         * Unsubscribes the given subscriber from this service. If no subscription exists for the given event id,
         * calling this method has no effect.
         *
         * @param subscriber The given subscriber
         */
        void unsubscribe(PublicationService.Subscriber subscriber);

        /**
         * Unsubscribes the given subscriber from this service for the given event id. If no subscription exists for the
         * given event id, calling this method has no effect.
         *
         * @param subscriber The given subscriber
         * @param eventId    The given event id
         */
        void unsubscribe(PublicationService.Subscriber subscriber, String eventId);

        /**
         * Unsubscribes the given subscriber from this service for a given iterable collection of event ids.
         * Non-existing event ids are ignored.
         *
         * @param subscriber The given subscriber
         * @param eventIds   The given event ids
         */
        default void unsubscribe(PublicationService.Subscriber subscriber, Iterable<String> eventIds) {
            eventIds.forEach(eventId -> this.unsubscribe(subscriber, eventId));
        }

    }

    /**
     * This sub-interface defines a single method {@link Subscriber#onPublication(Publication)} that publication service
     * subscribers need to implement. When someone uses this service to submit a publication for a given event, this
     * method is called for all subscribers of the corresponding event id.
     */
    interface Subscriber extends Service.Subscriber {

        /**
         * This method is called by the publication service whenever a publication for one of the subscriber's event ids
         * has been sent by someone.
         *
         * @param publication The submitted publication
         */
        void onPublication(Publication publication);

    }

}
