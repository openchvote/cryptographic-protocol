/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework;

import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.exceptions.FrameworkException;
import ch.openchvote.base.framework.protocol.Phase;
import ch.openchvote.base.framework.protocol.Role;
import ch.openchvote.base.framework.services.EventService;
import ch.openchvote.base.framework.services.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static java.lang.System.Logger.Level.TRACE;
import static java.lang.System.Logger.Level.DEBUG;

/**
 * This class implements an event coordinator, which is responsible for initializing, conducting, and terminating one
 * specific election event. It uses an {@link EventService} to submit its coordination messages to the participating
 * parties and to receive their status notifications. For this, the coordinator needs to know the roles and ids of these
 * parties. For running multiple events simultaneously, different coordinators are required, one for each event.
 */
public abstract class Coordinator implements EventService.Coordinator {

    private final String id;
    private final Set<String> initializedEventIds;
    private final Set<String> terminatedEventIds;
    private final Set<String> abortedEventIds;
    private final Map<String, List<Status>> statusNotifications;
    private final Map<String, Map<Class<? extends Role>, Set<String>>> registeredParties;
    private final EventService.Source eventServiceSource;
    private final System.Logger logger;

    /**
     * Creates a new coordinator based on the given id.
     *
     * @param id The coordinator's unique id
     */
    public Coordinator(String id) {
        this.id = id;
        this.initializedEventIds = ConcurrentHashMap.newKeySet();
        this.terminatedEventIds = ConcurrentHashMap.newKeySet();
        this.abortedEventIds = ConcurrentHashMap.newKeySet();
        this.statusNotifications = new ConcurrentHashMap<>();
        this.registeredParties = new ConcurrentHashMap<>();
        this.eventServiceSource = Service.load(EventService.Source.class);
        this.logger = System.getLogger(this.getClass().getSimpleName());
    }

    /**
     * Registers multiple new party for participating in the event coordinated by the coordinator. The roles of the
     * parties must be one defined by the protocol. Roles are assigned to parties using the
     * {@link ch.openchvote.base.framework.annotations.party.Role.Annotations} annotation.
     *
     * @param partyIds The given party ids
     * @param eventId  The given event id
     * @param role     The given role
     */
    public void registerParties(Iterable<String> partyIds, String eventId, Class<? extends Role> role) {
        partyIds.forEach(partyId -> this.registerParty(partyId, eventId, role));
    }

    /**
     * Registers a new party for participating in the event coordinated by the coordinator. The role of the party must
     * be one defined by the protocol. Roles are assigned to parties using the
     * {@link ch.openchvote.base.framework.annotations.party.Role} annotation.
     *
     * @param partyId The given party id
     * @param eventId The given event id
     * @param role    The given role
     */
    public void registerParty(String partyId, String eventId, Class<? extends Role> role) {
        this.registeredParties
                .computeIfAbsent(eventId, e -> new HashMap<>())
                .computeIfAbsent(role, r -> new HashSet<>())
                .add(partyId);
    }

    /**
     * Initializes the event by informing all registered parties over the event service.
     *
     * @param eventId       The given event id
     * @param protocolId    The given protocol id
     * @param securityLevel The given security level
     */
    public void initializeEvent(String eventId, String protocolId, String securityLevel) {

        // check event id
        if (this.initializedEventIds.contains(eventId)) {
            throw new FrameworkException(FrameworkException.Type.INVALID_EVENT_ID);
        }

        // update event ids
        this.initializedEventIds.add(eventId);

        // send initialization message
        this.eventServiceSource.initialize(this, eventId, protocolId, securityLevel);

        // make logger entry
        this.log(DEBUG, eventId, "Event initialized");
    }

    /**
     * Terminates the event by informing all registered parties over the event service.
     *
     * @param eventId The given event id
     */
    public void terminateEvent(String eventId) {

        // check event id
        if (!this.initializedEventIds.contains(eventId)) {
            throw new FrameworkException(FrameworkException.Type.INVALID_EVENT_ID);
        }

        // send termination message
        this.eventServiceSource.terminate(eventId);

        // update event ids
        this.terminatedEventIds.add(eventId);

        // make logger entry
        this.log(DEBUG, eventId, "Event terminated");
    }

    /**
     * Aborts the event by informing all registered parties over the event service.
     *
     * @param eventId The given event id
     */
    public void abortEvent(String eventId) {

        // check event id
        if (!this.initializedEventIds.contains(eventId)) {
            throw new FrameworkException(FrameworkException.Type.INVALID_EVENT_ID);
        }

        // send abort message
        this.eventServiceSource.abort(eventId);

        // update event ids
        this.abortedEventIds.add(eventId);

        // make logger entry
        this.log(DEBUG, eventId, "Event aborted");
    }

    /**
     * Starts the given phase in the current event by informing all registered parties over the event service.
     *
     * @param eventId The given event id
     * @param phaseId The given phase id
     */
    public void startPhase(String eventId, String phaseId) {

        // check event id
        if (!this.initializedEventIds.contains(eventId)) {
            throw new FrameworkException(FrameworkException.Type.INVALID_EVENT_ID);
        }

        // start phase
        this.eventServiceSource.start(eventId, phaseId);

        // make logger entry
        var phaseName = Phase.getPrintName(phaseId);
        this.log(DEBUG, eventId, "Phase started", phaseName);
    }

    /**
     * Stops the given phase in the current event by informing all registered parties over the event service.
     *
     * @param eventId The given event id
     * @param phaseId The id of the phase to stop
     */
    public void stopPhase(String eventId, String phaseId) {

        // check event id
        if (!this.initializedEventIds.contains(eventId)) {
            throw new FrameworkException(FrameworkException.Type.INVALID_EVENT_ID);
        }

        // stop phase
        this.eventServiceSource.stop(eventId, phaseId);

        // make logger entry
        var phaseName = Phase.getPrintName(phaseId);
        this.log(DEBUG, eventId, "Phase stopped", phaseName);

    }

    @Override
    public String getId() {
        return this.id;
    }

    /**
     * Returns the event service being used by this coordinator. Can be used by subclasses if needed.
     *
     * @return The event service
     */
    protected EventService.Source getEventServiceSource() {
        return this.eventServiceSource;
    }

    @Override
    public synchronized void onStatus(Status status) {

        // add status to map
        var eventId = status.eventId();
        this.statusNotifications.computeIfAbsent(eventId, e -> new ArrayList<>()).add(status);

        // make logger entry
        var statusType = status.type();
        var phaseId = status.phaseId();
        var phaseName = Phase.getPrintName(phaseId);
        var senderId = status.senderId();
        this.log(TRACE, eventId, "Status received", statusType, phaseName, senderId);

        switch (statusType) {
            case READY -> this.handleReadyStatus(eventId, phaseId);
            case DONE -> this.handleDoneStatus(eventId, phaseId);
            case ERROR -> this.handleErrorStatus(eventId);
            case FINAL -> this.handleFinalStatus(eventId);
        }
    }

    protected abstract void handleReadyStatus(String eventId, String phaseId);

    protected abstract void handleDoneStatus(String eventId, String phaseId);

    protected abstract void handleErrorStatus(String eventId);

    protected abstract void handleFinalStatus(String eventId);

    // check if all registered parties are ready for a particular phase to start
    protected boolean readyToStart(String eventId, String phasedId) {
        var phase = Phase.getPhase(phasedId);
        var rolesNotifyingReady = Phase.getRolesNotifyingReady(phase);
        return rolesNotifyingReady.stream().allMatch(role -> this.hasStatusFromRegisteredParties(role, eventId, phasedId, Status.Type.READY));
    }

    protected boolean requiresStop(String phasedId) {
        var phase = Phase.getPhase(phasedId);
        var rolesNotifyingDone = Phase.getRolesNotifyingDone(phase);
        return !rolesNotifyingDone.isEmpty();
    }

    // check if all registered parties are ready for a particular phase to stop
    protected boolean readyToStop(String eventId, String phasedId) {
        var phase = Phase.getPhase(phasedId);
        var rolesNotifyingDone = Phase.getRolesNotifyingDone(phase);
        return rolesNotifyingDone.stream().allMatch(role -> this.hasStatusFromRegisteredParties(role, eventId, phasedId, Status.Type.DONE));
    }

    protected boolean hasStatusFromRegisteredParties(Class<? extends Role> role, String eventId, String phaseId, Status.Type type) {
        return this.registeredParties
                .getOrDefault(eventId, Map.of())
                .getOrDefault(role, Set.of())
                .stream()
                .allMatch(partyId -> this.hasStatusFromParty(partyId, eventId, phaseId, type));
    }

    protected synchronized boolean hasStatusFromParty(String partyId, String eventId, String phaseId, Status.Type type) {
        return this.statusNotifications.getOrDefault(eventId, List.of()).stream()
                .anyMatch(status -> status.phaseId().equals(phaseId) && status.senderId().equals(partyId) && status.type() == type);
    }

    // check if all registered parties are ready for an event to terminate
    protected boolean readyToTerminate(String eventId) {
        var roles = this.registeredParties.getOrDefault(eventId, Map.of()).keySet();
        return roles.stream().allMatch(role -> this.hasFinalStatusFromRegisteredParties(role, eventId));
    }

    protected boolean hasFinalStatusFromRegisteredParties(Class<? extends Role> role, String eventId) {
        return this.registeredParties.getOrDefault(eventId, Map.of()).getOrDefault(role, Set.of()).stream().allMatch(partyId -> this.hasFinalStatusFromRegisteredParty(partyId, eventId));
    }

    protected synchronized boolean hasFinalStatusFromRegisteredParty(String partyId, String eventId) {
        return this.statusNotifications.getOrDefault(eventId, List.of()).stream()
                .anyMatch(status -> status.senderId().equals(partyId) && status.type() == Status.Type.FINAL);
    }

    // check if exactly one registered party has reached an error state
    protected synchronized boolean readyToAbort(String eventId) {
        return this.statusNotifications.getOrDefault(eventId, List.of()).stream()
                .filter(status -> status.type() == Status.Type.ERROR)
                .count() == 1;
    }


    // private convenience method for making uniform logger entries
    protected void log(System.Logger.Level level, String eventId, String message, Object... logObjects) {
        var objectsString = "";
        if (logObjects != null && logObjects.length > 0) {
            objectsString = Arrays.stream(logObjects).map(Object::toString).collect(Collectors.joining(", ", "(", ")"));
        }
        var logString = String.format("%-11s %-28s : %s %s", eventId, this.id, message, objectsString);
        this.logger.log(level, logString);
    }

}
