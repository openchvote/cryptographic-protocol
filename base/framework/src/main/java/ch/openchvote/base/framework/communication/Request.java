/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.communication;

import ch.openchvote.base.framework.services.RequestResponseService;

/**
 * Instances of this class represent the requests to be transmitted over the {@link RequestResponseService}. All fields
 * are of type {@link String}. The class has no methods other than the getter methods for accessing the fields. Every
 * request has a unique internal id, which is created when constructing the object.
 */
public final class Request extends Communication {

    static private int ID_COUNTER = 1;

    private final String requesterId;
    private final String responderId;
    private final String requestId;

    /**
     * Constructs a new request object from the given parameters.
     *
     * @param eventId       The given event id
     * @param requesterId   The given requester id
     * @param responderId   The given responder id
     * @param contentId     The request's content id
     * @param contentString The request's content string
     */
    public Request(String eventId, String requesterId, String responderId, String contentId, String contentString) {
        super(eventId, contentId, contentString);
        this.requesterId = requesterId;
        this.responderId = responderId;
        this.requestId = this.getClass().getSimpleName() + "-" + ID_COUNTER++;
    }

    /**
     * Returns the requester id.
     *
     * @return The requester id
     */
    public String getRequesterId() {
        return this.requesterId;
    }

    /**
     * Returns the responder id.
     *
     * @return The responder id
     */
    public String getResponderId() {
        return this.responderId;
    }

    /**
     * Returns the request id.
     *
     * @return The request id
     */
    public String getRequestId() {
        return this.requestId;
    }

    @Override
    public String toString() {
        return String.format("Request={eventID=%s, requesterId=%s, responderId=%s, requestId=%s, contentId=%s}", this.eventId, this.requesterId, this.responderId, this.requestId, this.contentId);
    }

}
