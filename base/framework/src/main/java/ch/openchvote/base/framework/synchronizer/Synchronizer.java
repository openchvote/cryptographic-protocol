/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.synchronizer;

import java.util.concurrent.CountDownLatch;

/**
 * A synchronizer can stop the current thread until a notification from the synchronizer's notifier is received.
 * Internally, the blocking of the current thread is realized with a {@link CountDownLatch} initialized to 1. When the
 * blocking of the current thread is released, the synchronizer unsubscribes itself from the notifier. Therefore, a
 * synchronizer can not be used more than once.
 */
public class Synchronizer implements Subscriber {

    private final Notifier notifier;
    private final CountDownLatch countDownLatch;

    /**
     * Creates a new synchronizer for a given notifier.
     *
     * @param notifier The given notifier
     */
    public Synchronizer(Notifier notifier) {
        this.notifier = notifier;
        this.notifier.subscribe(this);
        this.countDownLatch = new CountDownLatch(this.notifier.isDone() ? 0 : 1);
    }

    /**
     * Stops the current thread until a notification from the synchronizer's notifier is received. When that happens,
     * the blocking of the current thread is released.
     */
    public void await() {
        try {
            this.countDownLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onNotification(Notifier notifier) {
        if (this.notifier == notifier) {
            this.countDownLatch.countDown();
        }
    }

}
