/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.annotations.service;

import ch.openchvote.base.framework.services.Service;

import java.lang.annotation.*;

/**
 * This annotation can be used to specify a priority value to a given service class. When loading a class for a given
 * service interface using the method {@link Service#load(Class)}, the class with the highest priority value is
 * selected. If multiple service classes are annotated with the same maximum priority value, one of them is selected
 * without specifying which one.
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Priority {

    /**
     * Returns the priority value assigned to the annotated service class. Without assigning such a priority value, the
     * default value is 0.
     *
     * @return The assigned priority value
     */
    int value() default 0;

}
