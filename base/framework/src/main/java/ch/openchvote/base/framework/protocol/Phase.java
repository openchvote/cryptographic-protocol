/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.protocol;

import ch.openchvote.base.framework.annotations.phase.Done;
import ch.openchvote.base.framework.annotations.phase.Ready;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.exceptions.FrameworkException;

import java.util.Set;

/**
 * This class serves as a common parent class for the phases defined for a given protocol. The phases are defined by the
 * classes implementing this interface, not by objects of these classes. This interface has no functionality other than
 * defining a common super-type for these phase classes. Some supplementary static convenience methods exist for
 * obtaining the id and print name of a given phase or information about its annotations.
 */
public abstract class Phase {

    /**
     * This is the default phase that is used for state classes that have no phase assigned.
     */
    static public class NONE extends Phase {

    }

    /**
     * Returns the id of a given phase. The id corresponds to the phase class's fully qualified name (the name of the
     * class prefixed with the package name), which is obtained by calling {@link Class#getName()}.
     *
     * @param phase The given phase
     * @return The id of the given phase
     */
    static public String getId(Class<? extends Phase> phase) {
        return phase.getName();
    }

    /**
     * Returns the phase for a given phase id. For this method to work, the id must correspond to the phase class's
     * fully qualified name. An exception is thrown if no such phase exists.
     *
     * @param phaseId The given phase id
     * @return The corresponding phase
     * @throws FrameworkException if no phase exists for the given phase id
     */
    @SuppressWarnings("unchecked")
    static public Class<? extends Phase> getPhase(String phaseId) {
        try {
            return (Class<? extends Phase>) Class.forName(phaseId);
        } catch (ClassNotFoundException exception) {
            throw new FrameworkException(FrameworkException.Type.PHASE_CLASS_NOT_FOUND, exception, phaseId);
        }
    }

    /**
     * Returns the print name of the phase for a given phase id. The name is obtained by calling
     * {@link Class#getSimpleName()}.
     *
     * @param phaseId The given phase id
     * @return The print name of the corresponding phase
     */
    static public String getPrintName(String phaseId) {
        var phase = Phase.getPhase(phaseId);
        return Phase.getPrintName(phase);
    }

    /**
     * Returns the print name of a given phase, which is obtained by calling {@link Class#getSimpleName()}.
     *
     * @param phase The given phase
     * @return The print name of the given phase
     */
    static public String getPrintName(Class<? extends Phase> phase) {
        return phase.getSimpleName();
    }

    /**
     * Returns the set of roles that are expected to send {@link Status.Type#READY} notifications for a given phase.
     * These roles can be defined for a phase using the {@link Ready} annotation.
     *
     * @param phase The given phase
     * @return The set of roles sending {@link Status.Type#READY} notifications
     */
    static public Set<Class<? extends Role>> getRolesNotifyingReady(Class<? extends Phase> phase) {
        var annotation = Ready.class;
        return phase.isAnnotationPresent(annotation)
                ? Set.of(phase.getAnnotation(annotation).value())
                : Set.of();
    }

    /**
     * Returns the set of roles that are expected to send {@link Status.Type#DONE} notifications for a given phase.
     * These roles can be defined for a phase using the {@link Done} annotation.
     *
     * @param phase The given phase
     * @return The set of roles sending {@link Status.Type#DONE} notifications
     */
    static public Set<Class<? extends Role>> getRolesNotifyingDone(Class<? extends Phase> phase) {
        var annotation = Done.class;
        return phase.isAnnotationPresent(annotation)
                ? Set.of(phase.getAnnotation(annotation).value())
                : Set.of();
    }

}
