/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.exceptions;

import ch.openchvote.base.framework.party.State;

/**
 * This class is used for throwing specific exceptions that happen upon dealing with states. The different types of
 * state exceptions are represented as an internal enum class.
 */
public final class StateException extends TypedException {

    /**
     * This enum type defines different types of state exceptions.
     */
    @SuppressWarnings("MissingJavadoc")
    public enum Type implements TypedException.Type {
        UNDEFINED_MESSAGE_HANDLER,
        DUPLICATE_MESSAGE_HANDLER,
        UNDEFINED_MAIL_HANDLER,
        DUPLICATE_MAIL_HANDLER,
        UNDEFINED_REQUEST_HANDLER,
        DUPLICATE_REQUEST_HANDLER,
        UNDEFINED_RESPONSE_HANDLER,
        DUPLICATE_RESPONSE_HANDLER,
        UNDEFINED_INPUT_HANDLER,
        DUPLICATE_INPUT_HANDLER,
        UNDEFINED_OUTPUT_HANDLER,
        DUPLICATE_OUTPUT_HANDLER,
        UNDEFINED_PUBLICATION_HANDLER,
        DUPLICATE_PUBLICATION_HANDLER;

        @Override
        public String getId() {
            return this.name().toUpperCase();
        }
    }

    /**
     * Constructs a new state exception for the given parameters.
     *
     * @param type The exception type
     * @param state The state causing the exception
     * @param cause The origin of the exception
     */
    public StateException(Type type, State<?, ?> state, Throwable cause) {
        super(type, cause, state);
    }

    /**
     * Constructs a new state exception for the given parameters.
     *
     * @param type The exception type
     * @param state The state causing the exception
     */
    public StateException(Type type, State<?, ?> state) {
        super(type, state);
    }

}
