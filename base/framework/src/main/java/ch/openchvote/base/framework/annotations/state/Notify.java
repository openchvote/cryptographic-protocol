/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.annotations.state;

import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;

import java.lang.annotation.*;

/**
 * This annotation can be used to specify that a transition into the annotated {@link State} class should be notified by
 * parties reaching such a state. They are expected to submit notifications of the given {@link Status.Type} using their
 * event service. To allow the assignment of multiple role types to a single state class, the annotation is declared as
 * repeatable. The implementation of a repeatable annotation requires the existence of a so-called holder annotation,
 * which stores all the {@link Notify} annotations in an array. In this particular case, the holder annotation is
 * defined as an inner annotation type {@link Annotations}.
 */
@Documented
@Repeatable(Notify.Annotations.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Notify {

    /**
     * Returns the status type assigned to the annotated state class.
     *
     * @return The assigned status type
     */
    Status.Type value();

    /**
     * The purpose of this inner holder annotation type is to allow repeatable {@link Notify} annotations in state
     * classes. It should never be used on its own, except for calling its method {@link Notify.Annotations#value()} for
     * obtaining the array of all {@link Notify} annotations assigned to a given state class,
     */
    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @interface Annotations {

        /**
         * Returns an array of all {@link Notify} annotations assigned to a given state class,
         *
         * @return An array of all assigned {@link Notify} annotations
         */
        Notify[] value() default {};

    }

}
