/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.protocol;

import ch.openchvote.base.framework.annotations.protocol.Roles;
import ch.openchvote.base.framework.exceptions.FrameworkException;

import java.util.Set;

/**
 * This class serves as a common parent class for different protocols. The protocols are defined by the classes
 * implementing this interface, not by objects of these classes. This interface has no functionality other than defining
 * a common super-type for these protocol classes. Some supplementary static convenience methods exist for obtaining the
 * id and print name of a given protocol or information about their annotations.
 */
public abstract class Protocol {

    /**
     * Returns the id of a given protocol. The id corresponds to the protocol class's fully qualified name (the name of
     * the class prefixed with the package name), which is obtained by calling {@link Class#getName()}.
     *
     * @param protocol The given protocol
     * @return The id of the given protocol
     */
     static public String getId(Class<? extends Protocol> protocol) {
        return protocol.getName();
    }

    /**
     * Returns the protocol for a given protocol id. For this method to work, the id must correspond to the fully
     * qualified name of the protocol class. An exception is thrown if no such class exists.
     *
     * @param protocolId The given protocol id
     * @return The corresponding protocol
     * @throws FrameworkException if no protocol exists for the given protocol id
     */
    @SuppressWarnings("unchecked")
    static public Class<? extends Protocol> getProtocol(String protocolId) {
        try {
            return (Class<? extends Protocol>) Class.forName(protocolId);
        } catch (ClassNotFoundException exception) {
            throw new FrameworkException(FrameworkException.Type.PROTOCOL_CLASS_NOT_FOUND, exception, protocolId);
        }
    }

    /**
     * Returns the print name of the given protocol id. The name is obtained by calling {@link Class#getSimpleName()}.
     *
     * @param protocolId The given protocol id
     * @return The print name of the corresponding protocol
     */
    static public String getPrintName(String protocolId) {
        var protocol = Protocol.getProtocol(protocolId);
        return Protocol.getPrintName(protocol);
    }

    /**
     * Returns the print name of a given protocol, which is obtained by calling {@link Class#getSimpleName()}.
     *
     * @param protocol The given protocol
     * @return The print name of the given protocol
     */
    static public String getPrintName(Class<? extends Protocol> protocol) {
        return protocol.getSimpleName();
    }

    /**
     * Returns the set of roles assigned to a given protocol using the {@link Roles} annotation.
     *
     * @param protocol The given protocol
     * @return The set of roles assigned to a given protocol
     */
    static public Set<Class<? extends Role>> getAssignedRoles(Class<? extends Protocol> protocol) {
        var annotationClass = Roles.class;
        return protocol.isAnnotationPresent(annotationClass)
                ? Set.of(protocol.getAnnotation(annotationClass).value())
                : Set.of();
    }

}
