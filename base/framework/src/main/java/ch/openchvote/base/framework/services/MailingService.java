/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.communication.Mail;

/**
 * A mailing service can be used by a party participating in a protocol to deliver a mail to another protocol
 * participant. The interface provides methods for subscribing a party to the mailing service, unsubscribing a party
 * from the mailing service, and for sending a {@link Mail} to a subscribed party. Upon receiving a mail, the receiver's
 * method {@link MailingService.Subscriber#onMail(Mail)} is called. Mails are unsigned.
 */
public interface MailingService {

    /**
     * This sub-interface defines the sender's part of the mailing service needed for delivering mails to other
     * parties.
     */
    interface Source extends Service {

        /**
         * Delivers the given mail to the intended receiver.
         *
         * @param mail The given mail
         */
        void deliver(Mail mail);

    }

    /**
     * This sub-interface defines the subscriber's part of the mailing service. It provides methods for subscribing and
     * unsubscribing.
     */
    interface Target extends Service {

        /**
         * Subscribes the given subscriber to the mailing service. Nothing happens if the subscriber is already
         * subscribed.
         *
         * @param subscriber The given subscriber
         */
        void subscribe(MailingService.Subscriber subscriber);

        /**
         * Unsubscribes the given subscriber from the mailing service. Nothing happens if currently the subscriber is
         * not subscribed to the mailing service.
         *
         * @param subscriber The given subscriber
         */
        void unsubscribe(MailingService.Subscriber subscriber);

    }

    /**
     * This sub-interface defines the method that a mailing service subscriber must provide for receiving mails.
     */
    interface Subscriber extends Service.Subscriber {

        /**
         * This method is called upon receiving an incoming mail.
         *
         * @param mail The received mail
         */
        void onMail(Mail mail);

    }

}
