/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.communication;

/**
 * This is an abstract parent class for different communication classes carrying some content. Instances of this type
 * are tied to an event id, and they provide a content id specifying the type of the content and a string representation
 * of the content itself.
 */
public sealed abstract class Communication permits Input, Mail, Output, Request, TestData, Communication.Signed {

    protected final String eventId;
    protected final String contentId;
    protected final String contentString;

    // protected constructor for internal use in subclasses
    protected Communication(String eventId, String contentId, String contentString) {
        this.eventId = eventId;
        this.contentId = contentId;
        this.contentString = contentString;
    }

    /**
     * Returns the event id.
     *
     * @return The event id
     */
    public String getEventId() {
        return this.eventId;
    }

    /**
     * Returns the content id.
     *
     * @return The content id
     */
    public String getContentId() {
        return this.contentId;
    }

    /**
     * Returns a string representation of the content.
     *
     * @return A string representation of the content
     */
    public String getContentString() {
        return this.contentString;
    }

    /**
     * This abstract subclass is a specialization of its parent class for communication classes in which the transmitted
     * content is signed. In addition to the fields defined for the parent classe, it provides a signer id and a string
     * representation of the signature itself.
     */
    public sealed abstract static class Signed extends Communication permits Message, Publication, Response {

        private final String signerId;
        protected final String signature;

        // protected constructor for internal use in subclasses
        protected Signed(String eventId, String contentId, String contentString, String signerId, String signature) {
            super(eventId, contentId, contentString);
            this.signerId = signerId;
            this.signature = signature;
        }

        /**
         * Returns the signer id.
         *
         * @return The signer id
         */
        public String getSignerId() {
            return this.signerId;
        }

        /**
         * Returns a string representation of the signature.
         *
         * @return A string representation of the signature
         */
        public String getSignature() {
            return this.signature;
        }

    }

}
