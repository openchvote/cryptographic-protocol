/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.synchronizer;

/**
 * Instances of this class are notifiers that keep track of a counter that counts down from its initial value to 0. When
 * 0 is reached, a notification is sent to all subscribers from the notifier's list of subscribers. Since the event of
 * reaching 0 happens at most once, sending the notifications also happens at most once. This class is thread-safe.
 */
public class CountDownNotifier extends Notifier {

    // the notifier's counter
    private int counter;

    /**
     * Constructs a new countdown notifier with its counter initialized to the given value.
     *
     * @param value The given value
     */
    public CountDownNotifier(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("Counter must be a positive number");
        }
        this.counter = value;
    }

    @Override
    public boolean isDone() {
        return this.counter == 0;
    }

    /**
     * Performs a single countdown operation, which means that unless it has already reached 0, the notifier's counter
     * is decreased by 1.
     */
    public synchronized void countDown() {
        if (this.counter != 0) {
            this.counter--;
            if (this.counter == 0) {
                this.notifySubscribers();
            }
        }
    }

}
