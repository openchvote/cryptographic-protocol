/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.party.State;

/**
 * The purpose of this service is to enable parties consuming this service to activate themselves during their
 * involvement in an event. Usually, this will be necessary in a state with a guard that triggers a transition. In such
 * a case, it is typical that the guard's condition will have to be checked multiple times until it becomes true. This
 * interface allows an implementation of this service to decide whether the activation is conducted in the same or in a
 * separate thread.
 */
public interface SelfActivationService extends Service {

    /**
     * Triggers the self-activation of the service's consumer for the given event. Upon calling, this method executes
     * the given handler.
     *
     * @param consumer          A self-reference to the service's consumer
     * @param eventId           The given event
     * @param activationHandler The handler to execute
     */
    void selfActivate(Consumer consumer, String eventId, java.util.function.Consumer<State<?, ?>> activationHandler);

    /**
     * This sub-interface defines the method that a service consumer must provide.
     */
    interface Consumer {

        /**
         * This method is called by the service when the consumer's activation is triggered for the given event id and
         * activation handler.
         *
         * @param eventId           The given event id
         * @param activationHandler The given activation handler
         */
        void onSelfActivation(String eventId, java.util.function.Consumer<State<?, ?>> activationHandler);

    }

}
