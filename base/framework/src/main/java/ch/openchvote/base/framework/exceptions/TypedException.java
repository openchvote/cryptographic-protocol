/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.exceptions;

import ch.openchvote.base.framework.interfaces.Identifiable;

import java.util.Arrays;

/**
 * This is an abstract base exception class for exceptions in this framework. The common properties of these exceptions
 * are an exception type (which will usually be implemented by an enum subclass of {@link TypedException.Type}) and an
 * object causing the exceptions.
 */
public abstract class TypedException extends RuntimeException {

    /**
     * This is the common interface for different exception types.
     */
    public interface Type extends Identifiable {

    }

    /**
     * The type of the exception
     */
    private final Type type;

    /**
     * The object causing the exception
     */
    private final Object[] objects;

    /**
     * Constructs a typed exception from the given parameters.
     *
     * @param type    The type of the exception
     * @param objects The objects responsible for causing the exception
     */
    public TypedException(Type type, Object... objects) {
        super(type.getId());
        this.type = type;
        this.objects = objects;
    }

    /**
     * Constructs a typed exception from the given parameters.
     *
     * @param type    The type of the exception
     * @param cause   The origin of the exception
     * @param objects The objects responsible for causing the exception
     */
    public TypedException(Type type, Throwable cause, Object... objects) {
        super(type.getId(), cause);
        this.type = type;
        this.objects = objects;
    }

    /**
     * Returns the type of this exception.
     *
     * @return The type of this exception
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Returns the objects causing this exception.
     *
     * @return The objects causing this exception
     */
    public Object getObjects() {
        return this.objects;
    }

    @Override
    public String toString() {
        return String.format("%s[type=%s,objects=%s]", this.getClass().getSimpleName(), this.type, Arrays.toString(this.objects));
    }

}
