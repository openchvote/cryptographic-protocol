/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.exceptions;

import ch.openchvote.base.framework.communication.Communication;

/**
 * This class is used for throwing specific exceptions related the communication between parties. The different types of
 * communication exceptions are represented as an internal enum class.
 */
public final class CommunicationException extends TypedException {

    /**
     * This enum type defines different types of communication exceptions.
     */
    @SuppressWarnings("MissingJavadoc")
    public enum Type implements TypedException.Type {
        INCONSISTENT_EVENT_SETUP,
        INVALID_CONTENT,
        INVALID_SIGNATURE,
        UNKNOWN_CERTIFICATE,
        UNKNOWN_RECEIVER,
        UNKNOWN_REQUESTER,
        UNKNOWN_RESPONDER,
        WRONG_RECEIVER;

        @Override
        public String getId() {
            return this.name().toUpperCase();
        }
    }

    /**
     * Constructs an instance of this exception from the given parameters.
     * @param type The type of the exception
     */
    public CommunicationException(Type type) {
        super(type);
    }

    /**
     * Constructs an instance of this exception from the given parameters.
     * @param type The type of the exception
     * @param communication The communication causing the exception
     */
    public CommunicationException(Type type, Communication communication) {
        super(type, communication);
    }

    /**
     * Constructs an instance of this exception from the given parameters.
     * @param type The type of the exception
     * @param communication The message causing the exception
     * @param cause The origin of the exception
     */
    public CommunicationException(Type type, Communication communication, Throwable cause) {
        super(type, cause, communication);
    }

}
