/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.services;

import ch.openchvote.base.framework.security.Certificate;

import java.util.Optional;

/**
 * This is a simple service interface for requesting new certificates and retrieving existing certificates.
 */
public interface CertificateService extends Service {

    /**
     * Requests a new certificate for the given parameters.
     *
     * @param publicKey     The subject's public key
     * @param subject       The certificate's subject
     * @param type          The type of the certificate
     * @param algorithmName The algorithm name
     * @param securityLevel The security level
     * @return The certificate
     */
    @SuppressWarnings("UnusedReturnValue")
    Certificate requestCertificate(String publicKey, String subject, Certificate.Type type, String algorithmName, String securityLevel);

    /**
     * Returns a certificate, if any, for the given parameters.
     *
     * @param subject       The subject of the certificate
     * @param type          The type of the certificate
     * @param algorithmName The algorithm name
     * @param securityLevel The security level
     * @return An optional that may contain the respective certificate
     */
    Optional<Certificate> retrieveCertificate(String subject, Certificate.Type type, String algorithmName, String securityLevel);

}
