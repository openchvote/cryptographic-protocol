/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.synchronizer.collector;

import ch.openchvote.base.framework.communication.Publication;
import ch.openchvote.base.framework.services.PublicationService;
import ch.openchvote.base.framework.services.Service;

/**
 * This class implements a specific event data collector that listens to a {@link PublicationService} for a given event.
 * It collects all the publications up to a certain amount. When that amount of data is reached, a notification is sent
 * to every subscriber of the collector's list of subscribers.
 */
public class PublicationCollector extends EventDataCollector<Publication> implements PublicationService.Subscriber {

    // used for creating unique ids
    static private int ID_COUNTER = 1;

    // the collector's publication service
    private PublicationService.Target publicationServiceTarget;

    /**
     * This is the general constructor of this class. It defines the event id and the number of expected publications.
     *
     * @param eventId              The given event id
     * @param numberOfPublications The expected number of publications
     */
    public PublicationCollector(String eventId, int numberOfPublications) {
        super(eventId, numberOfPublications);
    }

    @Override
    public void onPublication(Publication publication) {
        var eventId = publication.getEventId();
        this.onData(eventId, publication);
    }

    @Override
    protected void loadServices() {
        this.publicationServiceTarget = Service.load(PublicationService.Target.class);
    }

    @Override
    public void subscribeToServices() {
        this.publicationServiceTarget.subscribe(this, this.eventId);
    }

    @Override
    public void unsubscribeFromServices() {
        this.publicationServiceTarget.unsubscribe(this, this.eventId);
    }

    @Override
    public String getId() {
        return this.getClass().getSimpleName() + "-" + ID_COUNTER++;
    }

}
