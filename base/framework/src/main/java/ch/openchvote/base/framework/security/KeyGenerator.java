/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.security;

import ch.openchvote.base.framework.services.EncryptionService;
import ch.openchvote.base.framework.services.SignatureService;

import java.util.Set;

/**
 * This interface defines the basic functionality of a key generator, which allows generating fresh key pairs
 * for either a given {@link EncryptionService} or {@link SignatureService}.
 */
public interface KeyGenerator {

    /**
     * Returns the set of supported security levels.
     *
     * @return The set of supported security levels
     */
    Set<String> getSecurityLevels();

    /**
     * Generates a key pair for a given security level.
     *
     * @param securityLevel The desired security level
     * @return The generated key pair
     */
    KeyPair generateKeyPair(String securityLevel);

}
