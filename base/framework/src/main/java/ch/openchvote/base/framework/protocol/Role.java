/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.protocol;

import ch.openchvote.base.framework.exceptions.FrameworkException;

/**
 * This class serves as a common parent class for the roles defined for a given protocol. The roles are defined by the
 * classes implementing this interface, not by objects of these classes. This interface has no functionality other than
 * defining a common super-type for these role classes. Some supplementary static convenience methods exist for
 * obtaining the id and print name of a given role or information about their annotations.
 */
public abstract class Role {

    /**
     * Returns the id of a given role. The id corresponds to the role class's fully qualified name (the name of the
     * class prefixed with the package name), which is obtained by calling {@link Class#getName()}.
     *
     * @param role The given role
     * @return The id of the given role
     */
    static public String getId(Class<? extends Role> role) {
        return role.getName();
    }

    /**
     * Returns the role for a given role id. For this method to work, the id must correspond to the fully qualified name
     * of the role class. An exception is thrown if no such class exists.
     *
     * @param roleId The given role id
     * @return The corresponding role
     * @throws FrameworkException if no role exists for the given role id
     */
    @SuppressWarnings("unchecked")
    static public Class<? extends Role> getRole(String roleId) {
        try {
            return (Class<? extends Role>) Class.forName(roleId);
        } catch (ClassNotFoundException exception) {
            throw new FrameworkException(FrameworkException.Type.ROLE_CLASS_NOT_FOUND, exception, roleId);
        }
    }

    /**
     * Returns the print name of the given role id. The name is obtained by calling {@link Class#getSimpleName()}.
     *
     * @param roleId The given role id
     * @return The print name of the corresponding role
     */
    static public String getPrintName(String roleId) {
        var role = Role.getRole(roleId);
        return Role.getPrintName(role);
    }

    /**
     * Returns the print name of a given role, which is obtained by calling {@link Class#getSimpleName()}.
     *
     * @param role The given role
     * @return The print name of the given role
     */
    static public String getPrintName(Class<? extends Role> role) {
        return role.getSimpleName();
    }

}
