/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.exceptions;

/**
 * This class is used for throwing specific exceptions that happen when executing a task. The different types of task
 * exceptions are represented as an internal enum class.
 */
public final class TaskException extends TypedException {

    /**
     * This enum type defines different types of task exceptions.
     */
    @SuppressWarnings("MissingJavadoc")
    public enum Type implements TypedException.Type {
        INVALID_ZKP_PROOF,
        INVALID_BALLOT,
        INVALID_CONFIRMATION,
        DUPLICATE_BALLOT,
        DUPLICATE_CONFIRMATION,
        MISSING_BALLOT,
        VERIFICATION_CODE_MISMATCH,
        PARTICIPATION_CODE_MISMATCH,
        INSPECTION_CODE_MISMATCH,
        VALUES_NOT_EQUAL;

        @Override
        public String getId() {
            return this.name().toUpperCase();
        }
    }

    /**
     * Constructs a new task exception from the given parameters.
     *
     * @param type The type of the task exception
     * @param taskClass The task class causing the exception
     * @param cause The origin of the exception
     */
    public TaskException(Type type, Class<?> taskClass, Throwable cause) {
        super(type, cause, taskClass);
    }

    /**
     * Constructs a new task exception from the given parameters.
     *
     * @param type The type of the task exception
     * @param taskClass The task class causing the exception
     */
    public TaskException(Type type, Class<?> taskClass) {
        super(type, taskClass);
    }

}
