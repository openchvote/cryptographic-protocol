/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.exceptions;

/**
 * This class is used for throwing specific exceptions while accessing event data objects. The different types of
 * event data exceptions are represented as an internal enum class.
 */
public final class EventContextException extends TypedException {

    /**
     * This enum type defines different types of event context exceptions.
     */
    @SuppressWarnings("MissingJavadoc")
    public enum Type implements TypedException.Type {
        EVENT_CONTEXT_SEALED,
        INVALID_READ_ACCESS,
        INVALID_WRITE_ACCESS,
        SYNCHRONIZATION_INTERRUPTED,
        UNKNOWN_EVENT_ID;

        @Override
        public String getId() {
            return this.name().toUpperCase();
        }
    }

    /**
     * Creates an exception instance from the given parameters.
     * @param type The type of the exception
     * @param source The object causing the exception
     * @param cause The origin of the exception
     */
    public EventContextException(Type type, Object source, Throwable cause) {
        super(type, cause, source);
    }

    /**
     * Creates an exception instance from the given parameters.
     * @param type The type of the exception
     * @param source The object causing the exception
     */
    public EventContextException(Type type, Object source) {
        super(type, source);
    }

}