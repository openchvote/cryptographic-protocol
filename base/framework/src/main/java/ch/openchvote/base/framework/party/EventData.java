/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.party;

import ch.openchvote.base.framework.exceptions.EventContextException;
import ch.openchvote.base.framework.exceptions.FrameworkException;

import java.util.*;
import java.util.function.Function;

/**
 * This is the common interface for different event data classes of specific parties in different protocols.
 */
public interface EventData {

    /**
     * Returns a shallow copy of this event data by iterating through all instance variable using Java reflection. This
     * method is called from {@link EventContext#getCopy()} to obtain a copy of the event data object.
     *
     * @return A shallow copy of this event data
     */
    default EventData getCopy() {
        var eventDataClass = this.getClass();
        try {
            var constructor = eventDataClass.getDeclaredConstructor(eventDataClass);
            constructor.setAccessible(true);
            return constructor.newInstance(this);
        } catch (ReflectiveOperationException exception) {
            throw new FrameworkException(FrameworkException.Type.MISSING_CONSTRUCTOR, exception, eventDataClass);
        }
    }

    /**
     * This is a generic data container class for event data values. A newly created data container is empty until its
     * value has been set. After setting the data container's value, it cannot be overridden. This class is
     * thread-safe.
     *
     * @param <V> The generic type of the value
     */
    class Data<V> implements Iterable<V> {

        private V value = null;

        /**
         * Constructs an empty data container, i.e.,  without setting its value.
         */
        public Data() {
        }

        /**
         * Constructs a new data container from a given iterable collection of values. There are three options: if the
         * iterable collection is empty, an empty data container is constructed; if the iterable collection contains a
         * single value, a data container containing the single value is constructed; if the iterable collection
         * contains more than one value, an exception is thrown. The main purpose of this constructor is to offer a
         * deserialization interface similar to the one defined for {@link DataMap}.
         *
         * @param values The given iterable collection of values
         * @throws EventContextException if the iterable collection contains more than one value
         */
        public Data(Iterable<V> values) {
            this();
            values.forEach(this::set);
        }

        /**
         * Checks if the value is already present.
         *
         * @return {@code true} if the value is already present, {@code false} otherwise
         */
        public synchronized boolean isPresent() {
            return this.value != null;
        }

        /**
         * Returns the value stored in the data container.
         *
         * @return The value stored in the data container
         * @throws EventContextException if the value has not been set
         */
        public synchronized V get() throws EventContextException {
            if (this.value == null) {
                throw new EventContextException(EventContextException.Type.INVALID_READ_ACCESS, this);
            }
            return this.value;
        }

        /**
         * Stores the given value into the data container.
         *
         * @param value The given value
         * @throws EventContextException if the value already exist
         */
        public synchronized void set(V value) throws EventContextException {
            if (this.value != null) {
                throw new EventContextException(EventContextException.Type.INVALID_WRITE_ACCESS, this);
            }
            this.value = value;
        }

        @Override
        public String toString() {
            return this.isPresent() ? this.value.toString() : "null";
        }

        @Override
        public Iterator<V> iterator() {
            if (this.isPresent()) {
                return Collections.singleton(this.value).iterator();
            } else {
                return Collections.emptyIterator();
            }
        }

    }

    /**
     * This is a generic data container class for indexed event data values. Newly created data maps are empty. Once a
     * value is set for a given index, in can no longer be overridden. The range of the permitted indices is
     * unrestricted. When all values are set, the collection of values can be mapped into a target data structure (list,
     * set, vector, matrix, etc.) using the method {@link DataMap#mapTo}. Once such a target data structure has been
     * generated, the data map is locked, i.e., it is no longer permitted to add further values to the container. This
     * class is thread-safe.
     *
     * @param <V> The generic type of the values
     */
    class DataMap<V> implements Iterable<Map.Entry<Integer, V>> {

        private final SortedMap<Integer, V> map;
        private boolean locked = false;

        /**
         * Constructs an empty data map.
         */
        public DataMap() {
            this(Collections::emptyIterator);
        }

        /**
         * Constructs a new data map from a given iterable collection of (index,value)-pairs of type {@link Map.Entry}.
         * The main purpose of this constructor is to offer a deserialization interface similar to the one defined for
         * {@link Data}.
         *
         * @param indexValuePairs The given iterable collection of (index,value)-pairs
         */
        public DataMap(Iterable<Map.Entry<Integer, V>> indexValuePairs) {
            this.map = new TreeMap<>();
            indexValuePairs.forEach(indexValuePair -> this.set(indexValuePair.getKey(), indexValuePair.getValue()));
        }

        /**
         * Sets a value for a given index. Throws an exception if a value already exists for the given index or if the
         * data map has been locked.
         *
         * @param index The index
         * @param value The value
         * @throws EventContextException if the target value is null or if the map contains the index
         */
        public synchronized void set(int index, V value) throws EventContextException {
            if (this.locked || this.map.containsKey(index)) {
                throw new EventContextException(EventContextException.Type.INVALID_WRITE_ACCESS, this);
            }
            this.map.put(index, value);
        }

        /**
         * Returns the value associated to a given index. Throws an exception, if no value for the given index is
         * available in the data map.
         *
         * @param index The index
         * @return The value associated to the given index
         * @throws EventContextException if no value for the given index is available
         */
        public synchronized V get(int index) throws EventContextException {
            var value = this.map.get(index);
            if (value == null) {
                throw new EventContextException(EventContextException.Type.INVALID_READ_ACCESS, this);
            }
            return value;
        }

        /**
         * Checks if a given index exists in the data map.
         *
         * @param index The index
         * @return {@code true} if the index exists, {@code false} otherwise
         */
        public synchronized boolean isPresent(int index) {
            return this.map.containsKey(index);
        }

        /**
         * Checks if all indices of a given iterable collection of indices exist in the data map.
         *
         * @param indices An iterable collection of integer indices
         * @return {@code true} if the all indices exist, {@code false} otherwise
         */
        public synchronized boolean arePresent(Iterable<Integer> indices) {
            for (int index : indices) {
                if (!this.isPresent(index)) {
                    return false;
                }
            }
            return true;
        }

        /**
         * Maps the (index,value)-pairs of the given data map into a target data structure of type {@link W}. The
         * mapping is defined by the given function, which maps a set of objects of type {@code Map.Entry<Integer, V>}
         * into an object of type {@link W}. To ensure consistent results when calling this method multiple times,
         * possibly for different target tpes, the data map is locked at the first invocation of this method.
         *
         * @param function The function that defines the map into a target data structure
         * @param <W>      The generic type of the target data structure
         * @return The resulting target data structure
         */
        public synchronized <W> W mapTo(Function<? super Set<Map.Entry<Integer, V>>, W> function) {
            this.locked = true;
            return function.apply(this.map.entrySet());
        }

        @Override
        public Iterator<Map.Entry<Integer, V>> iterator() {
            return this.map.entrySet().iterator();
        }

        @Override
        public String toString() {
            return this.map.toString();
        }

    }

}
