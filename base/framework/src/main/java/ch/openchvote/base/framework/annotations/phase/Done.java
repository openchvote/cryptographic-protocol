/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.annotations.phase;

import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.protocol.Role;
import ch.openchvote.base.framework.services.EventService;

import java.lang.annotation.*;

/**
 * For a phase annotated with {@link Done} to complete, all parties assigned with one of the specified roles are
 * required to finish the phase. Corresponding status notifications can be submitted by calling
 * {@link EventService.Target#notifyStatus} with the value {@link Status.Type#DONE} included in the status.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Done {

    /**
     * Returns an array of the roles assigned to the phase using this annotation. These roles define the
     * pre-condition for the phase to complete.
     *
     * @return The array of roles assigned to the phase
     */
    Class<? extends Role>[] value() default {};

}
