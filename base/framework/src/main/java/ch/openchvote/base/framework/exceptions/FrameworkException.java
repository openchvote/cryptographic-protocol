/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.base.framework.exceptions;

/**
 * This class is used for throwing specific exceptions that happen in the context of the framework. The different types
 * of framework exceptions are represented as an internal enum class.
 */
public class FrameworkException extends TypedException {

    /**
     * This enum type defines different types of framework exceptions.
     */
    @SuppressWarnings("MissingJavadoc")
    public enum Type implements TypedException.Type {
        CONTENT_CLASS_NOT_FOUND,
        EVENT_CONTEXT_CLASS_NOT_FOUND,
        INVALID_EVENT_ID,
        INVALID_CONTENT_ID,
        INVALID_NUMBER_OF_ROLES_ASSIGNED,
        MISSING_ANNOTATION,
        MISSING_CONSTRUCTOR,
        MISSING_PRIVATE_KEY,
        PHASE_CLASS_NOT_FOUND,
        PROTOCOL_CLASS_NOT_FOUND,
        ROLE_CLASS_NOT_FOUND,
        SERVICE_NOT_AVAILABLE,
        STATE_CLASS_NOT_FOUND,
        THREAD_INTERRUPTION_EXCEPTION,
        UNKNOWN_CONFIGURATION,
        UNKNOWN_SECURITY_LEVEL;

        @Override
        public String getId() {
            return this.name().toUpperCase();
        }
    }

    /**
     * Constructs a new framework exception for the given parameters.
     *
     * @param type    The exception type
     * @param objects Objects related to the creation of this exception
     */
    public FrameworkException(Type type, Object... objects) {
        super(type, objects);
    }

    /**
     * Constructs a new framework exception for the given parameters.
     *
     * @param type    The exception type
     * @param cause   The origin of the exception
     * @param objects Objects related to the creation of this exception
     */
    public FrameworkException(Type type, Throwable cause, Object... objects) {
        super(type, cause, objects);
    }

}
