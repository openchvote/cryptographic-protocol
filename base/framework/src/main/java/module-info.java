/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import ch.openchvote.base.framework.services.*;

/**
 * This module is an implementation of the OpenCHVote protocol framework.
 */
module ch.openchvote.base.framework {

    exports ch.openchvote.base.framework;
    exports ch.openchvote.base.framework.annotations.content;
    exports ch.openchvote.base.framework.annotations.party;
    exports ch.openchvote.base.framework.annotations.phase;
    exports ch.openchvote.base.framework.annotations.protocol;
    exports ch.openchvote.base.framework.annotations.service;
    exports ch.openchvote.base.framework.annotations.state;
    exports ch.openchvote.base.framework.communication;
    exports ch.openchvote.base.framework.exceptions;
    exports ch.openchvote.base.framework.interfaces;
    exports ch.openchvote.base.framework.party;
    exports ch.openchvote.base.framework.protocol;
    exports ch.openchvote.base.framework.security;
    exports ch.openchvote.base.framework.services;
    exports ch.openchvote.base.framework.synchronizer;
    exports ch.openchvote.base.framework.synchronizer.collector;
    exports ch.openchvote.base.framework.synchronizer.monitor;

    uses CertificateService;
    uses EncryptionService;
    uses EventService.Source;
    uses EventService.Target;
    uses KeystoreService;
    uses MessagingService.Source;
    uses MessagingService.Target;
    uses MailingService.Source;
    uses MailingService.Target;
    uses PersistenceService;
    uses PublicationService.Source;
    uses PublicationService.Target;
    uses RequestResponseService.Source;
    uses RequestResponseService.Target;
    uses SelfActivationService;
    uses SerializationService.Content;
    uses SerializationService.EventContext;
    uses SignatureService;
    uses TestingService.Source;
    uses TestingService.Target;

}
