# Framework

This module offers a generic framework for implementing cryptographic protocols. While the CHVote protocol is its
principal use case, the framework is independent of any CHVote particularities. Achieving this independence has been an
important design goal.

## 1. Introduction

In a cryptographic protocol, different parties interact with each other over a certain period of time to achieve a
common goal. During the protocol execution, they perform certain tasks and communicate with each other. Since the
required properties of the communication channels are not always the same, parties must be flexible to send and receive
messages that are either signed or unsigned respectively encrypted or not encrypted. Because protocol executions may
overlap in time, it must be possible for the parties to simultaneously participate in such *events*. For maximal
flexibility, support for different protocol versions and security levels must be provided individually for each event.

This module offers a common abstraction for the implementation of such a setting. In particular, the framework provides
a solution for the following questions:

- How can a party participate in multiple events simultaneously?
- How can support for different protocol versions be provided?
- How can the required flexibility with respect to the properties of the communication channels be implemented?

More general design questions result from the goal of making the parties independent of concrete infrastructure
services. This independence is the key for achieving a strict separation between cryptographically relevant code and
cryptographically irrelevant code. This separation defines the boundaries of the current CHVote project.

## 2. General Concepts

A party is essentially a state machine. Each state represents a particular position in the protocol flow of an
event, and state transitions are triggered by receiving messages from other parties. Upon receiving a message,
a party executes different tasks such as cryptographic computations, the sending of new messages to other parties, or
setting the current state to a new state. The data required for these tasks is stored in the party's *event context*,
which is available to all states. This basic principle is illustrated in the following diagram:

![state-event](img/state-event.svg)

To run multiple events simultaneously, the parties maintain a context for each event using their persistence service.
Each event context contains some public and secret data, which is accumulated by the party during the protocol run.
In this way, the event data objects are strictly separated over the different events. Other elements stored in
the party's event context are a unique event id, the protocol id, and the desired security level. This implies that
different protocols can be executed simultaneously.

## 3. Design

In the OOP design that follows from the above general concepts, the classes <code>Party</code>, <code>State</code>,
<code>EventContext</code>, and the interfaces <code>Service</code> and <code>EventData</code>, are the most important
ones. They are shown in the following simplified UML class diagram.

![class-diagram](img/class-diagram.svg)

### Communication Services

The following table gives an overview of the framework's communication services and their properties:

|                        | Communication Type | One-Way | Symmetric | Broadcast |  Signed  | Encrypted | Subscription |
|:-----------------------|:-------------------|:-------:|:---------:|:---------:|:--------:|:---------:|:------------:|
| EventService           | *Command* / Status |   No    |    No     |    Yes    |    No    |    No     |  Yes / Yes   |
| MailingService         | Mail               |   Yes   |    No     |    No     |    No    |    No     |   No / Yes   |
| MessagingService       | Message            |   No    |    Yes    |    No     |   Yes    | Optional  |     Yes      |
| PublicationService     | Publication        |   Yes   |    No     |    Yes    |   Yes    |    No     |   No / Yes   |
| RequestResponseService | Request / Response |   No    |    No     |    No     | No / Yes |    No     |  Yes / Yes   |
| TestingService         | Publication        |   Yes   |    No     |    Yes    |    No    |    No     |   No / Yes   |
| UserInterface          | Input / Output     |   No    |    No     |    No     |    No    |    No     |   No / No    |

### Infrastructure Services

Some additional service interfaces for making the framework code independent of infrastructure services and specific
cryptographic schemes are the following:

- Service interfaces: `PersistenceService`, `SerializationService`, `SelfActivationService`, `System.Logger` (from Java)
- Cryptographic services: `EncryptionService`, `SignatureService`, `CertificateService`, `KeystoreService`

## 4. Technical Aspects

### Event Coordination

All framework parties are automatically subscribed to an `EventService`, which means that they listen to the following
commands from the coordinator using this service:

- `EventService.Source.initialize(coordinator, eventId, protocolId, securityLevel)`
- `EventService.Source.terminate(eventId)`
- `EventService.Source.start(eventId, phaseID)`
- `EventService.Source.stop(eventId, phaseId)`

The first two items in the above list can be used to inform the subscribed parties about the initialization or
termination of events. Corresponding methods will then be invoked: 

- `Party.onInitialize(eventId, protocolId, securityLevel)`
- `Party.onTerminate(eventId)`

Parties reaching particular states (beginning of a phase, end of a phase, final event state, error state) may send
status notifications back to the coordinator:

- `EventService.Target.notifyStatus(status)`


### Communication Interfaces

For communicating with each other during the execution of a protocol, the parties may use the following methods of the
available communication interfaces:

- `MailingService.Source.deliver(mail)`
- `MessagingService.Source.send(message)`
- `PublicationService.Source.publish(publication)`
- `RequestResponseService.Source.request(request)`
- `RequestResponseService.Target.respond(response)`
- `TestingService.Source.provide(testData)`
- `UserInterface.Source.enter(input)`
- `UserInterface.Target.display(output)`

### State Transitions

For a given party participating in an event, state transitions are mainly activated by the available communication
services to which the party is subscribed. The following methods of the `Party` class handle these activations:

- `Party.onStart(eventId, phaseId)`
- `Party.onStop(eventId, phaseId)`
- `Party.onMail(mail)`
- `Party.onMessage(message)`
- `Party.onRequest(request)`
- `Party.onResponse(response)`
- `Party.onInput(input)`
- `Party.onOutput(output)`
- `Party.onSelfActivation(eventId, activationHandler)`

Here is a summary of the tasks that are triggered by these methods:

1. Lock the event and load the event context using the persistence service for the given event id.
2. Get the current state id from the event context and load the state object.
3. Pass the received object (if available) to one of the following handlers of the loaded state:
    - `handleStart()`
    - `handleStop()`
    - `handleMail(mail)`
    - `handleMessage(message)`
    - `handleRequest(request)`
    - `handleResponse(response)`
    - `handleInput(input)`
    - `handleOutput(output)`
    - `handleSelfActivation()`
4. Save the updated event context using the persistence service and unlock the event.

In the cases of receiving a content from either `Mail`, `Message`, `Request`, `Response`, `Input`, or `Output`, the
above handlers perform the following steps:

1. If the content is encrypted, decrypt it using the party's encryption service.
2. Deserialize the content using the party's serialization service.
3. If the content is signed, verify the signature using the party's signature service.
4. Use the retrieved content and event context to perform some computational tasks.
5. Update the public and secret event data.
6. If necessary, communicate some of the data to other parties.
7. If necessary, update the current state in the event context.

## 5. Using the Framework

This generic framework allows framework users to provide their own implementations of parties and services, all
following the protocols and rules defined by the framework. The resulting system is then configured such that
OpenCHVote parties use the effective services provided. And the provided party wrappers wrap the OpenCHVote
parties as delegates.

Sometimes, parties must be configured with proxies for the services they use.

### Implementing Services

In its simplest case, a service implementation provides a class implementing the interface(s) of an OpenCHVote service.
For example, the implementing class of the messaging service being used by the simulation looks like:

```java
public class SimpleMessagingService extends AbstractSimpleService implements MessagingService.Source, MessagingService.Target {
    // ...
}
```

Class `AbstractSimpleService` extracts properties and behaviors common to all such services. Note also that in
this example the implementing class implements both inner interfaces of the messaging service interface.

### Proxy Service

Sometimes it is necessary to provide a proxy for an effective service being used by an OpenCHVote party.
A reason for such a design is, for example, that the proxy must obtain a final configuration
prior to its use by a party. For example:

```java
public class MessagingServiceSourceProxy extends AbstractServiceProxy implements MessagingService.Source {

    @Override
    public void send(Message message) {
        // ...
    }

}
```

Here, class `MessagingServiceSourceProxy` implements the respective sub-interfaces, here method
`send(Message)` of interface `MessagingService.Source`. Parts of the code being configured are subsumed in
class `AbstractServiceProxy`. Upon construction, an OpenCHVote party obtains the proxy
via Java's service loading mechanism and uses
its `send(Message)` method whenever it needs to send a message. The proxy itself redirects
the message to the effective messaging service, perhaps encapsulating the provided
`Message` object into an appropriate container necessary by the underlying communication
mechanism.

The JPMS module providing the service proxy must use the `provides` directive
in its `module-info.java` file:

```java
module org.yourimplementation.common {
    // ...
    requires ch.openchvote.base.framework;
    // ...
    provides MessagingService.Source with MessagingServiceSourceProxy;
    // ...
    exports org.yourimplementation.proxy;
}
```

### Implementing Parties

Framework users will use the OpenCHVote parties to avoid "reimplementing" the CHVote protocol.
We expect that framework users will wrap OpenCHVote parties with their own implementations,
thus building a facade for OpenCHVote parties. For example:

```java
public class EffectiveElectionAuthority extends CommonPartyFeatures {

    // 
    private ElectionAuthority electionAuthority;

    //
    protected void createOCHVdelegate() {
        // Create the OpenCHVote party instance
        this.electionAuthority = new ElectionAuthority(id, loggerLevel);
        // Get the proxy for the target interface of the messaging service from the OpenCHVote party
        var messagingServiceTarget = (MessagingServiceTargetProxy) this.electionAuthority.getService(MessagingService.Target.class);
        // Post-configuration of the proxy
        messagingServiceTarget.configure(some_args);
        // ...
    }

}
```

Comments:

- Class `EffectiveElectionAuthority` inherits common features valid for all effective
  party implementations.
- It maintains a reference for the OpenCHVote delegate, here in form of instance variable `electionAuthority`.
- The OpenCHVote delegate is instantiated within a delayed post-construction call,
  as shown in method `createOCHVdelegate`.
- In Method `createOCHVdelegate`, more or less the following steps occur:

    1. The OpenCHVote party `ElectionAuthority` is instantiated.
    2. The proxy of the messaging service is obtained from the delegate.
    3. The proxy `messagingServiceTarget` gets its final configuration.

## 6. Installation of Maven Artefact

This module can be built as follows from its root:

```console
mvn clean install
-- a lot of output
```

## 7. Maven Dependencies

This module has no dependencies to other Maven modules.

## 8. Java Module Dependencies

File `module-info.java` of this module does not possess any `requires` directives. Hence, this module is independent
of any other Java module. However, it uses services provided by other modules via their `provides` directives. To obtain
these services, Java's utility `java.util.ServiceLoader` in interface `services.ch.openchvote.base.framework.Service`
is used.

```java
module ch.openchvote.base.framework {

    exports ch.openchvote.base.framework;
    exports ch.openchvote.base.framework.annotations.content;
    exports ch.openchvote.base.framework.annotations.party;
    exports ch.openchvote.base.framework.annotations.phase;
    exports ch.openchvote.base.framework.annotations.protocol;
    exports ch.openchvote.base.framework.annotations.service;
    exports ch.openchvote.base.framework.annotations.state;
    exports ch.openchvote.base.framework.communication;
    exports ch.openchvote.base.framework.exceptions;
    exports ch.openchvote.base.framework.interfaces;
    exports ch.openchvote.base.framework.party;
    exports ch.openchvote.base.framework.protocol;
    exports ch.openchvote.base.framework.security;
    exports ch.openchvote.base.framework.services;
    exports ch.openchvote.base.framework.synchronizer;

    uses CertificateService;
    uses EncryptionService;
    uses EventService.Source;
    uses EventService.Target;
    uses KeystoreService;
    uses MessagingService.Source;
    uses MessagingService.Target;
    uses MailingService.Source;
    uses MailingService.Target;
    uses PersistenceService;
    uses PublicationService.Source;
    uses PublicationService.Target;
    uses RequestResponseService.Source;
    uses RequestResponseService.Target;
    uses SelfActivationService;
    uses SerializationService.Certificate;
    uses SerializationService.Content;
    uses SerializationService.EventContext;
    uses SignatureService;
    uses TestingService.Source;
    uses TestingService.Target;
}
```
