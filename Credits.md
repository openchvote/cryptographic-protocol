# Credits

## Developers

In alphabetical order:

| Name               | E-Mail                                                | Affiliation                         |
|:-------------------|:------------------------------------------------------|:------------------------------------|
| Eric Dubuis        | [eric.dubuis@bfh.ch](MAILTO:eric.dubuis@bfh.ch)       | Bern University of Applied Sciences |
| Rolf Haenni (lead) | [rolf.haenni@bfh.ch](MAILTO:rolf.haenni@bfh.ch)       | Bern University of Applied Sciences |
| Reto Koenig        | [reto.koenig@bfh.ch](MAILTO:reto.koenig@bfh.ch)       | Bern University of Applied Sciences |
| Philipp Locher     | [philipp.locher@bfh.ch](MAILTO:philipp.locher@bfh.ch) | Bern University of Applied Sciences |

## Former and External Contributors

In alphabetical order:

| Name               | Affiliation                         | Topic                       |
|:-------------------|:------------------------------------|:----------------------------|
| Timo Lennart Bürk  | Bern University of Applied Sciences | Unit tests                  |
| Stephan Fischli    | Bern University of Applied Sciences | Framework blueprint         |
| Florian Moser      | INRIA Nancy                         | CI pipeline                 |
| Alain Peytrignet   | Bern University of Applied Sciences | JavaScript, EBNF serializer |
| Elias Schmidhalter | Bern University of Applied Sciences | JavaScript, EBNF serializer |

## Special Thanks

Special thanks goes to Thomas Hofer (Objectif Sécurité, Switzerland), who provided valuable recommendations about the
software architecture at an early stage of the project.

## Support

This research has been supported by the Swiss Federal Chancellery.

