/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.services;

import ch.openchvote.base.framework.exceptions.SerializerException;
import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.base.framework.services.SerializationService;
import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.serializer.Reflection;
import ch.openchvote.base.utilities.serializer.Serializer;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Quintuple;

import static ch.openchvote.base.framework.exceptions.SerializerException.Type.DESERIALIZATION;
import static ch.openchvote.base.framework.exceptions.SerializerException.Type.SERIALIZATION;

/**
 * Default implementation of the {@link SerializationService.EventContext} interface.
 */
public final class EventContextSerializationService extends CoreService implements SerializationService.EventContext {

    // expected name of a static TypeReference field in the content class
    static private final String TYPE_REFERENCE_FIELD_NAME = "TYPE_REFERENCE";

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public EventContextSerializationService() {
    }

    @Override
    public String serialize(ch.openchvote.base.framework.party.EventContext<?, ?> eventContext) throws SerializerException {
        try {
            var eventId = eventContext.getEventId();
            var protocolId = eventContext.getProtocolId();
            var stateId = eventContext.getCurrentStateId();
            var publicData = eventContext.getPublicData();
            var secretData = eventContext.getSecretData();
            var quintuple =  new Quintuple<>(eventId, protocolId, stateId, publicData, secretData);
            return Serializer.serialize(quintuple);
        } catch (UtilityException exception) {
            throw new SerializerException(SERIALIZATION, exception, eventContext);
        }
    }

    @Override
    public <S extends ch.openchvote.base.framework.party.EventContext<? extends EventData, ? extends EventData>> S deserialize(String eventContextString, Class<S> eventContextClass) throws SerializerException {
        try {
            var declaredField = eventContextClass.getDeclaredField(TYPE_REFERENCE_FIELD_NAME);
            var typeReference = (TypeReference<?>) declaredField.get(null);
            var quintuple = (Quintuple<?, ?, ?, ?, ?>) Serializer.deserialize(eventContextString, typeReference);
            return Reflection.getInstance(eventContextClass, quintuple.toArray());
        } catch (ReflectiveOperationException | UtilityException exception) {
            throw new SerializerException(DESERIALIZATION, exception, eventContextClass);
        }
    }

}
