/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.services;

import ch.openchvote.core.algorithms.general.algorithms.ByteArrayToString;
import ch.openchvote.core.algorithms.general.algorithms.GenCiphertext;
import ch.openchvote.core.algorithms.general.algorithms.GetPlaintext;
import ch.openchvote.core.algorithms.general.algorithms.StringToByteArray;
import ch.openchvote.core.algorithms.general.model.Ciphertext;
import ch.openchvote.base.framework.exceptions.SerializerException;
import ch.openchvote.base.framework.security.KeyGenerator;
import ch.openchvote.base.framework.services.EncryptionService;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.serializer.Serializer;
import ch.openchvote.base.utilities.serializer.TypeReference;

import java.math.BigInteger;

import static ch.openchvote.base.framework.exceptions.SerializerException.Type.DESERIALIZATION;
import static ch.openchvote.base.framework.exceptions.SerializerException.Type.SERIALIZATION;

/**
 * This class implements a hybrid encryption scheme for encrypting arbitrarily large messages. The group parameters are
 * selected from the {@link SecurityParameters} class. It uses the hybrid encryption algorithm {@link GenCiphertext}.
 */
public final class HybridEncryptionService extends CoreService implements EncryptionService {

    // singleton key generator
    static private final KeyGenerator KEY_GENERATOR = new SchnorrKeyGenerator();
    static private final String ALGORITHM_NAME = "HybridEncryption";

    // used for serializing the resulting ciphertext
    static private final TypeReference<Ciphertext> CIPHERTEXT_TYPE = new TypeReference<>() {
    };

    // used for serializing private and public encryption keys
    static private final TypeReference<BigInteger> KEY_TYPE = new TypeReference<>() {
    };

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public HybridEncryptionService() {
    }

    @Override
    public KeyGenerator getKeyGenerator() {
        return KEY_GENERATOR;
    }

    @Override
    public String getAlgorithmName() {
        return ALGORITHM_NAME;
    }

    @Override
    public String encrypt(String plaintext, String encryptionKey, String securityLevel) throws SerializerException {
        var securityParameters = new SecurityParameters(securityLevel);
        BigInteger pk;
        try {
            pk = Serializer.deserialize(encryptionKey, KEY_TYPE);
        } catch (UtilityException exception) {
            throw new SerializerException(DESERIALIZATION, exception, encryptionKey);
        }
        var M = StringToByteArray.run(plaintext);
        var e = GenCiphertext.run(pk, M, securityParameters);
        String ciphertext;
        try {
            ciphertext = Serializer.serialize(e);
        } catch (UtilityException exception) {
            throw new SerializerException(SERIALIZATION, exception, e);
        }
        return ciphertext;
    }

    @Override
    public String decrypt(String ciphertext, String decryptionKey, String securityLevel) throws SerializerException {
        var securityParameters = new SecurityParameters(securityLevel);
        BigInteger sk;
        try {
            sk = Serializer.deserialize(decryptionKey, KEY_TYPE);
        } catch (UtilityException exception) {
            throw new SerializerException(DESERIALIZATION, exception, decryptionKey);
        }
        Ciphertext e;
        try {
            e = Serializer.deserialize(ciphertext, CIPHERTEXT_TYPE);
        } catch (UtilityException exception) {
            throw new SerializerException(DESERIALIZATION, exception, ciphertext);
        }
        var M = GetPlaintext.run(sk, e, securityParameters);
        return ByteArrayToString.run(M);
    }

}
