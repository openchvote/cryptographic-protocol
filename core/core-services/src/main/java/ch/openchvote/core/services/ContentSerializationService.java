/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.services;

import ch.openchvote.base.framework.exceptions.SerializerException;
import ch.openchvote.base.framework.services.SerializationService;
import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.serializer.Serializer;

import static ch.openchvote.base.framework.exceptions.SerializerException.Type.DESERIALIZATION;

/**
 * Default implementation of the {@link SerializationService.Content} interface.
 */
public final class ContentSerializationService extends CoreService implements SerializationService.Content {

    // expected name of a static TypeReference field in the content class
    static private final String TYPE_REFERENCE_FIELD_NAME = "TYPE_REFERENCE";

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public ContentSerializationService() {
    }

    @Override
    public String serialize(ch.openchvote.base.framework.communication.Content content) throws SerializerException {
        try {
            return Serializer.serialize(content);
        } catch (UtilityException exception) {
            throw new SerializerException(DESERIALIZATION, exception, content);
        }
    }

    @Override
    public <S extends ch.openchvote.base.framework.communication.Content> S deserialize(String contentString, Class<S> contentClass) throws SerializerException {
        try {
            return Serializer.deserialize(contentString, contentClass, TYPE_REFERENCE_FIELD_NAME);
        } catch (UtilityException exception) {
            throw new SerializerException(DESERIALIZATION, exception, contentString, contentClass);
        }

    }

}
