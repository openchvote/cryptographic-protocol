/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.services;

import ch.openchvote.core.algorithms.general.algorithms.GenSchnorrKeyPair;
import ch.openchvote.base.framework.exceptions.SerializerException;
import ch.openchvote.base.framework.security.KeyGenerator;
import ch.openchvote.base.framework.security.KeyPair;
import ch.openchvote.core.protocol.parameters.SecurityLevel;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.serializer.Serializer;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static ch.openchvote.base.framework.exceptions.SerializerException.Type.SERIALIZATION;

/**
 * This class implements a {@link KeyGenerator} for generating key pairs of a given Schnorr group. The group parameters
 * are selected from the {@link SecurityParameters} class. It uses the key generation algorithm
 * {@link GenSchnorrKeyPair}.
 */
public final class SchnorrKeyGenerator extends CoreService implements KeyGenerator {

    /**
     * Constructs a new instance of this class.
     */
    public SchnorrKeyGenerator() {
    }

    @Override
    public Set<String> getSecurityLevels() {
        return Arrays.stream(SecurityLevel.values()).map(Enum::name).collect(Collectors.toSet());
    }

    @Override
    public KeyPair generateKeyPair(String securityLevel) {
        var securityParameters = new SecurityParameters(securityLevel);
        var pair = GenSchnorrKeyPair.run(securityParameters);
        try {
            var privateKey = Serializer.serialize(pair.getFirst());
            var publicKey = Serializer.serialize(pair.getSecond());
            return new KeyPair() {

                @Override
                public String getPrivateKey() {
                    return privateKey;
                }

                @Override
                public String getPublicKey() {
                    return publicKey;
                }

                @Override
                public String getSecurityLevel() {
                    return securityLevel;
                }
            };
        } catch (UtilityException exception) {
            throw new SerializerException(SERIALIZATION, exception, pair);
        }
    }

}
