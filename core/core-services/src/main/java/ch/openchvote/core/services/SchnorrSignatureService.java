/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.services;

import ch.openchvote.core.algorithms.general.algorithms.CheckSignature;
import ch.openchvote.core.algorithms.general.algorithms.GenSignature;
import ch.openchvote.core.algorithms.general.model.Signature;
import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.base.framework.exceptions.SerializerException;
import ch.openchvote.base.framework.security.KeyGenerator;
import ch.openchvote.base.framework.services.SignatureService;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.base.utilities.UtilityException;
import ch.openchvote.base.utilities.serializer.Serializer;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

import static ch.openchvote.base.framework.exceptions.SerializerException.Type.DESERIALIZATION;
import static ch.openchvote.base.framework.exceptions.SerializerException.Type.SERIALIZATION;

/**
 * This class implements the Schnorr signature scheme. The group parameters are selected from the
 * {@link SecurityParameters} class. It uses the signature algorithm {@link GenSignature}.
 */
public final class SchnorrSignatureService extends CoreService implements SignatureService {

    // singleton key generator
    static private final KeyGenerator KEY_GENERATOR = new SchnorrKeyGenerator();
    static private final String ALGORITHM_NAME = "SchnorrSignature";

    // used for serializing the resulting signatures
    static private final TypeReference<Signature> SIGNATURE_TYPE = new TypeReference<>() {
    };

    // used for serializing private and public verification keys
    static private final TypeReference<BigInteger> KEY_TYPE = new TypeReference<>() {
    };

    /**
     * Explicit no-argument constructor used by {@link java.util.ServiceLoader}.
     */
    public SchnorrSignatureService() {
    }

    @Override
    public KeyGenerator getKeyGenerator() {
        return KEY_GENERATOR;
    }

    @Override
    public String getAlgorithmName() {
        return ALGORITHM_NAME;
    }

    @Override
    public String sign(Content content, Object auxiliaryData, String signatureKey, String securityLevel) {
        BigInteger sk;
        try {
            sk = Serializer.deserialize(signatureKey, KEY_TYPE);
        } catch (UtilityException exception) {
            throw new SerializerException(DESERIALIZATION, exception, signatureKey);
        }
        var contentName = content.getPrintName();
        var aux = (auxiliaryData == null) ? contentName : new Pair<>(contentName, auxiliaryData);
        var securityParameters = new SecurityParameters(securityLevel);
        var sigma = GenSignature.run(sk, content, aux, securityParameters);
        String signature;
        try {
            signature = Serializer.serialize(sigma);
        } catch (UtilityException exception) {
            throw new SerializerException(SERIALIZATION, exception, sigma);
        }
        return signature;
    }

    @Override
    public boolean verify(String signature, Content content, Object auxiliaryData, String verificationKey, String securityLevel) {
        BigInteger pk;
        try {
            pk = Serializer.deserialize(verificationKey, KEY_TYPE);
        } catch (UtilityException exception) {
            throw new SerializerException(DESERIALIZATION, exception, verificationKey);
        }
        Signature sigma;
        try {
            sigma = Serializer.deserialize(signature, SIGNATURE_TYPE);
        } catch (UtilityException exception) {
            throw new SerializerException(DESERIALIZATION, exception, signature);
        }
        var contentName = content.getPrintName();
        var aux = (auxiliaryData == null) ? contentName : new Pair<>(contentName, auxiliaryData);
        var securityParameters = new SecurityParameters(securityLevel);
        return CheckSignature.run(sigma, pk, content, aux, securityParameters);
    }

}
