/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms


import ch.openchvote.base.utilities.sequence.ByteArray
import ch.openchvote.core.algorithms.Algorithm
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class SetWatermarkSpec extends Specification {

    //Tests adopted from https://gitlab.com/chvote2/protocol-core/chvote-protocol/blob/master/protocol-algorithms/src/test/groovy/ch/ge/ve/protocol/core/support/ByteArrayUtilsTest.groovy
    @Shared
    def default_B
    @Shared
    def default_m
    @Shared
    def default_n

    def setupSpec() {
        default_B = ByteArray.of(([0x1A] as byte[]))
        default_m = 1
        default_n = 2
    }

    @Unroll
    def "run(ByteArray B, int m, int n) test preconditions case: #caseDesc"() {
        when:
        SetWatermark.run(B, m, n)

        then:
        thrown(Algorithm.Exception)

        where:
        caseDesc              | B         | m         | n
        "B is null"           | null      | default_m | default_n
        "m is negative"       | default_B | -1        | default_n
        "m equal to n"        | default_B | 3         | 3
        "m bigger then n"     | default_B | 4         | 3
        "n does not fit in B" | default_B | default_m | 257
    }

    @Unroll
    def "run(ByteArray B, int m, int n) should watermark the target array according to spec"() {
        given:
        def B = ByteArray.of(bytes as byte[])
        def expected = ByteArray.of(eBytes as byte[])

        expect:
        SetWatermark.run(B, m, n) == expected

        where:
        bytes                    | m | n  || eBytes
        [0x00]                   | 0 | 3  || [0x00]
        [0x00]                   | 1 | 3  || [0x01]
        [0x00]                   | 2 | 3  || [0x10]
        [0xFF, 0xFF, 0xFF, 0xFF] | 0 | 3  || [0xFE, 0xFF, 0xFE, 0xFF]
        [0xC1, 0xD2]             | 0 | 3  || [0xC0, 0xD2]
        [0xC1, 0xD2]             | 1 | 3  || [0xC1, 0xD2]
        [0xC1, 0xD2]             | 0 | 15 || [0xC0, 0xC2]
        [0xCC, 0xDD]             | 0 | 15 || [0xCC, 0xCC]
        [0xE3, 0xF4]             | 1 | 3  || [0xE3, 0xF4]
    }

}
