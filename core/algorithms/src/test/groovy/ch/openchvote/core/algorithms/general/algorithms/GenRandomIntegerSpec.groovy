/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms


import spock.lang.Specification

class GenRandomIntegerSpec extends Specification {

    def "RandomDistribution over 1 byte"() {
        when:
        def randomIntegers = [].toSet()
        for (int i = 0; i < 100; i++) {
            def r = GenRandomInteger.run(250) // q = 250 uses 1 byte
            randomIntegers.add(r)
        }

        then: // with 100 random numbers we should at least get 200 different values
        randomIntegers.size() >= 50
    }

    def "RandomDistribution over 2 bytes"() {
        when:
        def randomIntegers = [].toSet()
        for (int i = 0; i < 500; i++) {
            def r = GenRandomInteger.run(500) // q = 500 uses 2 bytes
            randomIntegers.add(r)
        }

        then: // with 500 random numbers we should at least get 200 different values
        randomIntegers.size() >= 200
    }

}