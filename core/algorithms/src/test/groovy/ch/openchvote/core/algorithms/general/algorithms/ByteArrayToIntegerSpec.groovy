/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms


import ch.openchvote.base.utilities.sequence.ByteArray
import ch.openchvote.core.algorithms.Algorithm
import spock.lang.Specification
import spock.lang.Unroll

class ByteArrayToIntegerSpec extends Specification {

    @Unroll
    def "ByteArrayToInteger.run(ByteArray B) test preconditions case: #caseDesc"() {
        when:
        ByteArrayToInteger.run(B)

        then:
        thrown(Algorithm.Exception)

        where:
        caseDesc    | B
        "B is null" | null
    }

    @SuppressWarnings('GroovyAssignabilityCheck')
    def "ByteArrayToInteger.run(ByteArray B) test by using ByteArray.of(r.toByteArray())"(BigInteger r) {
        given:
        ByteArray x = ByteArray.of(r.toByteArray())

        expect:
        ByteArrayToInteger.run(x) == r

        where:
        r << (0..1000)
    }

}
