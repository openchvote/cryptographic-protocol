/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms


import ch.openchvote.base.utilities.sequence.ByteArray
import ch.openchvote.core.algorithms.Algorithm
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class IntegerToByteArraySpec extends Specification {

    @Shared
    def default_x
    @Shared
    def default_n

    def setupSpec() {
        default_x = BigInteger.valueOf(1)
        default_n = 1
    }

    @Unroll
    def "IntegerToByteArray.run(BigInteger) test preconditions case: #caseDesc"() {
        when:
        IntegerToByteArray.run(x as BigInteger)

        then:
        thrown(Algorithm.Exception)

        where:
        caseDesc        | x
        "x is null"     | null
        "x is negativ " | BigInteger.valueOf(-1)
    }

    def "IntegerToByteArray(BigInteger)"() {
        given:
        def expected = ByteArray.of(eBytes as byte[])

        expect:
        IntegerToByteArray.run(x) == expected

        where:
        x                              || eBytes
        BigInteger.valueOf(0)          || []
        BigInteger.valueOf(1)          || [0x1]
        BigInteger.valueOf(255)        || [0xFF]
        BigInteger.valueOf(256)        || [0x01, 0x00]
        BigInteger.valueOf(65_535)     || [0xFF, 0xFF]
        BigInteger.valueOf(65_536)     || [0x01, 0x00, 0x00]
        BigInteger.valueOf(16_777_215) || [0xFF, 0xFF, 0xFF]
        BigInteger.valueOf(16_777_216) || [0x01, 0x00, 0x00, 0x00]
    }

    @Unroll
    def "IntegerToByteArray.run(BigInteger, n) test preconditions case: #caseDesc"() {
        when:
        IntegerToByteArray.run(x as BigInteger, n)

        then:
        thrown(Algorithm.Exception)

        where:
        caseDesc                    | x                       | n
        "x is null"                 | null                    | default_n
        "x is negativ "             | BigInteger.valueOf(-1)  | default_n
        "n is negativ"              | default_x               | -1
        "x does not fit in n bytes" | BigInteger.valueOf(256) | 1
    }

    def "IntegerToByteArray(BigInteger, int)"() {
        given:
        def expected = ByteArray.of(eBytes as byte[])

        expect:
        IntegerToByteArray.run(x, n) == expected

        where:
        x                       | n | eBytes
        BigInteger.valueOf(0)   | 0 | []
        BigInteger.valueOf(0)   | 1 | [0x00]
        BigInteger.valueOf(0)   | 2 | [0x00, 0x00]
        BigInteger.valueOf(0)   | 3 | [0x00, 0x00, 0x00]
        BigInteger.valueOf(255) | 1 | [0xFF]
        BigInteger.valueOf(255) | 2 | [0x00, 0xFF]
        BigInteger.valueOf(255) | 3 | [0x00, 0x00, 0xFF]
        BigInteger.valueOf(256) | 2 | [0x01, 0x00]
        BigInteger.valueOf(256) | 3 | [0x00, 0x01, 0x00]
        BigInteger.valueOf(256) | 4 | [0x00, 0x00, 0x01, 0x00]
    }

}
