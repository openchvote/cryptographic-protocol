/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms


import ch.openchvote.base.utilities.sequence.ByteArray
import ch.openchvote.base.utilities.set.Alphabet
import ch.openchvote.core.algorithms.Algorithm
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class ByteArrayToStringSpec extends Specification {

    @Shared
    def default_A
    @Shared
    def default_B

    def setupSpec() {
        default_A = Alphabet.BASE_16
        default_B = ByteArray.of(([0x00] as byte[]))
    }

    @Unroll
    def "ByteArrayToString.run(ByteArray B, Alphabet A) test preconditions case: #caseDesc"() {
        when:
        ByteArrayToString.run(B, A)

        then:
        thrown(Algorithm.Exception)

        where:
        caseDesc    | B         | A
        "B is null" | null      | default_A
        "A is null" | default_B | null
    }

    @Unroll
    def "ByteArrayToString.run(ByteArray B, Alphabet A)"() {
        given:
        def B = ByteArray.of((bytes as byte[]))

        expect:
        ByteArrayToString.run(B, A) == expected

        where:
        bytes              | A                   || expected
        [0x00]             | Alphabet.BASE2 || "00000000"
        [0x01]             | Alphabet.BASE2 || "00000001"
        [0x02]             | Alphabet.BASE2 || "00000010"
        [0x04]             | Alphabet.BASE2 || "00000100"
        [0x08]             | Alphabet.BASE2 || "00001000"
        [0x0F]             | Alphabet.BASE2 || "00001111"
        [0x1A]             | Alphabet.BASE2 || "00011010"
        [0x02, 0xDB]       | Alphabet.BASE_16 || "02DB"
        [0x19]             | Alphabet.BASE_16 || "19"
        [0x02, 0x8A]       | Alphabet.BASE_16 || "028A"
        [0x02, 0xA3]       | Alphabet.BASE_16 || "02A3"
        [0x42, 0x04]       | Alphabet.BASE_16 || "4204"
        [0x44, 0xA7]       | Alphabet.BASE_16 || "44A7"
        [0x06, 0xB4, 0x68] | Alphabet.BASE_16 || "06B468"
        [0x06, 0xF9, 0x0F] | Alphabet.BASE_16 || "06F90F"
        [0x02, 0xDB]       | Alphabet.LATIN_26 || "ABCD"
        [0x19]             | Alphabet.LATIN_26 || "AZ"
        [0x02, 0x8A]       | Alphabet.LATIN_26 || "AAZA"
        [0x02, 0xA3]       | Alphabet.LATIN_26 || "AAZZ"
        [0x42, 0x04]       | Alphabet.LATIN_26 || "AZAA"
        [0x44, 0xA7]       | Alphabet.LATIN_26 || "AZZZ"
        [0x06, 0xB4, 0x68] | Alphabet.LATIN_26 || "AAZAAA"
        [0x06, 0xF9, 0x0F] | Alphabet.LATIN_26 || "AAZZZZ"
    }

    @Unroll
    def "ByteArrayToString.run(ByteArray B) test preconditions case: #caseDesc"() {
        when:
        ByteArrayToString.run(B)

        then:
        thrown(Algorithm.Exception)

        where:
        caseDesc        | B
        "B is null"     | null
        "B not in UTF8" | ByteArray.of(([0xFF] as byte[]))
    }

    def "ByteArrayToString.run(ByteArray B)"() {
        given:
        def B = ByteArray.of((bytes as byte[]))

        expect:
        ByteArrayToString.run(B) == expected

        where:
        bytes                                                              || expected
        [0x61, 0x62, 0x63]                                                 || "abc"
        [0x68, 0x65, 0x6C, 0x6C, 0x6F]                                     || "hello"
        [0x77, 0x6F, 0x72, 0x6C, 0x64, 0x21]                               || "world!"
        [0x6C, 0x6F, 0x72, 0x65, 0x6D, 0x20, 0x69, 0x70, 0x73, 0x75, 0x6D] || "lorem ipsum"
    }

}
