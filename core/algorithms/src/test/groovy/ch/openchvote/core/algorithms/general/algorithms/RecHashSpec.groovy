/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms


import ch.openchvote.base.utilities.crypto.HashAlgorithm
import ch.openchvote.base.utilities.sequence.ByteArray
import ch.openchvote.base.utilities.sequence.Matrix
import ch.openchvote.base.utilities.sequence.Vector
import ch.openchvote.base.utilities.set.Alphabet
import ch.openchvote.base.utilities.tuples.*
import spock.lang.Specification
import spock.lang.Unroll


class RecHashSpec extends Specification {

    @Unroll
    def "run(int L, Object... values)"() {
        given:
        def expected = IntegerToByteArray.run(new BigInteger(expectedHexString, 16))

        expect:
        RecHash.run(value, HashAlgorithm.SHA3) == expected

        where:
        value                                               | expectedHexString
        null                                                | "5D53469F20FEF4F8EAB52B88044EDE69C77A6A68A60728609FC4A65FF531E7D0"
        "test"                                              | "D3C65606206EA80CBD64DFF8B9D1C514D6D132FD7E9A68D0487019B71BB06035"
        0                                                   | "0A1E2736777F80A62BEB2DF72B649878481C0CA10194B832B5136BEFBAE54017"
        42                                                  | "B84829B96BFFABC60B419FA64C68448402F28042EA805281DE3B52F1E5DC8B1B"
        BigInteger.valueOf(42739L)                          | "AA4349124C574D3C3E6070D8A17EEFE6C1A2C05564C68A268CC6B07AA483CEE1"
        ["test", BigInteger.valueOf(42L)] as Object[]       | "E6DC7AB721E8F575ABEC1D95A3F485300589D3BA3BCD90B91EA2589567063DDB"
        [null, "test", BigInteger.valueOf(42L)] as Object[] | "3744BEB150A8D0578D0C3A07DED7506952D72D82B47A13479A9C42C18E52B82E"
        ByteArray.EMPTY                                     | "2767F15C8AF2F2C7225D5273FDD683EDC714110A987D1054697C348AED4E6CC7"
        ByteArray.of(10)                                    | "FD56BA320041F04456DC1006397BEADF46873898C729530AF40EEB4EC258CA25"
    }

    def "run(int L, Object... values) for Matrix"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.size() > 0 ? inputMatrix.get(0).size() : 0

        and: "build Matrix"
        def builder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.set(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def matrix = builder.build()

        when:
        def expected = IntegerToByteArray.run(StringToInteger.run(expectedHexString, Alphabet.BASE_16))

        then:
        RecHash.run(matrix, HashAlgorithm.SHA3) == expected

        where:
        inputMatrix    | L  | expectedHexString
        []             | 32 | "3B0C4D506212CD7E7B88BC93B5B1811AB5DE6796D2780E9DE7378C87FE9A80A6"
        [[1]]          | 32 | "E61DAB4785A40A93D89D10ACC57E04F57DDC63EDD957E793371B3E3136F3F38E"
        [[1, 2, 3],
         [3, 2, 1]]    | 32 | "9C5EBD8FFCA4988231972FDF8A428D25FA8D8F93BC75DA9FC1D2EB5BBD894C86"
        [[1, 2, 3],
         [3, 2, null]] | 32 | "619375D6D29557FB697E4343645AFF63AE84AC8E0D33F9446DA90EEB20AFAF5E"
    }

    @Unroll
    def "run(int L, Object... values) for Vector"() {
        given:
        def vector = Vector.of(input.toArray())
        def expected = IntegerToByteArray.run(StringToInteger.run(expectedHexString, Alphabet.BASE_16))

        expect:
        RecHash.run(vector, HashAlgorithm.SHA3) == expected

        where:
        input                    | L  | expectedHexString
        []                       | 32 | "989216075A288AF2C12F115557518D248F93C434965513F5F739DF8C9D6E1932"
        [1]                      | 32 | "0611A4C9CCA9BB48C5337F738E116EB800B87DEA48769C68C7C3466229AC8F8F"
        [1, 1, 1, 1]             | 32 | "40FB3F38F7A0D6865A2D5182611FC48B89875CC0C1F6CD239713E581BD4DD82E"
        [1, 2, 3, 4]             | 32 | "9FE2EAC04A8C42A0A78A82BD20D1760F3DCBF3863DCB6F32B96AE91F4F02A477"
        [null]                   | 32 | "6D3B799EE2E7A2F449FD21B085E4734C765FA9E10FCBF21969B91E9B6EAB83CC"
        [null, null, null, null] | 32 | "26F1E56D321277D687648F00DA58A1443DEC1BE7DBC40ED9F6CA55A4538371C7"
        ["test", null, 0]        | 32 | "B6A0ACD7570D96D8520F5787975635CB94E97BD40E96CE86F0B4765B1EE17FD0"
    }

    def "run(int L, Object... values) for Singleton"() {
        given:
        def pair = new Singleton(elems1.get(0))
        def expected = IntegerToByteArray.run(StringToInteger.run(expectedHexString, Alphabet.BASE_16))

        expect:
        RecHash.run(pair, HashAlgorithm.SHA3, ) == expected

        where:
        elems1 | L  | expectedHexString
        [1]    | 32 | "0611A4C9CCA9BB48C5337F738E116EB800B87DEA48769C68C7C3466229AC8F8F"
    }

    def "run(int L, Object... values) for Pair"() {
        given:
        def pair = new Pair(elems1.get(0), elems1.get(1))
        def expected = IntegerToByteArray.run(StringToInteger.run(expectedHexString, Alphabet.BASE_16))

        expect:
        RecHash.run(pair, HashAlgorithm.SHA3, ) == expected

        where:
        elems1 | L  | expectedHexString
        [1, 2] | 32 | "B137339C9479E8B538D07664D7B74EBECA5CE674524D7FAAF0F7C825DC2AF783"
    }

    def "run(int L, Object... values) for Triple"() {
        given:
        def triple = new Triple(elems1.get(0), elems1.get(1), elems1.get(2))
        def expected = IntegerToByteArray.run(StringToInteger.run(expectedHexString, Alphabet.BASE_16))

        expect:
        RecHash.run(triple, HashAlgorithm.SHA3, ) == expected

        where:
        elems1    | L  | expectedHexString
        [1, 2, 3] | 32 | "A26FDDD8D817CEF61867F9CCE5BE4F763B4F5B264225E67A15B1682C7C2A591C"
    }

    def "run(int L, Object... values) for Quadruple"() {
        given:
        def quadruple = new Quadruple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3))
        def expected = IntegerToByteArray.run(StringToInteger.run(expectedHexString, Alphabet.BASE_16))

        expect:
        RecHash.run(quadruple, HashAlgorithm.SHA3) == expected

        where:
        elems1       | L  | expectedHexString
        [1, 2, 3, 4] | 32 | "9FE2EAC04A8C42A0A78A82BD20D1760F3DCBF3863DCB6F32B96AE91F4F02A477"
    }

    def "run(int L, Object... values) for Quintuple"() {
        given:
        def quintuple = new Quintuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4))
        def expected = IntegerToByteArray.run(StringToInteger.run(expectedHexString, Alphabet.BASE_16))

        expect:
        RecHash.run(quintuple, HashAlgorithm.SHA3) == expected

        where:
        elems1          | L  | expectedHexString
        [1, 2, 3, 4, 5] | 32 | "49C36EEC6228B985066FCF6CC1AA7527F7202762D57AE7317D7F3377CC9B792C"
    }

    def "run(int L, Object... values) for Sextuple"() {
        given:
        def sextuple = new Sextuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5))
        def expected = IntegerToByteArray.run(StringToInteger.run(expectedHexString, Alphabet.BASE_16))

        expect:
        RecHash.run(sextuple, HashAlgorithm.SHA3) == expected

        where:
        elems1             | L  | expectedHexString
        [1, 2, 3, 4, 5, 6] | 32 | "38267722BD47F01265DCD9768AC74BDE06F8614FA505EF4D82A5514161286816"
    }

    def "run(int L, Object... values) for Septuple"() {
        given:
        def septuple = new Septuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6))
        def expected = IntegerToByteArray.run(StringToInteger.run(expectedHexString, Alphabet.BASE_16))

        expect:
        RecHash.run(septuple, HashAlgorithm.SHA3) == expected

        where:
        elems1                | L  | expectedHexString
        [1, 2, 3, 4, 5, 6, 7] | 32 | "3E064C96729DEA84E727CC872DE48442D0CE214893F3911DB1F30ACA2F57B0A6"
    }

    def "run(int L, Object... values) for Octuple"() {
        given:
        def octuple = new Octuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7))
        def expected = IntegerToByteArray.run(StringToInteger.run(expectedHexString, Alphabet.BASE_16))

        expect:
        RecHash.run(octuple, HashAlgorithm.SHA3) == expected

        where:
        elems1                   | L  | expectedHexString
        [1, 2, 3, 4, 5, 6, 7, 8] | 32 | "89CCABCF9BDE1EF4452E213974401A2D78E20D8F65ACE156EC90FFF467EAF9E8"
    }

    def "run(int L, Object... values) for Nonuple"() {
        given:
        def nonuple = new Nonuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8))
        def expected = IntegerToByteArray.run(StringToInteger.run(expectedHexString, Alphabet.BASE_16))

        expect:
        RecHash.run(nonuple, HashAlgorithm.SHA3) == expected

        where:
        elems1                      | L  | expectedHexString
        [1, 2, 3, 4, 5, 6, 7, 8, 9] | 32 | "8AC1ADC4BDD981D2497D8C62D1D74B6FD08F9CD0482B78E1765DD2EFBB7D11AF"
    }

}
