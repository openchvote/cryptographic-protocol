/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms


import ch.openchvote.base.utilities.sequence.ByteArray
import ch.openchvote.core.algorithms.Algorithm
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class SetBitSpec extends Specification {

    @Shared
    def default_B
    @Shared
    def default_i
    @Shared
    def default_b

    def setupSpec() {
        default_B = ByteArray.of(([0x1A] as byte[]))
        default_i = 1
        default_b = 1
    }

    @Unroll
    def "run(ByteArray B, int i, int b) test preconditions case: #caseDesc"() {
        when:
        SetBit.run(B, i, b)

        then:
        thrown(Algorithm.Exception)

        where:
        caseDesc                       | B         | i         | b
        "B is null"                    | null      | default_i | default_b
        "b is not in array"            | default_B | -1        | default_b
        "b is not in array"            | default_B | 17        | default_b
        "b is not an element of {0,1}" | default_B | default_i | 2
    }

    @Unroll
    def "run(ByteArray B, int i, int b) returns #expectedReadable with B = #bytesReadable,i =#i and b = #b "() {
        given:
        def B = ByteArray.of((bytes as byte[]))
        def expected = ByteArray.of(e as byte[])

        expect:
        SetBit.run(B, i, b) == expected

        where:
        bytes        | bytesReadable       | i  | b | e            | expectedReadable
        [0x1A]       | "00011010"          | 6  | 1 | [0x5A]       | "01011010"
        [0x1A]       | "00011010"          | 1  | 1 | [0x1A]       | "00011010"
        [0x1A]       | "00011010"          | 0  | 0 | [0x1A]       | "00011010"
        [0x1A]       | "00011010"          | 4  | 0 | [0x0A]       | "00001010"
        [0x01, 0x13] | "00000001 00010011" | 14 | 1 | [0x01, 0x53] | "00000001 01010011"
        [0x01, 0x13] | "00000001 00010011" | 9  | 1 | [0x01, 0x13] | "00000001 00010011"
        [0x01, 0x13] | "00000001 00010011" | 8  | 0 | [0x01, 0x12] | "00000001 00010010"
        [0x01, 0x13] | "00000001 00010011" | 12 | 0 | [0x01, 0x03] | "00000001 00000011"
    }

}
