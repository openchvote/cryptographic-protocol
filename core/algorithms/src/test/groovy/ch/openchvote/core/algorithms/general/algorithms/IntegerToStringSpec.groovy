/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms


import ch.openchvote.base.utilities.set.Alphabet
import ch.openchvote.core.algorithms.Algorithm
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class IntegerToStringSpec extends Specification {

    @Shared
    def default_A
    @Shared
    def default_x
    @Shared
    def default_n

    def setupSpec() {
        default_A = Alphabet.BASE_16
        default_x = BigInteger.valueOf(1)
        default_n = 1
    }

    @Unroll
    def "IntegerToString.run(x, n, A) test preconditions case: #caseDesc"() {
        when:
        IntegerToString.run(x, n, A)

        then:
        thrown(Algorithm.Exception)

        where:
        caseDesc                    | x                       | n         | A
        "x is null"                 | null                    | default_n | default_A
        "x is negativ "             | BigInteger.valueOf(-1)  | default_n | default_A
        "n is negativ"              | default_x               | -1        | default_A
        "x does not fit in n bytes" | BigInteger.valueOf(256) | 1         | default_A
        "A is null"                 | default_x               | default_n | null
    }

    def "IntegerToString.run(BigInteger x, int n, Alphabet A)"() {
        expect:
        IntegerToString.run(x, n, A) == expected

        where:
        x                          | n | A                 || expected
        BigInteger.valueOf(0)      | 4 | Alphabet.BASE2    || "0000"
        BigInteger.valueOf(0)      | 0 | Alphabet.BASE2    || ""
        BigInteger.valueOf(1)      | 4 | Alphabet.BASE2    || "0001"
        BigInteger.valueOf(1)      | 1 | Alphabet.BASE2    || "1"
        BigInteger.valueOf(2)      | 4 | Alphabet.BASE2    || "0010"
        BigInteger.valueOf(2)      | 2 | Alphabet.BASE2    || "10"
        BigInteger.valueOf(4)      | 4 | Alphabet.BASE2    || "0100"
        BigInteger.valueOf(4)      | 3 | Alphabet.BASE2    || "100"
        BigInteger.valueOf(8)      | 4 | Alphabet.BASE2    || "1000"
        BigInteger.valueOf(15)     | 4 | Alphabet.BASE2    || "1111"
        BigInteger.valueOf(731)    | 4 | Alphabet.LATIN_26 || "ABCD"
        BigInteger.valueOf(25)     | 1 | Alphabet.LATIN_26 || "Z"
        BigInteger.valueOf(650)    | 2 | Alphabet.LATIN_26 || "ZA"
        BigInteger.valueOf(675)    | 2 | Alphabet.LATIN_26 || "ZZ"
        BigInteger.valueOf(16900)  | 3 | Alphabet.LATIN_26 || "ZAA"
        BigInteger.valueOf(17575)  | 3 | Alphabet.LATIN_26 || "ZZZ"
        BigInteger.valueOf(439400) | 4 | Alphabet.LATIN_26 || "ZAAA"
        BigInteger.valueOf(456975) | 4 | Alphabet.LATIN_26 || "ZZZZ"
    }

}
