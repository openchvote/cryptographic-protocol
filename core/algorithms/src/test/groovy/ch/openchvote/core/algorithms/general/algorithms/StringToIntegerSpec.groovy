/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms


import ch.openchvote.base.utilities.set.Alphabet
import ch.openchvote.core.algorithms.Algorithm
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class StringToIntegerSpec extends Specification {

    @Shared
    def default_S
    @Shared
    def default_A

    def setupSpec() {
        default_S = "123"
        default_A = Alphabet.BASE_32
    }

    @Unroll
    def "StringToInteger.run(String S, Alphabet A) test preconditions case: #caseDesc"() {
        when:
        StringToInteger.run(S, A)

        then:
        thrown(Algorithm.Exception)

        where:
        caseDesc                | S         | A
        "S is null"             | null      | default_A
        "A is null"             | default_S | null
        "S is not element of A" | "123"     | Alphabet.BASE2
        "S is not element of A" | "9AB"     | Alphabet.BASE_8
        "S is not element of A" | "ABC"     | Alphabet.BASE_10
        "S is not element of A" | "GHI"     | Alphabet.BASE_16
        "S is not element of A" | "789"     | Alphabet.BASE_32
        "S is not element of A" | "!?"      | Alphabet.BASE_64
        "S is not element of A" | "123"     | Alphabet.LATIN_26
        "S is not element of A" | "123"     | Alphabet.LATIN_52
        "S is not element of A" | "123"     | Alphabet.LATIN_EXTENDED
        "S is not element of A" | "!?"      | Alphabet.ALPHANUMERIC_32
        "S is not element of A" | "!?"      | Alphabet.ALPHANUMERIC_36
        "S is not element of A" | "!?"      | Alphabet.ALPHANUMERIC_57
        "S is not element of A" | "!?"      | Alphabet.ALPHANUMERIC_62
    }

    @Unroll
    def "StringToInteger.run(String S, Alphabet A) converts #S to #expected with alphabet #A.getCharacters()"() {
        expect:
        StringToInteger.run(S, A) == expected

        where:
        S       | A                        | expected
        "11010" | Alphabet.BASE2           | BigInteger.valueOf(26)
        "32"    | Alphabet.BASE_8          | BigInteger.valueOf(26)
        "26"    | Alphabet.BASE_10         | BigInteger.valueOf(26)
        "1A"    | Alphabet.BASE_16         | BigInteger.valueOf(26)
        "2"     | Alphabet.BASE_32         | BigInteger.valueOf(26)
        "a"     | Alphabet.BASE_64         | BigInteger.valueOf(26)
        "BA"    | Alphabet.LATIN_26        | BigInteger.valueOf(26)
        "a"     | Alphabet.LATIN_52        | BigInteger.valueOf(26)
        "a"     | Alphabet.LATIN_EXTENDED  | BigInteger.valueOf(26)
        "a"     | Alphabet.ALPHANUMERIC_62 | BigInteger.valueOf(26)
    }

}
