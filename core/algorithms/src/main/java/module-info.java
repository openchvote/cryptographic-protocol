/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import ch.openchvote.core.algorithms.*;

/**
 * This module is an implementation of all CHVote algorithms.
 */
module ch.openchvote.core.algorithms {

    requires transitive ch.openchvote.base.utilities;

    exports ch.openchvote.core.algorithms;
    exports ch.openchvote.core.algorithms.parameters.security;
    exports ch.openchvote.core.algorithms.parameters.usability;
    exports ch.openchvote.core.algorithms.protocols.common.algorithms;
    exports ch.openchvote.core.algorithms.protocols.common.model;
    exports ch.openchvote.core.algorithms.general.algorithms;
    exports ch.openchvote.core.algorithms.general.model;
    exports ch.openchvote.core.algorithms.protocols.plain.algorithms;
    exports ch.openchvote.core.algorithms.protocols.plain.model;
    exports ch.openchvote.core.algorithms.protocols.writein.algorithms;
    exports ch.openchvote.core.algorithms.protocols.writein.model;

    uses AlgorithmService;
    provides AlgorithmService with JavaAlgorithmService;

}
