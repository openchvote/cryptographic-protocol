/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.model;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionResult;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Pair;

import java.util.ArrayList;

/**
 * Model class for election results inheriting from {@link Pair}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class ElectionResultPlain extends Pair<IntMatrix, IntMatrix> implements ElectionResult {

    public ElectionResultPlain(IntMatrix bold_V, IntMatrix bold_W) {
        super(bold_V, bold_W);
    }

    @Override
    public IntMatrix get_bold_V() {
        return this.getFirst();
    }

    @Override
    public IntMatrix get_bold_W() {
        return this.getSecond();
    }

    public boolean isEquivalent(ElectionResultPlain other) {
        var n = this.get_bold_V().getWidth();
        if (other.get_bold_V().getWidth() != n) {
            return false;
        }
        var w = this.get_bold_W().getWidth();
        if (other.get_bold_W().getWidth() != w) {
            return false;
        }
        // we consider two election results as equal if all candidates have received the same amount of votes in every counting circle
        for (int k : IntSet.range(1, n)) {
            for (int c : IntSet.range(1, w)) {
                if (this.getNumberOfVotes(k, c) != other.getNumberOfVotes(k, c)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public String toString() {
        var n = this.get_bold_V().getWidth();
        var w = this.get_bold_W().getWidth();

        // initialize output table
        var stringTable = new ArrayList<String>();
        var cols = w + 2;

        // print table header
        IntSet.range(1, cols).forEach(col -> stringTable.add(""));
        stringTable.add("Candidate");
        for (int countingCircle : IntSet.range(1, w)) {
            stringTable.add("CC" + countingCircle);
        }
        stringTable.add("Total");
        IntSet.range(1, cols).forEach(col -> stringTable.add(""));

        // print main table
        for (int candidate : IntSet.range(1, n)) {
            stringTable.add("Candidate-" + candidate);
            for (int c : IntSet.range(1, w)) {
                stringTable.add(Integer.toString(this.getNumberOfVotes(candidate, c)));
            }
            stringTable.add(Integer.toString(this.getNumberOfVotes(candidate)));
        }
        IntSet.range(1, cols).forEach(col -> stringTable.add(""));

        // convert table to string
        return this.toString(stringTable, cols, 0);
    }

}
