/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenges;
import ch.openchvote.core.algorithms.general.algorithms.GetGenerators;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.AugmentedEncryption;
import ch.openchvote.core.algorithms.protocols.writein.model.ShuffleProof;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tuples.Quadruple;
import ch.openchvote.base.utilities.tuples.Quintuple;
import ch.openchvote.base.utilities.tuples.Sextuple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.21 from CHVote Protocol Specification
 */
public final class CheckShuffleProof extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Boolean> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters & NIZKPParameters>
    boolean
    run(ShuffleProof pi, Vector<AugmentedEncryption> bold_e_bar, Vector<AugmentedEncryption> bold_e_tilde, BigInteger pk, Vector<BigInteger> bold_pk_prime, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var g = securityParameters.get_g();
        var h = securityParameters.get_h();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(pi, bold_e_bar, bold_e_tilde, pk, bold_pk_prime);
        var N = bold_e_tilde.getLength();
        var z = bold_pk_prime.getLength();
        Precondition.check(Set.Quadruple(ZZ_twoToTheTau, Set.Sextuple(ZZ_q, ZZ_q, ZZ_q, Set.Pair(ZZ_q, ZZ_q), Set.Vector(ZZ_q, N), Set.Vector(ZZ_q, N)), Set.Vector(ZZPlus_p, N), Set.Vector(ZZPlus_p, N)).contains(pi));
        Precondition.check(Set.Vector(Set.Quadruple(ZZPlus_p, ZZPlus_p, Set.Vector(ZZPlus_p, z), ZZPlus_p), N).contains(bold_e_bar));
        Precondition.check(Set.Vector(Set.Quadruple(ZZPlus_p, ZZPlus_p, Set.Vector(ZZPlus_p, z), ZZPlus_p), N).contains(bold_e_tilde));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.Vector(ZZPlus_p, z).contains(bold_pk_prime));

        // PREPARATION
        var c = pi.get_c();
        var s = pi.get_s();
        var s_1 = s.getFirst();
        var s_2 = s.getSecond();
        var s_3 = s.getThird();
        var s_4 = s.getFourth().getFirst();
        var s_4_prime = s.getFourth().getSecond();
        var bold_s_hat = s.getFifth();
        var bold_s_tilde = s.getSixth();
        var bold_c = pi.get_bold_c();
        var bold_c_hat = pi.get_bold_c_hat();
        var builder_bold_t_hat = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        var bold_h = GetGenerators.run(N, securityParameters);
        var bold_u = GetChallenges.run(N, new Quadruple<>(bold_e_bar, bold_e_tilde, bold_c, pk), securityParameters);
        var c_hat_0 = h;
        var c_bar = ZZPlus_p.divide(ZZPlus_p.prod(bold_c), ZZPlus_p.prod(bold_h));
        var u = ZZ_q.prod(bold_u);
        var c_hat = ZZPlus_p.divide(N == 0 ? c_hat_0 : bold_c_hat.getValue(N), ZZPlus_p.pow(h, u));
        var c_tilde = ZZPlus_p.prodPow(bold_c, bold_u);
        var a_tilde = ZZPlus_p.prodPow(bold_e_bar.map(AugmentedEncryption::get_a), bold_u);
        var b_tilde = ZZPlus_p.prodPow(bold_e_bar.map(AugmentedEncryption::get_b), bold_u);
        for (int i : IntSet.range(1, N)) {
            var c_hat_i_minus_1 = i == 1 ? c_hat_0 : bold_c_hat.getValue(i - 1);
            var t_hat_i = ZZPlus_p.multiply(ZZPlus_p.pow(bold_c_hat.getValue(i), c), ZZPlus_p.multiply(ZZPlus_p.pow(g, bold_s_hat.getValue(i)), ZZPlus_p.pow(c_hat_i_minus_1, bold_s_tilde.getValue(i))));
            builder_bold_t_hat.set(i, t_hat_i);
        }
        var t_1 = ZZPlus_p.multiply(ZZPlus_p.pow(c_bar, c), ZZPlus_p.pow(g, s_1));
        var t_2 = ZZPlus_p.multiply(ZZPlus_p.pow(c_hat, c), ZZPlus_p.pow(g, s_2));
        var t_3 = ZZPlus_p.multiply(ZZPlus_p.pow(c_tilde, c), ZZPlus_p.multiply(ZZPlus_p.pow(g, s_3), ZZPlus_p.prodPow(bold_h, bold_s_tilde)));
        var t_41 = ZZPlus_p.multiply(ZZPlus_p.pow(a_tilde, c), ZZPlus_p.multiply(ZZPlus_p.invert(ZZPlus_p.pow(pk, s_4)), ZZPlus_p.prodPow(bold_e_tilde.map(AugmentedEncryption::get_a), bold_s_tilde)));
        var t_42 = ZZPlus_p.multiply(ZZPlus_p.pow(b_tilde, c), ZZPlus_p.multiply(ZZPlus_p.invert(ZZPlus_p.pow(g, s_4)), ZZPlus_p.prodPow(bold_e_tilde.map(AugmentedEncryption::get_b), bold_s_tilde)));
        var builder_bold_t_43 = new Vector.Builder<BigInteger>(z);
        for (int j : IntSet.range(1, z)) {
            var j_final = j; // necessary for using j in lambda expression
            var a_prime_j = ZZPlus_p.prodPow(bold_e_bar.map(e_i -> e_i.get_bold_a_prime().getValue(j_final)), bold_u);
            var pk_prime_j = bold_pk_prime.getValue(j);
            var t_43_j = ZZPlus_p.multiply(ZZPlus_p.pow(a_prime_j, c), ZZPlus_p.invert(ZZPlus_p.pow(pk_prime_j, s_4_prime)), ZZPlus_p.prodPow(bold_e_tilde.map(e_tilde_i -> e_tilde_i.get_bold_a_prime().getValue(j_final)), bold_s_tilde));
            builder_bold_t_43.set(j, t_43_j);
        }
        var bold_t_43 = builder_bold_t_43.build();
        var b_prime = ZZPlus_p.prodPow(bold_e_bar.map(AugmentedEncryption::get_b_prime), bold_u);
        var t_44 = ZZPlus_p.multiply(ZZPlus_p.pow(b_prime, c), ZZPlus_p.invert(ZZPlus_p.pow(g, s_4_prime)), ZZPlus_p.prodPow(bold_e_tilde.map(AugmentedEncryption::get_b_prime), bold_s_tilde));
        var t = new Quintuple<>(t_1, t_2, t_3, new Quadruple<>(t_41, t_42, bold_t_43, t_44), builder_bold_t_hat.build());
        var y = new Sextuple<>(bold_e_bar, bold_e_tilde, bold_c, bold_c_hat, pk, bold_pk_prime);
        var c_prime = GetChallenge.run(y, t, securityParameters);
        return c.equals(c_prime);
    }

}
