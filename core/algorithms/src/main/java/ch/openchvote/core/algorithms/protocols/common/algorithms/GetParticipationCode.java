/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.ByteArrayToString;
import ch.openchvote.core.algorithms.general.algorithms.RecHash;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Finalization;
import ch.openchvote.core.algorithms.protocols.common.model.Response;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GetAllPoints;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;

/**
 * Implementation of Algorithm 8.37 from CHVote Protocol Specification
 */
public final class GetParticipationCode extends Algorithm<String> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<String> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    <SP extends ZZPlusParameters & GGParameters & HashParameters, UP extends CodeParameters>
    String
    run(Vector<Response> bold_beta, Vector<Finalization> bold_delta, IntVector bold_s, Vector<BigInteger> bold_r, IntVector bold_n, IntVector bold_k, IntVector bold_e_hat, BigInteger pk, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var L_M = securityParameters.get_L_M();

        // USABILITY PARAMETERS
        Precondition.checkNotNull(usabilityParameters);
        var A_PA = usabilityParameters.get_A_PA();
        var L_PA = usabilityParameters.get_L_PA();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_beta, bold_delta, bold_s, bold_r, bold_n, bold_k, bold_e_hat, pk);
        var s = bold_beta.getLength();
        var n = bold_beta.getValue(1).get_bold_C().getHeight();
        var k = bold_s.getLength();
        var t = bold_n.getLength();
        Precondition.check(Set.Vector(Set.Triple(Set.Vector(ZZPlus_p, k), Set.Matrix(Set.B(L_M).orNull(), n, k), ZZPlus_p), s).contains(bold_beta));
        Precondition.check(Set.Vector(Set.Pair(ZZ_q, ZZ_q), s).contains(bold_delta));
        Precondition.check(Set.IntVector(IntSet.range(1, n), k).contains(bold_s));
        Precondition.check(Set.Vector(ZZ_q, k).contains(bold_r));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_e_hat));
        Precondition.check(ZZPlus_p.contains(pk));

        // CONSTRAINTS
        Precondition.check(n == bold_n.sum());
        Precondition.check(k == bold_e_hat.multiply(bold_k));
        Precondition.check(IntVector.allMatch(bold_k, bold_n, SMALLER));
        Precondition.check(bold_s.allMatch(SMALLER));

        // PREPARATION
        var bold_p_builder = new Vector.Builder<ByteArray>(s);

        // ALGORITHM
        for (int j : IntSet.range(1, s)) {
            var bold_p_j = GetAllPoints.run(bold_beta.getValue(j), bold_delta.getValue(j), bold_s, bold_r, bold_n, bold_k, bold_e_hat, pk, securityParameters);
            var P_j = RecHash.run(bold_p_j, securityParameters).truncate(L_PA);
            bold_p_builder.set(j, P_j);
        }
        var bold_p = bold_p_builder.build();
        var PC = ByteArrayToString.run(ByteArray.xor(bold_p, L_PA), A_PA);
        return PC;
    }

}
