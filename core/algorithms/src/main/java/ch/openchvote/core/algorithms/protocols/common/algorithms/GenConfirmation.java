/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.StringToInteger;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.usability.CredentialParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Confirmation;
import ch.openchvote.core.algorithms.protocols.common.model.Point;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GenConfirmationProof;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GetValidityCredential;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.32 from CHVote Protocol Specification
 */
public final class GenConfirmation extends Algorithm<Confirmation> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Confirmation> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "SuspiciousNameCombination", "unused"})
    static public //
    <SP extends GGParameters & NIZKPParameters, UP extends CredentialParameters>
    Confirmation
    run(String Y, Matrix<Point> bold_P, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var g_hat = securityParameters.get_g_hat();

        // USABILITY PARAMETERS
        Precondition.checkNotNull(usabilityParameters);
        var A_Y = usabilityParameters.get_A_Y();
        var ell_Y = usabilityParameters.get_ell_Y();

        // PRECONDITIONS
        Precondition.checkNotNull(Y, bold_P);
        var k = bold_P.getHeight();
        var s = bold_P.getWidth();
        Precondition.check(Set.String(A_Y, ell_Y).contains(Y));
        Precondition.check(Set.Matrix(Set.Pair(ZZ_q_hat, ZZ_q_hat), k, s).contains(bold_P));

        // PREPARATION
        var builder_bold_z = new Vector.Builder<BigInteger>(s);

        // ALGORITHM
        var y = StringToInteger.run(Y, A_Y);
        var y_hat = GG_q_hat.pow(g_hat, y);
        for (int j : IntSet.range(1, s)) {
            var bold_p_j = bold_P.getCol(j);
            var z_j = GetValidityCredential.run(bold_p_j, securityParameters);
            builder_bold_z.set(j, z_j);
        }
        var bold_z = builder_bold_z.build();
        var z = ZZ_q_hat.sum(bold_z);
        var z_hat = GG_q_hat.pow(g_hat, z);
        var pi = GenConfirmationProof.run(y, z, y_hat, z_hat, securityParameters);
        var gamma = new Confirmation(y_hat, z_hat, pi);
        return gamma;
    }

}
