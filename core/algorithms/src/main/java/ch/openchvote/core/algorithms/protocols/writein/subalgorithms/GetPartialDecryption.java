/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.AugmentedEncryption;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.26 from CHVote Protocol Specification
 */
public final class GetPartialDecryption extends Algorithm<Pair<BigInteger, Vector<BigInteger>>> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters>
    Pair<BigInteger, Vector<BigInteger>>
    run(AugmentedEncryption e, BigInteger sk, Vector<BigInteger> bold_sk_prime, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();

        // PREPARATION
        var z = bold_sk_prime.getLength();
        var b = e.get_b();
        var b_prime = e.get_b_prime();
        var builder_bold_c_prime = new Vector.Builder<BigInteger>(z);

        // ALGORITHM
        var c = ZZPlus_p.pow(b, sk);
        for (int j : IntSet.range(1, z)) {
            var sk_prime_j = bold_sk_prime.getValue(j);
            var c_prime_j = ZZPlus_p.pow(b_prime, sk_prime_j);
            builder_bold_c_prime.set(j, c_prime_j);
        }
        var bold_c_prime = builder_bold_c_prime.build();
        return new Pair<>(c, bold_c_prime);
    }

}
