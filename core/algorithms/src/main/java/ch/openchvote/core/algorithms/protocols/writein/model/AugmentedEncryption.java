/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.model;

import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tools.Math;
import ch.openchvote.base.utilities.tuples.Quadruple;

import java.math.BigInteger;

/**
 * Model class for augmented encryptions inheriting from {@link Quadruple}, with specific constructor and getter
 * methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class AugmentedEncryption extends Quadruple<BigInteger, BigInteger, Vector<BigInteger>, BigInteger> implements Comparable<AugmentedEncryption> {

    public AugmentedEncryption(BigInteger a, BigInteger b, Vector<BigInteger> bold_a_prime, BigInteger b_prime) {
        super(a, b, bold_a_prime, b_prime);
    }

    public BigInteger get_a() {
        return this.getFirst();
    }

    public BigInteger get_b() {
        return this.getSecond();
    }

    public Vector<BigInteger> get_bold_a_prime() {
        return this.getThird();
    }

    public BigInteger get_b_prime() {
        return this.getFourth();
    }

    @Override
    public int compareTo(AugmentedEncryption other) {
        var c = this.get_a().compareTo(other.get_a());
        if (c == 0) {
            c = this.get_b().compareTo(other.get_b());
        }
        if (c == 0) { // simplified implementation for a very unlikely case
            c = Math.sum(this.get_bold_a_prime()).compareTo(Math.sum(other.get_bold_a_prime()));
        }
        if (c == 0) {
            c = this.get_b_prime().compareTo(other.get_b_prime());
        }
        return c;
    }
}
