/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.12 from CHVote Protocol Specification
 */
public final class GenPolynomial extends Algorithm<Vector<BigInteger>> {

    // the set {0} consisting of a single value 0
    static private final java.util.Set<BigInteger> ZERO_SET = java.util.Set.of(BigInteger.ZERO);

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends GGParameters>
    Vector<BigInteger>
    run(int d, SP securityParameters) {

        // SECURITY PARAMETERS
        var q_hat = securityParameters.get_q_hat();

        // PREPARATION
        var builder_bold_a = new Vector.Builder.IndicesFromZero<BigInteger>(d == -1 ? 0 : d);

        // ALGORITHM
        if (d == -1) {
            builder_bold_a.set(0, BigInteger.ZERO);
        } else {
            for (int i : IntSet.range(0, d - 1)) {
                var a_i = GenRandomInteger.run(q_hat);
                builder_bold_a.set(i, a_i);
            }
            var a_d = GenRandomInteger.run(q_hat, ZERO_SET);
            builder_bold_a.set(d, a_d);
        }
        var bold_a = builder_bold_a.build();
        return bold_a;
    }

}
