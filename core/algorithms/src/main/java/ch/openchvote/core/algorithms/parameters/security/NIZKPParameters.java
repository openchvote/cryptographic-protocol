/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.parameters.security;

import ch.openchvote.base.utilities.algebra.ZZ;
import ch.openchvote.base.utilities.tools.Math;

import java.math.BigInteger;

/**
 * Instances implementing this interface represent a list of parameters required to generate and verify
 * non-interactive zero-knowledge proofs (NIZKP).
 */
public interface NIZKPParameters extends HashParameters {

    /**
     * Returns the security parameter {@code tau}.
     *
     * @return The security parameter {@code tau}
     */
    int get_tau();

    /**
     * Returns {@code 2^tau} for the security parameter {@code tau}.
     * @return {@code 2^tau}
     */
    default BigInteger get_twoToTheTau() {
        return Math.powerOfTwo(this.get_tau());
    }

    /**
     * Returns the corresponding additive group of integers modulo {@code 2^tau}.
     *
     * @return The corresponding additive group of integers
     */
    default ZZ get_ZZ_twoToTheTau() {
        return ZZ.of(this.get_twoToTheTau());
    }

}
