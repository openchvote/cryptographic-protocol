/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.tools.Math;

/**
 * Implementation of Algorithm 8.2 from CHVote Protocol Specification
 */
public final class IsPrime extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination"})
    static public //
    boolean
    run(int x) {

        // ALGORITHM
        if (x <= 1) {
            return false;
        }
        if (x <= 3) {
            return true;
        }
        if (Math.divides(2, x) || Math.divides(3, x)) {
            return false;
        }
        var i = 5;
        while (i * i <= x) {
            if (Math.divides(i, x) || Math.divides(i + 2, x)) {
                return false;
            }
            i = i + 6;
        }
        return true;
    }

}
