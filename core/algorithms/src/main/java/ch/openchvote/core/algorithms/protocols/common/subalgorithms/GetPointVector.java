/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.ByteArrayToInteger;
import ch.openchvote.core.algorithms.general.algorithms.RecHash;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Point;
import ch.openchvote.core.algorithms.protocols.common.model.Response;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tools.Math;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.29 from CHVote Protocol Specification
 */
public final class GetPointVector extends Algorithm<Vector<Point>> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters & GGParameters & HashParameters>
    Vector<Point>
    run(Response beta, IntVector bold_s, Vector<BigInteger> bold_r, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var q_hat = securityParameters.get_q_hat();
        var L_M = securityParameters.get_L_M();
        var L = securityParameters.get_L();

        // PREPARATION
        var bold_b = beta.get_bold_b();
        var bold_C = beta.get_bold_C();
        var d = beta.get_d();
        var k = bold_b.getLength();
        var builder_bold_p = new Vector.Builder<Point>(k);

        // ALGORITHM
        var ell_M = Math.ceilDiv(L_M, L);
        for (int j : IntSet.range(1, k)) {
            var b_j = bold_b.getValue(j);
            var r_j = bold_r.getValue(j);
            var k_j = ZZPlus_p.multiply(b_j, ZZPlus_p.pow(d, ZZ_q.minus(r_j)));
            var K_j = ByteArray.EMPTY;
            for (int c : IntSet.range(1, ell_M)) {
                K_j = K_j.concatenate(RecHash.run(k_j, c, securityParameters));
            }
            K_j = K_j.truncate(L_M);
            var C_sj_j = bold_C.getValue(bold_s.getValue(j), j);
            if (C_sj_j == null) {
                throw new Exception(Exception.Type.INCOMPATIBLE_MATRIX, GetPointVector.class);
            }
            var M_j = C_sj_j.xor(K_j);
            var x_j = ByteArrayToInteger.run(M_j.truncate(L_M / 2));
            var y_j = ByteArrayToInteger.run(M_j.skip(L_M / 2));
            if (x_j.compareTo(q_hat) >= 0 || y_j.compareTo(q_hat) >= 0) {
                throw new Exception(Exception.Type.INCOMPATIBLE_POINT, GetPointVector.class);
            }
            var p_j = new Point(x_j, y_j);
            builder_bold_p.set(j, p_j);
        }
        var bold_p = builder_bold_p.build();
        return bold_p;
    }

}
