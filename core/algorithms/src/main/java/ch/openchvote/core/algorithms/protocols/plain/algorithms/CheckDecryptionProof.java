/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.core.algorithms.protocols.plain.model.DecryptionProof;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Parallel;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.50 from CHVote Protocol Specification
 */
public final class CheckDecryptionProof extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Boolean> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "BooleanMethodIsAlwaysInverted"})
    static public //
    <SP extends ZZPlusParameters & NIZKPParameters>
    boolean
    run(DecryptionProof pi, BigInteger pk, Vector<Encryption> bold_e, Vector<BigInteger> bold_c, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var g = securityParameters.get_g();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(pi, pk, bold_e, bold_c);
        var N = bold_e.getLength();
        Precondition.check(Set.Pair(ZZ_twoToTheTau, ZZ_q).contains(pi));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.Vector(Set.Pair(ZZPlus_p, ZZPlus_p), N).contains(bold_e));
        Precondition.check(Set.Vector(ZZPlus_p, N).contains(bold_c));

        // PREPARATION
        var c = pi.get_c();
        var s = pi.get_s();
        var builder_bold_t = new Vector.Builder.IndicesFromZero<BigInteger>(N);

        // ALGORITHM
        var t_0 = ZZPlus_p.multiply(ZZPlus_p.pow(pk, c), ZZPlus_p.pow(g, s));
        builder_bold_t.set(0, t_0);
        Parallel.forLoop(1, N, i -> {
            var c_i = bold_c.getValue(i);
            var b_i = bold_e.getValue(i).get_b();
            var t_i = ZZPlus_p.multiply(ZZPlus_p.pow(c_i, c), ZZPlus_p.pow(b_i, s));
            builder_bold_t.set(i, t_i);
        });
        var bold_t = builder_bold_t.build();
        var y = new Triple<>(pk, bold_e, bold_c);
        var c_prime = GetChallenge.run(y, bold_t, securityParameters);
        return c.equals(c_prime);
    }
}
