/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.52 from CHVote Protocol Specification
 */
public final class GetVotes extends Algorithm<Vector<BigInteger>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Vector<BigInteger>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters>
    Vector<BigInteger>
    run(Vector<Encryption> bold_e, Vector<BigInteger> bold_c, Vector<BigInteger> bold_c_prime, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_e, bold_c, bold_c_prime);
        var N = bold_e.getLength();
        Precondition.check(Set.Vector(Set.Pair(ZZPlus_p, ZZPlus_p), N).contains(bold_e));
        Precondition.check(Set.Vector(ZZPlus_p, N).contains(bold_c));
        Precondition.check(Set.Vector(ZZPlus_p, N).contains(bold_c_prime));

        // PREPARATION
        var builder_bold_m = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        for (int i : IntSet.range(1, N)) {
            var a_i = bold_e.getValue(i).get_a();
            var c_i = bold_c.getValue(i);
            var c_prime_i = bold_c_prime.getValue(i);
            var m_i = ZZPlus_p.multiply(a_i, ZZPlus_p.invert(ZZPlus_p.multiply(c_i, c_prime_i)));
            builder_bold_m.set(i, m_i);
        }
        var bold_m = builder_bold_m.build();
        return bold_m;
    }

}
