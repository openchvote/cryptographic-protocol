/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.set.IntSet;

import java.util.stream.IntStream;

/**
 * Implementation of Algorithm 8.42 from CHVote Protocol Specification
 */
public final class GenPermutation extends Algorithm<IntVector> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    IntVector
    run(int N) {

        // PREPARATION
        var builder_psi = new IntVector.Builder(N);

        // ALGORITHM
        int[] I = IntStream.rangeClosed(1, N).toArray();
        for (int i : IntSet.range(0, N - 1)) {
            var k = GenRandomInteger.run(i, N - 1);
            builder_psi.set(i + 1, I[k]);
            I[k] = I[i];
        }
        var psi = builder_psi.build();
        return psi;
    }

}
