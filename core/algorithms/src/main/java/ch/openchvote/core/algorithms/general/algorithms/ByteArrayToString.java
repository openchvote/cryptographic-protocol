/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.set.Alphabet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Math;

import java.nio.charset.StandardCharsets;

/**
 * ALGORITHMS 4.9 and 4.10
 */
public final class ByteArrayToString extends Algorithm<String> {

    // ALGORITHM 4.9
    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    String
    run(ByteArray B, Alphabet A) {

        // PRECONDITIONS
        Precondition.checkNotNull(B, A);
        var n = B.getLength();
        Precondition.check(Set.B(n).contains(B));

        // ALGORITHM
        var N = A.getSize();
        var x_B = ByteArrayToInteger.run(B);
        var m = Math.ceilLog(Math.powerOfTwo(Byte.SIZE * n), N);
        var S = IntegerToString.run(x_B, m, A);
        return S;
    }

    // ALGORITHM 4.10
    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    String
    run(ByteArray B) {

        // PRECONDITIONS
        Precondition.checkNotNull(B);
        Precondition.check(Set.UTF8.contains(B));

        // ALGORITHM
        var S = new String(B.toByteArray(), StandardCharsets.UTF_8);
        return S;
    }

}