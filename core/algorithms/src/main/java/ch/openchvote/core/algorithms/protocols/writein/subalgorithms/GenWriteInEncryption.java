/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.MultiEncryption;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.10 from CHVote Protocol Specification
 */
public final class GenWriteInEncryption extends Algorithm<Pair<MultiEncryption, BigInteger>> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters>
    Pair<MultiEncryption, BigInteger>
    run(Vector<BigInteger> bold_pk, Vector<BigInteger> bold_m, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();

        // PREPARATION
        var z = bold_m.getLength();
        var builder_bold_a = new Vector.Builder<BigInteger>(z);

        // ALGORITHM
        var r = GenRandomInteger.run(q);
        for (int i : IntSet.range(1, z)) {
            var a_i = ZZPlus_p.multiply(bold_m.getValue(i), ZZPlus_p.pow(bold_pk.getValue(i), r));
            builder_bold_a.set(i, a_i);
        }
        var bold_a = builder_bold_a.build();
        var b = ZZPlus_p.pow(g, r);
        var e = new MultiEncryption(bold_a, b);
        return new Pair<>(e, r);
    }

}
