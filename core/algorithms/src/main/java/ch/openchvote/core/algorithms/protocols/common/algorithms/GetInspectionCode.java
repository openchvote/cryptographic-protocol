/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.ByteArrayToString;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.Set;

/**
 * Implementation of Algorithm 8.56 from CHVote Protocol Specification
 */
public final class GetInspectionCode extends Algorithm<String> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<String> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    <UP extends CodeParameters>
    String
    run(Vector<ByteArray> bold_i, UP usabilityParameters) {

        // USABILITY PARAMETERS
        Precondition.checkNotNull(usabilityParameters);
        var A_PA = usabilityParameters.get_A_PA();
        var L_PA = usabilityParameters.get_L_PA();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_i);
        var s = bold_i.getLength();
        Precondition.check(Set.Vector(Set.B(L_PA), s).contains(bold_i));

        // ALGORITHM
        var I = ByteArray.xor(bold_i, L_PA);
        var IC = ByteArrayToString.run(I, A_PA);
        return IC;
    }

}
