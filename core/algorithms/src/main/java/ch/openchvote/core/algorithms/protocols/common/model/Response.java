/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.model;

import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Model class for responses inheriting from {@link Triple}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class Response extends Triple<Vector<BigInteger>, Matrix<ByteArray>, BigInteger> {

    public Response(Vector<BigInteger> bold_b, Matrix<ByteArray> bold_C, BigInteger d) {
        super(bold_b, bold_C, d);
    }

    public Vector<BigInteger> get_bold_b() {
        return this.getFirst();
    }

    public Matrix<ByteArray> get_bold_C() {
        return this.getSecond();
    }

    public BigInteger get_d() {
        return this.getThird();
    }

}
