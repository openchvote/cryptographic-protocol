/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.parameters.usability;

import ch.openchvote.base.utilities.set.Alphabet;
import ch.openchvote.base.utilities.tools.Math;

import java.math.BigInteger;

/**
 * Instances of this interface represent a list of parameters for specifying the alphabets and lengths of the
 * verification, participation, and abstention codes.
 */
public interface CodeParameters extends UsabilityParameters {

    /**
     * Returns the security parameter {@code epsilon}.
     *
     * @return The security parameter {@code epsilon}
     */
    double get_epsilon();

    /**
     * Returns the alphabet used for the verification codes.
     *
     * @return The alphabet used for the verification codes
     */
    Alphabet get_A_V();

    /**
     * Returns the alphabet used for the participation and abstention codes.
     *
     * @return The alphabet used for the participation and abstention codes
     */
    Alphabet get_A_PA();

    /**
     * Returns the maximal number of candidates.
     *
     * @return The maximal number of candidates
     */
    int get_n_max();

    /**
     * Return the length (number of bits) of the verification codes.
     *
     * @return The length of the verification codes
     */
    default int get_ell_V() {
        return Math.ceilLog(Math.powerOfTwo(Byte.SIZE * this.get_L_V()), this.get_A_V().getSize());
    }

    /**
     * Return the length (number of bits) of the participation and abstention codes.
     *
     * @return The length of the participation and abstention codes
     */
    default int get_ell_PA() {
        return Math.ceilLog(Math.powerOfTwo(Byte.SIZE * this.get_L_PA()), this.get_A_PA().getSize());
    }

    /**
     * Return the length (number of bytes) of the verification codes.
     *
     * @return The length of the verification codes
     */
    default int get_L_V() {
        return Math.ceilLog(BigInteger.valueOf((int) ((this.get_n_max() - 1.0) / (1.0 - this.get_epsilon()))), 256);
    }

    /**
     * Return the length (number of bytes) of the participation and abstention codes.
     *
     * @return The length of the participation and abstention codes
     */
    default int get_L_PA() {
        return Math.ceilLog(BigInteger.valueOf((int) (1.0 / (1.0 - this.get_epsilon()))), 256);
    }

}
