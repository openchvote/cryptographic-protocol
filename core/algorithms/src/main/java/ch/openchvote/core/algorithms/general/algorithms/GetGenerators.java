/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.3 from CHVote Protocol Specification
 */
public final class GetGenerators extends Algorithm<Vector<BigInteger>> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters & HashParameters>
    Vector<BigInteger>
    run(int n, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var q = securityParameters.get_q();

        // PRECONDITIONS
        Precondition.check(IntSet.NN.contains(n));

        // PREPARATION
        var builder_bold_h = new Vector.Builder<BigInteger>(n);

        // ALGORITHM
        BigInteger h_i;
        for (int i : IntSet.range(1, n)) {
            var x = 0;
            do {
                x = x + 1;
                var h = ByteArrayToInteger.run(RecHash.run("CHVote", "ggen", i, x, securityParameters));
                h_i = h.mod(q).add(BigInteger.ONE);
            } while (h_i.equals(BigInteger.ONE));
            builder_bold_h.set(i, h_i);
        }
        var bold_h = builder_bold_h.build();
        return bold_h;
    }

}
