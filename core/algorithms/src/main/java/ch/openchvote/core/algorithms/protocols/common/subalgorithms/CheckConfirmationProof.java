/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.protocols.common.model.ConfirmationProof;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.36 from CHVote Protocol Specification
 */
public final class CheckConfirmationProof extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination"})
    static public //
    <SP extends GGParameters & NIZKPParameters>
    boolean
    run(ConfirmationProof pi, BigInteger y_hat, BigInteger z_hat, SP securityParameters) {

        // SECURITY PARAMETERS
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var g_hat = securityParameters.get_g_hat();

        // PREPARATION
        var c = pi.get_c();
        var s = pi.get_s();
        var s_1 = s.getFirst();
        var s_2 = s.getSecond();

        // ALGORITHM
        var y = new Pair<>(y_hat, z_hat);
        var t1 = GG_q_hat.multiply(GG_q_hat.pow(y_hat, c), GG_q_hat.pow(g_hat, s_1));
        var t2 = GG_q_hat.multiply(GG_q_hat.pow(z_hat, c), GG_q_hat.pow(g_hat, s_2));
        var t = new Pair<>(t1, t2);
        var c_prime = GetChallenge.run(y, t, securityParameters);
        return c.equals(c_prime);
    }
}
