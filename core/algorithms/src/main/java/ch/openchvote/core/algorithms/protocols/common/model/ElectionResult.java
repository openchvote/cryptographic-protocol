/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.model;

import ch.openchvote.base.utilities.sequence.IntMatrix;

import java.util.List;

/**
 * The purpose of this interface is to provide a common API for both types of election results (plain, write-ins). It
 * provides default methods for counting the number of votes of specific candidates or specific counting circles based
 * on the vector bold_u and the matrices bold_V and bold_W. These three elements are common to both types of election
 * results.
 */
public interface ElectionResult {

    /**
     * Return the matrix bold_V.
     *
     * @return The matrix bold_V
     */
    IntMatrix get_bold_V();

    /**
     * Return the matrix bold_W.
     *
     * @return The matrix bold_W
     */
    IntMatrix get_bold_W();

    /**
     * Return the matrix of vote counts for all candidates and all counting circles.
     *
     * @return The matrix of vote counts
     */
    default IntMatrix getNumberOfVotes() {
        return this.get_bold_V().transpose().multiply(this.get_bold_W());
    }

    /**
     * Returns the number of votes for candidate k in counting circle c.
     *
     * @param j   Candidate index
     * @param ell Counting circle
     * @return Number of votes for candidate k in counting circle c
     */
    default int getNumberOfVotes(int j, int ell) {
        return this.getNumberOfVotes().getValue(j, ell);
    }

    /**
     * Returns the number of votes for candidate k.
     *
     * @param j Candidate index
     * @return Number of votes for candidate k
     */
    default int getNumberOfVotes(int j) {
        var bold_v_j = this.get_bold_V().getCol(j);
        return bold_v_j.sum();
    }

    /**
     * Default method for printing a table with rows and columns nicely aligned. The contents of the table cells are
     * given as a list of strings. Using the specified table width, these strings are printed from left to right and
     * from top to bottom.
     *
     * @param stringTable The given contents of the table to print
     * @param width       The width of the table
     * @param indent      The overall indent of the printed table
     * @return A string containing the printed table
     */
    default String toString(List<String> stringTable, int width, int indent) {
        var builder = new StringBuilder();
        var cellWidths = new int[width];
        var i = 0;
        for (var str : stringTable) {
            cellWidths[i] = java.lang.Math.max(cellWidths[i], str.length());
            i = (i + 1) % width;
        }
        i = 0;
        var newLine = "";
        for (var str : stringTable) {
            if (i == 0) {
                builder.append(newLine).append(" ".repeat(indent));
            }
            newLine = "\n";
            var cellWidth = cellWidths[i];
            String formattedString;
            if (str.isEmpty()) {
                if (i == width - 1) {
                    formattedString = "-".repeat(cellWidth);
                } else {
                    formattedString = "-".repeat(cellWidth + 2);
                }
            } else if (str.matches("^[0-9]+$")) {
                formattedString = String.format("%" + cellWidth + "s  ", str);
            } else {
                formattedString = String.format("%-" + cellWidth + "s  ", str);
            }
            builder.append(formattedString);
            i = (i + 1) % width;
        }
        return builder.toString();
    }

}
