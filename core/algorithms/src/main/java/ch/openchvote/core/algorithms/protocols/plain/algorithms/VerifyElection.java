/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.algorithms.CheckConfirmation;
import ch.openchvote.core.algorithms.protocols.common.algorithms.CheckCredentialProof;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetPublicCredentials;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetPublicKey;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.core.algorithms.protocols.plain.model.ElectionResultPlain;
import ch.openchvote.core.algorithms.protocols.plain.model.KeyPairProof;
import ch.openchvote.core.algorithms.protocols.plain.model.PublicDataAdministrator;
import ch.openchvote.core.algorithms.protocols.plain.model.PublicDataElectionAuthority;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;
import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER_OR_EQUAL;

/**
 * Implementation of Algorithm 8.54 from CHVote Protocol Specification
 */
public final class VerifyElection extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Boolean> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public //
    <SP extends ZZPlusParameters & GGParameters & NIZKPParameters>
    boolean
    run(IntVector bold_n, IntVector bold_k, IntVector bold_u, IntVector bold_w, IntMatrix bold_E, ElectionResultPlain ER, PublicDataAdministrator PD_A, Vector<PublicDataElectionAuthority> bold_pd, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        try {
            
            // PRECONDITIONS
            Precondition.checkNotNull(bold_n, bold_k, bold_u, bold_w, bold_E, ER, PD_A, bold_pd);
            var t = bold_n.getLength();
            var u = bold_E.getWidth();
            var N_E = bold_w.getLength();
            var k_max = bold_E.select(bold_u).multiply(bold_k).maxOrZero();
            var bold_V = ER.get_bold_V();
            var N = bold_V.getHeight();
            var n = bold_V.getWidth();
            var bold_W = ER.get_bold_W();
            var w = bold_W.getWidth();
            var s = bold_pd.getLength();
            Precondition.check(Set.IntVector(IntSet.range(1, u), t).contains(bold_u));
            Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
            Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
            Precondition.check(Set.IntVector(IntSet.range(1, w), N_E).contains(bold_w));
            Precondition.check(Set.IntMatrix(IntSet.BB, N_E, u).contains(bold_E));
            Precondition.check(Set.Pair(Set.IntMatrix(IntSet.BB, N, n), Set.IntMatrix(IntSet.BB, N, w)).contains(ER));
            Precondition.check(Set.Quadruple(
                            ZZPlus_p, // pk
                            Set.Pair(ZZ_twoToTheTau, ZZ_q), // pi
                            Set.Vector(ZZPlus_p, N), // bold_c
                            Set.Pair(ZZ_twoToTheTau, ZZ_q)) // pi_prime
                    .contains(PD_A));
            Precondition.check(Set.Vector(Set.DuoDecuple(
                            ZZPlus_p, // pk
                            Set.Pair(ZZ_twoToTheTau, ZZ_q), // pi
                            Set.Vector(GG_q_hat, N_E), Set.Vector(GG_q_hat, N_E), Set.Vector(GG_q_hat, N_E), // bold_x_hat, bold_y_hat, bold_z_hat
                            Set.Pair(ZZ_twoToTheTau, Set.Vector(Set.Triple(ZZ_q_hat, ZZ_q_hat, ZZ_q_hat), N_E)), // pi_hat
                            Set.Vector(Set.Pair(ZZPlus_p, ZZPlus_p), N), // bold_e_tilde
                            Set.Quadruple(ZZ_twoToTheTau, Set.Sextuple(ZZ_q, ZZ_q, ZZ_q, ZZ_q, Set.Vector(ZZ_q, N), Set.Vector(ZZ_q, N)), Set.Vector(ZZPlus_p, N), Set.Vector(ZZPlus_p, N)), // pi_tilde
                            Set.Vector(ZZPlus_p, N), // bold_c
                            Set.Pair(ZZ_twoToTheTau, ZZ_q), // pi_prime
                            Set.IndexedFamily(IntSet.range(1, N_E), Set.Triple(GG_q_hat, Set.Vector(Set.Pair(ZZPlus_p, ZZPlus_p), 0, k_max), Set.Pair(ZZ_twoToTheTau, Set.Triple(ZZ_q_hat, ZZPlus_p, ZZ_q)))), // B
                            Set.IndexedFamily(IntSet.range(1, N_E), Set.Triple(GG_q_hat, GG_q_hat, Set.Pair(ZZ_twoToTheTau, Set.Pair(ZZ_q_hat, ZZ_q_hat))))),
                    s).contains(bold_pd));

            // CONSTRAINTS
            Precondition.check(s >= 1);
            Precondition.check(bold_u.allMatch(SMALLER_OR_EQUAL));
            Precondition.check(n == bold_n.sum());
            Precondition.check(IntVector.allMatch(bold_k, bold_n, SMALLER));

            // PREPARATION
            var bold_pk = new Vector.Builder.IndicesFromZero<BigInteger>(s)
                    .add(PD_A.get_pk()).addAll(bold_pd.map(PublicDataElectionAuthority::get_pk))
                    .build();
            var bold_pi = new Vector.Builder.IndicesFromZero<KeyPairProof>(s)
                    .add(PD_A.get_pi()).addAll(bold_pd.map(PublicDataElectionAuthority::get_pi))
                    .build();
            var builder_arrow_bold_e = new Vector.Builder<Vector<Encryption>>(s);

            // ALGORITHM
            for (int j : IntSet.range(0, s)) {
                var pk_j = bold_pk.getValue(j);
                var pi_j = bold_pi.getValue(j);
                if (!CheckKeyPairProof.run(pi_j, pk_j, securityParameters)) {
                    return false;
                }
            }
            var pk = GetPublicKey.run(bold_pk, securityParameters);
            for (int j : IntSet.range(1, s)) {
                var PD_j = bold_pd.getValue(j);
                var bold_x_hat_j = PD_j.get_bold_x_hat();
                var bold_y_hat_j = PD_j.get_bold_y_hat();
                var bold_z_hat_j = PD_j.get_bold_z_hat();
                var pi_hat_j = PD_j.get_pi_hat();
                if (!CheckCredentialProof.run(pi_hat_j, bold_x_hat_j, bold_y_hat_j, bold_z_hat_j, securityParameters)) {
                    return false;
                }
            }
            var bold_X_hat = Matrix.ofCols(bold_pd.map(PublicDataElectionAuthority::get_bold_x_hat));
            var bold_Y_hat = Matrix.ofCols(bold_pd.map(PublicDataElectionAuthority::get_bold_y_hat));
            var bold_Z_hat = Matrix.ofCols(bold_pd.map(PublicDataElectionAuthority::get_bold_z_hat));
            var publicCredentials = GetPublicCredentials.run(bold_X_hat, bold_Y_hat, bold_Z_hat, securityParameters);
            var bold_x_hat = publicCredentials.get_bold_x_hat();
            var bold_y_hat = publicCredentials.get_bold_y_hat();
            var bold_z_hat = publicCredentials.get_bold_z_hat();
            for (int j : IntSet.range(1, s)) {
                var PD_j = bold_pd.getValue(j);
                var B_j = PD_j.get_B();
                for (var ballot : B_j) {
                    var v = ballot.getIndex();
                    var alpha = ballot.getElement();
                    if (!CheckBallot.run(v, alpha, pk, bold_k, bold_u, bold_E, bold_x_hat, securityParameters)) {
                        return false;
                    }
                }
                var C_j = PD_j.get_C();
                for (var confirmation : C_j) {
                    var v = confirmation.getIndex();
                    var gamma = confirmation.getElement();
                    if (!CheckConfirmation.run(v, gamma, bold_y_hat, bold_z_hat, securityParameters)) {
                        return false;
                    }
                }
                var bold_e_j = GetEncryptions.run(B_j, C_j, bold_n, bold_k, bold_u, bold_w, bold_E, securityParameters);
                builder_arrow_bold_e.add(bold_e_j);
            }
            var arrow_bold_e = builder_arrow_bold_e.build();
            if (!arrow_bold_e.isConstant()) {
                return false;
            }
            var bold_e_tilde_j_minus_1 = arrow_bold_e.getValue(1);
            var bold_e_tilde_s = bold_pd.getValue(s).get_bold_e_tilde();
            for (int j : IntSet.range(1, s)) {
                var PD_j = bold_pd.getValue(j);
                var pi_tilde_j = PD_j.get_pi_tilde();
                var bold_e_tilde_j = PD_j.get_bold_e_tilde();
                if (!CheckShuffleProof.run(pi_tilde_j, bold_e_tilde_j_minus_1, bold_e_tilde_j, pk, securityParameters)) {
                    return false;
                }
                bold_e_tilde_j_minus_1 = bold_e_tilde_j;
                var pi_prime_j = PD_j.get_pi_prime();
                var pk_j = PD_j.get_pk();
                var bold_c_j = PD_j.get_bold_c();
                if (!CheckDecryptionProof.run(pi_prime_j, pk_j, bold_e_tilde_s, bold_c_j, securityParameters)) {
                    return false;
                }
            }
            var pk_0 = PD_A.get_pk();
            var bold_c_0 = PD_A.get_bold_c();
            var pi_prime_0 = PD_A.get_pi_prime();
            if (!CheckDecryptionProof.run(pi_prime_0, pk_0, bold_e_tilde_s, bold_c_0, securityParameters)) {
                return false;
            }
            var bold_C = Matrix.ofCols(bold_pd.map(PublicDataElectionAuthority::get_bold_c));
            var bold_c = GetCombinedDecryptions.run(bold_C, securityParameters);
            var bold_m = GetVotes.run(bold_e_tilde_s, bold_c, bold_c_0, securityParameters);
            var ER_prime = GetElectionResult.run(bold_m, bold_n, bold_w, securityParameters);
            return ER.equals(ER_prime);

        } catch (Algorithm.Exception exception) {
            return false;
        }
    }

}
