/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.KeyPairProof;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.4 from CHVote Protocol Specification
 */
public final class GenKeyPairProof extends Algorithm<KeyPairProof> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<KeyPairProof> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    <SP extends ZZPlusParameters & NIZKPParameters>
    KeyPairProof
    run(BigInteger sk, BigInteger pk, Vector<BigInteger> bold_sk_prime, Vector<BigInteger> bold_pk_prime, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();

        // PRECONDITIONS
        Precondition.checkNotNull(sk, pk, bold_sk_prime, bold_pk_prime);
        var z = bold_sk_prime.getLength();
        Precondition.check(ZZ_q.contains(sk));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.Vector(ZZ_q, z).contains(bold_sk_prime));
        Precondition.check(Set.Vector(ZZPlus_p, z).contains(bold_pk_prime));

        // PREPARATION
        var builder_bold_omega = new Vector.Builder.IndicesFromZero<BigInteger>(z);
        var builder_bold_t = new Vector.Builder.IndicesFromZero<BigInteger>(z);
        var builder_bold_s = new Vector.Builder.IndicesFromZero<BigInteger>(z);

        // ALGORITHM
        for (int i : IntSet.range(0, z)) {
            var omega_i = GenRandomInteger.run(q);
            var t_i = ZZPlus_p.pow(g, omega_i);
            builder_bold_omega.set(i, omega_i);
            builder_bold_t.set(i, t_i);
        }
        var bold_omega = builder_bold_omega.build();
        var bold_t = builder_bold_t.build();
        var y = new Pair<>(pk, bold_pk_prime);
        var c = GetChallenge.run(y, bold_t, securityParameters);
        var omega_0 = bold_omega.getValue(0);
        var s_0 = ZZ_q.subtract(omega_0, ZZ_q.multiply(c, sk));
        builder_bold_s.set(0, s_0);
        for (int i : IntSet.range(1, z)) {
            var omega_i = bold_omega.getValue(i);
            var sk_i = bold_sk_prime.getValue(i);
            var s_i = ZZ_q.subtract(omega_i, ZZ_q.multiply(c, sk_i));
            builder_bold_s.set(i, s_i);
        }
        var bold_s = builder_bold_s.build();
        var pi = new KeyPairProof(c, bold_s);
        return pi;
    }

}
