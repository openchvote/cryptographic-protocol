/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.model;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionResult;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Quadruple;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Model class for election results inheriting from {@link Quadruple}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class ElectionResultWriteIn extends Quadruple<IntMatrix, IntMatrix, Matrix<WriteIn>, IntMatrix> implements ElectionResult {

    public ElectionResultWriteIn(IntMatrix bold_V, IntMatrix bold_W, Matrix<WriteIn> bold_S, IntMatrix bold_T) {
        super(bold_V, bold_W, bold_S, bold_T);
    }

    @Override
    public IntMatrix get_bold_V() {
        return this.getFirst();
    }

    @Override
    public IntMatrix get_bold_W() {
        return this.getSecond();
    }

    public Matrix<WriteIn> get_bold_S() {
        return this.getThird();
    }

    public IntMatrix get_bold_T() {
        return this.getFourth();
    }

    public Set<Integer> getWriteInElections() {
        return this.get_bold_T().toIntStream()
                .filter(j -> j > 0)
                .boxed()
                .collect(Collectors.toSet());
    }

    public Set<WriteIn> getWriteIns() {
        var writeIns = new TreeSet<WriteIn>();
        var N = this.get_bold_S().getHeight();
        var z = this.get_bold_S().getWidth();
        for (int i : IntSet.range(1, N)) {
            for (int k : IntSet.range(1, z)) {
                var S_ik = this.get_bold_S().getValue(i, k);
                if (S_ik != null && !S_ik.equals(WriteIn.EMPTY)) {
                    writeIns.add(S_ik);
                }
            }
        }
        return writeIns;
    }

    /**
     * Returns the set of write-ins submitted to election j.
     *
     * @param j Election index
     * @return The set of write-ins submitted to election j
     */
    public Set<WriteIn> getWriteIns(int j) {
        var writeIns = new TreeSet<WriteIn>();
        var N = this.get_bold_S().getHeight();
        var z = this.get_bold_S().getWidth();
        for (int i : IntSet.range(1, N)) {
            for (int k : IntSet.range(1, z)) {
                var t_ik = this.get_bold_T().getValue(i, k);
                if (t_ik == j) {
                    var S_ik = this.get_bold_S().getValue(i, k);
                    if (S_ik != null && !S_ik.equals(WriteIn.EMPTY)) {
                        writeIns.add(S_ik);
                    }
                }
            }
        }
        return writeIns;
    }

    public int getNumberOfWriteIns(int j) {
        var result = 0;
        var N = this.get_bold_S().getHeight();
        var z = this.get_bold_S().getWidth();
        for (int i : IntSet.range(1, N)) {
            for (int k : IntSet.range(1, z)) {
                var t_ik = this.get_bold_T().getValue(i, k);
                if (t_ik == j) {
                    var S_ik = this.get_bold_S().getValue(i, k);
                    if (S_ik != null && !S_ik.equals(WriteIn.EMPTY)) {
                        result++;
                    }
                }
            }
        }
        return result;
    }

    public int getNumberOfWriteIns(WriteIn S, int c, int j) {
        var result = 0;
        var N = this.get_bold_S().getHeight();
        var z = this.get_bold_S().getWidth();
        for (int i : IntSet.range(1, N)) {
            for (int k : IntSet.range(1, z)) {
                var S_ik = this.get_bold_S().getValue(i, k);
                var t_ik = this.get_bold_T().getValue(i, k);
                var w_ic = this.get_bold_W().getValue(i, c);
                if (S.equals(S_ik) && t_ik == j && w_ic == 1) {
                    result = result + 1;
                }
            }
        }
        return result;
    }

    public int getNumberOfWriteIns(WriteIn S, int j) {
        var result = 0;
        var N = this.get_bold_S().getHeight();
        var z = this.get_bold_S().getWidth();
        for (int i : IntSet.range(1, N)) {
            for (int k : IntSet.range(1, z)) {
                var S_ik = this.get_bold_S().getValue(i, k);
                var t_ik = this.get_bold_T().getValue(i, k);
                if (S.equals(S_ik) && t_ik == j) {
                    result = result + 1;
                }
            }
        }
        return result;
    }

    public boolean isEquivalent(ElectionResultWriteIn other) {
        var n = this.get_bold_V().getWidth();
        if (other.get_bold_V().getWidth() != n) {
            return false;
        }
        var w = this.get_bold_W().getWidth();
        if (other.get_bold_W().getWidth() != w) {
            return false;
        }
        // we consider two election results as equal if all candidates have received the same amount of votes in every counting circle
        for (int k : IntSet.range(1, n)) {
            for (int c : IntSet.range(1, w)) {
                if (this.getNumberOfVotes(k, c) != other.getNumberOfVotes(k, c)) {
                    return false;
                }
            }
        }
        // plus if the sets of write-ins are the same and each of them has received the same amount of votes in every counting circle
        var J = this.getWriteInElections();
        if (!other.getWriteInElections().equals(J)) {
            return false;
        }
        for (int j : J) {
            var writeIns = this.getWriteIns(j);
            if (!writeIns.equals(other.getWriteIns(j))) {
                return false;
            }
            for (var writeIn : writeIns) {
                for (int c : IntSet.range(1, w)) {
                    if (this.getNumberOfWriteIns(writeIn, c, j) != other.getNumberOfWriteIns(writeIn, c, j)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public String toString() {
        var n = this.get_bold_V().getWidth();
        var w = this.get_bold_W().getWidth();

        // initialize output table
        var stringTable = new ArrayList<String>();
        var cols = w + 2;

        // print table header
        IntSet.range(1, cols).forEach(col -> stringTable.add(""));
        stringTable.add("Candidate");
        for (int countingCircle : IntSet.range(1, w)) {
            stringTable.add("CC" + countingCircle);
        }
        stringTable.add("Total");
        IntSet.range(1, cols).forEach(col -> stringTable.add(""));

        // print main table
        for (int candidate : IntSet.range(1, n)) {
            stringTable.add("Candidate-" + candidate);
            for (int c : IntSet.range(1, w)) {
                stringTable.add(Integer.toString(this.getNumberOfVotes(candidate, c)));
            }
            stringTable.add(Integer.toString(this.getNumberOfVotes(candidate)));
        }
        IntSet.range(1, cols).forEach(col -> stringTable.add(""));

        // print write-ins
        for (int j : this.getWriteInElections()) {
            for (var S : this.getWriteIns(j)) {
                stringTable.add(S.toString());
                for (int c : IntSet.range(1, w)) {
                    stringTable.add(Integer.toString(this.getNumberOfWriteIns(S, c, j)));
                }
                stringTable.add(Integer.toString(this.getNumberOfWriteIns(S, j)));
            }
            IntSet.range(1, cols).forEach(col -> stringTable.add(""));
        }

        // convert table to string
        return this.toString(stringTable, cols, 0);
    }

}
