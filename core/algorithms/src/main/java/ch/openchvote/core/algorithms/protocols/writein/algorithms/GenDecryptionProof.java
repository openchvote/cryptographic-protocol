/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.AugmentedEncryption;
import ch.openchvote.core.algorithms.protocols.writein.model.DecryptionProof;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tuples.Quintuple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.23 from CHVote Protocol Specification
 */
public final class GenDecryptionProof extends Algorithm<DecryptionProof> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<DecryptionProof> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    <SP extends ZZPlusParameters & NIZKPParameters>
    DecryptionProof
    run(BigInteger sk, BigInteger pk, Vector<BigInteger> bold_sk_prime, Vector<BigInteger> bold_pk_prime, Vector<AugmentedEncryption> bold_e_bar, Vector<BigInteger> bold_c, Matrix<BigInteger> bold_D, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();

        // PRECONDITIONS
        Precondition.checkNotNull(sk, pk, bold_sk_prime, bold_pk_prime, bold_e_bar, bold_c, bold_D);
        var z = bold_sk_prime.getLength();
        var N = bold_e_bar.getLength();
        Precondition.check(ZZ_q.contains(sk));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.Vector(ZZ_q, z).contains(bold_sk_prime));
        Precondition.check(Set.Vector(ZZPlus_p, z).contains(bold_pk_prime));
        Precondition.check(Set.Vector(Set.Quadruple(ZZPlus_p, ZZPlus_p, Set.Vector(ZZPlus_p, z), ZZPlus_p), N).contains(bold_e_bar));
        Precondition.check(Set.Vector(ZZPlus_p, N).contains(bold_c));
        Precondition.check(Set.Matrix(ZZPlus_p, N, z).contains(bold_D));

        // PREPARATION
        var builder_bold_omega = new Vector.Builder.IndicesFromZero<BigInteger>(z);
        var builder_bold_T = new Matrix.Builder.IndicesFromZero<BigInteger>(N, z);
        var builder_bold_s = new Vector.Builder.IndicesFromZero<BigInteger>(z);

        // ALGORITHM
        for (int j : IntSet.range(0, z)) {
            var omega_j = GenRandomInteger.run(q);
            for (int i : IntSet.range(0, N)) {
                BigInteger t_ij;
                if (i == 0) {
                    t_ij = ZZPlus_p.pow(g, omega_j);
                } else {
                    var e_bar_i = bold_e_bar.getValue(i);
                    if (j == 0) {
                        var b_i = e_bar_i.get_b();
                        t_ij = ZZPlus_p.pow(b_i, omega_j);
                    } else {
                        var b_prime_i = e_bar_i.get_b_prime();
                        t_ij = ZZPlus_p.pow(b_prime_i, omega_j);
                    }
                }
                builder_bold_T.set(i, j, t_ij);
            }
            builder_bold_omega.set(j, omega_j);
        }
        var bold_Omega = builder_bold_omega.build();
        var bold_T = builder_bold_T.build();
        var y = new Quintuple<>(pk, bold_pk_prime, bold_e_bar, bold_c, bold_D);
        var c = GetChallenge.run(y, bold_T, securityParameters);
        for (int j : IntSet.range(0, z)) {
            var omega_j = bold_Omega.getValue(j);
            BigInteger s_j;
            if (j == 0) {
                s_j = ZZ_q.subtract(omega_j, ZZ_q.multiply(c, sk));
            } else {
                var sk_prime_j = bold_sk_prime.getValue(j);
                s_j = ZZ_q.subtract(omega_j, ZZ_q.multiply(c, sk_prime_j));
            }
            builder_bold_s.set(j, s_j);
        }
        var bold_s = builder_bold_s.build();
        var pi = new DecryptionProof(c, bold_s);
        return pi;
    }

}
