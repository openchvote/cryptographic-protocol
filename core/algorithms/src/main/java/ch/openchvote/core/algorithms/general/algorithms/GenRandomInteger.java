/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.algebra.ZZ;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Math;

import java.math.BigInteger;

/**
 * ALGORITHMS 4.11, 4.12, 4.13
 */
public final class GenRandomInteger extends Algorithm<BigInteger> {

    // ALGORITHM 4.11 (version for BigInteger values)
    @SuppressWarnings("MissingJavadoc")
    static public //
    BigInteger
    run(BigInteger n) {

        // PRECONDITIONS
        Precondition.check(Set.NN_plus.contains(n));

        // ALGORITHM
        BigInteger r;
        var ell = n.subtract(BigInteger.ONE).bitLength();
        var L = Math.ceilDiv(ell, Byte.SIZE);
        var k = Byte.SIZE * L - ell;
        do {
            var R = RandomBytes.run(L);
            for (int i : IntSet.range(1, k)) {
                R = SetBit.run(R, Byte.SIZE - i, 0x00);
            }
            r = ByteArrayToInteger.run(R);
        } while (r.compareTo(n) >= 0);
        return r;
    }

    // ALGORITHM 4.11 (version for int values)
    @SuppressWarnings("MissingJavadoc")
    static public //
    int
    run(int n) {
        return GenRandomInteger.run(BigInteger.valueOf(n)).intValue();
    }

    // ALGORITHM 4.12
    @SuppressWarnings("MissingJavadoc")
    static public //
    BigInteger
    run(BigInteger n, java.util.Set<BigInteger> X) {

        // PRECONDITIONS
        Precondition.checkNotNull(n, X);
        Precondition.check(Set.NN_plus.contains(n));
        Precondition.check(ZZ.of(n).containsAll(X));

        // ALGORITHM
        BigInteger r;
        do {
            r = GenRandomInteger.run(n);
        } while (X.contains(r));
        return r;
    }

    // ALGORITHM 4.13 (version for BigInteger values)
    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    BigInteger
    run(BigInteger a, BigInteger b) {

        // PRECONDITIONS
        Precondition.check(a.compareTo(b) <= 0);

        // ALGORITHM
        var r_prime = GenRandomInteger.run(b.subtract(a).add(BigInteger.ONE));
        var r = a.add(r_prime);
        return r;
    }

    // ALGORITHM 4.13 (version for int values)
    @SuppressWarnings("MissingJavadoc")
    static public //
    int
    run(int a, int b) {
        return GenRandomInteger.run(BigInteger.valueOf(a), BigInteger.valueOf(b)).intValue();
    }

}
