/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.IntegerToString;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteIn;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.Alphabet;
import ch.openchvote.base.utilities.set.IntSet;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.2 from CHVote Protocol Specification
 */
public final class GetDecodedStrings extends Algorithm<WriteIn> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<WriteIn> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters>
    WriteIn
    run(BigInteger x, Alphabet A, int ell, char c, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();

        // PRECONDITIONS
        Precondition.checkNotNull(x, A);
        Precondition.check(A.getSize() >= 2);
        Precondition.check(ZZPlus_p.contains(x));
        Precondition.check(IntSet.NN.contains(ell));
        Precondition.check(ZZ_q.contains(BigInteger.valueOf(A.getSize()).add(BigInteger.ONE).pow(2 * ell)));
        Precondition.check(!A.contains(c));

        // ALGORITHM
        var S12 = IntegerToString.run(x.subtract(BigInteger.ONE), 2 * ell, A.addCharacter(c));
        var S1 = S12.substring(0, ell);
        while (!S1.isEmpty() && S1.charAt(0) == c) {
            S1 = S1.substring(1);
        }
        var S2 = S12.substring(ell, 2 * ell);
        while (!S2.isEmpty() && S2.charAt(0) == c) {
            S2 = S2.substring(1);
        }
        var S = new WriteIn(S1, S2);
        return S;
    }

}
