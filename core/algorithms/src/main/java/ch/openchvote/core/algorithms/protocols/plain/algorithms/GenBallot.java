/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetPrimes;
import ch.openchvote.core.algorithms.general.algorithms.StringToInteger;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.parameters.usability.CredentialParameters;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GenBallotProof;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GenQuery;
import ch.openchvote.core.algorithms.protocols.plain.model.Ballot;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Math;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;

/**
 * Implementation of Algorithm 8.22 from CHVote Protocol Specification
 */
public final class GenBallot extends Algorithm<Pair<Ballot, Vector<BigInteger>>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Pair<Ballot, Vector<BigInteger>>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination", "unused"})
    static public //
    <SP extends ZZPlusParameters & GGParameters & NIZKPParameters, UP extends CredentialParameters>
    Pair<Ballot, Vector<BigInteger>>
    run(String X, IntVector bold_s, BigInteger pk, IntVector bold_n, int w, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var q = securityParameters.get_q();
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var g_hat = securityParameters.get_g_hat();

        // USABILITY PARAMETERS
        Precondition.checkNotNull(usabilityParameters);
        var A_X = usabilityParameters.get_A_X();
        var ell_X = usabilityParameters.get_ell_X();

        // PRECONDITIONS
        Precondition.checkNotNull(X, bold_s, pk, bold_n);
        var k = bold_s.getLength();
        var t = bold_n.getLength();
        var n = bold_n.sum();
        Precondition.check(Set.String(A_X, ell_X).contains(X));
        Precondition.check(Set.IntVector(IntSet.range(1, n), k).contains(bold_s));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(IntSet.NN_plus.contains(w));

        // CONSTRAINTS
        Precondition.check(bold_s.allMatch(SMALLER));

        // ALGORITHM
        var x = StringToInteger.run(X, A_X);
        var x_hat = GG_q_hat.pow(g_hat, x);
        var bold_p = GetPrimes.run(n + w, securityParameters);
        var bold_m = bold_p.select(bold_s);
        var m = Math.prod(bold_m);
        if (bold_p.getValue(n + w).multiply(m).compareTo(q) > 0) {
            throw new Exception(Exception.Type.INCOMPATIBLE_POINT, GenBallot.class);
        }
        var pair = GenQuery.run(bold_m, pk, securityParameters);
        var bold_a = pair.getFirst();
        var bold_r = pair.getSecond();
        var r = ZZ_q.sum(bold_r);
        var pi = GenBallotProof.run(x, ZZPlus_p.mapToGroup(m), r, x_hat, bold_a, pk, securityParameters);
        var alpha = new Ballot(x_hat, bold_a, pi);
        return new Pair<>(alpha, bold_r);
    }

}

