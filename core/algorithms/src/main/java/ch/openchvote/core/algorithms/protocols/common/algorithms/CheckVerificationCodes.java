/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.Alphabet;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;

/**
 * Implementation of Algorithm 8.31 from CHVote Protocol Specification
 */
public final class CheckVerificationCodes extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Boolean> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "BooleanMethodIsAlwaysInverted", "unused"})
    static public //
    boolean
    run(Vector<String> bold_vc, Vector<String> bold_vc_prime, IntVector bold_s) {

        // PRECONDITIONS
        Precondition.checkNotNull(bold_vc, bold_vc_prime, bold_s);
        var n = bold_vc.getLength();
        var k = bold_vc_prime.getLength();
        Precondition.check(Set.Vector(Alphabet.UCS_star, n).contains(bold_vc));
        Precondition.check(Set.Vector(Alphabet.UCS_star, k).contains(bold_vc_prime));
        Precondition.check(Set.IntVector(IntSet.range(1, n), k).contains(bold_s));

        // CONSTRAINTS
        Precondition.check(bold_s.allMatch(SMALLER));

        // ALGORITHM
        for (int j : IntSet.range(1, k)) {
            var VC_s_j = bold_vc.getValue(bold_s.getValue(j));
            var VC_prime_j = bold_vc_prime.getValue(j);
            if (!VC_s_j.equals(VC_prime_j)) {
                return false;
            }
        }
        return true;
    }

}
