/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.model;

import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Model class for decryption proofs inheriting from {@link Pair}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class DecryptionProof extends Pair<BigInteger, Vector<BigInteger>> {

    public DecryptionProof(BigInteger c, Vector<BigInteger> bold_s) {
        super(c, bold_s);
    }

    public BigInteger get_c() {
        return this.getFirst();
    }

    public Vector<BigInteger> get_bold_s() {
        return this.getSecond();
    }

}
