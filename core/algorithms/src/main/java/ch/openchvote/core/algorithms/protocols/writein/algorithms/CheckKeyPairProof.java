/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.KeyPairProof;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.5 from CHVote Protocol Specification
 */
public final class CheckKeyPairProof extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Boolean> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "BooleanMethodIsAlwaysInverted"})
    static public //
    <SP extends ZZPlusParameters & NIZKPParameters>
    boolean
    run(KeyPairProof pi, BigInteger pk, Vector<BigInteger> bold_pk_prime, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var g = securityParameters.get_g();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(pi, pk, bold_pk_prime);
        var z = bold_pk_prime.getLength();
        Precondition.check(Set.Pair(ZZ_twoToTheTau, Set.Vector(ZZ_q, z + 1)).contains(pi));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.Vector(ZZPlus_p, z).contains(bold_pk_prime));

        // PREPARATION
        var c = pi.get_c();
        var bold_s = pi.get_bold_s();
        var builder_bold_t = new Vector.Builder.IndicesFromZero<BigInteger>(z);

        // ALGORITHM
        var s_0 = bold_s.getValue(0);
        var t_0 = ZZPlus_p.multiply(ZZPlus_p.pow(pk, c), ZZPlus_p.pow(g, s_0));
        builder_bold_t.set(0, t_0);
        for (int i : IntSet.range(1, z)) {
            var s_i = bold_s.getValue(i);
            var pk_prime_i = bold_pk_prime.getValue(i);
            var t_i = ZZPlus_p.multiply(ZZPlus_p.pow(pk_prime_i, c), ZZPlus_p.pow(g, s_i));
            builder_bold_t.set(i, t_i);
        }
        var bold_t = builder_bold_t.build();
        var y = new Pair<>(pk, bold_pk_prime);
        var c_prime = GetChallenge.run(y, bold_t, securityParameters);
        return c.equals(c_prime);
    }

}
