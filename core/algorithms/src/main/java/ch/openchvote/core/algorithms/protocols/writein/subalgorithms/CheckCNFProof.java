/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteInProof;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Pair;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.16 from CHVote Protocol Specification
 */
public final class CheckCNFProof extends Algorithm<Boolean> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters & NIZKPParameters>
    boolean
    run(WriteInProof pi, Matrix<Triple<BigInteger, BigInteger, Encryption>> bold_Y, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var g = securityParameters.get_g();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PREPARATION
        var m = bold_Y.getHeight();
        var n = bold_Y.getWidth();
        var bold_C = pi.get_bold_C();
        var bold_S = pi.get_bold_S();
        var builder_bold_c = new Vector.Builder<BigInteger>(m);
        var builder_bold_T = new Matrix.Builder<Pair<BigInteger, BigInteger>>(m, n);

        // ALGORITHM
        for (int i : IntSet.range(1, m)) {
            var c_i = ZZ_twoToTheTau.sum(bold_C.getRow(i));
            builder_bold_c.set(i, c_i);
            for (int j : IntSet.range(1, n)) {
                var y_ij = bold_Y.getValue(i, j);
                var pk_ij = y_ij.getFirst();
                var m_ij = y_ij.getSecond();
                var e_ij = y_ij.getThird();
                var a_ij = e_ij.get_a();
                var b_ij = e_ij.get_b();
                var c_ij = bold_C.getValue(i, j);
                var s_ij = bold_S.getValue(i, j);
                var t_ij = new Pair<>(ZZPlus_p.multiply(ZZPlus_p.pow(pk_ij, s_ij), ZZPlus_p.pow(ZZPlus_p.divide(a_ij, m_ij), c_ij)), ZZPlus_p.multiply(ZZPlus_p.pow(g, s_ij), ZZPlus_p.pow(b_ij, c_ij)));
                builder_bold_T.set(i, j, t_ij);
            }
        }
        var bold_c = builder_bold_c.build();
        var bold_T = builder_bold_T.build();
        var c_prime = GetChallenge.run(bold_Y, bold_T, securityParameters);
        return bold_c.isConstant() && (m == 0 || bold_c.getValue(1).equals(c_prime));
    }

}
