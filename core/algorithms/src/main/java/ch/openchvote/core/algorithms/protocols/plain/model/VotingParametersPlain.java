/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.model;

import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.tuples.Sextuple;

/**
 * Model class for voting parameters inheriting from {@link Sextuple}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class VotingParametersPlain extends Sextuple<String, IntVector, IntVector, IntVector, Integer, IntVector> {

    public VotingParametersPlain(String U, IntVector bold_n, IntVector bold_k, IntVector bold_u, Integer w_v, IntVector bold_e_hat_v) {
        super(U, bold_n, bold_k, bold_u, w_v, bold_e_hat_v);
    }

    public String get_U() {
        return this.getFirst();
    }

    public IntVector get_bold_n() {
        return this.getSecond();
    }

    public IntVector get_bold_k() {
        return this.getThird();
    }

    public IntVector get_bold_u() {
        return this.getFourth();
    }

    public Integer get_w_v() {
        return this.getFifth();
    }

    public IntVector get_bold_hat_e_v() {
        return this.getSixth();
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean checkConsistency(String eventId) {
        return this.get_U().equals(eventId);
    }

}
