/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.model;

import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Nonuple;

import java.math.BigInteger;

/**
 * Model class for electorate data inheriting from {@link Nonuple}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class ElectorateData extends Nonuple<Vector<ByteArray>, Vector<ElectionCardData>, Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, Matrix<Point>> {

    public ElectorateData(Vector<ByteArray> bold_a, Vector<ElectionCardData> bold_d, Vector<BigInteger> bold_x, Vector<BigInteger> bold_y, Vector<BigInteger> bold_z, Vector<BigInteger> bold_x_hat, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, Matrix<Point> bold_P) {
        super(bold_a, bold_d, bold_x, bold_y, bold_z, bold_x_hat, bold_y_hat, bold_z_hat, bold_P);
    }

    public Vector<ByteArray> get_bold_a() {
        return this.getFirst();
    }

    public Vector<ElectionCardData> get_bold_d() {
        return this.getSecond();
    }

    public Vector<BigInteger> get_bold_x() {
        return this.getThird();
    }

    public Vector<BigInteger> get_bold_y() {
        return this.getFourth();
    }

    public Vector<BigInteger> get_bold_z() {
        return this.getFifth();
    }

    public Vector<BigInteger> get_bold_x_hat() {
        return this.getSixth();
    }

    public Vector<BigInteger> get_bold_y_hat() {
        return this.getSeventh();
    }

    public Vector<BigInteger> get_bold_z_hat() {
        return this.getEighth();
    }

    public Matrix<Point> get_bold_P() {
        return this.getNinth();
    }

}
