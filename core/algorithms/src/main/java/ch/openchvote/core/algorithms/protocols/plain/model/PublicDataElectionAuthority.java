/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.model;

import ch.openchvote.core.algorithms.protocols.common.model.Confirmation;
import ch.openchvote.core.algorithms.protocols.common.model.CredentialProof;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IndexedFamily;
import ch.openchvote.base.utilities.tuples.decuple.DuoDecuple;

import java.math.BigInteger;

/**
 * Model class for the election authority's public data inheriting from {@link DuoDecuple}, with specific constructor
 * and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class PublicDataElectionAuthority extends DuoDecuple<BigInteger, KeyPairProof, Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, CredentialProof, Vector<Encryption>, ShuffleProof, Vector<BigInteger>, DecryptionProof, IndexedFamily<Ballot>, IndexedFamily<Confirmation>> {

    public PublicDataElectionAuthority(BigInteger pk, KeyPairProof pi, Vector<BigInteger> bold_x_hat, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, CredentialProof pi_hat, Vector<Encryption> bold_e_tilde, ShuffleProof pi_tilde, Vector<BigInteger> bold_c, DecryptionProof pi_prime, IndexedFamily<Ballot> B, IndexedFamily<Confirmation> C) {
        super(pk, pi, bold_x_hat, bold_y_hat, bold_z_hat, pi_hat, bold_e_tilde, pi_tilde, bold_c, pi_prime, B, C);
    }

    public BigInteger get_pk() {
        return this.getFirst();
    }

    public KeyPairProof get_pi() {
        return this.getSecond();
    }

    public Vector<BigInteger> get_bold_x_hat() {
        return this.getThird();
    }

    public Vector<BigInteger> get_bold_y_hat() {
        return this.getFourth();
    }

    public Vector<BigInteger> get_bold_z_hat() {
        return this.getFifth();
    }

    public CredentialProof get_pi_hat() {
        return this.getSixth();
    }

    public Vector<Encryption> get_bold_e_tilde() {
        return this.getSeventh();
    }

    public ShuffleProof get_pi_tilde() {
        return this.getEighth();
    }

    public Vector<BigInteger> get_bold_c() {
        return this.getNinth();
    }

    public DecryptionProof get_pi_prime() {
        return this.getTenth();
    }

    public IndexedFamily<Ballot> get_B() {
        return this.getEleventh();
    }

    public IndexedFamily<Confirmation> get_C() {
        return this.getTwelfth();
    }

}
