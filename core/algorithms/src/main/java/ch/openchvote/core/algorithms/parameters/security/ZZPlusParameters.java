/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.parameters.security;

import ch.openchvote.base.utilities.algebra.ZZ;
import ch.openchvote.base.utilities.algebra.ZZPlus;

import java.math.BigInteger;

/**
 * Instances of this interface represent a list of parameters for specifying a multiplicative group of absolute values
 * modulo a safe prime {@code p=2q+1}. This list includes two group generators {@code g} and {@code h}.
 */
public interface ZZPlusParameters extends SecurityParameters {

    /**
     * Returns the modulus {@code p}.
     *
     * @return The modulus {@code p}
     */
    BigInteger get_p();

    /**
     * Returns the group order {@code q}.
     *
     * @return The group order {@code q}
     */
    BigInteger get_q();

    /**
     * Returns the first group generator {@code g}.
     *
     * @return The first group generator {@code g}
     */
    BigInteger get_g();

    /**
     * Returns the second group generator {@code h}.
     *
     * @return The second group generator {@code h}
     */
    BigInteger get_h();

    /**
     * Returns the corresponding multiplicative group of absolute values modulo {@code p}.
     *
     * @return The corresponding multiplicative group of absolute values modulo {@code p}
     */
    default ZZPlus get_ZZPlus_p() {
        return ZZPlus.of(this.get_p());
    }

    /**
     * Returns the corresponding additive group of integers modulo {@code q}.
     *
     * @return The corresponding additive group of integers modulo {@code q}
     */
    default ZZ get_ZZ_q() {
        return ZZ.of(this.get_q());
    }

}
