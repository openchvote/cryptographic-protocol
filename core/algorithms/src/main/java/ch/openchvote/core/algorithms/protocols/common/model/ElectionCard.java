/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.model;

import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Septuple;

/**
 * Model class for election cards inheriting from {@link Septuple}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class ElectionCard extends Septuple<Integer, String, String, Vector<String>, String, String, String> {

    public ElectionCard(Integer v, String X, String Y, Vector<String> bold_vc, String PC, String AC, String AD) {
        super(v, X, Y, bold_vc, PC, AC, AD);
    }

    public Integer get_v() {
        return this.getFirst();
    }

    public String get_X() {
        return this.getSecond();
    }

    public String get_Y() {
        return this.getThird();
    }

    public Vector<String> get_bold_vc() {
        return this.getFourth();
    }

    public String get_PC() {
        return this.getFifth();
    }

    public String get_AC() {
        return this.getSixth();
    }

    public String get_AD() {
        return this.getSeventh();
    }

}
