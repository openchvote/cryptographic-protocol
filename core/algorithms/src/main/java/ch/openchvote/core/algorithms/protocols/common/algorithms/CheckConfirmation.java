/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Confirmation;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.CheckConfirmationProof;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.35 from CHVote Protocol Specification
 */
public final class CheckConfirmation extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Boolean> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination", "BooleanMethodIsAlwaysInverted"})
    static public //
    <SP extends GGParameters & NIZKPParameters>
    boolean
    run(int v, Confirmation gamma, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var ZZ_p_hat = securityParameters.get_ZZ_p_hat();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(gamma, bold_y_hat, bold_z_hat);
        var N_E = bold_y_hat.getLength();
        var y_hat = gamma.get_y_hat();
        var z_hat = gamma.get_z_hat();
        var pi = gamma.get_pi();
        Precondition.check(IntSet.range(1, N_E).contains(v));
        Precondition.check(GG_q_hat.contains(y_hat));
        Precondition.check(GG_q_hat.contains(z_hat));
        Precondition.check(Set.Pair(ZZ_twoToTheTau, Set.Pair(ZZ_q_hat, ZZ_q_hat)).contains(pi));
        // for improved efficiency, we use ZZ_p_hat to perform inexact group membership tests for the values of bold_y_hat and bold_z_hat
        Precondition.check(Set.Vector(ZZ_p_hat, N_E).contains(bold_y_hat) && GG_q_hat.contains(bold_y_hat.getValue(v)));
        Precondition.check(Set.Vector(ZZ_p_hat, N_E).contains(bold_z_hat) && GG_q_hat.contains(bold_z_hat.getValue(v)));

        // ALGORITHM
        var y_hat_v = bold_y_hat.getValue(v);
        var z_hat_v = bold_z_hat.getValue(v);
        if (y_hat.equals(y_hat_v) && z_hat.equals(z_hat_v)) {
            return CheckConfirmationProof.run(pi, y_hat, z_hat, securityParameters);
        }
        return false;
    }

}
