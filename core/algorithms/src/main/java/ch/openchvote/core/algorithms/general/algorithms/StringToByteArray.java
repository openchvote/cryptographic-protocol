/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.set.Set;

import java.nio.charset.StandardCharsets;

/**
 * Implementation of Algorithm 4.6 from CHVote Protocol Specification
 */
public final class StringToByteArray extends Algorithm<ByteArray> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    ByteArray
    run(String S) {

        // PRECONDITIONS
        Precondition.checkNotNull(S);
        Precondition.check(Set.UCS_star.contains(S));

        // ALGORITHM
        var bytes = S.getBytes(StandardCharsets.UTF_8);
        return ByteArray.of(bytes);
    }

}