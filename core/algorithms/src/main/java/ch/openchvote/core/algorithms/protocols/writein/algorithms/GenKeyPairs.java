/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.KeyPairs;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER_OR_EQUAL;

/**
 * Implementation of Algorithm 9.3 from CHVote Protocol Specification
 */
public final class GenKeyPairs extends Algorithm<KeyPairs> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<KeyPairs> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public //
    <SP extends ZZPlusParameters>
    KeyPairs
    run(IntVector bold_k, IntVector bold_u, IntMatrix bold_E, IntVector bold_z, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_k, bold_u, bold_E, bold_z);
        var t = bold_u.getLength();
        var u = bold_E.getWidth();
        var N_E = bold_E.getHeight();
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntVector(IntSet.range(1, u), t).contains(bold_u));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, u).contains(bold_E));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_z));

        // CONSTRAINTS
        Precondition.check(bold_u.allMatch(SMALLER_OR_EQUAL));

        // ALGORITHM
        var z_max = bold_E.select(bold_u).multiply(bold_z.times(bold_k)).maxOrZero();
        var builder_bold_sk_prime = new Vector.Builder<BigInteger>(z_max);
        var builder_bold_pk_prime = new Vector.Builder<BigInteger>(z_max);
        for (int i : IntSet.range(1, z_max)) {
            var sk_prime_i = GenRandomInteger.run(q);
            var pk_prime_i = ZZPlus_p.pow(g, sk_prime_i);
            builder_bold_sk_prime.set(i, sk_prime_i);
            builder_bold_pk_prime.set(i, pk_prime_i);
        }
        var bold_sk = builder_bold_sk_prime.build();
        var bold_pk = builder_bold_pk_prime.build();
        return new KeyPairs(bold_sk, bold_pk);
    }

}
