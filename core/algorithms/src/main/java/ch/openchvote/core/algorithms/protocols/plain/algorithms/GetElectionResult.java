/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetPrimes;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.plain.model.ElectionResultPlain;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Math;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.53 from CHVote Protocol Specification
 */
public final class GetElectionResult extends Algorithm<ElectionResultPlain> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<ElectionResultPlain> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters>
    ElectionResultPlain
    run(Vector<BigInteger> bold_m, IntVector bold_n, IntVector bold_w, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_m, bold_n, bold_w);
        var N = bold_m.getLength();
        var t = bold_n.getLength();
        var N_E = bold_w.getLength();
        Precondition.check(Set.Vector(ZZPlus_p, N).contains(bold_m));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, N_E).contains(bold_w));

        // ALGORITHM
        var n = bold_n.sum();
        var w = bold_w.maxOrZero();
        var bold_p = GetPrimes.run(n + w, securityParameters);
        var builder_bold_V = new IntMatrix.Builder(N, n);
        var builder_bold_W = new IntMatrix.Builder(N, w);
        for (int i : IntSet.range(1, N)) {
            var m_i = bold_m.getValue(i);
            for (int j : IntSet.range(1, n)) {
                if (Math.divides(bold_p.getValue(j), m_i)) {
                    builder_bold_V.set(i, j, 1);
                } else {
                    builder_bold_V.set(i, j, 0);
                }
            }
            for (int c : IntSet.range(1, w)) {
                if (Math.divides(bold_p.getValue(n + c), m_i)) {
                    builder_bold_W.set(i, c, 1);
                } else {
                    builder_bold_W.set(i, c, 0);
                }
            }
        }
        var bold_V = builder_bold_V.build();
        var bold_W = builder_bold_W.build();
        return new ElectionResultPlain(bold_V, bold_W);
    }

}
