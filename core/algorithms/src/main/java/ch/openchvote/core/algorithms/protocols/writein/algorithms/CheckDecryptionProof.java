/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.AugmentedEncryption;
import ch.openchvote.core.algorithms.protocols.writein.model.DecryptionProof;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tuples.Quintuple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.24 from CHVote Protocol Specification
 */
public final class CheckDecryptionProof extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Boolean> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "BooleanMethodIsAlwaysInverted"})
    static public //
    <SP extends ZZPlusParameters & NIZKPParameters>
    boolean
    run(DecryptionProof pi, BigInteger pk, Vector<BigInteger> bold_pk_prime, Vector<AugmentedEncryption> bold_e_bar, Vector<BigInteger> bold_c, Matrix<BigInteger> bold_D, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var g = securityParameters.get_g();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(pi, pk, bold_pk_prime, bold_e_bar, bold_c, bold_D);
        var z = bold_pk_prime.getLength();
        var N = bold_e_bar.getLength();
        Precondition.check(Set.Pair(ZZ_twoToTheTau, Set.Vector(ZZ_q, z + 1)).contains(pi));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.Vector(ZZPlus_p, z).contains(bold_pk_prime));
        Precondition.check(Set.Vector(Set.Quadruple(ZZPlus_p, ZZPlus_p, Set.Vector(ZZPlus_p, z), ZZPlus_p), N).contains(bold_e_bar));
        Precondition.check(Set.Vector(ZZPlus_p, N).contains(bold_c));
        Precondition.check(Set.Matrix(ZZPlus_p, N, z).contains(bold_D));

        // PREPARATION
        var c = pi.get_c();
        var bold_s = pi.get_bold_s();
        var builder_bold_T = new Matrix.Builder.IndicesFromZero<BigInteger>(N, z);
        // PERFORMANCE OPTIMIZATIONS (precompute some modular exponentiations)
        var builder_bold_c_power_c = new Vector.Builder<BigInteger>(N);
        var builder_bold_pk_prime_power_c = new Vector.Builder<BigInteger>(z);
        builder_bold_c_power_c.addAll(bold_c.map(c_i -> ZZPlus_p.pow(c_i, c)));
        builder_bold_pk_prime_power_c.addAll(bold_pk_prime.map(pk_prime_j -> ZZPlus_p.pow(pk_prime_j, c)));
        var bold_c_power_c = builder_bold_c_power_c.build();
        var bold_pk_prime_power_c = builder_bold_pk_prime_power_c.build();

        // ALGORITHM
        for (int j : IntSet.range(0, z)) {
            var s_j = bold_s.getValue(j);
            var g_power_s_j = ZZPlus_p.pow(g, s_j);
            for (int i : IntSet.range(0, N)) {
                BigInteger t_ij;
                if (i == 0 && j == 0) {
                    t_ij = ZZPlus_p.multiply(ZZPlus_p.pow(pk, c), g_power_s_j);
                } else if (i == 0) {
                    var pk_prime_j_power_c = bold_pk_prime_power_c.getValue(j);
                    t_ij = ZZPlus_p.multiply(pk_prime_j_power_c, g_power_s_j);
                } else {
                    var e_bar_i = bold_e_bar.getValue(i);
                    if (j == 0) {
                        var b_i = e_bar_i.get_b();
                        var c_i_power_c = bold_c_power_c.getValue(i);
                        t_ij = ZZPlus_p.multiply(c_i_power_c, ZZPlus_p.pow(b_i, s_j));
                    } else {
                        var b_prime_i = e_bar_i.get_b_prime();
                        var d_ij = bold_D.getValue(i, j);
                        t_ij = ZZPlus_p.multiply(ZZPlus_p.pow(d_ij, c), ZZPlus_p.pow(b_prime_i, s_j));
                    }
                }
                builder_bold_T.set(i, j, t_ij);
            }
        }
        var bold_T = builder_bold_T.build();
        var y = new Quintuple<>(pk, bold_pk_prime, bold_e_bar, bold_c, bold_D);
        var c_prime = GetChallenge.run(y, bold_T, securityParameters);
        return c.equals(c_prime);
    }

}
