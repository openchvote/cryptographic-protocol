/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.model;

import ch.openchvote.core.algorithms.protocols.common.model.Confirmation;
import ch.openchvote.core.algorithms.protocols.common.model.CredentialProof;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IndexedFamily;
import ch.openchvote.base.utilities.tuples.decuple.QuattuorDecuple;

import java.math.BigInteger;

/**
 * Model class for the election authority's public data inheriting from {@link QuattuorDecuple}, with specific
 * constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class PublicDataElectionAuthority extends QuattuorDecuple<BigInteger, Vector<BigInteger>, KeyPairProof, Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, CredentialProof, Vector<AugmentedEncryption>, ShuffleProof, Vector<BigInteger>, Matrix<BigInteger>, DecryptionProof, IndexedFamily<Ballot>, IndexedFamily<Confirmation>> {

    public PublicDataElectionAuthority(BigInteger pk, Vector<BigInteger> bold_pk_prime, KeyPairProof pi, Vector<BigInteger> bold_x_hat, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, CredentialProof pi_hat, Vector<AugmentedEncryption> bold_e_tilde, ShuffleProof pi_tilde, Vector<BigInteger> bold_c, Matrix<BigInteger> bold_D, DecryptionProof pi_prime, IndexedFamily<Ballot> B, IndexedFamily<Confirmation> C) {
        super(pk, bold_pk_prime, pi, bold_x_hat, bold_y_hat, bold_z_hat, pi_hat, bold_e_tilde, pi_tilde, bold_c, bold_D, pi_prime, B, C);
    }

    public BigInteger get_pk() {
        return this.getFirst();
    }

    public Vector<BigInteger> get_bold_pk_prime() {
        return this.getSecond();
    }

    public KeyPairProof get_pi() {
        return this.getThird();
    }

    public Vector<BigInteger> get_bold_x_hat() {
        return this.getFourth();
    }

    public Vector<BigInteger> get_bold_y_hat() {
        return this.getFifth();
    }

    public Vector<BigInteger> get_bold_z_hat() {
        return this.getSixth();
    }

    public CredentialProof get_pi_hat() {
        return this.getSeventh();
    }

    public Vector<AugmentedEncryption> get_bold_e_tilde() {
        return this.getEighth();
    }

    public ShuffleProof get_pi_tilde() {
        return this.getNinth();
    }

    public Vector<BigInteger> get_bold_c() {
        return this.getTenth();
    }

    public Matrix<BigInteger> get_bold_D() {
        return this.getEleventh();
    }

    public DecryptionProof get_pi_prime() {
        return this.getTwelfth();
    }

    public IndexedFamily<Ballot> get_B() {
        return this.getThirteenth();
    }

    public IndexedFamily<Confirmation> get_C() {
        return this.getFourteenth();
    }

}
