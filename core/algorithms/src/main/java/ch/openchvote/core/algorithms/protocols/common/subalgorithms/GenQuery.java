/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Query;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tools.Parallel;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.23 from CHVote Protocol Specification
 */
public final class GenQuery extends Algorithm<Pair<Vector<Query>, Vector<BigInteger>>> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters>
    Pair<Vector<Query>, Vector<BigInteger>>
    run(Vector<BigInteger> bold_m, BigInteger pk, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();

        // PREPARATION
        var k = bold_m.getLength();
        var builder_bold_a = new Vector.Builder<Query>(k);
        var builder_bold_r = new Vector.Builder<BigInteger>(k);

        // ALGORITHM
        Parallel.forLoop(1, k, j -> {
            var m_j = bold_m.getValue(j);
            var r_j = GenRandomInteger.run(q);
            var a_j_1 = ZZPlus_p.multiply(m_j, ZZPlus_p.pow(pk, r_j));
            var a_j_2 = ZZPlus_p.pow(g, r_j);
            var a_j = new Query(a_j_1, a_j_2);
            builder_bold_a.set(j, a_j);
            builder_bold_r.set(j, r_j);
        });
        var bold_a = builder_bold_a.build();
        var bold_r = builder_bold_r.build();
        return new Pair<>(bold_a, bold_r);
    }

}
