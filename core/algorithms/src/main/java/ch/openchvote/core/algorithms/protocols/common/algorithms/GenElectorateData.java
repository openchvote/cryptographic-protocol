/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.RandomBytes;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.parameters.usability.CredentialParameters;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionCardData;
import ch.openchvote.core.algorithms.protocols.common.model.ElectorateData;
import ch.openchvote.core.algorithms.protocols.common.model.Point;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GenCredentials;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GenPoints;
import ch.openchvote.base.utilities.sequence.*;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Parallel;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;
import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER_OR_EQUAL;

/**
 * Implementation of Algorithm 8.10 from CHVote Protocol Specification
 */
public final class GenElectorateData extends Algorithm<ElectorateData> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<ElectorateData> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public //
    <SP extends GGParameters, UP extends CodeParameters & CredentialParameters>
    ElectorateData
    run(IntVector bold_n, IntVector bold_k, IntVector bold_u, IntMatrix bold_E, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);

        // USABILITY PARAMETERS
        Precondition.checkNotNull(usabilityParameters);
        var L_PA = usabilityParameters.get_L_PA();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_n, bold_k, bold_u, bold_E);
        var n = bold_n.sum();
        var t = bold_n.getLength();
        var u = bold_E.getWidth();
        var N_E = bold_E.getHeight();
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntVector(IntSet.range(1, u), t).contains(bold_u));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, u).contains(bold_E));

        // CONSTRAINTS
        Precondition.check(bold_u.allMatch(SMALLER_OR_EQUAL));
        Precondition.check(IntVector.allMatch(bold_k, bold_n, SMALLER));

        // PREPARATION
        var builder_bold_a = new Vector.Builder<ByteArray>(N_E);
        var builder_bold_d = new Vector.Builder<ElectionCardData>(N_E);
        var builder_bold_x = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_y = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_z = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_x_hat = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_y_hat = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_z_hat = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_P = new Matrix.RowBuilder<Point>(N_E, n);

        // ALGORITHM
        Parallel.forLoop(1, N_E, v -> {
            var bold_e_hat_v = bold_E.getRow(v).expand(bold_u);
            var pair = GenPoints.run(bold_n, bold_k, bold_e_hat_v, securityParameters);
            var bold_p_v = pair.getFirst();
            var z_v = pair.getSecond();
            var quintuple = GenCredentials.run(z_v, securityParameters);
            var x_v = quintuple.getFirst();
            var y_v = quintuple.getSecond();
            var x_hat_v = quintuple.getThird();
            var y_hat_v = quintuple.getFourth();
            var z_hat_v = quintuple.getFifth();
            var A_v = RandomBytes.run(L_PA);
            var d_v = new ElectionCardData(x_v, y_v, A_v, bold_p_v);
            builder_bold_P.setRow(v, bold_p_v);
            builder_bold_x.set(v, x_v);
            builder_bold_y.set(v, y_v);
            builder_bold_z.set(v, z_v);
            builder_bold_x_hat.set(v, x_hat_v);
            builder_bold_y_hat.set(v, y_hat_v);
            builder_bold_z_hat.set(v, z_hat_v);
            builder_bold_a.set(v, A_v);
            builder_bold_d.set(v, d_v);
        });

        var bold_a = builder_bold_a.build();
        var bold_d = builder_bold_d.build();
        var bold_x = builder_bold_x.build();
        var bold_y = builder_bold_y.build();
        var bold_z = builder_bold_z.build();
        var bold_x_hat = builder_bold_x_hat.build();
        var bold_y_hat = builder_bold_y_hat.build();
        var bold_z_hat = builder_bold_z_hat.build();
        var bold_P = builder_bold_P.build();
        return new ElectorateData(bold_a, bold_d, bold_x, bold_y, bold_z, bold_x_hat, bold_y_hat, bold_z_hat, bold_P);
    }

}
