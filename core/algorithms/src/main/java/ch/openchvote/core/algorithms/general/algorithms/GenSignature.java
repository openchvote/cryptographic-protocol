/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.model.Signature;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.base.utilities.tuples.Pair;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.57 from CHVote Protocol Specification
 */
public final class GenSignature extends Algorithm<Signature> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends GGParameters & NIZKPParameters>
    Signature
    run(BigInteger sk, Object m, Object aux, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var q_hat = securityParameters.get_q_hat();
        var g_hat = securityParameters.get_g_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();

        // PRECONDITIONS
        Precondition.checkNotNull(sk, m);
        Precondition.check(ZZ_q_hat.contains(sk));

        // ALGORITHM
        var pk = GG_q_hat.pow(g_hat, sk); // cache pk over multiple calls for improving efficiency
        var omega = GenRandomInteger.run(q_hat);
        var t = GG_q_hat.pow(g_hat, omega);
        var y = (aux == null) ? new Pair<>(pk, m) : new Triple<>(pk, m, aux);
        var c = GetChallenge.run(y, t, securityParameters);
        var s = ZZ_q_hat.subtract(omega, ZZ_q_hat.multiply(c, sk));
        var sigma = new Signature(c, s);
        return sigma;
    }

}
