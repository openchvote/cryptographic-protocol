/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.tools.Math;

import java.math.BigInteger;
import java.util.SortedSet;

/**
 * Implementation of Algorithm 10.3 from CHVote Protocol Specification
 */
public final class IsNotPrime extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination"})
    static public //
    boolean
    run(BigInteger x, SortedSet<BigInteger> P) {

        // ALGORITHM
        for (BigInteger p : P) {
            if (p.compareTo(x) < 0 && Math.divides(p, x)) {
                return true;
            }
        }
        return false;
    }

}
