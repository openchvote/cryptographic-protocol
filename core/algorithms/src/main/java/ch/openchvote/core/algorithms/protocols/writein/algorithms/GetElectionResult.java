/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetPrimes;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.parameters.usability.WriteInsParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.ElectionResultWriteIn;
import ch.openchvote.core.algorithms.protocols.writein.subalgorithms.GetWriteIns;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Math;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;

/**
 * Implementation of Algorithm 9.27 from CHVote Protocol Specification
 */
public final class GetElectionResult extends Algorithm<ElectionResultWriteIn> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<ElectionResultWriteIn> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters, UP extends CodeParameters & WriteInsParameters>
    ElectionResultWriteIn
    run(Vector<BigInteger> bold_m, IntVector bold_n, IntVector bold_w, Matrix<BigInteger> bold_M, IntVector bold_k, IntVector bold_z, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_m, bold_n, bold_w, bold_M, bold_k, bold_z);
        var N = bold_m.getLength();
        var t = bold_n.getLength();
        var N_E = bold_w.getLength();
        var z = bold_M.getWidth();
        Precondition.check(Set.Vector(ZZPlus_p, N).contains(bold_m));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, N_E).contains(bold_w));
        Precondition.check(Set.Matrix(ZZPlus_p, N, z).contains(bold_M));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_z));

        // CONSTRAINTS
        Precondition.check(IntVector.allMatch(bold_k, bold_n, SMALLER));

        // ALGORITHM
        var n = bold_n.sum();
        var w = bold_w.maxOrZero();
        var bold_p = GetPrimes.run(n + w, securityParameters);
        var builder_bold_V = new IntMatrix.Builder(N, n);
        var builder_bold_W = new IntMatrix.Builder(N, w);
        for (int i : IntSet.range(1, N)) {
            for (int k : IntSet.range(1, n)) {
                if (Math.divides(bold_p.getValue(k), bold_m.getValue(i))) {
                    builder_bold_V.set(i, k, 1);
                } else {
                    builder_bold_V.set(i, k, 0);
                }
            }
            for (int j : IntSet.range(1, w)) {
                if (Math.divides(bold_p.getValue(n + j), bold_m.getValue(i))) {
                    builder_bold_W.set(i, j, 1);
                } else {
                    builder_bold_W.set(i, j, 0);
                }
            }
        }
        var bold_V = builder_bold_V.build();
        var bold_W = builder_bold_W.build();
        var pair = GetWriteIns.run(bold_M, bold_V, bold_n, bold_k, bold_z, securityParameters, usabilityParameters);
        var bold_S = pair.getFirst();
        var bold_T = pair.getSecond();
        return new ElectionResultWriteIn(bold_V, bold_W, bold_S, bold_T);
    }

}
