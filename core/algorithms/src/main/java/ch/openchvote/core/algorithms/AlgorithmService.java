/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms;

import ch.openchvote.core.algorithms.parameters.security.SecurityParameters;
import ch.openchvote.core.algorithms.parameters.usability.UsabilityParameters;

import java.util.ServiceLoader;

/**
 * The purpose of an algorithm service is to direct the execution of an algorithm to a specific implementation in a
 * given programming language. For executing an algorithm, one of the four {@link AlgorithmService#run}-methods needs to
 * be called, depending on whether security and usability parameters are present or not. Service implementations of this
 * interface are free to use any execution environment, as long as they implement all four abstract methods
 * appropriately. In all four cases, the algorithm arguments are given by an object array. In addition to this basic
 * functionality, the class provides static helper methods for loading corresponding services.
 */
public interface AlgorithmService {

    /**
     * The default algorithm service, which is an instance of {@link JavaAlgorithmService}.
     */
    AlgorithmService DEFAULT = new JavaAlgorithmService();

    /**
     * A selection of potentially supported programming languages.
     */
    @SuppressWarnings({"MissingJavadoc", "unused"})
    enum Language {
        C,
        JAVA,
        JAVASCRIPT,
        KOTLIN,
        PYTHON,
        RUST
    }

    /**
     * Executes the specified algorithm without expecting security or usability parameters. The algorithm arguments are
     * given by an array of objects. The type of the value returned corresponds to the generic type defined by the
     * algorithm.
     *
     * @param algorithm The given algorithm
     * @param arguments The given algorithm arguments
     * @param <R>       The generic type of the return value
     * @return The return value obtained from executing the algorithm
     * @throws Algorithm.Exception if the algorithm does not provide the expected method for executing the algorithm
     */
    <R> R run(Class<? extends Algorithm<R>> algorithm, Object... arguments) throws Algorithm.Exception;

    /**
     * Executes the specified algorithm for the given security parameters. The algorithm arguments are given by an array
     * of objects. The type of the value returned corresponds to the generic type defined by the algorithm class.
     *
     * @param algorithm          The given algorithm
     * @param securityParameters The given security parameters
     * @param arguments          The given algorithm arguments
     * @param <R>                The generic type of the return value
     * @return The return value obtained from executing the algorithm
     * @throws Algorithm.Exception if the algorithm does not provide the expected method for executing the algorithm
     */
    <R> R run(Class<? extends Algorithm<R>> algorithm, SecurityParameters securityParameters, Object... arguments) throws Algorithm.Exception;

    /**
     * Executes the specified algorithm for the given usability parameters. The algorithm arguments are given by an
     * array of objects. The type of the value returned corresponds to the generic type defined by the algorithm class.
     *
     * @param algorithm           The given algorithm
     * @param usabilityParameters The given usability parameters
     * @param arguments           The given algorithm arguments
     * @param <R>                 The generic type of the return value
     * @return The return value obtained from executing the algorithm
     * @throws Algorithm.Exception if the algorithm does not provide the expected method for executing the algorithm
     */
    <R> R run(Class<? extends Algorithm<R>> algorithm, UsabilityParameters usabilityParameters, Object... arguments) throws Algorithm.Exception;

    /**
     * Executes the specified algorithm for the given security and usability parameters. The algorithm arguments are
     * given by an array of objects. The type of the value returned corresponds to the generic type defined by the
     * algorithm class.
     *
     * @param algorithm           The given algorithm
     * @param securityParameters  The given security parameters
     * @param usabilityParameters The given usability parameters
     * @param arguments           The given algorithm arguments
     * @param <R>                 The generic type of the return value
     * @return The return value obtained from executing the algorithm
     * @throws Algorithm.Exception if the algorithm does not provide the expected method for executing the algorithm
     */
    <R> R run(Class<? extends Algorithm<R>> algorithm, SecurityParameters securityParameters, UsabilityParameters usabilityParameters, Object... arguments) throws Algorithm.Exception;

    /**
     * Return the service's programming language.
     *
     * @return The service's programming language
     */
    @SuppressWarnings("SameReturnValue")
    Language getLanguage();

    /**
     * Returns an instance of {@link JavaAlgorithmService}. This is the default algorithm service.
     *
     * @return An instance of {@link JavaAlgorithmService}
     */
    @SuppressWarnings("SameReturnValue")
    static AlgorithmService load() {
        return DEFAULT;
    }

    /**
     * Loads and returns an algorithm service that matches the specified programming language. If no service
     * implementation matching the specified programming language is available, the default algorithm service is
     * returned.
     *
     * @param language The given programming language
     * @return The loaded algorithm service
     * @throws Algorithm.Exception if no service implementation matching the specified programming language is
     *                             available
     */
    static AlgorithmService load(Language language) throws Algorithm.Exception {
        var serviceLoader = ServiceLoader.load(AlgorithmService.class);
        return serviceLoader.stream()
                .map(ServiceLoader.Provider::get)
                .filter(service -> service.getLanguage() == language)
                .findFirst()
                .orElse(DEFAULT);
    }

}
