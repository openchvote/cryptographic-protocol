/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.general.algorithms.GetPrimes;
import ch.openchvote.core.algorithms.general.algorithms.IntegerToByteArray;
import ch.openchvote.core.algorithms.general.algorithms.RecHash;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Finalization;
import ch.openchvote.core.algorithms.protocols.common.model.Point;
import ch.openchvote.core.algorithms.protocols.common.model.Query;
import ch.openchvote.core.algorithms.protocols.common.model.Response;
import ch.openchvote.base.utilities.sequence.*;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Math;
import ch.openchvote.base.utilities.tools.Parallel;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;
import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER_OR_EQUAL;

/**
 * Implementation of Algorithm 8.27 from CHVote Protocol Specification
 */
public final class GenResponse extends Algorithm<Pair<Response, Finalization>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Pair<Response, Finalization>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination", "unused"})
    static public //
    <SP extends ZZPlusParameters & GGParameters & HashParameters>
    Pair<Response, Finalization>
    run(Integer v, Vector<Query> bold_a, BigInteger pk, IntVector bold_n, IntVector bold_k, IntVector bold_u, IntMatrix bold_E, Matrix<Point> bold_P, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var L_M = securityParameters.get_L_M();
        var L = securityParameters.get_L();

        // PRECONDITIONS
        Precondition.checkNotNull(v, bold_a, pk, bold_n, bold_k, bold_u, bold_E, bold_P);
        var k = bold_a.getLength();
        var t = bold_n.getLength();
        var u = bold_E.getWidth();
        var N_E = bold_E.getHeight();
        var n = bold_P.getWidth();
        Precondition.check(IntSet.range(1, N_E).contains(v));
        Precondition.check(Set.Vector(Set.Pair(ZZPlus_p, ZZPlus_p), k).contains(bold_a));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntVector(IntSet.range(1, u), t).contains(bold_u));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, u).contains(bold_E));
        Precondition.check(Set.Matrix(Set.Pair(ZZ_q_hat, ZZ_q_hat), N_E, n).contains(bold_P));

        // CONSTRAINTS
        Precondition.check(bold_u.allMatch(SMALLER_OR_EQUAL));
        Precondition.check(n == bold_n.sum());
        Precondition.check(k == bold_E.getRow(v).expand(bold_u).multiply(bold_k));
        Precondition.check(IntVector.allMatch(bold_k, bold_n, SMALLER));

        // PREPARATION
        var builder_bold_beta = new Vector.Builder<BigInteger>(k);
        var builder_bold_b = new Vector.Builder<BigInteger>(k);
        var builder_bold_C = new Matrix.Builder<ByteArray>(n, k);

        // ALGORITHM
        var ell_M = Math.ceilDiv(L_M, L);
        var z_1 = GenRandomInteger.run(q);
        var z_2 = GenRandomInteger.run(q);
        Parallel.forLoop(1, k, j -> {
            var beta_j = GenRandomInteger.run(BigInteger.ONE, q);
            var a_j = bold_a.getValue(j);
            var b_j = ZZPlus_p.multiply(ZZPlus_p.pow(a_j.get_a_1(), z_1), ZZPlus_p.multiply(ZZPlus_p.pow(a_j.get_a_2(), z_2), beta_j));
            builder_bold_beta.set(j, beta_j);
            builder_bold_b.set(j, b_j);
        });
        builder_bold_C.fill(null);
        var bold_beta = builder_bold_beta.build();
        var bold_e_hat_v = bold_E.getRow(v).expand(bold_u);
        var bold_p = GetPrimes.run(n, securityParameters);
        var n_prime = 0;
        var k_prime = 0;
        for (int l : IntSet.range(1, t)) {
            var n_l = bold_n.getValue(l);
            var k_l = bold_k.getValue(l);
            if (bold_e_hat_v.getValue(l) == 1) {
                // parallel pre-computation of all p_prime_i values
                var builder_bold_p_prime = new Vector.Builder<BigInteger>(n_prime + 1, n_prime + n_l);
                Parallel.forLoop(n_prime + 1, n_prime + n_l, i -> {
                    var p_i = bold_p.getValue(i);
                    var p_prime_i = ZZPlus_p.pow(p_i, z_1);
                    builder_bold_p_prime.set(i, p_prime_i);
                });
                var bold_p_prime = builder_bold_p_prime.build();
                for (int i : IntSet.range(n_prime + 1, n_prime + n_l)) {
                    var p_prime_i = bold_p_prime.getValue(i);
                    var p_vi = bold_P.getValue(v, i);
                    var x_vi = p_vi.get_x();
                    var y_vi = p_vi.get_y();
                    var M_i = IntegerToByteArray.run(x_vi, L_M / 2).concatenate(IntegerToByteArray.run(y_vi, L_M / 2));
                    for (int j : IntSet.range(k_prime + 1, k_prime + k_l)) {
                        var beta_j = bold_beta.getValue(j);
                        var k_ij = ZZPlus_p.multiply(p_prime_i, beta_j);
                        var K_ij = ByteArray.EMPTY;
                        for (int c : IntSet.range(1, ell_M)) {
                            K_ij = K_ij.concatenate(RecHash.run(k_ij, c, securityParameters));
                        }
                        K_ij = K_ij.truncate(L_M);
                        var C_ij = M_i.xor(K_ij);
                        builder_bold_C.set(i, j, C_ij);
                    }
                }
                k_prime = k_prime + k_l;
            }
            n_prime = n_prime + n_l;
        }
        var bold_b = builder_bold_b.build();
        var bold_C = builder_bold_C.build();
        var d = ZZPlus_p.multiply(ZZPlus_p.pow(pk, z_1), ZZPlus_p.pow(g, z_2));
        var beta = new Response(bold_b, bold_C, d);
        var delta = new Finalization(z_1, z_2);
        return new Pair<>(beta, delta);
    }

}
