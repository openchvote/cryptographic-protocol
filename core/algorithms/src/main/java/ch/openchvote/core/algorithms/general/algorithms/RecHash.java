/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.base.utilities.crypto.HashAlgorithm;
import ch.openchvote.base.utilities.sequence.*;
import ch.openchvote.base.utilities.set.FiniteSet;
import ch.openchvote.base.utilities.tools.Hashable;
import ch.openchvote.base.utilities.tools.Streamable;
import ch.openchvote.base.utilities.tuples.Tuple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 4.14 from CHVote Protocol Specification
 */
public final class RecHash extends Algorithm<ByteArray> {

    // NOTE: This algorithm stores computed hash values for all {@link Hashable} objects to avoid recomputing the same
    // hash value multiple times. This is an important performance improvement.

    // prefix bytes for different domain types
    static private final ByteArray PREFIX_NULL = ByteArray.of(0x00);
    static private final ByteArray PREFIX_BYTE_ARRAY = ByteArray.of(0x01);
    static private final ByteArray PREFIX_INTEGER = ByteArray.of(0x02);
    static private final ByteArray PREFIX_STRING = ByteArray.of(0x03);
    static private final ByteArray PREFIX_TUPLE_VECTOR = ByteArray.of(0x04);
    static private final ByteArray PREFIX_MATRIX = ByteArray.of(0x05);
    static private final ByteArray PREFIX_FINITE_SET = ByteArray.of(0x06);

    @SuppressWarnings("MissingJavadoc")
    static public // public convenience method for zero inputs
    ByteArray
    run(HashParameters hashParameters) {
        Precondition.checkNotNull(hashParameters);
        return RecHash.run(new Object[]{}, hashParameters.getHashAlgorithm());
    }

    @SuppressWarnings("MissingJavadoc")
    static public // public convenience method for one input
    ByteArray
    run(Object value, HashParameters hashParameters) {
        Precondition.checkNotNull(hashParameters);
        return RecHash.run(new Object[]{value}, hashParameters.getHashAlgorithm());
    }

    @SuppressWarnings("MissingJavadoc")
    static public // public convenience method for two inputs
    ByteArray
    run(Object value1, Object value2, HashParameters hashParameters) {
        Precondition.checkNotNull(hashParameters);
        return RecHash.run(new Object[]{value1, value2}, hashParameters.getHashAlgorithm());
    }

    @SuppressWarnings("MissingJavadoc")
    static public // public convenience method for three inputs
    ByteArray
    run(Object value1, Object value2, Object value3, HashParameters hashParameters) {
        Precondition.checkNotNull(hashParameters);
        return RecHash.run(new Object[]{value1, value2, value3}, hashParameters.getHashAlgorithm());
    }

    @SuppressWarnings("MissingJavadoc")
    static public // public convenience method for four inputs
    ByteArray
    run(Object value1, Object value2, Object value3, Object value4, HashParameters hashParameters) {
        Precondition.checkNotNull(hashParameters);
        return RecHash.run(new Object[]{value1, value2, value3, value4}, hashParameters.getHashAlgorithm());
    }

    static private // private method for the general case of arbitrarily many inputs
    ByteArray
    run(Object[] values, HashAlgorithm hashAlgorithm) {
        Object value;
        if (values.length == 1) {
            value = values[0];
        } else {
            value = Vector.of(values);
        }
        return RecHash.run(value, hashAlgorithm);
    }

    @SuppressWarnings("IfCanBeSwitch")
    static private //
    ByteArray
    run(Object value, HashAlgorithm hashAlgorithm) {
        // ALGORITHM
        if (value == null) return hashAlgorithm.hash(PREFIX_NULL);
        if (value instanceof Boolean boole) return RecHash.run(boole ? BigInteger.ONE : BigInteger.ZERO, hashAlgorithm);
        if (value instanceof ByteArray byteArray) return hashAlgorithm.hash(PREFIX_BYTE_ARRAY.concatenate(byteArray));
        if (value instanceof Integer integer) return RecHash.run(BigInteger.valueOf(integer), hashAlgorithm);
        if (value instanceof BigInteger integer)
            return hashAlgorithm.hash(PREFIX_INTEGER.concatenate(IntegerToByteArray.run(integer)));
        if (value instanceof String string)
            return hashAlgorithm.hash(PREFIX_STRING.concatenate(StringToByteArray.run(string)));
        if (value instanceof Hashable hashable) {
            if (hashable.hasHashValue(hashAlgorithm)) {
                return hashable.getHashValue(hashAlgorithm);
            }
            ByteArray H = null;
            if (value instanceof Tuple tuple)
                H = RecHash.run(PREFIX_TUPLE_VECTOR, tuple, hashAlgorithm);
            if (value instanceof Vector<?> vector)
                H = RecHash.run(PREFIX_TUPLE_VECTOR, vector, hashAlgorithm);
            if (value instanceof IntVector vector)
                H = RecHash.run(PREFIX_TUPLE_VECTOR, vector, hashAlgorithm);
            if (value instanceof Matrix<?> matrix)
                H = RecHash.run(PREFIX_MATRIX, matrix.getRows(), hashAlgorithm);
            if (value instanceof IntMatrix matrix)
                H = RecHash.run(PREFIX_MATRIX, matrix.getRows(), hashAlgorithm);
            if (value instanceof FiniteSet<?, ?> finiteSet)
                H = RecHash.run(PREFIX_FINITE_SET, finiteSet, hashAlgorithm);
            if (H != null) {
                hashable.putHashValue(hashAlgorithm, H);
                return H;
            }
        }
        throw new Exception(Exception.Type.UNSUPPORTED_HASH_TYPE, RecHash.class);
    }

    // private helper method to hash a streamable collection of values
    static private ByteArray run(ByteArray prefix, Streamable<?> values, HashAlgorithm hashAlgorithm) {
        var vectorOfHashes = values.toStream().map(value -> RecHash.run(value, hashAlgorithm)).collect(Vector.getCollector());
        return hashAlgorithm.hash(prefix.concatenate(ByteArray.concatenate(vectorOfHashes)));
    }

}
