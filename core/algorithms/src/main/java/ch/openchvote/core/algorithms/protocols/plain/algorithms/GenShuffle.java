/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GenPermutation;
import ch.openchvote.core.algorithms.protocols.plain.subalgorithms.GenReEncryption;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Parallel;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.41 from CHVote Protocol Specification
 */
public final class GenShuffle extends Algorithm<Triple<Vector<Encryption>, Vector<BigInteger>, IntVector>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Triple<Vector<Encryption>, Vector<BigInteger>, IntVector>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public //
    <SP extends ZZPlusParameters>
    Triple<Vector<Encryption>, Vector<BigInteger>, IntVector>
    run(Vector<Encryption> bold_e, BigInteger pk, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_e, pk);
        var N = bold_e.getLength();
        Precondition.check(Set.Vector(Set.Pair(ZZPlus_p, ZZPlus_p), N).contains(bold_e));
        Precondition.check(ZZPlus_p.contains(pk));

        // PREPARATION
        var builder_bold_e_tilde = new Vector.Builder<Encryption>(N);
        var builder_bold_r_tilde = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        var psi = GenPermutation.run(N);
        Parallel.forLoop(1, N, i -> {
            var j_i = psi.getValue(i);
            var e_j_i = bold_e.getValue(j_i);
            var pair = GenReEncryption.run(e_j_i, pk, securityParameters);
            builder_bold_e_tilde.set(i, pair.getFirst());
            builder_bold_r_tilde.set(j_i, pair.getSecond());
        });
        var bold_e_tilde = builder_bold_e_tilde.build();
        var bold_r_tilde = builder_bold_r_tilde.build();
        return new Triple<>(bold_e_tilde, bold_r_tilde, psi);
    }

}
