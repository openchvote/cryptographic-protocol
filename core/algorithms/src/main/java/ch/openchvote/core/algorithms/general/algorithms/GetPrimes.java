/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.subalgorithms.IsPrime;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;

import java.math.BigInteger;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Implementation of Algorithms 8.1 and 10.2 from CHVote Protocol Specification
 */
public final class GetPrimes extends Algorithm<Vector<BigInteger>> {

    // implementation of Algorithm 8.1
    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters>
    Vector<BigInteger>
    run(int n, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var q = securityParameters.get_q();

        // PRECONDITIONS
        Precondition.check(IntSet.NN.contains(n));

        // PREPARATION
        var builder_bold_p = new Vector.Builder<BigInteger>(n);

        // ALGORITHM
        var x = BigInteger.ONE;
        for (int i : IntSet.range(1, n)) {
            do {
                if (x.equals(BigInteger.ONE) || x.equals(BigInteger.TWO)) {
                    x = x.add(BigInteger.ONE);
                } else {
                    x = x.add(BigInteger.TWO);
                }
                if (x.compareTo(q) > 0) {
                    throw new Exception(Exception.Type.INSUFFICIENT_GROUP_SIZE, GetPrimes.class);
                }
            } while (!(IsPrime.run(x.intValueExact())));
            builder_bold_p.set(i, x);
        }
        var bold_p = builder_bold_p.build();
        return bold_p;
    }

    // implementation of Algorithm 10.2
    @SuppressWarnings("MissingJavadoc")
    static public //
    SortedSet<BigInteger>
    run(int l, int u) {

        // PRECONDITIONS
        Precondition.check(IntSet.NN.contains(l));
        Precondition.check(u >= l);

        // ALGORITHM
        var P = new TreeSet<BigInteger>();
        var x = l;
        while (x <= u) {
            if (IsPrime.run(x)) {
                P.add(BigInteger.valueOf(x));
            }
            if (x % 2 == 0) {
                x = x + 1;
            } else {
                x = x + 2;
            }
        }
        return P;
    }
}