/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.model;

import ch.openchvote.base.utilities.tuples.Sextuple;

import java.math.BigInteger;

/**
 * Model class for ciphertexts inheriting from {@link Sextuple}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class GroupParameters extends Sextuple<BigInteger, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger> {

    public GroupParameters(BigInteger p, BigInteger q, BigInteger p_hat, BigInteger q_hat, BigInteger k_hat, BigInteger g_hat) {
        super(p, q, p_hat, q_hat, k_hat, g_hat);
    }

    public BigInteger get_p() {
        return this.getFirst();
    }

    public BigInteger get_q() {
        return this.getSecond();
    }

    public BigInteger get_p_hat() {
        return this.getThird();
    }

    public BigInteger get_q_hat() {
        return this.getFourth();
    }

    public BigInteger get_k_hat() {
        return this.getFifth();
    }

    public BigInteger get_g_hat() {
        return this.getSixth();
    }

}
