/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.parameters.security;

import ch.openchvote.base.utilities.algebra.GG;
import ch.openchvote.base.utilities.algebra.ZZ;
import ch.openchvote.base.utilities.tools.Math;

import java.math.BigInteger;

/**
 * Instances of this interface represent a list of parameters for specifying a prime-order subgroup of integers modulo a
 * prime. This list includes a group generator {@code g_hat}.
 */
public interface GGParameters extends SecurityParameters {

    /**
     * Returns the modulus {@code p_hat}.
     *
     * @return The modulus {@code p_hat}
     */
    BigInteger get_p_hat();

    /**
     * Returns the group order {@code q_hat}.
     *
     * @return The group order {@code q_hat}
     */
    BigInteger get_q_hat();

    /**
     * Returns the group generator {@code g_hat}.
     *
     * @return The group generator {@code g_hat}
     */
    BigInteger get_g_hat();

    /**
     * Returns the corresponding multiplicative subgroup of integers modulo {@code p_hat}.
     *
     * @return The corresponding multiplicative subgroup of integers modulo {@code p_hat}
     */
    default GG get_GG_q_hat() {
        return GG.of(this.get_p_hat(), this.get_q_hat());
    }

    /**
     * Returns the corresponding additive group of integers modulo {@code q_hat}.
     *
     * @return The corresponding additive group of integers modulo {@code q_hat}
     */
    default ZZ get_ZZ_q_hat() {
        return ZZ.of(this.get_q_hat());
    }

    /**
     * Returns the corresponding additive group of integers modulo {@code p_hat}.
     *
     * @return The corresponding additive group of integers modulo {@code p_hat}
     */
    default ZZ get_ZZ_p_hat() {
        return ZZ.of(this.get_p_hat());
    }

    /**
     * Return the parameter {@code L_M}, which is twice the length of {@code g_hat} in bytes.
     *
     * @return The parameter {@code L_M}
     */
    default int get_L_M() {
        return 2 * Math.byteLength(this.get_q_hat());
    }

}
