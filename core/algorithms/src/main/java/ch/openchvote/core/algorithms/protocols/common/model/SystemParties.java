/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.model;

import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Triple;

/**
 * Model class for system parties inheriting from {@link Triple}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class SystemParties extends Triple<String, String, Vector<String>> {

    public SystemParties(String AD, String PA, Vector<String> bold_ea) {
        super(AD, PA, bold_ea);
    }

    // administrator id
    public String get_AD() {
        return this.getFirst();
    }

    // printing authority id
    public String get_PA() {
        return this.getSecond();
    }

    // election authority ids
    public Vector<String> get_bold_ea() {
        return this.getThird();
    }

    // number of election authorities
    public int get_s() {
        return this.get_bold_ea().getLength();
    }

    // index of a specific election authority
    public int getIndexOf(String EA) {
        var bold_ea = this.get_bold_ea();
        for (int index : IntSet.range(1, this.get_s())) {
            if (bold_ea.getValue(index).equals(EA)) {
                return index;
            }
        }
        return -1;
    }

}
