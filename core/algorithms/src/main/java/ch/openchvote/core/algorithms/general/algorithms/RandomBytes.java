/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.crypto.Entropy;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.set.IntSet;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

/**
 * This class implements a static method {@link RandomBytes#run(int)}} for generating random byte arrays of a given
 * length. Internally, the class uses an instance of the standard Java class {@link SecureRandom}, which is instantiated
 * with the {@code SHA1PRNG} pseudorandom number generator (PRNG). Initially, the PRNG is seeded using the predefined
 * entropy gathering mechanism of the available {@code SHA1PRNG} implementation. Additional entropy (so-called CPU
 * jitter entropy) is then gathered to complement the initial seed and for reseeding the PRNG. The reseeding is invoked
 * at random intervals.
 */
public final class RandomBytes extends Algorithm<ByteArray> {

    static private final SecureRandom RANDOM_GENERATOR;

    static private final int SEEDING_BITS_OF_ENTROPY = 256; // see https://eprint.iacr.org/2018/349.pdf, page 5
    static private final int RESEEDING_BITS_OF_ENTROPY = 256;
    static private final int RESEEDING_FREQUENCY = 100_000;

    static {
        try {
            RANDOM_GENERATOR = SecureRandom.getInstance("SHA1PRNG", "SUN");
            RANDOM_GENERATOR.nextBytes(new byte[1]); // provoke initial seeding
            RANDOM_GENERATOR.setSeed(Entropy.getEntropy(SEEDING_BITS_OF_ENTROPY).toByteArray());
        } catch (NoSuchAlgorithmException | NoSuchProviderException exception) {
            throw new Exception(Exception.Type.SECURE_RANDOM_LOADING_FAILURE, RandomBytes.class);
        }
    }

    /**
     * Generates a random byte array of length {@code L}.
     *
     * @param L The desired length of the random byte array
     * @return The random byte array
     */
    @SuppressWarnings("UnnecessaryLocalVariable")
    static public //
    ByteArray
    run(int L) {

        // PRECONDITIONS
        Precondition.checkNotNull(IntSet.NN.contains(L));

        // ALGORITHM
        // Step 1: Reseeding if necessary on a probabilistic base
        if (RANDOM_GENERATOR.nextInt(RESEEDING_FREQUENCY) == 0) {
            RANDOM_GENERATOR.setSeed(Entropy.getEntropy(RESEEDING_BITS_OF_ENTROPY).toByteArray());
        }
        // Step 2: Generate and return random bytes
        byte[] bytes = new byte[L];
        RANDOM_GENERATOR.nextBytes(bytes);
        var B = ByteArray.of(bytes);
        return B;
    }

}
