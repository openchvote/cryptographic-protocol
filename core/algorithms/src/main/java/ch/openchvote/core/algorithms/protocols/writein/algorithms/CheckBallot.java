/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetPrimes;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.parameters.usability.WriteInsParameters;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.CheckBallotProof;
import ch.openchvote.core.algorithms.protocols.writein.model.Ballot;
import ch.openchvote.core.algorithms.protocols.writein.subalgorithms.CheckWriteInProof;
import ch.openchvote.core.algorithms.protocols.writein.subalgorithms.GetWriteInIndices;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;
import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER_OR_EQUAL;

/**
 * Implementation of Algorithm 9.14 from CHVote Protocol Specification
 */
public final class CheckBallot extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Boolean> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters & GGParameters & NIZKPParameters, UP extends CodeParameters & WriteInsParameters>
    boolean
    run(int v, Ballot alpha, BigInteger pk, Vector<BigInteger> bold_pk_prime, IntVector bold_n, IntVector bold_k, IntVector bold_u, IntMatrix bold_E, IntVector bold_v, IntVector bold_z, Vector<BigInteger> bold_x_hat, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters, usabilityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var ZZ_p_hat = securityParameters.get_ZZ_p_hat();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(v, alpha, pk, bold_pk_prime, bold_n, bold_k, bold_u, bold_E, bold_z, bold_v, bold_x_hat);
        var x_hat = alpha.get_x_hat();
        var bold_a = alpha.get_bold_a();
        var pi = alpha.get_pi();
        var e_prime = alpha.get_e_prime();
        var pi_prime = alpha.get_pi_prime();
        var k = bold_a.getLength();
        var t = bold_n.getLength();
        var u = bold_E.getWidth();
        var N_E = bold_E.getHeight();
        var n = bold_v.getLength();
        var z = e_prime.get_bold_a().getLength();
        var z_max = bold_pk_prime.getLength();
        Precondition.check(IntSet.range(1, N_E).contains(v));
        Precondition.check(GG_q_hat.contains(x_hat));
        Precondition.check(Set.Vector(Set.Pair(ZZPlus_p, ZZPlus_p), k).contains(bold_a));
        Precondition.check(Set.Pair(ZZ_twoToTheTau, Set.Triple(ZZ_q_hat, ZZPlus_p, ZZ_q)).contains(pi));
        Precondition.check(Set.Pair(Set.Vector(ZZPlus_p, z), ZZPlus_p).contains(e_prime));
        Precondition.check(Set.Pair(Set.Matrix(ZZ_twoToTheTau, z, 2), Set.Matrix(ZZ_q, z, 2)).contains(pi_prime));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.Vector(ZZPlus_p, z_max).contains(bold_pk_prime));
        Precondition.check(Set.IntVector(IntSet.range(1, u), t).contains(bold_u));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, u).contains(bold_E));
        Precondition.check(Set.IntVector(IntSet.BB, n).contains(bold_v));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_z));
        // for improved efficiency, we use ZZ_p_hat to perform inexact group membership tests for the values of bold_x_hat
        Precondition.check(Set.Vector(ZZ_p_hat, N_E).contains(bold_x_hat));
        // except for bold_x_hat_v
        Precondition.check(GG_q_hat.contains(bold_x_hat.getValue(v)));

        // CONSTRAINTS
        Precondition.check(bold_u.allMatch(SMALLER_OR_EQUAL));
        Precondition.check(n == bold_n.sum());
        Precondition.check(IntVector.allMatch(bold_k, bold_n, SMALLER));
        Precondition.check(z_max == bold_E.select(bold_u).multiply(bold_z.times(bold_k)).maxOrZero());

        // ALGORITHM
        var x_hat_v = bold_x_hat.getValue(v);
        var bold_e_hat_v = bold_E.getRow(v).expand(bold_u);
        var k_prime_v = bold_e_hat_v.multiply(bold_k);
        var z_prime_v = bold_e_hat_v.multiply(bold_z.times(bold_k));
        if (x_hat.equals(x_hat_v) && k == k_prime_v && z == z_prime_v) {
            if (CheckBallotProof.run(pi, x_hat, bold_a, pk, securityParameters)) {
                var bold_p = GetPrimes.run(n, securityParameters);
                bold_pk_prime = bold_pk_prime.select(IntSet.range(1, z));
                var pair = GetWriteInIndices.run(bold_n, bold_k, bold_e_hat_v, bold_v, bold_z);
                var I = pair.getFirst();
                var J = pair.getSecond();
                var bold_a_I = bold_a.select(I);
                var bold_p_J = bold_p.select(J);
                return CheckWriteInProof.run(pi_prime, pk, bold_a_I, bold_pk_prime, e_prime, bold_p_J, securityParameters, usabilityParameters);
            }
        }
        return false;
    }

}
