/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.parameters.usability.CredentialParameters;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionCard;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionCardData;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GetElectionCard;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.Alphabet;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

/**
 * Implementation of Algorithm 8.18 from CHVote Protocol Specification
 */
public final class GetElectionCards extends Algorithm<Vector<ElectionCard>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Vector<ElectionCard>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    <SP extends GGParameters & HashParameters, UP extends CodeParameters & CredentialParameters>
    Vector<ElectionCard>
    run(String AD, IntVector bold_n, Matrix<ElectionCardData> bold_D, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();

        // USABILITY PARAMETERS
        Precondition.checkNotNull(usabilityParameters);
        var L_PA = usabilityParameters.get_L_PA();

        // PRECONDITIONS
        Precondition.checkNotNull(AD, bold_n, bold_D);
        var t = bold_n.getLength();
        var s = bold_D.getWidth();
        var N_E = bold_D.getHeight();
        var n = N_E > 0 && s > 0 ? bold_D.getValue(1, 1).get_bold_p().getLength() : bold_n.sum();
        Precondition.check(Alphabet.UCS_star.contains(AD));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.Matrix(Set.Quadruple(ZZ_q_hat, ZZ_q_hat, Set.B(L_PA), Set.Vector(Set.Pair(ZZ_q_hat, ZZ_q_hat), n)), N_E, s).contains(bold_D));

        // CONSTRAINTS
        Precondition.check(n == bold_n.sum());

        // PREPARATION
        var builder_bold_ec = new Vector.Builder<ElectionCard>(N_E);

        // ALGORITHM
        for (int v : IntSet.range(1, N_E)) {
            var bold_d_v = bold_D.getRow(v);
            var EC_v = GetElectionCard.run(v, AD, n, bold_d_v, securityParameters, usabilityParameters);
            builder_bold_ec.set(v, EC_v);
        }
        var bold_ec = builder_bold_ec.build();
        return bold_ec;
    }

}
