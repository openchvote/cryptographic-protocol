/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.protocols.common.model.CredentialProof;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Parallel;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.15 from CHVote Protocol Specification
 */
public final class GenCredentialProof extends Algorithm<CredentialProof> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<CredentialProof> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "SuspiciousNameCombination", "unused"})
    static public //
    <SP extends GGParameters & NIZKPParameters>
    CredentialProof
    run(Vector<BigInteger> bold_x, Vector<BigInteger> bold_y, Vector<BigInteger> bold_z, Vector<BigInteger> bold_x_hat, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var q_hat = securityParameters.get_q_hat();
        var g_hat = securityParameters.get_g_hat();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_x, bold_y, bold_z, bold_x_hat, bold_y_hat, bold_z_hat);
        var N_E = bold_x.getLength();
        Precondition.check(Set.Vector(ZZ_q_hat, N_E).contains(bold_x));
        Precondition.check(Set.Vector(ZZ_q_hat, N_E).contains(bold_y));
        Precondition.check(Set.Vector(ZZ_q_hat, N_E).contains(bold_z));
        Precondition.check(Set.Vector(GG_q_hat, N_E).contains(bold_x_hat));
        Precondition.check(Set.Vector(GG_q_hat, N_E).contains(bold_y_hat));
        Precondition.check(Set.Vector(GG_q_hat, N_E).contains(bold_z_hat));

        // PREPARATION
        var builder_bold_omega = new Vector.Builder<Triple<BigInteger, BigInteger, BigInteger>>(N_E);
        var builder_bold_t = new Vector.Builder<Triple<BigInteger, BigInteger, BigInteger>>(N_E);
        var builder_bold_s = new Vector.Builder<Triple<BigInteger, BigInteger, BigInteger>>(N_E);

        // ALGORITHM
        Parallel.forLoop(1, N_E, v -> {
            var omega_v_1 = GenRandomInteger.run(q_hat);
            var omega_v_2 = GenRandomInteger.run(q_hat);
            var omega_v_3 = GenRandomInteger.run(q_hat);
            var omega_v = new Triple<>(omega_v_1, omega_v_2, omega_v_3);
            builder_bold_omega.set(v, omega_v);
            var t_v_1 = GG_q_hat.pow(g_hat, omega_v_1);
            var t_v_2 = GG_q_hat.pow(g_hat, omega_v_2);
            var t_v_3 = GG_q_hat.pow(g_hat, omega_v_3);
            var t_v = new Triple<>(t_v_1, t_v_2, t_v_3);
            builder_bold_t.set(v, t_v);
        });

        var bold_omega = builder_bold_omega.build();
        var bold_t = builder_bold_t.build();
        var y = new Triple<>(bold_x_hat, bold_y_hat, bold_z_hat);
        var c = GetChallenge.run(y, bold_t, securityParameters);
        for (int v : IntSet.range(1, N_E)) {
            var s_v_1 = ZZ_q_hat.subtract(bold_omega.getValue(v).getFirst(), ZZ_q_hat.multiply(c, bold_x.getValue(v)));
            var s_v_2 = ZZ_q_hat.subtract(bold_omega.getValue(v).getSecond(), ZZ_q_hat.multiply(c, bold_y.getValue(v)));
            var s_v_3 = ZZ_q_hat.subtract(bold_omega.getValue(v).getThird(), ZZ_q_hat.multiply(c, bold_z.getValue(v)));
            var s_v = new Triple<>(s_v_1, s_v_2, s_v_3);
            builder_bold_s.set(v, s_v);
        }
        var bold_s = builder_bold_s.build();
        var pi = new CredentialProof(c, bold_s);
        return pi;
    }

}
