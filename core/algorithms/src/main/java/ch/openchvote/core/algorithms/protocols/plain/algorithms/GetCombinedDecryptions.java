/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.51 from CHVote Protocol Specification
 */
public final class GetCombinedDecryptions extends Algorithm<Vector<BigInteger>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Vector<BigInteger>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters>
    Vector<BigInteger>
    run(Matrix<BigInteger> bold_C, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_C);
        var N = bold_C.getHeight();
        var s = bold_C.getWidth();
        Precondition.check(Set.Matrix(ZZPlus_p, N, s).contains(bold_C));

        // PREPARATION
        var builder_bold_c = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        for (int i : IntSet.range(1, N)) {
            var bold_c_i = bold_C.getRow(i);
            var c_i = ZZPlus_p.prod(bold_c_i);
            builder_bold_c.set(i, c_i);
        }
        var bold_c = builder_bold_c.build();
        return bold_c;
    }

}
