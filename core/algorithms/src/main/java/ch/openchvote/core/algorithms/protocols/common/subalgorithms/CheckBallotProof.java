/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.BallotProof;
import ch.openchvote.core.algorithms.protocols.common.model.Query;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.26 from CHVote Protocol Specification
 */
public final class CheckBallotProof extends Algorithm<Boolean> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters & GGParameters & NIZKPParameters>
    boolean
    run(BallotProof pi, BigInteger x_hat, Vector<Query> bold_a, BigInteger pk, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var g = securityParameters.get_g();
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var g_hat = securityParameters.get_g_hat();

        // PREPARATION
        var c = pi.get_c();
        var s = pi.get_s();
        var s_1 = s.getFirst();
        var s_2 = s.getSecond();
        var s_3 = s.getThird();

        // ALGORITHM
        var a_1 = ZZPlus_p.prod(bold_a.map(Query::get_a_1));
        var a_2 = ZZPlus_p.prod(bold_a.map(Query::get_a_2));
        var t_1 = GG_q_hat.multiply(GG_q_hat.pow(x_hat, c), GG_q_hat.pow(g_hat, s_1));
        var t_2 = ZZPlus_p.multiply(ZZPlus_p.pow(a_1, c), s_2, ZZPlus_p.pow(pk, s_3));
        var t_3 = ZZPlus_p.multiply(ZZPlus_p.pow(a_2, c), ZZPlus_p.pow(g, s_3));
        var t = new Triple<>(t_1, t_2, t_3);
        var y = new Triple<>(x_hat, bold_a, pk);
        var c_prime = GetChallenge.run(y, t, securityParameters);
        return c.equals(c_prime);
    }

}
