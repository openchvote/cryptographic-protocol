/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GenPermutation;
import ch.openchvote.core.algorithms.protocols.writein.model.AugmentedEncryption;
import ch.openchvote.core.algorithms.protocols.writein.subalgorithms.GenReEncryption;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tuples.Quadruple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.18 from CHVote Protocol Specification
 */
public final class GenShuffle extends Algorithm<Quadruple<Vector<AugmentedEncryption>, Vector<BigInteger>, Vector<BigInteger>, IntVector>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Quadruple<Vector<AugmentedEncryption>, Vector<BigInteger>, Vector<BigInteger>, IntVector>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public //
    <SP extends ZZPlusParameters>
    Quadruple<Vector<AugmentedEncryption>, Vector<BigInteger>, Vector<BigInteger>, IntVector>
    run(Vector<AugmentedEncryption> bold_e_bar, BigInteger pk, Vector<BigInteger> bold_pk_prime, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_e_bar, pk, bold_pk_prime);
        var N = bold_e_bar.getLength();
        var z = bold_pk_prime.getLength();
        Precondition.check(Set.Vector(Set.Quadruple(ZZPlus_p, ZZPlus_p, Set.Vector(ZZPlus_p, z), ZZPlus_p), N).contains(bold_e_bar));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.Vector(ZZPlus_p, z).contains(bold_pk_prime));

        // PREPARATION
        var builder_bold_e_tilde = new Vector.Builder<AugmentedEncryption>(N);
        var builder_bold_r_tilde = new Vector.Builder<BigInteger>(N);
        var builder_bold_r_tilde_prime = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        var psi = GenPermutation.run(N);
        for (int i : IntSet.range(1, N)) {
            var j_i = psi.getValue(i);
            var e_bar_j_i = bold_e_bar.getValue(j_i);
            var triple = GenReEncryption.run(e_bar_j_i, pk, bold_pk_prime, securityParameters);
            builder_bold_e_tilde.set(i, triple.getFirst());
            builder_bold_r_tilde.set(j_i, triple.getSecond());
            builder_bold_r_tilde_prime.set(j_i, triple.getThird());
        }
        var bold_e_tilde = builder_bold_e_tilde.build();
        var bold_r_tilde = builder_bold_r_tilde.build();
        var bold_r_tilde_prime = builder_bold_r_tilde_prime.build();
        return new Quadruple<>(bold_e_tilde, bold_r_tilde, bold_r_tilde_prime, psi);
    }

}
