/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.RecHash;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Confirmation;
import ch.openchvote.core.algorithms.protocols.common.model.Point;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IndexedFamily;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

/**
 * Implementation of Algorithm 8.55 from CHVote Protocol Specification
 */
public final class GetInspection extends Algorithm<ByteArray> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<GetInspection> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public //
    <SP extends GGParameters & HashParameters, UP extends CodeParameters>
    ByteArray
    run(int v, Matrix<Point> bold_P, Vector<ByteArray> bold_a, IndexedFamily<Confirmation> C, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();

        // USABILITY PARAMETERS
        Precondition.checkNotNull(usabilityParameters);
        var L_PA = usabilityParameters.get_L_PA();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_P, bold_a, C);
        var N_E = bold_P.getHeight();
        var n = bold_P.getWidth();
        Precondition.check(IntSet.range(1, N_E).contains(v));
        Precondition.check(Set.Matrix(Set.Pair(ZZ_q_hat, ZZ_q_hat), N_E, n).contains(bold_P));
        Precondition.check(Set.Vector(Set.B(L_PA), N_E).contains(bold_a));
        Precondition.check(IntSet.range(1, N_E).containsAll(C.getIndices()));

        // ALGORITHM
        ByteArray I_v;
        if (C.containsIndex(v)) {
            var bold_p_v = bold_P.getRow(v);
            I_v = RecHash.run(bold_p_v, securityParameters).truncate(L_PA);
        } else {
            I_v = bold_a.getValue(v);
        }
        return I_v;
    }

}
