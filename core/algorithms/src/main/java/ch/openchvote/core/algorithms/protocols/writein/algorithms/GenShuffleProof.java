/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenges;
import ch.openchvote.core.algorithms.general.algorithms.GetGenerators;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GenCommitmentChain;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GenPermutationCommitment;
import ch.openchvote.core.algorithms.protocols.writein.model.AugmentedEncryption;
import ch.openchvote.core.algorithms.protocols.writein.model.ShuffleProof;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tuples.Pair;
import ch.openchvote.base.utilities.tuples.Quadruple;
import ch.openchvote.base.utilities.tuples.Quintuple;
import ch.openchvote.base.utilities.tuples.Sextuple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.20 from CHVote Protocol Specification
 */
public final class GenShuffleProof extends Algorithm<ShuffleProof> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<ShuffleProof> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    <SP extends ZZPlusParameters & NIZKPParameters>
    ShuffleProof
    run(Vector<AugmentedEncryption> bold_e_bar, Vector<AugmentedEncryption> bold_e_tilde, Vector<BigInteger> bold_r_tilde, Vector<BigInteger> bold_r_tilde_prime, IntVector psi, BigInteger pk, Vector<BigInteger> bold_pk_prime, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();
        var h = securityParameters.get_h();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_e_bar, bold_e_tilde, bold_r_tilde, bold_r_tilde_prime, psi, pk, bold_pk_prime);
        var N = bold_e_bar.getLength();
        var z = bold_pk_prime.getLength();
        Precondition.check(Set.Vector(Set.Quadruple(ZZPlus_p, ZZPlus_p, Set.Vector(ZZPlus_p, z), ZZPlus_p), N).contains(bold_e_bar));
        Precondition.check(Set.Vector(Set.Quadruple(ZZPlus_p, ZZPlus_p, Set.Vector(ZZPlus_p, z), ZZPlus_p), N).contains(bold_e_tilde));
        Precondition.check(Set.Vector(ZZ_q, N).contains(bold_r_tilde));
        Precondition.check(Set.Vector(ZZ_q, N).contains(bold_r_tilde_prime));
        Precondition.check(Set.Psi(N).contains(psi));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.Vector(ZZPlus_p, z).contains(bold_pk_prime));

        // PREPARATION
        var builder_bold_u_tilde = new Vector.Builder<BigInteger>(N);
        var builder_bold_omega_hat = new Vector.Builder<BigInteger>(N);
        var builder_bold_omega_tilde = new Vector.Builder<BigInteger>(N);
        var builder_bold_t_hat = new Vector.Builder<BigInteger>(N);
        var builder_bold_v = new Vector.Builder<BigInteger>(N);
        var builder_bold_s_hat = new Vector.Builder<BigInteger>(N);
        var builder_bold_s_tilde = new Vector.Builder<BigInteger>(N);
        var builder_bold_t_43 = new Vector.Builder<BigInteger>(z);

        // ALGORITHM
        var bold_h = GetGenerators.run(N, securityParameters);
        var pair1 = GenPermutationCommitment.run(psi, bold_h, securityParameters);
        var bold_c = pair1.getFirst();
        var bold_r = pair1.getSecond();
        var bold_u = GetChallenges.run(N, new Quadruple<>(bold_e_bar, bold_e_tilde, bold_c, pk), securityParameters);
        for (int i : IntSet.range(1, N)) {
            builder_bold_u_tilde.set(i, bold_u.getValue(psi.getValue(i)));
        }
        var bold_u_tilde = builder_bold_u_tilde.build();
        var pair2 = GenCommitmentChain.run(bold_u_tilde, securityParameters);
        var bold_c_hat = pair2.getFirst();
        var bold_r_hat = pair2.getSecond();
        var R_i_minus_1 = BigInteger.ZERO;
        var U_i_minus_1 = BigInteger.ONE;
        for (int i : IntSet.range(1, N)) {
            var omega_hat_i = GenRandomInteger.run(q);
            var omega_tilde_i = GenRandomInteger.run(q);
            var R_i = ZZ_q.add(bold_r_hat.getValue(i), ZZ_q.multiply(bold_u_tilde.getValue(i), R_i_minus_1));
            var U_i = ZZ_q.multiply(bold_u_tilde.getValue(i), U_i_minus_1);
            var R_prime_i = ZZ_q.add(omega_hat_i, ZZ_q.multiply(omega_tilde_i, R_i_minus_1));
            var U_prime_i = ZZ_q.multiply(omega_tilde_i, U_i_minus_1);
            var t_hat_i = ZZPlus_p.multiply(ZZPlus_p.pow(g, R_prime_i), ZZPlus_p.pow(h, U_prime_i));
            builder_bold_omega_hat.set(i, omega_hat_i);
            builder_bold_omega_tilde.set(i, omega_tilde_i);
            builder_bold_t_hat.set(i, t_hat_i);
            R_i_minus_1 = R_i; // preparation for next loop cycle
            U_i_minus_1 = U_i; // preparation for next loop cycle
        }
        var bold_omega_hat = builder_bold_omega_hat.build();
        var bold_omega_tilde = builder_bold_omega_tilde.build();
        var omega_1 = GenRandomInteger.run(q);
        var omega_2 = GenRandomInteger.run(q);
        var omega_3 = GenRandomInteger.run(q);
        var omega_4 = GenRandomInteger.run(q);
        var omega_prime_4 = GenRandomInteger.run(q);
        var t_1 = ZZPlus_p.pow(g, omega_1);
        var t_2 = ZZPlus_p.pow(g, omega_2);
        var t_3 = ZZPlus_p.multiply(ZZPlus_p.pow(g, omega_3), ZZPlus_p.prodPow(bold_h, bold_omega_tilde));
        var t_41 = ZZPlus_p.multiply(ZZPlus_p.invert(ZZPlus_p.pow(pk, omega_4)), ZZPlus_p.prodPow(bold_e_tilde.map(AugmentedEncryption::get_a), bold_omega_tilde));
        var t_42 = ZZPlus_p.multiply(ZZPlus_p.invert(ZZPlus_p.pow(g, omega_4)), ZZPlus_p.prodPow(bold_e_tilde.map(AugmentedEncryption::get_b), bold_omega_tilde));
        for (int j : IntSet.range(1, z)) {
            var j_final = j; // necessary for using j in lambda expression
            var pk_prime_j = bold_pk_prime.getValue(j);
            var t_43_j = ZZPlus_p.multiply(ZZPlus_p.invert(ZZPlus_p.pow(pk_prime_j, omega_prime_4)), ZZPlus_p.prodPow(bold_e_tilde.map(AugmentedEncryption::get_bold_a_prime).map(a -> a.getValue(j_final)), bold_omega_tilde));
            builder_bold_t_43.set(j, t_43_j);
        }
        var bold_t_43 = builder_bold_t_43.build();
        var t_44 = ZZPlus_p.multiply(ZZPlus_p.invert(ZZPlus_p.pow(g, omega_prime_4)), ZZPlus_p.prodPow(bold_e_tilde.map(AugmentedEncryption::get_b_prime), bold_omega_tilde));
        var t = new Quintuple<>(t_1, t_2, t_3, new Quadruple<>(t_41, t_42, bold_t_43, t_44), builder_bold_t_hat.build());
        var y = new Sextuple<>(bold_e_bar, bold_e_tilde, bold_c, bold_c_hat, pk, bold_pk_prime);
        var c = GetChallenge.run(y, t, securityParameters);
        var r_bar = ZZ_q.sum(bold_r);
        var s_1 = ZZ_q.subtract(omega_1, ZZ_q.multiply(c, r_bar));
        var v_i = BigInteger.ONE;
        for (int i = N; i >= 1; i--) {
            builder_bold_v.set(i, v_i);
            v_i = ZZ_q.multiply(bold_u_tilde.getValue(i), v_i);
        }
        var bold_v = builder_bold_v.build();
        var r_hat = ZZ_q.sumProd(bold_r_hat, bold_v);
        var s_2 = ZZ_q.subtract(omega_2, ZZ_q.multiply(c, r_hat));
        var r = ZZ_q.sumProd(bold_r, bold_u);
        var s_3 = ZZ_q.subtract(omega_3, ZZ_q.multiply(c, r));
        var r_tilde = ZZ_q.sumProd(bold_r_tilde, bold_u);
        var s_4 = ZZ_q.subtract(omega_4, ZZ_q.multiply(c, r_tilde));
        for (int i : IntSet.range(1, N)) {
            var s_hat_i = ZZ_q.subtract(bold_omega_hat.getValue(i), ZZ_q.multiply(c, bold_r_hat.getValue(i)));
            builder_bold_s_hat.set(i, s_hat_i);
            var s_tilde_i = ZZ_q.subtract(bold_omega_tilde.getValue(i), ZZ_q.multiply(c, bold_u_tilde.getValue(i)));
            builder_bold_s_tilde.set(i, s_tilde_i);
        }
        var r_tilde_prime = ZZ_q.sumProd(bold_r_tilde_prime, bold_u);
        var s_prime_4 = ZZ_q.subtract(omega_prime_4, ZZ_q.multiply(c, r_tilde_prime));
        var s = new Sextuple<>(s_1, s_2, s_3, new Pair<>(s_4, s_prime_4), builder_bold_s_hat.build(), builder_bold_s_tilde.build());
        var pi = new ShuffleProof(c, s, bold_c, bold_c_hat);
        return pi;
    }

}
