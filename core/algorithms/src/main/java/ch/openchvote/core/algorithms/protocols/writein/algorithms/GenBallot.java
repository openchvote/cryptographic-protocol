/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetPrimes;
import ch.openchvote.core.algorithms.general.algorithms.StringToInteger;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.parameters.usability.CredentialParameters;
import ch.openchvote.core.algorithms.parameters.usability.WriteInsParameters;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GenBallotProof;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GenQuery;
import ch.openchvote.core.algorithms.protocols.writein.model.Ballot;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteIn;
import ch.openchvote.core.algorithms.protocols.writein.subalgorithms.GenWriteInEncryption;
import ch.openchvote.core.algorithms.protocols.writein.subalgorithms.GenWriteInProof;
import ch.openchvote.core.algorithms.protocols.writein.subalgorithms.GetEncodedWriteIns;
import ch.openchvote.core.algorithms.protocols.writein.subalgorithms.GetWriteInIndices;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Math;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;

/**
 * Implementation of Algorithm 9.8 from CHVote Protocol Specification
 */
public final class GenBallot extends Algorithm<Pair<Ballot, Vector<BigInteger>>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Pair<Ballot, Vector<BigInteger>>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination", "unused"})
    static public //
    <SP extends ZZPlusParameters & GGParameters & NIZKPParameters, UP extends CredentialParameters & CodeParameters & WriteInsParameters>
    Pair<Ballot, Vector<BigInteger>>
    run(String X, IntVector bold_s, Vector<WriteIn> bold_s_prime, BigInteger pk, Vector<BigInteger> bold_pk_prime, IntVector bold_n, IntVector bold_k, int w, IntVector bold_e_hat, IntVector bold_v, IntVector bold_z, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var q = securityParameters.get_q();
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var g_hat = securityParameters.get_g_hat();

        // USABILITY PARAMETERS
        Precondition.checkNotNull(usabilityParameters);
        var A_X = usabilityParameters.get_A_X();
        var ell_X = usabilityParameters.get_ell_X();
        var A_W = usabilityParameters.get_A_W();
        var ell_W = usabilityParameters.get_ell_W();

        // PRECONDITIONS
        Precondition.checkNotNull(X, bold_s, bold_s_prime, pk, bold_pk_prime, bold_n, bold_k, bold_e_hat, bold_v, bold_z);
        var k = bold_s.getLength();
        var z = bold_s_prime.getLength();
        var t = bold_n.getLength();
        var n = bold_v.getLength();
        var z_max = bold_pk_prime.getLength();
        Precondition.check(Set.String(A_X, ell_X).contains(X));
        Precondition.check(Set.IntVector(IntSet.range(1, n), k).contains(bold_s));
        Precondition.check(Set.Vector(Set.Pair(Set.String(A_W, 0, ell_W), Set.String(A_W, 0, ell_W)), z).contains(bold_s_prime));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.Vector(ZZPlus_p, z_max).contains(bold_pk_prime));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(IntSet.NN_plus.contains(w));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_e_hat));
        Precondition.check(Set.IntVector(IntSet.BB, n).contains(bold_v));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_z));
        Precondition.check(IntSet.NN.contains(z_max));

        // CONSTRAINTS
        Precondition.check(bold_s.allMatch(SMALLER));
        Precondition.check(n == bold_n.sum());
        Precondition.check(k == bold_e_hat.multiply(bold_k));
        Precondition.check(IntVector.allMatch(bold_k, bold_n, SMALLER));
        Precondition.check(z == bold_e_hat.multiply(bold_z.times(bold_k)));
        Precondition.check(z <= z_max && z_max <= bold_z.multiply(bold_k));

        // ALGORITHM
        var x = StringToInteger.run(X, A_X);
        var x_hat = GG_q_hat.pow(g_hat, x);
        var bold_p = GetPrimes.run(n + w, securityParameters);
        var bold_m = bold_p.select(bold_s);
        var m = Math.prod(bold_m);
        if (bold_p.getValue(n + w).multiply(m).compareTo(q) > 0) {
            throw new Exception(Exception.Type.INCOMPATIBLE_MATRIX, GenBallot.class);
        }
        var pair1 = GenQuery.run(bold_m, pk, securityParameters);
        var bold_a = pair1.getFirst();
        var bold_r = pair1.getSecond();
        var r = ZZ_q.sum(bold_r);
        var pi = GenBallotProof.run(x, ZZPlus_p.mapToGroup(m), r, x_hat, bold_a, pk, securityParameters);
        var bold_m_prime = GetEncodedWriteIns.run(bold_s_prime, usabilityParameters);
        var pair2 = GenWriteInEncryption.run(bold_pk_prime, bold_m_prime, securityParameters);
        var e_prime = pair2.getFirst();
        var r_prime = pair2.getSecond();
        var pair3 = GetWriteInIndices.run(bold_n, bold_k, bold_e_hat, bold_v, bold_z);
        var I = pair3.getFirst();
        var bold_m_I = bold_m.select(I);
        var bold_a_I = bold_a.select(I);
        var bold_r_I = bold_r.select(I);
        var J = pair3.getSecond();
        var bold_p_J = bold_p.select(J);
        var pi_prime = GenWriteInProof.run(pk, bold_m_I, bold_a_I, bold_r_I, bold_pk_prime, bold_m_prime, e_prime, r_prime, bold_p_J, securityParameters, usabilityParameters);
        var alpha = new Ballot(x_hat, bold_a, pi, e_prime, pi_prime);
        return new Pair<>(alpha, bold_r);
    }

}
