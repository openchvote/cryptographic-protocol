/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.model;

/**
 * This is a common interface for different election parameter classes. Currently, there are two such classes, one for
 * each of the two protocols.
 */
public interface ElectionParameters {

    /**
     * Return the number of elections in an election event.
     *
     * @return The number of elections
     */
    int get_t();

    /**
     * Return the number of election groups in an election event.
     *
     * @return The number of election groups
     */
    int get_u();

    /**
     * Return the number of candidates in an election event.
     *
     * @return The number of candidates
     */
    int get_n();

    /**
     * Return the number of voters in an election event.
     *
     * @return The number of voters
     */
    int get_N_E();

    /**
     * Return the number of counting circles in an election event.
     *
     * @return The number of counting circles
     */
    int get_w();

}
