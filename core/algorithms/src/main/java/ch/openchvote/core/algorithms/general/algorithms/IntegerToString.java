/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.set.Alphabet;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 4.7 from CHVote Protocol Specification
 */
public final class IntegerToString extends Algorithm<String> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    String
    run(BigInteger x, int n, Alphabet A) {

        // PRECONDITIONS
        Precondition.checkNotNull(x, A);
        var N = BigInteger.valueOf(A.getSize());
        Precondition.check(Set.NN.contains(x));
        Precondition.check(IntSet.NN.contains(n));
        Precondition.check(x.compareTo(N.pow(n)) < 0);

        // PREPARATION
        var builder_S = new StringBuilder(n);
        builder_S.setLength(n); // ensures that the StringBuilder can be filled up backwards

        // ALGORITHM
        for (int i : IntSet.range(1, n)) {
            builder_S.setCharAt(n - i, A.getChar(x.mod(N).intValue()));
            x = x.divide(N);
        }
        var S = builder_S.toString();
        return S;
    }

}
