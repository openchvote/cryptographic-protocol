/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.protocols.common.model.PublicCredentials;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.17 from CHVote Protocol Specification
 */
public final class GetPublicCredentials extends Algorithm<PublicCredentials> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<PublicCredentials> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination"})
    static public //
    <SP extends GGParameters>
    PublicCredentials
    run(Matrix<BigInteger> bold_X_hat, Matrix<BigInteger> bold_Y_hat, Matrix<BigInteger> bold_Z_hat, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var GG_q_hat = securityParameters.get_GG_q_hat();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_X_hat, bold_Y_hat, bold_Z_hat);
        var N_E = bold_X_hat.getHeight();
        var s = bold_X_hat.getWidth();
        Precondition.check(Set.Matrix(GG_q_hat, N_E, s).contains(bold_X_hat));
        Precondition.check(Set.Matrix(GG_q_hat, N_E, s).contains(bold_Y_hat));
        Precondition.check(Set.Matrix(GG_q_hat, N_E, s).contains(bold_Z_hat));

        // PREPARATION
        var builder_bold_x_hat = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_y_hat = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_z_hat = new Vector.Builder<BigInteger>(N_E);

        // ALGORITHM
        for (int v : IntSet.range(1, N_E)) {
            var x_hat_v = GG_q_hat.prod(bold_X_hat.getRow(v));
            var y_hat_v = GG_q_hat.prod(bold_Y_hat.getRow(v));
            var z_hat_v = GG_q_hat.prod(bold_Z_hat.getRow(v));
            builder_bold_x_hat.set(v, x_hat_v);
            builder_bold_y_hat.set(v, y_hat_v);
            builder_bold_z_hat.set(v, z_hat_v);
        }
        var bold_x_hat = builder_bold_x_hat.build();
        var bold_y_hat = builder_bold_y_hat.build();
        var bold_z_hat = builder_bold_z_hat.build();
        return new PublicCredentials(bold_x_hat, bold_y_hat, bold_z_hat);
    }

}
