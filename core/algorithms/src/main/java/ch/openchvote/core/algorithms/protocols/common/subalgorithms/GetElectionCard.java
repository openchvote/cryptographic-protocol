/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.ByteArrayToString;
import ch.openchvote.core.algorithms.general.algorithms.IntegerToString;
import ch.openchvote.core.algorithms.general.algorithms.RecHash;
import ch.openchvote.core.algorithms.general.algorithms.SetWatermark;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.parameters.usability.CredentialParameters;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionCard;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionCardData;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Quadruple;

/**
 * Implementation of Algorithm 8.19 from CHVote Protocol Specification
 */
public final class GetElectionCard extends Algorithm<ElectionCard> {

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination"})
    static public //
    <SP extends GGParameters & HashParameters, UP extends CodeParameters & CredentialParameters>
    ElectionCard
    run(int v, String AD, int n, Vector<ElectionCardData> bold_d, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();

        // USABILITY PARAMETERS
        var A_X = usabilityParameters.get_A_X();
        var A_Y = usabilityParameters.get_A_Y();
        var ell_X = usabilityParameters.get_ell_X();
        var ell_Y = usabilityParameters.get_ell_Y();
        var A_V = usabilityParameters.get_A_V();
        var L_V = usabilityParameters.get_L_V();
        var A_PA = usabilityParameters.get_A_PA();
        var L_PA = usabilityParameters.get_L_PA();
        var n_max = usabilityParameters.get_n_max();

        // PREPARATION
        var s = bold_d.getLength();
        var builder_bold_p = new Vector.Builder<ByteArray>(s);
        var builder_bold_vc = new Vector.Builder<String>(n);

        // ALGORITHM
        var x = ZZ_q_hat.sum(bold_d.map(ElectionCardData::get_x));
        var y = ZZ_q_hat.sum(bold_d.map(ElectionCardData::get_y));
        var X = IntegerToString.run(x, ell_X, A_X);
        var Y = IntegerToString.run(y, ell_Y, A_Y);
        for (int i : IntSet.range(1, n)) {
            var builder_bold_v_i = new Vector.Builder<ByteArray>(s);
            for (int j : IntSet.range(1, s)) {
                var V_ij = RecHash.run(bold_d.getValue(j).getFourth().getValue(i), securityParameters).truncate(L_V);
                builder_bold_v_i.set(j, V_ij);
            }
            var bold_v_i = builder_bold_v_i.build();
            var V_i = ByteArray.xor(bold_v_i, L_V);
            V_i = SetWatermark.run(V_i, i - 1, n_max);
            var VC_i = ByteArrayToString.run(V_i, A_V);
            builder_bold_vc.set(i, VC_i);
        }
        var bold_vc = builder_bold_vc.build();
        for (int j : IntSet.range(1, s)) {
            var P_j = RecHash.run(bold_d.getValue(j).getFourth(), securityParameters).truncate(L_PA);
            builder_bold_p.set(j, P_j);
        }
        var bold_p = builder_bold_p.build();
        var PC = ByteArrayToString.run(ByteArray.xor(bold_p, L_PA), A_PA);
        var bold_a = bold_d.map(Quadruple::getThird);
        var AC = ByteArrayToString.run(ByteArray.xor(bold_a, L_PA), A_PA);
        return new ElectionCard(v, X, Y, bold_vc, PC, AC, AD);
    }

}
