/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Parallel;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.48 from CHVote Protocol Specification
 */
public final class GetDecryptions extends Algorithm<Vector<BigInteger>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Vector<BigInteger>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    <SP extends ZZPlusParameters>
    Vector<BigInteger>
    run(Vector<Encryption> bold_e, BigInteger sk, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_e, sk);
        var N = bold_e.getLength();
        Precondition.check(Set.Vector(Set.Pair(ZZPlus_p, ZZPlus_p), N).contains(bold_e));
        Precondition.check(ZZ_q.contains(sk));

        // PREPARATION
        var builder_bold_c = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        Parallel.forLoop(1, N, i -> {
            var b_i = bold_e.getValue(i).getSecond();
            var c_i = ZZPlus_p.pow(b_i, sk);
            builder_bold_c.set(i, c_i);
        });
        var bold_c = builder_bold_c.build();
        return bold_c;
    }

}
