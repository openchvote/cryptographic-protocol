/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.parameters.usability.WriteInsParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteIn;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.9 from CHVote Protocol Specification
 */
public final class GetEncodedWriteIns extends Algorithm<Vector<BigInteger>> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <UP extends CodeParameters & WriteInsParameters>
    Vector<BigInteger>
    run(Vector<WriteIn> bold_s, UP usabilityParameters) {

        // USABILITY PARAMETERS
        var A_W = usabilityParameters.get_A_W();
        var ell_W = usabilityParameters.get_ell_W();
        var c_W = usabilityParameters.get_c_W();

        // PREPARATION
        var z = bold_s.getLength();
        var builder_bold_m = new Vector.Builder<BigInteger>(z);

        // ALGORITHM
        for (int i : IntSet.range(1, z)) {
            var m_i = GetEncodedStrings.run(bold_s.getValue(i), A_W, ell_W, c_W);
            builder_bold_m.set(i, m_i);
        }
        var bold_m = builder_bold_m.build();
        return bold_m;
    }

}
