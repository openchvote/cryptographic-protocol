/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms;

import ch.openchvote.core.algorithms.parameters.security.SecurityParameters;
import ch.openchvote.core.algorithms.parameters.usability.UsabilityParameters;

import java.util.Arrays;

import static ch.openchvote.core.algorithms.Algorithm.Exception.Type.UNDEFINED_RUN_METHOD;

/**
 * Default implementation of the {@link AlgorithmService} interface for directing algorithm executions to their Java
 * implementations.
 */
public class JavaAlgorithmService implements AlgorithmService {

    @Override
    public Language getLanguage() {
        return Language.JAVA;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <R> R run(Class<? extends Algorithm<R>> algorithm, Object... arguments) throws Algorithm.Exception {
        var runMethod = Algorithm.getRunMethod(algorithm);
        try {
            var result = runMethod.invoke(null, arguments); // null means invoking static method
            return (R) result;
        } catch (Exception exception) {
            throw new Algorithm.Exception(UNDEFINED_RUN_METHOD, algorithm);
        }
    }

    @Override
    public <R> R run(Class<? extends Algorithm<R>> algorithm, SecurityParameters securityParameters, Object... arguments) throws Algorithm.Exception {
        var extArguments = Arrays.copyOf(arguments, arguments.length + 1);
        extArguments[extArguments.length - 1] = securityParameters;
        return this.run(algorithm, extArguments);
    }

    @Override
    public <R> R run(Class<? extends Algorithm<R>> algorithm, UsabilityParameters usabilityParameters, Object... arguments) throws Algorithm.Exception {
        var extArguments = Arrays.copyOf(arguments, arguments.length + 1);
        extArguments[extArguments.length - 1] = usabilityParameters;
        return this.run(algorithm, extArguments);
    }

    @Override
    public <R> R run(Class<? extends Algorithm<R>> algorithm, SecurityParameters securityParameters, UsabilityParameters usabilityParameters, Object... arguments) throws Algorithm.Exception {
        var extArguments = Arrays.copyOf(arguments, arguments.length + 2);
        extArguments[extArguments.length - 2] = securityParameters;
        extArguments[extArguments.length - 1] = usabilityParameters;
        return this.run(algorithm, extArguments);
    }

}
