/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.model;

import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Model class for encryptions inheriting from {@link Pair}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public sealed class Encryption extends Pair<BigInteger, BigInteger> implements Comparable<Encryption> permits Query {

    public Encryption(BigInteger a, BigInteger b) {
        super(a, b);
    }

    public BigInteger get_a() {
        return this.getFirst();
    }

    public BigInteger get_b() {
        return this.getSecond();
    }

    @Override
    public int compareTo(Encryption other) {
        var c = this.get_a().compareTo(other.get_a());
        return c == 0 ? this.get_b().compareTo(other.get_b()) : c;
    }

}
