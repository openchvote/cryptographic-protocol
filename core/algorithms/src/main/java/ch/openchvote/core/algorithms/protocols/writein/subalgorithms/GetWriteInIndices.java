/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Pair;

/**
 * Implementation of Algorithm 9.11 from CHVote Protocol Specification
 */
public final class GetWriteInIndices extends Algorithm<Pair<IntSet, IntSet>> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    Pair<IntSet, IntSet>
    run(IntVector bold_n, IntVector bold_k, IntVector bold_e_hat, IntVector bold_v, IntVector bold_z) {

        // PREPARATION
        var t = bold_n.getLength();

        // ALGORITHM
        var builder_I = new IntSet.Builder();
        var builder_J = new IntSet.Builder();
        var n_prime = 0;
        var k_prime = 0;
        for (int l : IntSet.range(1, t)) {
            var n_l = bold_n.getValue(l);
            var k_l = bold_k.getValue(l);
            var e_hat_l = bold_e_hat.getValue(l);
            var z_l = bold_z.getValue(l);
            if (e_hat_l == 1) {
                if (z_l == 1) {
                    for (int i : IntSet.range(k_prime + 1, k_prime + k_l)) {
                        builder_I.add(i);
                    }
                    for (int j : IntSet.range(n_prime + 1, n_prime + n_l)) {
                        var v_j = bold_v.getValue(j);
                        if (v_j == 1) {
                            builder_J.add(j);
                        }
                    }
                }
                k_prime = k_prime + k_l;
            }
            n_prime = n_prime + n_l;
        }
        var I = builder_I.build();
        var J = builder_J.build();
        return new Pair<>(I, J);
    }

}
