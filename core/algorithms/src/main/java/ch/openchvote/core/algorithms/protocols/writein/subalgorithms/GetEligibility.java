/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.set.IntSet;

/**
 * Implementation of Algorithm 9.29 from CHVote Protocol Specification
 */
public final class GetEligibility extends Algorithm<IntVector> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    IntVector
    run(IntVector bold_v, IntVector bold_n) {

        // PREPARATION
        var t = bold_n.getLength();
        var builder_bold_e_hat = new IntVector.Builder(t);

        // ALGORITHM
        var n_prime = 0;
        for (int l : IntSet.range(1, t)) {
            var e_hat_l = 0;
            var n_l = bold_n.getValue(l);
            for (int i : IntSet.range(n_prime + 1, n_prime + n_l)) {
                var v_i = bold_v.getValue(i);
                if (v_i == 1) {
                    e_hat_l = 1;
                }
            }
            n_prime = n_prime + n_l;
            builder_bold_e_hat.set(l, e_hat_l);
        }
        var bold_e_hat = builder_bold_e_hat.build();
        return bold_e_hat;
    }

}
