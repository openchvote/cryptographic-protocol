/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.parameters.usability.WriteInsParameters;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.GetDecodedStrings;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteIn;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.28 from CHVote Protocol Specification
 */
public final class GetWriteIns extends Algorithm<Pair<Matrix<WriteIn>, IntMatrix>> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters, UP extends CodeParameters & WriteInsParameters>
    Pair<Matrix<WriteIn>, IntMatrix>
    run(Matrix<BigInteger> bold_M, IntMatrix bold_V, IntVector bold_n, IntVector bold_k, IntVector bold_z, SP securityParameters, UP usabilityParameters) {

        // USABILITY PARAMETERS
        var A_W = usabilityParameters.get_A_W();
        var ell_W = usabilityParameters.get_ell_W();
        var c_W = usabilityParameters.get_c_W();

        // PREPARATION
        var t = bold_n.getLength();
        var z = bold_M.getWidth();
        var N = bold_M.getHeight();
        var builder_bold_S = new Matrix.Builder<WriteIn>(N, z);
        var builder_bold_T = new IntMatrix.Builder(N, z);

        // ALGORITHM
        var m_max = BigInteger.valueOf(A_W.getSize() + 1).pow(2 * ell_W);
        for (int i : IntSet.range(1, N)) {
            var bold_v = bold_V.getRow(i);
            var bold_e_hat = GetEligibility.run(bold_v, bold_n);
            int j = 0;
            var k_prime = 0;
            for (int l : IntSet.range(1, t)) {
                var e_hat_l = bold_e_hat.getValue(l);
                var k_l = bold_k.getValue(l);
                var z_l = bold_z.getValue(l);
                if (e_hat_l * z_l == 1) {
                    for (int k : IntSet.range(k_prime + 1, k_prime + k_l)) {
                        BigInteger m_ij;
                        do {
                            j = j + 1;
                            if (j > z) {
                                throw new Exception(Exception.Type.INCOMPATIBLE_MATRIX, GetWriteIns.class);
                            }
                            m_ij = bold_M.getValue(i, j);
                        } while (m_ij.compareTo(m_max) > 0);
                        var S_ik = GetDecodedStrings.run(m_ij, A_W, ell_W, c_W, securityParameters);
                        var t_ik = l;
                        builder_bold_S.set(i, k, S_ik);
                        builder_bold_T.set(i, k, t_ik);
                    }
                    k_prime = k_prime + k_l;
                }
            }
            for (int k : IntSet.range(k_prime + 1, z)) {
                builder_bold_S.set(i, k, null); // default value
                builder_bold_T.set(i, k, 0); // default value
            }
        }
        var bold_S = builder_bold_S.build();
        var bold_T = builder_bold_T.build();
        return new Pair<>(bold_S, bold_T);
    }

}
