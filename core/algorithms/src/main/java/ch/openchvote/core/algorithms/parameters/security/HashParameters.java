/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.parameters.security;

import ch.openchvote.base.utilities.crypto.HashAlgorithm;

/**
 * Instances implementing this interface provide the parameters of a hash algorithm.
 */
public interface HashParameters extends SecurityParameters {

    /**
     * Returns the hash algorithm itself.
     *
     * @return The hash algorithm
     */
    HashAlgorithm getHashAlgorithm();

    /**
     * Returns the length (number of bytes) of the generated hash codes.
     *
     * @return The hash code length in bytes
     */
    default int get_L() {
        return this.getHashAlgorithm().getLength();
    }

    /**
     * Returns the length (number of bits) of the generated hash codes.
     *
     * @return The hash code length in bits
     */
    default int get_ell() {
        return this.get_L() * Byte.SIZE;
    }

}
