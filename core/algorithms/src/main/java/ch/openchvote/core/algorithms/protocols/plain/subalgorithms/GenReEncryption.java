/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.43 from CHVote Protocol Specification
 */
public final class GenReEncryption extends Algorithm<Pair<Encryption, BigInteger>> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters>
    Pair<Encryption, BigInteger>
    run(Encryption e, BigInteger pk, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();

        // PREPARATION
        var a = e.get_a();
        var b = e.get_b();

        // ALGORITHM
        var r_tilde = GenRandomInteger.run(q);
        var a_tilde = ZZPlus_p.multiply(a, ZZPlus_p.pow(pk, r_tilde));
        var b_tilde = ZZPlus_p.multiply(b, ZZPlus_p.pow(g, r_tilde));
        var e_tilde = new Encryption(a_tilde, b_tilde);
        return new Pair<>(e_tilde, r_tilde);
    }

}
