/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Math;

import java.math.BigInteger;

/**
 * ALGORITHMS 4.3 and 4.4
 */
public final class IntegerToByteArray extends Algorithm<ByteArray> {

    // ALGORITHM 4.3
    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    ByteArray
    run(BigInteger x) {

        // PRECONDITIONS
        Precondition.checkNotNull(x);
        Precondition.check(Set.NN.contains(x));

        // ALGORITHM
        var n_min = Math.ceilDiv(x.bitLength(), Byte.SIZE);
        var B = IntegerToByteArray.run(x, n_min);
        return B;
    }

    // ALGORITHM 4.4
    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    ByteArray
    run(BigInteger x, int n) {

        // PRECONDITIONS
        Precondition.checkNotNull(x);
        Precondition.check(Set.NN.contains(x));
        Precondition.check(IntSet.NN.contains(n));
        Precondition.check(Math.ceilDiv(x.bitLength(), Byte.SIZE) <= n);

        // PREPARATION
        var builder_B = new ByteArray.Builder(n);

        // ALGORITHM
        for (int i : IntSet.range(1, n)) {
            builder_B.set(n - i, Math.mod256(x));
            x = Math.div256(x);
        }
        var B = builder_B.build();
        return B;
    }

}
