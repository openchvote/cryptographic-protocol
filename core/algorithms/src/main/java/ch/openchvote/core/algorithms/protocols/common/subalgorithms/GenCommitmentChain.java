/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.46 from CHVote Protocol Specification
 */
public final class GenCommitmentChain extends Algorithm<Pair<Vector<BigInteger>, Vector<BigInteger>>> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters>
    Pair<Vector<BigInteger>, Vector<BigInteger>>
    run(Vector<BigInteger> bold_u_tilde, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();
        var h = securityParameters.get_h();

        // PREPARATION
        var N = bold_u_tilde.getLength();
        var builder_bold_c_hat = new Vector.Builder<BigInteger>(N);
        var builder_bold_r_hat = new Vector.Builder<BigInteger>(N);
        var builder_bold_R = new Vector.Builder<BigInteger>(N);
        var builder_bold_U = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        var R_i_minus_1 = BigInteger.ZERO;
        var U_i_minus_1 = BigInteger.ONE;
        for (int i : IntSet.range(1, N)) {
            var r_hat_i = GenRandomInteger.run(q);
            var R_i = ZZ_q.add(r_hat_i, ZZ_q.multiply(bold_u_tilde.getValue(i), R_i_minus_1));
            var U_i = ZZ_q.multiply(bold_u_tilde.getValue(i), U_i_minus_1);
            builder_bold_r_hat.set(i, r_hat_i);
            builder_bold_R.set(i, R_i);
            builder_bold_U.set(i, U_i);
            R_i_minus_1 = R_i; // preparation for next loop cycle
            U_i_minus_1 = U_i; // preparation for next loop cycle
        }
        var bold_R = builder_bold_R.build();
        var bold_U = builder_bold_U.build();
        for (int i : IntSet.range(1, N)) {
            var R_i = bold_R.getValue(i);
            var U_i = bold_U.getValue(i);
            var c_hat_i = ZZPlus_p.multiply(ZZPlus_p.pow(g, R_i), ZZPlus_p.pow(h, U_i));
            builder_bold_c_hat.set(i, c_hat_i);
        }
        var bold_c_hat = builder_bold_c_hat.build();
        var bold_r_hat = builder_bold_r_hat.build();
        return new Pair<>(bold_c_hat, bold_r_hat);
    }

}
