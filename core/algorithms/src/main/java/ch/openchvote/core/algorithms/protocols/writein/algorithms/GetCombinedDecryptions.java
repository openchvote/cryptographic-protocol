/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.25 from CHVote Protocol Specification
 */
public final class GetCombinedDecryptions extends Algorithm<Pair<Vector<BigInteger>, Matrix<BigInteger>>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Pair<Vector<BigInteger>, Matrix<BigInteger>>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters>
    Pair<Vector<BigInteger>, Matrix<BigInteger>>
    run(int z, Matrix<BigInteger> bold_C, Vector<Matrix<BigInteger>> arrow_bold_D, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_C, arrow_bold_D);
        var N = bold_C.getHeight();
        var s = bold_C.getWidth();
        Precondition.check(IntSet.NN.contains(z));
        Precondition.check(Set.Matrix(ZZPlus_p, N, s).contains(bold_C));
        Precondition.check(Set.Vector(Set.Matrix(ZZPlus_p, N, z), s).contains(arrow_bold_D));

        // PREPARATION
        var builder_bold_c = new Vector.Builder<BigInteger>(N);
        var builder_bold_D = new Matrix.Builder<BigInteger>(N, z);

        // ALGORITHM
        for (int i : IntSet.range(1, N)) {
            var bold_c_i = bold_C.getRow(i);
            var c_i = ZZPlus_p.prod(bold_c_i);
            builder_bold_c.set(i, c_i);
            for (int j : IntSet.range(1, z)) {
                var i_final = i; // effectively final variables needed
                var j_final = j; // ... in subsequent lambda expression
                var bold_d_ij = arrow_bold_D.map(matrix -> matrix.getValue(i_final, j_final));
                var d_ij = ZZPlus_p.prod(bold_d_ij);
                builder_bold_D.set(i, j, d_ij);
            }
        }
        var bold_c = builder_bold_c.build();
        var bold_D = builder_bold_D.build();
        return new Pair<>(bold_c, bold_D);
    }

}
