/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.VotingDescriptions;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

/**
 * Implementation of Algorithm 8.21 from CHVote Protocol Specification
 */
public final class GetVotingDescriptions extends Algorithm<VotingDescriptions> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<VotingDescriptions> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    VotingDescriptions
    run(int v, ElectionDescriptions ED) {

        // PRECONDITIONS
        Precondition.checkNotNull(ED);
        var D = ED.get_D();
        var bold_d_E = ED.get_bold_d_E();
        var bold_d_G = ED.get_bold_d_G();
        var bold_d_A = ED.get_bold_d_A();
        var bold_d_V = ED.get_bold_d_V();
        var bold_d_C = ED.get_bold_d_C();
        var t = bold_d_E.getLength();
        var u = bold_d_G.getLength();
        var n = bold_d_A.getLength();
        var N_E = bold_d_V.getLength();
        var w = bold_d_C.getLength();
        Precondition.check(IntSet.range(1, N_E).contains(v));
        Precondition.check(Set.UCS_star.contains(D));
        Precondition.check(Set.Vector(Set.UCS_star, t).contains(bold_d_E));
        Precondition.check(Set.Vector(Set.UCS_star, u).contains(bold_d_G));
        Precondition.check(Set.Vector(Set.UCS_star, n).contains(bold_d_A));
        Precondition.check(Set.Vector(Set.UCS_star, N_E).contains(bold_d_V));
        Precondition.check(Set.Vector(Set.UCS_star, w).contains(bold_d_C));

        // PREPARATION
        var DV_v = bold_d_V.getValue(v);

        // ALGORITHM
        var VD_v = new VotingDescriptions(D, bold_d_E, bold_d_G, bold_d_A, DV_v, bold_d_C);
        return VD_v;
    }

}
