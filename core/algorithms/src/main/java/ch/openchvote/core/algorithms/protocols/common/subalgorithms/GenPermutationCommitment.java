/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tools.Parallel;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.45 from CHVote Protocol Specification
 */
public final class GenPermutationCommitment extends Algorithm<Pair<Vector<BigInteger>, Vector<BigInteger>>> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters>
    Pair<Vector<BigInteger>, Vector<BigInteger>>
    run(IntVector psi, Vector<BigInteger> bold_h, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();

        // PREPARATION
        var N = psi.getLength();
        var builder_bold_c = new Vector.Builder<BigInteger>(N);
        var builder_bold_r = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        Parallel.forLoop(1, N, i -> {
            var j_i = psi.getValue(i);
            var r_j_i = GenRandomInteger.run(q);
            var c_j_i = ZZPlus_p.multiply(ZZPlus_p.pow(g, r_j_i), bold_h.getValue(i));
            builder_bold_r.set(j_i, r_j_i);
            builder_bold_c.set(j_i, c_j_i);
        });
        var bold_c = builder_bold_c.build();
        var bold_r = builder_bold_r.build();
        return new Pair<>(bold_c, bold_r);
    }

}
