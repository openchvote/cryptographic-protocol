/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Point;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;
import java.util.HashSet;

/**
 * Implementation of Algorithm 8.11 from CHVote Protocol Specification
 */
public final class GenPoints extends Algorithm<Pair<Vector<Point>, BigInteger>> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends GGParameters>
    Pair<Vector<Point>, BigInteger>
    run(IntVector bold_n, IntVector bold_k, IntVector bold_e_hat, SP securityParameters) {

        // SECURITY PARAMETERS
        var q_hat = securityParameters.get_q_hat();

        // PREPARATION
        var t = bold_n.getLength();
        var n = bold_n.sum();
        var builder_bold_p = new Vector.Builder<Point>(n);

        // ALGORITHM
        var k = bold_e_hat.multiply(bold_k);
        var bold_a = GenPolynomial.run(k - 1, securityParameters);
        var X = new HashSet<>(java.util.Set.of(BigInteger.ZERO));
        var n_prime = 0;
        for (int l : IntSet.range(1, t)) {
            var n_l = bold_n.getValue(l);
            for (int i : IntSet.range(n_prime + 1, n_prime + n_l)) {
                Point p_i;
                if (bold_e_hat.getValue(l) == 1) {
                    var x = GenRandomInteger.run(q_hat, X);
                    X.add(x);
                    var y = GetYValue.run(x, bold_a, securityParameters);
                    p_i = new Point(x, y);
                } else {
                    p_i = Point.ZERO_ZERO;
                }
                builder_bold_p.set(i, p_i);
            }
            n_prime = n_prime + n_l;
        }
        var y_0 = GetYValue.run(BigInteger.ZERO, bold_a, securityParameters);
        var bold_p = builder_bold_p.build();
        return new Pair<>(bold_p, y_0);
    }

}
