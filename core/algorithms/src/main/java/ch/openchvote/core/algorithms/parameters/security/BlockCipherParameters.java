/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.parameters.security;

import ch.openchvote.base.utilities.crypto.BlockCipher;

/**
 * Instances implementing this interface provide the parameters of a block cipher.
 */
public interface BlockCipherParameters extends SecurityParameters {

    /**
     * Returns the block cipher algorithm itself.
     *
     * @return The block cipher algorithm
     */
    BlockCipher getBlockCipher();

    /**
     * Returns the key length (number of bytes) of the block cipher.
     *
     * @return The key length in bytes
     */
    default int get_L_K() {
        return this.getBlockCipher().getKeyLength();
    }

    /**
     * Returns the length (number of bytes) of the initialization vector.
     *
     * @return The length of the initialization vector in bytes
     */
    default int get_L_IV() {
        return this.getBlockCipher().getIVLength();
    }

}
