/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.4 from CHVote Protocol Specification
 */
public final class GetChallenge extends Algorithm<BigInteger> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends NIZKPParameters>
    BigInteger
    run(Object y, Object t, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var twoToTheTau = securityParameters.get_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(y, t);

        // ALGORITHM
        var H = RecHash.run(y, t, securityParameters);
        var c = ByteArrayToInteger.run(H).mod(twoToTheTau);
        return c;
    }

}
