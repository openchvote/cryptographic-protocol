/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.model;

import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Quadruple;

import java.math.BigInteger;

/**
 * Model class for election card data inheriting from {@link Quadruple}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class ElectionCardData extends Quadruple<BigInteger, BigInteger, ByteArray, Vector<Point>> {

    public ElectionCardData(BigInteger x, BigInteger y, ByteArray A, Vector<Point> bold_p) {
        super(x, y, A, bold_p);
    }

    public BigInteger get_x() {
        return this.getFirst();
    }

    public BigInteger get_y() {
        return this.getSecond();
    }

    public ByteArray get_A() {
        return this.getThird();
    }

    public Vector<Point> get_bold_p() {
        return this.getFourth();
    }

}
