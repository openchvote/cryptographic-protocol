/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.CheckBallotProof;
import ch.openchvote.core.algorithms.protocols.plain.model.Ballot;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER_OR_EQUAL;

/**
 * Implementation of Algorithm 8.25 from CHVote Protocol Specification
 */
public final class CheckBallot extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Boolean> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters & GGParameters & NIZKPParameters>
    boolean
    run(int v, Ballot alpha, BigInteger pk, IntVector bold_k, IntVector bold_u, IntMatrix bold_E, Vector<BigInteger> bold_x_hat, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var ZZ_p_hat = securityParameters.get_ZZ_p_hat();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(v, alpha, pk, bold_k, bold_u, bold_E, bold_x_hat);
        var x_hat = alpha.get_x_hat();
        var bold_a = alpha.get_bold_a();
        var pi = alpha.get_pi();
        var k = bold_a.getLength();
        var t = bold_k.getLength();
        var u = bold_E.getWidth();
        var N_E = bold_E.getHeight();
        Precondition.check(IntSet.range(1, N_E).contains(v));
        Precondition.check(GG_q_hat.contains(x_hat));
        Precondition.check(Set.Vector(Set.Pair(ZZPlus_p, ZZPlus_p), k).contains(bold_a));
        Precondition.check(Set.Pair(ZZ_twoToTheTau, Set.Triple(ZZ_q_hat, ZZPlus_p, ZZ_q)).contains(pi));
        Precondition.check(ZZPlus_p.contains(pk));
        Precondition.check(Set.IntVector(IntSet.range(1, u), t).contains(bold_u));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, u).contains(bold_E));
        // for improved efficiency, we perform inexact group membership tests in ZZ_p_hat for the values of bold_x_hat
        Precondition.check(Set.Vector(ZZ_p_hat, N_E).contains(bold_x_hat) && GG_q_hat.contains(bold_x_hat.getValue(v)));

        // CONSTRAINTS
        Precondition.check(bold_u.allMatch(SMALLER_OR_EQUAL));

        // ALGORITHM
        var bold_e_hat_v = bold_E.getRow(v).expand(bold_u);
        var k_prime_v = bold_k.multiply(bold_e_hat_v);
        var x_hat_v = bold_x_hat.getValue(v);
        if (x_hat.equals(x_hat_v) && k == k_prime_v) {
            return CheckBallotProof.run(pi, x_hat, bold_a, pk, securityParameters);
        }
        return false;
    }

}
