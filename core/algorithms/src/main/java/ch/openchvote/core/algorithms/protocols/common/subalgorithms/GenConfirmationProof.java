/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.protocols.common.model.ConfirmationProof;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.34 from CHVote Protocol Specification
 */
public final class GenConfirmationProof extends Algorithm<ConfirmationProof> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends GGParameters & NIZKPParameters>
    ConfirmationProof
    run(BigInteger y, BigInteger z, BigInteger y_hat, BigInteger z_hat, SP securityParameters) {

        // SECURITY PARAMETERS
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var q_hat = securityParameters.get_q_hat();
        var g_hat = securityParameters.get_g_hat();

        // ALGORITHM
        var omega_1 = GenRandomInteger.run(q_hat);
        var omega_2 = GenRandomInteger.run(q_hat);
        var t_1 = GG_q_hat.pow(g_hat, omega_1);
        var t_2 = GG_q_hat.pow(g_hat, omega_2);
        var t = new Pair<>(t_1, t_2);
        var c = GetChallenge.run(new Pair<>(y_hat, z_hat), t, securityParameters);
        var s_1 = ZZ_q_hat.subtract(omega_1, ZZ_q_hat.multiply(c, y));
        var s_2 = ZZ_q_hat.subtract(omega_2, ZZ_q_hat.multiply(c, z));
        var s = new Pair<>(s_1, s_2);
        var pi = new ConfirmationProof(c, s);
        return pi;
    }

}
