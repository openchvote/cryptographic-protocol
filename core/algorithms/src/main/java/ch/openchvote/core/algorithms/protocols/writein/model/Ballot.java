/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.model;

import ch.openchvote.core.algorithms.protocols.common.model.BallotProof;
import ch.openchvote.core.algorithms.protocols.common.model.Query;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Quintuple;

import java.math.BigInteger;

/**
 * Model class for ballots inheriting from {@link Quintuple}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class Ballot extends Quintuple<BigInteger, Vector<Query>, BallotProof, MultiEncryption, WriteInProof> {

    public Ballot(BigInteger x_hat, Vector<Query> bold_a, BallotProof pi, MultiEncryption e_prime, WriteInProof pi_prime) {
        super(x_hat, bold_a, pi, e_prime, pi_prime);
    }

    public BigInteger get_x_hat() {
        return this.getFirst();
    }

    public Vector<Query> get_bold_a() {
        return this.getSecond();
    }

    public BallotProof get_pi() {
        return this.getThird();
    }

    public MultiEncryption get_e_prime() {
        return this.getFourth();
    }

    public WriteInProof get_pi_prime() {
        return this.getFifth();
    }

}
