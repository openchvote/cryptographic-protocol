/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.AugmentedEncryption;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.26 from CHVote Protocol Specification
 */
public final class GetVotes extends Algorithm<Pair<Vector<BigInteger>, Matrix<BigInteger>>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Pair<Vector<BigInteger>, Matrix<BigInteger>>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters>
    Pair<Vector<BigInteger>, Matrix<BigInteger>>
    run(Vector<AugmentedEncryption> bold_e_bar, Vector<BigInteger> bold_c, Vector<BigInteger> bold_c_prime, Matrix<BigInteger> bold_D, Matrix<BigInteger> bold_D_prime, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_e_bar, bold_c, bold_c_prime, bold_D, bold_D_prime);
        var N = bold_e_bar.getLength();
        var z = bold_D.getWidth();
        Precondition.check(Set.Vector(Set.Quadruple(ZZPlus_p, ZZPlus_p, Set.Vector(ZZPlus_p, z), ZZPlus_p), N).contains(bold_e_bar));
        Precondition.check(Set.Vector(ZZPlus_p, N).contains(bold_c));
        Precondition.check(Set.Vector(ZZPlus_p, N).contains(bold_c_prime));
        Precondition.check(Set.Matrix(ZZPlus_p, N, z).contains(bold_D));
        Precondition.check(Set.Matrix(ZZPlus_p, N, z).contains(bold_D_prime));

        // PREPARATION
        var builder_bold_m = new Vector.Builder<BigInteger>(N);
        var builder_bold_M = new Matrix.Builder<BigInteger>(N, z);

        // ALGORITHM
        for (int i : IntSet.range(1, N)) {
            var e_bar_i = bold_e_bar.getValue(i);
            var a_i = e_bar_i.get_a();
            var bold_a_prime = e_bar_i.get_bold_a_prime();
            var c_i = bold_c.getValue(i);
            var c_prime_i = bold_c_prime.getValue(i);
            var m_i = ZZPlus_p.multiply(a_i, ZZPlus_p.invert(ZZPlus_p.multiply(c_i, c_prime_i)));
            builder_bold_m.set(i, m_i);
            for (int j : IntSet.range(1, z)) {
                var a_prime_ij = bold_a_prime.getValue(j);
                var d_ij = bold_D.getValue(i, j);
                var d_prime_ij = bold_D_prime.getValue(i, j);
                var m_ij = ZZPlus_p.multiply(a_prime_ij, ZZPlus_p.invert(ZZPlus_p.multiply(d_ij, d_prime_ij)));
                builder_bold_M.set(i, j, m_ij);
            }
        }
        var bold_m = builder_bold_m.build();
        var bold_M = builder_bold_M.build();
        return new Pair<>(bold_m, bold_M);
    }

}
