/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Point;
import ch.openchvote.core.algorithms.protocols.common.model.Response;
import ch.openchvote.core.algorithms.protocols.common.subalgorithms.GetPointVector;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;

/**
 * Implementation of Algorithm 8.28 from CHVote Protocol Specification
 */
public final class GetPointMatrix extends Algorithm<Matrix<Point>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Matrix<Point>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    <SP extends ZZPlusParameters & GGParameters & HashParameters>
    Matrix<Point>
    run(IntVector bold_n, Vector<Response> bold_beta, IntVector bold_s, Vector<BigInteger> bold_r, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var L_M = securityParameters.get_L_M();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_n, bold_beta, bold_s, bold_r);
        var t = bold_n.getLength();
        var s = bold_beta.getLength();
        var n = bold_beta.getValue(1).get_bold_C().getHeight();
        var k = bold_s.getLength();
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.Vector(Set.Triple(Set.Vector(ZZPlus_p, k), Set.Matrix(Set.B(L_M).orNull(), n, k), ZZPlus_p), s).contains(bold_beta));
        Precondition.check(Set.IntVector(IntSet.range(1, n), k).contains(bold_s));
        Precondition.check(Set.Vector(ZZ_q, k).contains(bold_r));

        // CONSTRAINTS
        Precondition.check(n == bold_n.sum());
        Precondition.check(bold_s.allMatch(SMALLER));

        // PREPARATION
        var builder_bold_P = new Matrix.ColBuilder<Point>(k, s);

        // ALGORITHM
        for (int j : IntSet.range(1, s)) {
            var bold_p_j = GetPointVector.run(bold_beta.getValue(j), bold_s, bold_r, securityParameters);
            builder_bold_P.addCol(bold_p_j);
        }
        var bold_P = builder_bold_P.build();
        return bold_P;
    }

}
