/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.ByteArrayToInteger;
import ch.openchvote.core.algorithms.general.algorithms.GetPrimes;
import ch.openchvote.core.algorithms.general.algorithms.RecHash;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Finalization;
import ch.openchvote.core.algorithms.protocols.common.model.Point;
import ch.openchvote.core.algorithms.protocols.common.model.Response;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tools.Math;
import ch.openchvote.base.utilities.tools.Parallel;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.38 from CHVote Protocol Specification
 */
public final class GetAllPoints extends Algorithm<Vector<Point>> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters & GGParameters & HashParameters>
    Vector<Point>
    run(Response beta, Finalization delta, IntVector bold_s, Vector<BigInteger> bold_r, IntVector bold_n, IntVector bold_k, IntVector bold_e_hat, BigInteger pk, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var g = securityParameters.get_g();
        var q_hat = securityParameters.get_q_hat();
        var L_M = securityParameters.get_L_M();
        var L = securityParameters.get_L();

        // PREPARATION
        var t = bold_n.getLength();
        var n = bold_n.sum();
        var bold_b = beta.get_bold_b();
        var bold_C = beta.get_bold_C();
        var d = beta.get_d();
        var z_1 = delta.get_z_1();
        var z_2 = delta.get_z_2();
        var builder_bold_p = new Vector.Builder<Point>(n);

        // ALGORITHM
        var d_prime = ZZPlus_p.multiply(ZZPlus_p.pow(pk, z_1), ZZPlus_p.pow(g, z_2));
        if (!d_prime.equals(d)) {
            throw new Exception(Exception.Type.INCOMPATIBLE_VALUES, GetAllPoints.class);
        }
        var bold_primes = GetPrimes.run(n, securityParameters);
        var ell_M = Math.ceilDiv(L_M, L);
        for (int i : IntSet.range(1, n)) {
            builder_bold_p.set(i, Point.ZERO_ZERO);
        }
        var n_prime = 0;
        var k_prime = 0;
        for (int l : IntSet.range(1, t)) {
            var n_l = bold_n.getValue(l);
            var k_l = bold_k.getValue(l);
            if (bold_e_hat.getValue(l) == 1) {
                var builder_bold_p_prime_l = new Vector.Builder<BigInteger>(n_prime, n_prime + n_l);
                Parallel.forLoop(n_prime + 1, n_prime + n_l, i -> {
                    var p_i = bold_primes.getValue(i);
                    var p_prime_i = ZZPlus_p.pow(p_i, z_1);
                    builder_bold_p_prime_l.set(i, p_prime_i);
                });
                var bold_p_prime_l = builder_bold_p_prime_l.build();
                var builder_bold_beta_l = new Vector.Builder<BigInteger>(k_prime, k_prime + k_l);
                Parallel.forLoop(k_prime + 1, k_prime + k_l, j -> {
                    var b_j = bold_b.getValue(j);
                    var p_prime_s_j = bold_p_prime_l.getValue(bold_s.getValue(j));
                    var r_j = bold_r.getValue(j);
                    var beta_j = ZZPlus_p.multiply(b_j, ZZPlus_p.pow(d, ZZ_q.minus(r_j)), ZZPlus_p.invert(p_prime_s_j));
                    builder_bold_beta_l.set(j, beta_j);
                });
                var bold_beta_l = builder_bold_beta_l.build();
                for (int i : IntSet.range(n_prime + 1, n_prime + n_l)) {
                    var builder_bold_m_i = new Vector.Builder<ByteArray>(k_prime + 1, k_prime + k_l);
                    for (int j : IntSet.range(k_prime + 1, k_prime + k_l)) {
                        var p_prime_i = bold_p_prime_l.getValue(i);
                        var beta_j = bold_beta_l.getValue(j);
                        var k_ij = ZZPlus_p.multiply(p_prime_i, beta_j);
                        var K_ij = ByteArray.EMPTY;
                        for (int c : IntSet.range(1, ell_M)) {
                            K_ij = K_ij.concatenate(RecHash.run(k_ij, c, securityParameters));
                        }
                        K_ij = K_ij.truncate(L_M);
                        var C_ij = bold_C.getValue(i, j);
                        var M_ij = C_ij.xor(K_ij);
                        builder_bold_m_i.set(j, M_ij);
                    }
                    var bold_m_i = builder_bold_m_i.build();
                    if (!bold_m_i.isConstant()) {
                        throw new Exception(Exception.Type.INCOMPATIBLE_MATRIX, GetAllPoints.class);
                    }
                    var x_i = ByteArrayToInteger.run(bold_m_i.getValue(k_prime + 1).truncate(L_M / 2));
                    var y_i = ByteArrayToInteger.run(bold_m_i.getValue(k_prime + 1).skip(L_M / 2));
                    if (x_i.compareTo(q_hat) >= 0 || y_i.compareTo(q_hat) >= 0) {
                        throw new Exception(Exception.Type.INCOMPATIBLE_POINT, GetAllPoints.class);
                    }
                    var p_i = new Point(x_i, y_i);
                    builder_bold_p.set(i, p_i);
                }
                k_prime = k_prime + k_l;
            }
            n_prime = n_prime + n_l;
        }
        var bold_p = builder_bold_p.build();
        return bold_p;
    }

}
