/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tools.Math;

/**
 * Implementation of Algorithm 4.2 from CHVote Protocol Specification
 */
public final class SetBit extends Algorithm<ByteArray> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    ByteArray
    run(ByteArray B, int i, int b) {

        // PRECONDITIONS
        Precondition.checkNotNull(B);
        Precondition.check(IntSet.ZZ(Byte.SIZE * B.getLength()).contains(i));
        Precondition.check(IntSet.BB.contains(b));

        // ALGORITHM
        byte z;
        var j = i / Byte.SIZE;
        var x = Math.intPowerOfTwo(i % Byte.SIZE);
        if (b == 0) {
            z = Math.and(B.getByte(j), Math.intToByte(255 - x));
        } else {
            z = Math.or(B.getByte(j), Math.intToByte(x));
        }
        B = B.setByte(j, z);
        return B;
    }

}
