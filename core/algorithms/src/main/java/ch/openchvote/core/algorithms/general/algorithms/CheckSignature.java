/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.model.Signature;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tuples.Pair;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.57 from CHVote Protocol Specification
 */
public final class CheckSignature extends Algorithm<Boolean> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends GGParameters & NIZKPParameters>
    boolean
    run(Signature sigma, BigInteger pk, Object m, Object aux, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var g_hat = securityParameters.get_g_hat();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(pk, m, sigma);
        Precondition.check(Set.Pair(ZZ_twoToTheTau, ZZ_q_hat).contains(sigma));
        Precondition.check(GG_q_hat.contains(pk));

        // PREPARATION
        var c = sigma.get_c();
        var s = sigma.get_s();

        // ALGORITHM
        var t = GG_q_hat.multiply(GG_q_hat.pow(pk, c), GG_q_hat.pow(g_hat, s));
        var y = (aux == null) ? new Pair<>(pk, m) : new Triple<>(pk, m, aux);
        var c_prime = GetChallenge.run(y, t, securityParameters);
        return c.equals(c_prime);
    }

}
