/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.model.GroupParameters;
import ch.openchvote.core.algorithms.general.subalgorithms.IsNotPrime;
import ch.openchvote.base.utilities.tools.Math;

import java.math.BigInteger;
import java.util.stream.Stream;

/**
 * Implementation of Algorithm 10.1 from CHVote Protocol Specification
 */
public final class GetGroupParameters extends Algorithm<GroupParameters> {

    /**
     * Local enum class for different security levels.
     */
    public enum SecurityLevel {

        /**
         * Security Level 0: parameters taken from Section 10.2.1
         */
        ZERO(48, 32, 32),

        /**
         * Security Level 0: parameters taken from Section 10.2.2
         */
        ONE(2048, 224, 112),

        /**
         * Security Level 0: parameters taken from Section 10.2.3
         */
        TWO(3072, 256, 128);

        private final int s;
        private final int t;
        private final int kappa;

        // implicitly private constructor
        SecurityLevel(int s, int t, int kappa) {
            this.s = s;
            this.t = t;
            this.kappa = kappa;
        }

    }

    // Euler's number (digits behind the decimal point) in hexadecimal
    static private final String EULER = "B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF324E7738926CFBE5F4BF8D8D8C31D763DA06C80ABB1185EB4F7C7B5757F5958490CFD47D7C19BB42158D9554F7B46BCED55C4D79FD5F24D6613C31C3839A2DDF8A9A276BCFBFA1C877C56284DAB79CD4C2B3293D20E9E5EAF02AC60ACC93ED874422A52ECB238FEEE5AB6ADD835FD1A0753D0A8F78E537D2B95BB79D8DCAEC642C1E9F23B829B5C2780BF38737DF8BB300D01334A0D0BD8645CBFA73A6160FFE393C48CBBBCA060F0FF8EC6D31BEB5CCEED7F2F0BB088017163BC60DF45A0ECB1BCD289B06CBBFEA21AD08E1847F3F7378D56CED94640D6EF0D3D37BE67008E186D1BF275B9B241DEB64749A47DFDFB96632C3EB061B6472BBF84C26144E49C2D04C324EF10DE513D3F5114B8B5D374D93CB8879C7D52FFD72BA0AAE7277DA7BA1B4AF1488D8E836AF14865E6C37AB6876FE690B571121382AF341AFE94F77BCF06C83B8FF5675F0979074AD9A787BC5B9BD4B0C5937D3EDE4C3A79396215EDA";

    // some fixed numbers used in the algorithm
    static private final BigInteger SIX = BigInteger.valueOf(6);
    static private final BigInteger TWELVE = BigInteger.valueOf(12);

    @SuppressWarnings("MissingJavadoc")
    static public // direct translation from pseudocode
    GroupParameters
    run(int s, int t, int kappa, int ub) {

        // PRECONDITIONS
        Precondition.check(2 <= t && 4 <= s && t < s && s <= 4 * EULER.length());

        // ALGORITHM
        var P = GetPrimes.run(3, ub);
        var e = new BigInteger(EULER.substring(0, Math.ceilDiv(s, 4)), 16).shiftRight((-s) % 4);
        var p = e.subtract(e.mod(TWELVE)).subtract(BigInteger.ONE);
        var q = Math.div2(p);
        do {
            do {
                p = p.add(TWELVE);
                q = q.add(SIX);
            } while (IsNotPrime.run(p, P) || IsNotPrime.run(q, P));
        } while (!p.isProbablePrime(kappa) || !q.isProbablePrime(kappa));
        var e_hat = new BigInteger(EULER.substring(0, Math.ceilDiv(t, 4)), 16).shiftRight((-t) % 4);
        var q_hat = e_hat.subtract(e_hat.mod(BigInteger.TWO)).subtract(BigInteger.ONE);
        do {
            do {
                q_hat = q_hat.add(BigInteger.TWO);
            } while (IsNotPrime.run(q_hat, P));
        } while (!q_hat.isProbablePrime(kappa));
        var q_hat_times_2 = Math.mult2(q_hat);
        var p_hat = q_hat_times_2.multiply(e.divide(q_hat_times_2)).add(BigInteger.ONE);
        do {
            do {
                p_hat = p_hat.add(q_hat_times_2);
            } while (IsNotPrime.run(p_hat, P));
        } while (!p_hat.isProbablePrime(kappa));
        var k_hat = p_hat.divide(q_hat);
        var g_hat = BigInteger.TWO.modPow(k_hat, p_hat);
        return new GroupParameters(p, q, p_hat, q_hat, k_hat, g_hat);
    }

    @SuppressWarnings("MissingJavadoc")
    static public // optimized version using Java parallel streams
    GroupParameters
    run_parallel(int s, int t, int kappa, int ub) {

        // PRECONDITIONS
        Precondition.check(2 <= t && 4 <= s && t < s && s <= 4 * EULER.length());

        // ALGORITHM
        var P = GetPrimes.run(3, ub);
        var e = new BigInteger(EULER.substring(0, Math.ceilDiv(s, 4)), 16).shiftRight((-s) % 4);
        var p_init = e.subtract(e.mod(TWELVE)).subtract(BigInteger.ONE);
        var p = Stream.iterate(p_init.add(TWELVE), x -> x.add(TWELVE))
                .parallel()
                .filter(x -> !IsNotPrime.run(x, P))
                .filter(x -> !IsNotPrime.run(x.divide(BigInteger.TWO), P))
                .filter(x -> x.isProbablePrime(kappa))
                .filter(x -> x.divide(BigInteger.TWO).isProbablePrime(kappa))
                .findFirst().orElseThrow();
        var q = Math.div2(p);
        var e_hat = new BigInteger(EULER.substring(0, Math.ceilDiv(t, 4)), 16).shiftRight((-t) % 4);
        var q_hat_init = e_hat.subtract(e_hat.mod(BigInteger.TWO)).subtract(BigInteger.ONE);
        var q_hat = Stream.iterate(q_hat_init.add(BigInteger.TWO), x -> x.add(BigInteger.TWO))
                .parallel()
                .filter(x -> x.testBit(0))
                .filter(x -> !IsNotPrime.run(x, P))
                .filter(x -> x.isProbablePrime(kappa))
                .findFirst().orElseThrow();
        var q_hat_times_2 = Math.mult2(q_hat);
        var p_hat_init = q_hat_times_2.multiply(e.divide(q_hat_times_2)).add(BigInteger.ONE);
        var p_hat = Stream.iterate(p_hat_init.add(q_hat_times_2), x -> x.add(q_hat_times_2))
                .parallel()
                .filter(x -> !IsNotPrime.run(x, P))
                .filter(x -> x.isProbablePrime(kappa))
                .findFirst().orElseThrow();
        var k_hat = p_hat.divide(q_hat);
        var g_hat = BigInteger.TWO.modPow(k_hat, p_hat);
        return new GroupParameters(p, q, p_hat, q_hat, k_hat, g_hat);
    }

    /**
     * Main method for computing the group parameters for the given security levels.
     *
     * @param args The given string arguments (ignored)
     */
    @SuppressWarnings("ConstantValue")
    static public void main(String[] args) {

        var parallel = true; // parallel or serial execution
        var ub = 2000; // largest small prime in sieve

        for (SecurityLevel securityLevel : SecurityLevel.values()) {
            System.out.println("SECURITY_LEVEL = " + securityLevel);
            System.out.println("#####################");
            long start = System.currentTimeMillis();
            var parameters = parallel
                    ? GetGroupParameters.run_parallel(securityLevel.s, securityLevel.t, securityLevel.kappa, ub)
                    : GetGroupParameters.run(securityLevel.s, securityLevel.t, securityLevel.kappa, ub);
            long end = System.currentTimeMillis();
            System.out.printf("Computation time: %d milliseconds%n", end - start);
            System.out.println("p = " + parameters.get_p().toString(16).toUpperCase());
            System.out.println("q = " + parameters.get_q().toString(16).toUpperCase());
            System.out.println("p_hat = " + parameters.get_p_hat().toString(16).toUpperCase());
            System.out.println("q_hat = " + parameters.get_q_hat().toString(16).toUpperCase());
            System.out.println("k_hat = " + parameters.get_k_hat().toString(16).toUpperCase());
            System.out.println("g_hat = " + parameters.get_g_hat().toString(16).toUpperCase());
            System.out.println();
        }
    }

}