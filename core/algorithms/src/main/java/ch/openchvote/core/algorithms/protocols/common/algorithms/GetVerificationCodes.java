/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.ByteArrayToString;
import ch.openchvote.core.algorithms.general.algorithms.RecHash;
import ch.openchvote.core.algorithms.general.algorithms.SetWatermark;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Point;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;

/**
 * Implementation of Algorithm 8.30 from CHVote Protocol Specification
 */
public final class GetVerificationCodes extends Algorithm<Vector<String>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Vector<String>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    <SP extends GGParameters & HashParameters, UP extends CodeParameters>
    Vector<String>
    run(IntVector bold_s, Matrix<Point> bold_P, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();

        // USABILITY PARAMETERS
        Precondition.checkNotNull(usabilityParameters);
        var A_V = usabilityParameters.get_A_V();
        var L_V = usabilityParameters.get_L_V();
        var n_max = usabilityParameters.get_n_max();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_s, bold_P);
        var k = bold_P.getHeight();
        var s = bold_P.getWidth();
        Precondition.check(Set.IntVector(IntSet.range(1, n_max), k).contains(bold_s));
        Precondition.check(Set.Matrix(Set.Pair(ZZ_q_hat, ZZ_q_hat), k, s).contains(bold_P));

        // CONSTRAINTS
        Precondition.check(bold_s.allMatch(SMALLER));

        // PREPARATION
        var builder_bold_vc = new Vector.Builder<String>(k);

        // ALGORITHM
        for (int i : IntSet.range(1, k)) {
            var builder_bold_v_i = new Vector.Builder<ByteArray>(s);
            for (int j : IntSet.range(1, s)) {
                var p_ij = bold_P.getValue(i, j);
                var V_ij = RecHash.run(p_ij, securityParameters).truncate(L_V);
                builder_bold_v_i.set(j, V_ij);
            }
            var bold_v_i = builder_bold_v_i.build();
            var V_i = ByteArray.xor(bold_v_i, L_V);
            V_i = SetWatermark.run(V_i, bold_s.getValue(i) - 1, n_max);
            var VC_i = ByteArrayToString.run(V_i, A_V);
            builder_bold_vc.set(i, VC_i);
        }
        var bold_vc = builder_bold_vc.build();
        return bold_vc;
    }

}
