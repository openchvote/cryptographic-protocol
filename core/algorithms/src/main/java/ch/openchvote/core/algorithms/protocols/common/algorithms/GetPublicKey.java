/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.9 from CHVote Protocol Specification
 */
public final class GetPublicKey extends Algorithm<BigInteger> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<BigInteger> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters>
    BigInteger
    run(Vector<BigInteger> bold_pk, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_pk);
        var s = bold_pk.getLength() - 1;
        Precondition.check(s >= 0);
        Precondition.check(Set.Vector(ZZPlus_p, s + 1).contains(bold_pk));

        // ALGORITHM
        var pk = ZZPlus_p.prod(bold_pk);
        return pk;
    }

}
