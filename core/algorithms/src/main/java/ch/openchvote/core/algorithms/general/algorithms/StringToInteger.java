/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.set.Alphabet;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 4.8 from CHVote Protocol Specification
 */
public final class StringToInteger extends Algorithm<BigInteger> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    BigInteger
    run(String S, Alphabet A) {

        // PRECONDITIONS
        Precondition.checkNotNull(S, A);
        Precondition.check(Set.String(A).contains(S));

        // ALGORITHM
        var N = BigInteger.valueOf(A.getSize());
        var x = BigInteger.ZERO;
        for (int i : IntSet.range(0, S.length() - 1)) {
            x = x.multiply(N).add(BigInteger.valueOf(A.getRank(S.charAt(i))));
        }
        return x;
    }

}