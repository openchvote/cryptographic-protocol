/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.model;

import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Model class for write-in proofs inheriting from {@link Pair}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class WriteInProof extends Pair<Matrix<BigInteger>, Matrix<BigInteger>> {

    public WriteInProof(Matrix<BigInteger> bold_C, Matrix<BigInteger> bold_S) {
        super(bold_C, bold_S);
    }

    public Matrix<BigInteger> get_bold_C() {
        return this.getFirst();
    }

    public Matrix<BigInteger> get_bold_S() {
        return this.getSecond();
    }
}
