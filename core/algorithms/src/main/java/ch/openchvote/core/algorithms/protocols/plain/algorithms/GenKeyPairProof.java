/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.plain.model.KeyPairProof;
import ch.openchvote.base.utilities.serializer.TypeReference;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.7 from CHVote Protocol Specification
 */
public final class GenKeyPairProof extends Algorithm<KeyPairProof> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<KeyPairProof> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    <SP extends ZZPlusParameters & NIZKPParameters>
    KeyPairProof
    run(BigInteger sk, BigInteger pk, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();

        // PRECONDITIONS
        Precondition.checkNotNull(sk, pk);
        Precondition.check(ZZ_q.contains(sk));
        Precondition.check(ZZPlus_p.contains(pk));

        // ALGORITHM
        var omega = GenRandomInteger.run(q);
        var t = ZZPlus_p.pow(g, omega);
        var c = GetChallenge.run(pk, t, securityParameters);
        var s = ZZ_q.subtract(omega, ZZ_q.multiply(c, sk));
        var pi = new KeyPairProof(c, s);
        return pi;
    }

}
