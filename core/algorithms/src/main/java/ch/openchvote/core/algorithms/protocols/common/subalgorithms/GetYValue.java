/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.13 from CHVote Protocol Specification
 */
public final class GetYValue extends Algorithm<BigInteger> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends GGParameters>
    BigInteger
    run(BigInteger x, Vector<BigInteger> bold_a, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();

        // PREPARATION
        var d = bold_a.getLength() - 1;

        // ALGORITHM
        BigInteger y;
        if (x.equals(BigInteger.ZERO)) {
            y = bold_a.getValue(0);
        } else {
            y = BigInteger.ZERO;
            for (int i : IntSet.range(0, d)) {
                y = ZZ_q_hat.add(bold_a.getValue(d - i), ZZ_q_hat.multiply(x, y));
            }
        }
        return y;
    }

}
