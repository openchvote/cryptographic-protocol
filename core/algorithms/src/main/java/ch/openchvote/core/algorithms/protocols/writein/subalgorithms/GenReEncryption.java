/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.AugmentedEncryption;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.19 from CHVote Protocol Specification
 */
public final class GenReEncryption extends Algorithm<Triple<AugmentedEncryption, BigInteger, BigInteger>> {

    @SuppressWarnings("MissingJavadoc")
    static public //
    <SP extends ZZPlusParameters>
    Triple<AugmentedEncryption, BigInteger, BigInteger>
    run(AugmentedEncryption e_bar, BigInteger pk, Vector<BigInteger> bold_pk_prime, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();

        // PREPARATION
        var z = bold_pk_prime.getLength();
        var a = e_bar.get_a();
        var b = e_bar.get_b();
        var bold_a_prime = e_bar.get_bold_a_prime();
        var b_prime = e_bar.get_b_prime();
        var builder_bold_a_tilde_prime = new Vector.Builder<BigInteger>(z);

        // ALGORITHM
        var r_tilde = GenRandomInteger.run(q);
        var r_tilde_prime = GenRandomInteger.run(q);
        var a_tilde = ZZPlus_p.multiply(a, ZZPlus_p.pow(pk, r_tilde));
        var b_tilde = ZZPlus_p.multiply(b, ZZPlus_p.pow(g, r_tilde));
        for (int i : IntSet.range(1, z)) {
            var a_prime_i = bold_a_prime.getValue(i);
            var pk_prime_i = bold_pk_prime.getValue(i);
            var a_tilde_prime_i = ZZPlus_p.multiply(a_prime_i, ZZPlus_p.pow(pk_prime_i, r_tilde_prime));
            builder_bold_a_tilde_prime.set(i, a_tilde_prime_i);
        }
        var b_tilde_prime = ZZPlus_p.multiply(b_prime, ZZPlus_p.pow(g, r_tilde_prime));
        var bold_a_tilde_prime = builder_bold_a_tilde_prime.build();
        var e_tilde = new AugmentedEncryption(a_tilde, b_tilde, bold_a_tilde_prime, b_tilde_prime);
        return new Triple<>(e_tilde, r_tilde, r_tilde_prime);
    }

}
