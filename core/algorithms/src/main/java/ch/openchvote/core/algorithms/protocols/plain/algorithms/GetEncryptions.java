/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetPrimes;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Confirmation;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.core.algorithms.protocols.plain.model.Ballot;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IndexedFamily;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;
import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER_OR_EQUAL;

/**
 * Implementation of Algorithm 8.40 from CHVote Protocol Specification
 */
public final class GetEncryptions extends Algorithm<Vector<Encryption>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Vector<Encryption>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc"})
    static public //
    <SP extends ZZPlusParameters & GGParameters & NIZKPParameters>
    Vector<Encryption>
    run(IndexedFamily<Ballot> B, IndexedFamily<Confirmation> C, IntVector bold_n, IntVector bold_k, IntVector bold_u, IntVector bold_w, IntMatrix bold_E, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(B, C, bold_n, bold_k, bold_E, bold_w);
        var t = bold_n.getLength();
        var u = bold_E.getWidth();
        var N_E = bold_E.getHeight();
        var k_max = bold_E.select(bold_u).multiply(bold_k).maxOrZero();
        Precondition.check(Set.IndexedFamily(IntSet.range(1, N_E), Set.Triple(GG_q_hat, Set.Vector(Set.Pair(ZZPlus_p, ZZPlus_p), 0, k_max), Set.Pair(ZZ_twoToTheTau, Set.Triple(ZZ_q_hat, ZZPlus_p, ZZ_q)))).contains(B));
        Precondition.check(Set.IndexedFamily(IntSet.range(1, N_E), Set.Triple(GG_q_hat, GG_q_hat, Set.Pair(ZZ_twoToTheTau, Set.Pair(ZZ_q_hat, ZZ_q_hat)))).contains(C));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntVector(IntSet.range(1, u), t).contains(bold_u));
        Precondition.check(Set.IntVector(IntSet.NN_plus, N_E).contains(bold_w));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, u).contains(bold_E));

        // CONSTRAINTS
        Precondition.check(bold_u.allMatch(SMALLER_OR_EQUAL));
        Precondition.check(IntVector.allMatch(bold_k, bold_n, SMALLER));

        // PREPARATION
        var builder_bold_e_bar = new Vector.Builder<Encryption>();

        // ALGORITHM
        var n = bold_n.sum();
        var w = bold_w.maxOrZero();
        var bold_p = GetPrimes.run(n + w, securityParameters);
        var i = 1;
        for (var ballot : B) {
            var v = ballot.getFirst();
            var alpha = ballot.getSecond();
            if (C.containsIndex(v)) {
                var bold_e = alpha.getSecond();
                var w_v = bold_w.getValue(v);
                var k_prime = 0;
                for (int k : IntSet.range(1, u)) {
                    if (bold_u.contains(k) && bold_E.getValue(v, k) == 1) {
                        var a = bold_p.getValue(n + w_v);
                        var b = BigInteger.ONE;
                        for (int l : IntSet.range(1, t)) {
                            if (bold_u.getValue(l) == k) {
                                var k_l = bold_k.getValue(l);
                                for (int j : IntSet.range(k_prime + 1, k_prime + k_l)) {
                                    var e_j = bold_e.getValue(j);
                                    a = ZZPlus_p.multiply(a, e_j.get_a());
                                    b = ZZPlus_p.multiply(b, e_j.get_b());
                                }
                                k_prime = k_prime + k_l;
                            }
                        }
                        var e_bar_i = new Encryption(a, b);
                        builder_bold_e_bar.set(i, e_bar_i);
                        i = i + 1;
                    }
                }
            }
        }
        var bold_e_bar = builder_bold_e_bar.build();
        bold_e_bar = Vector.sort(bold_e_bar);
        return bold_e_bar;
    }

}
