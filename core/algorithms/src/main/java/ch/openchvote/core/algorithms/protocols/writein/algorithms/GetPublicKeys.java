/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetPublicKey;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.6 from CHVote Protocol Specification
 */
public final class GetPublicKeys extends Algorithm<Vector<BigInteger>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Vector<BigInteger>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters>
    Vector<BigInteger>
    run(Matrix<BigInteger> bold_PK_prime, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_PK_prime);
        var z = bold_PK_prime.getHeight();
        var s = bold_PK_prime.getWidth() - 1;
        Precondition.check(s >= 0);
        Precondition.check(Set.Matrix(ZZPlus_p, z, s + 1).contains(bold_PK_prime));

        // PREPARATION
        var builder_bold_pk_prime = new Vector.Builder<BigInteger>(z);

        // ALGORITHM
        for (int i : IntSet.range(1, z)) {
            var bold_pk_prime_i = bold_PK_prime.getRow(i);
            var pk_prime_i = GetPublicKey.run(bold_pk_prime_i, securityParameters);
            builder_bold_pk_prime.set(i, pk_prime_i);
        }
        var bold_pk_prime = builder_bold_pk_prime.build();
        return bold_pk_prime;
    }

}
