/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetPrimes;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Confirmation;
import ch.openchvote.core.algorithms.protocols.writein.model.AugmentedEncryption;
import ch.openchvote.core.algorithms.protocols.writein.model.Ballot;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteInProof;
import ch.openchvote.base.utilities.sequence.IntMatrix;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IndexedFamily;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER;
import static ch.openchvote.base.utilities.tools.IntBiPredicate.SMALLER_OR_EQUAL;

/**
 * Implementation of Algorithm 9.17 from CHVote Protocol Specification
 */
public final class GetEncryptions extends Algorithm<Vector<AugmentedEncryption>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Vector<AugmentedEncryption>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc"})
    static public //
    <SP extends ZZPlusParameters & GGParameters & NIZKPParameters>
    Vector<AugmentedEncryption>
    run(IndexedFamily<Ballot> B, IndexedFamily<Confirmation> C, IntVector bold_n, IntVector bold_k, IntVector bold_u, IntVector bold_w, IntMatrix bold_E, IntVector bold_z, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(B, C, bold_n, bold_k, bold_u, bold_w, bold_E, bold_z);
        var t = bold_n.getLength();
        var u = bold_E.getWidth();
        var N_E = bold_E.getHeight();
        var bold_E_hat = bold_E.select(bold_u);
        var k_max = bold_E_hat.multiply(bold_k).maxOrZero();
        var z_max = bold_E_hat.multiply(bold_z.times(bold_k)).maxOrZero();
        Precondition.check(Set.IndexedFamily(IntSet.range(1, N_E), Set.Quintuple(GG_q_hat, Set.Vector(Set.Pair(ZZPlus_p, ZZPlus_p), 0, k_max), Set.Pair(ZZ_twoToTheTau, Set.Triple(ZZ_q_hat, ZZPlus_p, ZZ_q)), Set.Pair(Set.Vector(ZZPlus_p, 0, z_max), ZZPlus_p), Set.universal(WriteInProof.class))).contains(B));
        Precondition.check(Set.IndexedFamily(IntSet.range(1, N_E), Set.Triple(GG_q_hat, GG_q_hat, Set.Pair(ZZ_twoToTheTau, Set.Pair(ZZ_q_hat, ZZ_q_hat)))).contains(C));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntVector(IntSet.range(1, u), t).contains(bold_u));
        Precondition.check(Set.IntVector(IntSet.NN_plus, N_E).contains(bold_w));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, u).contains(bold_E));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_z));

        // CONSTRAINTS
        Precondition.check(bold_u.allMatch(SMALLER_OR_EQUAL));
        Precondition.check(IntVector.allMatch(bold_k, bold_n, SMALLER));

        // PREPARATION
        var builder_bold_e_hat = new Vector.Builder<AugmentedEncryption>();

        // ALGORITHM
        var n = bold_n.sum();
        var w = bold_w.maxOrZero();
        var bold_p = GetPrimes.run(n + w, securityParameters);
        var i = 1;
        for (var ballot : B) {
            var v = ballot.getFirst();
            var alpha = ballot.getSecond();
            if (C.containsIndex(v)) {
                var bold_e = alpha.get_bold_a();
                var e_prime = alpha.get_e_prime();
                var bold_a_prime = e_prime.get_bold_a();
                var b_prime = e_prime.get_b();
                var w_v = bold_w.getValue(v);
                int k_prime = 0;
                int z_prime = 0;
                for (int k : IntSet.range(1, u)) {
                    if (bold_u.contains(k) && bold_E.getValue(v, k) == 1) {
                        var a = bold_p.getValue(n + w_v);
                        var b = BigInteger.ONE;
                        var builder_bold_a_star = new Vector.Builder<BigInteger>(z_max);
                        builder_bold_a_star.fill(BigInteger.ONE);
                        for (int l : IntSet.range(1, t)) {
                            if (bold_u.getValue(l) == k) {
                                int k_l = bold_k.getValue(l);
                                for (int j : IntSet.range(k_prime + 1, k_prime + k_l)) {
                                    var a_j = bold_e.getValue(j);
                                    a = ZZPlus_p.multiply(a, a_j.get_a_1());
                                    b = ZZPlus_p.multiply(b, a_j.get_a_2());
                                }
                                k_prime = k_prime + k_l;
                                if (bold_z.getValue(l) == 1) {
                                    for (int j : IntSet.range(z_prime + 1, z_prime + k_l)) {
                                        var a_prime_j = bold_a_prime.getValue(j);
                                        builder_bold_a_star.set(j, a_prime_j);
                                    }
                                    z_prime = z_prime + k_l;
                                }
                            }
                        }
                        var bold_a_star = builder_bold_a_star.build();
                        var e_hat_i = new AugmentedEncryption(a, b, bold_a_star, b_prime);
                        builder_bold_e_hat.set(i, e_hat_i);
                        i = i + 1;
                    }
                }
            }
        }
        var bold_e_hat = builder_bold_e_hat.build();
        bold_e_hat = Vector.sort(bold_e_hat);
        return bold_e_hat;
    }

}
