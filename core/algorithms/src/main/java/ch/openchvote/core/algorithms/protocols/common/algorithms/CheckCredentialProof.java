/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.protocols.common.model.CredentialProof;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tools.Parallel;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.16 from CHVote Protocol Specification
 */
public final class CheckCredentialProof extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Boolean> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination", "BooleanMethodIsAlwaysInverted"})
    static public //
    <SP extends GGParameters & NIZKPParameters>
    boolean
    run(CredentialProof pi, Vector<BigInteger> bold_x_hat, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var g_hat = securityParameters.get_g_hat();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();

        // PRECONDITIONS
        Precondition.checkNotNull(pi, bold_x_hat, bold_y_hat, bold_z_hat);
        var c = pi.get_c();
        var bold_s = pi.get_bold_s();
        var N_E = bold_s.getLength();
        Precondition.check(Set.Pair(ZZ_twoToTheTau, Set.Vector(Set.Triple(ZZ_q_hat, ZZ_q_hat, ZZ_q_hat), N_E)).contains(pi));
        Precondition.check(Set.Vector(GG_q_hat, N_E).contains(bold_x_hat));
        Precondition.check(Set.Vector(GG_q_hat, N_E).contains(bold_y_hat));
        Precondition.check(Set.Vector(GG_q_hat, N_E).contains(bold_z_hat));

        // PREPARATION
        var builder_bold_t = new Vector.Builder<Triple<BigInteger, BigInteger, BigInteger>>(N_E);

        // ALGORITHM
        Parallel.forLoop(1, N_E, v -> {
            var s_v = bold_s.getValue(v);
            var s_v_1 = s_v.getFirst();
            var s_v_2 = s_v.getSecond();
            var s_v_3 = s_v.getThird();
            var x_hat_v = bold_x_hat.getValue(v);
            var y_hat_v = bold_y_hat.getValue(v);
            var z_hat_v = bold_z_hat.getValue(v);
            var t_v_1 = GG_q_hat.multiply(GG_q_hat.pow(x_hat_v, c), GG_q_hat.pow(g_hat, s_v_1));
            var t_v_2 = GG_q_hat.multiply(GG_q_hat.pow(y_hat_v, c), GG_q_hat.pow(g_hat, s_v_2));
            var t_v_3 = GG_q_hat.multiply(GG_q_hat.pow(z_hat_v, c), GG_q_hat.pow(g_hat, s_v_3));
            var t_v = new Triple<>(t_v_1, t_v_2, t_v_3);
            builder_bold_t.set(v, t_v);
        });
        var bold_t = builder_bold_t.build();
        var y = new Triple<>(bold_x_hat, bold_y_hat, bold_z_hat);
        var c_prime = GetChallenge.run(y, bold_t, securityParameters);
        return c.equals(c_prime);
    }
}
