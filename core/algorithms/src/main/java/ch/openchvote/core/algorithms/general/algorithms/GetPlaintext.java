/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.general.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.model.Ciphertext;
import ch.openchvote.core.algorithms.parameters.security.BlockCipherParameters;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.HashParameters;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.set.Set;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.61 from CHVote Protocol Specification
 */
public final class GetPlaintext extends Algorithm<ByteArray> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends GGParameters & HashParameters & BlockCipherParameters>
    ByteArray
    run(BigInteger sk, Ciphertext e, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var AES = securityParameters.getBlockCipher(); // AES-GCM-128
        var L_K = securityParameters.get_L_K(); // 16 for AES-GCM-128
        var L_IV = securityParameters.get_L_IV(); // 12 for AES-GCM-128

        // PRECONDITIONS
        Precondition.checkNotNull(sk, e);
        Precondition.check(ZZ_q_hat.contains(sk));
        Precondition.check(Set.Triple(GG_q_hat, Set.B(L_IV), Set.B_star).contains(e));

        // PREPARATION
        var ek = e.get_ek();
        var IV = e.get_IV();
        var C = e.get_C();

        // ALGORITHM
        var K = RecHash.run(GG_q_hat.pow(ek, sk), securityParameters).truncate(L_K);
        var M = AES.decrypt(K, IV, C);
        return M;
    }

}
