/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.model;

import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Sextuple;

import java.math.BigInteger;

/**
 * Model class for the election authority's public data inheriting from {@link Sextuple}, with specific constructor and
 * getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class PublicDataAdministrator extends Sextuple<BigInteger, Vector<BigInteger>, KeyPairProof, Vector<BigInteger>, Matrix<BigInteger>, DecryptionProof> {

    public PublicDataAdministrator(BigInteger pk, Vector<BigInteger> bold_pk_prime, KeyPairProof pi, Vector<BigInteger> bold_c, Matrix<BigInteger> bold_D, DecryptionProof pi_prime) {
        super(pk, bold_pk_prime, pi, bold_c, bold_D, pi_prime);
    }

    public BigInteger get_pk() {
        return this.getFirst();
    }

    public Vector<BigInteger> get_bold_pk_prime() {
        return this.getSecond();
    }

    public KeyPairProof get_pi() {
        return this.getThird();
    }

    public Vector<BigInteger> get_bold_c() {
        return this.getFourth();
    }

    public Matrix<BigInteger> get_bold_D() {
        return this.getFifth();
    }

    public DecryptionProof get_pi_prime() {
        return this.getSixth();
    }

}
