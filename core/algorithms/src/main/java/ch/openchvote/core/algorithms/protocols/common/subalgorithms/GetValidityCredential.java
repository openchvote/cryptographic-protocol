/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Point;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.33 from CHVote Protocol Specification
 */
public final class GetValidityCredential extends Algorithm<BigInteger> {

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination"})
    static public //
    <SP extends GGParameters>
    BigInteger
    run(Vector<Point> bold_p, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();

        // PREPARATION
        var k = bold_p.getLength();

        // ALGORITHM
        var z = BigInteger.ZERO;
        for (int i : IntSet.range(1, k)) {
            var n = BigInteger.ONE;
            var d = BigInteger.ONE;
            var p_i = bold_p.getValue(i);
            var x_i = p_i.get_x();
            var y_i = p_i.get_y();
            for (int j : IntSet.range(1, k)) {
                if (i != j) {
                    var p_j = bold_p.getValue(j);
                    var x_j = p_j.get_x();
                    n = ZZ_q_hat.multiply(n, x_j);
                    d = ZZ_q_hat.multiply(d, ZZ_q_hat.subtract(x_j, x_i));
                }
            }
            z = ZZ_q_hat.add(z, ZZ_q_hat.multiply(y_i, ZZ_q_hat.divide(n, d)));
        }
        return z;
    }

}
