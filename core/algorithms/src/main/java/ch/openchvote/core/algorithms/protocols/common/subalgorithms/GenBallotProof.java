/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.BallotProof;
import ch.openchvote.core.algorithms.protocols.common.model.Query;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.24 from CHVote Protocol Specification
 */
public final class GenBallotProof extends Algorithm<BallotProof> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "SuspiciousNameCombination"})
    static public //
    <SP extends ZZPlusParameters & GGParameters & NIZKPParameters>
    BallotProof
    run(BigInteger x, BigInteger m, BigInteger r, BigInteger x_hat, Vector<Query> bold_a, BigInteger pk, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();
        var q_hat = securityParameters.get_q_hat();
        var g_hat = securityParameters.get_g_hat();

        // ALGORITHM
        var omega_1 = GenRandomInteger.run(q_hat);
        var omega_2 = GenRandomInteger.run(BigInteger.ONE, q);
        var omega_3 = GenRandomInteger.run(q);
        var t_1 = GG_q_hat.pow(g_hat, omega_1);
        var t_2 = ZZPlus_p.multiply(omega_2, ZZPlus_p.pow(pk, omega_3));
        var t_3 = ZZPlus_p.pow(g, omega_3);
        var t = new Triple<>(t_1, t_2, t_3);
        var y = new Triple<>(x_hat, bold_a, pk);
        var c = GetChallenge.run(y, t, securityParameters);
        var s_1 = ZZ_q_hat.subtract(omega_1, ZZ_q_hat.multiply(c, x));
        var s_2 = ZZPlus_p.multiply(omega_2, ZZPlus_p.pow(m, ZZ_q.minus(c)));
        var s_3 = ZZ_q.subtract(omega_3, ZZ_q.multiply(c, r));
        var s = new Triple<>(s_1, s_2, s_3);
        var pi = new BallotProof(c, s);
        return pi;
    }

}
