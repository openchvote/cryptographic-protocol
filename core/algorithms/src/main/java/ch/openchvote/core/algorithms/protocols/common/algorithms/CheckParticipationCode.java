/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.Alphabet;

/**
 * Implementation of Algorithm 8.39 from CHVote Protocol Specification
 */
public final class CheckParticipationCode extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Boolean> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "BooleanMethodIsAlwaysInverted", "unused"})
    static public //
    boolean
    run(String PC, String PC_prime) {

        // PRECONDITIONS
        Precondition.checkNotNull(PC, PC_prime);
        Precondition.check(Alphabet.UCS_star.contains(PC));
        Precondition.check(Alphabet.UCS_star.contains(PC_prime));

        // ALGORITHM
        return PC.equals(PC_prime);
    }

}
