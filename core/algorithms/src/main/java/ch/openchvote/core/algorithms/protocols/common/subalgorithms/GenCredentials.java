/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.base.utilities.tuples.Quintuple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 8.14 from CHVote Protocol Specification
 */
public final class GenCredentials extends Algorithm<Quintuple<BigInteger, BigInteger, BigInteger, BigInteger, BigInteger>> {

    @SuppressWarnings({"MissingJavadoc", "SuspiciousNameCombination"})
    static public //
    <SP extends GGParameters>
    Quintuple<BigInteger, BigInteger, BigInteger, BigInteger, BigInteger>
    run(BigInteger z, SP securityParameters) {

        // SECURITY PARAMETERS
        var GG_q_hat = securityParameters.get_GG_q_hat();
        var q_hat = securityParameters.get_q_hat();
        var g_hat = securityParameters.get_g_hat();

        // ALGORITHM
        var x = GenRandomInteger.run(q_hat);
        var y = GenRandomInteger.run(q_hat);
        var x_hat = GG_q_hat.pow(g_hat, x);
        var y_hat = GG_q_hat.pow(g_hat, y);
        var z_hat = GG_q_hat.pow(g_hat, z);
        return new Quintuple<>(x, y, x_hat, y_hat, z_hat);
    }

}
