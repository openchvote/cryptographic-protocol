/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.model;

import ch.openchvote.base.utilities.tuples.Pair;

/**
 * Model class for write-ins inheriting from {@link Pair}, with specific constructor and getter methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class WriteIn extends Pair<String, String> implements Comparable<WriteIn> {

    /**
     * An empty write-in consisting of two empty strings.
     */
    static public final WriteIn EMPTY = new WriteIn("", "");

    public WriteIn(String S1, String S2) {
        super(S1, S2);
    }

    public String get_S_1() {
        return this.getFirst();
    }

    public String get_S_2() {
        return this.getSecond();
    }

    @Override
    public String toString() {
        return this.get_S_1() + "_" + this.get_S_2();
    }

    @Override
    public int compareTo(WriteIn other) {
        var c = this.get_S_1().compareTo(other.get_S_1());
        return c == 0 ? this.get_S_2().compareTo(other.get_S_2()) : c;
    }

}
