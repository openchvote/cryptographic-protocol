/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.common.model;

import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Sextuple;

/**
 * Model class for election descriptions inheriting from {@link Sextuple}, with specific constructor and getter
 * methods.
 */
@SuppressWarnings("MissingJavadoc")
public final class ElectionDescriptions extends Sextuple<String, Vector<String>, Vector<String>, Vector<String>, Vector<String>, Vector<String>> implements ElectionParameters {

    public ElectionDescriptions(String D, Vector<String> bold_d_E, Vector<String> bold_d_G, Vector<String> bold_d_A, Vector<String> bold_d_V, Vector<String> bold_d_C) {
        super(D, bold_d_E, bold_d_G, bold_d_A, bold_d_V, bold_d_C);
    }

    public String get_D() {
        return this.getFirst();
    }

    public Vector<String> get_bold_d_E() {
        return this.getSecond();
    }

    public Vector<String> get_bold_d_G() {
        return this.getThird();
    }

    public Vector<String> get_bold_d_A() {
        return this.getFourth();
    }

    public Vector<String> get_bold_d_V() {
        return this.getFifth();
    }

    public Vector<String> get_bold_d_C() {
        return this.getSixth();
    }

    public int get_t() {
        return this.get_bold_d_E().getLength();
    }

    public int get_u() {
        return this.get_bold_d_G().getLength();
    }

    public int get_n() {
        return this.get_bold_d_A().getLength();
    }

    public int get_N_E() {
        return this.get_bold_d_V().getLength();
    }

    public int get_w() {
        return this.get_bold_d_C().getLength();
    }

}
