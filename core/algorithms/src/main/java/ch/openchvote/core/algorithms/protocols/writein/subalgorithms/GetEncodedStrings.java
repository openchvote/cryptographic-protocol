/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.StringToInteger;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteIn;
import ch.openchvote.base.utilities.set.Alphabet;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.1 from CHVote Protocol Specification
 */
public final class GetEncodedStrings extends Algorithm<BigInteger> {

    @SuppressWarnings({"MissingJavadoc", "StringConcatenationInLoop", "UnnecessaryLocalVariable"})
    static public //
    BigInteger
    run(WriteIn S, Alphabet A, int ell, char c) {

        // ALGORITHM
        var S_1 = S.get_S_1();
        while (S_1.length() < ell) {
            S_1 = c + S_1;
        }
        var S_2 = S.get_S_2();
        while (S_2.length() < ell) {
            S_2 = c + S_2;
        }
        var S12 = S_1 + S_2;
        var x = StringToInteger.run(S12, A.addCharacter(c)).add(BigInteger.ONE);
        return x;
    }

}
