/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.plain.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.protocols.plain.model.ElectionParametersPlain;
import ch.openchvote.core.algorithms.protocols.plain.model.VotingParametersPlain;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;

/**
 * Implementation of Algorithm 8.20 from CHVote Protocol Specification
 */
public final class GetVotingParameters extends Algorithm<VotingParametersPlain> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<VotingParametersPlain> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable", "unused"})
    static public //
    VotingParametersPlain
    run(int v, ElectionParametersPlain EP) {

        // PRECONDITIONS
        Precondition.checkNotNull(EP);
        var U = EP.get_U();
        var bold_n = EP.get_bold_n();
        var bold_k = EP.get_bold_k();
        var bold_u = EP.get_bold_u();
        var bold_w = EP.get_bold_w();
        var bold_E = EP.get_bold_E();
        var t = EP.get_t();
        var u = EP.get_u();
        var n = EP.get_n();
        var N_E = EP.get_N_E();
        var w = EP.get_w();
        Precondition.check(IntSet.range(1, N_E).contains(v));
        Precondition.check(Set.UCS_star.contains(U));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntVector(IntSet.range(1, u), t).contains(bold_u));
        Precondition.check(Set.IntVector(IntSet.range(1, w), N_E).contains(bold_w));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, u).contains(bold_E));

        // ALGORITHM
        var w_v = bold_w.getValue(v);
        var bold_e_hat_v = bold_E.getRow(v).expand(bold_u);
        var VP_v = new VotingParametersPlain(U, bold_n, bold_k, bold_u, w_v, bold_e_hat_v);
        return VP_v;
    }

}
