/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.parameters.usability.WriteInsParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.core.algorithms.protocols.common.model.Query;
import ch.openchvote.core.algorithms.protocols.writein.model.MultiEncryption;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteIn;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteInProof;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.15 from CHVote Protocol Specification
 */
public final class CheckWriteInProof extends Algorithm<Boolean> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters & NIZKPParameters, UP extends CodeParameters & WriteInsParameters>
    boolean // return type
    run(WriteInProof pi, BigInteger pk, Vector<Query> bold_a, Vector<BigInteger> bold_pk_prime, MultiEncryption e_prime, Vector<BigInteger> bold_p, SP securityParameters, UP usabilityParameters) {

        // USABILITY PARAMETERS
        var A_W = usabilityParameters.get_A_W();
        var ell_W = usabilityParameters.get_ell_W();
        var c_W = usabilityParameters.get_c_W();

        // PREPARATION
        var z = bold_a.getLength();
        var builder_bold_Y_star = new Matrix.Builder<Triple<BigInteger, BigInteger, Encryption>>(z, 2);
        var bold_a_prime = e_prime.get_bold_a();
        var b_prime = e_prime.get_b();

        // ALGORITHM
        var epsilon = GetEncodedStrings.run(WriteIn.EMPTY, A_W, ell_W, c_W);
        for (int i : IntSet.range(1, z)) {
            var a_i = bold_a.getValue(i);
            var y_star_i_1 = new Triple<>(pk, bold_p.getValue(i), new Encryption(a_i.get_a_1(), a_i.get_a_2())); // type conversion from query to encryption
            var y_star_i_2 = new Triple<>(bold_pk_prime.getValue(i), epsilon, new Encryption(bold_a_prime.getValue(i), b_prime));
            builder_bold_Y_star.set(i, 1, y_star_i_1);
            builder_bold_Y_star.set(i, 2, y_star_i_2);
        }
        var bold_Y_star = builder_bold_Y_star.build();
        var b = CheckCNFProof.run(pi, bold_Y_star, securityParameters);
        return b;
    }

}
