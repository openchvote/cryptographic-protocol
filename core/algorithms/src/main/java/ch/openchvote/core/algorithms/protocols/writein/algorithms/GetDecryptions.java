/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.algorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.AugmentedEncryption;
import ch.openchvote.core.algorithms.protocols.writein.subalgorithms.GetPartialDecryption;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.set.Set;
import ch.openchvote.base.utilities.tuples.Pair;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.22 from CHVote Protocol Specification
 */
public final class GetDecryptions extends Algorithm<Pair<Vector<BigInteger>, Matrix<BigInteger>>> {

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public final TypeReference<Pair<Vector<BigInteger>, Matrix<BigInteger>>> RETURN_TYPE = new TypeReference<>() {
    };

    @SuppressWarnings({"MissingJavadoc", "unused"})
    static public //
    <SP extends ZZPlusParameters>
    Pair<Vector<BigInteger>, Matrix<BigInteger>>
    run(Vector<AugmentedEncryption> bold_e_bar, BigInteger sk, Vector<BigInteger> bold_sk_prime, SP securityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_e_bar, sk, bold_sk_prime);
        var N = bold_e_bar.getLength();
        var z = bold_sk_prime.getLength();
        Precondition.check(Set.Vector(Set.Quadruple(ZZPlus_p, ZZPlus_p, Set.Vector(ZZPlus_p, z), ZZPlus_p), N).contains(bold_e_bar));
        Precondition.check(ZZ_q.contains(sk));
        Precondition.check(Set.Vector(ZZ_q, z).contains(bold_sk_prime));

        // PREPARATION
        var builder_bold_c = new Vector.Builder<BigInteger>(N);
        var builder_bold_C_prime = new Matrix.RowBuilder<BigInteger>(N, z);

        // ALGORITHM
        for (int i : IntSet.range(1, N)) {
            var e_bar_i = bold_e_bar.getValue(i);
            var pair = GetPartialDecryption.run(e_bar_i, sk, bold_sk_prime, securityParameters);
            var c_i = pair.getFirst();
            var bold_c_prime_i = pair.getSecond();
            builder_bold_c.set(i, c_i);
            builder_bold_C_prime.setRow(i, bold_c_prime_i);
        }
        var bold_c = builder_bold_c.build();
        var bold_C_prime = builder_bold_C_prime.build();
        return new Pair<>(bold_c, bold_C_prime);
    }

}
