/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.protocols.writein.subalgorithms;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GenRandomInteger;
import ch.openchvote.core.algorithms.general.algorithms.GetChallenge;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.core.algorithms.protocols.writein.model.WriteInProof;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Pair;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

/**
 * Implementation of Algorithm 9.13 from CHVote Protocol Specification
 */
public final class GenCNFProof extends Algorithm<WriteInProof> {

    @SuppressWarnings({"MissingJavadoc", "UnnecessaryLocalVariable"})
    static public //
    <SP extends ZZPlusParameters & NIZKPParameters>
    WriteInProof
    run(Matrix<Triple<BigInteger, BigInteger, Encryption>> bold_Y, Vector<BigInteger> bold_r, IntVector bold_j, SP securityParameters) {

        // SECURITY PARAMETERS
        var ZZPlus_p = securityParameters.get_ZZPlus_p();
        var ZZ_q = securityParameters.get_ZZ_q();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();
        var ZZ_twoToTheTau = securityParameters.get_ZZ_twoToTheTau();
        var twoToTheTau = securityParameters.get_twoToTheTau();

        // PREPARATION
        var m = bold_Y.getHeight();
        var n = bold_Y.getWidth();
        var builder_bold_c = new Vector.Builder<BigInteger>(m);
        var builder_bold_omega = new Vector.Builder<BigInteger>(m);
        var builder_bold_T = new Matrix.Builder<Pair<BigInteger, BigInteger>>(m, n);
        var builder_bold_C = new Matrix.Builder<BigInteger>(m, n);
        var builder_bold_S = new Matrix.Builder<BigInteger>(m, n);

        // ALGORITHM
        for (int i : IntSet.range(1, m)) {
            var c_i = BigInteger.ZERO;
            BigInteger omega_i;
            for (int j : IntSet.range(1, n)) {
                Pair<BigInteger, BigInteger> t_ij;
                var y_ij = bold_Y.getValue(i, j);
                var pk_ij = y_ij.getFirst();
                var m_ij = y_ij.getSecond();
                var e_ij = y_ij.getThird();
                var a_ij = e_ij.get_a();
                var b_ij = e_ij.get_b();
                BigInteger c_ij;
                BigInteger s_ij;
                if (j == bold_j.getValue(i)) {
                    omega_i = GenRandomInteger.run(q);
                    t_ij = new Pair<>(ZZPlus_p.pow(pk_ij, omega_i), ZZPlus_p.pow(g, omega_i));
                    builder_bold_omega.set(i, omega_i);
                } else {
                    c_ij = GenRandomInteger.run(twoToTheTau);
                    s_ij = GenRandomInteger.run(q);
                    t_ij = new Pair<>(
                            ZZPlus_p.multiply(ZZPlus_p.pow(pk_ij, s_ij), ZZPlus_p.pow(ZZPlus_p.divide(a_ij, m_ij), c_ij)),
                            ZZPlus_p.multiply(ZZPlus_p.pow(g, s_ij), ZZPlus_p.pow(b_ij, c_ij)));
                    c_i = ZZ_twoToTheTau.add(c_i, c_ij);
                    builder_bold_C.set(i, j, c_ij);
                    builder_bold_S.set(i, j, s_ij);
                }
                builder_bold_T.set(i, j, t_ij);
            }
            builder_bold_c.set(i, c_i);
        }
        var bold_T = builder_bold_T.build();
        var c = GetChallenge.run(bold_Y, bold_T, securityParameters);
        var bold_c = builder_bold_c.build();
        var bold_omega = builder_bold_omega.build();
        for (int i : IntSet.range(1, m)) {
            var j = bold_j.getValue(i);
            var c_ij = ZZ_twoToTheTau.subtract(c, bold_c.getValue(i));
            var s_ij = ZZ_q.subtract(bold_omega.getValue(i), ZZ_q.multiply(c_ij, bold_r.getValue(i)));
            builder_bold_C.set(i, j, c_ij);
            builder_bold_S.set(i, j, s_ij);
        }
        var bold_C = builder_bold_C.build();
        var bold_S = builder_bold_S.build();
        var pi = new WriteInProof(bold_C, bold_S);
        return pi;
    }

}
