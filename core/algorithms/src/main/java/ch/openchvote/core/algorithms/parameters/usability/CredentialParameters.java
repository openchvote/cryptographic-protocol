/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.algorithms.parameters.usability;

import ch.openchvote.base.utilities.set.Alphabet;
import ch.openchvote.base.utilities.tools.Math;

/**
 * Instances of this interface represent a list of parameters for specifying the alphabets and lengths used for
 * encoding the voting and confirmation credentials.
 */
public interface CredentialParameters extends UsabilityParameters {

    /**
     * Returns the security parameter {@code tau}.
     *
     * @return The security parameter {@code tau}
     */
    int get_tau();

    /**
     * Returns the alphabet for encoding the voting credentials.
     *
     * @return The alphabet for encoding the voting credentials
     */
    Alphabet get_A_X();

    /**
     * Returns the alphabet for encoding the confirmation credentials.
     *
     * @return The alphabet for encoding the confirmation credentials
     */
    Alphabet get_A_Y();

    /**
     * Returns the length (number of bits) of the voting credentials.
     *
     * @return The length of the voting credentials
     */
    default int get_ell_X() {
        return Math.ceilLog(Math.powerOfTwo(2 * this.get_tau()), this.get_A_X().getSize());
    }

    /**
     * Returns the length (number of bits) of the confirmation credentials.
     *
     * @return The length of the confirmation credentials
     */
    default int get_ell_Y() {
        return Math.ceilLog(Math.powerOfTwo(2 * this.get_tau()), this.get_A_Y().getSize());
    }

}
