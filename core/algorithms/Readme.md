# Algorithms

This module contains implementations of all pseudocode algorithms contained in
the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325). Both protocol versions (*Plain* and *WriteIn*)
are fully covered.

## Module Description

### Design

A class with a single static method called `run` exists for every pseudocode algorithm. Domain
membership is tested systematically for all input parameters of all top-level algorithms, except in sub-algorithms,
where these tests are omitted. All algorithms are stateless and are free of side effects, which implies that they can be
used concurrently without taking any thread-safety measures.

Most algorithms require access to two types of additional parameters: security parameters and usability parameters.
Interfaces for these parameters are defined in corresponding packages `parameters.security` and `parameters.usability`.
Each algorithm implementation defines two generic types `SP` and `UP`, which derive from the subset of parameter
interfaces required for that particular algorithm, and corresponding algorithm parameters `securityParameters` of
type `SP` and `usabilityParameters` of type `UP`. In this way, access to all parameters required by the algorithm is
guaranteed without imposing a specific implementation. This maximizes the flexibility of using the algorithms in
different contexts.

### Example

The primary design goal in this module is to reach the highest possible correspondence between pseudocode algorithms
and Java code. Here is an exemplary algorithm (Algorithm 8.30: GetVerificationCodes) that demonstrates the level of
correspondence that has been reached:

#### Pseudo-Code Algorithm:

| <img src="img/pseudocode.png" width="450" alt="pseudocode"/> |
|--------------------------------------------------------------|

#### Java Code:

```java
public final class GetVerificationCodes extends Algorithm<Vector<String>> {

    static public <SP extends GGParameters & HashParameters, UP extends CodeParameters>
    Vector<String>
    run(IntVector bold_s, Matrix<Point> bold_P, SP securityParameters, UP usabilityParameters) {

        // SECURITY PARAMETERS
        Precondition.checkNotNull(securityParameters);
        var ZZ_q_hat = securityParameters.get_ZZ_q_hat();

        // USABILITY PARAMETERS
        Precondition.checkNotNull(usabilityParameters);
        var A_V = usabilityParameters.get_A_V();
        var L_V = usabilityParameters.get_L_V();
        var n_max = usabilityParameters.get_n_max();

        // PRECONDITIONS
        Precondition.checkNotNull(bold_s, bold_P);
        var k = bold_P.getHeight();
        var s = bold_P.getWidth();
        Precondition.check(Set.IntVector(IntSet.range(1, n_max), k).contains(bold_s));
        Precondition.check(Set.Matrix(Set.Pair(ZZ_q_hat, ZZ_q_hat), k, s).contains(bold_P));

        // CONSTRAINTS
        Precondition.check(bold_s.allMatch(SMALLER));

        // PREPARATION
        var builder_bold_vc = new Vector.Builder<String>(k);

        // ALGORITHM
        for (int i = 1; i <= k; i++) {
            var builder_bold_v_i = new Vector.Builder<ByteArray>(s);
            for (int j = 1; j <= s; j++) {
                var p_ij = bold_P.getValue(i, j);
                var V_ij = RecHash.run(securityParameters, p_ij).truncate(L_V);
                builder_bold_v_i.setValue(j, V_ij);
            }
            var bold_v_i = builder_bold_v_i.build();
            var V_i = ByteArray.xor(bold_v_i);
            V_i = SetWatermark.run(V_i, bold_s.getValue(i) - 1, n_max);
            var VC_i = ByteArrayToString.run(V_i, A_V);
            builder_bold_vc.setValue(i, VC_i);
        }
        var bold_vc = builder_bold_vc.build();
        return bold_vc;
    }

}
```

#### Remarks:

The exemplary static Java method `run` from the class `GetVerificationCodes` contains two algorithm parameters `bold_s`
and `bold_P`, which correspond to the two parameters in the pseudocode algorithm. The correctness of the domains of such
algorithm parameters is checked systematically.

The shown method also contains two arguments `securityParameter` and `usabilityParameter`, which contain all the
necessary security and usability parameters used in this algorithm (some algorithms only have one such argument). These
parameters correspond to the security and usability parameters as defined in Section 6.3 and Chapter 10
of [CHVote Protocol Specification](https://eprint.iacr.org/2017/325).

The structure of these algorithms is always follows:

- `USABILITY PARAMETERS`: The necessary security parameters are selected from the corresponding argument.
- `SECURITY PARAMETERS`: The necessary usability parameters are selected from the corresponding argument.
- `PRECONDITIONS`: The domains of all involved algorithm parameters are checked (not for sub-algorithms)
- `PREPARATION`: Further preparatory steps are executed.
- `ALGORITHM`: The actual algorithm is executed in a manner as closely as possible to the specification.

### Documentation

In this module, we intentionally depart from common JavaDoc best practices such as documenting every public class,
method, and variable. In the JavaDoc section of each algorithm class, we only give the algorithm number from the
[CHVote Protocol Specification](https://eprint.iacr.org/2017/325) document as a reference. Any further detail about the
parameters and return values can be taken from there. Our goal is to avoid unnecessary redundancies, i.e., to eliminate
the problem of introducing unintended inconsistencies after modifications. We also assume that all model classes are
clear in the context of the specification document, i.e., no JavaDoc is needed for giving further explanations.

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of
this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts
is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as
follows:

```console
mvn clean install
-- a lot of output
```

## Maven Dependencies

The following dependency tree illustrates that this module has a single direct dependency
to `io.gitlab.openchvote:utilities`.

```console
mvn dependency:tree -Dscope=compile -Dverbose=false
[INFO] --- dependency:3.6.0:tree (default-cli) @ algorithms ---
[INFO] io.gitlab.openchvote:algorithms:jar:2.4
[INFO] \- io.gitlab.openchvote:utilities:jar:2.4:compile
[INFO]    \- com.verificatum:vmgj:jar:1.2.2:compile
```

## Java Module Dependencies

The following module dependency diagram illustrates the Java module dependencies to other modules within this project:

![Java module dependencies](img/algorithms.svg)