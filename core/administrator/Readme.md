# Administrator

This module provides an implementation of the CHVote party *"Administrator"*. In
the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325), the role of the administrator is described as
follows:

> The election administrator is responsible for setting up an election event. This includes tasks such as defining the
> electoral roll, the number of elections, the set of candidates in each election, and the eligibility of each voter in
> each election [...]. At the end of the election process, the election administrator determines and publishes the final
> election result.

The intention of this Maven module is to provide production-quality Java code that implements the administrator's role
in the CHVote protocol. Robust implementations of corresponding services can be connected to this module by using Java's
`ServiceLoader.load()` functionality.

## Specification

### Phases and States

| Phase          | States | Error States | Task                                                                          |
|:---------------|:-------|:-------------|:------------------------------------------------------------------------------|
| Initialization | S100   | –            | Send event setup and election parameters to election and printing authorities |
| Preparation    | S200   | A1           | Generate and distribute public key share                                      |
| Election       | S300   | –            | Send event setup, voting parameters, and public key share to voting clients   |
| Tallying       | S400   | A3, B3       | Perform final decryption are publish election result                          |
| Verification   | S500   | –            | Publish election data for verification                                        |
| Inspection     | S600   | –            | Send event setup and voting parameters to inspection clients                  |
| Testing        | –      | –            | –                                                                             |
| [none]         | FINAL  | –            | –                                                                             |

### State Diagram

![StateDiagram](img/stateDiagram.svg)

### Communication

#### Services:

|                        | Administrator |
|:-----------------------|:--------------|
| EventService           | Subscriber    |
| MailingService         | –             |
| MessagingService       | Subscriber    |
| PublicationService     | Publisher     |
| RequestResponseService | Responder     |
| UserInterface          | –             |
| TestingService         | –             |

#### Incoming Messages:

| State | Phase      | Protocol | Name | Type    | Sender             | Signed | Encrypted | Content                      |
|:-----:|:-----------|:--------:|:----:|:--------|:-------------------|:------:|:---------:|------------------------------|
| S300  | Election   |   7.5    | RCA1 | Request | Voting client      |   no   |    no     | Voter index                  |
| S400  | Tallying   |   7.10   | MEA1 | Message | Election authority |  yes   |    no     | Combined partial decryptions |
| S600  | Inspection |   7.12   | RIA1 | Request | Inspection client  |   no   |    no     | Voter index                  |

#### Outgoing Messages:

| State | Phase          | Protocol | Name | Type        | Receiver           | Signed | Encrypted | Content                                          |
|:-----:|:---------------|:--------:|:----:|:------------|:-------------------|:------:|:---------:|--------------------------------------------------|
| S100  | Initialization |   7.1    | MAE1 | Message     | Election authority |  yes   |    no     | Event setup, election parameters                 |
| S100  | Initialization |   7.1    | MAP1 | Message     | Printing authority |  yes   |    no     | Event setup, election parameters                 |
| S200  | Preparation    |   7.2    | MAE2 | Message     | Election authority |  yes   |    no     | Public key share                                 |
| S300  | Election       |   7.5    | RAC1 | Response    | Voting client      |  yes   |    no     | Event setup, voting parameters, public key share |
| S400  | Tallying       |   7.10   | PAP1 | Publication | Public             |  yes   |    no     | Event setup, voting parameters, election result  |
| S500  | Verification   |   7.11   | PAP2 | Publication | Public             |  yes   |    no     | Public election data                             |
| S600  | Inspection     |   7.12   | RAI1 | Response    | Inspection client  |  yes   |    no     | Event setup, voting parameters                   |

#### Coordination:

| Type           | State | Phase          | New State | New Phase      |
|:---------------|:-----:|:---------------|:---------:|:---------------|
| onInitialize() |   –   | –              |   S100    | Initialization |
| onStart()      | S100  | Initialization |   S200    | Preparation    | 
| onStart()      | S200  | Preparation    |   S300    | Election       |
| onStart()      | S300  | Election       |   S300    | Election       |
| onStop()       | S300  | Election       |   S400    | Tallying       |
| onStart()      | S400  | Tallying       |   S400    | Tallying       |
| onStart()      | S500  | Verification   |   S600    | Inspection     |
| onStart()      | S600  | Inspection     |   S600    | Inspection     |
| onStop()       | S600  | Inspection     |   FINAL   | [none]         |
| onTerminate    | FINAL | [none]         |     –     | –              |

### Tasks

| Task | State | Phase       | Protocol | AlgorithmicException | TaskException |
|:----:|:-----:|:------------|:--------:|:--------------------:|:-------------:|
| TA1  | S200  | Preparation |   7.2    |          A1          |       –       |
| TA2  | S300  | Election    |   7.5    |        (S300)        |       –       |
| TA3  | S400  | Tallying    |   7.10   |          A3          |      B3       |
| TA2  | S600  | Inspection  |   7.12   |        (S600)        |       –       |

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of
this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts
is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as
follows:

```console
mvn clean install
-- a lot of output
```

## Maven Dependencies

The following dependency tree shows the Maven dependencies of this module.

```console
mvn dependency:tree -Dscope=compile -Dverbose=false
[INFO] --- dependency:3.6.0:tree (default-cli) @ administrator ---
[INFO] io.gitlab.openchvote:administrator:jar:2.4
[INFO] \- io.gitlab.openchvote:protocol:jar:2.4:compile
[INFO]    +- io.gitlab.openchvote:algorithms:jar:2.4:compile
[INFO]    |  \- io.gitlab.openchvote:utilities:jar:2.4:compile
[INFO]    |     \- com.verificatum:vmgj:jar:1.2.2:compile
[INFO]    \- io.gitlab.openchvote:framework:jar:2.4:compile
```

## Java Module Dependencies

The following module dependency diagram illustrates the Java module dependencies to other modules within this project:

![Java module dependencies](img/administrator.svg)