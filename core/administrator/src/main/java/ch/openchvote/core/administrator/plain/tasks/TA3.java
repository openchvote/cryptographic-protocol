/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator.plain.tasks;

import ch.openchvote.core.administrator.plain.PublicData;
import ch.openchvote.core.administrator.plain.SecretData;
import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.GenDecryptionProof;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.GetDecryptions;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.GetElectionResult;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.GetVotes;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;

@SuppressWarnings("MissingJavadoc")
public final class TA3 {

    static public boolean isReady(PublicData publicData) {
        var s = publicData.get_ES().get().get_SP().get_s();
        return publicData.get_arrow_bold_c().arePresent(IntSet.range(1, s)) && publicData.get_arrow_bold_e_tilde_s().arePresent(IntSet.range(1, s));
    }

    static public void
    run(PublicData publicData, SecretData secretData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();
        var s = ES.get_SP().get_s();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select election parameters
        var EP = publicData.get_EP().get();
        var bold_n = EP.get_bold_n();
        var bold_w = EP.get_bold_w();

        // select event data
        var sk_0 = secretData.get_sk_0().get();
        var pk_0 = publicData.get_pk_0().get();
        var arrow_bold_e_tilde_s = publicData.get_arrow_bold_e_tilde_s().mapTo(Vector::of);
        var arrow_bold_c = publicData.get_arrow_bold_c().mapTo(Vector::of);

        // perform task
        if (arrow_bold_c.isConstant() && arrow_bold_e_tilde_s.isConstant() && s >= 1) {
            var bold_c = arrow_bold_c.getValue(1);
            var bold_e_tilde_s = arrow_bold_e_tilde_s.getValue(1);
            var bold_c_0 = algorithmService.run(GetDecryptions.class, securityParameters, bold_e_tilde_s, sk_0);
            var pi_prime_0 = algorithmService.run(GenDecryptionProof.class, securityParameters, sk_0, pk_0, bold_e_tilde_s, bold_c_0);
            var bold_m = algorithmService.run(GetVotes.class, securityParameters, bold_e_tilde_s, bold_c, bold_c_0);
            var ER = algorithmService.run(GetElectionResult.class, securityParameters, bold_m, bold_n, bold_w);

            // update event data
            publicData.get_bold_c_0().set(bold_c_0);
            publicData.get_pi_prime_0().set(pi_prime_0);
            publicData.get_ER().set(ER);
        } else {
            throw new TaskException(TaskException.Type.VALUES_NOT_EQUAL, TA3.class);
        }
    }

}
