/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator.writein;

import ch.openchvote.core.administrator.writein.states.S100;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Sextuple;

/**
 * Instances of this class represent the administrator's event context. The private constructor
 * {@link EventContext#EventContext(String, String, String)} defines the administrator's initial state and its public and secret
 * event data. The framework calls this constructor using reflection.
 */
@SuppressWarnings("unused")
public final class EventContext extends ch.openchvote.base.framework.party.EventContext<PublicData, SecretData> {

    @SuppressWarnings("MissingJavadoc")
    static public final TypeReference<Sextuple<String, String, String, String, PublicData, SecretData>> TYPE_REFERENCE = new TypeReference<>() {
    };

    // private constructor for an empty event context set to the initial state
    private EventContext(String eventId, String protocolId, String securityLevel) {
        this(eventId, protocolId, securityLevel, State.getId(S100.class), new PublicData(), new SecretData());
    }

    // private constructor for initializing all fields
    private EventContext(String eventId, String protocolId, String securityLevel, String stateId, PublicData publicData, SecretData secretData) {
        super(eventId, protocolId, securityLevel, stateId, publicData, secretData);
    }

    // private copy constructor
    private EventContext(EventContext eventContext) {
        super(eventContext);
    }

}
