/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionParameters;
import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.core.administrator.plain.states.S100;
import ch.openchvote.core.protocol.protocols.CHVote;

import java.util.ServiceLoader;

/**
 * This service interface provides methods for generating an election setup and election parameters. The administrator
 * calls these methods in its initial states {@link S100} and
 * {@link ch.openchvote.core.administrator.writein.states.S100}, respectively, to initiate the election process.
 *
 * @param <EP> The generic type of the election parameters
 */
public interface EventDefinitionService<EP extends ElectionParameters> {

    /**
     * Generates an event setup for the given event id.
     *
     * @param eventId The given event id
     * @return The resulting event setup
     */
    EventSetup getEventSetup(String eventId);

    /**
     * Generates election parameters for the given event id.
     *
     * @param eventId The given event id
     * @return The resulting election parameters
     */
    EP getElectionParameters(String eventId);

    /**
     * Generates election descriptions for the given event id.
     *
     * @param eventId The given event id
     * @return The resulting election descriptions
     */
    ElectionDescriptions getElectionDescriptions(String eventId);

    /**
     * Checks if the event definition service supports a given protocol.
     *
     * @param protocol The given protocol
     * @return {@code true}, if the given protocol is supported, {@code false} otherwise
     */
    boolean canHandle(Class<? extends CHVote<?, ?>> protocol);

    /**
     * Returns an instance of an event definition service class that supports the specified protocol. If there are
     * multiple such service classes, the first from the loaded list is taken. If no suitable service classes are
     * available, an exception is thrown.
     *
     * @param protocol The given protocol
     * @param <EP>     The generic type of the election parameters
     * @return An instance of an event definition service class
     * @throws UnsupportedOperationException if no service implementation is available
     */
    @SuppressWarnings("unchecked")
    static <EP extends ElectionParameters> EventDefinitionService<EP> load(Class<? extends CHVote<EP, ?>> protocol) {
        for (EventDefinitionService<?> service : ServiceLoader.load(EventDefinitionService.class)) {
            if (service.canHandle(protocol)) {
                return (EventDefinitionService<EP>) service;
            }
        }
        throw new UnsupportedOperationException("Event definition service not available: " + protocol);
    }

}
