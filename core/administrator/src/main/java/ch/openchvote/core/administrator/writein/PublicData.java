/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator.writein;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.core.algorithms.protocols.writein.model.*;
import ch.openchvote.core.protocol.protocols.writein.content.message.MEA1;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Tuple;
import ch.openchvote.base.utilities.tuples.decuple.TreDecuple;

import java.math.BigInteger;

/**
 * Instances of this class represent the administrator's public data. The class inherits from the generic {@link Tuple}
 * subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects to store.
 * Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class PublicData extends TreDecuple<
        // event setup and election parameters
        EventData.Data<EventSetup>, // ES
        EventData.Data<ElectionParametersWriteIn>, // EP
        EventData.Data<ElectionDescriptions>, // ED
        // public keys
        EventData.Data<BigInteger>, // pk_0
        EventData.Data<Vector<BigInteger>>, // bold_pk_prime_0
        EventData.Data<KeyPairProof>, // pi_0
        // encrypted votes and partial decryption
        EventData.DataMap<Vector<AugmentedEncryption>>, // arrow_bold_e_tilde_s
        EventData.DataMap<Vector<BigInteger>>, // arrow_bold_c
        EventData.DataMap<Matrix<BigInteger>>, // arrow_bold_D
        EventData.Data<Vector<BigInteger>>, // bold_c_0
        EventData.Data<Matrix<BigInteger>>, // bold_D_0
        EventData.Data<DecryptionProof>, // pi_prime_0
        // election result
        EventData.Data<ElectionResultWriteIn>> // ER
        implements EventData {

    public PublicData() {
        this(new Data<>(), new Data<>(), new Data<>(), new Data<>(), new Data<>(), new Data<>(), new DataMap<>(), new DataMap<>(), new DataMap<>(), new Data<>(), new Data<>(), new Data<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private PublicData(Data<EventSetup> ES, Data<ElectionParametersWriteIn> EP, Data<ElectionDescriptions> ED, Data<BigInteger> pk_0, Data<Vector<BigInteger>> bold_pk_prime_0, Data<KeyPairProof> pi_0, DataMap<Vector<AugmentedEncryption>> bold_E_tilde_s, DataMap<Vector<BigInteger>> bold_C, DataMap<Matrix<BigInteger>> arrow_bold_D, Data<Vector<BigInteger>> bold_c_0, Data<Matrix<BigInteger>> bold_D_0, Data<DecryptionProof> pi_prime_0, Data<ElectionResultWriteIn> ER) {
        super(ES, EP, ED, pk_0, bold_pk_prime_0, pi_0, bold_E_tilde_s, bold_C, arrow_bold_D, bold_c_0, bold_D_0, pi_prime_0, ER);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private PublicData(PublicData publicData) {
        this(publicData.get_ES(), publicData.get_EP(), publicData.get_ED(), publicData.get_pk_0(), publicData.get_bold_pk_prime_0(), publicData.get_pi_0(), publicData.get_arrow_bold_e_tilde_s(), publicData.get_arrow_bold_c(), publicData.get_arrow_bold_D(), publicData.get_bold_c_0(), publicData.get_bold_D_0(), publicData.get_pi_prime_0(), publicData.get_ER());
    }

    public Data<EventSetup> get_ES() {
        return this.getFirst();
    }

    public Data<ElectionParametersWriteIn> get_EP() {
        return this.getSecond();
    }

    public Data<ElectionDescriptions> get_ED() {
        return this.getThird();
    }

    public Data<BigInteger> get_pk_0() {
        return this.getFourth();
    }

    public Data<Vector<BigInteger>> get_bold_pk_prime_0() {
        return this.getFifth();
    }

    public Data<KeyPairProof> get_pi_0() {
        return this.getSixth();
    }

    public DataMap<Vector<AugmentedEncryption>> get_arrow_bold_e_tilde_s() {
        return this.getSeventh();
    }

    public DataMap<Vector<BigInteger>> get_arrow_bold_c() {
        return this.getEighth();
    }

    public DataMap<Matrix<BigInteger>> get_arrow_bold_D() {
        return this.getNinth();
    }

    public Data<Vector<BigInteger>> get_bold_c_0() {
        return this.getTenth();
    }

    public Data<Matrix<BigInteger>> get_bold_D_0() {
        return this.getEleventh();
    }

    public Data<DecryptionProof> get_pi_prime_0() {
        return this.getTwelfth();
    }

    public Data<ElectionResultWriteIn> get_ER() {
        return this.getThirteenth();
    }

    /**
     * Calling this method stores the given content of type {@link MEA1} into the administrator's public data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, MEA1 content) {
        this.get_arrow_bold_c().set(index, content.get_bold_c());
        this.get_arrow_bold_D().set(index, content.get_bold_D());
        this.get_arrow_bold_e_tilde_s().set(index, content.get_bold_e_tilde_s());
    }

}
