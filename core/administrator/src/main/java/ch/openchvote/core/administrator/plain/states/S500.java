/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator.plain.states;

import ch.openchvote.core.administrator.Administrator;
import ch.openchvote.core.administrator.plain.EventContext;
import ch.openchvote.core.algorithms.protocols.plain.model.PublicDataAdministrator;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Verification;
import ch.openchvote.core.protocol.protocols.plain.content.publication.PAP2;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Verification.class)
@Notify(Status.Type.READY)
public final class S500 extends State<Administrator, EventContext> {

    public S500(Administrator administrator, EventContext eventContext) {
        super(administrator, eventContext);
    }

    @SuppressWarnings("unused")
    @Override
    public void handleStart() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select event definition
        var ES = publicData.get_ES().get();
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get event data
        var pk_0 = publicData.get_pk_0().get();
        var pi_0 = publicData.get_pi_0().get();
        var bold_c_0 = publicData.get_bold_c_0().get();
        var pi_prime_0 = publicData.get_pi_prime_0().get();

        // publish PAP2
        var PD_A = new PublicDataAdministrator(pk_0, pi_0, bold_c_0, pi_prime_0);
        var aux = new Triple<>(ES, EP, ED);
        this.party.sendPublication(eventId, new PAP2(PD_A), aux, securityLevel);

        // update state
        this.party.updateState(this.eventContext, S600.class);

    }

}
