/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator.plain;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.core.algorithms.protocols.plain.model.DecryptionProof;
import ch.openchvote.core.algorithms.protocols.plain.model.ElectionParametersPlain;
import ch.openchvote.core.algorithms.protocols.plain.model.ElectionResultPlain;
import ch.openchvote.core.algorithms.protocols.plain.model.KeyPairProof;
import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.core.protocol.protocols.plain.content.message.MEA1;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Tuple;
import ch.openchvote.base.utilities.tuples.decuple.Decuple;

import java.math.BigInteger;

/**
 * Instances of this class represent the administrator's public data. The class inherits from the generic {@link Tuple}
 * subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects to store.
 * Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class PublicData extends Decuple<
        // event setup and election parameters
        EventData.Data<EventSetup>, // ES
        EventData.Data<ElectionParametersPlain>, // EP
        EventData.Data<ElectionDescriptions>, // ES
        // public key
        EventData.Data<BigInteger>, // pk_0
        EventData.Data<KeyPairProof>, // pi_0
        // encrypted votes and partial decryption
        EventData.DataMap<Vector<Encryption>>, // arrow_bold_e_tilde_s
        EventData.DataMap<Vector<BigInteger>>, // arrow_bold_c
        EventData.Data<Vector<BigInteger>>, // bold_c_0
        EventData.Data<DecryptionProof>, // pi_prime_0
        // election result
        EventData.Data<ElectionResultPlain>> // ER
        implements EventData {

    public PublicData() {
        this(new Data<>(), new Data<>(), new Data<>(), new Data<>(), new Data<>(), new DataMap<>(), new DataMap<>(), new Data<>(), new Data<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private PublicData(Data<EventSetup> ES, Data<ElectionParametersPlain> EP, Data<ElectionDescriptions> ED, Data<BigInteger> pk_0, Data<KeyPairProof> pi_0, DataMap<Vector<Encryption>> bold_E_tilde_s, DataMap<Vector<BigInteger>> bold_C, Data<Vector<BigInteger>> bold_c_0, Data<DecryptionProof> pi_prime_0, Data<ElectionResultPlain> ER) {
        super(ES, EP, ED, pk_0, pi_0, bold_E_tilde_s, bold_C, bold_c_0, pi_prime_0, ER);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private PublicData(PublicData publicData) {
        this(publicData.get_ES(), publicData.get_EP(), publicData.get_ED(), publicData.get_pk_0(), publicData.get_pi_0(), publicData.get_arrow_bold_e_tilde_s(), publicData.get_arrow_bold_c(), publicData.get_bold_c_0(), publicData.get_pi_prime_0(), publicData.get_ER());
    }

    public Data<EventSetup> get_ES() {
        return this.getFirst();
    }

    public Data<ElectionParametersPlain> get_EP() {
        return this.getSecond();
    }

    public Data<ElectionDescriptions> get_ED() {
        return this.getThird();
    }

    public Data<BigInteger> get_pk_0() {
        return this.getFourth();
    }

    public Data<KeyPairProof> get_pi_0() {
        return this.getFifth();
    }

    public DataMap<Vector<Encryption>> get_arrow_bold_e_tilde_s() {
        return this.getSixth();
    }

    public DataMap<Vector<BigInteger>> get_arrow_bold_c() {
        return this.getSeventh();
    }

    public Data<Vector<BigInteger>> get_bold_c_0() {
        return this.getEighth();
    }

    public Data<DecryptionProof> get_pi_prime_0() {
        return this.getNinth();
    }

    public Data<ElectionResultPlain> get_ER() {
        return this.getTenth();
    }

    /**
     * Calling this method stores the given content of type {@link MEA1} into the administrator's public data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, MEA1 content) {
        this.get_arrow_bold_c().set(index, content.get_bold_c());
        this.get_arrow_bold_e_tilde_s().set(index, content.get_bold_e_tilde_s());
    }

}
