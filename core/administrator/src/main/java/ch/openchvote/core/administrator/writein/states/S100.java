/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator.writein.states;

import ch.openchvote.core.administrator.Administrator;
import ch.openchvote.core.administrator.EventDefinitionService;
import ch.openchvote.core.administrator.writein.EventContext;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Initialization;
import ch.openchvote.core.protocol.protocols.writein.WriteIn;
import ch.openchvote.core.protocol.protocols.writein.content.message.MAE1;
import ch.openchvote.core.protocol.protocols.writein.content.message.MAP1;

@SuppressWarnings("MissingJavadoc")
@Phase(Initialization.class)
@Notify(Status.Type.READY)
public final class S100 extends State<Administrator, EventContext> {

    public S100(Administrator administrator, EventContext eventContext) {
        super(administrator, eventContext);
    }

    @SuppressWarnings("unused")
    @Override
    public void handleStart() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // generate event setup and election parameters
        var service = EventDefinitionService.load(WriteIn.class);
        var ES = service.getEventSetup(eventId);
        var EP = service.getElectionParameters(eventId);
        var ED = service.getElectionDescriptions(eventId);

        // update event data
        publicData.get_ES().set(ES);
        publicData.get_EP().set(EP);
        publicData.get_ED().set(ED);

        // select setup parameters
        var SP = ES.get_SP();
        var PA = SP.get_PA();
        var bold_ea = SP.get_bold_ea();

        // send MAE1 to election authorities (no auxiliary data)
        this.party.sendMessage(eventId, bold_ea, new MAE1(ES, EP, ED), null, securityLevel);

        // send MAP1 to printing authority (no auxiliary data)
        this.party.sendMessage(eventId, PA, new MAP1(ES, EP, ED), null, securityLevel);

        // update state
        this.party.updateState(this.eventContext, S200.class);

    }

}
