/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator.writein.states;

import ch.openchvote.core.administrator.Administrator;
import ch.openchvote.core.administrator.writein.EventContext;
import ch.openchvote.core.administrator.writein.states.error.A1;
import ch.openchvote.core.administrator.writein.tasks.TA1;
import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Preparation;
import ch.openchvote.core.protocol.protocols.writein.content.message.MAE2;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Preparation.class)
@Notify(Status.Type.READY)
public final class S200 extends State<Administrator, EventContext> {

    public S200(Administrator administrator, EventContext eventContext) {
        super(administrator, eventContext);
    }

    @SuppressWarnings("unused")
    @Override
    public void handleStart() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SP = ES.get_SP();
        var bold_ea = SP.get_bold_ea();

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        try {
            // perform task
            TA1.run(publicData, secretData);

            // select event data
            var pk_0 = publicData.get_pk_0().get();
            var bold_pk_prime_0 = publicData.get_bold_pk_prime_0().get();
            var pi_0 = publicData.get_pi_0().get();

            // send MAE2 to election authorities
            var aux = new Triple<>(ES, EP, ED);
            this.party.sendMessage(eventId, bold_ea, new MAE2(pk_0, bold_pk_prime_0, pi_0), aux, securityLevel);

            // update state
            this.party.updateState(this.eventContext, S300.class);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A1.class);
        }
    }

}
