/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator.writein;

import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Pair;
import ch.openchvote.base.utilities.tuples.Tuple;

import java.math.BigInteger;

/**
 * Instances of this class represent the administrator's secret data. The class inherits from the generic {@link Tuple}
 * subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects to store. 
 * Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class SecretData extends Pair<
        EventData.Data<BigInteger>, // sk_0
        EventData.Data<Vector<BigInteger>>> // bold_sk_prime_0
        implements EventData {

    public SecretData() {
        this(new Data<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private SecretData(Data<BigInteger> sk_0, Data<Vector<BigInteger>> bold_sk_prime_0) {
        super(sk_0, bold_sk_prime_0);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private SecretData(SecretData secretData) {
        this(secretData.get_sk_0(), secretData.get_bold_sk_prime_0());
    }

    public Data<BigInteger> get_sk_0() {
        return this.getFirst();
    }

    public Data<Vector<BigInteger>> get_bold_sk_prime_0() {
        return this.getSecond();
    }

}
