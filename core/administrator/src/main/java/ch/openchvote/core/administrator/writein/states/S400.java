/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator.writein.states;

import ch.openchvote.core.administrator.Administrator;
import ch.openchvote.core.administrator.writein.EventContext;
import ch.openchvote.core.administrator.writein.states.error.A3;
import ch.openchvote.core.administrator.writein.states.error.B3;
import ch.openchvote.core.administrator.writein.tasks.TA3;
import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Message;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Tallying;
import ch.openchvote.core.protocol.protocols.writein.content.message.MEA1;
import ch.openchvote.core.protocol.protocols.writein.content.publication.PAP1;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Tallying.class)
@Notify(Status.Type.READY)
public final class S400 extends State<Administrator, EventContext> {

    public S400(Administrator administrator, EventContext eventContext) {
        super(administrator, eventContext);
        this.registerMessageHandler(MEA1.class, this::handleMEA1);
    }

    private void handleMEA1(Message message) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var j = ES.get_SP().getIndexOf(message.getSenderId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var MEA1 = this.party.getAndCheckContent(MEA1.class, message, aux, securityLevel);

        // update public data
        publicData.setContent(j, MEA1);

        // check if all MEA1 messages are available
        if (TA3.isReady(publicData)) {
            try {
                // perform task
                TA3.run(publicData, secretData);

                // select event data
                var ER = publicData.get_ER().get();

                // publish PAP1 (no auxiliary data)
                this.party.sendPublication(eventId, new PAP1(ES, EP, ED, ER), null, securityLevel);

                // update state
                this.party.updateState(this.eventContext, S500.class);

            } catch (Algorithm.Exception exception) {
                // move to error state
                this.party.updateState(this.eventContext, A3.class);
            } catch (TaskException exception) {
                // move to error state
                this.party.updateState(this.eventContext, B3.class);
            }
        }
    }

}
