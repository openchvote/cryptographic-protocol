/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator.writein.states;

import ch.openchvote.core.administrator.Administrator;
import ch.openchvote.core.administrator.writein.EventContext;
import ch.openchvote.core.administrator.writein.tasks.TA2;
import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Request;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.exceptions.CommunicationException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Inspection;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.request.RIA1;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response.RAI1;

import static ch.openchvote.base.framework.exceptions.CommunicationException.Type.INVALID_CONTENT;

@SuppressWarnings("MissingJavadoc")
@Phase(Inspection.class)
@Notify(Status.Type.READY)
public final class S600 extends State<Administrator, EventContext> {

    public S600(Administrator administrator, EventContext eventContext) {
        super(administrator, eventContext);
        this.registerRequestHandler(RIA1.class, this::handleRIA1);
    }

    private void handleRIA1(Request request) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();

        // get request content
        var RIA1 = this.party.getContent(RIA1.class, request);
        var v = RIA1.get_v();

        try {
            // perform task
            var pair = TA2.run(v, publicData);
            var VP_v = pair.getFirst();
            var VD_v = pair.getSecond();

            // send RAI1 to inspection client (no auxiliary data)
            this.party.sendResponse(eventId, request.getRequestId(), request.getRequesterId(), new RAI1(ES, VP_v, VD_v), null, securityLevel);

        } catch (Algorithm.Exception exception) {
            // discard request
            throw new CommunicationException(INVALID_CONTENT, request, exception);
        }
    }

    @Override
    public void handleStop() {

        // update state
        this.party.updateState(this.eventContext, FINAL.class);

    }

}
