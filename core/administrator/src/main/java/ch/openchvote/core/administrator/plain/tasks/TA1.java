/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.administrator.plain.tasks;

import ch.openchvote.core.administrator.plain.PublicData;
import ch.openchvote.core.administrator.plain.SecretData;
import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GenKeyPair;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.GenKeyPairProof;
import ch.openchvote.core.protocol.parameters.SecurityParameters;

@SuppressWarnings("MissingJavadoc")
public final class TA1 {

    static public void
    run(PublicData publicData, SecretData secretData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // perform task
        var keyPair = algorithmService.run(GenKeyPair.class, securityParameters);
        var sk_0 = keyPair.get_sk();
        var pk_0 = keyPair.get_pk();
        var pi_0 = algorithmService.run(GenKeyPairProof.class, securityParameters, sk_0, pk_0);

        // update event data
        secretData.get_sk_0().set(sk_0);
        publicData.get_pk_0().set(pk_0);
        publicData.get_pi_0().set(pi_0);
    }

}
