/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetInspection;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetVotingDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.VotingDescriptions;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.GetVotingParameters;
import ch.openchvote.core.algorithms.protocols.writein.model.VotingParametersWriteIn;
import ch.openchvote.core.electionauthority.writein.PublicData;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.core.protocol.protocols.writein.UsabilityParameters;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.set.IndexedFamily;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
public final class TE16 {

    static public Triple<VotingParametersWriteIn, VotingDescriptions, ByteArray>
    run(int v, PublicData publicData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();
        var UC = ES.get_UC();

        // get security and usability parameters
        var securityParameters = new SecurityParameters(SL);
        var usabilityParameters = new UsabilityParameters(UC, SL);

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // select event data
        var bold_a_j = publicData.get_bold_a_j().get();
        var bold_P_j = publicData.get_bold_P_j().get();
        var C_j = publicData.get_C_j().mapTo(IndexedFamily::of);

        // perform task
        var VP_v = algorithmService.run(GetVotingParameters.class, v, EP);
        var VD_v = algorithmService.run(GetVotingDescriptions.class, v, ED);
        var I_j = algorithmService.run(GetInspection.class, securityParameters, usabilityParameters, v, bold_P_j, bold_a_j, C_j);

        // return data
        return new Triple<>(VP_v, VD_v, I_j);
    }

}
