/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetPublicCredentials;
import ch.openchvote.core.electionauthority.writein.PublicData;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.set.IntSet;

@SuppressWarnings("MissingJavadoc")
public final class TE6 {

    static public boolean isReady(PublicData publicData) {
        var s = publicData.get_ES().get().get_SP().get_s();
        return publicData.get_bold_X_hat().arePresent(IntSet.range(1, s)) && publicData.get_bold_Y_hat().arePresent(IntSet.range(1, s)) && publicData.get_bold_Z_hat().arePresent(IntSet.range(1, s));
    }

    static public void
    run(PublicData publicData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // perform task
        var bold_X_hat = publicData.get_bold_X_hat().mapTo(Matrix::ofCols);
        var bold_Y_hat = publicData.get_bold_Y_hat().mapTo(Matrix::ofCols);
        var bold_Z_hat = publicData.get_bold_Z_hat().mapTo(Matrix::ofCols);
        var publicCredenitals = algorithmService.run(GetPublicCredentials.class, securityParameters, bold_X_hat, bold_Y_hat, bold_Z_hat);
        var bold_x_hat = publicCredenitals.get_bold_x_hat();
        var bold_y_hat = publicCredenitals.get_bold_y_hat();
        var bold_z_hat = publicCredenitals.get_bold_z_hat();

        // update event data
        publicData.get_bold_x_hat().set(bold_x_hat);
        publicData.get_bold_y_hat().set(bold_y_hat);
        publicData.get_bold_z_hat().set(bold_z_hat);
    }

}
