/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.CheckCredentialProof;
import ch.openchvote.core.electionauthority.writein.PublicData;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.core.protocol.parameters.SecurityParameters;

@SuppressWarnings("MissingJavadoc")
public final class TE5 {

    static public void
    run(int k, PublicData publicData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select event data
        var bold_x_hat_k = publicData.get_bold_X_hat().get(k);
        var bold_y_hat_k = publicData.get_bold_Y_hat().get(k);
        var bold_z_hat_k = publicData.get_bold_Z_hat().get(k);
        var pi_hat_k = publicData.get_bold_pi_hat().get(k);

        // perform task
        if (!algorithmService.run(CheckCredentialProof.class, securityParameters, pi_hat_k, bold_x_hat_k, bold_y_hat_k, bold_z_hat_k)) {
            throw new TaskException(TaskException.Type.INVALID_ZKP_PROOF, TE5.class);
        }
    }

}
