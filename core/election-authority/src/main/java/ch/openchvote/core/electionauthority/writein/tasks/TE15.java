/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.GetCombinedDecryptions;
import ch.openchvote.core.electionauthority.writein.PublicData;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;

@SuppressWarnings("MissingJavadoc")
public final class TE15 {

    static public boolean isReady(PublicData publicData) {
        var s = publicData.get_ES().get().get_SP().get_s();
        return publicData.get_bold_C().arePresent(IntSet.range(1, s)) && publicData.get_bold_pi_prime().arePresent(IntSet.range(1, s));
    }

    static public void
    run(PublicData publicData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select event data
        var z = publicData.get_bold_pk_prime().get().getLength();

        // perform task
        var bold_C = publicData.get_bold_C().mapTo(Matrix::ofCols);
        var arrow_bold_D = publicData.get_arrow_bold_D().mapTo(Vector::of);
        var pair = algorithmService.run(GetCombinedDecryptions.class, securityParameters, z, bold_C, arrow_bold_D);
        var bold_c = pair.getFirst();
        var bold_D = pair.getSecond();

        // update event data
        publicData.get_bold_c().set(bold_c);
        publicData.get_bold_D().set(bold_D);
    }

}
