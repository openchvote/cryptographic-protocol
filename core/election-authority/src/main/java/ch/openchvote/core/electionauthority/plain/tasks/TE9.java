/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.plain.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.CheckConfirmation;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetVotingDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.Confirmation;
import ch.openchvote.core.algorithms.protocols.common.model.Finalization;
import ch.openchvote.core.algorithms.protocols.common.model.VotingDescriptions;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.GetVotingParameters;
import ch.openchvote.core.algorithms.protocols.plain.model.VotingParametersPlain;
import ch.openchvote.core.electionauthority.plain.PublicData;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
public final class TE9 {

    static public Triple<VotingParametersPlain, VotingDescriptions, Finalization>
    run(int v, Confirmation gamma, PublicData publicData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // select event data
        var bold_y_hat = publicData.get_bold_y_hat().get();
        var bold_z_hat = publicData.get_bold_z_hat().get();
        var C_j = publicData.get_C_j();
        var F_j = publicData.get_F_j();

        // perform task
        if (C_j.isPresent(v)) {
            throw new TaskException(TaskException.Type.DUPLICATE_CONFIRMATION, TE9.class);
        }
        if (!F_j.isPresent(v)) {
            throw new TaskException(TaskException.Type.MISSING_BALLOT, TE9.class);
        }
        if (!algorithmService.run(CheckConfirmation.class, securityParameters, v, gamma, bold_y_hat, bold_z_hat)) {
            throw new TaskException(TaskException.Type.INVALID_CONFIRMATION, TE9.class);
        }
        var VP_v = algorithmService.run(GetVotingParameters.class, v, EP);
        var VD_v = algorithmService.run(GetVotingDescriptions.class, v, ED);
        var delta_j = F_j.get(v);
        C_j.set(v, gamma);

        // return data
        return new Triple<>(VP_v, VD_v, delta_j);
    }

}
