/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GenKeyPair;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.GenKeyPairProof;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.GenKeyPairs;
import ch.openchvote.core.electionauthority.writein.PublicData;
import ch.openchvote.core.electionauthority.writein.SecretData;
import ch.openchvote.core.protocol.parameters.SecurityParameters;

@SuppressWarnings("MissingJavadoc")
public final class TE2 {

    static public void
    run(int j, PublicData publicData, SecretData secretData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select election parameters
        var EP = publicData.get_EP().get();
        var bold_u = EP.get_bold_u();
        var bold_k = EP.get_bold_k();
        var bold_E = EP.get_bold_E();
        var bold_z = EP.get_bold_z();

        // perform task
        var keyPair = algorithmService.run(GenKeyPair.class, securityParameters);
        var sk_j = keyPair.get_sk();
        var pk_j = keyPair.get_pk();
        var keyPairs = algorithmService.run(GenKeyPairs.class, securityParameters, bold_k, bold_u, bold_E, bold_z);
        var bold_sk_prime_j = keyPairs.get_bold_sk();
        var bold_pk_prime_j = keyPairs.get_bold_pk();
        var pi_j = algorithmService.run(GenKeyPairProof.class, securityParameters, sk_j, pk_j, bold_sk_prime_j, bold_pk_prime_j);

        // update event data
        secretData.get_sk_j().set(sk_j);
        secretData.get_bold_sk_prime_j().set(bold_sk_prime_j);
        publicData.get_bold_pk().set(j, pk_j);
        publicData.get_bold_PK_prime().set(j, bold_pk_prime_j);
        publicData.get_bold_pi().set(j, pi_j);
    }

}
