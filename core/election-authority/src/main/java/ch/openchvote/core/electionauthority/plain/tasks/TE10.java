/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.plain.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.GetEncryptions;
import ch.openchvote.core.electionauthority.plain.PublicData;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.base.utilities.set.IndexedFamily;

@SuppressWarnings("MissingJavadoc")
public final class TE10 {

    static public void
    run(PublicData publicData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select election parameters
        var EP = publicData.get_EP().get();
        var bold_n = EP.get_bold_n();
        var bold_k = EP.get_bold_k();
        var bold_u = EP.get_bold_u();
        var bold_E = EP.get_bold_E();
        var bold_w = EP.get_bold_w();

        // select event data
        var B_j = publicData.get_B_j().mapTo(IndexedFamily::of);
        var C_j = publicData.get_C_j().mapTo(IndexedFamily::of);

        // perform task
        var bold_e_tilde_0 = algorithmService.run(GetEncryptions.class, securityParameters, B_j, C_j, bold_n, bold_k, bold_u, bold_w, bold_E);

        // update event data
        publicData.get_bold_E_tilde().set(0, bold_e_tilde_0);
    }

}
