/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.plain.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.GenDecryptionProof;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.GetDecryptions;
import ch.openchvote.core.electionauthority.plain.PublicData;
import ch.openchvote.core.electionauthority.plain.SecretData;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.base.utilities.set.IntSet;

@SuppressWarnings("MissingJavadoc")
public final class TE13 {

    static public boolean isReady(PublicData publicData) {
        var s = publicData.get_ES().get().get_SP().get_s();
        return publicData.get_bold_E_tilde().arePresent(IntSet.range(0, s)) && publicData.get_bold_pi_tilde().arePresent(IntSet.range(1, s));
    }

    static public void
    run(int j, PublicData publicData, SecretData secretData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();
        var s = ES.get_SP().get_s();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select event data
        var sk_j = secretData.get_sk_j().get();
        var pk_j = publicData.get_bold_pk().get(j);
        var bold_e_tilde_s = publicData.get_bold_E_tilde().get(s);

        // perform task
        var bold_c_j = algorithmService.run(GetDecryptions.class, securityParameters, bold_e_tilde_s, sk_j);
        var pi_prime_j = algorithmService.run(GenDecryptionProof.class, securityParameters, sk_j, pk_j, bold_e_tilde_s, bold_c_j);

        // update event data
        publicData.get_bold_C().set(j, bold_c_j);
        publicData.get_bold_pi_prime().set(j, pi_prime_j);
    }

}
