/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.tasks;

import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.CheckShuffleProof;
import ch.openchvote.core.electionauthority.writein.PublicData;
import ch.openchvote.core.protocol.parameters.SecurityParameters;

@SuppressWarnings("MissingJavadoc")
public final class TE12 {

    static public boolean isReady(int k, PublicData publicData) {
        return publicData.get_bold_E_tilde().isPresent(k - 1)
                && publicData.get_bold_E_tilde().isPresent(k)
                && publicData.get_bold_pi_tilde().isPresent(k);
    }

    static public void
    run(int k, PublicData publicData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select event data
        var pi_tilde_k = publicData.get_bold_pi_tilde().get(k);
        var bold_e_tilde_k_minus_1 = publicData.get_bold_E_tilde().get(k - 1);
        var bold_e_tilde_k = publicData.get_bold_E_tilde().get(k);
        var pk = publicData.get_pk().get();
        var bold_pk_prime = publicData.get_bold_pk_prime().get();

        // perform task
        if (!algorithmService.run(CheckShuffleProof.class, securityParameters, pi_tilde_k, bold_e_tilde_k_minus_1, bold_e_tilde_k, pk, bold_pk_prime)) {
            throw new TaskException(TaskException.Type.INVALID_ZKP_PROOF, TE12.class);
        }
    }

}
