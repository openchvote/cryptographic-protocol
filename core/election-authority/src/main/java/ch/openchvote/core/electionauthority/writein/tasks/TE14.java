/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.CheckDecryptionProof;
import ch.openchvote.core.electionauthority.writein.PublicData;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.core.protocol.parameters.SecurityParameters;

@SuppressWarnings("MissingJavadoc")
public final class TE14 {

    static public void
    run(int k, PublicData publicData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();
        var s = ES.get_SP().get_s();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select event data
        var bold_pi_prime_k = publicData.get_bold_pi_prime().get(k);
        var pk_k = publicData.get_bold_pk().get(k);
        var bold_pk_prime_k = publicData.get_bold_PK_prime().get(k);
        var bold_e_tilde_s = publicData.get_bold_E_tilde().get(s);
        var bold_c_k = publicData.get_bold_C().get(k);
        var bold_D_k = publicData.get_arrow_bold_D().get(k);

        // perform task
        if (!algorithmService.run(CheckDecryptionProof.class, securityParameters, bold_pi_prime_k, pk_k, bold_pk_prime_k, bold_e_tilde_s, bold_c_k, bold_D_k)) {
            throw new TaskException(TaskException.Type.INVALID_ZKP_PROOF, TE14.class);
        }
    }

}
