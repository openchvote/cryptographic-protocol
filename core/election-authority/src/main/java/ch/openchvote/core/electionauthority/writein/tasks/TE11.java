/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.GenShuffle;
import ch.openchvote.core.algorithms.protocols.writein.algorithms.GenShuffleProof;
import ch.openchvote.core.electionauthority.writein.PublicData;
import ch.openchvote.core.protocol.parameters.SecurityParameters;

@SuppressWarnings("MissingJavadoc")
public final class TE11 {

    static public boolean isReady(PublicData publicData, int j) {
        return publicData.get_bold_E_tilde().isPresent(j - 1);
    }

    static public void
    run(int j, PublicData publicData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select event data
        var pk = publicData.get_pk().get();
        var bold_pk_prime = publicData.get_bold_pk_prime().get();
        var bold_e_tilde_j_minus_1 = publicData.get_bold_E_tilde().get(j - 1);

        // perform task
        var quadruple = algorithmService.run(GenShuffle.class, securityParameters, bold_e_tilde_j_minus_1, pk, bold_pk_prime);
        var bold_e_tilde_j = quadruple.getFirst();
        var bold_r_tilde_j = quadruple.getSecond();
        var bold_r_tilde_prime_j = quadruple.getThird();
        var psi_j = quadruple.getFourth();
        var pi_tilde_j = algorithmService.run(GenShuffleProof.class, securityParameters, bold_e_tilde_j_minus_1, bold_e_tilde_j, bold_r_tilde_j, bold_r_tilde_prime_j, psi_j, pk, bold_pk_prime);

        // update event data
        publicData.get_bold_E_tilde().set(j, bold_e_tilde_j);
        publicData.get_bold_pi_tilde().set(j, pi_tilde_j);
    }

}
