/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.plain.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.electionauthority.plain.states.error.A14;
import ch.openchvote.core.electionauthority.plain.states.error.A15;
import ch.openchvote.core.electionauthority.plain.states.error.B14;
import ch.openchvote.core.electionauthority.plain.tasks.TE14;
import ch.openchvote.core.electionauthority.plain.tasks.TE15;
import ch.openchvote.core.electionauthority.ElectionAuthority;
import ch.openchvote.core.electionauthority.plain.EventContext;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Message;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Tallying;
import ch.openchvote.core.protocol.protocols.plain.content.message.MEA1;
import ch.openchvote.core.protocol.protocols.plain.content.message.MEE4;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Tallying.class)
public final class S420 extends State<ElectionAuthority, EventContext> {

    public S420(ElectionAuthority electionAuthority, EventContext eventContext) {
        super(electionAuthority, eventContext);
        this.registerMessageHandler(MEE4.class, this::handleMEE4);
    }

    private void handleMEE4(Message message) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var k = ES.get_SP().getIndexOf(message.getSenderId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var MEE4 = this.party.getAndCheckContent(MEE4.class, message, aux, securityLevel);

        // update public data
        publicData.setContent(k, MEE4);

        try {
            // perform task
            TE14.run(k, publicData);

            // run self activation
            var stateId = this.getId();
            this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A14.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B14.class);
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void handleSelfActivation() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SP = ES.get_SP();
        var s = SP.get_s();
        var AD = SP.get_AD();

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // check if all MEE4 messages are available
        if (TE15.isReady(publicData)) {
            try {
                // perform task
                TE15.run(publicData);

                // select event data
                var bold_c = publicData.get_bold_c().get();
                var bold_e_tilde_s = publicData.get_bold_E_tilde().get(s);

                // send MEA1 to administrator
                var aux = new Triple<>(ES, EP, ED);
                this.party.sendMessage(eventId, AD, new MEA1(bold_c, bold_e_tilde_s), aux, securityLevel);

                // update state
                this.party.updateState(this.eventContext, S500.class);

            } catch (Algorithm.Exception exception) {
                // move on to error state
                this.party.updateState(this.eventContext, A15.class);
            }
        }
    }

}
