/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.electionauthority.writein.tasks.TE11;
import ch.openchvote.core.electionauthority.writein.tasks.TE12;
import ch.openchvote.core.electionauthority.ElectionAuthority;
import ch.openchvote.core.electionauthority.writein.EventContext;
import ch.openchvote.core.electionauthority.writein.states.error.A12;
import ch.openchvote.core.electionauthority.writein.states.error.A11;
import ch.openchvote.core.electionauthority.writein.states.error.B12;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.communication.Message;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.protocols.writein.content.message.MEE3;
import ch.openchvote.core.protocol.phases.Tallying;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Tallying.class)
@Notify(Status.Type.READY)
public final class S400 extends State<ElectionAuthority, EventContext> {

    public S400(ElectionAuthority electionAuthority, EventContext eventContext) {
        super(electionAuthority, eventContext);
        this.registerMessageHandler(MEE3.class, this::handleMEE3);
    }

    @SuppressWarnings("unused")
    @Override
    public void handleStart() {

        // decompose event context
        var eventId = this.eventContext.getEventId();

        // run self activation
        var stateId = this.getId();
        this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

    }

    private void handleMEE3(Message message) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var k = ES.get_SP().getIndexOf(message.getSenderId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var MEE3 = this.party.getAndCheckContent(MEE3.class, message, aux, securityLevel);

        // update event data
        publicData.setContent(k, MEE3);

        try {
            // perform task
            if (TE12.isReady(k, publicData)) {
                TE12.run(k, publicData);
            }
            if (TE12.isReady(k + 1, publicData)) {
                TE12.run(k + 1, publicData);
            }

            // run self activation
            var stateId = this.getId();
            this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A12.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B12.class);
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void handleSelfActivation() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SP = ES.get_SP();
        var bold_ea = SP.get_bold_ea();
        var j = SP.getIndexOf(this.party.getId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // check if all MEE3 messages are available
        if (TE11.isReady(publicData, j)) {
            try {
                // perform task
                TE11.run(j, publicData);

                // select event data
                var bold_e_tilde_j = publicData.get_bold_E_tilde().get(j);
                var pi_tilde_j = publicData.get_bold_pi_tilde().get(j);

                // send MEE3 to election authorities
                var aux = new Triple<>(ES, EP, ED);
                this.party.sendMessage(eventId, bold_ea, new MEE3(bold_e_tilde_j, pi_tilde_j), aux, securityLevel);

                // update state
                this.party.updateState(this.eventContext, S410.class);

                // run self activation
                var stateId = this.getId();
                this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

            } catch (Algorithm.Exception exception) {
                // move on to error state
                this.party.updateState(this.eventContext, A11.class);
            }
        }
    }

}
