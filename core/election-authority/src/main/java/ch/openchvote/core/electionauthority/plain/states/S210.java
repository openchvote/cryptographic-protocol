/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.plain.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.electionauthority.plain.states.error.*;
import ch.openchvote.core.electionauthority.plain.tasks.TE1;
import ch.openchvote.core.electionauthority.plain.tasks.TE3;
import ch.openchvote.core.electionauthority.plain.tasks.TE4;
import ch.openchvote.core.electionauthority.plain.tasks.TE5;
import ch.openchvote.core.electionauthority.ElectionAuthority;
import ch.openchvote.core.electionauthority.plain.EventContext;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Message;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.core.protocol.phases.Preparation;
import ch.openchvote.core.protocol.protocols.plain.content.message.MEE1;
import ch.openchvote.core.protocol.protocols.plain.content.message.MEE2;
import ch.openchvote.core.protocol.protocols.plain.content.message.MEP1;
import ch.openchvote.base.utilities.tools.VMGJFassade;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Preparation.class)
public final class S210 extends State<ElectionAuthority, EventContext> {

    public S210(ElectionAuthority electionAuthority, EventContext eventContext) {
        super(electionAuthority, eventContext);
        this.registerMessageHandler(MEE1.class, this::handleMEE1);
        this.registerMessageHandler(MEE2.class, this::handleMEE2);
    }

    private void handleMEE1(Message message) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var k = ES.get_SP().getIndexOf(message.getSenderId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var MEE1 = this.party.getAndCheckContent(MEE1.class, message, aux, securityLevel);

        // update public data
        publicData.setContent(k, MEE1);

        try {
            // perform task
            TE1.run(k, publicData);

            // run self activation
            var stateId = this.getId();
            this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A1.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B1.class);
        }
    }

    private void handleMEE2(Message message) {

        // decompose event context
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var k = ES.get_SP().getIndexOf(message.getSenderId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var MEE2 = this.party.getAndCheckContent(MEE2.class, message, aux, securityLevel);

        // update public data
        publicData.setContent(k, MEE2);

        try {
            // perform task
            TE5.run(k, publicData);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A5.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B5.class);
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void handleSelfActivation() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SP = ES.get_SP();
        var PA = SP.get_PA();
        var bold_ea = SP.get_bold_ea();
        var j = SP.getIndexOf(this.party.getId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();
        var N_E = EP.get_N_E();

        // check if all MEE1 messages are available
        if (TE3.isReady(publicData)) {
            try {
                // perform task
                TE3.run(publicData);

                // select event data
                var pk = publicData.get_pk().get();

                // create precomputation table (numbers taken from Table 12.5)
                var securityParameters = new SecurityParameters(securityLevel);
                var p = securityParameters.get_p();
                var q = securityParameters.get_q();
                VMGJFassade.precomputeTable(pk, p, q, 3 * N_E);

                try {
                    // perform task
                    TE4.run(j, publicData, secretData);

                    // select event data
                    var bold_d_j = secretData.get_bold_d_j().get();
                    var bold_x_hat_j = publicData.get_bold_X_hat().get(j);
                    var bold_y_hat_j = publicData.get_bold_Y_hat().get(j);
                    var bold_z_hat_j = publicData.get_bold_Z_hat().get(j);
                    var pi_hat_j = publicData.get_bold_pi_hat().get(j);

                    // send MEP1 to printing authority
                    var aux = new Triple<>(ES, EP, ED);
                    this.party.sendMessage(eventId, PA, new MEP1(bold_d_j), aux, securityLevel);

                    // send MEE2 to election authorities
                    this.party.sendMessage(eventId, bold_ea, new MEE2(bold_x_hat_j, bold_y_hat_j, bold_z_hat_j, pi_hat_j), aux, securityLevel);

                    // update state
                    this.party.updateState(this.eventContext, S220.class);

                    // run self activation
                    var stateId = this.getId();
                    this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

                } catch (Algorithm.Exception exception) {
                    // move to error state
                    this.party.updateState(this.eventContext, A4.class);
                }
            } catch (Algorithm.Exception exception) {
                // move to error state
                this.party.updateState(this.eventContext, A3.class);
            }
        }
    }

}
