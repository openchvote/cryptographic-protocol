/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.electionauthority.writein.tasks.TE10;
import ch.openchvote.core.electionauthority.writein.tasks.TE7;
import ch.openchvote.core.electionauthority.ElectionAuthority;
import ch.openchvote.core.electionauthority.writein.EventContext;
import ch.openchvote.core.electionauthority.writein.states.error.A10;
import ch.openchvote.core.electionauthority.writein.tasks.TE8;
import ch.openchvote.core.electionauthority.writein.tasks.TE9;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Request;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.exceptions.CommunicationException;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Election;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.request.RCE1;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.request.RCE2;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.request.RCE3;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response.REC1;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response.REC2;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response.REC3;
import ch.openchvote.base.utilities.tuples.Triple;

import static ch.openchvote.base.framework.exceptions.CommunicationException.Type.INVALID_CONTENT;

@SuppressWarnings("MissingJavadoc")
@Phase(Election.class)
@Notify(Status.Type.READY)
public final class S300 extends State<ElectionAuthority, EventContext> {

    public S300(ElectionAuthority electionAuthority, EventContext eventContext) {
        super(electionAuthority, eventContext);
        this.registerRequestHandler(RCE1.class, this::handleRCE1);
        this.registerRequestHandler(RCE2.class, this::handleRCE2);
        this.registerRequestHandler(RCE3.class, this::handleRCE3);
    }

    private void handleRCE1(Request request) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var j = ES.get_SP().getIndexOf(this.party.getId());

        // get request content
        var RCE1 = this.party.getContent(RCE1.class, request);
        var v = RCE1.get_v();

        try {

            // perform task
            var pair = TE7.run(v, publicData);
            var VP_v = pair.getFirst();
            var VD_v = pair.getSecond();

            // select event data
            var pk_j = publicData.get_bold_pk().get(j);
            var bold_pk_prime_j = publicData.get_bold_PK_prime().get(j);
            var pi_j = publicData.get_bold_pi().get(j);

            // send REC1 to voting client
            var aux = new Triple<>(ES, VP_v, VD_v);
            this.party.sendResponse(eventId, request.getRequestId(), request.getRequesterId(), new REC1(pk_j, bold_pk_prime_j, pi_j), aux, securityLevel);

        } catch (Algorithm.Exception exception) {
            // discard request
            throw new CommunicationException(INVALID_CONTENT, request, exception);
        }
    }

    private void handleRCE2(Request request) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();

        // get request content
        var RCE2 = this.party.getContent(RCE2.class, request);
        var v = RCE2.get_v();
        var alpha = RCE2.get_alpha();

        try {
            // perform task
            var triple = TE8.run(v, alpha, publicData);
            var VP_v = triple.getFirst();
            var VD_v = triple.getSecond();
            var beta_j = triple.getThird();

            // send REC2 to voting client
            var aux = new Triple<>(ES, VP_v, VD_v);
            this.party.sendResponse(eventId, request.getRequestId(), request.getRequesterId(), new REC2(beta_j), aux, securityLevel);

        } catch (TaskException | Algorithm.Exception exception) {
            // discard request
            throw new CommunicationException(INVALID_CONTENT, request, exception);
        }
    }

    private void handleRCE3(Request request) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();

        // get request content
        var RCE3 = this.party.getContent(RCE3.class, request);
        var v = RCE3.get_v();
        var gamma = RCE3.get_gamma();

        try {
            // perform task
            var triple = TE9.run(v, gamma, publicData);
            var VP_v = triple.getFirst();
            var VD_v = triple.getSecond();
            var delta_j = triple.getThird();

            // send REC3 to voting client
            var aux = new Triple<>(ES, VP_v, VD_v);
            this.party.sendResponse(eventId, request.getRequestId(), request.getRequesterId(), new REC3(delta_j), aux, securityLevel);

        } catch (TaskException | Algorithm.Exception exception) {
            // discard request
            throw new CommunicationException(INVALID_CONTENT, request, exception);
        }
    }

    @Override
    public void handleStop() {

        // decompose event context
        var publicData = this.eventContext.getPublicData();

        try {
            // perform task
            TE10.run(publicData);

            // update state
            this.party.updateState(this.eventContext, S400.class);

        } catch (Algorithm.Exception exception) {
            // move on to error state
            this.party.updateState(this.eventContext, A10.class);
        }
    }

}
