/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein;

import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.core.algorithms.protocols.common.model.*;
import ch.openchvote.core.algorithms.protocols.writein.model.*;
import ch.openchvote.core.protocol.protocols.writein.content.message.*;
import ch.openchvote.base.utilities.sequence.ByteArray;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Tuple;
import ch.openchvote.base.utilities.tuples.viguple.SeptenViguple;

import java.math.BigInteger;

/**
 * Instances of this class represent the election authority's public data. The class inherits from the generic {@link Tuple}
 * subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects to store. 
 * Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class PublicData extends SeptenViguple<
        // event setup and election parameters
        EventData.Data<EventSetup>, // ES
        EventData.Data<ElectionParametersWriteIn>, // EP
        EventData.Data<ElectionDescriptions>, // ED
        // public keys
        EventData.DataMap<BigInteger>, // bold_pk
        EventData.DataMap<Vector<BigInteger>>, // bold_PK_prime
        EventData.Data<BigInteger>, // pk
        EventData.Data<Vector<BigInteger>>, // bold_pk_prime
        EventData.DataMap<KeyPairProof>, // bold_pi
        // polynomials
        EventData.Data<Vector<ByteArray>>, // bold_a_j
        EventData.Data<Matrix<Point>>, // bold_P_j
        // public credentials
        EventData.DataMap<Vector<BigInteger>>, // bold_X_hat
        EventData.DataMap<Vector<BigInteger>>, // bold_Y_hat
        EventData.DataMap<Vector<BigInteger>>, // bold_Z_hat
        EventData.DataMap<CredentialProof>, // bold_pi_hat
        EventData.Data<Vector<BigInteger>>, // bold_x_hat
        EventData.Data<Vector<BigInteger>>, // bold_y_hat
        EventData.Data<Vector<BigInteger>>, // bold_z_hat
        // ballots, confirmations, finalizations
        EventData.DataMap<Ballot>, // B_j
        EventData.DataMap<Confirmation>, // C_j
        EventData.DataMap<Finalization>, // F_j
        // shuffle
        EventData.DataMap<Vector<AugmentedEncryption>>, // bold_E_tilde
        EventData.DataMap<ShuffleProof>, // bold_pi_tilde
        // decryption
        EventData.DataMap<Vector<BigInteger>>, // bold_C
        EventData.DataMap<Matrix<BigInteger>>, // arrow_bold_D
        EventData.DataMap<DecryptionProof>, // bold_pi_prime
        EventData.Data<Vector<BigInteger>>, // bold_c
        EventData.Data<Matrix<BigInteger>>> // bold_D
        implements EventData {

    public PublicData() {
        this(new Data<>(), new Data<>(), new Data<>(), new DataMap<>(), new DataMap<>(), new Data<>(), new Data<>(), new DataMap<>(), new Data<>(), new Data<>(), new DataMap<>(), new DataMap<>(), new DataMap<>(), new DataMap<>(), new Data<>(), new Data<>(), new Data<>(), new DataMap<>(), new DataMap<>(), new DataMap<>(), new DataMap<>(), new DataMap<>(), new DataMap<>(), new DataMap<>(), new DataMap<>(), new Data<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private PublicData(Data<EventSetup> ES, Data<ElectionParametersWriteIn> EP, Data<ElectionDescriptions> ED, DataMap<BigInteger> bold_pk, DataMap<Vector<BigInteger>> bold_PK_prime, Data<BigInteger> pk, Data<Vector<BigInteger>> bold_pk_prime, DataMap<KeyPairProof> bold_pi, Data<Vector<ByteArray>> bold_a_j, Data<Matrix<Point>> bold_P_j, DataMap<Vector<BigInteger>> bold_X_hat, DataMap<Vector<BigInteger>> bold_Y_hat, DataMap<Vector<BigInteger>> bold_Z_hat, DataMap<CredentialProof> bold_pi_hat, Data<Vector<BigInteger>> bold_x_hat, Data<Vector<BigInteger>> bold_y_hat, Data<Vector<BigInteger>> bold_z_hat, DataMap<Ballot> B_j, DataMap<Confirmation> C_j, DataMap<Finalization> F_j, DataMap<Vector<AugmentedEncryption>> bold_E_tilde, DataMap<ShuffleProof> bold_pi_tilde, DataMap<Vector<BigInteger>> bold_C, DataMap<Matrix<BigInteger>> arrow_bold_D, DataMap<DecryptionProof> bold_pi_prime, Data<Vector<BigInteger>> bold_c, Data<Matrix<BigInteger>> bold_D) {
        super(ES, EP, ED, bold_pk, bold_PK_prime, pk, bold_pk_prime, bold_pi, bold_a_j, bold_P_j, bold_X_hat, bold_Y_hat, bold_Z_hat, bold_pi_hat, bold_x_hat, bold_y_hat, bold_z_hat, B_j, C_j, F_j, bold_E_tilde, bold_pi_tilde, bold_C, arrow_bold_D, bold_pi_prime, bold_c, bold_D);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private PublicData(PublicData publicData) {
        this(publicData.get_ES(), publicData.get_EP(), publicData.get_ED(), publicData.get_bold_pk(), publicData.get_bold_PK_prime(), publicData.get_pk(), publicData.get_bold_pk_prime(), publicData.get_bold_pi(), publicData.get_bold_a_j(), publicData.get_bold_P_j(), publicData.get_bold_X_hat(), publicData.get_bold_Y_hat(), publicData.get_bold_Z_hat(), publicData.get_bold_pi_hat(), publicData.get_bold_x_hat(), publicData.get_bold_y_hat(), publicData.get_bold_z_hat(), publicData.get_B_j(), publicData.get_C_j(), publicData.get_F_j(), publicData.get_bold_E_tilde(), publicData.get_bold_pi_tilde(), publicData.get_bold_C(), publicData.get_arrow_bold_D(), publicData.get_bold_pi_prime(), publicData.get_bold_c(), publicData.get_bold_D());
    }

    public Data<EventSetup> get_ES() {
        return this.getFirst();
    }

    public Data<ElectionParametersWriteIn> get_EP() {
        return this.getSecond();
    }

    public Data<ElectionDescriptions> get_ED() {
        return this.getThird();
    }

    public DataMap<BigInteger> get_bold_pk() {
        return getFourth();
    }

    public DataMap<Vector<BigInteger>> get_bold_PK_prime() {
        return this.getFifth();
    }

    public Data<BigInteger> get_pk() {
        return this.getSixth();
    }

    public Data<Vector<BigInteger>> get_bold_pk_prime() {
        return this.getSeventh();
    }

    public DataMap<KeyPairProof> get_bold_pi() {
        return this.getEighth();
    }

    public Data<Vector<ByteArray>> get_bold_a_j() {
        return this.getNinth();
    }

    public Data<Matrix<Point>> get_bold_P_j() {
        return this.getTenth();
    }

    public DataMap<Vector<BigInteger>> get_bold_X_hat() {
        return this.getEleventh();
    }

    public DataMap<Vector<BigInteger>> get_bold_Y_hat() {
        return this.getTwelfth();
    }

    public DataMap<Vector<BigInteger>> get_bold_Z_hat() {
        return this.getThirteenth();
    }

    public DataMap<CredentialProof> get_bold_pi_hat() {
        return this.getFourteenth();
    }

    public Data<Vector<BigInteger>> get_bold_x_hat() {
        return this.getFifteenth();
    }

    public Data<Vector<BigInteger>> get_bold_y_hat() {
        return this.getSixteenth();
    }

    public Data<Vector<BigInteger>> get_bold_z_hat() {
        return this.getSeventeenth();
    }

    public DataMap<Ballot> get_B_j() {
        return this.getEighteenth();
    }

    public DataMap<Confirmation> get_C_j() {
        return this.getNineteenth();
    }

    public DataMap<Finalization> get_F_j() {
        return this.getTwentieth();
    }

    public DataMap<Vector<AugmentedEncryption>> get_bold_E_tilde() {
        return this.getTwentyFirst();
    }

    public DataMap<ShuffleProof> get_bold_pi_tilde() {
        return this.getTwentySecond();
    }

    public DataMap<Vector<BigInteger>> get_bold_C() {
        return this.getTwentyThird();
    }

    public DataMap<Matrix<BigInteger>> get_arrow_bold_D() {
        return this.getTwentyFourth();
    }

    public DataMap<DecryptionProof> get_bold_pi_prime() {
        return this.getTwentyFifth();
    }

    public Data<Vector<BigInteger>> get_bold_c() {
        return this.getTwentySixth();
    }

    public Data<Matrix<BigInteger>> get_bold_D() {
        return this.getTwentySeventh();
    }

    /**
     * Calling this method stores the given content of type {@link MEE1} into the election authority's public data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, MEE1 content) {
        this.get_bold_pk().set(index, content.get_pk());
        this.get_bold_PK_prime().set(index, content.get_bold_pk_prime());
        this.get_bold_pi().set(index, content.get_pi());
    }

    /**
     * Calling this method stores the given content of type {@link MEE2} into the election authority's public data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, MEE2 content) {
        this.get_bold_X_hat().set(index, content.get_bold_x_hat());
        this.get_bold_Y_hat().set(index, content.get_bold_y_hat());
        this.get_bold_Z_hat().set(index, content.get_bold_z_hat());
        this.get_bold_pi_hat().set(index, content.get_pi_hat());
    }

    /**
     * Calling this method stores the given content of type {@link MEE3} into the election authority's public data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, MEE3 content) {
        this.get_bold_E_tilde().set(index, content.get_bold_e_tilde());
        this.get_bold_pi_tilde().set(index, content.get_pi_tilde());
    }

    /**
     * Calling this method stores the given content of type {@link MEE4} into the election authority's public data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, MEE4 content) {
        this.get_bold_C().set(index, content.get_bold_c());
        this.get_arrow_bold_D().set(index, content.get_bold_D());
        this.get_bold_pi_prime().set(index, content.get_pi_prime());
    }

    /**
     * Calling this method stores the given content of type {@link MAE1} into the election authority's public data.
     *
     * @param content The given content
     */
    public void setContent(MAE1 content) {
        this.get_ES().set(content.get_ES());
        this.get_EP().set(content.get_EP());
        this.get_ED().set(content.get_ED());
    }

    /**
     * Calling this method stores the given content of type {@link MAE2} into the election authority's public data.
     *
     * @param content The given content
     */
    public void setContent(MAE2 content) {
        this.get_bold_pk().set(0, content.get_pk());
        this.get_bold_PK_prime().set(0, content.get_bold_pk_prime());
        this.get_bold_pi().set(0, content.get_pi());
    }

}
