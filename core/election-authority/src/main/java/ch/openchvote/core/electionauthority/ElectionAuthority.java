/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority;

import ch.openchvote.core.electionauthority.plain.PublicData;
import ch.openchvote.base.framework.annotations.party.Role;
import ch.openchvote.base.framework.annotations.party.SignatureKeys;
import ch.openchvote.base.framework.party.Party;
import ch.openchvote.base.framework.services.MessagingService;
import ch.openchvote.base.framework.services.RequestResponseService;

/**
 * This class implements the 'Election Authority' party of the CHVote protocol. It is a direct subclass of {@link Party}
 * with no particular extensions. The specific role of the election authority in the protocol is implemented in the
 * classes {@link PublicData} (plain protocol) and {@link ch.openchvote.core.electionauthority.writein.PublicData} (write-in
 * protocol) and in corresponding state and task classes.
 */
@SignatureKeys
@Role(ch.openchvote.core.protocol.roles.ElectionAuthority.class)
public class ElectionAuthority extends Party {

    /**
     * Constructs a new instance of this class.
     *
     * @param id The id of this election authority
     */
    public ElectionAuthority(String id) {
        super(id);
    }

    @Override
    public void subscribeToServices() {
        super.subscribeToServices();

        // subscribe to messaging service and to request/response service as responder
        this.getService(MessagingService.Target.class).subscribe(this);
        this.getService(RequestResponseService.Target.class).subscribe(this);
    }

    @Override
    public void unsubscribeFromServices() {
        super.unsubscribeFromServices();

        // unsubscribe from messaging service and from request/response service as responder
        this.getService(MessagingService.Target.class).unsubscribe(this);
        this.getService(RequestResponseService.Target.class).unsubscribe(this);
    }

}
