/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GenCredentialProof;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GenElectorateData;
import ch.openchvote.core.electionauthority.writein.PublicData;
import ch.openchvote.core.electionauthority.writein.SecretData;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.core.protocol.protocols.writein.UsabilityParameters;

@SuppressWarnings("MissingJavadoc")
public final class TE4 {

    static public void
    run(int j, PublicData publicData, SecretData secretData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();
        var UC = ES.get_UC();

        // get security and usability parameters
        var securityParameters = new SecurityParameters(SL);
        var usabilityParameters = new UsabilityParameters(UC, SL);

        // select election parameters
        var EP = publicData.get_EP().get();
        var bold_u = EP.get_bold_u();
        var bold_n = EP.get_bold_n();
        var bold_k = EP.get_bold_k();
        var bold_E = EP.get_bold_E();

        // perform task
        var electorateData = algorithmService.run(GenElectorateData.class, securityParameters, usabilityParameters, bold_n, bold_k, bold_u, bold_E);
        var bold_a_j = electorateData.get_bold_a();
        var bold_d_j = electorateData.get_bold_d();
        var bold_x_j = electorateData.get_bold_x();
        var bold_y_j = electorateData.get_bold_y();
        var bold_z_j = electorateData.get_bold_z();
        var bold_x_hat_j = electorateData.get_bold_x_hat();
        var bold_y_hat_j = electorateData.get_bold_y_hat();
        var bold_z_hat_j = electorateData.get_bold_z_hat();
        var bold_P_j = electorateData.get_bold_P();
        var pi_hat_j = algorithmService.run(GenCredentialProof.class, securityParameters, bold_x_j, bold_y_j, bold_z_j, bold_x_hat_j, bold_y_hat_j, bold_z_hat_j);

        // update event data
        publicData.get_bold_a_j().set(bold_a_j);
        secretData.get_bold_d_j().set(bold_d_j);
        publicData.get_bold_X_hat().set(j, bold_x_hat_j);
        publicData.get_bold_Y_hat().set(j, bold_y_hat_j);
        publicData.get_bold_Z_hat().set(j, bold_z_hat_j);
        publicData.get_bold_pi_hat().set(j, pi_hat_j);
        publicData.get_bold_P_j().set(bold_P_j);
    }

}
