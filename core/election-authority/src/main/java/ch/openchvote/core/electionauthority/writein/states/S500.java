/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.states;

import ch.openchvote.core.algorithms.protocols.writein.model.PublicDataElectionAuthority;
import ch.openchvote.core.electionauthority.ElectionAuthority;
import ch.openchvote.core.electionauthority.writein.EventContext;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Verification;
import ch.openchvote.core.protocol.protocols.writein.content.publication.PEP1;
import ch.openchvote.base.utilities.set.IndexedFamily;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Verification.class)
@Notify(Status.Type.READY)
public final class S500 extends State<ElectionAuthority, EventContext> {

    public S500(ElectionAuthority administrator, EventContext eventContext) {
        super(administrator, eventContext);
    }

    @SuppressWarnings("unused")
    @Override
    public void handleStart() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SP = ES.get_SP();
        var j = SP.getIndexOf(this.party.getId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get event data
        var pk_j = publicData.get_bold_pk().get(j);
        var bold_pk_prime_j = publicData.get_bold_PK_prime().get(j);
        var pi_j = publicData.get_bold_pi().get(j);
        var bold_x_hat_j = publicData.get_bold_X_hat().get(j);
        var bold_y_hat_j = publicData.get_bold_Y_hat().get(j);
        var bold_z_hat_j = publicData.get_bold_Z_hat().get(j);
        var pi_hat_j = publicData.get_bold_pi_hat().get(j);
        var B_j = publicData.get_B_j().mapTo(IndexedFamily::of);
        var C_j = publicData.get_C_j().mapTo(IndexedFamily::of);
        var bold_e_tilde_j = publicData.get_bold_E_tilde().get(j);
        var pi_tilde = publicData.get_bold_pi_tilde().get(j);
        var bold_c_j = publicData.get_bold_C().get(j);
        var bold_D_j = publicData.get_arrow_bold_D().get(j);
        var pi_prime_j = publicData.get_bold_pi_prime().get(j);

        // publish PAP2
        var PD = new PublicDataElectionAuthority(pk_j, bold_pk_prime_j, pi_j, bold_x_hat_j, bold_y_hat_j, bold_z_hat_j, pi_hat_j, bold_e_tilde_j, pi_tilde, bold_c_j, bold_D_j, pi_prime_j, B_j, C_j);
        var aux = new Triple<>(ES, EP, ED);
        this.party.sendPublication(eventId, new PEP1(PD), aux, securityLevel);

        // update state
        this.party.updateState(this.eventContext, S600.class);

    }

}
