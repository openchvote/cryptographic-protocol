/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.plain;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionCardData;
import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Pair;
import ch.openchvote.base.utilities.tuples.Tuple;

import java.math.BigInteger;

/**
 * Instances of this class represent the election authority's secret data. The class inherits from the generic
 * {@link Tuple} subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects
 * to store. Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class SecretData extends Pair<
        // private key
        EventData.Data<BigInteger>, // sk_j
        // election card data
        EventData.Data<Vector<ElectionCardData>>> // bold_d_j
        implements EventData {

    public SecretData() {
        this(new Data<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private SecretData(Data<BigInteger> sk_j, Data<Vector<ElectionCardData>> bold_d_j) {
        super(sk_j, bold_d_j);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private SecretData(SecretData secretData) {
        this(secretData.get_sk_j(), secretData.get_bold_d_j());
    }

    public Data<BigInteger> get_sk_j() {
        return this.getFirst();
    }

    public Data<Vector<ElectionCardData>> get_bold_d_j() {
        return this.getSecond();
    }

}
