/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.electionauthority.ElectionAuthority;
import ch.openchvote.core.electionauthority.writein.EventContext;
import ch.openchvote.core.electionauthority.writein.states.error.A5;
import ch.openchvote.core.electionauthority.writein.states.error.A6;
import ch.openchvote.core.electionauthority.writein.states.error.B5;
import ch.openchvote.core.electionauthority.writein.tasks.TE5;
import ch.openchvote.core.electionauthority.writein.tasks.TE6;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Message;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.protocols.writein.content.message.MEE2;
import ch.openchvote.core.protocol.phases.Preparation;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Preparation.class)
public final class S220 extends State<ElectionAuthority, EventContext> {

    public S220(ElectionAuthority electionAuthority, EventContext eventContext) {
        super(electionAuthority, eventContext);
        this.registerMessageHandler(MEE2.class, this::handleMEE2);
    }

    private void handleMEE2(Message message) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var k = ES.get_SP().getIndexOf(message.getSenderId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var MEE2 = this.party.getAndCheckContent(MEE2.class, message, aux, securityLevel);

        // update public data
        publicData.setContent(k, MEE2);

        try {
            // perform task
            TE5.run(k, publicData);

            // run self activation
            var stateId = this.getId();
            this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A5.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B5.class);
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void handleSelfActivation() {

        // decompose event context
        var publicData = this.eventContext.getPublicData();

        // check if all MEE2 messages are available
        if (TE6.isReady(publicData)) {
            try {
                // perform task
                TE6.run(publicData);

                // update state
                this.party.updateState(this.eventContext, S300.class);

            } catch (Algorithm.Exception exception) {
                // move to error state
                this.party.updateState(this.eventContext, A6.class);
            }
        }
    }

}
