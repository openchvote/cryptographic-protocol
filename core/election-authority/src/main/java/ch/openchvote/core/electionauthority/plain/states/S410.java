/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.plain.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.electionauthority.plain.states.error.*;
import ch.openchvote.core.electionauthority.plain.tasks.TE12;
import ch.openchvote.core.electionauthority.plain.tasks.TE13;
import ch.openchvote.core.electionauthority.plain.tasks.TE14;
import ch.openchvote.core.electionauthority.ElectionAuthority;
import ch.openchvote.core.electionauthority.plain.EventContext;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.communication.Message;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.core.protocol.protocols.plain.content.message.MEE3;
import ch.openchvote.core.protocol.protocols.plain.content.message.MEE4;
import ch.openchvote.core.protocol.phases.Tallying;
import ch.openchvote.base.utilities.tools.VMGJFassade;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Tallying.class)
public final class S410 extends State<ElectionAuthority, EventContext> {

    public S410(ElectionAuthority electionAuthority, EventContext eventContext) {
        super(electionAuthority, eventContext);
        this.registerMessageHandler(MEE3.class, this::handleMEE3);
        this.registerMessageHandler(MEE4.class, this::handleMEE4);
    }

    private void handleMEE3(Message message) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var k = ES.get_SP().getIndexOf(message.getSenderId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var MEE3 = this.party.getAndCheckContent(MEE3.class, message, aux, securityLevel);

        // update public data
        publicData.setContent(k, MEE3);

        try {
            // perform task
            if (TE12.isReady(k, publicData)) {
                TE12.run(k, publicData);
            }
            if (TE12.isReady(k + 1, publicData)) {
                TE12.run(k + 1, publicData);
            }

            // run self activation
            var stateId = this.getId();
            this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A12.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B12.class);
        }
    }

    private void handleMEE4(Message message) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var k = ES.get_SP().getIndexOf(message.getSenderId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var MEE4 = this.party.getAndCheckContent(MEE4.class, message,aux, securityLevel);

        // update event data
        publicData.setContent(k, MEE4);

        try {
            // perform task
            TE14.run(k, publicData);

            // run self activation
            var stateId = this.getId();
            this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A14.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B14.class);
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void handleSelfActivation() {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SP = ES.get_SP();
        var bold_ea = SP.get_bold_ea();
        var j = SP.getIndexOf(this.party.getId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // check if all MEE3 messages are available
        if (TE13.isReady(publicData)) {

            // select event data
            var pk = publicData.get_pk().get();

            // release precomputation table
            var securityParameters = new SecurityParameters(securityLevel);
            var p = securityParameters.get_p();
            VMGJFassade.releasePrecomputationTable(pk, p);

            try {
                // perform task
                TE13.run(j, publicData, secretData);

                // select event data
                var bold_c_j = publicData.get_bold_C().get(j);
                var pi_prime_j = publicData.get_bold_pi_prime().get(j);

                // send MEE4 to election authorities
                var aux = new Triple<>(ES, EP, ED);
                this.party.sendMessage(eventId, bold_ea, new MEE4(bold_c_j, pi_prime_j), aux, securityLevel);

                // update state
                this.party.updateState(this.eventContext, S420.class);

                // run self activation
                var stateId = this.getId();
                this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

            } catch (Algorithm.Exception exception) {
                // move on to error state
                this.party.updateState(this.eventContext, A13.class);
            }
        }
    }

}
