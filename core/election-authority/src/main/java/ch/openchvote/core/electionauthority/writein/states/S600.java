/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.electionauthority.ElectionAuthority;
import ch.openchvote.core.electionauthority.writein.EventContext;
import ch.openchvote.core.electionauthority.writein.tasks.TE16;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Request;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.exceptions.CommunicationException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.phases.Inspection;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.request.RIE1;
import ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response.REI1;
import ch.openchvote.base.utilities.tuples.Triple;

import static ch.openchvote.base.framework.exceptions.CommunicationException.Type.INVALID_CONTENT;

@SuppressWarnings("MissingJavadoc")
@Phase(Inspection.class)
@Notify(Status.Type.READY)
public final class S600 extends State<ElectionAuthority, EventContext> {

    public S600(ElectionAuthority electionAuthority, EventContext eventContext) {
        super(electionAuthority, eventContext);
        this.registerRequestHandler(RIE1.class, this::handleRIE1);
    }

    private void handleRIE1(Request request) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();

        // get request content
        var RIE1 = this.party.getContent(RIE1.class, request);
        var v = RIE1.get_v();

        try {
            // perform task
            var triple = TE16.run(v, publicData);
            var VP_v = triple.getFirst();
            var VD_v = triple.getSecond();
            var I_j = triple.getThird();

            // send REI1 to inspection client
            var aux = new Triple<>(ES, VP_v, VD_v);
            this.party.sendResponse(eventId, request.getRequestId(), request.getRequesterId(), new REI1(I_j), aux, securityLevel);

        } catch (Algorithm.Exception exception) {
            // discard request
            throw new CommunicationException(INVALID_CONTENT, request, exception);
        }
    }

    @Override
    public void handleStop() {

        // update state
        this.party.updateState(this.eventContext, FINAL.class);

    }

}
