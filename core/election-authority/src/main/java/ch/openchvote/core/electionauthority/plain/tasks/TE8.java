/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.plain.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GenResponse;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetVotingDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.Response;
import ch.openchvote.core.algorithms.protocols.common.model.VotingDescriptions;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.CheckBallot;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.GetVotingParameters;
import ch.openchvote.core.algorithms.protocols.plain.model.Ballot;
import ch.openchvote.core.algorithms.protocols.plain.model.VotingParametersPlain;
import ch.openchvote.core.electionauthority.plain.PublicData;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
public final class TE8 {

    static public Triple<VotingParametersPlain, VotingDescriptions, Response>
    run(int v, Ballot alpha, PublicData publicData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security parameters
        var securityParameters = new SecurityParameters(SL);

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // select event data
        var pk = publicData.get_pk().get();
        var bold_x_hat = publicData.get_bold_x_hat().get();
        var bold_P_j = publicData.get_bold_P_j().get();
        var B_j = publicData.get_B_j();
        var F_j = publicData.get_F_j();

        // perform task
        if (B_j.isPresent(v)) {
            throw new TaskException(TaskException.Type.DUPLICATE_BALLOT, TE8.class);
        }
        var bold_n = EP.get_bold_n();
        var bold_k = EP.get_bold_k();
        var bold_u = EP.get_bold_u();
        var bold_E = EP.get_bold_E();
        if (!algorithmService.run(CheckBallot.class, securityParameters, v, alpha, pk, bold_k, bold_u, bold_E, bold_x_hat)) {
            throw new TaskException(TaskException.Type.INVALID_BALLOT, TE8.class);
        }
        var bold_a = alpha.get_bold_a();
        var pair = algorithmService.run(GenResponse.class, securityParameters, v, bold_a, pk, bold_n, bold_k, bold_u, bold_E, bold_P_j);
        var beta_j = pair.getFirst();
        var delta_j = pair.getSecond();
        var VP_v = algorithmService.run(GetVotingParameters.class, v, EP);
        var VD_v = algorithmService.run(GetVotingDescriptions.class, v, ED);
        B_j.set(v, alpha);
        F_j.set(v, delta_j);

        // return data
        return new Triple<>(VP_v, VD_v, beta_j);
    }

}
