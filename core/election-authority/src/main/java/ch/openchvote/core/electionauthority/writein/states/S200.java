/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.electionauthority.writein.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.general.algorithms.GetPrimes;
import ch.openchvote.core.electionauthority.ElectionAuthority;
import ch.openchvote.core.electionauthority.writein.EventContext;
import ch.openchvote.core.electionauthority.writein.states.error.A1;
import ch.openchvote.core.electionauthority.writein.states.error.A2;
import ch.openchvote.core.electionauthority.writein.states.error.B1;
import ch.openchvote.core.electionauthority.writein.tasks.TE1;
import ch.openchvote.core.electionauthority.writein.tasks.TE2;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Message;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.exceptions.TaskException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.core.protocol.phases.Preparation;
import ch.openchvote.core.protocol.protocols.writein.content.message.MAE2;
import ch.openchvote.core.protocol.protocols.writein.content.message.MEE1;
import ch.openchvote.base.utilities.tools.Parallel;
import ch.openchvote.base.utilities.tools.VMGJFassade;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Preparation.class)
@Notify(Status.Type.READY)
public final class S200 extends State<ElectionAuthority, EventContext> {

    public S200(ElectionAuthority electionAuthority, EventContext eventContext) {
        super(electionAuthority, eventContext);
        this.registerMessageHandler(MEE1.class, this::handleMEE1);
        this.registerMessageHandler(MAE2.class, this::handleMAE2);
    }

    private void handleMEE1(Message message) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var k = ES.get_SP().getIndexOf(message.getSenderId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var MEE1 = this.party.getAndCheckContent(MEE1.class, message, aux, securityLevel);

        // update public data
        publicData.setContent(k, MEE1);

        try {
            // perform task
            TE1.run(k, publicData);

            // run self activation
            var stateId = this.getId();
            this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A1.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B1.class);
        }
    }

    private void handleMAE2(Message message) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SP = ES.get_SP();
        var s = SP.get_s();
        var bold_ea = SP.get_bold_ea();
        var j = SP.getIndexOf(this.party.getId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();
        var N_E = EP.get_N_E();
        var bold_n = EP.get_bold_n();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var MAE2 = this.party.getAndCheckContent(MAE2.class, message, aux, securityLevel);

        // update public data
        publicData.setContent(MAE2);

        // create precomputation tables (numbers taken from Table 12.5)
        var securityParameters = new SecurityParameters(securityLevel);
        var p = securityParameters.get_p();
        var q = securityParameters.get_q();
        var g = securityParameters.get_g();
        var h = securityParameters.get_h();
        var p_hat = securityParameters.get_p_hat();
        var q_hat = securityParameters.get_q_hat();
        var g_hat = securityParameters.get_g_hat();
        VMGJFassade.precomputeTable(g, p, q, (s + 5) * N_E);
        VMGJFassade.precomputeTable(h, p, q, 2 * N_E);
        Parallel.forEachLoop(GetPrimes.run(bold_n.sum(), securityParameters), p_i -> VMGJFassade.precomputeTable(p_i, p, q, N_E));
        VMGJFassade.precomputeTable(g_hat, p_hat, q_hat, (3 * s + 12) * N_E);

        try {
            // perform task
            TE1.run(0, publicData);

            try {
                // perform task
                TE2.run(j, publicData, secretData);

                // select event data
                var pk_j = publicData.get_bold_pk().get(j);
                var bold_pk_prime_j = publicData.get_bold_PK_prime().get(j);
                var pi_j = publicData.get_bold_pi().get(j);

                // send MEE1 to election authorities
                this.party.sendMessage(eventId, bold_ea, new MEE1(pk_j, bold_pk_prime_j, pi_j), aux, securityLevel);

                // update state
                this.party.updateState(this.eventContext, S210.class);

                // run self activation
                var stateId = this.getId();
                this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

            } catch (Algorithm.Exception exception) {
                // move to error state
                this.party.updateState(this.eventContext, A2.class);
            }
        } catch (Algorithm.Exception exception) {
            // move to error state
            this.party.updateState(this.eventContext, A1.class);
        } catch (TaskException exception) {
            // move to error state
            this.party.updateState(this.eventContext, B1.class);
        }
    }

}
