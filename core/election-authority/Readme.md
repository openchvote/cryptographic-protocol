# Election Authority

This module provides an implementation of the CHVote party *"Election Authority"*. In
the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325), the role of the election authority is described
as follows:

> A group of election authorities guarantees the integrity and privacy of the votes submitted during the election
> period. They are numbered with indices *j=1,..., s*. During the preparation phase, they establish jointly a public
> ElGamal encryption key *pk*. They also generate the credentials and codes to be printed on the election cards. During
> vote casting, they respond to the submitted ballots and confirmations. At the end of the election period, they perform
> a
> cryptographic shuffle of the encrypted votes. Finally, they use their private key shares *sk_j* to decrypt the votes
> in
> a distributed manner. During the protocol execution, the keep track of all their incoming and outgoing messages, which
> they provide as input for the universal verification process.

The intention of this Maven module is to provide production-quality Java code that implements the election authority's
role in the CHVote protocol. Robust implementations of corresponding services can be connected to this module by using
Java's `ServiceLoader.load()` functionality.

## Specification

### Phases and States

| Phase          | States           | Error States                      | Task                                        |
|:---------------|:-----------------|:----------------------------------|:--------------------------------------------|
| Initialization | S100             | –                                 | Receive event setup and election parameters |
| Preparation    | S200, S210, S120 | A1, B1, A2, A3, A4, A5, B5, A6    | Generate keys and election card data        |
| Election       | S300             | A10                               | Respond to vote casts and confirmations     |
| Tallying       | S400, S410, S420 | A11, A12, B12, A13, A14, B14, A15 | Mix and decrypt votes                       |
| Verification   | S600             | –                                 | Publish election data for verification      |
| Inspection     | S500             | –                                 | Respond to vote inspections                 |
| Testing        | –                | –                                 | –                                           |
| [none]         | FINAL            | –                                 | –                                           |

### State Diagram

![StateDiagram](img/stateDiagram.svg)

### Communication

#### Services:

|                        | ElectionAuthority |
|:-----------------------|:------------------|
| EventService           | Subscriber        |
| MailingService         | –                 |
| MessagingService       | Subscriber        |
| PublicationService     | –                 |
| RequestResponseService | Responder         |
| UserInterface          | –                 |
| TestingService         | –                 |

#### Incoming Messages:

| State | Phase          | Protocol | Name | Type    | Sender             | Signed | Encrypted | Content                          |
|:-----:|:---------------|:--------:|:----:|:--------|:-------------------|:------:|:---------:|:---------------------------------|
| S100  | Initialization |   7.1    | MAE1 | Message | Administrator      |  yes   |    no     | Event setup, election parameters |
| S200  | Preparation    |   7.2    | MAE2 | Message | Administrator      |  yes   |    no     | Public key share                 |
| S200  | Preparation    |   7.2    | MEE1 | Message | Election authority |  yes   |    no     | Public key share                 |
| S210  | Preparation    |   7.2    | MEE1 | Message | Election authority |  yes   |    no     | Public key share                 |
| S210  | Preparation    |   7.3    | MEE2 | Message | Election authority |  yes   |    no     | Voting credentials               |
| S220  | Preparation    |   7.3    | MEE2 | Message | Election authority |  yes   |    no     | Voting credentials               |
| S300  | Election       |   7.6    | RCE1 | Request | Voting client      |   no   |    no     | Voter index                      |
| S300  | Election       |   7.6    | RCE2 | Request | Voting client      |   no   |    no     | Voter index, ballot              |
| S300  | Election       |   7.7    | RCE3 | Request | Voting client      |   no   |    no     | Voter index, confirmation        |
| S400  | Tallying       |   7.8    | MEE3 | Message | Election authority |  yes   |    no     | Shuffled encryptions             |
| S410  | Tallying       |   7.8    | MEE3 | Message | Election authority |  yes   |    no     | Shuffled encryptions             |
| S410  | Tallying       |   7.9    | MEE4 | Message | Election authority |  yes   |    no     | Partial decryptions              |
| S420  | Tallying       |   7.9    | MEE4 | Message | Election authority |  yes   |    no     | Partial decryptions              |
| S600  | Inspection     |   7.12   | RIE1 | Request | Inspection client  |   no   |    no     | Voter index                      |

#### Outgoing Messages:

| State | Phase        | Protocol | Name | Type        | Receiver           | Signed | Encrypted | Content                      |
|:-----:|:-------------|:--------:|:----:|:------------|:-------------------|:------:|:---------:|:-----------------------------|
| S200  | Preparation  |   7.2    | MEE1 | Message     | Election authority |  yes   |    no     | Public key share             |
| S210  | Preparation  |   7.4    | MEP1 | Message     | Printing authority |  yes   |    yes    | Election card data           |
| S210  | Preparation  |   7.3    | MEE2 | Message     | Election authority |  yes   |    no     | Voting credentials           |
| S300  | Election     |   7.6    | REC1 | Response    | Voting client      |  yes   |    no     | Public key share             |
| S300  | Election     |   7.6    | REC2 | Response    | Voting client      |  yes   |    no     | Response                     |
| S300  | Election     |   7.7    | REC3 | Response    | Voting client      |  yes   |    no     | Finalization                 |
| S400  | Tallying     |   7.8    | MEE3 | Message     | Election authority |  yes   |    no     | Shuffled encryptions         |
| S410  | Tallying     |   7.9    | MEE4 | Message     | Election authority |  yes   |    no     | Partial decryptions          |
| S420  | Tallying     |   7.10   | MEA1 | Message     | Administrator      |  yes   |    no     | Combined partial decryptions |
| S500  | Verification |   7.11   | PEP1 | Publication | Public             |  yes   |    no     | Public election data         |
| S600  | Inspection   |   7.12   | REI1 | Response    | Inspection client  |  yes   |    no     | Inspection                   |

#### Coordination:

| Type           | State | Phase          | New State | New Phase      |
|:---------------|:-----:|:---------------|:---------:|:---------------|
| onInitialize() |   –   | –              |   S100    | Initialization |
| onStart()      | S100  | Initialization |   S100    | Initialization |
| onStart()      | S200  | Preparation    |   S200    | Preparation    |
| onStart()      | S300  | Election       |   S300    | Election       |
| onStop()       | S300  | Election       |   S400    | Tallying       |
| onStart()      | S400  | Tallying       |   S400    | Tallying       |
| onStart()      | S500  | Verification   |   S600    | Inspection     |
| onStart()      | S600  | Inspection     |   S600    | Inspection     |
| onStop()       | S600  | Inspection     |   FINAL   | [none]         |
| onTerminate()  | FINAL | [none]         |     –     | –              |

### Tasks

| Task |   State    | Phase       | Protocol | AlgorithmicException | TaskException |
|:----:|:----------:|:------------|:--------:|:--------------------:|:-------------:|
| TE1  | S200, S210 | Preparation |   7.2    |          A1          |      B1       |
| TE2  |    S200    | Preparation |   7.2    |          A2          |       –       |
| TE3  |    S210    | Preparation |   7.2    |          A3          |       –       |
| TE4  |    S210    | Preparation |   7.3    |          A4          |       –       |
| TE5  | S210, S220 | Preparation |   7.2    |          A5          |      B5       |
| TE6  |    S220    | Preparation |   7.3    |          A6          |       –       |
| TE7  |    S300    | Election    |   7.6    |        (S300)        |       –       |
| TE8  |    S300    | Election    |   7.6    |        (S300)        |    (S300)     |
| TE9  |    S300    | Election    |   7.7    |        (S300)        |    (S300)     |
| TE10 |    S300    | Election    |   7.8    |         A10          |       –       |
| TE11 |    S400    | Tallying    |   7.8    |         A11          |       –       |
| TE12 | S400, S410 | Tallying    |   7.8    |         A12          |      B12      |
| TE13 |    S410    | Tallying    |   7.9    |         A13          |       –       |
| TE14 | S410, S420 | Tallying    |   7.9    |         A14          |      B14      |
| TE15 |    S420    | Tallying    |   7.9    |         A15          |       –       |
| TE16 |    S600    | Inspection  |   7.12   |        (S600)        |       –       |

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of
this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts
is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as
follows:

```console
mvn clean install
-- a lot of output
```

## Maven Dependencies

The following dependency tree shows the Maven dependencies of this module.

```console
mvn dependency:tree -Dscope=compile -Dverbose=false
[INFO] --- dependency:3.6.0:tree (default-cli) @ election-authority ---
[INFO] io.gitlab.openchvote:election-authority:jar:2.4
[INFO] \- io.gitlab.openchvote:protocol:jar:2.4:compile
[INFO]    +- io.gitlab.openchvote:algorithms:jar:2.4:compile
[INFO]    |  \- io.gitlab.openchvote:utilities:jar:2.4:compile
[INFO]    |     \- com.verificatum:vmgj:jar:1.2.2:compile
[INFO]    \- io.gitlab.openchvote:framework:jar:2.4:compile
```

## Java Module Dependencies

The following module dependency diagram illustrates the Java module dependencies to other modules within this project:

![Java module dependencies](img/electionAuthority.svg)
