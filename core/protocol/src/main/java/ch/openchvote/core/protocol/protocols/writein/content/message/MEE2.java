/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.protocols.writein.content.message;

import ch.openchvote.base.framework.annotations.content.Signed;
import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.core.algorithms.protocols.common.model.CredentialProof;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Quadruple;

import java.math.BigInteger;

@SuppressWarnings("MissingJavadoc")
@Signed
public final class MEE2 extends Quadruple<Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, CredentialProof> implements Content {

    @SuppressWarnings("unused")
    static public final TypeReference<MEE2> TYPE_REFERENCE = new TypeReference<>() {
    };

    public MEE2(Vector<BigInteger> bold_x_hat, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, CredentialProof pi_hat) {
        super(bold_x_hat, bold_y_hat, bold_z_hat, pi_hat);
    }

    public Vector<BigInteger> get_bold_x_hat() {
        return this.getFirst();
    }

    public Vector<BigInteger> get_bold_y_hat() {
        return this.getSecond();
    }

    public Vector<BigInteger> get_bold_z_hat() {
        return this.getThird();
    }

    public CredentialProof get_pi_hat() {
        return this.getFourth();
    }

}
