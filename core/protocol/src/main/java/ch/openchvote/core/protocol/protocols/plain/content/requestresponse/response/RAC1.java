/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.protocols.plain.content.requestresponse.response;

import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.core.algorithms.protocols.common.model.VotingDescriptions;
import ch.openchvote.core.algorithms.protocols.plain.model.KeyPairProof;
import ch.openchvote.core.algorithms.protocols.plain.model.VotingParametersPlain;
import ch.openchvote.base.framework.annotations.content.Signed;
import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Quintuple;

import java.math.BigInteger;

@SuppressWarnings("MissingJavadoc")
@Signed
public final class RAC1 extends Quintuple<EventSetup, VotingParametersPlain, VotingDescriptions, BigInteger, KeyPairProof> implements Content {

    @SuppressWarnings("unused")
    static public final TypeReference<RAC1> TYPE_REFERENCE = new TypeReference<>() {
    };

    public RAC1(EventSetup ES, VotingParametersPlain VP, VotingDescriptions VD, BigInteger pk, KeyPairProof pi) {
        super(ES, VP, VD, pk, pi);
    }

    public EventSetup get_ES() {
        return this.getFirst();
    }

    public VotingParametersPlain get_VP() {
        return this.getSecond();
    }

    public VotingDescriptions get_VD() {
        return this.getThird();
    }

    public BigInteger get_pk() {
        return this.getFourth();
    }

    public KeyPairProof get_pi() {
        return this.getFifth();
    }

}
