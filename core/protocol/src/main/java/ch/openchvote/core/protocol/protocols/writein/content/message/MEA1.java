/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.protocols.writein.content.message;

import ch.openchvote.base.framework.annotations.content.Signed;
import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.core.algorithms.protocols.writein.model.AugmentedEncryption;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

@SuppressWarnings("MissingJavadoc")
@Signed
public final class MEA1 extends Triple<Vector<BigInteger>, Matrix<BigInteger>, Vector<AugmentedEncryption>> implements Content {

    @SuppressWarnings("unused")
    static public final TypeReference<MEA1> TYPE_REFERENCE = new TypeReference<>() {
    };

    public MEA1(Vector<BigInteger> bold_c, Matrix<BigInteger> bold_D, Vector<AugmentedEncryption> bold_e_tilde_s) {
        super(bold_c, bold_D, bold_e_tilde_s);
    }

    public Vector<BigInteger> get_bold_c() {
        return this.getFirst();
    }

    public Matrix<BigInteger> get_bold_D() {
        return this.getSecond();
    }

    public Vector<AugmentedEncryption> get_bold_e_tilde_s() {
        return this.getThird();
    }

}
