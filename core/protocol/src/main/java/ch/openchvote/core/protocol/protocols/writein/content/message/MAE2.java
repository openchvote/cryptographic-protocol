/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.protocols.writein.content.message;

import ch.openchvote.base.framework.annotations.content.Signed;
import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.core.algorithms.protocols.writein.model.KeyPairProof;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Triple;

import java.math.BigInteger;

@SuppressWarnings("MissingJavadoc")
@Signed
public final class MAE2 extends Triple<BigInteger, Vector<BigInteger>, KeyPairProof> implements Content {

    @SuppressWarnings("unused")
    static public final TypeReference<MAE2> TYPE_REFERENCE = new TypeReference<>() {
    };

    public MAE2(BigInteger pk, Vector<BigInteger> bold_pk_prime, KeyPairProof pi) {
        super(pk, bold_pk_prime, pi);
    }

    public BigInteger get_pk() {
        return this.getFirst();
    }

    public Vector<BigInteger> get_bold_pk_prime() {
        return this.getSecond();
    }

    public KeyPairProof get_pi() {
        return this.getThird();
    }

}
