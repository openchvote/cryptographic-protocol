/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.protocols.writein;

import ch.openchvote.core.algorithms.parameters.usability.CodeParameters;
import ch.openchvote.core.algorithms.parameters.usability.CredentialParameters;
import ch.openchvote.core.algorithms.parameters.usability.WriteInsParameters;
import ch.openchvote.base.framework.exceptions.FrameworkException;
import ch.openchvote.core.protocol.parameters.SecurityLevel;
import ch.openchvote.core.protocol.parameters.UsabilityConfiguration;
import ch.openchvote.base.utilities.set.Alphabet;

import static ch.openchvote.base.framework.exceptions.FrameworkException.Type.UNKNOWN_CONFIGURATION;
import static ch.openchvote.base.framework.exceptions.FrameworkException.Type.UNKNOWN_SECURITY_LEVEL;

/**
 * Objects of this class represent a full set of security and usability parameters from Section 6.3. For this, the class
 * implements all interfaces for each category of parameters. The goal of providing all parameters in a single instance
 * of this class is to maximize the convenience in accessing the parameters in the algorithm and protocol
 * implementations.
 */
public final class UsabilityParameters implements CredentialParameters, CodeParameters, WriteInsParameters {

    private final UsabilityConfiguration usabilityConfiguration;
    private final SecurityLevel securityLevel;

    /**
     * Constructs a new instance based on a given usability configuration and security level.
     *
     * @param usabilityConfiguration The given usability configuration
     * @param securityLevel          The given security level
     */
    public UsabilityParameters(String usabilityConfiguration, String securityLevel) {
        try {
            this.usabilityConfiguration = UsabilityConfiguration.of(usabilityConfiguration);
        } catch (Exception exception) {
            throw new FrameworkException(UNKNOWN_CONFIGURATION, usabilityConfiguration);
        }
        try {
            this.securityLevel = SecurityLevel.valueOf(securityLevel);
        } catch (IllegalArgumentException exception) {
            throw new FrameworkException(UNKNOWN_SECURITY_LEVEL, securityLevel);
        }
    }

    @Override
    public String getUsabilityConfiguration() {
        return this.usabilityConfiguration.getId();
    }

    @Override
    public String getSecurityLevel() {
        return this.securityLevel.getId();
    }

    @Override
    public int get_tau() {
        return this.securityLevel.tau;
    }

    @Override
    public double get_epsilon() {
        return this.securityLevel.epsilon;
    }

    @Override
    public Alphabet get_A_X() {
        return this.usabilityConfiguration.A_X;
    }

    @Override
    public Alphabet get_A_Y() {
        return this.usabilityConfiguration.A_Y;
    }

    @Override
    public Alphabet get_A_V() {
        return this.usabilityConfiguration.A_V;
    }

    @Override
    public Alphabet get_A_PA() {
        return this.usabilityConfiguration.A_PA;
    }

    @Override
    public int get_n_max() {
        return this.usabilityConfiguration.n_max;
    }

    @Override
    public Alphabet get_A_W() {
        return this.usabilityConfiguration.A_W;
    }

    @Override
    public char get_c_W() {
        return this.usabilityConfiguration.c_W;
    }

    @Override
    public int get_ell_W() {
        return this.usabilityConfiguration.ell_W;
    }

}

