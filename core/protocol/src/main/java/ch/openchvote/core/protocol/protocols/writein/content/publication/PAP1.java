/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.protocols.writein.content.publication;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.core.algorithms.protocols.writein.model.ElectionParametersWriteIn;
import ch.openchvote.core.algorithms.protocols.writein.model.ElectionResultWriteIn;
import ch.openchvote.base.framework.annotations.content.Signed;
import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Quadruple;

@SuppressWarnings("MissingJavadoc")
@Signed
public final class PAP1 extends Quadruple<EventSetup, ElectionParametersWriteIn, ElectionDescriptions, ElectionResultWriteIn> implements Content {

    @SuppressWarnings("unused")
    static public final TypeReference<PAP1> TYPE_REFERENCE = new TypeReference<>() {
    };

    public PAP1(EventSetup ES, ElectionParametersWriteIn EP, ElectionDescriptions ED, ElectionResultWriteIn ER) {
        super(ES, EP, ED, ER);
    }

    public EventSetup get_ES() {
        return this.getFirst();
    }

    public ElectionParametersWriteIn get_EP() {
        return this.getSecond();
    }

    public ElectionDescriptions get_ED() {
        return this.getThird();
    }

    public ElectionResultWriteIn get_ER() {
        return this.getFourth();
    }

}
