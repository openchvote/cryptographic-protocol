/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response;

import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.core.algorithms.protocols.common.model.VotingDescriptions;
import ch.openchvote.core.algorithms.protocols.writein.model.KeyPairProof;
import ch.openchvote.core.algorithms.protocols.writein.model.VotingParametersWriteIn;
import ch.openchvote.base.framework.annotations.content.Signed;
import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Sextuple;

import java.math.BigInteger;

@SuppressWarnings("MissingJavadoc")
@Signed
public final class RAC1 extends Sextuple<EventSetup, VotingParametersWriteIn, VotingDescriptions, BigInteger, Vector<BigInteger>, KeyPairProof> implements Content {

    @SuppressWarnings("unused")
    static public final TypeReference<RAC1> TYPE_REFERENCE = new TypeReference<>() {
    };

    public RAC1(EventSetup ES, VotingParametersWriteIn VP, VotingDescriptions VD, BigInteger pk, Vector<BigInteger> bold_pk_prime, KeyPairProof pi) {
        super(ES, VP, VD, pk, bold_pk_prime, pi);
    }

    public EventSetup get_ES() {
        return this.getFirst();
    }

    public VotingParametersWriteIn get_VP() {
        return this.getSecond();
    }

    public VotingDescriptions get_VD() {
        return this.getThird();
    }

    public BigInteger get_pk() {
        return this.getFourth();
    }

    public Vector<BigInteger> get_bold_pk_prime() {
        return this.getFifth();
    }

    public KeyPairProof get_pi() {
        return this.getSixth();
    }

}
