/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.parameters;

import ch.openchvote.core.algorithms.general.algorithms.GetGroupParameters;
import ch.openchvote.base.framework.interfaces.Identifiable;
import ch.openchvote.base.utilities.crypto.BlockCipher;
import ch.openchvote.base.utilities.crypto.HashAlgorithm;

import java.math.BigInteger;

/**
 * This enum class contains all the group parameters from Section 10.1 and 10.2. A singleton object is provided for
 * every security level. All values could be generated using the {@link GetGroupParameters} algorithm, but this is a
 * relatively time-consuming operation.
 */
public enum SecurityLevel implements Identifiable {

    /**
     * Security Level 0, with parameters taken from Section 10.2.1 (the name LEVEL_0 is specified in Table 10.1).
     */
    LEVEL_0(16, 16, 0.999,
            new BigInteger("B7E151629927", 16),
            BigInteger.valueOf(2),
            BigInteger.valueOf(3),
            new BigInteger("B7FC9CE51713", 16),
            new BigInteger("B7E15173", 16),
            new BigInteger("8145D710FE7F", 16),
            HashAlgorithm.SHA3,
            BlockCipher.AES
    ),

    /**
     * Security Level 1, with parameters taken from Section 10.2.2 (the name LEVEL_1 is specified in Table 10.1).
     */
    LEVEL_1(112, 112, 0.9999,
            new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF324E7738926CFBE5F4BF8D8D8C31D763DA06C80ABB1185EB4F7C7B5757F5958490CFD47D7C19BB42158D9554F7B46BCED55C4D79FD5F24D6613C31C3839A2DDF8A9A276BCFBFA1C877C56284DAB79CD4C2B3293D20E9E5EAF02AC60ACC93ED874422A52ECB238FEEE5AB6ADD835FD1A0753D0A8F78E537D2B95BB79D8DCAEC642C1E9F23B829B5C2780BF38737DF8BB300D01334A0D0BD8645CBFA73A6160FFE393C48CBBBCA060F0FF8EC6D31BEB5CCEED7F2F0BB088017163BC60DF45A0ECB1BCD289B06CBBFEA21AD08E1847F3F7378D56CED94640D6EF0D3D37BE69D0063", 16),
            BigInteger.valueOf(2),
            BigInteger.valueOf(3),
            new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF324E7738926CFBE5F4BF8D8D8C31D763DA06C80ABB1185EB4F7C7B5757F5958490CFD47D7C19BB42158D9554F7B46BCED55C4D79FD5F24D6613C31C3839A2DDF8A9A276BCFBFA1C877C56284DAB79CD4C2B3293D20E9E5EAF02AC60ACC93ED874422A52ECB238FEEE5AB6ADD835FD1A0753D0A8F78E537D2B95BB79D8DCAEC642C1E9F23B829B5C2780BF38737DF8BB300D01334A0D0BD8645CBFA73A6160FFE393C48CBBBCA060F0FF8EC6D31BEB5CCEED7F2F0BB088017163BC60DF45A0ECB1BCD3548E571733F4A8C724DC97F56F0AE89897D8A6B93C6F87D7494503A5D6D", 16),
            new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D991", 16),
            new BigInteger("7C41B5D002301514D10155BF22BA33947C96EB398837B9E6AC1A25ABFC3F9D44FB7D943A3317771A26615814BB06E58B5531F4D81CF23B778F23A2364FFB0C28A7335AE731761FAB304975C8DB647FCCFC1E64239373F60FAD80FE12D750B3CD753B98D548A325A9A629B06E63A7FC2860D4EB1B885482B64D7177854104554363DFD70DAFDF529F9AFF072F78B7FEAA92D00DC6A7180FF49B60F84979A777919E42484A6A1C014E7F8E8CC184546CAE0557124F7F21FB2C16AC6EF4F122BB70966F9FBF03A7807AF8190CDF95DCDF0509C0FA8302681130E7B60C9E9A65BDF83940F0CCC164989B558B9724D97C524E1A2810E0BB546F83754A846000A9ADB2", 16),
            HashAlgorithm.SHA3,
            BlockCipher.AES
    ),

    /**
     * Security Level 2, with parameters taken from Section 10.2.3 (the name LEVEL_0 is specified in Table 10.1).
     */
    LEVEL_2(128, 128, 0.99999,
            new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF324E7738926CFBE5F4BF8D8D8C31D763DA06C80ABB1185EB4F7C7B5757F5958490CFD47D7C19BB42158D9554F7B46BCED55C4D79FD5F24D6613C31C3839A2DDF8A9A276BCFBFA1C877C56284DAB79CD4C2B3293D20E9E5EAF02AC60ACC93ED874422A52ECB238FEEE5AB6ADD835FD1A0753D0A8F78E537D2B95BB79D8DCAEC642C1E9F23B829B5C2780BF38737DF8BB300D01334A0D0BD8645CBFA73A6160FFE393C48CBBBCA060F0FF8EC6D31BEB5CCEED7F2F0BB088017163BC60DF45A0ECB1BCD289B06CBBFEA21AD08E1847F3F7378D56CED94640D6EF0D3D37BE67008E186D1BF275B9B241DEB64749A47DFDFB96632C3EB061B6472BBF84C26144E49C2D04C324EF10DE513D3F5114B8B5D374D93CB8879C7D52FFD72BA0AAE7277DA7BA1B4AF1488D8E836AF14865E6C37AB6876FE690B571121382AF341AFE94F77BCF06C83B8FF5675F0979074AD9A787BC5B9BD4B0C5937D3EDE4C3A79396419CD7", 16),
            BigInteger.valueOf(2),
            BigInteger.valueOf(3),
            new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF324E7738926CFBE5F4BF8D8D8C31D763DA06C80ABB1185EB4F7C7B5757F5958490CFD47D7C19BB42158D9554F7B46BCED55C4D79FD5F24D6613C31C3839A2DDF8A9A276BCFBFA1C877C56284DAB79CD4C2B3293D20E9E5EAF02AC60ACC93ED874422A52ECB238FEEE5AB6ADD835FD1A0753D0A8F78E537D2B95BB79D8DCAEC642C1E9F23B829B5C2780BF38737DF8BB300D01334A0D0BD8645CBFA73A6160FFE393C48CBBBCA060F0FF8EC6D31BEB5CCEED7F2F0BB088017163BC60DF45A0ECB1BCD289B06CBBFEA21AD08E1847F3F7378D56CED94640D6EF0D3D37BE67008E186D1BF275B9B241DEB64749A47DFDFB96632C3EB061B6472BBF84C26144E49C2D04C324EF10DE513D3F5114B8B5D374D93CB8879C7D52FFD72BA0AAE7277DA7BA1B4AF1488D8E836AF14865E6C37AB6876FE690B571121382AF341AFE94F790F02FA1BCE9C73886B4C0ACABDC3DD14E0D8C955577C9764844038771FC25F84BB", 16),
            new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190D05D", 16),
            new BigInteger("47DAD70733EFE399D1AFF4FE387250218BB88FD5F4040C31851AE1DF0985D0019950A958710C6B935B6B3BB45C278381DC5883CC933C5B7052D3BC8C77D746E3D1FB2B7EF3630C1014417D2F83BEAD0E1F4DFD986104CDF16C4AEC33BB5906C8149C83E6C5B8837E12AB32E73A69C4ABEB0B014FFF1FBB3173EAD73A1404DAEDF52F62D605D3787900124829751320FEDAA1F5B2D90FB846C7EB7815193E5C2460F93A3A5D16FB7A3DBAC9CE31B7517D2F88D530E61D06B529A43A0806F6A931247C9166C32CC9BAA019823528D3F156B60ECE5DA9A6D60148661F59670AD98A1B8EAFEBC4A68D8A5D3F29105FD33D994751A9AD8E0EB7367D5BFE7A2F082981869FA2F177C472D1988844E4DA58170BB3DDE9DFB2E61DC06FA5249C3200CD3BBBF24D5C257879CB23D7931ED4AD1F9FA168B38FAA3C6DB89AA9D89BB6DB3F47BF1BE57856C12AD2FD708A932DC4C91A48E662B37C4076A5D2BE54AC800EC1E6A13E1FC8EB61CA52E5D7B7608483E3BC225FBC62456AB46E39DA3CF45AB11A50", 16),
            HashAlgorithm.SHA3,
            BlockCipher.AES
    );

    /**
     * Security parameter {@code sigma} from Section 6.3.1.
     */
    public final int sigma;

    /**
     * Security parameter {@code tau} from Section 6.3.1.
     */
    public final int tau;

    /**
     * Security parameter {@code epsilon} from Section 6.3.1.
     */
    public final double epsilon;

    /**
     * Security parameter {@code p} from Table 6.1 in Section 6.3.1.
     */
    public final BigInteger p;

    /**
     * Security parameter {@code g} from Table 6.1 in Section 6.3.1.
     */
    public final BigInteger g;

    /**
     * Security parameter {@code h} from Table 6.1 in Section 6.3.1.
     */
    public final BigInteger h;

    /**
     * Security parameter {@code p_hat} from Table 6.1 in Section 6.3.1.
     */
    public final BigInteger p_hat;

    /**
     * Security parameter {@code q_hat} from Table 6.1 in Section 6.3.1.
     */
    public final BigInteger q_hat;

    /**
     * Security parameter {@code g_hat} from Table 6.1 in Section 6.3.1.
     */
    public final BigInteger g_hat;

    /**
     * Hash algorithm as specified in Section 10.1.
     */
    public final HashAlgorithm hashAlgorithm;

    /**
     * Block cipher as specified in Section 5.7.
     */
    public final BlockCipher blockCipher;

    // implicitly private constructor
    SecurityLevel(int sigma, int tau, double epsilon, BigInteger p, BigInteger g, BigInteger h, BigInteger p_hat, BigInteger q_hat, BigInteger g_hat, HashAlgorithm hashAlgorithm, BlockCipher blockCipher) {
        this.sigma = sigma;
        this.tau = tau;
        this.epsilon = epsilon;
        this.p = p;
        this.g = g;
        this.h = h;
        this.p_hat = p_hat;
        this.q_hat = q_hat;
        this.g_hat = g_hat;
        this.hashAlgorithm = hashAlgorithm;
        this.blockCipher = blockCipher;
    }

    @Override
    public String getId() {
        return this.name();
    }

}
