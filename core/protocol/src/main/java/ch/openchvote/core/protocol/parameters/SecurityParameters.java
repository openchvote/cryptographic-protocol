/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.parameters;

import ch.openchvote.core.algorithms.parameters.security.BlockCipherParameters;
import ch.openchvote.core.algorithms.parameters.security.GGParameters;
import ch.openchvote.core.algorithms.parameters.security.NIZKPParameters;
import ch.openchvote.core.algorithms.parameters.security.ZZPlusParameters;
import ch.openchvote.base.framework.exceptions.FrameworkException;
import ch.openchvote.base.utilities.crypto.BlockCipher;
import ch.openchvote.base.utilities.crypto.HashAlgorithm;

import java.math.BigInteger;

import static ch.openchvote.base.framework.exceptions.FrameworkException.Type.UNKNOWN_SECURITY_LEVEL;

/**
 * Objects of this class represent the set of security parameters from Section 6.3. For this, the class implements all
 * interfaces for each category of security parameters. The goal of providing all security parameters in a single
 * instance of this class is to maximize the convenience in accessing the parameters in the algorithm and protocol
 * implementations.
 */
public class SecurityParameters implements ZZPlusParameters, GGParameters, NIZKPParameters, BlockCipherParameters {

    protected final SecurityLevel securityLevel;

    /**
     * Constructs a security parameters object based on a given security level.
     *
     * @param SL The given security level
     */
    public SecurityParameters(String SL) {
        try {
            this.securityLevel = SecurityLevel.valueOf(SL);
        } catch (IllegalArgumentException exception) {
            throw new FrameworkException(UNKNOWN_SECURITY_LEVEL, SL);
        }
    }

    @Override
    public String getSecurityLevel() {
        return this.securityLevel.getId();
    }

    @Override
    public BigInteger get_p() {
        return this.securityLevel.p;
    }

    @Override
    public BigInteger get_q() {
        return this.securityLevel.p.subtract(BigInteger.ONE).shiftRight(1);
    }

    @Override
    public BigInteger get_g() {
        return this.securityLevel.g;
    }

    @Override
    public BigInteger get_h() {
        return this.securityLevel.h;
    }

    @Override
    public BigInteger get_p_hat() {
        return this.securityLevel.p_hat;
    }

    @Override
    public BigInteger get_q_hat() {
        return this.securityLevel.q_hat;
    }

    @Override
    public BigInteger get_g_hat() {
        return this.securityLevel.g_hat;
    }

    @Override
    public int get_tau() {
        return this.securityLevel.tau;
    }

    @Override
    public HashAlgorithm getHashAlgorithm() {
        return this.securityLevel.hashAlgorithm;
    }

    @Override
    public BlockCipher getBlockCipher() {
        return this.securityLevel.blockCipher;
    }

}

