/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.protocols.plain.content.testdata;

import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.base.utilities.sequence.IntVector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Pair;

@SuppressWarnings("MissingJavadoc")
public final class TVT1 extends Pair<Integer, IntVector> implements Content {

    @SuppressWarnings("unused")
    static public final TypeReference<TVT1> TYPE_REFERENCE = new TypeReference<>() {
    };

    public TVT1(Integer v, IntVector bold_s) {
        super(v, bold_s);
    }

    public Integer get_v() {
        return this.getFirst();
    }

    public IntVector get_bold_s() {
        return this.getSecond();
    }

}
