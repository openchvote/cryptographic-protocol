/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.parameters;

import ch.openchvote.base.framework.interfaces.Identifiable;
import ch.openchvote.base.utilities.set.Alphabet;

/**
 * This class contains all the usability configuration parameters from Sections 6.3.2 (Table 6.1) and 9.1.1 (Table 9.1).
 * A singleton object is provided for every distinct configuration. The naming convention is defined in Section 11.1.
 */
public class UsabilityConfiguration implements Identifiable {

    /**
     * Four different alphabets for testing purposes.
     */
    static public final UsabilityConfiguration TEST = UsabilityConfiguration.of("A10_A16_A26_A52");

    /**
     * Alphabet for keys set to Base-16, alphabet for verification codes set to Base-10, alphabet for participation and
     * abstention codes set to Base-32.
     */
    static public final UsabilityConfiguration DEFAULT = UsabilityConfiguration.of("A57_A57_A10_A16");

    /**
     * Alphabet for keys set to Base-16, alphabet for verification codes set to Base-10, alphabet for participation and
     * abstention codes set to Base-32.
     */
    static public final UsabilityConfiguration SWISS_POST = UsabilityConfiguration.of("A32_A10_A10_A10");

    // regex for checking the format of the usability configuration string
    static private final String REGEX = "^(A(10|16|26|32|36|52|57|62|64)_){3}A(10|16|26|32|36|52|57|62|64)$";

    /**
     * The method maps a usability configuration string into a usability configuration. The string specifies the four
     * alphabets (separated by an underscore) of the configuration, for example "A10_A10_A10_A10" for a configuration,
     * in which all four alphabets are equal to the {@link Alphabet#BASE_10}. An exception is thrown if the string
     * format is invalid or if it contains an undefined alphabet.
     *
     * @param UC The given usability configuration string
     * @return The corresponding usability configuration
     */
    static public UsabilityConfiguration of(String UC) {
        if (UC.matches(REGEX)) {
            var alphabets = UC.split("_");
            var A_X = getAlphabet(alphabets[0]);
            var A_Y = getAlphabet(alphabets[1]);
            var A_V = getAlphabet(alphabets[2]);
            var A_PA = getAlphabet(alphabets[3]);
            return new UsabilityConfiguration(UC, A_X, A_Y, A_V, A_PA);
        }
        throw new IllegalArgumentException("Usability configuration undefined");
    }

    // private helper method for selecting the alphabet from its description
    static private Alphabet getAlphabet(String alphabet) {
        return switch (alphabet) {
            case "A10" -> Alphabet.BASE_10;
            case "A16" -> Alphabet.BASE_16;
            case "A26" -> Alphabet.LATIN_26;
            case "A32" -> Alphabet.ALPHANUMERIC_32;
            case "A36" -> Alphabet.ALPHANUMERIC_36;
            case "A52" -> Alphabet.LATIN_52;
            case "A57" -> Alphabet.ALPHANUMERIC_57;
            case "A62" -> Alphabet.ALPHANUMERIC_62;
            case "A64" -> Alphabet.BASE_64;
            default -> throw new IllegalArgumentException("Alphabet undefined");
        };
    }

    /**
     * Voting code alphabet.
     */
    public final Alphabet A_X;

    /**
     * Confirmation code alphabet.
     */
    public final Alphabet A_Y;

    /**
     * Verification code alphabet.
     */
    public final Alphabet A_V;

    /**
     * Participation and abstention code alphabet.
     */
    public final Alphabet A_PA;

    /**
     * Write-ins alphabet.
     */
    public final Alphabet A_W;

    /**
     * Write-ins padding character (as defined in Section 9.1.1).
     */
    public final char c_W;

    /**
     * Length of write-ins text fields (as defined in Section 9.1.1).
     */
    public final int ell_W;

    /**
     * Maximal number of candidates (as defined in Section 11.1.2).
     */
    public final int n_max = 1678;

    // private field for storing the usability configuration string
    private final String UC;

    // private constructor
    private UsabilityConfiguration(String UC, Alphabet A_X, Alphabet A_Y, Alphabet A_V, Alphabet A_PA) {
        this.UC = UC;
        this.A_X = A_X;
        this.A_Y = A_Y;
        this.A_V = A_V;
        this.A_PA = A_PA;
        this.A_W = Alphabet.LATIN_EXTENDED;
        this.c_W = '|'; // default write-ins padding character
        this.ell_W = 80; // default length of write-ins text fields
    }

    @Override
    public String getId() {
        return this.UC;
    }

    @Override
    public String toString() {
        return this.UC;
    }

}
