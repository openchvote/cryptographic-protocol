/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.protocols;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionParameters;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionResult;
import ch.openchvote.base.framework.protocol.Protocol;
import ch.openchvote.core.protocol.protocols.plain.Plain;
import ch.openchvote.core.protocol.protocols.writein.WriteIn;

/**
 * This is the common root class for different CHVote protocols. Currently, there are only two protocols {@link Plain}
 * and {@link WriteIn} that inherit the type of this class.
 *
 * @param <EP> Generic parameter for the protocol-specific election parameters class
 * @param <ER> Generic parameter for the protocol-specific election result class
 */
public sealed class CHVote<EP extends ElectionParameters, ER extends ElectionResult> extends Protocol permits Plain, WriteIn {

}
