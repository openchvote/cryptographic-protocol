/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.protocols.plain.content.message;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.core.algorithms.protocols.plain.model.ElectionParametersPlain;
import ch.openchvote.base.framework.annotations.content.Signed;
import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Signed
public final class MAE1 extends Triple<EventSetup, ElectionParametersPlain, ElectionDescriptions> implements Content {

    @SuppressWarnings("unused")
    static public final TypeReference<MAE1> TYPE_REFERENCE = new TypeReference<>() {
    };

    public MAE1(EventSetup ES, ElectionParametersPlain EP, ElectionDescriptions ED) {
        super(ES, EP, ED);
    }

    public EventSetup get_ES() {
        return this.getFirst();
    }

    public ElectionParametersPlain get_EP() {
        return this.getSecond();
    }

    public ElectionDescriptions get_ED() {
        return this.getThird();
    }

}
