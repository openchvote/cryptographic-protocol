/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.protocol.protocols.plain.content.message;

import ch.openchvote.base.framework.annotations.content.Signed;
import ch.openchvote.core.algorithms.protocols.common.model.Encryption;
import ch.openchvote.core.algorithms.protocols.plain.model.ShuffleProof;
import ch.openchvote.base.framework.communication.Content;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.serializer.TypeReference;
import ch.openchvote.base.utilities.tuples.Pair;

@SuppressWarnings("MissingJavadoc")
@Signed
public final class MEE3 extends Pair<Vector<Encryption>, ShuffleProof> implements Content {

    @SuppressWarnings("unused")
    static public final TypeReference<MEE3> TYPE_REFERENCE = new TypeReference<>() {
    };

    public MEE3(Vector<Encryption> bold_e_tilde, ShuffleProof pi_tilde) {
        super(bold_e_tilde, pi_tilde);
    }

    public Vector<Encryption> get_bold_e_tilde() {
        return this.getFirst();
    }

    public ShuffleProof get_pi_tilde() {
        return this.getSecond();
    }

}
