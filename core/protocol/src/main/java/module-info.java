/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
/**
 * This module is an implementation of the CHVote protocol.
 */
module ch.openchvote.core.protocol {

    requires transitive ch.openchvote.base.framework;
    requires transitive ch.openchvote.core.algorithms;

    exports ch.openchvote.core.protocol.parameters;
    exports ch.openchvote.core.protocol.phases;
    exports ch.openchvote.core.protocol.roles;
    exports ch.openchvote.core.protocol.protocols;
    exports ch.openchvote.core.protocol.protocols.plain;
    exports ch.openchvote.core.protocol.protocols.plain.content.mail;
    exports ch.openchvote.core.protocol.protocols.plain.content.message;
    exports ch.openchvote.core.protocol.protocols.plain.content.publication;
    exports ch.openchvote.core.protocol.protocols.plain.content.requestresponse.request;
    exports ch.openchvote.core.protocol.protocols.plain.content.requestresponse.response;
    exports ch.openchvote.core.protocol.protocols.plain.content.testdata;
    exports ch.openchvote.core.protocol.protocols.plain.content.userinterface.input;
    exports ch.openchvote.core.protocol.protocols.plain.content.userinterface.output;
    exports ch.openchvote.core.protocol.protocols.writein;
    exports ch.openchvote.core.protocol.protocols.writein.content.mail;
    exports ch.openchvote.core.protocol.protocols.writein.content.message;
    exports ch.openchvote.core.protocol.protocols.writein.content.publication;
    exports ch.openchvote.core.protocol.protocols.writein.content.requestresponse.request;
    exports ch.openchvote.core.protocol.protocols.writein.content.requestresponse.response;
    exports ch.openchvote.core.protocol.protocols.writein.content.testdata;
    exports ch.openchvote.core.protocol.protocols.writein.content.userinterface.input;
    exports ch.openchvote.core.protocol.protocols.writein.content.userinterface.output;
}
