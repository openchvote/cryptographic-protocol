# Protocol

This module provides classes necessary for instantiating the generic framework module to the particularities of the
CHVote protocol. It consists of various components in different packages. Currently, CHVote defines two protocol
versions `plain` and `writein` and three security levels `LEVEL_0`, `LEVEL_1`, and `LEVEL_2`
(see [CHVote Protocol Specification](https://eprint.iacr.org/2017/325)).

## Overview of Module

The following list gives an overview of the main packages and a short description of their contents.

### Package `ch.openchvote.core.protocol.parameters`

This package defines the system and usability parameter for different security levels as defined in Sections 10.1
and 10.2 of the OpenCHVote specification document. It consists of three classes `SecurityLevel`, `SecurityParameters`,
and `UsabilityParameters` with respective purposes.

### Package `ch.openchvote.core.protocol.phases`

This package defines the protocol phases as described in Chapter 7 of the OpenCHVote specification document (with an
additional testing phase that is only used for testing purposes):

- initialization,
- preparation,
- election,
- tallying,
- inspection
- testing.

Each protocol phase is represented by an empty class, for example `Preparation` for the preparation phase. Using the
annotations `@Ready` and `@Done`, the start and stop conditions of the phase are specified, by listing corresponding
party roles that need to submit a `Status.Type.Ready` or `Status.Type.Ready` notification, respectively.

### Package `ch.openchvote.core.protocol.roles`

This package defines the different roles of the protocol parties as described in Section 6.1 of the OpenCHVote
specification document:

- administrator,
- election authority,
- printing authority,
- voting client,
- inspection client,
- voter.

Each protocol is represented by an empty class, for example `Administrator` for the role of the administrator. These
role classes are used in different annotations, for example in the annotations `@Ready` and `@Done` to specify the
relevant roles of a particular protocol phase, or in the annotation `@Roles` to specify the roles of a protocol.

### Package `ch.openchvote.core.protocol.protocols`

The content of this package defines further details about each of the two currently supported protocol, for example
by defining corresponding empty classes `Plain` and `WriteIn`. A set of `Content` classes is defined in each case,
which defines the exact Java type of the communication content for every protocol message from the specification
document. As an example, consider the message MEE3, which contains a vector of encryptions and a shuffle
proof. The corresponding content class `MEE3` is defined as follows:

  ```java
  public final class MEE3 extends Pair<Vector<Encryption>, ShuffleProof> implements Content {
    // some code
}
  ```

### Remarks

- A given protocol may have 0, 1, or multiple phases, and the same phase may appear in different protocols.
- A given protocol may have 0, 1, or multiple roles, and the same role may appear in different protocols.
- For a given phase to start or stop,

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of
this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts
is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as
follows:

```console
mvn clean install
-- a lot of output
```

## Maven Dependencies

The following dependency tree shows the Maven dependencies of this module.

```console
mvn dependency:tree -Dscope=compile -Dverbose=false
[INFO] --- dependency:3.6.0:tree (default-cli) @ protocol ---
[INFO] io.gitlab.openchvote:protocol:jar:2.4
[INFO] +- io.gitlab.openchvote:algorithms:jar:2.4:compile
[INFO] |  \- io.gitlab.openchvote:utilities:jar:2.4:compile
[INFO] |     \- com.verificatum:vmgj:jar:1.2.2:compile
[INFO] \- io.gitlab.openchvote:framework:jar:2.4:compile
```

## Java Module Dependencies

The following module dependency diagram illustrates the Java module dependencies to other modules within this project:

![Java module dependencies](img/protocol.svg)
