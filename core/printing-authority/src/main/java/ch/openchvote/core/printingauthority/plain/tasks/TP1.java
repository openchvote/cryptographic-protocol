/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.printingauthority.plain.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.common.algorithms.GetElectionCards;
import ch.openchvote.core.printingauthority.plain.PublicData;
import ch.openchvote.core.printingauthority.plain.SecretData;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.core.protocol.protocols.plain.UsabilityParameters;
import ch.openchvote.base.utilities.sequence.Matrix;
import ch.openchvote.base.utilities.set.IntSet;

@SuppressWarnings("MissingJavadoc")
public final class TP1 {

    static public boolean isReady(PublicData publicData, SecretData secretData) {
        var s = publicData.get_ES().get().get_SP().get_s();
        return secretData.get_bold_D().arePresent(IntSet.range(1, s));
    }

    static public void
    run(PublicData publicData, SecretData secretData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();
        var UC = ES.get_UC();

        // get security and usability parameters
        var securityParameters = new SecurityParameters(SL);
        var usabilityParameters = new UsabilityParameters(UC, SL);

        // perform task
        var AD = publicData.get_ES().get().get_SP().get_AD();
        var bold_n = publicData.get_EP().get().get_bold_n();
        var bold_D = secretData.get_bold_D().mapTo(Matrix::ofCols);
        var bold_ec = algorithmService.run(GetElectionCards.class, securityParameters, usabilityParameters, AD, bold_n, bold_D);

        // update event data
        secretData.get_bold_ec().set(bold_ec);
    }

}
