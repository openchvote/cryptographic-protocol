/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.printingauthority.plain;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.core.algorithms.protocols.plain.model.ElectionParametersPlain;
import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.core.protocol.protocols.plain.content.message.MAP1;
import ch.openchvote.base.utilities.tuples.Triple;
import ch.openchvote.base.utilities.tuples.Tuple;

/**
 * Instances of this class represent the printing authority's public data. The class inherits from the generic
 * {@link Tuple} subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects
 * to store. Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class PublicData extends Triple<
        // event setup and election parameters
        EventData.Data<EventSetup>, // ES
        EventData.Data<ElectionParametersPlain>, // EP
        EventData.Data<ElectionDescriptions>> // ED
        implements EventData {

    public PublicData() {
        this(new Data<>(), new Data<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private PublicData(Data<EventSetup> ES, Data<ElectionParametersPlain> EP, Data<ElectionDescriptions> ED) {
        super(ES, EP, ED);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private PublicData(PublicData publicData) {
        this(publicData.get_ES(), publicData.get_EP(), publicData.get_ED());
    }

    public Data<EventSetup> get_ES() {
        return this.getFirst();
    }

    public Data<ElectionParametersPlain> get_EP() {
        return this.getSecond();
    }

    public Data<ElectionDescriptions> get_ED() {
        return this.getThird();
    }

    /**
     * Calling this method stores the given content of type {@link MAP1} into the printing authority's public data.
     *
     * @param content The given content
     */
    public void setContent(MAP1 content) {
        this.get_ES().set(content.get_ES());
        this.get_EP().set(content.get_EP());
        this.get_ED().set(content.get_ED());
    }

}
