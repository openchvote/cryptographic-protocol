/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.printingauthority.writein;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionCard;
import ch.openchvote.core.algorithms.protocols.common.model.ElectionCardData;
import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.core.protocol.protocols.writein.content.message.MEP1;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.tuples.Pair;
import ch.openchvote.base.utilities.tuples.Tuple;

/**
 * Instances of this class represent the printing authority's secret data. The class inherits from the generic
 * {@link Tuple} subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects
 * to store. Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class SecretData extends Pair<
        // election cards
        EventData.DataMap<Vector<ElectionCardData>>, // bold_D
        EventData.Data<Vector<ElectionCard>>> // bold_ec
        implements EventData {

    public SecretData() {
        this(new DataMap<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private SecretData(DataMap<Vector<ElectionCardData>> bold_D, Data<Vector<ElectionCard>> bold_ec) {
        super(bold_D, bold_ec);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private SecretData(SecretData secretData) {
        this(secretData.get_bold_D(), secretData.get_bold_ec());
    }

    public DataMap<Vector<ElectionCardData>> get_bold_D() {
        return this.getFirst();
    }

    public Data<Vector<ElectionCard>> get_bold_ec() {
        return this.getSecond();
    }

    /**
     * Calling this method stores the given content of type {@link MEP1} into the printing authority's secret data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, MEP1 content) {
        this.get_bold_D().set(index, content.get_bold_d());
    }

}
