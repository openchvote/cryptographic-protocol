/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.printingauthority.plain.states;

import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Message;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.exceptions.CommunicationException;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.base.framework.protocol.Protocol;
import ch.openchvote.core.printingauthority.PrintingAuthority;
import ch.openchvote.core.printingauthority.plain.EventContext;
import ch.openchvote.core.protocol.phases.Initialization;
import ch.openchvote.core.protocol.protocols.plain.content.message.MAP1;

import static ch.openchvote.base.framework.exceptions.CommunicationException.Type.INCONSISTENT_EVENT_SETUP;

@SuppressWarnings("MissingJavadoc")
@Phase(Initialization.class)
@Notify(Status.Type.READY)
public final class S100 extends State<PrintingAuthority, EventContext> {

    public S100(PrintingAuthority printingAuthority, EventContext eventContext) {
        super(printingAuthority, eventContext);
        this.registerMessageHandler(MAP1.class, this::handleMAP1);
    }

    private void handleMAP1(Message message) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var protocolId = this.eventContext.getProtocolId();
        var protocolName = Protocol.getPrintName(protocolId);
        var securityLevel = this.eventContext.getSecurityLevel();

        // get and check message content (no auxiliary data)
        var MAP1 = this.party.getAndCheckContent(MAP1.class, message, null, securityLevel);

        // check election setup consistency
        if (!MAP1.get_ES().checkConsistency(protocolName, securityLevel) || !MAP1.get_EP().checkConsistency(eventId)) {
            throw new CommunicationException(INCONSISTENT_EVENT_SETUP, message);
        }

        // update public data
        publicData.setContent(MAP1);

        // update state
        this.party.updateState(this.eventContext, S200.class);
    }

}
