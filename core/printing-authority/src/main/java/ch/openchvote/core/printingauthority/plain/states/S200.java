/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.printingauthority.plain.states;

import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.communication.Message;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.printingauthority.PrintingAuthority;
import ch.openchvote.core.printingauthority.plain.EventContext;
import ch.openchvote.core.printingauthority.plain.states.error.A1;
import ch.openchvote.core.printingauthority.plain.tasks.TP1;
import ch.openchvote.core.protocol.protocols.plain.content.message.MEP1;
import ch.openchvote.core.protocol.protocols.plain.content.mail.MPV1;
import ch.openchvote.core.protocol.phases.Preparation;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.base.utilities.tuples.Triple;

@SuppressWarnings("MissingJavadoc")
@Phase(Preparation.class)
@Notify(Status.Type.READY)
public final class S200 extends State<PrintingAuthority, EventContext> {

    public S200(PrintingAuthority printingAuthority, EventContext eventContext) {
        super(printingAuthority, eventContext);
        this.registerMessageHandler(MEP1.class, this::handleMEP1);
    }

    private void handleMEP1(Message message) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var secretData = this.eventContext.getSecretData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var j = ES.get_SP().getIndexOf(message.getSenderId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();
        var N_E = EP.get_N_E();
        var bold_d_V = ED.get_bold_d_V();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var MEP1 = this.party.getAndCheckContent(MEP1.class, message, aux, securityLevel);

        // update secret data
        secretData.setContent(j, MEP1);

        // check if all MEP1 messages are available
        if (TP1.isReady(publicData, secretData)) {
            try {
                // perform task
                TP1.run(publicData, secretData);

                // select event data
                var bold_ec = secretData.get_bold_ec().get();

                // send MPV1 to voters
                for (int i : IntSet.range(1, N_E)) {
                    var EC_i = bold_ec.getValue(i);
                    this.party.deliverMail(eventId, bold_d_V.getValue(i), new MPV1(EC_i));
                }

                // update state
                this.party.updateState(this.eventContext, FINAL.class);

            } catch (Algorithm.Exception exception) {
                // move to error state
                this.party.updateState(this.eventContext, A1.class);
            }
        }
    }

}
