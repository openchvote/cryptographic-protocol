# Printing Authority

This module provides an implementation of the CHVote party *"Printing Authority"*. In
the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325), the role of the printing authority is described
as follows:

> The printing authority is responsible for printing the election cards and delivering them to the voters. They receive
> the data necessary for generating the election cards from the election administrator and the election authorities.

The intention of this Maven module is to provide production-quality Java code that implements the printing authority's
role in the CHVote protocol. Robust implementations of corresponding services can be connected to this module by using
Java's `ServiceLoader.load()` functionality.

## Specification

### Phases and States

| Phase          | States | Error States | Task                                     |
|:---------------|:-------|:-------------|:-----------------------------------------|
| Initialization | S100   | –            | –                                        |
| Preparation    | S200   | A1           | Create and send election cards to voters |
| Election       | –      | –            | –                                        |
| Tallying       | –      | –            | –                                        |
| Verification   | –      | –            | –                                        |
| Inspection     | –      | –            | –                                        |
| Testing        | –      | –            | –                                        |
| [none]         | FINAL  | –            | –                                        |

### State Diagram

![StateDiagram](img/stateDiagram.svg)

### Communication

#### Services:

|                        | PrintingAuthority |
|:-----------------------|:------------------|
| EventService           | Subscriber        |
| MailingService         | Sender            |
| MessagingService       | Subscriber        |
| PublicationService     | –                 |
| RequestResponseService | –                 |
| UserInterface          | –                 |
| TestingService         | –                 |

#### Incoming Messages:

| State | Phase          | Protocol | Name | Type    | Sender             | Signed | Encrypted | Content                          |
|:-----:|:---------------|:--------:|:----:|:--------|:-------------------|:------:|:---------:|----------------------------------|
| S100  | Initialization |   7.1    | MAP1 | Message | Administrator      |  yes   |    no     | Event setup, election parameters |
| S200  | Preparation    |   7.4    | MEP1 | Message | Election authority |  yes   |    yes    | Election card data               |

#### Outgoing Messages:

| State | Phase       | Protocol | Name | Type | Receiver | Signed | Encrypted | Content        |
|:-----:|:------------|:--------:|:----:|:-----|:---------|:------:|:---------:|----------------|
| S200  | Preparation |   7.4    | MPV1 | Mail | Voter    |   no   |    no     | Election cards |

#### Coordination:

| Type           | State | Phase          | New State | New Phase      |
|:---------------|:-----:|:---------------|:---------:|:---------------|
| onInitialize() |   –   | –              |   S100    | Initialization |
| onStart()      | S100  | Initialization |   S100    | Initialization |
| onStart()      | S200  | Preparation    |   S200    | Preparation    |
| onTerminate()  | FINAL | [none]         |     –     | –              |

### Tasks

| Task | State | Phase       | Protocol | AlgorithmicException | TaskException |
|:----:|:-----:|:------------|:--------:|:--------------------:|:-------------:|
| TP1  | S200  | Preparation |   7.4    |          A1          |       –       |

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of
this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts
is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as
follows:

```console
mvn clean install
-- a lot of output
```

## Maven Dependencies

The following dependency tree shows the Maven dependencies of this module.

```console
mvn dependency:tree -Dscope=compile -Dverbose=false
[INFO] --- dependency:3.6.0:tree (default-cli) @ printing-authority ---
[INFO] io.gitlab.openchvote:printing-authority:jar:2.4
[INFO] \- io.gitlab.openchvote:protocol:jar:2.4:compile
[INFO]    +- io.gitlab.openchvote:algorithms:jar:2.4:compile
[INFO]    |  \- io.gitlab.openchvote:utilities:jar:2.4:compile
[INFO]    |     \- com.verificatum:vmgj:jar:1.2.2:compile
[INFO]    \- io.gitlab.openchvote:framework:jar:2.4:compile
```

## Java Module Dependencies

The following module dependency diagram illustrates the Java module dependencies to other modules within this project:

![Java module dependencies](img/printingAuthority.svg)
