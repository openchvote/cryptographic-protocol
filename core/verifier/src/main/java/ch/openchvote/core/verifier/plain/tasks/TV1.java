/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.verifier.plain.tasks;

import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.protocols.plain.algorithms.VerifyElection;
import ch.openchvote.core.protocol.parameters.SecurityParameters;
import ch.openchvote.base.utilities.sequence.Vector;
import ch.openchvote.base.utilities.set.IntSet;
import ch.openchvote.core.verifier.plain.PublicData;

@SuppressWarnings("MissingJavadoc")
public final class TV1 {

    static public boolean isReady(PublicData publicData) {
        var s = publicData.get_ES().get().get_SP().get_s();
        return publicData.get_bold_pd().arePresent(IntSet.range(1, s));
    }

    static public void
    run(PublicData publicData) {

        // get algorithm service
        var algorithmService = AlgorithmService.load();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var SL = ES.get_SL();

        // get security and usability parameters
        var securityParameters = new SecurityParameters(SL);

        // select event data
        var EP = publicData.get_EP().get();
        var ER = publicData.get_ER().get();
        var PD_A = publicData.get_PD_A().get();
        var bold_pd = publicData.get_bold_pd().mapTo(Vector::of);

        // perform task
        var bold_u = EP.get_bold_u();
        var bold_n = EP.get_bold_n();
        var bold_k = EP.get_bold_k();
        var bold_w = EP.get_bold_w();
        var bold_E = EP.get_bold_E();
        var v = algorithmService.run(VerifyElection.class, securityParameters, bold_n, bold_k, bold_u, bold_w, bold_E, ER, PD_A, bold_pd);

        // update event data
        publicData.get_v().set(v);
    }

}
