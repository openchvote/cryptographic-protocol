/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.verifier.writein;

import ch.openchvote.core.algorithms.protocols.common.model.ElectionDescriptions;
import ch.openchvote.core.algorithms.protocols.common.model.EventSetup;
import ch.openchvote.core.algorithms.protocols.writein.model.ElectionParametersWriteIn;
import ch.openchvote.core.algorithms.protocols.writein.model.ElectionResultWriteIn;
import ch.openchvote.core.algorithms.protocols.writein.model.PublicDataAdministrator;
import ch.openchvote.core.algorithms.protocols.writein.model.PublicDataElectionAuthority;
import ch.openchvote.base.framework.party.EventData;
import ch.openchvote.core.protocol.protocols.writein.content.publication.PAP1;
import ch.openchvote.core.protocol.protocols.writein.content.publication.PAP2;
import ch.openchvote.core.protocol.protocols.writein.content.publication.PEP1;
import ch.openchvote.base.utilities.tuples.Septuple;
import ch.openchvote.base.utilities.tuples.Tuple;

/**
 * Instances of this class represent the verifier's public data. The class inherits from the generic {@link Tuple}
 * subclass, that matches with the number of {@link EventData.Data} and {@link EventData.DataMap} objects to store.
 * Convenience methods for accessing the fields with names as specified by the CHVote protocol are provided.
 */
@SuppressWarnings("MissingJavadoc")
public final class PublicData extends Septuple<
        // event setup and election parameters
        EventData.Data<EventSetup>, // ES
        EventData.Data<ElectionParametersWriteIn>, // EP
        EventData.Data<ElectionDescriptions>, // ED
        // election result
        EventData.Data<ElectionResultWriteIn>, // ER
        // event data
        EventData.Data<PublicDataAdministrator>, // PD_A
        EventData.DataMap<PublicDataElectionAuthority>,
        EventData.Data<Boolean>> // v
        implements EventData {

    public PublicData() {
        this(new Data<>(), new Data<>(), new Data<>(), new Data<>(), new Data<>(), new DataMap<>(), new Data<>());
    }

    // private constructor for initializing all fields
    private PublicData(Data<EventSetup> ES, Data<ElectionParametersWriteIn> EP, Data<ElectionDescriptions> ED, Data<ElectionResultWriteIn> ER, Data<PublicDataAdministrator> PD_A, DataMap<PublicDataElectionAuthority> bold_PD, Data<Boolean> v) {
        super(ES, EP, ED, ER, PD_A, bold_PD, v);
    }

    // private copy constructor
    @SuppressWarnings("unused")
    private PublicData(PublicData publicData) {
        this(publicData.get_ES(), publicData.get_EP(), publicData.get_ED(), publicData.get_ER(), publicData.get_PD_A(), publicData.get_bold_pd(), publicData.get_v());
    }

    public Data<EventSetup> get_ES() {
        return this.getFirst();
    }

    public Data<ElectionParametersWriteIn> get_EP() {
        return this.getSecond();
    }

    public Data<ElectionDescriptions> get_ED() {
        return this.getThird();
    }

    public Data<ElectionResultWriteIn> get_ER() {
        return this.getFourth();
    }

    public Data<PublicDataAdministrator> get_PD_A() {
        return this.getFifth();
    }

    public DataMap<PublicDataElectionAuthority> get_bold_pd() {
        return this.getSixth();
    }

    public Data<Boolean> get_v() {
        return this.getSeventh();
    }

    /**
     * Calling this method stores the given content of type {@link PAP1} into the verifier's public data.
     *
     * @param content The given content
     */
    public void setContent(PAP1 content) {
        this.get_ES().set(content.get_ES());
        this.get_EP().set(content.get_EP());
        this.get_ED().set(content.get_ED());
        this.get_ER().set(content.get_ER());
    }

    /**
     * Calling this method stores the given content of type {@link PAP2} into the verifier's public data.
     *
     * @param content The given content
     */
    public void setContent(PAP2 content) {
        this.get_PD_A().set(content.get_PD());
    }

    /**
     * Calling this method stores the given content of type {@link PEP1} into the verifier's public data.
     *
     * @param index   The index of the received content
     * @param content The given content
     */
    public void setContent(int index, PEP1 content) {
        this.get_bold_pd().set(index, content.get_PD());
    }

}
