/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.core.verifier.plain.states;

import ch.openchvote.base.framework.annotations.state.Notify;
import ch.openchvote.base.framework.annotations.state.Phase;
import ch.openchvote.base.framework.communication.Publication;
import ch.openchvote.base.framework.communication.Status;
import ch.openchvote.base.framework.party.State;
import ch.openchvote.core.verifier.Verifier;
import ch.openchvote.core.protocol.phases.Verification;
import ch.openchvote.core.protocol.protocols.plain.content.publication.PAP2;
import ch.openchvote.core.protocol.protocols.plain.content.publication.PEP1;
import ch.openchvote.base.utilities.tuples.Triple;
import ch.openchvote.core.verifier.plain.EventContext;

@SuppressWarnings("MissingJavadoc")
@Phase(Verification.class)
@Notify(Status.Type.READY)
public final class S500 extends State<Verifier, EventContext> {

    public S500(Verifier verifier, EventContext eventContext) {
        super(verifier, eventContext);
        this.registerPublicationHandler(PEP1.class, this::handlePEP1);
        this.registerPublicationHandler(PAP2.class, this::handlePAP2);
    }

    private void handlePAP2(Publication publication) {

        // decompose event context
        var eventId = this.eventContext.getEventId();
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select event definition
        var ES = publicData.get_ES().get();
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();


        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var PAP2 = this.party.getAndCheckContent(PAP2.class, publication, aux, securityLevel);

        // update public data
        publicData.setContent(PAP2);

        // update state
        this.party.updateState(this.eventContext, S510.class);

        // run self activation
        var stateId = this.getId();
        this.party.selfActivate(eventId, stateId, State::handleSelfActivation);

    }

    private void handlePEP1(Publication publication) {

        // decompose event context
        var publicData = this.eventContext.getPublicData();
        var securityLevel = this.eventContext.getSecurityLevel();

        // select setup parameters
        var ES = publicData.get_ES().get();
        var j = ES.get_SP().getIndexOf(publication.getPublisherId());

        // select election parameters and descriptions
        var EP = publicData.get_EP().get();
        var ED = publicData.get_ED().get();

        // get and check message content
        var aux = new Triple<>(ES, EP, ED);
        var PEP1 = this.party.getAndCheckContent(PEP1.class, publication, aux, securityLevel);

        // update public data
        publicData.setContent(j, PEP1);

    }

}
