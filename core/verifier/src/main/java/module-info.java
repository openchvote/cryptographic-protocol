/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
/**
 * This module is an implementation of the CHVote party <i>Verifier</i>.
 */
module ch.openchvote.core.verifier {

    requires transitive ch.openchvote.core.protocol;

    exports ch.openchvote.core.verifier;

    opens ch.openchvote.core.verifier.plain to ch.openchvote.base.framework, ch.openchvote.base.utilities;
    opens ch.openchvote.core.verifier.plain.states to ch.openchvote.base.framework;

    opens ch.openchvote.core.verifier.writein to ch.openchvote.base.framework, ch.openchvote.base.utilities;
    opens ch.openchvote.core.verifier.writein.states to ch.openchvote.base.framework;
}
