# OpenCHVote Internet Voting System

This OpenCHVote system is a concise Java implementation of the CHVote voting protocol. The protocol specification is
published on the *Cryptology ePrint Archive* since 2017. Current and earlier versions of this document are available
at <https://eprint.iacr.org/2017/325>. The current Version 2.4.2 of this software matches with Version 4.3 of the
specification document.

## Project Overview

The origin of this project is a collaboration between the State of Geneva and the Bern University of Applied Sciences,
which lasted from 2016 until 2019. The aforementioned CHVote Protocol Specification document is the main output of this
collaboration.

The software development for this project started in October 2019. At an early stage of the project, the code released
by the State of Geneva in June 2019 at <https://chvote2.gitlab.io> served as a reference point for the software design
in this project. Otherwise, the two projects have nothing in common.

The general goal of this project is to pursue the achievements of CHVote project and to make them available to the
public under a free [license](Readme.md#License). By releasing the source code to the public, we invite the
community to participate in this project.

#### Scope

In the design of the system, we introduced a strict separation between *cryptographically relevant* and *
cryptographically irrelevant* components. As the scope of our software is limited to the cryptographically relevant
components, OpenCHVote does not provide a complete Internet voting system that is ready to use in practice. However, the
current software allows simulating elections events by executing the CHVote protocol on a single machine. All
cryptographically relevant aspects of the protocol are included in such a simulation.

Within this scope, the implementation is centered around the CHVote voting protocol. The code covers almost all relevant
aspects of the specification document, including the extended protocol that allows write-in candidates and the proposed
performance optimizations. In order to achieve the highest possible degree of correspondence, we looked at the
specification and the code as two facets of *the same thing*. In such a mindset, deviations are only acceptable in
exceptional cases, which need to be well documented.

#### Key Features

The current OpenCHVote implementation provides the following key features:

* Complete parameterizable protocol run.
* Simultaneous execution of multiple protocol runs in parallel.
* Two protocol variants: *plain* (basic protocol without write-ins) and *writeIn* (extended protocol with write-ins).
* Performance optimizations: fixed-based exponentiations, product exponentiations, group membership tests, caching of
  recursive hash values, etc.
* Infrastructure interfaces to cryptographically irrelevant components (messaging service, publication service,
  persistence service, logging, etc.).
* No compulsory dependencies to third-party libraries or frameworks (dependencies
  to [VMGJ](https://github.com/verificatum/verificatum-vmgj) and [Spock](http://spockframework.org/) are optional).
* No dependencies to non-JavaSE technologies.
* JavaScript implementation and integration of client algorithms.

#### Missing Features

* Reliable and secure infrastructure services.
* Other infrastructure software components.
* Strategy to recover from error states.
* Rigorous testing of algorithms and framework.
* Update of formal proofs.

## Maven Modules

This is a multimodule Maven project. The dependencies among modules can be shown via Maven's
command:

```console
mvn dependency:tree -Dscope=compile -Dverbose=true -Pjavascript
```

Output (truncated):
```console
...
[INFO] openchvote ......................................... SUCCESS [  0.233 s]
[INFO] openchvote:base .................................... SUCCESS [  0.001 s]
[INFO] openchvote:base:framework .......................... SUCCESS [  0.005 s]
[INFO] openchvote:base:utilities .......................... SUCCESS [  0.007 s]
[INFO] openchvote:core .................................... SUCCESS [  0.001 s]
[INFO] openchvote:core:algorithms ......................... SUCCESS [  0.005 s]
[INFO] openchvote:core:protocol ........................... SUCCESS [  0.002 s]
[INFO] openchvote:core:administrator ...................... SUCCESS [  0.002 s]
[INFO] openchvote:core:services ........................... SUCCESS [  0.001 s]
[INFO] openchvote:core:election-authority ................. SUCCESS [  0.001 s]
[INFO] openchvote:core:printing-authority ................. SUCCESS [  0.002 s]
[INFO] openchvote:core:verifier ........................... SUCCESS [  0.001 s]
[INFO] openchvote:simulation .............................. SUCCESS [  0.002 s]
[INFO] openchvote:simulation:inspection-client ............ SUCCESS [  0.001 s]
[INFO] openchvote:simulation:services ..................... SUCCESS [  0.001 s]
[INFO] openchvote:simulation:voting-client ................ SUCCESS [  0.003 s]
[INFO] openchvote:simulation:voter ........................ SUCCESS [  0.003 s]
[INFO] openchvote:javascript .............................. SUCCESS [  0.001 s]
[INFO] openchvote:javascript:services-js .................. SUCCESS [  0.021 s]
[INFO] openchvote:simulation:simulator .................... SUCCESS [  0.006 s]
[INFO] openchvote:javascript:algorithms-js ................ SUCCESS [  0.003 s]
...
```

## Java Modules

This version of the OpenCHVote implementation is based on
[Java's Platform Modularization System JPMS](https://en.wikipedia.org/wiki/Java_Platform_Module_System), which was
introduced with Java 9. In this implementation, the Java modules correspond one-to-one to the Maven modules as
illustrated above.

OpenCHVote consists of the following JPMS modules and submodule (in alphabetical order):

- `ch.openchvote.base`
- `ch.openchvote.base.framework`
- `ch.openchvote.base.utilities`
- `ch.openchvote.core`
- `ch.openchvote.core.administrator`
- `ch.openchvote.core.algorithms`
- `ch.openchvote.core.electionauthority`
- `ch.openchvote.core.printingauthority`
- `ch.openchvote.core.protocol`
- `ch.openchvote.core.services`
- `ch.openchvote.core.verifier`
- `ch.openchvote.javascript.services`
- `ch.openchvote.simulation`
- `ch.openchvote.simulation.inspectionclient`
- `ch.openchvote.simulation.services`
- `ch.openchvote.simulation.simulator`
- `ch.openchvote.simulation.voter`
- `ch.openchvote.simulation.votingclient`

These modules and their dependencies are shown in the following diagram:

![Java Modules](img/java-modules.svg)

Remarks:

- Module `ch.openchvote.base.framework` does not depend on anything.
- Module `ch.openchvote.base.utilities` does not depend on anything else but `vmgj` (the optional VMGJ library)
  dependency.
- All party modules are *«open»* to the module `ch.openchvote.base.framework` to enable access to the party's states via
  reflection.
- Module `ch.openchvote.core.services` *«provides»* services inherently tied to the CHVote protocol.
- Module `ch.openchvote.simulation.services` *«provides»* exemplary implementations of services referenced in
  module `ch.openchvote.base.framework` as Java interfaces. Real e-voting implementations must provide their own service
  implementations.

## Usage

This section explains how applications using this software must be configured. It is assumed that you use Maven as your
build system. In addition, it is assumed that your program uses the Java module system JPMS.

#### Maven Dependencies

Add the necessary dependencies into you `dependencies` section of your POM file(s). For example:

```code
<dependencies>
    <dependency>
        <groupId>io.gitlab.openchvote</groupId>
        <artifactId>utilities</artifactId>
        <version>VERSION</version>
    </dependency>
    :
    :
</dependencies>
```

`VERSION` denotes the OpenCHVote version you are going to use.

#### Java Module Descriptor Files

Depending on the Java modules of OpenCHVote you use, add the required `requires` directives to your `module-info.java`
files. For example:

```code
module YOUR_MODULE_NAME {
    requires ch.openchvote.base.utilities;
    :
    :
}
```

#### Project-Specific Service Implementations

OpenCHVote provides complete implementations for each party (administrator, election authorities, etc.), including
exemplary implementations for the services (messaging service, persistence service, etc.) used by the parties. Depending
on the technology being used, individual parties may need companion components which delegate request ets. to the
OpenCHVote parties.

Module `ch.openchvote.simulation.services` provides exemplary implementations which must be substituted by real
implementations.

To substitute the exemplary service implementations with real ones, make a separate Java module providing the necessary
service classes, and add the module to the projects main module description file. The OpenCHVote framework then uses
Java's ServiceLoader mechanism to load respective instances of your service classes.

Assuming that you implement the service classes in a separate Java module, provide the following directives within
the `module-info.java` file:

```code
module YOUR_SERVICE_MODULE_NAME {
    requires ch.openchvote.base.framework;
    provides ch.openchvote.base.framework.MessagingService with your.package.services.YourMessagingService;
    :
    :
}
```

## Software and Installation

This section refers to the software, its dependencies, its compilation from scratch, and its installation.

OpenCHVote is a multimodule [Maven](http://www.maven.org) project for Java SE. Currently, no other platforms or
languages (e.g. JavaScript) are supported by any of the included components.

#### Version History

| Branch Name | Tag Name | Comment       | Java | Release Date       | [CHVote Protocol Specification](https://eprint.iacr.org/2017/325) | 
|:-----------:|:--------:|:--------------|:----:|:-------------------|:-----------------------------------------------------------------:|
|    2.4.2    |  r2.4.2  | Release 2.4.2 |  21  | February 7, 2025   |                                4.3                                |
|    2.4.1    |  r2.4.1  | Release 2.4.1 |  21  | January 10, 2025   |                                4.3                                |
|     2.4     |   r2.4   | Release 2.4   |  21  | January 10, 2025   |                                4.3                                |
|    2.3.1    |  r2.3.1  | Release 2.3.1 |  21  | December 20, 2024  |                                4.3                                |
|     2.3     |   r2.3   | Release 2.3   |  21  | December 13, 2024  |                                4.3                                |
|     2.2     |   r2.2   | Release 2.2   |  21  | September 30, 2024 |                                4.2                                |
|     2.1     |   r2.1   | Release 2.1   |  21  | August 16, 2024    |                                4.1                                |
|     2.0     |   r2.0   | Release 2.0   |  21  | June 21, 2024      |                                4.0                                |
|     1.3     |   r1.3   | Release 1.3   |  17  | February 28, 2023  |                                3.5                                |
|    1.2.2    |  r1.2.2  | Release 1.2.2 |  17  | November 29, 2022  |                                3.4                                |
|    1.2.1    |  r1.2.1  | Release 1.2.1 |  17  | August 29, 2022    |                                3.3                                |
|     1.2     |   r1.2   | Release 1.2   |  17  | August 15, 2022    |                                3.3                                |
|     1.1     |   r1.1   | Release 1.1   |  11  | December 22, 2020  |                                3.2                                |
|     1.0     |   r1.0   | Release 1.0   |  11  | October 7, 2020    |                                3.1                                |

See [release notes](ReleaseNotes.md)

#### Dependencies

Except for the natural dependency to Java SE, there are only two optional dependencies to other non-proprietary software
components:

* For improving the performance of modular exponentiations,
  the [Verificatum Multiplicative Groups Library for Java](https://github.com/verificatum/verificatum-vmgj) (VMGJ) can
  be used. For convenience purposes, a wrapper is part of this distribution, located in the `./project-maven-repo`
  repository. At runtime, the wrapper tries to resolve this dependency. No harm occurs if the wrapper cannot resolve the
  dependency (a warning is displayed). Douglas Wikstrom's [free license](LICENSE_VMGJ.md) applies. VMGJ implies two
  transitive dependencies to [GMP](https://gmplib.org) and [GMPMEE](https://github.com/verificatum/verificatum-gmpmee).
  Corresponding licenses apply.

* The [Spock](http://spockframework.org/) testing framework is used for implementing data-driven unit tests.
  The [Apache License, Version 2.0,](LICENSE_APACHE.md) applies.

#### Compilation, Packaging, and Installation without JavaScript Algorithms (Default)

This step compiles and installs OpenCHVote with the algorithms written in Java only. This is the preferred method if you
don't have [Docker](https://www.docker.com/) installed on your machine, or if you can't execute Docker.

To build every artifact in each module of this project except the ones of the `javascript` submodules, simply type the
following in the project's root directory:

```console
mvn clean install [-DskipTests]
```

**Note:** You can safely ignore to following warnings (they result from the automatic module name inferred for the
non-modular dependency "vmgj-1.2.2"):

```
[WARNING] **************************************************************************************************************************************
[WARNING] * Required filename-based automodules detected: [vmgj-1.2.2.jar]. Please don't publish this project to a public artifact repository! *
[WARNING] **************************************************************************************************************************************
```

#### Compilation and Installation with JavaScript Algorithms

This step compiles and installs OpenCHVote with both, the algorithms written in Java and in JavaScript.

> **Note:** To perform the step below, you *must* have a local
> [Docker](https://www.docker.com/)
> installation on your machine, and Docker must be running.
> Otherwise, you get a `No <dockerHost> given, no DOCKER_HOST environment variable...` error message.
> See next section if you haven't.

To build every artifact in each module of this project, simply type the following in the project's root directory:

```console
mvn clean install -Pjavascript [-DskipTests]
```

> **Note** for [Docker Desktop](https://www.docker.com/products/docker-desktop/) users: If you get
> a `No <dockerHost> given, no DOCKER_HOST environment variable...` error during the build of `javascript` then it is
> likely a link is missing on your system. Then for Linux and Mac users create the missing link, for example by typing
> `sudo ln -s -f /Users/<user>/.docker/run/docker.sock /var/run/docker.sock` (replace `<user>` by your username).

This will compile, test, and generate a JAR file in each of the `target` subdirectories of each module and install them
in the local Maven repository. If activated, profile `skipSimulationTest` skips the long-lasting test of the `simulator`
module. (Recommended when using CI/CD pipelines.)

> **Note:** The same remark on warning messages above applies.

```
[WARNING] **************************************************************************************************************************************
[WARNING] * Required filename-based automodules detected: [vmgj-1.2.2.jar]. Please don't publish this project to a public artifact repository! *
[WARNING] **************************************************************************************************************************************
```

> **Note**: To perform this step, there is no need to have
> a Docker system installed and running. However, there is
> no harm if it is up and running.

Election events can be simulated by using the class `ElectionSimulator` from the `simulator` module. To execute the
election simulator from the project's root directory, type:

```console
mvn -q exec:exec
```

Output:

```text
[INFO]    Plain-3   : Election result published
---------------------------------
Candidate    CC1  CC2  CC3  Total  
---------------------------------
Candidate-1    0    0    2      2  
Candidate-2    0    1    0      1  
Candidate-3    0    1    1      2  
Candidate-4    0    0    1      1  
Candidate-5    0    0    1      1  
Candidate-6    0    1    1      2  
Candidate-7    0    1    2      3  
---------------------------------

[INFO]    Plain-4   : Election result verified
[INFO]    Plain-3   : Election result verified
[INFO]    WriteIn-2 : Event terminated
[INFO]    WriteIn-2 : Election rresult published
----------------------------------------
Candidate      CC1  CC2  CC3  CC4  Total  
----------------------------------------
Candidate-1      0    0    0    0      0  
Candidate-2      0    0    0    0      0  
Candidate-3      0    1    0    0      1  
Candidate-4      0    1    0    0      1  
Candidate-5      0    0    0    0      0  
Candidate-6      0    1    0    0      1  
Candidate-7      0    0    0    0      0  
Candidate-8      0    0    0    0      0  
Candidate-9      0    1    0    0      1  
----------------------------------------
Merle_Rodarte    0    1    0    0      1  
Oma_Huggard      0    1    0    0      1  
----------------------------------------
Pierre_Riter     0    1    0    0      1  
----------------------------------------

[INFO]    WriteIn-2 : Election result verified
```

Remark: Do not use the exec-plugin's `exec:java` goal.

#### Executing the Election Simulator with JavaScript Algorithms

> **Note**: To perform this step, you *must* have installed this software as described in section "Compilation and
> Installation with JavaScript Algorithms" above.

> **Note**: To perform this step, your Docker system must be up and running.

Election events can be simulated by using the class `ElectionSimulator` from the `simulator` module. To execute the
election simulator from the project's root directory, type:

```console
mvn -q exec:exec -Pjavascript
```

Output:

```text
[STATUS]  WriteIn-1 : EVENT INITIALIZED
[INFO]    JavaScriptAlgorithmService: Server successfully started
[STATUS]  WriteIn-1 : EVENT TERMINATED
[STATUS]  WriteIn-1 : RESULT PUBLISHED

Candidate     CC1  CC2  CC3  CC4  Total  
---------------------------------------
Candidate-1     1    0    0    1      2  
Candidate-2     0    0    0    0      0  
Candidate-3     1    0    0    1      2  
Candidate-4     0    0    0    0      0  
Candidate-5     1    0    0    1      2  
  :
  :
Candidate-20        0    0    0      0  
Candidate-21        1    0    1      2  
--------------------------------------
Chantell_Huggard    0    1    0      1  
Kary_Bordelon       0    0    1      1  
Lester_Brass        0    0    1      1  
Robyn_Aichele       1    0    0      1  
--------------------------------------
--------------------------------------
Chantell_Ringler    0    0    1      1  
Suzanne_Huggard     1    0    0      1  
--------------------------------------

[STATUS]  WriteIn-2 : SUCCESSFULLY VERIFIED
```

Remark: Do not use the exec-plugin's `exec:java` goal.

#### Generating API Documentation

You can use Maven to build the API documentation. The API documentation will reside in your file system
under `target/reports/apidocs`. To create this documentation, type the following:

```console
mvn clean javadoc:aggregate
```

Or, if a Docker system is installed and running:

```console
mvn clean javadoc:aggregate -Pjavascript
```

> Note: Do not use Maven's command `mvn clean javadoc:javadoc`.

#### Generating Site Documentation

You can use Maven to build the site documentation. The site documentation will reside in your file system
under `target/staging`. To create this documentation, type the following:

```console
mvn clean site site:stage
```

The generated documentation is then ready for publication.

Or, if a Docker system is installed and running:

```console
mvn clean site site:stage -Pjavascript
```

## Project Documentation on GitLab (Pages)

See the GitLab.com [Project Pages](https://openchvote.gitlab.io/cryptographic-protocol/) for further documentation of OpenCHVote.

## Publications

* R. Haenni, E. Dubuis, R. E. Koenig, P. Locher, [*CHVote: Sixteen Best Practices and Lessons
  Learned*](https://e-voting.bfh.ch/app/download/8038993661/HDKL20.pdf), E-Vote-ID 2020, 5th International Joint
  Conference on Electronic Voting, LNCS 12455, 2020

* R. Haenni, R. E. Koenig and P. Locher and E. Dubuis, [*CHVote Protocol Specification - Version
  4.3*](https://eprint.iacr.org/2017/325), IACR Cryptology ePrint Archive, 2017/325, 2024

* D. Bernhard, V. Cortier, P. Gaudry, M. Turuani, B. Warinschi, [*Verifiability Analysis of
  CHVote*](https://eprint.iacr.org/2018/1052), IACR Cryptology ePrint Archive, 2018/1052, 2018

* R. Haenni, E. Dubuis, R. E. Koenig, [*Cast-as-Intended Verification in Electronic Elections Based on Oblivious
  Transfer*](https://e-voting.bfh.ch/app/download/7233132561/HKD16.pdf), E-Vote-ID 2016, 1st International Joint
  Conference on Electronic Voting, LNCS 10141, 2016

* R. Haenni, P. Locher, N. Gailly, [*Improving the Performance of Cryptographic Voting
  Protocols*](https://e-voting.bfh.ch/app/download/7833228661/HLG19.pdf), Voting'19, FC 2019 International Workshops,
  LNCS 11599, 272–288, 2019

* R. Haenni, P. Locher, [*Performance of Shuffling: Taking it to the
  Limits*](https://e-voting.bfh.ch/app/download/8038993261/HL20.pdf), Voting'20, FC 2020 International Workshops, LNCS
  12063, 369–385, 2020

* R. Haenni, I. Starý
  Kořánová, [An Alternative Group for Applications of ElGamal in Cryptographic Protocols](https://e-voting.bfh.ch/app/download/8227617461/HS23.pdf),
  E-Vote-ID'23, 2023

## License

For this software, the [GNU AFFERO GENERAL PUBLIC LICENSE](LICENSE.md) Version 3, 19 November 2007, applies.

## Feedback

We'd be happy to receive feedback of any kind from the community. Please use the GitLab issue tracking service
at [https://gitlab.com/groups/openchvote](https://gitlab.com/groups/openchvote/-/issues) or contact the contributors
list [here](Credits.md).

## Credits

Click [here](./Credits.md).
