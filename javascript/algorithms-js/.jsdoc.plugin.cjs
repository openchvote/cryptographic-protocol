exports.handlers = {
	beforeParse: function(e) {
		if (e.filename.endsWith("Tuples.js")) {
			e.source = e.source.replace(/@type \{\[([\w\s,]+)]}/g, "@type Array<$1>")
		}
	}
}
