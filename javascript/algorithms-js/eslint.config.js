import globals from "globals"
import js from "@eslint/js"
import jsdoc from "eslint-plugin-jsdoc"
import sortImportsEs6Autofix from "eslint-plugin-sort-imports-es6-autofix"


/** @type {import('eslint').Linter.Config[]} */
export default [
	{languageOptions: { globals: {...globals.browser, ...globals.node} }},
	js.configs.recommended,
	{ignores: ["target/"]},
	{
		plugins: {
			"sort-imports-es6-autofix": sortImportsEs6Autofix,
			jsdoc,
		},

		rules: {
			"indent": ["error", "tab"],
			"linebreak-style": ["error", "unix"],
			"quotes": ["error", "double"],
			"semi": ["error", "never"],

			"sort-imports-es6-autofix/sort-imports-es6": ["error", {
				ignoreCase: false,
				ignoreMemberSort: true,
				memberSyntaxSortOrder: ["none", "all", "single", "multiple"],
			}],

			"jsdoc/no-undefined-types": [1, {
				definedTypes: ["Iterable"],
			}],

			"jsdoc/require-param-description": 0,
			"jsdoc/require-returns-description": 0,
			"jsdoc/tag-lines": 1,
		}
	}
]
