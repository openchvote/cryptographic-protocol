/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { Alphabet } from "#Alphabet.js"
import { UsabilityParameters } from "#UsabilityParameters.js"


describe("UsabilityParameters", () => {
	it("should be constructable", () => {
		const usabilityParams = new UsabilityParameters("A57_A57_A10_A16", "LEVEL_1")
		assert.equal(usabilityParams.A_X, Alphabet.ALPHANUMERIC_57)
		assert.equal(usabilityParams.A_Y, Alphabet.ALPHANUMERIC_57)
		assert.equal(usabilityParams.A_V, Alphabet.BASE_10)
		assert.equal(usabilityParams.A_PA, Alphabet.BASE_16)
	})

	it("should no be constructable for invalid arguments", () => {
		assert.throw(() => new UsabilityParameters("A57_A57_A10", "LEVEL_1"))
		assert.throw(() => new UsabilityParameters("A01_A57_A10_A16", "LEVEL_1"))
		assert.throw(() => new UsabilityParameters("A57_A57_A10_A16_A16", "LEVEL_1"))
		assert.throw(() => new UsabilityParameters("A57_A57_A10_A16", "LEVEL_X"))
	})

	it("should compute correct L values", () => {
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_1").L_V, 3)
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_1").L_PA, 2)
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_2").L_V, 4)
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_2").L_PA, 3)
	})

	it("should compute correct ell values", () => {
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_1").ell_X, 39)
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_1").ell_X, 39)
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_1").ell_V, 8)
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_1").ell_PA, 4)
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_2").ell_X, 44)
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_2").ell_X, 44)
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_2").ell_V, 10)
		assert.equal(new UsabilityParameters("A57_A57_A10_A16", "LEVEL_2").ell_PA, 6)
	})

	it("should be immutable", () => {
		const usabilityParams = new UsabilityParameters("A57_A57_A10_A16", "LEVEL_1")
		assert.throw(() => usabilityParams.A_X = Alphabet.ALPHANUMERIC_62)
		assert.throw(() => usabilityParams._values.A_X = Alphabet.ALPHANUMERIC_62)
		assert.throw(() => usabilityParams.A_Y = 7)
		assert.throw(() => usabilityParams._values.A_Y = 7)
		assert.throw(() => usabilityParams.xyz = 7)
		assert.throw(() => usabilityParams._values.xyz = 7)

	})
})
