/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { StringBuilder } from "#util/StringBuilder.js"

describe("StringBuilder", () => {
	it("combines strings", () => {
		const sb = new StringBuilder()
		sb.append("a")
		sb.append("b")
		sb.append("c")
		assert.equal(sb.toString(), "abc")
	})
	it("can be filled up using setAt", () => {
		const sb = new StringBuilder(3)
		sb.setAt(1, "b")
		sb.setAt(0, "a")
		sb.setAt(2, "c")
		assert.equal(sb.toString(), "abc")
	})
	it("can be filled up using setAt and append", () => {
		const sb = new StringBuilder(3)
		sb.append("d")
		sb.setAt(1, "b")
		sb.setAt(0, "a")
		sb.setAt(2, "c")
		assert.equal(sb.toString(), "abcd")
	})
})
