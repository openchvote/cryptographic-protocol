/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { ByteArray } from "#util/sequence/ByteArray.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { Pair, Singleton, Triple, Tuple } from "#util/Tuples.js"
import { SerializationError } from "#util/Errors.js"
import { TWO } from "#util/math/BigInteger.js"
import { Vector, ZeroIndexedVector } from "#util/sequence/Vector.js"
import { bi } from "../util.js"
import { serialize } from "#util/Serializer.js"

describe("Serializer", () => {
	it("correctly serializes Integer", () => {
		assert.equal(serialize(2147483647), "2147483647")
		assert.equal(serialize(-2147483648), "-2147483648")
		assert.equal(serialize(0), "0")
		assert.equal(serialize(-237), "-237")
		assert.equal(serialize(2857), "2857")
		assert.throws(() => serialize(NaN), SerializationError)
		assert.throws(() => serialize(Infinity), SerializationError)
	})


	it("correctly serializes BigInteger", () => {
		assert.deepEqual(serialize(TWO.pow(1024)), "10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000")
		assert.deepEqual(serialize(TWO.pow(1024).multiply(bi(-1))), "-10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000")
		assert.deepEqual(serialize(bi(0)), "0")
		assert.deepEqual(serialize(bi(-237)), "-ed")
		assert.deepEqual(serialize(bi(2857)), "b29")

	})

	it("correctly serializes Booleans", () => {
		assert.equal(serialize(true), "1")
		assert.equal(serialize(false), "0")
	})

	it("correctly serializes Strings", () => {
		assert.equal(serialize(""), "[]")
		assert.equal(serialize("Hello World]"), "[Hello World\\]]")
		assert.equal(serialize("Lorem ipsum [dolor] sit amet,"), "[Lorem ipsum \\[dolor\\] sit amet,]")
	})


	it("correctly serializes Vector", () => {
		assert.equal(serialize(new Vector([])), "[1,0,[]]")
		assert.equal(serialize(new Vector([0, 1, 0])), "[1,3,[0,1,0]]")
		assert.equal(serialize(new Vector([728, 5763, 7462])), "[1,3,[728,5763,7462]]")
		assert.equal(serialize(new ZeroIndexedVector([728, 5763, 7462])), "[0,2,[728,5763,7462]]")
		assert.equal(serialize(new ZeroIndexedVector([0, 1, "bla"])), "[0,2,[0,1,[bla]]]")
	})


	it("correctly serializes Bytearrays)", () => {
		assert.equal(serialize(ByteArray.of(Uint8Array.from([49, 50, 51]))), "[313233]")
	})

	it("correctly serializes Matrix", () => {
		assert.equal(serialize(new Matrix(3, 3, [
			[11, 12, 13],
			[21, 22, 23],
			[31, 32, 33]])), "[1,3,1,3,[11,12,13,21,22,23,31,32,33]]")

	})

	it("correctly serializes Tuples", () => {
		assert.equal(serialize(new Tuple("first", 2, bi(3))), "[[first],2,3]")
	})
	it("correctly serializes Singletons", () => {
		assert.equal(serialize(new Singleton("first")), "[[first]]")
	})
	it("correctly serializes Pairs", () => {
		assert.equal(serialize(new Pair("first", 2)), "[[first],2]")
	})
	it("correctly serializes Triples", () => {
		assert.equal(serialize(new Triple("first", 2, bi(3))), "[[first],2,3]")
	})

	it("correctly serializes null values", () => {
		assert.equal(serialize(null), "*")
		assert.equal(serialize(undefined), "*")
	})


	it("cannot serialize other objects", () => {
		class D {
		}

		assert.throws(() => serialize(new D()))
	})


})
