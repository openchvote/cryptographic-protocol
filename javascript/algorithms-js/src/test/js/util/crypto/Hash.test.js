/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { ByteArray } from "#util/sequence/ByteArray.js"
import { Hash } from "#util/crypto/Hash.js"
import { SecurityParameters } from "#SecurityParameters.js"

const {L} = new SecurityParameters("LEVEL_0")

describe("Hash", () => {
	it("should throw on no given or wrong input", () => {
		assert.throws(() => Hash())
		assert.throws(() => Hash("Strings are not supported"))
	})
	it("should return digest with correct Array type ", () => {
		assert.isTrue(Hash(ByteArray.of(new Uint8Array())) instanceof ByteArray)
	})
	it("should return digest with the correct size ", () => {
		assert.equal(Hash(ByteArray.of(new Uint8Array())).length, L)
	})
})
