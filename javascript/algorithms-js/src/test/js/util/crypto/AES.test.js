/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { AESDecrypt, AESEncrypt } from "#util/crypto/AES.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { GenCiphertext } from "#general/algorithms/GenCiphertext.js"
import { GetPlaintext } from "#general/algorithms/GetPlaintext.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { bi, byteArrayFromHex, byteArrayToHex } from "../../util.js"

const params = new SecurityParameters("LEVEL_0")

describe("AES", () => {
	it("should encrypt", async () => {
		let K = ByteArray.of(new Uint8Array(16))
		let IV = ByteArray.of(new Uint8Array(12))
		let M = ByteArray.of(new Uint8Array(16))

		assert.equal(byteArrayToHex((await AESEncrypt(K, IV, M)).toByteArray()), "0388DACE60B6A392F328C2B971B2FE78AB6E47D42CEC13BDF53A67B21257BDDF")
	})

	it("should decrypt", async () => {
		let K = ByteArray.of(new Uint8Array(16))
		let IV = ByteArray.of(new Uint8Array(12))
		let C = byteArrayFromHex("0388DACE60B6A392F328C2B971B2FE78AB6E47D42CEC13BDF53A67B21257BDDF")

		assert.deepEqual((await AESDecrypt(K, IV, C)).toByteArray(), new Uint8Array(16))
	})
})

describe("Hybrid encryption", () => {
	it("GetPlaintext(sk, GenCiphertext(pk, m)) = m", async () => {
		let M = ByteArray.of(new Uint8Array(16))
		let sk = bi(10)
		let pk = params.g_hat.modPow(sk, params.p_hat)
		let encryption = await GenCiphertext(pk, M, params)
		let decryption = await GetPlaintext(sk, encryption, params)

		assert.deepEqual(decryption.toByteArray(), M.toByteArray())
	})
})

