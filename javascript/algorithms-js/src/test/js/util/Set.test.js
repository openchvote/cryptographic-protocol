/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { Alphabet } from "#Alphabet.js"
import { BigInteger, FIVE, FOUR, ONE, SIX, THREE, TWO, ZERO } from "#util/math/BigInteger.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { Pair, Triple } from "#util/Tuples.js"
import { Set } from "#util/Set.js"
import { Vector } from "#util/sequence/Vector.js"
import { ZZ } from "#util/algebra/ZZ.js"


describe("Set.NN", () => {
	it("should provide NN", () => {
		assert.isTrue(Set.NN.contains(ZERO))
		assert.isTrue(Set.NN.contains(ONE))
		assert.isTrue(Set.NN.contains(TWO))
		assert.isFalse(Set.NN.contains(new BigInteger(-1)))
		assert.isFalse(Set.NN.contains(3))
		assert.isFalse(Set.NN.contains("4"))
		assert.isFalse(Set.NN.contains())
	})
	it("should provide NN+", () => {
		assert.isTrue(Set.NN_plus.contains(ONE))
		assert.isTrue(Set.NN_plus.contains(TWO))
		assert.isFalse(Set.NN_plus.contains(ZERO))
		assert.isFalse(Set.NN_plus.contains(new BigInteger(-1)))
		assert.isFalse(Set.NN_plus.contains(3))
		assert.isFalse(Set.NN_plus.contains("4"))
		assert.isFalse(Set.NN_plus.contains())
	})
	// TODO B, B_star
	it("should provide Pair", () => {
		const set = Set.Pair(Set.NN, Set.NN)
		assert.isTrue(set.contains(new Pair(ONE, TWO)))
		assert.isTrue(set.contains(new Pair(ZERO, ZERO)))
		assert.isFalse(set.contains(new Pair(1, 2)))
		assert.isFalse(set.contains(new Triple(ONE, TWO, THREE)))
		assert.throws(() => {Set.Pair(Set.NN)})
	})
	it("should provide Triple", () => {
		const set = Set.Triple(Set.NN, Set.NN_plus, new ZZ(THREE))
		assert.isTrue(set.contains(new Triple(ONE, TWO, TWO)))
		assert.isTrue(set.contains(new Triple(ZERO, ONE, ZERO)))
		assert.isFalse(set.contains(new Triple(ZERO, ONE, FOUR)))
		assert.isFalse(set.contains(new Triple(ZERO, ZERO, ZERO)))
		assert.isFalse(set.contains(new Triple(1, 0, THREE)))
		assert.isFalse(set.contains(new Triple(false, ONE, ONE)))
		assert.throws(() => {Set.Triple(Set.NN)})
		assert.throws(() => {Set.Triple(Set.NN, Set.NN)})
		assert.throws(() => {Set.Triple(null, Set.NN, Set.NN)})
	})
	it("should provide Vector", () => {
		const set = Set.Vector(new ZZ(FIVE))
		assert.isTrue(set.contains(new Vector([ONE, TWO, THREE])))
		assert.isTrue(set.contains(new Vector([ZERO, ONE, TWO, THREE, FOUR])))
		assert.isFalse(set.contains(new Vector([ZERO, ONE, SIX])))
		assert.isFalse(set.contains(new Vector([ZERO, SIX, ONE])))
		assert.isFalse(set.contains(new Vector([1, 0, THREE])))
		assert.isFalse(set.contains(new Vector([null, ONE, ONE])))
		assert.throws(() => {Set.Vector("")})
		assert.throws(() => {Set.Vector()})
		assert.throws(() => {Set.Vector(null)})
	})
	it("should provide Vector with length", () => {
		const set = Set.Vector(new ZZ(FIVE), 3)
		assert.isTrue(set.contains(new Vector([ONE, TWO, THREE])))
		assert.isTrue(set.contains(new Vector([ZERO, ONE, TWO])))
		assert.isFalse(set.contains(new Vector([ZERO, ONE, SIX])))
		assert.isFalse(set.contains(new Vector([ZERO, SIX, ONE, TWO])))
		assert.isFalse(set.contains(new Vector([ZERO, SIX])))
		assert.isFalse(set.contains(new Vector([1, 0, THREE])))
		assert.isFalse(set.contains(new Vector([ONE, null, ONE])))
		assert.throws(() => {Set.Vector(Set.NN, "2")})
		assert.throws(() => {Set.Vector(Set.NN, -3)})
		assert.throws(() => {Set.Vector(Set.NN, true)})
	})
	it("should provide Matrix", () => {
		const set = Set.Matrix(new ZZ(FIVE))
		assert.isTrue(set.contains(new Matrix(3, 2, [[ONE, TWO], [TWO, THREE], [THREE, FOUR]])))
		assert.isFalse(set.contains(new Matrix(3, 2, [[ONE, TWO], [TWO, THREE], [SIX, FOUR]])))
		assert.isTrue(set.contains(new Matrix(3, 3, [[ONE, TWO, ONE], [TWO, THREE, ONE], [ONE, FOUR, ONE]])))
		assert.isTrue(set.contains(new Matrix(2, 2, [[TWO, THREE], [TWO, FOUR]])))
		assert.isTrue(set.contains(new Matrix(3, 1, [[ONE], [TWO], [THREE]])))
		assert.throws(() => {Set.Matrix(Set.NN, -3, 2)})
		assert.throws(() => {Set.Matrix(Set.NN, true)})
	})
	it("should provide Matrix width height and width", () => {
		const set = Set.Matrix(new ZZ(FIVE), 3, 2)
		assert.isTrue(set.contains(new Matrix(3, 2, [[ONE, TWO], [TWO, THREE], [THREE, FOUR]])))
		assert.isFalse(set.contains(new Matrix(3, 2, [[ONE, TWO], [TWO, THREE], [SIX, FOUR]])))
		assert.isFalse(set.contains(new Matrix(3, 3, [[ONE, TWO, ONE], [TWO, THREE, ONE], [SIX, FOUR, ONE]])))
		assert.isFalse(set.contains(new Matrix(2, 2, [[TWO, THREE], [SIX, FOUR]])))
		assert.isFalse(set.contains(new Matrix(3, 1, [[ONE], [TWO], [THREE]])))

		const set2 = Set.Matrix(Set.NN, 2)
		assert.isTrue(set2.contains(new Matrix(2, 2, [[TWO, THREE], [TWO, FOUR]])))
		assert.isTrue(set2.contains(new Matrix(2, 3, [[ONE, TWO, THREE], [ONE, TWO, FOUR]])))
		assert.isFalse(set2.contains(new Matrix(1, 3, [[ONE, TWO, THREE]])))
		assert.isFalse(set.contains(new Matrix(3, 1, [[ONE], [TWO], [THREE]])))

		assert.throws(() => {Set.Matrix(Set.NN, -3, 2)})
		assert.throws(() => {Set.Matrix(Set.NN, true)})
	})
	it("should provide String", () => {
		const set = Set.String(Alphabet.BASE_16)
		assert.isTrue(set.contains(""))
		assert.isTrue(set.contains("023FD"))
		assert.isTrue(set.contains("DDAAF123FD"))
		assert.isFalse(set.contains("DEX12"))
		assert.isFalse(set.contains(null))
		assert.isFalse(set.contains())
		assert.throws(() => {Set.String(null)})
		assert.throws(() => {Set.String("asd")})
		assert.throws(() => {Set.Matrix(123)})
	})
	it("should provide String with length", () => {
		const set = Set.String(Alphabet.BASE_16, 5)
		assert.isTrue(set.contains("023FD"))
		assert.isTrue(set.contains("DDAAF"))
		assert.isFalse(set.contains(""))
		assert.isFalse(set.contains("DE"))
		assert.isFalse(set.contains("DE23452"))
		assert.isFalse(set.contains("DEX12"))
		assert.isFalse(set.contains(null))
		assert.isFalse(set.contains())
		assert.throws(() => {Set.String(Alphabet.BASE_16, -2)})
		assert.throws(() => {Set.String(Alphabet.BASE_16, "2")})
		assert.throws(() => {Set.String(Alphabet.BASE_16, new BigInteger(3))})
	})
	it("should provide String with minlength and maxLength", () => {
		const set = Set.String(Alphabet.BASE_16, 3, 5)
		assert.isTrue(set.contains("023"))
		assert.isTrue(set.contains("023F"))
		assert.isTrue(set.contains("023FD"))
		assert.isFalse(set.contains(""))
		assert.isFalse(set.contains("DE"))
		assert.isFalse(set.contains("DE23452"))
		assert.isFalse(set.contains("DEX12"))
		assert.isFalse(set.contains(null))
		assert.isFalse(set.contains())
		assert.throws(() => {Set.String(Alphabet.BASE_16, -2, 3)})
		assert.throws(() => {Set.String(Alphabet.BASE_16, "2", "5")})
		assert.throws(() => {Set.String(Alphabet.BASE_16, new BigInteger(3), new BigInteger(5))})
	})
	it("should provide UTF8", () => {
		assert.isTrue(Set.UTF8.contains(ByteArray.of(new Uint8Array([0x61, 0x62, 0x63]))))
		assert.isTrue(Set.UTF8.contains(ByteArray.of(new Uint8Array([0x68, 0x65, 0x6C, 0x6C, 0x6F]))))
		assert.isTrue(Set.UTF8.contains(ByteArray.of(new Uint8Array([0x77, 0x6F, 0x72, 0x6C, 0x64, 0x21]))))
		assert.isFalse(Set.UTF8.contains(ByteArray.of(new Uint8Array([61, 62, 5555]))))
		assert.isFalse(Set.UTF8.contains(undefined))
	})
	it("should provide orNull", () => {
		const set = Set.Vector(new ZZ(FIVE).orNull())
		assert.isTrue(set.contains(new Vector([ONE, TWO, THREE])))
		assert.isTrue(set.contains(new Vector([null, ONE, TWO, THREE])))
		assert.isTrue(set.contains(new Vector([ONE, null, TWO, THREE])))
		assert.isTrue(set.contains(new Vector([ONE, TWO, null])))
		assert.isFalse(set.contains(new Vector([ONE, 0, THREE])))
		assert.isFalse(set.contains(new Vector([undefined, ONE, ONE])))
	})
})

