/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { EIGHT, FIVE, FOUR, NINE, ONE, SEVEN, THREE, TWO, ZERO } from "#util/math/BigInteger.js"
import { Vector } from "#util/sequence/Vector.js"
import { ZZPlus } from "#util/algebra/ZZPlus.js"
import { bi } from "../../util.js"

describe("ZZPlus", () => {
	it("should create", () => {
		const zzp = new ZZPlus(bi(23))
		assert.isTrue(zzp.contains(ONE))
		assert.isTrue(zzp.contains(TWO))
		assert.throws(() => new ZZPlus(23))
		assert.throws(() => new ZZPlus())
		assert.throws(() => new ZZPlus(null))
		assert.throws(() => new ZZPlus("23"))
	})
	it("should provide contains", () => {
		const zzp = new ZZPlus(bi(23))
		for (let i = 1; i <= 11; i++) assert.isTrue(zzp.contains(bi(i)))
		assert.isFalse(zzp.contains(ZERO))
		assert.isFalse(zzp.contains(bi(12)))
		assert.isFalse(zzp.contains(1))
		assert.isFalse(zzp.contains("1"))
		assert.isFalse(zzp.contains(null))
	})
	it("should multiply", () => {
		const zzp = new ZZPlus(bi(23))
		assert.isTrue(zzp.multiply(ONE, ONE).equals(ONE))
		assert.isTrue(zzp.multiply(ONE, TWO).equals(TWO))
		assert.isTrue(zzp.multiply(TWO, TWO).equals(FOUR))
		assert.isTrue(zzp.multiply(TWO, bi(9)).equals(FIVE))
	})
	it("should prod", () => {
		const zzp = new ZZPlus(bi(23))
		assert.isTrue(zzp.prod(new Vector([ONE, ONE, TWO])).equals(TWO))
		assert.isTrue(zzp.prod(new Vector([TWO, THREE, FOUR])).equals(ONE))
		assert.isTrue(zzp.prod(new Vector([TWO, THREE, FIVE])).equals(SEVEN))
		assert.isTrue(zzp.prod(new Vector([THREE, THREE, FIVE])).equals(ONE))
	})
	it("should pow", () => {
		const zzp = new ZZPlus(bi(23))
		assert.isTrue(zzp.pow(ONE, ZERO).equals(ONE))
		assert.isTrue(zzp.pow(ONE, ONE).equals(ONE))
		assert.isTrue(zzp.pow(ONE, FOUR).equals(ONE))
		assert.isTrue(zzp.pow(TWO, THREE).equals(EIGHT))
		assert.isTrue(zzp.pow(THREE, THREE).equals(FOUR))
		assert.isTrue(zzp.pow(bi(10), THREE).equals(bi(11)))
		assert.isTrue(zzp.pow(bi(10), ZERO).equals(ONE))
	})
	it("should invert", () => {
		const zzp = new ZZPlus(bi(23))
		assert.isTrue(zzp.invert(ONE).equals(ONE))
		assert.isTrue(zzp.invert(TWO).equals(bi(11)))
		assert.isTrue(zzp.invert(bi(11)).equals(TWO))
	})
	it("should divide", () => {
		const zzp = new ZZPlus(bi(23))
		assert.isTrue(zzp.divide(EIGHT, TWO).equals(FOUR))
		assert.isTrue(zzp.divide(NINE, NINE).equals(ONE))
		assert.isTrue(zzp.divide(ONE, TWO).equals(bi(11)))
		assert.isTrue(zzp.divide(ONE, THREE).equals(EIGHT))
	})
})
