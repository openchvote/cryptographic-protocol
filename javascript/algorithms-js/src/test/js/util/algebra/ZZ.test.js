/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { EIGHT, FOUR, NINE, ONE, SEVEN, THREE, TWO, ZERO } from "#util/math/BigInteger.js"
import { Vector } from "#util/sequence/Vector.js"
import { ZZ } from "#util/algebra/ZZ.js"
import { bi } from "../../util.js"

describe("ZZ", () => {
	it("should create", () => {
		const zz = new ZZ(bi(17))
		assert.isTrue(zz.contains(ZERO))
		assert.isTrue(zz.contains(ONE))
		assert.isTrue(zz.contains(TWO))
		assert.throws(() => new ZZ(17))
		assert.throws(() => new ZZ())
		assert.throws(() => new ZZ(null))
		assert.throws(() => new ZZ("17"))
	})
	it("should provide contains", () => {
		const zz = new ZZ(bi(17))
		for (let i = 0; i < 17; i++) assert.isTrue(zz.contains(bi(i)))
		assert.isFalse(zz.contains(bi(17)))
		assert.isFalse(zz.contains(bi(18)))
		assert.isFalse(zz.contains(1))
		assert.isFalse(zz.contains("1"))
		assert.isFalse(zz.contains(null))
	})
	it("should add", () => {
		const zz = new ZZ(bi(17))
		assert.isTrue(zz.add(ZERO, TWO).equals(TWO))
		assert.isTrue(zz.add(ONE, TWO).equals(THREE))
		assert.isTrue(zz.add(ZERO, ZERO).equals(ZERO))
		assert.isTrue(zz.add(bi(10), bi(7)).equals(ZERO))
		assert.isTrue(zz.add(bi(10), bi(8)).equals(ONE))
	})
	it("should sum", () => {
		const zz = new ZZ(bi(17))
		assert.isTrue(zz.sum(new Vector([ZERO, ONE, TWO])).equals(THREE))
		assert.isTrue(zz.sum(new Vector([ZERO, ZERO])).equals(ZERO))
		assert.isTrue(zz.sum(new Vector([])).equals(ZERO))
		assert.isTrue(zz.sum(new Vector([bi(12), bi(7), bi(2)])).equals(FOUR))
	})
	it("should minus", () => {
		const zz = new ZZ(bi(17))
		assert.isTrue(zz.minus(ZERO).equals(ZERO))
		assert.isTrue(zz.minus(ONE).equals(bi(16)))
		assert.isTrue(zz.minus(TWO).equals(bi(15)))
		assert.isTrue(zz.minus(bi(16)).equals(ONE))
	})
	it("should subtract", () => {
		const zz = new ZZ(bi(17))
		assert.isTrue(zz.subtract(THREE, ZERO).equals(THREE))
		assert.isTrue(zz.subtract(ZERO, THREE).equals(bi(14)))
	})
	it("should multiply", () => {
		const zz = new ZZ(bi(17))
		assert.isTrue(zz.multiply(ZERO, ZERO).equals(ZERO))
		assert.isTrue(zz.multiply(ZERO, ONE).equals(ZERO))
		assert.isTrue(zz.multiply(ONE, TWO).equals(TWO))
		assert.isTrue(zz.multiply(TWO, TWO).equals(FOUR))
		assert.isTrue(zz.multiply(TWO, bi(9)).equals(ONE))
	})
	it("should prod", () => {
		const zz = new ZZ(bi(17))
		assert.isTrue(zz.prod(new Vector([ZERO, ONE, TWO])).equals(ZERO))
		assert.isTrue(zz.prod(new Vector([ONE, ONE, TWO])).equals(TWO))
		assert.isTrue(zz.prod(new Vector([TWO, THREE, FOUR])).equals(SEVEN))
	})
	it("should pow", () => {
		const zz = new ZZ(bi(17))
		assert.isTrue(zz.pow(ZERO, TWO).equals(ZERO))
		assert.isTrue(zz.pow(ONE, FOUR).equals(ONE))
		assert.isTrue(zz.pow(TWO, THREE).equals(EIGHT))
		assert.isTrue(zz.pow(THREE, THREE).equals(bi(10)))
	})
	it("should invert", () => {
		const zz = new ZZ(bi(17))
		assert.isTrue(zz.invert(ONE).equals(ONE))
		assert.isTrue(zz.invert(TWO).equals(NINE))
	})
	it("should divide", () => {
		const zz = new ZZ(bi(17))
		assert.isTrue(zz.divide(EIGHT, TWO).equals(FOUR))
		assert.isTrue(zz.divide(NINE, NINE).equals(ONE))
		assert.isTrue(zz.divide(NINE, TWO).equals(bi(13)))
	})
})
