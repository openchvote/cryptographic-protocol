/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { FIVE, FOUR, ONE, SEVEN, THREE, TWO, ZERO } from "#util/math/BigInteger.js"
import { GG } from "#util/algebra/GG.js"
import { Vector } from "#util/sequence/Vector.js"
import { bi } from "../../util.js"

describe("GG", () => {
	it("should create", () => {
		const gg = new GG(bi(29), bi(7))
		assert.isTrue(gg.contains(ONE))
		assert.isTrue(gg.contains(SEVEN))
		assert.throws(() => new GG(29, 7))
		assert.throws(() => new GG())
		assert.throws(() => new GG(null, null))
		assert.throws(() => new GG("29", "7"))
	})
	it("should provide contains", () => {
		const gg = new GG(bi(29), bi(7))
		assert.isTrue(gg.contains(ONE))
		assert.isTrue(gg.contains(SEVEN))
		assert.isTrue(gg.contains(bi(16)))
		assert.isTrue(gg.contains(bi(20)))
		assert.isTrue(gg.contains(bi(23)))
		assert.isTrue(gg.contains(bi(24)))
		assert.isTrue(gg.contains(bi(25)))
		assert.isFalse(gg.contains(ZERO))
		assert.isFalse(gg.contains(bi(12)))
		assert.isFalse(gg.contains(1))
		assert.isFalse(gg.contains("1"))
		assert.isFalse(gg.contains(null))
	})
	it("should multiply", () => {
		const gg = new GG(bi(29), bi(7))
		assert.isTrue(gg.multiply(ONE, ONE).equals(ONE))
		assert.isTrue(gg.multiply(SEVEN, SEVEN).equals(bi(20)))
		assert.isTrue(gg.multiply(bi(16), bi(16)).equals(bi(24)))
	})
	it("should prod", () => {
		const gg = new GG(bi(29), bi(7))
		assert.isTrue(gg.prod(new Vector([ONE, ONE, SEVEN])).equals(SEVEN))
		assert.isTrue(gg.prod(new Vector([SEVEN, SEVEN, ONE])).equals(bi(20)))
		assert.isTrue(gg.prod(new Vector([SEVEN, bi(23), bi(16)])).equals(bi(24)))
	})
	it("should pow", () => {
		const gg = new GG(bi(29), bi(7))
		assert.isTrue(gg.pow(ONE, ZERO).equals(ONE))
		assert.isTrue(gg.pow(ONE, FOUR).equals(ONE))
		assert.isTrue(gg.pow(SEVEN, ZERO).equals(ONE))
		assert.isTrue(gg.pow(SEVEN, TWO).equals(bi(20)))
		assert.isTrue(gg.pow(SEVEN, THREE).equals(bi(24)))
		assert.isTrue(gg.pow(bi(16), FIVE).equals(bi(23)))
	})
	it("should invert", () => {
		const gg = new GG(bi(29), bi(7))
		assert.isTrue(gg.invert(ONE).equals(ONE))
		assert.isTrue(gg.invert(SEVEN).equals(bi(25)))
		assert.isTrue(gg.invert(bi(16)).equals(bi(20)))
	})
	it("should divide", () => {
		const gg = new GG(bi(29), bi(7))
		assert.isTrue(gg.divide(ONE, ONE).equals(ONE))
		assert.isTrue(gg.divide(ONE, SEVEN).equals(bi(25)))
		assert.isTrue(gg.divide(bi(16), bi(20)).equals(bi(24)))
		assert.isTrue(gg.divide(SEVEN, SEVEN).equals(ONE))
	})
})
