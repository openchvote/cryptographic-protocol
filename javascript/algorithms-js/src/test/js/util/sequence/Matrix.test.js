/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { Matrix, ZeroIndexedColMatrix } from "#util/sequence/Matrix.js"
import { Vector, ZeroIndexedVector } from "#util/sequence/Vector.js"

describe("Matrix", () => {
	it("should return the correct cell/row/column", () => {
		let X = new Matrix(3, 3, [
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9]])
		assert.equal(X.get(3, 1), 7)
		assert.deepEqual(X.getRow(1), new Vector([1, 2, 3]))
		assert.deepEqual(X.getRow(2), new Vector([4, 5, 6]))
		assert.deepEqual(X.getRow(3), new Vector([7, 8, 9]))
		assert.deepEqual(X.getCol(1), new Vector([1, 4, 7]))
		assert.deepEqual(X.getCol(2), new Vector([2, 5, 8]))
		assert.deepEqual(X.getCol(3), new Vector([3, 6, 9]))
		assert.throws(() => X.getRow(0))
		assert.throws(() => X.getRow(4))
		assert.throws(() => X.getCol(0))
		assert.throws(() => X.getCol(4))
	})
	it("should throw if rows dont have the same length", () => {
		assert.throws(() => new Matrix(2, 2, [
			[1, 2],
			[3, 4, 5]
		]))
	})
	it("should be iterable", () => {
		let X = new Matrix(3, 3, [
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9]])
		let x = 0
		for (let val of X) {
			assert.equal(val, ++x)
		}
		assert.equal(x, 9)
	})
	it("should provide every", () => {
		let X = new Matrix(3, 3, [
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9]])
		assert.isTrue(X.every(x => x < 10))
		assert.isTrue(X.every((x, i, j) => x === (i-1) * 3 + j))
		assert.isFalse(X.every(x => x < 9))
	})
	it("should transpose", () => {
		let M1 = new Matrix(3, 5, [
			[1, 2, 3, 4, 5],
			[6, 7, 8, 9, 10],
			[11, 12, 13, 14, 15]
		]).transpose()

		assert.deepEqual(M1, new Matrix(5, 3, [
			[1, 6, 11],
			[2, 7, 12],
			[3, 8, 13],
			[4, 9, 14],
			[5, 10, 15]]))

		let M2 = new Matrix.Builder(2, 0).build()
		let M3 = new Matrix.Builder(0, 2).build()
		assert.deepEqual(M2.transpose(), M3)
		assert.deepEqual(M3.transpose(), M2)
	})
	it("should be immutable", () => {
		let X = new Matrix(3, 3, [
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9]])
		assert.throws(() => X._height = 7)
		assert.throws(() => X._width = 7)
		assert.throws(() => X._values[1][1] = 7)
		assert.throws(() => X._values = [])
	})
})

describe("Matrix.Builder", () => {
	it("should throw when accessing 0th row or column", () => {
		assert.throws(() => new Matrix.Builder(3, 4).build().get(1, 0))
		assert.throws(() => new Matrix.Builder(2, 2).build().get(0, 2))
	})
	it("should throw when accessing non-existing row or column", () => {
		assert.throws(() => new Matrix.Builder(3, 4).build().get(4, 4))
		assert.throws(() => new Matrix.Builder(3, 4).build().get(3, 5))
	})
	it("should build from single values", () => {
		let X = new Matrix.Builder(2, 3)
			.set(1, 1, "a")
			.set(1, 2, "b")
			.set(1, 3, "c")
			.set(2, 1, "d")
			.set(2, 2, "e")
			.set(2, 3, "f")
			.build()
		assert.deepEqual(X.size, [2, 3])
		assert.equal(X.get(1, 1), "a")
		assert.equal(X.get(1, 2), "b")
		assert.equal(X.get(1, 3), "c")
		assert.equal(X.get(2, 1), "d")
		assert.equal(X.get(2, 2), "e")
		assert.equal(X.get(2, 3), "f")
	})
	it("should build from 1D-array", () => {
		let X = new Matrix.Builder(3, 4)
			.setValues(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"])
			.build()
		assert.deepEqual(X.size, [3, 4])
		assert.equal(X.get(1, 1), "a")
		assert.equal(X.get(1, 2), "b")
		assert.equal(X.get(1, 4), "d")
		assert.equal(X.get(2, 1), "e")
		assert.equal(X.get(3, 4), "l")
	})
	it("should build from vector of vectors", () => {
		let X = new Matrix.Builder(3, 4).setValues(new Vector([
			new Vector(["a", "b", "c", "d"]),
			new Vector(["e", "f", "g", "h"]),
			new Vector(["i", "j", "k", "l"])])).build()
		assert.deepEqual(X.size, [3, 4])
		assert.equal(X.get(1, 1), "a")
		assert.equal(X.get(1, 2), "b")
		assert.equal(X.get(1, 4), "d")
		assert.equal(X.get(2, 1), "e")
		assert.equal(X.get(3, 4), "l")
	})
})

describe("ZeroIndexedColMatrix", () => {
	it("should return the correct cell/row/column", () => {
		let X = new ZeroIndexedColMatrix(3, 3, [
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9]])
		assert.equal(X.get(3, 1), 8)
		assert.deepEqual(X.getRow(1), new Vector([1, 2, 3]))
		assert.deepEqual(X.getRow(2), new Vector([4, 5, 6]))
		assert.deepEqual(X.getRow(3), new Vector([7, 8, 9]))
		assert.deepEqual(X.getCol(0), new Vector([1, 4, 7]))
		assert.deepEqual(X.getCol(1), new Vector([2, 5, 8]))
		assert.deepEqual(X.getCol(2), new Vector([3, 6, 9]))
		assert.throws(() => X.getRow(0))
		assert.throws(() => X.getRow(4))
		assert.throws(() => X.getCol(-1))
		assert.throws(() => X.getCol(3))
	})
	it("should be iterable", () => {
		let X = new ZeroIndexedColMatrix(3, 3, [
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9]])
		let x = 0
		for (let val of X) {
			assert.equal(val, ++x)
		}
		assert.equal(x, 9)
	})
	it("should provide every", () => {
		let X = new ZeroIndexedColMatrix(3, 3, [
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9]])
		assert.isTrue(X.every(x => x < 10))
		assert.isTrue(X.every((x, i, j) => x === (i-1) * 3 + (j+1)))
		assert.isFalse(X.every(x => x < 9))
	})
	it("should not transpose", () => {
		let M = new ZeroIndexedColMatrix(3, 5, [
			[1, 2, 3, 4, 5],
			[6, 7, 8, 9, 10],
			[11, 12, 13, 14, 15]
		])
		assert.throws(() => M.transpose())
	})
	it("should be immutable", () => {
		let X = new ZeroIndexedColMatrix(3, 3, [
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9]])
		assert.throws(() => X._height = 7)
		assert.throws(() => X._width = 7)
		assert.throws(() => X._values[1][1] = 7)
		assert.throws(() => X._values = [])
	})
})

describe("ZeroIndexedColMatrix.Builder", () => {
	it("should throw when accessing 0th row but not when accessing 0th column", () => {
		assert.throws(() => new ZeroIndexedColMatrix.Builder(2, 2).build().get(0, 2))
		new ZeroIndexedColMatrix.Builder(3, 4).build().get(1, 0)
	})
	it("should throw when accessing non-existing row or column", () => {
		assert.throws(() => new ZeroIndexedColMatrix.Builder(3, 4).build().get(4, 4))
		assert.throws(() => new ZeroIndexedColMatrix.Builder(3, 4).build().get(3, 5))
	})
	it("should build from single values", () => {
		let X = new ZeroIndexedColMatrix.Builder(2, 3)
			.set(1, 0, "a")
			.set(1, 1, "b")
			.set(1, 2, "c")
			.set(2, 0, "d")
			.set(2, 1, "e")
			.set(2, 2, "f")
			.build()
		assert.deepEqual(X.size, [2, 3])
		assert.equal(X.get(1, 0), "a")
		assert.equal(X.get(1, 1), "b")
		assert.equal(X.get(1, 2), "c")
		assert.equal(X.get(2, 0), "d")
		assert.equal(X.get(2, 1), "e")
		assert.equal(X.get(2, 2), "f")
	})
	it("should build from 1D-array", () => {
		let X = new ZeroIndexedColMatrix.Builder(3, 4)
			.setValues(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"])
			.build()
		assert.deepEqual(X.size, [3, 4])
		assert.equal(X.get(1, 0), "a")
		assert.equal(X.get(1, 1), "b")
		assert.equal(X.get(1, 3), "d")
		assert.equal(X.get(2, 0), "e")
		assert.equal(X.get(3, 3), "l")
	})
	it("should build from vector of zero-indexed vectors", () => {
		let X = new ZeroIndexedColMatrix.Builder(3, 4).setValues(new Vector([
			new ZeroIndexedVector(["a", "b", "c", "d"]),
			new ZeroIndexedVector(["e", "f", "g", "h"]),
			new ZeroIndexedVector(["i", "j", "k", "l"])])).build()
		assert.deepEqual(X.size, [3, 4])
		assert.equal(X.get(1, 0), "a")
		assert.equal(X.get(1, 1), "b")
		assert.equal(X.get(1, 3), "d")
		assert.equal(X.get(2, 0), "e")
		assert.equal(X.get(3, 3), "l")
	})
})
