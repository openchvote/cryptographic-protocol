/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { Vector, ZeroIndexedVector } from "#util/sequence/Vector.js"

describe("Vector", () => {
	it("should throw error when accessing 0th element", () => {
		assert.throws(() => new Vector(["a", "b", "c"]).get(0))
	})
	it("should throw error when accessing out of bound element", () => {
		assert.throws(() => new Vector(["a", "b", "c"]).get(4))
	})
	it("should provide correct size", () => {
		assert.equal(new Vector(["a", "b", "c"]).length, 3)
	})
	it("should give the first element on index 1", () => {
		assert.equal(new Vector(["a", "b", "c"]).get(1), "a")
	})
	it("should return the 3rd item", () => {
		assert.equal(new Vector(["a", "b", "c"]).get(3), "c")
	})
	it("should throw an error when 'getting' an invalid item", () => {
		assert.throws(() => (new Vector(["a", "b", "c"]).get(1.2)))
		assert.throws(() => (new Vector(["a", "b", "c"]).get("first")))
		assert.throws(() => (new Vector(["a", "b", "c"]).get(0)))
		assert.throws(() => (new Vector(["a", "b", "c"]).get(4)))
		assert.throws(() => (new Vector(["a", "b", "c"]).get()))
	})
	it("should return the last item", () => {
		let vec = new Vector(["a", "b", "c"])
		assert.equal(vec.get(vec.length), "c")
	})
	it("should be iterable", () => {
		let vec = new Vector(["a", "b", "c"])
		let last
		for (let x of vec) {
			last = x
		}
		assert.equal(last, "c")
	})
	it("should be equal if items are equal", () => {
		let vec = new Vector(["a", "b", "c"])
		assert.isTrue(vec.equals(new Vector(["a", "b", "c"])))
	})
	it("should not equal if items are not equal", () => {
		let vec = new Vector(["a", "b", "x"])
		assert.isFalse(vec.equals(null))
		assert.isFalse(vec.equals(undefined))
		assert.isFalse(vec.equals(new Vector(["a", "b"])))
		assert.isFalse(vec.equals(new Vector([""])))
		assert.isFalse(vec.equals(new Vector(["a", "b", "c"])))
	})
	it("should multiply every value by 2 with map", () => {
		let vec = new Vector([1, 2, 3])
		let multTwo = function(x) {
			return x * 2
		}
		assert.isTrue(vec.map(multTwo).equals(new Vector([2, 4, 6])))
	})
	it("should check every value with every", () => {
		let vec = new Vector([1, 2, 3])
		let checkInt = x => typeof x === "number"
		assert.isTrue(vec.every(checkInt))
		let vec1 = new Vector([2, 3, 4])
		assert.isTrue(vec.every((v, i) => v < vec1.get(i)))
		vec1 = new Vector([2, 2, 4])
		assert.isFalse(vec.every((v, i) => v < vec1.get(i)))
	})
	it("should have reduce method", () => {
		let vec = new Vector([1, 2, 3])
		let sum = vec.reduce((p, n) => p + n, 0)
		assert.equal(sum, 6)
	})
	it("should have any method", () => {
		let vec = new Vector([1, 2, 3])
		assert.isTrue(vec.any(x => x === 1))
		assert.isTrue(vec.any(x => x === 2))
		assert.isFalse(vec.any(x => x === 4))
	})
	it("should have count method", () => {
		let vec = new Vector([1, 1, 3])
		assert.equal(vec.count(x => x === 1), 2)
		assert.equal(vec.any(x => x === 2), 0)
		assert.equal(vec.any(x => x === 3), 1)
	})
	it("should have select method", () => {
		let vec = new Vector(["One", "Two", "Three", "Four"])
		assert.isTrue(vec.select(new Vector([1, 3])).equals(new Vector(["One", "Three"])))
		assert.isFalse(vec.select(new Vector([1, 3])).equals(new Vector([1, 2])))
		assert.isTrue(vec.select(new Vector([1, 2, 3, 4])).equals(vec))
		assert.throws(() => vec.select(3))
		assert.throws(() => vec.select(new Vector([0, 1, 2])))
		assert.throws(() => vec.select(new Vector([1, 2, 8])))
		assert.throws(() => vec.select(new Vector(["One", 2])))
	})
	it("should have times method", () => {
		let vec1 = new Vector([1, 2, 3])
		let vec2 = new Vector([1, 2, 3])
		let vec3 = vec1.times(vec2)
		assert.isTrue(vec3.equals(new Vector([1, 4, 9])))
	})
	it("should have replace method", () => {
		let vec1 = new Vector([1, 2, 3])
		let vec2 = vec1.replace(1, 7)
		assert.isTrue(vec1.equals(new Vector([1, 2, 3])))
		assert.isTrue(vec2.equals(new Vector([7, 2, 3])))
		assert.throws(() => vec1.replace(0, 7))
		assert.throws(() => vec1.replace(4, 7))
	})
	it("should be immutable", () => {
		let a = new Vector([1, 2, 3])
		assert.throws(() => a._values[0] = 7)
		assert.throws(() => a._values[a._values.length] = 4)
		assert.throws(() => a._values.push(4))
		let b = new Vector([4])
		assert.throws(() => a._values = b._values)
	})
})

describe("ZeroIndexedVector", () => {
	it("should get correct element", () => {
		let vec = new ZeroIndexedVector(["a", "b", "c"])
		assert.equal(vec.get(0), "a")
		assert.equal(vec.get(1), "b")
		assert.equal(vec.get(2), "c")
	})
	it("should throw error when accessing negative index", () => {
		const vec = new ZeroIndexedVector(["a", "b", "c"])
		assert.throws(() => vec.get(-1))
	})
	it("should throw error when accessing out of bound element", () => {
		const vec = new ZeroIndexedVector(["a", "b", "c"])
		assert.throws(() => vec.get(3))
	})
	it("should have replace method", () => {
		let vec1 = new ZeroIndexedVector([1, 2, 3])
		let vec2 = vec1.replace(0, 7)
		assert.isTrue(vec1.equals(new ZeroIndexedVector([1, 2, 3])))
		assert.isTrue(vec2.equals(new ZeroIndexedVector([7, 2, 3])))
		assert.throws(() => vec1.replace(-1, 7))
		assert.throws(() => vec1.replace(3, 7))
	})
	it("should check every value with every", () => {
		let vec = new ZeroIndexedVector([1, 2, 3])
		let checkInt = x => typeof x === "number"
		assert.isTrue(vec.every(checkInt))
		let vec1 = new ZeroIndexedVector([2, 3, 4])
		assert.isTrue(vec.every((v, i) => v < vec1.get(i)))
		vec1 = new ZeroIndexedVector([2, 2, 4])
		assert.isFalse(vec.every((v, i) => v < vec1.get(i)))
	})
	it("should have select method", () => {
		let vec = new ZeroIndexedVector(["Zero", "One", "Two", "Three", "Four"])
		assert.isTrue(vec.select(new Vector([1, 3])).equals(new ZeroIndexedVector(["One", "Three"])))
		assert.isFalse(vec.select(new Vector([1, 3])).equals(new ZeroIndexedVector([1, 2])))
		assert.isTrue(vec.select(new Vector([0, 1, 2, 3, 4])).equals(vec))
	})
})

describe("Vector.Builder", () => {
	it("should build a vector", () => {
		let builder = new Vector.Builder([7, 2, 3])
		builder.set(4, 4)
		builder.set(1, 1)
		builder.add(5)
		assert.throws(() => builder.set(0, 0))
		assert.isTrue(builder.build().equals(new Vector([1, 2, 3, 4, 5])))
		assert.throws(() => builder.set(3, 4))
		assert.throws(builder.build)
	})
})

describe("ZeroIndexedVector.Builder", () => {
	it("should build a zero indexed vector", () => {
		let builder = new ZeroIndexedVector.Builder([7, 2, 3])
		builder.set(3, 4)
		builder.set(0, 1)
		builder.add(5)
		assert.throws(() => builder.set(-1, -1))
		assert.isTrue(builder.build().equals(new ZeroIndexedVector([1, 2, 3, 4, 5])))
		assert.throws(() => builder.set(3, 4))
		assert.throws(builder.build)
	})
})
