/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { ByteArray } from "#util/sequence/ByteArray.js"
import { Vector } from "#util/sequence/Vector.js"


describe("ByteArray", () => {
	it("should work", () => {
		const ba = ByteArray.of(new Uint8Array([1, 2, 3, 4, 5]))
		assert.equal(ba.length, 5)
		for (let i = 0; i < 5; i++) assert.equal(ba.getByte(i), i + 1)
	})
	it("should skip", () => {
		const ba1 = ByteArray.of(new Uint8Array([1, 2, 3, 4, 5]))
		let k = 2
		const ba2 = ba1.skip(k)
		assert.equal(ba2.length, ba1.length - k)
		for (let i = 0; i < ba1.length - k; i++) assert.equal(ba2.getByte(i), i + 1 + k)
	})
	it("should truncate", () => {
		const ba1 = ByteArray.of(new Uint8Array([1, 2, 3, 4, 5]))
		let k = 3
		const ba2 = ba1.truncate(k)
		assert.equal(ba2.length, k)
		for (let i = 0; i < k; i++) assert.equal(ba2.getByte(i), i + 1)
	})
	it("should concatenate", () => {
		const ba1 = ByteArray.of(new Uint8Array([1, 2, 3, 4, 5]))
		const ba2 = ByteArray.of(new Uint8Array([6, 7, 8]))
		let ba = ba1.concatenate(ba2)
		assert.equal(ba.length, ba1.length + ba2.length)
		for (let i = 0; i < ba.length; i++) assert.equal(ba.getByte(i), i + 1)

		const ba3 = ByteArray.of(new Uint8Array([9, 10]))
		ba = ByteArray.concatenate(ba1, ba2, ba3)
		assert.equal(ba.length, ba1.length + ba2.length + ba3.length)
		for (let i = 0; i < ba.length; i++) assert.equal(ba.getByte(i), i + 1)
	})
	it("should setByte", () => {
		let ba = ByteArray.of(new Uint8Array([1, 2, 3]))
		ba = ba.setByte(0, 2)
		ba = ba.setByte(1, 4)
		ba = ba.setByte(2, 6)
		assert.equal(ba.length, 3)
		for (let i = 0; i < ba.length; i++) assert.equal(ba.getByte(i), (i + 1) * 2)
	})
	it("should toByteArray", () => {
		const ba1 = ByteArray.of(new Uint8Array([1, 2, 3]))
		assert.deepEqual(ba1.toByteArray(), new Uint8Array([1, 2, 3]))
		const ba2 = ByteArray.of(new Uint8Array([4, 5]))
		assert.deepEqual(ba1.concatenate(ba2).toByteArray(), new Uint8Array([1, 2, 3, 4, 5]))
		const ba3 = ByteArray.of(new Uint8Array([6]))
		assert.deepEqual(ByteArray.concatenate(ba1, ba2, ba3).toByteArray(), new Uint8Array([1, 2, 3, 4, 5, 6]))
	})
	it("should return correct bit sequences after xor operation of two byte arrays", () => {

		let ba1 = ByteArray.of(new Uint8Array([170]))
		let ba2 = ByteArray.of(new Uint8Array([85]))
		assert.equal(ba1.xor(ba2).getByte(0), 255)
	})
	it("should return correct bit sequences after xor operation of multiple byte arrays", () => {
		let threeByteArrays = new Vector([
			ByteArray.of(new Uint8Array([20, 100])),
			ByteArray.of(new Uint8Array([233, 158])),
			ByteArray.of(new Uint8Array([55, 255]))
		])

		let resultBytes = ByteArray.xor(...threeByteArrays)
		assert.isTrue(resultBytes.getByte(0) === 202 && resultBytes.getByte(1) === 5)
	})
	it("should be immutable", () => {
		const a = new Uint8Array([1, 2, 3])
		const ba = ByteArray.of(a)
		assert.throw(() => {
			ba._length = 7
		})
		a[0] = 7
		assert.equal(ba.getByte(0), 1)
		assert.throw(() => {
			ba.getByte = () => 7
		})
		assert.throw(() => {
			ba.xyz = 7
		})
	})
})


describe("ByteArray.Builder", () => {
	it("should build", () => {
		const ba = new ByteArray.Builder(3)
			.setByte(0, 1)
			.setByte(1, 2)
			.setByte(2, 3)
			.build()
		assert.equal(ba.length, 3)
		for (let i = 0; i < ba.length; i++) assert.equal(ba.getByte(i), i + 1)
		assert.deepEqual(ba.toByteArray(), new Uint8Array([1, 2, 3]))
	})
})
