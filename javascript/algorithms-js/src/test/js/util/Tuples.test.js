/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { Pair, Quadruple, Quintuple, Singleton, Triple, Tuple } from "#util/Tuples.js"

describe("Tuples", () => {
	it("should create a singleton", () => {
		let t = new Singleton("one")
		assert.equal(t.first, "one")
		assert.deepEqual(t.values, ["one"])
		assert.throws(() => t.first = "two")
	})
	it("should create a pair", () => {
		let t = new Pair("one", "two")
		assert.equal(t.first, "one")
		assert.equal(t.second, "two")
		assert.deepEqual(t.values, ["one", "two"])
		assert.throws(() => t.first = "two")
	})
	it("should create a triple", () => {
		let t = new Triple("one", "two", "three")
		assert.equal(t.first, "one")
		assert.equal(t.second, "two")
		assert.equal(t.third, "three")
		assert.deepEqual(t.values, ["one", "two", "three"])
		assert.throws(() => t.first = "two")
	})
	it("should create a quadruple", () => {
		let t = new Quadruple("one", "two", "three", "four")
		assert.equal(t.first, "one")
		assert.equal(t.second, "two")
		assert.equal(t.third, "three")
		assert.equal(t.fourth, "four")
		assert.deepEqual(t.values, ["one", "two", "three", "four"])
		assert.throws(() => t.first = "two")
	})
	it("should create a quintuple", () => {
		let t = new Quintuple("one", "two", "three", "four", "five")
		assert.equal(t.first, "one")
		assert.equal(t.second, "two")
		assert.equal(t.third, "three")
		assert.equal(t.fourth, "four")
		assert.equal(t.fifth, "five")
		assert.deepEqual(t.values, ["one", "two", "three", "four", "five"])
		assert.throws(() => t.first = "two")
	})
	it("should create a tuple", () => {
		let t = new Tuple(1, 2, 3, 4, 5)
		assert.deepEqual(t.values, [1, 2, 3, 4, 5])
		assert.equal(t.size, 5)
		let i = 0
		for (let v of t) assert.equal(v, ++i)
		assert.equal(i, t.size)
		assert.throws(() => new Tuple())
		assert.throws(() => new Tuple(1, null, 3))
		assert.throws(() => new Tuple(1, undefined, 3))
	})
})
