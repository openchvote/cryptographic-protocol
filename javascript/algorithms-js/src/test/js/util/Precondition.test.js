/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { PreconditionError } from "#util/Errors.js"
import { check } from "#util/Precondition.js"

class A {}
class B {}


describe("check", () => {
	it("should not throw for valid conditions", () => {
		check(true)
	})
	it("should throw for invalid conditions", () => {
		assert.throws(() => check(false), PreconditionError)
	})
})

describe("check.notNull", () => {
	it("should not throw for values", () => {
		check.notNull(0, 1, "", "A", false, true, new A())
	})
	it("should throw when a value is undefined", () => {
		assert.throws(() => check.notNull(0, 1, "", undefined, false, true, new A()), PreconditionError)
	})
	it("should throw when a value is null", () => {
		assert.throws(() => check.notNull(0, 1, "", "A", false, true, null), PreconditionError)
	})
})

describe("check.Type", () => {
	it("should not throw for correct type", () => {
		let a = new A()
		let b = new B()
		check.Type(A, a)
		check.Type([A, a], [B, b])
	})
	it("should throw when object is undefined", () => {
		assert.throws(() => check.Type(A, undefined), PreconditionError)
	})
	it("should throw when object is null", () => {
		assert.throws(() => check.Type(A, null), PreconditionError)
	})
	it("should throw when object is of different type", () => {
		let a = new A()
		let b = new B()
		assert.throws(() => check.Type(B, a), PreconditionError)
		assert.throws(() => check.Type([A, a], [B, a]), PreconditionError)
		assert.throws(() => check.Type([A, b], [A, a]), PreconditionError)
	})
})

describe("check.Number", () => {
	it("should not throw for correct type", () => {
		check.Number(12)
		check.Number(-12)
		check.Number(-12.345345)
		check.Number(12)
	})
	it("should throw for NaN", () => {
		assert.throws(() => check.Number(NaN), PreconditionError)
	})
	it("should throw for Infinity", () => {
		assert.throws(() => check.Number(Infinity), PreconditionError)
		assert.throws(() => check.Number(-Infinity), PreconditionError)
	})
	it("should throw for string", () => {
		assert.throws(() => check.Number("12"), PreconditionError)
	})
	it("should throw for object", () => {
		assert.throws(() => check.Number({}), PreconditionError)
	})
	it("should throw for array", () => {
		assert.throws(() => check.Number([1]), PreconditionError)
	})
	it("should throw for undefined", () => {
		assert.throws(() => check.Number(), PreconditionError)
	})
})

describe("check.String", () => {
	it("should not throw for correct type", () => {
		check.String("Literal")
		check.String("")
	})
	it("should throw for incorrect type", () => {
		assert.throws(() => check.String(), PreconditionError)
		assert.throws(() => check.String(2), PreconditionError)
		assert.throws(() => check.String(["String"]), PreconditionError)
		assert.throws(() => check.String({}), PreconditionError)
	})
})
