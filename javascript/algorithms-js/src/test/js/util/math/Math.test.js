/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { BigInteger, FIVE, FOUR, ONE, THREE, TWO } from "#util/math/BigInteger.js"
import { Vector } from "#util/sequence/Vector.js"
import {
	byteLength,
	ceilDiv,
	ceilLog,
	divides, intIsLess,
	intSum,
	intSumProd,
	powerOfTwo,
	prod
} from "#util/math/Math.js"


describe("byteLength", () => {
	it("should return the correct byte length", () => {
		assert.equal(byteLength(new BigInteger("0")), 0)
		assert.equal(byteLength(new BigInteger("1")), 1)
		assert.equal(byteLength(new BigInteger("8")), 1)
		assert.equal(byteLength(new BigInteger("ff", 16)), 1)
		assert.equal(byteLength(new BigInteger("100", 16)), 2)
		assert.equal(byteLength(new BigInteger("ffff", 16)), 2)
		assert.equal(byteLength(new BigInteger("10000", 16)), 3)
		assert.equal(byteLength(new BigInteger("ffffff", 16)), 3)
	})
})

describe("powerOfTwo", () => {
	it("should return the correct value", () => {
		assert.isTrue(powerOfTwo(0).equals(new BigInteger("1")))
		assert.isTrue(powerOfTwo(1).equals(new BigInteger("2")))
		assert.isTrue(powerOfTwo(2).equals(new BigInteger("4")))
		assert.isTrue(powerOfTwo(3).equals(new BigInteger("8")))
		assert.isTrue(powerOfTwo(4).equals(new BigInteger("16")))
	})
})

describe("ceilDiv", () => {
	it("should return the correct value", () => {
		assert.equal(ceilDiv(9, 2), 5)
		assert.equal(ceilDiv(8, 2), 4)
		assert.equal(ceilDiv(7, 2), 4)
		assert.equal(ceilDiv(6, 2), 3)
	})
})

describe("ceilLog", () => {
	it("should return the correct value", () => {
		assert.equal(ceilLog(new BigInteger(1), 2), 0)
		assert.equal(ceilLog(new BigInteger(4), 3), 2)
		assert.equal(ceilLog(new BigInteger(8), 2), 3)
		assert.equal(ceilLog(new BigInteger(16), 2), 4)
		assert.equal(ceilLog(new BigInteger(15), 2), 4)
		assert.equal(ceilLog(new BigInteger(17), 2), 5)
		assert.equal(ceilLog(new BigInteger(458), 4), 5)
		assert.equal(ceilLog(new BigInteger(711), 5), 5)
		assert.equal(ceilLog(new BigInteger(9674), 10), 4)
	})
})

describe("divides", () => {
	it("should be true for first multiples of 2 and 3", () => {
		for (let i = 0; i < 100; i++) {
			assert.isTrue(divides(2, 2 * i))
			assert.isTrue(divides(3, 3 * i))
			assert.isTrue(divides(TWO, new BigInteger(2 * i)))
			assert.isTrue(divides(THREE, new BigInteger(3 * i)))
		}
	})
	it("should be false for primes", () => {
		for (let prime of [11, 17, 2917, 5417]) {
			assert.isFalse(divides(2, prime))
			assert.isFalse(divides(3, prime))
			assert.isFalse(divides(TWO, new BigInteger(prime)))
			assert.isFalse(divides(THREE, new BigInteger(prime)))
		}
	})
})

describe("prod", () => {
	it("should return the correct value", () => {
		assert.isTrue(prod(new Vector([ONE, TWO, THREE])).equals(new BigInteger(6)))
		assert.isTrue(prod(new Vector([THREE])).equals(new BigInteger(3)))
		assert.isTrue(prod(new Vector([TWO, THREE, FOUR, FIVE])).equals(new BigInteger(120)))
	})
})

describe("intSum", () => {
	it("should return the correct value", () => {
		assert.equal(intSum(new Vector([])), 0)
		assert.equal(intSum(new Vector([1])), 1)
		assert.equal(intSum(new Vector([1, 2, 3, 4])), 10)
	})
})

describe("intSumProd", () => {
	it("should return the correct value", () => {
		assert.equal(intSumProd(new Vector([]), new Vector([])), 0)
		assert.equal(intSumProd(new Vector([1]), new Vector([1])), 1)
		assert.equal(intSumProd(new Vector([1, 2, 3]), new Vector([4, 5, 6])), 32)
	})
	it("should throw an error for illegal arguments", () => {
		assert.throw(() => intSumProd(new Vector([2, 3]), new Vector([1])))
		assert.throw(() => intSumProd(new Vector([]), new Vector([1])))
	})
})

describe("intIsLess", () => {
	it("should return the correct value", () => {
		assert.isTrue(intIsLess(new Vector([]), new Vector([])))
		assert.isFalse(intIsLess(new Vector([1]), new Vector([1])))
		assert.isTrue(intIsLess(new Vector([1]), new Vector([2])))
		assert.isTrue(intIsLess(new Vector([1, 2, 3]), new Vector([2, 3, 4])))
		assert.isFalse(intIsLess(new Vector([1, 2, 3]), new Vector([2, 2, 2])))
		assert.isFalse(intIsLess(new Vector([3]), new Vector([2])))
	})
	it("should throw an error for illegal arguments", () => {
		assert.throw(() => intIsLess(new Vector([2, 3]), new Vector([1])))
		assert.throw(() => intIsLess(new Vector([]), new Vector([1])))
	})
})
