/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import {
	BigInteger,
	EIGHT,
	FIVE,
	FOUR,
	NINE,
	ONE,
	SEVEN,
	SIX,
	THREE,
	TWO,
	ZERO
} from "#util/math/BigInteger.js"

/**
 * Helper to assert equality of two BigIntegers.
 * (<code>assert.equal</code> and <code>assert.deepEqual</code> do not work with BigIntegers)
 * @param {BigInteger} bigInt
 * @param {number|string} value
 */
function assertEqual(bigInt, value) {
	assert.isTrue(bigInt.equals(new BigInteger(value)))
}

describe("BigInteger", () => {
	it("should not modify other value", () => {
		const g = new BigInteger(4)
		let n = g.modPow(new BigInteger(2), new BigInteger(100))
		assertEqual(n, 16)
		assertEqual(g, "4")
	})
	it("should be constructable from numbers and strings", () => {
		assertEqual(new BigInteger(500), "500")
	})
	it("should be constructable hex and decimal numbers", () => {
		assertEqual(new BigInteger("01f4", 16), "500")
	})
	it("should be constructable from a BigInteger", () => {
		assertEqual(new BigInteger(new BigInteger("567")), "567")
	})
	it("should be equal to another BigInteger with same value", () => {
		assert.isTrue(new BigInteger("500").equals(new BigInteger("500")))
		assert.isFalse(new BigInteger("0").equals(new BigInteger("1")))
	})
	it("should provide bitLength", () => {
		assert.equal(new BigInteger("0").bitLength(), 0)
		assert.equal(new BigInteger("1").bitLength(), 1)
		assert.equal(new BigInteger("2").bitLength(), 2)
		assert.equal(new BigInteger("4").bitLength(), 3)
		assert.equal(new BigInteger("7").bitLength(), 3)
		assert.equal(new BigInteger("8").bitLength(), 4)
		assert.equal(new BigInteger("15").bitLength(), 4)
		assert.equal(new BigInteger("16").bitLength(), 5)
	})
	it("should provide signum", () => {
		assert.equal(new BigInteger("-1").signum(), -1)
		assert.equal(new BigInteger("0").signum(), 0)
		assert.equal(new BigInteger("1").signum(), 1)
	})
	it("should provide abs", () => {
		assertEqual(new BigInteger("-1").abs(), "1")
		assertEqual(new BigInteger("0").abs(), "0")
		assertEqual(new BigInteger("1").abs(), "1")
	})
	it("should provide add", () => {
		assertEqual(new BigInteger("4").add(new BigInteger("3")), "7")
	})
	it("should provide subtract", () => {
		assertEqual(new BigInteger("7").subtract(new BigInteger("4")), "3")
	})
	it("should provide multiply", () => {
		assertEqual(new BigInteger("3").multiply(new BigInteger("4")), "12")
	})
	it("should provide divide", () => {
		assertEqual(new BigInteger("12").divide(new BigInteger("4")), "3")
		assertEqual(new BigInteger("13").divide(new BigInteger("4")), "3")
	})
	it("should provide pow", () => {
		assertEqual(new BigInteger("2").pow(4), "16")
	})
	it("should provide mod", () => {
		assertEqual(new BigInteger("15").mod(new BigInteger("4")), "3")
	})
	it("should provide modPow", () => {
		assertEqual(new BigInteger("2").modPow(new BigInteger("4"), new BigInteger("13")), "3")
	})
	it("should provide modInverse", () => {
		assertEqual(new BigInteger("3").modInverse(new BigInteger("7")), "5")
	})
	it("should provide modDivide", () => {
		assertEqual(new BigInteger("3").modDivide(new BigInteger("4"), new BigInteger(17)), "5")
	})
	it("should provide compareTo", () => {
		assert.isTrue(new BigInteger("7").compareTo(new BigInteger("4")) > 0)
		assert.isTrue(new BigInteger("4").compareTo(new BigInteger("7")) < 0)
		assert.isTrue(new BigInteger("7").compareTo(new BigInteger("7")) === 0)
		assert.isTrue(new BigInteger("-7").compareTo(new BigInteger("4")) < 0)
		assert.isTrue(new BigInteger("7").compareTo(new BigInteger("-4")) > 0)
		assert.isTrue(new BigInteger("-7").compareTo(new BigInteger("-7")) === 0)
		assert.isTrue(new BigInteger("0").compareTo(new BigInteger("0")) === 0)
	})
	it("should provide shiftRight", () => {
		assertEqual(new BigInteger("8").shiftRight(1), "4")
		assertEqual(new BigInteger("8").shiftRight(2), "2")
		assertEqual(new BigInteger("8").shiftRight(3), "1")
		assertEqual(new BigInteger("8").shiftRight(4), "0")
	})
	it("should provide shiftLeft", () => {
		assertEqual(new BigInteger("1").shiftLeft(1), "2")
		assertEqual(new BigInteger("1").shiftLeft(2), "4")
		assertEqual(new BigInteger("1").shiftLeft(3), "8")
		assertEqual(new BigInteger("1").shiftLeft(4), "16")
	})
	it("should provide min", () => {
		assertEqual(new BigInteger("4").min(new BigInteger("2"), new BigInteger("8")), "2")
		assertEqual(new BigInteger("2").min(new BigInteger("4"), new BigInteger("8")), "2")
		assert.throws(() => new BigInteger("2").min(new BigInteger("8"), 4))
	})
	it("should provide intValue", () => {
		assert.equal(new BigInteger("1993").intValue(), 1993)
	})
	it("should provide toString", () => {
		assert.equal(new BigInteger("1993").toString(), "1993")
		assert.equal(new BigInteger("256").toString(2), "100000000")
		assert.equal(new BigInteger("256").toString(10), "256")
		assert.equal(new BigInteger("256").toString(16), "100")
	})

	it("should throw for invalid input", () => {
		assert.throws(() => new BigInteger({}))
	})
	it("should be immutable", () => {
		let a = new BigInteger("12345678901234567890123456789")
		assert.throws(() => a.__bigint = 7)
		assert.throws(() => a.__bigint[0] = 7)
		assert.throws(() => a.__bigint.s = 7)
		assert.throws(() => a.__bigint.t = 7)
		let b = new BigInteger("7")
		assert.throws(() => a.__bigint = b.__bigint)
	})
	it("shouldn't give access to the internal big integer representation", () => {
		let a = new BigInteger("12345678901234567890123456789")
		assert.isUndefined(a.__bigint)
	})
})

describe("BigInteger constants", () => {
	it("should provide 0-9", () => {
		assertEqual(ZERO, 0)
		assertEqual(ONE, 1)
		assertEqual(TWO, 2)
		assertEqual(THREE, 3)
		assertEqual(FOUR, 4)
		assertEqual(FIVE, 5)
		assertEqual(SIX, 6)
		assertEqual(SEVEN, 7)
		assertEqual(EIGHT, 8)
		assertEqual(NINE, 9)
	})
})
