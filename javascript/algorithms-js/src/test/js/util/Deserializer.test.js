/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import {
	BigIntegerDeserializer,
	BooleanDeserializer,
	ByteArrayDeserializer,
	Deserializer,
	MatrixDeserializer,
	IntegerDeserializer,
	PairDeserializer,
	SingletonDeserializer,
	StringDeserializer,
	TripleDeserializer,
	TupleDeserializer,
	VectorDeserializer
} from "#util/Deserializer.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { DeserializationError } from "#util/Errors.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { Pair, Singleton, Triple, Tuple } from "#util/Tuples.js"
import { TWO } from "#util/math/BigInteger.js"
import { Vector, ZeroIndexedVector } from "#util/sequence/Vector.js"
import { bi } from "../util.js"

describe("Deserializer", () => {
	it("correctly deserializes Booleans", () => {
		const ds = new BooleanDeserializer()
		assert.equal(ds.deserialize("1"), true)
		assert.equal(ds.deserialize("0"), false)
		assert.equal(ds.deserialize("*"), null)
		assert.throws(() => ds.deserialize("x"), DeserializationError)
	})

	it("correctly deserializes Integer", () => {
		const ds = new IntegerDeserializer()
		assert.equal(ds.deserialize("2147483647"), 2147483647)
		assert.equal(ds.deserialize("-2147483648"), "-2147483648")
		assert.equal(ds.deserialize("0"), 0)
		assert.equal(ds.deserialize("-237"), -237)
		assert.equal(ds.deserialize("2857"), 2857)
		assert.throws(() => ds.deserialize("x"), DeserializationError)
	})

	it("correctly deserializes BigInteger", () => {
		const ds = new BigIntegerDeserializer()
		assert.isTrue(ds.deserialize("10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000").equals(TWO.pow(1024)))
		assert.isTrue(ds.deserialize("-10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000").equals(TWO.pow(1024).multiply(bi(-1))))
		assert.isTrue(ds.deserialize("0").equals(bi(0)))
		assert.isTrue(ds.deserialize("-ed").equals(bi(-237)))
		assert.isTrue(ds.deserialize("B29").equals(bi(2857)))
		assert.throws(() => ds.deserialize("xxx"), DeserializationError)
	})

	it("correctly deserializes Strings", () => {
		const ds = new StringDeserializer()

		assert.equal(ds.deserialize("[]"), "")
		assert.equal(ds.deserialize("[Hello World\\]]"), "Hello World]")
		assert.equal(ds.deserialize("[Lorem ipsum \\[dolor\\] sit amet,]"), "Lorem ipsum [dolor] sit amet,")
		assert.equal(ds.deserialize("[He\\[l\\]\\[l\\]o World]"), "He[l][l]o World")
		assert.throws(() => ds.deserialize("asdf"), DeserializationError)
		assert.throws(() => ds.deserialize("[as]df]"), DeserializationError)
		assert.throws(() => ds.deserialize("asdf]"), DeserializationError)
		assert.throws(() => ds.deserialize("[asdf"), DeserializationError)

	})

	it("correctly deserializes Vector", () => {
		const ds = new VectorDeserializer(new IntegerDeserializer())

		assert.deepEqual(ds.deserialize("[1,0,[]]"), new Vector([]))
		assert.deepEqual(ds.deserialize("[1,1,[9]]"), new Vector([9]))
		assert.deepEqual(ds.deserialize("[0,0,[]]"), new ZeroIndexedVector([]))
		assert.deepEqual(ds.deserialize("[0,1,[2,3]]"), new ZeroIndexedVector([2, 3]))

		assert.deepEqual(ds.deserialize("[1,3,[0,1,0]]"), new Vector([0, 1, 0]))
		assert.deepEqual(ds.deserialize("[1,3,[728,5763,7462]]"), new Vector([728, 5763, 7462]))
		assert.deepEqual(ds.deserialize("[0,2,[728,5763,7462]]"), new ZeroIndexedVector([728, 5763, 7462]))

		assert.throws(() => ds.deserialize("[2,0,[]]"), DeserializationError)
		assert.throws(() => ds.deserialize("[1,[]]"), DeserializationError)
		assert.throws(() => ds.deserialize("[1,0,]"), DeserializationError)
		assert.throws(() => ds.deserialize("[1,0]"), DeserializationError)
		assert.throws(() => ds.deserialize("[1,0"), DeserializationError)

		const dsString = new VectorDeserializer(new StringDeserializer())
		assert.deepEqual(dsString.deserialize("[0,2,[[tri],[tra],[trallalla]]]"), new ZeroIndexedVector(["tri", "tra", "trallalla"]))
		assert.deepEqual(dsString.deserialize("[1,1,[[annoying, string]]]"), new Vector(["annoying, string"]))
		assert.deepEqual(dsString.deserialize("[0,2,[[tri],*,[trallalla]]]"), new ZeroIndexedVector(["tri", null, "trallalla"]))


	})

	it("correctly deserializes ByteArrays", () => {
		const ds = new ByteArrayDeserializer()
		assert.deepEqual(ds.deserialize("[313233]").values, ByteArray.of(Uint8Array.from([49, 50, 51])).values)
		assert.deepEqual(ds.deserialize("[]").values, ByteArray.of(Uint8Array.from([])).values)

		assert.throws(() => ds.deserialize("[XX]"), DeserializationError)
		assert.throws(() => ds.deserialize("[123]"), DeserializationError)

	})

	it("correctly deserializes Matrix", () => {
		const ds = new MatrixDeserializer(new IntegerDeserializer())
		assert.deepEqual(ds.deserialize("[1,3,1,3,[11,12,13,21,22,23,31,32,33]]"), new Matrix(3, 3, [[11, 12, 13], [21, 22, 23], [31, 32, 33]]))
		assert.deepEqual(ds.deserialize("[1,1,1,6,[11,12,13,21,22,23]]"), new Matrix(1, 6, [[11, 12, 13, 21, 22, 23]]))
		assert.deepEqual(ds.deserialize("[1,6,1,1,[11,12,13,21,22,23]]"), new Matrix(6, 1, [[11], [12], [13], [21], [22], [23]]))
		assert.deepEqual(ds.deserialize("[1,2,1,0,[]]"), new Matrix.Builder(2, 0).build())
		assert.deepEqual(ds.deserialize("[1,0,1,2,[]]"), new Matrix.Builder(0, 2).build())
		assert.throws(() => ds.deserialize("[2,1,1,6,[11,12,13,21,22,23]]"), DeserializationError)
		assert.throws(() => ds.deserialize("[1,1,2,6,[11,12,13,21,22,23]]"), DeserializationError)
		assert.throws(() => ds.deserialize("[1,1,[11,12,13,21,22,23]]"), DeserializationError)
	})

	it("correctly deserializes Tuples", () => {
		const ds = new TupleDeserializer(new StringDeserializer(), new IntegerDeserializer(), new BigIntegerDeserializer())
		assert.deepEqual(ds.deserialize("[[first],2,3]"), new Tuple("first", 2, bi(3)))
		assert.throws(() => ds.deserialize("[[test],123]"), DeserializationError)
	})

	it("correctly deserialize Singletons", () => {
		const ds = new SingletonDeserializer(new StringDeserializer())
		assert.deepEqual(ds.deserialize("[[first]]"), new Singleton("first"))
	})

	it("correctly deserialize Pairs", () => {
		const ds = new PairDeserializer(new StringDeserializer(), new IntegerDeserializer())
		assert.deepEqual(ds.deserialize("[[first],2]"), new Pair("first", 2))
	})

	it("correctly deserialize Triples", () => {
		const ds = new TripleDeserializer(new StringDeserializer(), new IntegerDeserializer(), new BigIntegerDeserializer())
		assert.deepEqual(ds.deserialize("[[first],2,3]"), new Triple("first", 2, bi(3)))
		assert.isTrue(new Triple("first", 2, bi(3)) instanceof Triple)
		assert.isTrue(ds.deserialize("[[first],2,3]") instanceof Triple)
		assert.isTrue(ds.deserialize("[[first],2,3]") instanceof Tuple)
	})

	it("only deserializes strings", () => {
		const ds = new Deserializer()
		class D {}
		assert.throws(() => ds.deserialize(new D()), DeserializationError)
		assert.throws(() => ds.deserialize(null), DeserializationError)
		assert.throws(() => ds.deserialize(undefined), DeserializationError)
		assert.throws(() => ds.deserialize(23), DeserializationError)
	})
})
