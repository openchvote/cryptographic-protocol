/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { Alphabet } from "#Alphabet.js"

describe("Alphabet", () => {
	it("should be constructable from a valid string", () => {
		let a = new Alphabet("0123")
		assert.isTrue(a.containsAll("0123"))
		assert.throws(() => new Alphabet())
		assert.throws(() => new Alphabet(""))
		assert.throws(() => new Alphabet("1"))
		assert.throws(() => new Alphabet(1))
		assert.throws(() => new Alphabet("01223"))
		assert.throws(() => new Alphabet("00"))
	})
	it("should provide alphabet 'BASE_2'", () => {
		const alphabet = "01".split("")
		for (let i = 0; i < Alphabet.BASE_2.length; i++) assert.equal(Alphabet.BASE_2[i], alphabet[i])
	})
	it("should provide alphabet 'BASE_10'", () => {
		const alphabet = "0123456789".split("")
		for (let i = 0; i < Alphabet.BASE_10.length; i++) assert.equal(Alphabet.BASE_10[i], alphabet[i])
	})
	it("should provide alphabet 'BASE_16'", () => {
		const alphabet = "0123456789ABCDEF".split("")
		for (let i = 0; i < Alphabet.BASE_16.length; i++) assert.equal(Alphabet.BASE_16[i], alphabet[i])
	})
	it("should provide alphabet 'BASE_32'", () => {
		const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".split("")
		for (let i = 0; i < Alphabet.BASE_32.length; i++) assert.equal(Alphabet.BASE_32[i], alphabet[i])
	})
	it("should provide alphabet 'BASE_64'", () => {
		const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789=/".split("")
		for (let i = 0; i < Alphabet.BASE_64.length; i++) assert.equal(Alphabet.BASE_64[i], alphabet[i])
	})
	it("should provide alphabet 'LATIN_26'", () => {
		const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("")
		for (let i = 0; i < Alphabet.LATIN_26.length; i++) assert.equal(Alphabet.LATIN_26[i], alphabet[i])
	})
	it("should provide alphabet 'LATIN_52'", () => {
		const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split("")
		for (let i = 0; i < Alphabet.LATIN_52.length; i++) assert.equal(Alphabet.LATIN_52[i], alphabet[i])
	})
	it("should provide alphabet 'LATIN_EXTENDED'", () => {
		const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽž -'".split("")
		for (let i = 0; i < Alphabet.LATIN_EXTENDED.length; i++) assert.equal(Alphabet.LATIN_EXTENDED[i], alphabet[i])
	})
	it("should provide alphabet 'ALPHANUMERIC_32'", () => {
		const alphabet = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789".split("")
		for (let i = 0; i < Alphabet.ALPHANUMERIC_32.length; i++) assert.equal(Alphabet.ALPHANUMERIC_32[i], alphabet[i])
	})
	it("should provide alphabet 'ALPHANUMERIC_36'", () => {
		const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".split("")
		for (let i = 0; i < Alphabet.ALPHANUMERIC_36.length; i++) assert.equal(Alphabet.ALPHANUMERIC_36[i], alphabet[i])
	})
	it("should provide alphabet 'ALPHANUMERIC_57'", () => {
		const alphabet = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz23456789".split("")
		for (let i = 0; i < Alphabet.ALPHANUMERIC_57.length; i++) assert.equal(Alphabet.ALPHANUMERIC_57[i], alphabet[i])
	})
	it("should provide alphabet 'ALPHANUMERIC_62'", () => {
		const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".split("")
		for (let i = 0; i < Alphabet.ALPHANUMERIC_62.length; i++) assert.equal(Alphabet.ALPHANUMERIC_62[i], alphabet[i])
	})
	it("should provide getRank", () => {
		assert.equal(Alphabet.LATIN_52.getRank("B"), 1)
		assert.equal(Alphabet.BASE_16.getRank("0"), 0)
		assert.equal(Alphabet.BASE_16.getRank("F"), 15)
		assert.throws(() => Alphabet.BASE_16.getRank("X"))
	})
	it("should provide getChar", () => {
		assert.equal(Alphabet.LATIN_52.getChar(1), "B")
		assert.equal(Alphabet.BASE_16.getChar(0), "0")
		assert.equal(Alphabet.BASE_16.getChar(0), "0")
		assert.throws(() => Alphabet.BASE_16.getChar(16))
	})
	it("should provide contains", () => {
		const alphabet = "0123456789ABCDEF".split("")
		for (let i = 0; i < alphabet.length; i++) assert.isTrue(Alphabet.BASE_16.contains(alphabet[i]))
		assert.isFalse(Alphabet.BASE_16.contains("X"))
	})
	it("should provide containsAll", () => {
		assert.isTrue(Alphabet.BASE_16.containsAll("0123"))
		assert.isTrue(Alphabet.BASE_16.containsAll("3DE57F"))
		assert.isFalse(Alphabet.BASE_16.containsAll("01X23"))
	})
	it("should be immutable", () => {
		assert.throws(() => Alphabet.BASE_2[0] = "2")
		assert.throws(() => Alphabet.BASE2 = new Alphabet("012"))
		assert.throws(() => Alphabet.BASE3 = new Alphabet("012"))
	})
	it("should inherit Array properties", () => {
		assert.equal("0", Alphabet.BASE_16[0])
		assert.equal("1", Alphabet.BASE_16[1])
		assert.equal("F", Alphabet.BASE_16[15])
		assert.equal(undefined, Alphabet.BASE_16[17])
		assert.equal(16, Alphabet.BASE_16.length)
		assert.equal("0123456789ABCDEF", Alphabet.BASE_16.join(""))
	})
})
