/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { ByteArray } from "#util/sequence/ByteArray.js"
import { ByteArrayToInteger } from "#general/algorithms/ByteArrayToInteger.js"
import { bi } from "../../util.js"

describe("ByteArrayToInteger", () => {
	it("should return the correct number, converted from byte array", () => {
		assert.equal(ByteArrayToInteger(ByteArray.of(new Uint8Array())).toString(), bi(0).toString())
		assert.equal(ByteArrayToInteger(ByteArray.of(new Uint8Array([1]))).toString(), bi(1).toString())
		assert.equal(ByteArrayToInteger(ByteArray.of(new Uint8Array([255]))).toString(), bi(255).toString())
		assert.equal(ByteArrayToInteger(ByteArray.of(new Uint8Array([1, 0]))).toString(), bi(256).toString())
		assert.equal(ByteArrayToInteger(ByteArray.of(new Uint8Array([255, 255]))).toString(), bi(65535).toString())
		assert.equal(ByteArrayToInteger(ByteArray.of(new Uint8Array([1, 0, 0]))).toString(), bi(65536).toString())
		assert.equal(ByteArrayToInteger(ByteArray.of(new Uint8Array([255, 255, 255]))).toString(), bi(16777215).toString())
		assert.equal(ByteArrayToInteger(ByteArray.of(new Uint8Array([1, 0, 0, 0]))).toString(), bi(16777216).toString())
	})
	it("should raise an exception on missing input", () => {
		assert.throws(() => ByteArrayToInteger())
	})
})
