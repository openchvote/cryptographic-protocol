/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { BigInteger } from "#util/math/BigInteger.js"
import { GetChallenge } from "#general/algorithms/GetChallenge.js"
import { SecurityParameters } from "#SecurityParameters.js"

const securityParams = new SecurityParameters("LEVEL_0")

describe("GetChallenge", () => {
	it("should compute a challenge", () => {
		const c = GetChallenge(new BigInteger("4"), "test", securityParams)
		assert.isTrue(c.equals(new BigInteger("65316")))
	})
	it("should return different challenges for different inputs", () => {
		const c1 = GetChallenge(new BigInteger("1"), "test1", securityParams)
		const c2 = GetChallenge(new BigInteger("2"), "test2", securityParams)
		assert.isFalse(c1.equals(c2))
	})
})
