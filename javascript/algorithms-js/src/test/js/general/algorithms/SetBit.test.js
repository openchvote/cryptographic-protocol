/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { ByteArray } from "#util/sequence/ByteArray.js"
import { SetBit } from "#general/algorithms/SetBit.js"

describe("SetBit", () => {
	it("should set a set a Bit 1 at the correct position", () => {
		assert.deepEqual(SetBit(ByteArray.of(new Uint8Array([0x1A])), 6, 1).toByteArray(), new Uint8Array([0x5A]))
		assert.deepEqual(SetBit(ByteArray.of(new Uint8Array([0x1A])), 1, 1).toByteArray(), new Uint8Array([0x1A]))
		assert.deepEqual(SetBit(ByteArray.of(new Uint8Array([0x1A])), 0, 0).toByteArray(), new Uint8Array([0x1A]))
		assert.deepEqual(SetBit(ByteArray.of(new Uint8Array([0x1A])), 4, 0).toByteArray(), new Uint8Array([0x0A]))
		assert.deepEqual(SetBit(ByteArray.of(new Uint8Array([0x01, 0x13])), 14, 1).toByteArray(), new Uint8Array([0x01, 0x53]))
		assert.deepEqual(SetBit(ByteArray.of(new Uint8Array([0x01, 0x13])), 9, 1).toByteArray(), new Uint8Array([0x01, 0x13]))
		assert.deepEqual(SetBit(ByteArray.of(new Uint8Array([0x01, 0x13])), 8, 0).toByteArray(), new Uint8Array([0x01, 0x12]))
		assert.deepEqual(SetBit(ByteArray.of(new Uint8Array([0x01, 0x13])), 12, 0).toByteArray(), new Uint8Array([0x01, 0x03]))
	})
})
