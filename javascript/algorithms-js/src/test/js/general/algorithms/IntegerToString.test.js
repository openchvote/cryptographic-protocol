/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { Alphabet } from "#Alphabet.js"
import { IntegerToString } from "#general/algorithms/IntegerToString.js"
import { bi } from "../../util.js"

describe("IntegerToString", () => {
	it("should convert Integer to a String", () => {
		assert.equal(IntegerToString(bi(0), 4, Alphabet.BASE_2), "0000")
		assert.equal(IntegerToString(bi(0), 0, Alphabet.BASE_2), "")
		assert.equal(IntegerToString(bi(1), 4, Alphabet.BASE_2), "0001")
		assert.equal(IntegerToString(bi(1), 1, Alphabet.BASE_2), "1")
		assert.equal(IntegerToString(bi(2), 4, Alphabet.BASE_2), "0010")
		assert.equal(IntegerToString(bi(2), 2, Alphabet.BASE_2), "10")
		assert.equal(IntegerToString(bi(4), 4, Alphabet.BASE_2), "0100")
		assert.equal(IntegerToString(bi(4), 3, Alphabet.BASE_2), "100")
		assert.equal(IntegerToString(bi(8), 4, Alphabet.BASE_2), "1000")
		assert.equal(IntegerToString(bi(15), 4, Alphabet.BASE_2), "1111")

		assert.equal(IntegerToString(bi(731), 4, Alphabet.LATIN_26), "ABCD")
		assert.equal(IntegerToString(bi(25), 1, Alphabet.LATIN_26), "Z")
		assert.equal(IntegerToString(bi(650), 2, Alphabet.LATIN_26), "ZA")
		assert.equal(IntegerToString(bi(675), 2, Alphabet.LATIN_26), "ZZ")
		assert.equal(IntegerToString(bi(16900), 3, Alphabet.LATIN_26), "ZAA")
		assert.equal(IntegerToString(bi(17575), 3, Alphabet.LATIN_26), "ZZZ")
		assert.equal(IntegerToString(bi(439400), 4, Alphabet.LATIN_26), "ZAAA")
		assert.equal(IntegerToString(bi(456975), 4, Alphabet.LATIN_26), "ZZZZ")
	})
})
