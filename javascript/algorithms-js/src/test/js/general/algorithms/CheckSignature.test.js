/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { BigInteger, THREE, TWO } from "#util/math/BigInteger.js"
import { CheckSignature } from "#general/algorithms/CheckSignature.js"
import { GetChallenge } from "#general/algorithms/GetChallenge.js"
import { Pair, Triple } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"

describe("CheckSignature", () => {
	const params = new SecurityParameters("LEVEL_0")
	const {GG_q_hat, g_hat, ZZ_q_hat} = params
	const sk = new BigInteger("12345")
	const pk = GG_q_hat.pow(g_hat, sk)

	const m = "Hello World"
	const aux = "Test"
	const omega = new BigInteger("9")
	const t = GG_q_hat.pow(g_hat, omega)
	const y = new Triple(pk, m, aux)
	const c = GetChallenge(y, t, params)
	const s = ZZ_q_hat.subtract(omega, ZZ_q_hat.multiply(c, sk))
	const sigma = new Pair(c, s)

	it("should return true for a valid signature", () => {
		const m = "Hello World"
		const aux = "Test"
		assert.isTrue(CheckSignature(sigma, pk, m, aux, params))
	})


	it("should return false for an invalid signature", () => {
		const mX = "Hello Worlds"
		const auxX = "Tests"

		assert.isFalse(CheckSignature(sigma, pk, mX, aux, params))
		assert.isFalse(CheckSignature(sigma, pk, m, auxX, params))
		assert.isFalse(CheckSignature(sigma, GG_q_hat.pow(g_hat, THREE), m, aux, params))
		assert.isFalse(CheckSignature(new Pair(TWO, THREE), pk, m, aux, params))
	})
})
