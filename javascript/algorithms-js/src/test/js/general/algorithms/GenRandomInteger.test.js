/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { BigInteger, FOUR, ONE, TWO, ZERO } from "#util/math/BigInteger.js"
import { GenRandomInteger } from "#general/algorithms/GenRandomInteger.js"
import { Vector } from "#util/sequence/Vector.js"

describe("GenRandomInteger", () => {
	it("should generate a random integer between 0 and the specified upper bound", () => {
		let r = GenRandomInteger(new BigInteger("1000"))
		assert.isBelow(r.intValue(), 1000)
		assert.isAtLeast(r.intValue(), 0)
		assert.isTrue((r.compareTo(new BigInteger("1000")) < 0) && r.compareTo(ZERO) > 0)
	})
	it("should generate a random number strictly lower than q", () => {
		let r = GenRandomInteger(ONE)
		assert.isTrue(r.equals(ZERO))
		assert.isBelow(r.intValue(), 1)
		assert.isAtLeast(r.intValue(), 0)
	})

	it("random number distribution", () => {
		let stats = {}
		for (let i = 0; i < 500; i++) {
			let r = GenRandomInteger(new BigInteger("500"))
			if (stats[r] === undefined)
				stats[r] = 0
			stats[r] = stats[r] + 1
		}
		assert.isTrue(Object.keys(stats).length > 200)
	})

})

describe("GenRandomIntegerExcluding", () => {
	it("should generate a random integer excluding", () => {
		let r = GenRandomInteger(FOUR, new Vector([ZERO, ONE, TWO]))
		assert.equal(r.intValue(), 3)
	})
})
