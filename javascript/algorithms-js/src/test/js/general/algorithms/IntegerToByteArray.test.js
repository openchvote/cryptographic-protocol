/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { IntegerToByteArray } from "#general/algorithms/IntegerToByteArray.js"
import { bi } from "../../util.js"

describe("IntegerToByteArray", () => {
	it("should convert BigInteger to Byte Arrays", () => {
		assert.deepEqual(IntegerToByteArray(bi(0)).toByteArray(), new Uint8Array())
		assert.deepEqual(IntegerToByteArray(bi(1)).toByteArray(), new Uint8Array([1]))
		assert.deepEqual(IntegerToByteArray(bi(255)).toByteArray(), new Uint8Array([255]))
		assert.deepEqual(IntegerToByteArray(bi(256)).toByteArray(), new Uint8Array([1, 0]))
		assert.deepEqual(IntegerToByteArray(bi(65535)).toByteArray(), new Uint8Array([255, 255]))
		assert.deepEqual(IntegerToByteArray(bi(65536)).toByteArray(), new Uint8Array([1, 0, 0]))
		assert.deepEqual(IntegerToByteArray(bi(16777215)).toByteArray(), new Uint8Array([255, 255, 255]))
		assert.deepEqual(IntegerToByteArray(bi(16777216)).toByteArray(), new Uint8Array([1, 0, 0, 0]))
	})
	it("should raise an exception on no input", () => {
		assert.throws(() => IntegerToByteArray())
	})
	it("should raise an exception when receiving a negative BigInteger", () => {
		assert.throws(() => IntegerToByteArray(bi(-1)))
	})
})
