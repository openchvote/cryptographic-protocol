/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { ByteArray } from "#util/sequence/ByteArray.js"
import { SetWatermark } from "#general/algorithms/SetWatermark.js"

describe("SetWatermark", () => {
	it("should encode the correct watermark", () => {
		assert.deepEqual(SetWatermark(ByteArray.of(new Uint8Array([0x00])), 0, 3).toByteArray(), new Uint8Array([0x00]))
		assert.deepEqual(SetWatermark(ByteArray.of(new Uint8Array([0x00])), 1, 3).toByteArray(), new Uint8Array([0x01]))
		assert.deepEqual(SetWatermark(ByteArray.of(new Uint8Array([0x00])), 2, 3).toByteArray(), new Uint8Array([0x10]))
		assert.deepEqual(SetWatermark(ByteArray.of(new Uint8Array([0xFF, 0xFF, 0xFF, 0xFF])), 0, 3).toByteArray(), new Uint8Array([0xFE, 0xFF, 0xFE, 0xFF]))
		assert.deepEqual(SetWatermark(ByteArray.of(new Uint8Array([0xC1, 0xD2])), 0, 3).toByteArray(), new Uint8Array([0xC0, 0xD2]))
		assert.deepEqual(SetWatermark(ByteArray.of(new Uint8Array([0xC1, 0xD2])), 1, 3).toByteArray(), new Uint8Array([0xC1, 0xD2]))
		assert.deepEqual(SetWatermark(ByteArray.of(new Uint8Array([0xC1, 0xD2])), 0, 15).toByteArray(), new Uint8Array([0xC0, 0xC2]))
		assert.deepEqual(SetWatermark(ByteArray.of(new Uint8Array([0xCC, 0xDD])), 0, 15).toByteArray(), new Uint8Array([0xCC, 0xCC]))
		assert.deepEqual(SetWatermark(ByteArray.of(new Uint8Array([0xE3, 0xF4])), 1, 3).toByteArray(), new Uint8Array([0xE3, 0xF4]))
	})
	it("should raise an exception on bad input parameters", () => {
		assert.throws(() => SetWatermark(ByteArray.of(new Uint8Array([1, 2, 3, 4])), 8, 6))
	})
})
