/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { AESDecrypt } from "#util/crypto/AES.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { GenCiphertext } from "#general/algorithms/GenCiphertext.js"
import { RecHash } from "#general/algorithms/RecHash.js"
import { SecurityParameters } from "#SecurityParameters.js"

const securityParams = new SecurityParameters("LEVEL_0")

describe("GenCiphertext", () => {

	it("should encrypt a message", async () => {
		const sk = new BigInteger("13")
		const pk = securityParams.GG_q_hat.pow(securityParams.g_hat, sk)
		const M = ByteArray.of(new Uint8Array([1, 2, 3, 4, 5]))
		const e = await GenCiphertext(pk, M, securityParams)

		const [ek, IV, C] = e.values
		const K = RecHash(securityParams.GG_q_hat.pow(ek, sk)).truncate(securityParams.L_K)
		const p = await AESDecrypt(K, IV, C)

		assert.deepEqual(M.toByteArray(), p.toByteArray())
	})
})
