/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { ByteArray, EMPTY } from "#util/sequence/ByteArray.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { Pair, Quadruple, Quintuple, Singleton, Triple } from "#util/Tuples.js"
import { RecHash } from "#general/algorithms/RecHash.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { bi, byteArrayFromHex } from "../../util.js"

const parameters = new SecurityParameters("LEVEL_0")

describe("RecHash", () => {
	it("should be able to calculate the correct format with good length", () => {
		let hash = RecHash(ByteArray.of(new Uint8Array([30, 20])))
		assert.instanceOf(hash, ByteArray)
		assert.equal(hash.length, parameters.L)
	})
	it("should calculate the correct values", () => {
		assert.isTrue(RecHash(null).equals(byteArrayFromHex("5D53469F20FEF4F8EAB52B88044EDE69C77A6A68A60728609FC4A65FF531E7D0")))
		assert.isTrue(RecHash("test").equals(byteArrayFromHex("D3C65606206EA80CBD64DFF8B9D1C514D6D132FD7E9A68D0487019B71BB06035")))
		assert.isTrue(RecHash(0).equals(byteArrayFromHex("0A1E2736777F80A62BEB2DF72B649878481C0CA10194B832B5136BEFBAE54017")))
		assert.isTrue(RecHash(42).equals(byteArrayFromHex("B84829B96BFFABC60B419FA64C68448402F28042EA805281DE3B52F1E5DC8B1B")))
		assert.isTrue(RecHash(42739).equals(byteArrayFromHex("AA4349124C574D3C3E6070D8A17EEFE6C1A2C05564C68A268CC6B07AA483CEE1")))
		assert.isTrue(RecHash(bi("42739")).equals(byteArrayFromHex("AA4349124C574D3C3E6070D8A17EEFE6C1A2C05564C68A268CC6B07AA483CEE1")))
		assert.isTrue(RecHash(EMPTY).equals(byteArrayFromHex("2767F15C8AF2F2C7225D5273FDD683EDC714110A987D1054697C348AED4E6CC7")))
		assert.isTrue(RecHash(null, "test", bi("42")).equals(byteArrayFromHex("3744BEB150A8D0578D0C3A07DED7506952D72D82B47A13479A9C42C18E52B82E")))

		assert.isTrue(RecHash(new Matrix(0, 0, [])).equals(byteArrayFromHex("3B0C4D506212CD7E7B88BC93B5B1811AB5DE6796D2780E9DE7378C87FE9A80A6")))
		assert.isTrue(RecHash(new Matrix(1, 1, [[1]])).equals(byteArrayFromHex("E61DAB4785A40A93D89D10ACC57E04F57DDC63EDD957E793371B3E3136F3F38E")))
		assert.isTrue(RecHash(new Matrix(2, 3, [[1, 2, 3], [3, 2, 1]])).equals(byteArrayFromHex("9C5EBD8FFCA4988231972FDF8A428D25FA8D8F93BC75DA9FC1D2EB5BBD894C86")))

		assert.isTrue(RecHash(new Vector([])).equals(byteArrayFromHex("989216075A288AF2C12F115557518D248F93C434965513F5F739DF8C9D6E1932")))
		assert.isTrue(RecHash(new Vector([bi(1), bi(1), bi(1), bi(1)])).equals(byteArrayFromHex("40FB3F38F7A0D6865A2D5182611FC48B89875CC0C1F6CD239713E581BD4DD82E")))
		assert.isTrue(RecHash(new Vector([null, null, null, null])).equals(byteArrayFromHex("26F1E56D321277D687648F00DA58A1443DEC1BE7DBC40ED9F6CA55A4538371C7")))

		assert.isTrue(RecHash(new Singleton(1)).equals(byteArrayFromHex("0611A4C9CCA9BB48C5337F738E116EB800B87DEA48769C68C7C3466229AC8F8F")))
		assert.isTrue(RecHash(new Pair(1, 2)).equals(byteArrayFromHex("B137339C9479E8B538D07664D7B74EBECA5CE674524D7FAAF0F7C825DC2AF783")))
		assert.isTrue(RecHash(new Triple(1, 2, 3)).equals(byteArrayFromHex("A26FDDD8D817CEF61867F9CCE5BE4F763B4F5B264225E67A15B1682C7C2A591C")))
		assert.isTrue(RecHash(new Quadruple(1, 2, 3, 4)).equals(byteArrayFromHex("9FE2EAC04A8C42A0A78A82BD20D1760F3DCBF3863DCB6F32B96AE91F4F02A477")))
		assert.isTrue(RecHash(new Quintuple(1, 2, 3, 4, 5)).equals(byteArrayFromHex("49C36EEC6228B985066FCF6CC1AA7527F7202762D57AE7317D7F3377CC9B792C")))
	})
	it("should throw an error for invalid input", () => {
		assert.throws(() => RecHash())
		assert.throws(() => RecHash(true))
	})
})
