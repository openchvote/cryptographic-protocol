/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { Alphabet } from "#Alphabet.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { ByteArrayToString, ByteArrayToStringUTF8 } from "#general/algorithms/ByteArrayToString.js"

describe("ByteArrayToString", () => {
	it("should convert bytes to string", () => {
		assert.equal(ByteArrayToString(ByteArray.of(new Uint8Array()), Alphabet.BASE_16), "")
		assert.equal(ByteArrayToString(ByteArray.of(new Uint8Array([0x02, 0xDB])), Alphabet.BASE_16), "02DB")
		assert.equal(ByteArrayToString(ByteArray.of(new Uint8Array([0x19])), Alphabet.BASE_16), "19")
		assert.equal(ByteArrayToString(ByteArray.of(new Uint8Array([0x02, 0x8A])), Alphabet.BASE_16), "028A")
		assert.equal(ByteArrayToString(ByteArray.of(new Uint8Array([0x02, 0xA3])), Alphabet.BASE_16), "02A3")
		assert.equal(ByteArrayToString(ByteArray.of(new Uint8Array([0x42, 0x04])), Alphabet.BASE_16), "4204")
		assert.equal(ByteArrayToString(ByteArray.of(new Uint8Array([0x44, 0xA7])), Alphabet.BASE_16), "44A7")
		assert.equal(ByteArrayToString(ByteArray.of(new Uint8Array([0x06, 0xB4, 0x68])), Alphabet.BASE_16), "06B468")
		assert.equal(ByteArrayToString(ByteArray.of(new Uint8Array([0x06, 0xF9, 0x0F])), Alphabet.BASE_16), "06F90F")
	})

	it("should convert bytes to UTF8 string", () => {
		assert.equal(ByteArrayToStringUTF8(ByteArray.of(new Uint8Array([0x61, 0x62, 0x63]))), "abc")
		assert.equal(ByteArrayToStringUTF8(ByteArray.of(new Uint8Array([0x68, 0x65, 0x6C, 0x6C, 0x6F]))), "hello")
		assert.equal(ByteArrayToStringUTF8(ByteArray.of(new Uint8Array([0x77, 0x6F, 0x72, 0x6C, 0x64, 0x21]))), "world!")
		assert.equal(ByteArrayToStringUTF8(ByteArray.of(new Uint8Array([0x6C, 0x6F, 0x72, 0x65, 0x6D, 0x20, 0x69, 0x70, 0x73, 0x75, 0x6D]))), "lorem ipsum")
		assert.equal(ByteArrayToStringUTF8(ByteArray.of(new Uint8Array([0xf0, 0x9f, 0x97, 0xb3]))), "🗳")
		assert.throws(() => ByteArrayToStringUTF8())
		assert.throws(() => ByteArrayToStringUTF8(ByteArray.of(new Uint8Array([61, 62, 5555]))))
	})
})
