/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { ByteArray } from "#util/sequence/ByteArray.js"


/**
 *
 * @param {string} n
 * @returns {BigInteger}
 */
export function bi(n) {
	return new BigInteger(n.toString())
}

/**
 *
 * @param {string} hex
 * @returns {ByteArray}
 */
export function byteArrayFromHex(hex) {
	return ByteArray.of(Uint8Array.from(hex.match(/.{2}/g).map((byte) => parseInt(byte, 16))))
}

/**
 *
 * @param {ByteArray} c
 * @returns {string}
 */
export function byteArrayToHex(c) {
	return Array.from(c).map(x => (x >>> 0).toString(16).padStart(2, "0")).reduce((x, y) => x + y).toUpperCase()
}
