/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { ByteArray } from "#util/sequence/ByteArray.js"
import { GetInspectionCode } from "#protocols/common/algorithms/GetInspectionCode.js"
import { UsabilityParameters } from "#UsabilityParameters.js"
import { Vector } from "#util/sequence/Vector.js"

const usabilityParams = new UsabilityParameters("A57_A57_A10_A16", "LEVEL_0")

describe("GetInspectionCode", () =>
	it("should return correct inspection code of multiple byte arrays", () => {
		let bold_i = new Vector([
			ByteArray.of(new Uint8Array(2).fill(170)),
			ByteArray.of(new Uint8Array(2).fill(86)),
		])

		assert.equal(GetInspectionCode(bold_i, usabilityParams), "FCFC")
	})
)
