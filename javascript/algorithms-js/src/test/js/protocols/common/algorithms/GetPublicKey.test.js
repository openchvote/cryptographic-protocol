/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { BigInteger } from "#util/math/BigInteger.js"
import { GetPublicKey } from "#protocols/common/algorithms/GetPublicKey.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"

const parameters = new SecurityParameters("LEVEL_0")
const {g, p, q} = parameters

describe("GetPublicKey", () =>
	it("should be the same as the sum of the exponents to the base", () => {
		const sks = [5, 6, 34, 12]

		let bold_pk = new Vector(sks.map(sk => g.modPow(new BigInteger(sk), p)))
		let pk = GetPublicKey(bold_pk, parameters)

		let exponent = new BigInteger(sks.reduce((e, sk) => e + sk, 0))
		let pk2 = g.modPow(exponent, p)
		pk2 = pk2.compareTo(q) <= 0 ?  pk2 : p.subtract(pk2)

		assert.isTrue(pk.equals(pk2))
	})
)
