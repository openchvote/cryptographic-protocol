/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { CheckConfirmationProof } from "../subalgorithms/GenConfirmationProof.test.js"

import { BigInteger } from "#util/math/BigInteger.js"
import { GenConfirmation } from "#protocols/common/algorithms/GenConfirmation.js"
import { IntegerToString } from "#general/algorithms/IntegerToString.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { Pair } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { UsabilityParameters } from "#UsabilityParameters.js"
import { bi } from "../../../util.js"

const securityParams = new SecurityParameters("LEVEL_0")
const usabilityParams = new UsabilityParameters("A57_A57_A10_A16", "LEVEL_0")
const {g_hat, p_hat, q_hat} = securityParams

describe("GenConfirmation", () => {
	it("should generate Confirmation gamma from Confirmation code Y and Matrix bold_P", () => {
		let Y = IntegerToString(new BigInteger("200000000", 10), usabilityParams.ell_Y, usabilityParams.A_Y)

		/*
		* 1st column: 100x^3 + 200x^2 + 300x + 400 mod q_hat
		* 2nd column: 1000x^3 + 2000x^2 + 3000x + 4000 mod q_hat
		* 3rd column: 10000x^3 + 20000x^2 + 30000x + 40000 mod q_hat
		* 4th column: 100000x^3 + 200000x^2 + 300000x + 400000 mod q_hat
		* Values x chosen randomly
		*/
		let bold_P = new Matrix(4, 4, [
			[new Pair(bi("50"), bi("13015400")), new Pair(bi("90"), bi("745474000")), new Pair(bi("130"), bi("716961147")), new Pair(bi("170"), bi("446886381"))],
			[new Pair(bi("60"), bi("22338400")), new Pair(bi("100"), bi("1020304000")), new Pair(bi("140"), bi("71267189")), new Pair(bi("180"), bi("499977011"))],
			[new Pair(bi("70"), bi("35301400")), new Pair(bi("110"), bi("1355534000")), new Pair(bi("150"), bi("269573231")), new Pair(bi("190"), bi("2138076704"))],
			[new Pair(bi("80"), bi("52504400")), new Pair(bi("120"), bi("1757164000")), new Pair(bi("160"), bi("1371879273")), new Pair(bi("200"), bi("2876188481"))]
		])

		let gamma = GenConfirmation(Y, bold_P, securityParams, usabilityParams)
		let [y_hat, z_hat, pi] = gamma.values

		assert.isTrue(CheckConfirmationProof(pi, y_hat, z_hat, securityParams))
		assert.deepEqual(y_hat, g_hat.modPow(bi(200000000), p_hat))
		assert.deepEqual(z_hat, g_hat.modPow(bi(400 + 4000 + 40000 + 400000).mod(q_hat), p_hat))
	})
})
