/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { BigInteger } from "#util/math/BigInteger.js"
import { GenQuery } from "#protocols/common/subalgorithms/GenQuery.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { bi } from "../../../util.js"


const params = new SecurityParameters("LEVEL_0")
const ZZPlus_p = params.ZZPlus_p


/**
 *
 * @param {Vector<BigInteger>} encryptions
 * @param {BigInteger} sk
 * @returns {Vector<BigInteger>}
 */
export function decrypt(encryptions, sk) {
	let decryptions = []
	for (let enc of encryptions) {
		let m = ZZPlus_p.multiply(enc.first, ZZPlus_p.pow(ZZPlus_p.invert(enc.second), sk))
		decryptions.push(m)
	}
	return new Vector(decryptions)
}

describe("GenQuery", () => {
	it("should create a valid OT Query / Elgamal encryption", () => {
		let sk = bi("2000000")
		let pk = params.ZZPlus_p.pow(params.g, sk)
		let bold_m = new Vector([bi(5), bi(23)])
		let [bold_a,] = GenQuery(bold_m, pk, params).values
		assert.deepEqual(bold_m.values.map(x => x.toString()), decrypt(bold_a, sk).values.map(x => x.toString()))
	})
})
