/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { GetValidityCredential } from "#protocols/common/subalgorithms/GetValidityCredential.js"
import { Pair } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { bi } from "../../../util.js"

let parameters = new SecurityParameters("LEVEL_0")

describe("GetValidityCredential", () => {
	it("should compute the correct value z = A(0) from a polynomial defined by a Vector of points", () => {
		let poly1 = new Vector([new Pair(bi(10), bi(200)), new Pair(bi(20), bi(300))]) // 10x+100
		assert.isTrue(GetValidityCredential(poly1, parameters).equals(bi(100)))
		let poly2 = new Vector([new Pair(bi(10), bi(13_574)), new Pair(bi(20), bi(97_504)), new Pair(bi(100), bi(11_223_344)), new Pair(bi(200), bi(88_886_644))]) // 11x^3 + 22x^2 + 33x + 44
		assert.isTrue(GetValidityCredential(poly2, parameters).equals(bi(44)))
	})
})
