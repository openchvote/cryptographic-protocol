/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { assert } from "chai"
import { describe, it } from "mocha"

import { BigInteger } from "#util/math/BigInteger.js"
import { ConfirmationProof } from "#protocols/common/model/ConfirmationProof.js"
import { GenConfirmationProof } from "#protocols/common/subalgorithms/GenConfirmationProof.js"
import { GetChallenge } from "#general/algorithms/GetChallenge.js"
import { Pair } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { bi } from "../../../util.js"

const parameters = new SecurityParameters("LEVEL_0")
const {g_hat, p_hat} = parameters


/**
 *
 * @param {ConfirmationProof} pi
 * @param {BigInteger} y_hat
 * @param {BigInteger} z_hat
 * @param {SecurityParameters} parameters
 * @returns {boolean}
 */
export function CheckConfirmationProof(pi, y_hat, z_hat, parameters) {
	let [c, s] = pi.values

	let y = new Pair(y_hat, z_hat)
	let t_1 = y_hat.modPow(c, p_hat).multiply(g_hat.modPow(s.first, p_hat)).mod(p_hat)
	let t_2 = z_hat.modPow(c, p_hat).multiply(g_hat.modPow(s.second, p_hat)).mod(p_hat)
	let t = new Pair(t_1, t_2)
	let c_prime = GetChallenge(y, t, parameters)
	return c.equals(c_prime)
}

// Cross-Checked GenConfirmationProof() output with OpenCHVote CheckConfirmationProof()
describe("GenConfirmationProof", () => {
	it("should generation confirmation proof over private credentials", () => {
		let y = bi(10000)
		let z = bi(20000)
		let y_hat = g_hat.modPow(y, p_hat)
		let z_hat = g_hat.modPow(z, p_hat)
		let pi = GenConfirmationProof(y, z, y_hat, z_hat, parameters)
		assert.isTrue(CheckConfirmationProof(pi, y_hat, z_hat, parameters))
	})
})
