/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { IllegalArgumentError } from "#util/Errors.js"

/**
 * This class implements the concept of an alphabet, which consists of an ordered finite set of characters. The position
 * of the characters in the order is called rank. The rank of the first character in an alphabet of size n is 0 and the
 * rank of the last element is n-1. By extending the `Array` object, an alphabet also inherits the properties of an
 * array. Instances of this class are immutable.
 */
export class Alphabet extends Array {

	/**
	 * Constructs a new alphabet. The characters of the alphabet are given by a string in which each character is
	 * contained at most once. The positions of the characters in the string determine theirranks. The minimal size of
	 * an alphabet is 2.
	 * @param {string} characters - The set of characters given as a string
	 */
	constructor(characters) {
		if (typeof characters !== "string" || characters.length < 2)
			throw new IllegalArgumentError("Invalid length")
		super(...characters)
		if (!(this.every((c, i) => this.lastIndexOf(c) === i)))
			throw new IllegalArgumentError("Duplicate character")
		Object.freeze(this)
	}

	/**
	 * Returns the rank of the character in the alphabet. If the character is not an element of the alphabet, an
	 * error is thrown.
	 * @param {string} c - The character
	 * @returns {number} - The rank of the character
	 */
	getRank(c) {
		if (!this.contains(c))
			throw new IllegalArgumentError()
		return this.indexOf(c)
	}

	/**
	 * Returns the character with the given rank in the alphabet. For invalid ranks, an error is thrown.
	 * @param {number} rank - The rank
	 * @returns {string} - The character with the rank
	 */
	getChar(rank) {
		if(rank < 0 || rank >= this.length)
			throw new IllegalArgumentError()
		return this[rank]
	}

	/**
	 * Checks if the given character is an element of the alphabet.
	 * @param {string} c - The given character
	 * @returns {boolean} - `true` if `c` is in the alphabet, otherwise `false`
	 */
	contains(c) {
		return this.includes(c)
	}

	/**
	 * Checks if all characters from a given string are elements of the alphabet.
	 * @param {string} s - The given string
	 * @returns {boolean} - `true` if all characters from `s` are in the alphabet, otherwise `false`
	 */
	containsAll(s) {
		return s.split("").every(c => this.contains(c))
	}

	/**
	 * Adds a new character to the alphabet. The returned alphabet is a new object, which has been created without
	 * modifying the given alphabet.
	 * @param {string} c - The character to be added
	 * @returns {Alphabet} - The extended alphabet
	 */
	addCharacter(c) {
		if (!(typeof c === "string") || c.length !== 1)
			throw new IllegalArgumentError("Not a character")
		if (this.contains(c)) {
			return this
		} else {
			return new Alphabet(this.join("") + c)
		}
	}
}


// STANDARD ALPHABETS

/** Binary alphabet (digits 0 and 1) */
Alphabet.BASE_2 = new Alphabet("01")
/** Decimal alphabet (digits 0 to 9) */
Alphabet.BASE_10 = new Alphabet("0123456789")
/** Hexadecimal alphabet (digits 0 to 9, letters A to E) */
Alphabet.BASE_16 = new Alphabet("0123456789ABCDEF")
/** Fail-safe 32-character alphabet (letters A to Z, digits 2 to 7) */
Alphabet.BASE_32 = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567")
/** 64-character alphabet (letters A to Z, a to z, digits 0 to 9, symbols = and /) */
Alphabet.BASE_64 = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789=/")
/** Upper-case letters alphabet (digits A to Z) */
Alphabet.LATIN_26 = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
/** Upper-case and lower-case letters alphabet (digits A to Z, a to z) */
Alphabet.LATIN_52 = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
/** Extended Latin alphabet with characters used in different Latin languages */
Alphabet.LATIN_EXTENDED = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽž -'")
/** Fail-safe alphanumeric alphabet (digits A to Z, 0 to 9, without I, O, 0, 1) */
Alphabet.ALPHANUMERIC_32 = new Alphabet("ABCDEFGHJKLMNPQRSTUVWXYZ23456789")
/** Alphanumeric alphabet (digits A to Z, 0 to 9) */
Alphabet.ALPHANUMERIC_36 = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
/** Fail-safe alphanumeric alphabet (digits A to Z, a to z, 0 to 9, without I, O, l, 0, 1) */
Alphabet.ALPHANUMERIC_57 = new Alphabet("ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz23456789")
/** Alphanumeric alphabet (digits A to Z, a to z, 0 to 9) */
Alphabet.ALPHANUMERIC_62 = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")

// Prevent Alphabet from being altered
Object.freeze(Alphabet)
