/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { PreconditionError } from "./Errors.js"

/**
 * This module provides the `check` function for performing precondition tests with respect to algorithm parameters.
 * In addition to the general 'check' function, which only accepts a boolean value but the actual precondition check
 * is performed independently of this module, there are also a number of convenience functions for type checking.
 */

/**
 * Throws an {@link PreconditionError} if the given boolean `b` is false.
 * @param {boolean} b - A boolean value representing the result of a precondition test
 * @throws {PreconditionError}
 */
export function check(b) {
	if (!b) throw new PreconditionError()
}

/**
 * Throws an {@link PreconditionError} if one of the objects in the given array is `null`
 * @param {...*} objects - The given objects
 * @throws {PreconditionError}
 */
check.notNull = function(...objects) {
	if (objects.some(value => value === null || value === undefined))
		throw new PreconditionError()
}

/**
 * Throws an {@link PreconditionError} if the given objects do not match their expected typ. Either a single object
 * (`check(<type>, <object>)`) or multiple objects (`check([<type>, <object], [<type>, <object>], ...)`) can be tested.
 * @param {...(Array[] | object)} typeObjectPairs - Either a single or multiple type/object pairs
 * @throws {PreconditionError}
 */
check.Type = function(...typeObjectPairs) {
	if (Array.isArray(typeObjectPairs[0]))
		typeObjectPairs.forEach(pair => checkType(...pair))
	else
		checkType(...typeObjectPairs)
}

/**
 * Throws an {@link PreconditionError} if the given value is not a number.
 * @param {*} value - The given value
 * @throws {PreconditionError}
 */
check.Number = function(value) {
	if (typeof value !== "number" || isNaN(value) || !isFinite(value))
		throw new PreconditionError()
}

/**
 * Throws an {@link PreconditionError} if the given value is not a string.
 * @param {*} value - The given value
 * @throws {PreconditionError}
 */
check.String = function(value) {
	if (!(typeof value === "string"))
		throw new PreconditionError()
}

/**
 * Helper to test a single object's type.
 * @private
 * @param {object} type
 * @param {object} object
 * @throws PreconditionError
 */
function checkType(type, object) {
	if (!(object instanceof type)) throw new PreconditionError()
}
