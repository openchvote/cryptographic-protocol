/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "./math/BigInteger.js"
import { ByteArray } from "./sequence/ByteArray.js"
import {
	CLOSE,
	DELIMITER,
	ESCAPED_CLOSE,
	ESCAPED_OPEN,
	ESCAPE,
	FALSE,
	NULL,
	OPEN,
	TRUE,
	OPEN_REGEX, CLOSE_REGEX
} from "./Serializer.js"
import { DeserializationError, UnsupportedOperationError } from "./Errors.js"
import { Matrix, ZeroIndexedColMatrix } from "./sequence/Matrix.js"
import { Pair, Singleton, Triple, Tuple } from "./Tuples.js"
import { Vector, ZeroIndexedVector } from "./sequence/Vector.js"

/**
 * This class provides the public interface for the deserialization of a string into an object. It s the base class for
 * all concrete deserializers, which in return only have to implement a private `_deserialize` method.
 * @abstract
 */
export class Deserializer {

	/**
	 * Deserializes the given string into an object. The type of the returned object is defined by the concrete
	 * deserializer instance.
	 * @param {string} string
	 * @returns {*}
	 */
	deserialize(string) {
		if (typeof string !== "string") throw new DeserializationError("Input is not a string.")
		if (string === NULL) return null
		return this._deserialize(string)
	}

	/**
	 * @abstract
	 * @param {string} string
	 * @returns {*}
	 */
	// eslint-disable-next-line no-unused-vars
	_deserialize(string) { throw new UnsupportedOperationError() }
}

/**
 * Deserialized a string into a boolean. The string must be either `0` ({@link FALSE}) or `1` ({@link TRUE}).
 */
export class BooleanDeserializer extends Deserializer {
	/** @override */
	_deserialize(string) {
		if (string === FALSE) return false
		else if (string === TRUE) return true
		else throw new DeserializationError(`Invalid boolean '${string}'`)

	}
}

/**
 * Deserializes a string into an integer of type `number`. The integer is expected to be encoded in base 10.
 */
export class IntegerDeserializer extends Deserializer {
	/** @override */
	_deserialize(string) {
		const number = parseInt(string, 10)
		if (isNaN(number) || !isFinite(number)) {
			throw new DeserializationError(`Invalid integer '${string}'`)
		}
		return number
	}
}

/**
 * Deserializes a string into a big integer. The big integer is expected to be encoded in base 16.
 */
export class BigIntegerDeserializer extends Deserializer {
	/** @override */
	_deserialize(string) {
		if (!string.match(/^-?[A-Fa-f0-9]+$/))
			throw new DeserializationError(`Invalid big integer '${string}'`)
		return new BigInteger(string, 16)
	}
}

/**
 * Deserializes a string into a string.
 */
export class StringDeserializer extends Deserializer {
	/** @override */
	_deserialize(string) {
		if (!string.match(/^\[(?:[^[\]]|\\]|\\\[)*]$/s))
			throw new DeserializationError(`Invalid string '${string}'`)
		return string
			.replace(OPEN_REGEX, "")
			.replace(CLOSE_REGEX, "")
			.replaceAll(ESCAPED_OPEN, OPEN)
			.replaceAll(ESCAPED_CLOSE, CLOSE)
	}
}

/**
 * Deserializes a string into a byte array.
 */
export class ByteArrayDeserializer extends Deserializer {
	/** @override */
	_deserialize(string) {
		if (!string.match(/^\[([A-Fa-f0-9]{2})*]$/))
			throw new DeserializationError(`Invalid byte array '${string}'`)

		const hex = string.replace(/[[\]]/g, "").match(/[A-Fa-f0-9]{2}/g)
		const bytes = hex === null ? new Uint8Array() : Uint8Array.from(hex.map((byte) => parseInt(byte, 16)))
		return ByteArray.of(bytes)
	}
}

/**
 * Deserializes a string into a vector. The vector's items are deserialized according to the given item deserializer.
 */
export class VectorDeserializer extends Deserializer {
	_itemDeserializer

	/**
	 * Constructs a new vector deserializer for the given item deserializer.
	 * @param {Deserializer} itemDeserializer - The item deserializer
	 */
	constructor(itemDeserializer) {
		super()
		this._itemDeserializer = itemDeserializer
	}

	/** @override */
	_deserialize(string) {
		if (!string.match(/^\[[A-Fa-f0-9]+,[A-Fa-f0-9]+,\[.*]]$/s))
			throw new DeserializationError(`Invalid vector '${string}'`)
		const [minIndexString, , values] = split(string)
		const minIndex = new IntegerDeserializer().deserialize(minIndexString)
		const items = split(values)
			.map(x => this._itemDeserializer.deserialize(x))

		if (minIndex === 0) {
			return new ZeroIndexedVector(items)
		} else if (minIndex === 1) {
			return new Vector(items)
		} else {
			throw new DeserializationError("Vector only supports zero and one indexed.")
		}
	}
}

/**
 * Deserializes a string into a matrix. The matrix's items are deserialized according to the given item deserializer.
 */
export class MatrixDeserializer extends Deserializer {
	_itemDeserializer

	/**
	 * Constructs a new matrix deserializer for the given item deserializer.
	 * @param {Deserializer} itemDeserializer - The item deserializer
	 */
	constructor(itemDeserializer) {
		super()
		this._itemDeserializer = itemDeserializer
	}

	/** @override */
	_deserialize(string) {
		if (!string.match(/^\[[A-Fa-f0-9]+,[A-Fa-f0-9]+,[A-Fa-f0-9]+,[A-Fa-f0-9]+,\[.*\]\]$/s))
			throw new DeserializationError(`Invalid Matrix '${string}'`)
		let [minRowIndex, maxRowIndex, minColIndex, maxColIndex, values] = split(string)
		const dimensionDeserializer = new IntegerDeserializer()
		minRowIndex = dimensionDeserializer.deserialize(minRowIndex)
		maxRowIndex = dimensionDeserializer.deserialize(maxRowIndex)
		minColIndex = dimensionDeserializer.deserialize(minColIndex)
		maxColIndex = dimensionDeserializer.deserialize(maxColIndex)

		if (minRowIndex !== 1 || minColIndex < 0 || minColIndex > 1)
			throw new DeserializationError(`Only 1 indexed row and 0/1 indexed col matrices supported '${string}'`)

		const height = maxRowIndex - minRowIndex + 1
		const width = maxColIndex - minColIndex + 1

		const data = split(values)
			.map(x => this._itemDeserializer.deserialize(x))

		if (height * width !== data.length)
			throw new DeserializationError(`Invalid Matrix '${string}'`)

		const M = minColIndex === 0 ? ZeroIndexedColMatrix : Matrix
		if (height === 0 || width === 0)
			return new M.Builder(height, width).build()

		return new M.Builder(height, width).setValues(data).build()
	}
}

/**
 * Deserializes a string into a tuple. The tuple's items are deserialized according to the given item deserializers.
 */
export class TupleDeserializer extends Deserializer {
	_itemDeserializers

	/**
	 * Constructs a new tuple deserializer for the given item deserializers.
	 * @param {...Deserializer} itemDeserializers - The item deserializers
	 */
	constructor(...itemDeserializers) {
		super()
		this._itemDeserializers = itemDeserializers
	}

	/** @override */
	_deserialize(s) {
		return new Tuple(...this._parse(s))
	}

	/**
	 * Parsing the string is implemented in a separate method to make it available to the subclasses.
	 * @param {string} s
	 * @returns {Array<*>}
	 */
	_parse(s) {
		const items = split(s)
		if (items.length !== this._itemDeserializers.length) {
			throw new DeserializationError("Tuples size does not match deserializers provided.")
		}
		const result = []
		for (let i = 0; i < items.length; i++) {
			result.push(this._itemDeserializers[i].deserialize(items[i]))
		}
		return result
	}
}

/**
 * Deserializes a string into a singleton. The singleton's item is deserialized according to the given item deserializer.
 */
export class SingletonDeserializer extends TupleDeserializer {
	/**
	 * Constructs a new singleton deserializer for the given item deserializer.
	 * @param {Deserializer} firstID - The item deserializer
	 */
	constructor(firstID) {
		super(firstID)
	}
	/** @override */
	_deserialize(s) {
		return new Singleton(...this._parse(s))
	}
}

/**
 * Deserializes a string into a pair. The pair's items are deserialized according to the given item deserializers.
 */
export class PairDeserializer extends TupleDeserializer {
	/**
	 * Constructs a new pair deserializer for the given item deserializers.
	 * @param {Deserializer} firstID - The first item deserializer
	 * @param {Deserializer} secondID - The second item deserializer
	 */
	constructor(firstID, secondID) {
		super(firstID, secondID)
	}
	/** @override */
	_deserialize(s) {
		return new Pair(...this._parse(s))
	}
}

/**
 * Deserializes a string into a triple. The triple's items are deserialized according to the given item deserializers.
 */
export class TripleDeserializer extends TupleDeserializer {
	/**
	 * Constructs a new triple deserializer for the given item deserializers.
	 * @param {Deserializer} firstID - The first item deserializer
	 * @param {Deserializer} secondID - The second item deserializer
	 * @param {Deserializer} thirdID - The third item deserializer
	 */
	constructor(firstID, secondID, thirdID) {
		super(firstID, secondID, thirdID)
	}
	/** @override */
	_deserialize(s) {
		return new Triple(...this._parse(s))
	}
}

/**
 * Deserializes a string into a quadruple. The quadruple's items are deserialized according to the given item deserializers.
 */
export class QuadrupleDeserializer extends TupleDeserializer {
	/**
	 * Constructs a new quadruple deserializer for the given item deserializers.
	 * @param {Deserializer} firstID - The first item deserializer
	 * @param {Deserializer} secondID - The second item deserializer
	 * @param {Deserializer} thirdID - The third item deserializer
	 * @param {Deserializer} fourthID - The fourth item deserializer
	 */
	constructor(firstID, secondID, thirdID, fourthID) {
		super(firstID, secondID, thirdID, fourthID)
	}
	/** @override */
	_deserialize(s) {
		return new Triple(...this._parse(s))
	}
}

/**
 * Deserializes a string into a quintuple. The quintuple's items are deserialized according to the given item deserializers.
 */
export class QuintupleDeserializer extends TupleDeserializer {
	/**
	 * Constructs a new quintuple deserializer for the given item deserializers.
	 * @param {Deserializer} firstID - The first item deserializer
	 * @param {Deserializer} secondID - The second item deserializer
	 * @param {Deserializer} thirdID - The third item deserializer
	 * @param {Deserializer} fourthID - The fourth item deserializer
	 * @param {Deserializer} fifthID - The fifth item deserializer
	 */
	constructor(firstID, secondID, thirdID, fourthID, fifthID) {
		super(firstID, secondID, thirdID, fourthID, fifthID)
	}
	/** @override */
	_deserialize(s) {
		return new Triple(...this._parse(s))
	}
}

/**
 * @private
 * @param {string} string
 * @returns {string[]}
 */
function split(string) {
	const result = []
	const length = string.length
	if (length < 2 || string.charAt(0) !== OPEN || string.charAt(length - 1) !== CLOSE)
		throw new DeserializationError(`Missing start and end bracket '${string}'`)
	if (length > 2) {
		let counter = 0
		let i = 1
		for (let j = 1; j < length; j++) {
			if (counter < 0)
				throw new DeserializationError(`Imbalanced string '${string}'`)
			const c = string.charAt(j)
			if (c === DELIMITER || j === length - 1) {
				if (counter === 0) {
					result.push(string.slice(i, j))
					i = j + 1
				}
			} else {
				if (c === OPEN && string.charAt(j - 1) !== ESCAPE)
					counter++
				if (c === CLOSE && string.charAt(j - 1) !== ESCAPE)
					counter--
			}
		}
	}
	return result
}
