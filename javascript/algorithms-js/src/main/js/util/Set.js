/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { Alphabet } from "#Alphabet.js"
import { BigInteger } from "./math/BigInteger.js"
import { ByteArray } from "./sequence/ByteArray.js"
import { IllegalArgumentError, UnsupportedOperationError } from "./Errors.js"
import { Matrix } from "./sequence/Matrix.js"
import { Pair, Triple } from "./Tuples.js"
import { Vector } from "./sequence/Vector.js"

/**
 * This class provides a simple and consistent way for implementing set membership tests for different types of sets.
 * The basic membership test can either be implemented by the subclass or, for convenience and to prevent excessive
 * subclassing, provided as a function during instantiation.
 * @template T
 */
export class Set {

	/**
	 * Creates a new set.
	 * @param {function(T): boolean} [contains] - The set membership test if the class is directly instantiated and
	 * not subclassed
	 */
	constructor(contains) {
		if (contains) {
			if (!(contains instanceof Function))
				throw new IllegalArgumentError()
			this.contains = contains
			Object.freeze(this)
		}
	}


	/**
	 * Performs the membership test on a single element `x`. Returns `true` if `x` is a member of the set, and `false`
	 * otherwise.
	 * @param {T} x - The given object
	 * @returns {boolean} - `true` if `x` is a member of the set
	 */
	// eslint-disable-next-line no-unused-vars
	contains(x) {
		throw new UnsupportedOperationError()
	}

	/**
	 * Extends the given set with the element `null`.
	 * @returns {Set<T>} - The set extended with `null`
	 */
	orNull() {
		const that = this
		return new Set(x => x === null || that.contains(x))
	}
}

/**
 * The set of non-negative integers {0,1,2,...}.
 * @type {Set<BigInteger>}
 */
Set.NN = new Set(x => x instanceof BigInteger && x.signum() >= 0)

/**
 * The set of positive integers {1,2,3,...}
 * @type {Set<BigInteger>}
 */
Set.NN_plus = new Set(x => x instanceof BigInteger && x.signum() > 0)

/**
 * Returns the set of all byte arrays of the given length.
 * @param {number} length - The of the byte arrays
 * @returns {Set<ByteArray>} - The set of byte arrays of length `length`
 */
Set.B = function(length) {
	if (!Number.isInteger(length) || length < 0)
		throw new IllegalArgumentError()
	return new Set(B => B instanceof ByteArray && B.length === length)
}

/**
 * The set of all byte arrays of arbitrary length.
 * @type {Set<ByteArray>}
 */
Set.B_star = new Set(B => B instanceof ByteArray)

/**
 * Returns the set of all strings
 *   - of arbitrary length if `length1`and `length2` are undefined
 *   - of length `length1` if `length1` is provided but `length2` is undefined
 *   - of length greater or equal to `length1` and smaller or equal to `length2` if `length1` and `length2` are provided
 * with characters from the given alphabet.
 * @param {Alphabet} alphabet - The given alphabet
 * @param {number} [length1] - The given length or minimal length
 * @param {number} [length2] - The maximal length
 * @returns {Set<string>} - The set of strings as described above
 */
Set.String = function(alphabet, length1, length2) {
	length2 = length2 || length1
	if (!(alphabet instanceof Alphabet)) throw new IllegalArgumentError()
	if (length1 === undefined) return new Set(s => typeof s === "string" && alphabet.containsAll(s))
	if (!Number.isInteger(length1) || length1 < 0 || !Number.isInteger(length2) || length2 < 0) throw new IllegalArgumentError()
	return new Set(s => typeof s === "string" && s.length >= length1 && s.length <= length2 && alphabet.containsAll(s))
}

/**
 * The set of all byte arrays representing a valid UTF8 encoding.
 * @type {Set<ByteArray>}
 */
Set.UTF8 = new Set(B => {
	if (!(B instanceof ByteArray)) return false
	const decoder = new TextDecoder("utf-8", {fatal: true})
	try {
		decoder.decode(B.toByteArray())
		return true
	} catch (e) { // eslint-disable-line no-unused-vars
		return false
	}
})

/**
 * Creates the Cartesian product of two input sets.
 * @template T1, T2
 * @param {Set<T1>} set1 - The first set
 * @param {Set<T2>} set2 - The second set
 * @returns {Set<Pair<T1, T2>>} - The Cartesian product of two sets
 */
Set.Pair = function(set1, set2) {
	if (!(set1 instanceof Set) || !(set2 instanceof Set)) throw new IllegalArgumentError()
	return new Set(x => x instanceof Pair && set1.contains(x.first) && set2.contains(x.second))
}

/**
 * Creates the Cartesian product of three input sets.
 * @template T1, T2, T3
 * @param {Set<T1>} set1 - The first set
 * @param {Set<T2>} set2 - The second set
 * @param {Set<T3>} set3 - The third set
 * @returns {Set<Triple<T1, T2, T3>>} - The Cartesian product of three sets
 */
Set.Triple = function(set1, set2, set3) {
	if (!(set1 instanceof Set) || !(set2 instanceof Set) || !(set3 instanceof Set)) throw new IllegalArgumentError()
	return new Set(x => x instanceof Triple && set1.contains(x.first) && set2.contains(x.second) && set3.contains(x.third))
}

/**
 * Returns the set of all vectors of length `length` with elements from the given set.
 * @template T
 * @param {Set<T>} set - The given set
 * @param {number} length - The length of the vectors
 * @returns {Set<Vector<T>>} - The set of all vectors of the given length with elements from the given set
 */
Set.Vector = function(set, length = false) {
	if (!(set instanceof Set) || (length !== false && (!Number.isInteger(length) || length < 0))) throw new IllegalArgumentError()
	return new Set(x => x instanceof Vector && (length === false || x.length === length) && x.every(set.contains.bind(set)))
}

/**
 * Returns the set of all matrices of the given height and width with elements from the given set.
 * @template T
 * @param {Set<T>} set - The given set
 * @param {number} height - The height of the matrices
 * @param {number} width - The width of the matrices
 * @returns {Set<Matrix<T>>} - The set of all matrices of the given height and width with elements from the given set
 */
Set.Matrix = function(set, height = false, width = false) {
	if (!(set instanceof Set) || (height !== false && (!Number.isInteger(height) || height < 0)) || (width !== false && (!Number.isInteger(width) || width < 0))) throw new IllegalArgumentError()
	return new Set(x => x instanceof Matrix && (height === false || x.height === height) && (width === false || x.width === width) && x.every(set.contains.bind(set)))
}


/**
 * The base class for all sets of elements of type number (integer). Its sole purpose is to distinguish integer-sets from
 * bigInteger-sets.
 */
export class IntSet extends Set {
	constructor(contains) {
		super(contains)
	}
}

/**
 * The set of non-negative integers {0,1,2,...}. The finite domain of the number data type limits this set to
 * integers smaller or equal to `Number.MAX_SAFE_INTEGER` (2^53 - 1).
 * @type {Set<number>}
 */
IntSet.NN = new IntSet(x => Number.isInteger(x) && x >= 0)

/**
 * The set of positive integers {1,2,3,...}. The finite domain of the number data type limits this set to
 * integers smaller or equal to `Number.MAX_SAFE_INTEGER` (2^53 - 1).
 * @type {Set<number>}
 */
IntSet.NN_plus = new IntSet(x => Number.isInteger(x) && x > 0)

/**
 * Creates the set {1,...,n} of positive integers smaller or equal to `n`.
 * @param {number} n - The upper bound of the range
 * @returns {Set<number>} - The set of positive integers smaller or equal to `n`
 */
IntSet.NN_plus_n = function(n) {
	if (!Number.isInteger(n) || n < 0) throw new IllegalArgumentError()
	return new IntSet(x => Number.isInteger(x) && x > 0 && x <= n)
}

/**
 * Returns the set {0,...,n-1} of integers modulo `n`.
 * @param {number} n - The modulo
 * @returns {Set<number>} - The set of integers modulo `n`
 */
IntSet.ZZ = function(n) {
	if (!Number.isInteger(n) || n < 0) throw new IllegalArgumentError()
	return new IntSet(x => Number.isInteger(x) && x >= 0 && x < n)
}

/**
 * The set of integers {0,1}.
 * @type {Set<number>}
 */
IntSet.BB = new IntSet(x => Number.isInteger(x) && x >= 0 && x <= 1)
