/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { Hashable } from "#util/Hashable.js"
import { IllegalArgumentError, IllegalStateError, UnsupportedOperationError } from "#util/Errors.js"
import { Vector, ZeroIndexedVector } from "./Vector.js"

/**
 * Objects of this class represent one-indexed matrices of a fixed height and width. The class can be instantiated by
 * passing the matrix's dimension and values to the constructor. Additionally, a {@link Matrix.Builder} is provided for
 * creating new instances incrementally. Instances of this class are immutable.
 * @template V
 */
export class Matrix extends Hashable {

	/**
	 * Number of rows
	 * @private
	 * @type {number}
	 */
	_height

	/**
	 * Number of columns
	 * @private
	 * @type {number}
	 */
	_width

	/**
	 * Zero-based array holding the values
	 * @private
	 * @type {V[][]}
	 */
	_values

	/**
	 * Creates a new matrix for the given dimension and values. It is the caller's responsibility to pass a copy of
	 * the original values if the underlying array must remain mutable.
	 * @param {number} height - The height of the matrix (number of rows)
	 * @param {number} width - The width of the matrix (number of columns)
	 * @param {V[][]} values - The values of the matrix
	 */
	constructor(height, width, values) {
		super()
		if (!Number.isInteger(height) || !Number.isInteger(width) || !Array.isArray(values))
			throw new IllegalArgumentError()
		if(values.length !== height || width < 0)
			throw new IllegalArgumentError()
		for (let row of values) {
			if (!Array.isArray(row) || row.length !== width)
				throw new IllegalArgumentError()
			Object.freeze(row)
		}
		this._height = height
		this._width = width
		this._values = values

		Object.freeze(this._values)
		Object.freeze(this)
	}

	/**
	 * Iterates row-wise over the values of the matrix.
	 * @yields {V}
	 */
	* [Symbol.iterator]() {
		for (let i = 0; i < this._values.length; i++) {
			yield* this._values[i]
		}
	}

	/**
	 * The height (number of rows) of the matrix.
	 * @returns {number} - The height
	 */
	get height() {
		return this._height
	}

	/**
	 * The width (number of columns) of the matrix.
	 * @returns {number} - The width
	 */
	get width() {
		return this._width
	}

	/**
	 * The size (dimension) of the matrix.
	 * @returns {Array<number>} - The size
	 */
	get size() {
		return [this._height, this._width]
	}

	/**
	 * Returns the value of the matrix at the given row and column index. If the index is invalid, an error is thrown.
	 * @param {number} rowIndex - The row index
	 * @param {number} colIndex - The column index
	 * @returns {V} - The value at the given index
	 */
	get(rowIndex, colIndex) {
		if (!Number.isInteger(rowIndex) || rowIndex < 1 || rowIndex > this._height )
			throw new IllegalArgumentError()
		if (!Number.isInteger(colIndex) || colIndex < 1 || colIndex > this._width )
			throw new IllegalArgumentError()
		return this._values[rowIndex - 1][colIndex - 1]
	}

	/**
	 * Returns the row as {@link Vector} of the matrix at the given index. If the index is invalid, an error is thrown.
	 * @param {number} rowIndex - The row index
	 * @returns {Vector<V>} - The row at the given index
	 */
	getRow(rowIndex) {
		if (!Number.isInteger(rowIndex) || rowIndex < 1 || rowIndex > this._height)
			throw new IllegalArgumentError()
		const row = this._values[rowIndex - 1]
		return new Vector(row)
	}

	/**
	 * Returns the column as {@link Vector} of the matrix at the given index. If the index is invalid, an error is
	 * thrown.
	 * @param {number} colIndex - The column index
	 * @returns {Vector<V>} - The column at the given index
	 */
	getCol(colIndex) {
		if (!Number.isInteger(colIndex) || colIndex < 1 || colIndex > this._width)
			throw new IllegalArgumentError()
		const col = this._values.map(x => x[colIndex - 1])
		return new Vector(col)
	}

	/**
	 * Tests whether all items of the matrix satisfy the given predicate. The predicate function is called with the item
	 * and the row and column index of the item.
	 * @param {function(V, number, number): boolean} predicate - The predicate function
	 * @returns {boolean} - `true` if all items of the matrix satisfy the predicate, `false` otherwise
	 */
	every(predicate) {
		return this._values.every((row, i) => row.every((item, j) => predicate(item, i+1, j+1)))
	}
	/**
	 * Returns the transposed matrix, in which the row and column indices and the height and width are swapped.
	 * @returns {Matrix<V>} - The transposed matrix
	 */
	transpose() {
		if (this._height === 0 || this._width === 0) {
			return new Matrix.Builder(this._width, this._height).build()
		}
		let values = this._values
		values = values[0].map((_, i) => values.map(row => row[i]))
		return new Matrix(this._width, this._height, values)
	}
}

/**
 * This builder class is the main tool for constructing one-indexed matrices from scratch. Values can be set either
 * individually, column-wise, or all at once using the different setters. When creating the builder, the dimension of
 * the resulting matrix must be provided.
 * @template V
 */
Matrix.Builder = class {
	_built
	_height  // #rows
	_width   // #cols
	_values  // Zero-based array

	/**
	 * Constructs a new matrix builder for the given dimension.
	 * @param {number} height - The height of the resulting matrix (number of rows)
	 * @param {number} width - The width of the resulting matrix (number of columns)
	 */
	constructor(height, width) {
		if (!Number.isInteger(height) || !Number.isInteger(width) || height < 0 || width < 0)
			throw new IllegalArgumentError()

		this._built = false
		this._height = height
		this._width = width
		this._values = new Array(this._height)

		for (let i = 0; i < this._height; i++) {
			this._values[i] = new Array(this._width)
		}
	}

	/**
	 * Sets the given value at the given index.
	 * @param {number} rowIndex - The row index
	 * @param {number} colIndex - The column index
	 * @param {V} value - The value to be set
	 * @returns {Matrix.Builder<V>} - The matrix builder itself
	 */
	set(rowIndex, colIndex, value) {
		if (this._built)
			throw new IllegalStateError()
		if (!Number.isInteger(rowIndex) || rowIndex < 1 || rowIndex > this._height)
			throw new IllegalArgumentError()
		if (!Number.isInteger(colIndex) || colIndex < 1 || colIndex > this._width)
			throw new IllegalArgumentError()
		this._values[rowIndex - 1][colIndex - 1] = value
		return this
	}

	/**
	 * Sets all values of the matrix at once. The values can be given either as a 1D array (used for deserialization)
	 * or as a vector of vectors. The number of values must match the dimension of the builder.
	 * @param {V[] | Vector<Vector<V>>} values - The values to be set
	 * @returns {Matrix.Builder<V>} - The matrix builder itself
	 */
	setValues(values) {
		if (this._built)
			throw new IllegalStateError()
		if (Array.isArray(values) && values.length === this._height * this._width) {
			// by 1D-Array
			for (let r = 0; r < this._height; r++) {
				for (let c = 0; c < this._width; c++) {
					this._values[r][c] = values[r * this._width + c]
				}
			}
		} else if (values instanceof Vector && values.length === this._height) {
			// by vector of vectors
			for (let r = 1; r <= this._height; r++) {
				if (!(values.get(r) instanceof Vector) || values.get(r).length !== this._width)
					throw new IllegalArgumentError()
				for (let c = 1; c <= this._width; c++) {
					this._values[r - 1][c - 1] = values.get(r).get(c)
				}
			}
		} else {
			throw new IllegalArgumentError()
		}
		return this
	}

	/**
	 * Sets the values of the column at the given index. The length of the given values vector must correspond to the
	 * dimension of the builder.
	 * @param {number} colIndex - The column index
	 * @param {Vector<V>} values - The column values
	 * @returns {Matrix.Builder<V>} - The matrix builder itself
	 */
	setColValues(colIndex, values) {
		if (this._built)
			throw new IllegalStateError()
		if (!Number.isInteger(colIndex) || !(values instanceof Vector))
			throw new IllegalArgumentError()
		if (colIndex < 1 || colIndex > this._width || values.length !== this._height)
			throw new IllegalArgumentError()
		for (let i = 1; i <= this._height; i++) {
			this._values[i - 1][colIndex - 1] = values.get(i)
		}
		return this
	}

	/**
	 * Terminates the building process. A new matrix containing the added values is returned.
	 * @returns {Matrix<V>} - The constructed matrix
	 */
	build() {
		if (this._built) throw new IllegalStateError()
		this._built = true
		Object.freeze(this)
		return new Matrix(this._height, this._width, this._values)
	}
}

/**
 * Objects of this class represent one-indexed row and zero-indexed column matrices of a fixed length. A zero-indexed
 * column matrix is like a one-indexed {@link Matrix}, except that their column indexing starts at zero instead of one.
 * @template V
 * @augments {Matrix<V>}
 */
export class ZeroIndexedColMatrix extends Matrix {

	/** @override */
	get(rowIndex, colIndex) {
		if (!Number.isInteger(rowIndex) || rowIndex < 1 || rowIndex > this._height )
			throw new IllegalArgumentError()
		if (!Number.isInteger(colIndex) || colIndex < 0 || colIndex >= this._width )
			throw new IllegalArgumentError()
		return this._values[rowIndex - 1][colIndex]
	}

	/**
	 * @override
	 * @returns {ZeroIndexedVector<V>}
	 */
	getRow(rowIndex) {
		if (!Number.isInteger(rowIndex) || rowIndex < 1 || rowIndex > this._height)
			throw new IllegalArgumentError()
		const row = this._values[rowIndex - 1]
		return new ZeroIndexedVector(row)
	}

	/** @override */
	getCol(colIndex) {
		if (!Number.isInteger(colIndex) || colIndex < 0 || colIndex >= this._width)
			throw new IllegalArgumentError()
		const col = this._values.map(x => x[colIndex])
		return new Vector(col)
	}

	/** @override */
	every(predicate) {
		return this._values.every((row, i) => row.every((item, j) => predicate(item, i+1, j)))
	}

	/** @override */
	transpose() {
		throw new UnsupportedOperationError()
	}
}

/**
 * This builder class is the main tool for constructing one-indexed row and zero-indexed column matrices from scratch.
 * The only difference compared to {@link Matrix.Builder} is that the column index starts at zero instead of one.
 * @template V
 * @augments {Matrix.Builder<V>}
 */
ZeroIndexedColMatrix.Builder = class extends Matrix.Builder {

	/**
	 * @override
	 * @returns {ZeroIndexedColMatrix.Builder<V>}
	 */
	set(rowIndex, colIndex, value) {
		if (this._built)
			throw new IllegalStateError()
		if (rowIndex < 1 || rowIndex > this._height)
			throw new IllegalArgumentError()
		if (colIndex < 0 || colIndex >= this._width)
			throw new IllegalArgumentError()
		this._values[rowIndex - 1][colIndex] = value
		return this
	}

	/**
	 * @override
	 * @param {V[] | Vector<ZeroIndexedVector<V>>} values
	 * @returns {ZeroIndexedColMatrix.Builder<V>}
	 */
	setValues(values) {
		if (this._built)
			throw new IllegalStateError()
		if (Array.isArray(values) && values.length === this._height * this._width) {
			// by 1D-Array
			for (let r = 0; r < this._height; r++) {
				for (let c = 0; c < this._width; c++) {
					this._values[r][c] = values[r * this._width + c]
				}
			}
		} else if (values instanceof Vector && values.length === this._height) {
			// by vector of zero-indexed vectors
			for (let r = 1; r <= this._height; r++) {
				if (!(values.get(r) instanceof ZeroIndexedVector) || values.get(r).length !== this._width)
					throw new IllegalArgumentError()
				for (let c = 0; c < this._width; c++) {
					this._values[r - 1][c] = values.get(r).get(c)
				}
			}
		} else {
			throw new IllegalArgumentError()
		}
		return this
	}

	/**
	 * @override
	 * @returns {ZeroIndexedColMatrix.Builder<V>}
	 */
	setColValues(colIndex, values) {
		if (this._built)
			throw new IllegalStateError()
		if (!Number.isInteger(colIndex) || !(values instanceof Vector))
			throw new IllegalArgumentError()
		if (colIndex < 0 || colIndex >= this._width || values.length !== this._height)
			throw new IllegalArgumentError()
		for (let i = 1; i <= this._height; i++) {
			this._values[i - 1][colIndex] = values.get(i)
		}
		return this
	}

	/**
	 * @override
	 * @returns {ZeroIndexedColMatrix<V>}
	 */
	build() {
		if (this._built) throw new IllegalStateError()
		this._built = true
		Object.freeze(this)
		return new ZeroIndexedColMatrix(this._height, this._width, this._values)
	}
}
