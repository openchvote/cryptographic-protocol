/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { IllegalArgumentError, IllegalStateError, UnsupportedOperationError } from "#util/Errors.js"

/** The byte size in bits */
export const BYTE_SIZE = 8
/** The maximal value of a byte */
export const MAX_BYTE = Math.pow(2, BYTE_SIZE) - 1

/**
 * Objects of this class represent immutable byte arrays of a fixed length n. Their elements are indexed from 0 to n-1.
 * The class provides methods for various operations on such byte arrays (skipping or truncating elements, bit-wise
 * logical operations, concatenation, etc.). To achieve immutability without excessive copying of the underlying
 * `Uint8Array`, instances of this class must override the `getByte` method. A {@link ByteArray.Builder} class is
 * provided for conveniently creating new instances.
 */
export class ByteArray {

	/**
	 * The length
	 * @private
	 * @type {number}
	 */
	_length

	/**
	 * Creates a new byte array of the given length.
	 * @param {number} length - The length of the byte array
	 */
	constructor(length) {
		this._length = length
	}

	/**
	 * The length of the byte array.
	 * @returns {number} - The length
	 */
	get length() {
		return this._length
	}

	/**
	 * Returns the byte at the given index. Instances and subclasses of this class MUST override this method.
	 * @param {number} index - The index
	 * @returns {number} - The byte at index `index`
	 *//* eslint-disable-next-line no-unused-vars */
	getByte(index) {
		throw new UnsupportedOperationError()
	}

	/**
	 * Returns a {@link Uint8Array} containing the bytes of the byte array in the same order.
	 * @returns {Uint8Array} - The resulting array
	 */
	toByteArray() {
		const bytes = new Uint8Array(this.length)
		this._toByteArray(bytes, 0)
		return bytes
	}

	/**
	 * Private helper method for transforming the byte array in a {@link Uint8Array}.
	 * @param {Uint8Array} bytes - The resulting array
	 * @param {number} offset - The offset inside the resulting byte array
	 */
	_toByteArray(bytes, offset) {
		for (let i = 0; i < this.length; i++)
			bytes[offset + i] = this.getByte(i)
	}

	/**
	 * Creates and returns a new byte array by skipping the first `k<=n` bytes of the given byte array of length `n`.
	 * The length of the returned byte array is `n-k`. An error is thrown for an invalid `k`.
	 * @param {number} k - The number of bytes to skip
	 * @returns {ByteArray} - The byte array obtained from skipping the first `k` bytes
	 */
	skip(k) {
		if (k < 0 || k > this.length)
			throw new IllegalArgumentError()
		if (k === 0) return this

		const orig = this
		const byteArray = new ByteArray(this.length - k)
		/** @override */
		byteArray.getByte = function(index) {
			// noinspection JSPotentiallyInvalidUsageOfClassThis
			if (index < 0 || index >= this.length)
				throw new IllegalArgumentError()
			return orig.getByte(k + index)
		}
		return byteArray
	}

	/**
	 * Creates and returns a new byte array, which contains the `k` first bytes of the given byte array. An
	 * error is thrown for an invalid `k`.
	 * @param {number} k - The length of the returned byte array
	 * @returns {ByteArray} - The byte array obtained from keeping the first `k` bytes
	 */
	truncate(k) {
		if (k < 0 || k > this.length)
			throw new IllegalArgumentError()
		if (k === this.length) return this

		const orig = this
		const byteArray = new ByteArray(k)
		/** @override */
		byteArray.getByte = function(index) {
			// noinspection JSPotentiallyInvalidUsageOfClassThis
			if (index < 0 || index >= this.length)
				throw new IllegalArgumentError()
			return orig.getByte(index)
		}
		return byteArray
	}

	/**
	 * Returns the concatenation of the given byte array with another byte array.
	 * @param {ByteArray} byteArray2 - The other byte array
	 * @returns {ByteArray} - The concatenated byte array
	 */
	concatenate(byteArray2) {
		if (!(byteArray2 instanceof ByteArray))
			throw IllegalArgumentError()
		const byteArray1 = this
		if (byteArray1.length === 0)
			return byteArray2
		if (byteArray2.length === 0)
			return byteArray1

		const byteArray = new ByteArray(byteArray1.length + byteArray2.length)
		/** @override */
		byteArray.getByte = function(index) {
			if (index < byteArray1.length)
				return byteArray1.getByte(index)
			else
				return byteArray2.getByte(index - byteArray1.length)
		}
		/** @override */
		byteArray._toByteArray = function(bytes, offset) {
			byteArray1._toByteArray(bytes, offset)
			byteArray2._toByteArray(bytes, offset + byteArray1.length)
		}

		return byteArray
	}

	/**
	 * Creates and returns a new byte array with the same bytes, except for the byte at the specified index, which
	 * receives a new value. An error is thrown if the specified index is invalid. The new byte is given as an
	 * integer value.
	 * @param {number} byteIndex - The index of the changed byte
	 * @param {number} value - The new byte given as an integer value
	 * @returns {ByteArray} - The new byte array with the byte changed at the specified index
	 */
	setByte(byteIndex, value) {
		if (byteIndex < 0 || byteIndex >= this.length)
			throw new IllegalArgumentError()
		if (value < 0 || value > MAX_BYTE)
			throw new IllegalArgumentError()

		const orig = this
		const byteArray = new ByteArray(this.length)
		/** @override */
		byteArray.getByte = function(index) {
			return index === byteIndex ? value : orig.getByte(index)
		}
		/** @override */
		byteArray._toByteArray = function(bytes, offset) {
			orig._toByteArray(bytes, offset)
			bytes[offset + byteIndex] = value
		}
		return byteArray
	}

	/**
	 * Returns the byte array obtained from applying the logical XOR-operator bit-wise to all bytes of the two byte
	 * arrays. An error is thrown if their lengths are incompatible.
	 * @param {ByteArray} other - The other byte array
	 * @returns {ByteArray} - The result from applying the XOR-operator
	 */
	xor(other) {
		if (!(other instanceof ByteArray))
			throw new IllegalArgumentError()
		if (this.length !== other.length)
			throw new IllegalArgumentError()

		const orig = this
		const byteArray = new ByteArray(this.length)
		/** @override */
		byteArray.getByte = function(index) {
			return orig.getByte(index) ^ other.getByte(index)
		}
		return byteArray
	}

	/**
	 * Indicates whether some other object is equal to this byte array.
	 * @param {object} object - The other object
	 * @returns {boolean} - `true` if the given object is equal to this byte array, otherwise `false`
	 */
	equals(object) {
		if (this === object) return true
		if (object instanceof ByteArray)
			if (this.length === object.length) {
				for (let i = 0; i < this.length; i++)
					if (this.getByte(i) !== object.getByte(i)) return false
				return true
			}
		return false
	}

	/**
	 * Computes the concatenation of multiple byte arrays in the given order.
	 * @param {...ByteArray} byteArrays - The byte arrays to concatenate
	 * @returns {ByteArray} - The concatenation of the given byte arrays
	 */
	static concatenate(...byteArrays) {
		return byteArrays.reduce((result, byteArray) => result.concatenate(byteArray), EMPTY)
	}

	/**
	 * Returns the byte array obtained from applying the logical XOR-operator bit-wise to all bytes of the given byte
	 * arrays.
	 * @param {...ByteArray} byteArrays - The byte arrays
	 * @returns {ByteArray} - The result from applying the XOR-operation
	 */
	static xor(...byteArrays) {
		return byteArrays.reduce((result, byteArray) => result === EMPTY ? byteArray : result.xor(byteArray), EMPTY)
	}

	/**
	 * Creates a new byte array from the given bytes. Since `TypedArrays` cannot be frozen a copy of the given
	 * array is made and stored internally to achieve immutability.
	 * @param {Uint8Array} B - The given bytes
	 * @returns {ByteArray} - Tge newly created byte array
	 */
	static of(B) {
		if (!(B instanceof Uint8Array))
			throw new IllegalArgumentError()

		const bytes = B.slice(0) // Create a copy as TypedArrays cannot be frozen
		const byteArray = new ByteArray(bytes.length)

		/** @override */
		byteArray.getByte = function(index) {
			// noinspection JSPotentiallyInvalidUsageOfClassThis
			if (index < 0 || index >= this.length)
				throw new IllegalArgumentError()
			return bytes[index]
		}
		/** @override */
		byteArray._toByteArray = function(destBytes, offset) {
			destBytes.set(bytes, offset)
		}
		Object.freeze(byteArray)
		return byteArray
	}
}

/**
 * This class allows building new byte arrays of a given length. Initially, all bytes of the byte array are set to 0.
 * The bytes can then be set in arbitrary order using `setByte`. When the building process is over, the construction
 * of the byte array can be finalized using `build`. While the resulting byte array offers read-only access, the builder
 * offers write-only access.
 */
ByteArray.Builder = class {
	_bytes
	_built

	/**
	 * Creates a new builder for a byte array of a given length.
	 * @param {number} length - The length of the byte array
	 */
	constructor(length) {
		if (length < 0)
			throw new IllegalArgumentError()
		this._bytes = new Uint8Array(length)
		this._built = false
	}

	/**
	 * Sets the byte at the specified index. The byte is given as an integer value. The builder object is returned to
	 * enable chaining.
	 * @param {number} index - The index
	 * @param {number} b - The byte as integer value to be set at the index
	 * @returns {ByteArray.Builder} - The builder object
	 */
	setByte(index, b) {
		if (this._built) throw new IllegalStateError()
		if (index < 0 || index >= this._bytes.length)
			throw new IllegalArgumentError()
		if (b < 0 || b > MAX_BYTE)
			throw new IllegalArgumentError()
		this._bytes[index] = b
		return this
	}

	/**
	 * Terminates the building process and returns the constructed byte array.
	 * @returns {ByteArray} - The constructed byte array
	 */
	build() {
		if (this._built) throw new IllegalStateError()
		this._built = true
		Object.freeze(this)
		return ByteArray.of(this._bytes)
	}
}

/** The empty byte array */
export const EMPTY = ByteArray.of(new Uint8Array([]))
