/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { Hashable } from "#util/Hashable.js"
import { IllegalArgumentError, IllegalStateError } from "#util/Errors.js"

/**
 * Objects of this class represent one-indexed vectors of a fixed length. The class can be instantiated by passing
 * the vector's values as an array to the constructor. Additionally, a {@link Vector.Builder} is provided for creating
 * new instances incrementally. Instances of this class are immutable.
 * @template V
 */
export class Vector extends Hashable {

	/**
	 * Zero-based array holding the values
	 * @private
	 * @type {V[]}
	 */
	_values

	/**
	 * Creates a new vector for the given values. It is the caller's responsibility to pass a copy of the original
	 * values if the underlying array must remain mutable.
	 * @param {V[]} values - The values of the vector
	 */
	constructor(values) {
		super()
		this._values = values
		Object.freeze(this._values)
		Object.freeze(this)
	}

	/**
	 * Iterates over the values of the vector.
	 * @yields {V}
	 */
	* [Symbol.iterator]() {
		yield* this._values
	}

	/**
	 * The length of the vector
	 * @returns {number} - The length
	 */
	get length() {
		return this._values.length
	}

	/**
	 * The values of the vector as array. The returned array is immuatble (frozen).
	 * @returns {V[]} - The values
	 */
	get values() {
		return this._values
	}

	/**
	 * Returns the value of the vector at the given index. If the index is invalid, an error is thrown.
	 * @param {number} index - The index
	 * @returns {V} - The value at the given index
	 */
	get(index) {
		if (!Number.isInteger(index) || index < 1 || index > this.length)
			throw new IllegalArgumentError()
		return this._values[index - 1]
	}

	/**
	 * Returns `true` if the given object is equal to this one.
	 * @param {*} object - The object to compare
	 * @returns {boolean} - `true` if the two objects are equal, `false` otherwise
	 */
	equals(object) {
		return (object instanceof Vector) &&
			this.length === object.length &&
			this.every((val, index) => val === object.get(index))
	}

	/**
	 * Creates a new vector by applying a function to each value of the vector. Depending on the specified function, the
	 * type `W` of the returned vector may be different from the type `V` of the original vector.
	 * @template W
	 * @param {function(V): W} func - The function that maps the values of the vector
	 * @returns {Vector<W>} - A vector containing all mapped values
	 */
	map(func) {
		return new Vector(this._values.map(func))
	}

	/**
	 * Checks whether at least one item of the vector satisfies the given predicate.
	 * @param {function(V): boolean} predicate - The predicate function which is called for the items of the vector
	 * @returns {boolean} - `true` if at least one item satisfies the predicate, `false` otherwise
	 */
	any(predicate) {
		return this._values.reduce((c, v) => c || predicate(v), false)
	}

	/**
	 * Counts the number of items satisfying the given predicate.
	 * @param {function(V): boolean} predicate - The predicate function which is called for the items of the vector
	 * @returns {number} - The number of items satisfying the predicate
	 */
	count(predicate) {
		return this._values.reduce((c, v) => c + !!predicate(v), 0)
	}

	/**
	 * Tests whether all items of the vector satisfy the given predicate. The predicate function is called with the item
	 * and the index of the item.
	 * @param {function(V, number): boolean} predicate - The predicate function
	 * @returns {boolean} - `true` if all items of the vector satisfy the predicate, `false` otherwise
	 */
	every(predicate) {
		return this._values.every((v, i) => predicate(v, i + 1))
	}

	/**
	 * Wrapper function for {@link Array.reduce}.
	 * @template T
	 * @param {function(T, V, number, V[]): T} callbackFn
	 * @param {T} initialValue
	 * @returns {T}
	 */
	reduce(callbackFn, initialValue) {
		return this._values.reduce(callbackFn, initialValue)
	}

	/**
	 * Creates a new vector by selecting the values specified by the given {@code indexVector}. The length of the
	 * resulting vector is therefore equal to the length of the index vector, and indexing starts at 1.
	 * @param {Vector<number>} indexVector - The given index vector
	 * @returns {Vector} A new vector containing the selected values
	 */
	select(indexVector) {
		if (!(indexVector instanceof Vector))
			throw IllegalArgumentError()
		const selections = []
		indexVector._values.forEach(index => selections.push(this.get(index)))
		return new Vector(selections)
	}

	/**
	 * Computes the Hadamard product `this ○ other` of two int vectors of equal length. The resulting vector is
	 * obtained through element-wise multiplication. An exception is thrown if the vectors are not equally long.
	 * @param {Vector<number>} other The other int vector
	 * @returns {Vector<number>} The vector product of the two vectors
	 */
	times(other) {
		if (!(other instanceof Vector) || this.length !== other.length)
			throw IllegalArgumentError()
		return new Vector(this._values.map((v, i) => v * other._values[i]))
	}

	/**
	 * Returns a new vector that is identical to the given vector, except for the given new value which is placed at
	 * the given index.
	 * @param {number} index - The index for the new value
	 * @param {V} value - The new value
	 * @returns {Vector} - The newly created vector
	 */
	replace(index, value) {
		if (!Number.isInteger(index) || index < 1 || index > this.length)
			throw new IllegalArgumentError()
		return new Vector(this._values.map((v, i) => i + 1 === index ? value : v))
	}

	/**
	 * Returns `true` if the values of the vector are sorted. That is, if `vector.get(i) < vector.get(i+1)` for
	 * `1 <= i < vector.length`. This method is intended for vectors with numeric values.
	 * @returns {boolean} - `true` if the vector is sorted, `false` otherwise
	 */
	isSorted() {
		for (let i = 1; i < this._values.length; i++) {
			if (this._values[i-1] >= this._values[i]) return false
		}
		return true
	}

	/** @override */
	toString() {
		return "[" + this._values.map(o => o.toString()).join(",") + "]"
	}
}

/**
 * This builder class is the main tool for constructing one-indexed vectors from scratch. Values can either be added
 * incrementally or set in arbitrary order by index. The builder can be created for an initial length or for some
 * initial values. In both cases, the vector may grow when new values are added.
 * @template V
 */
Vector.Builder = class {
	_built
	_values // Zero-based array

	/**
	 * Constructs a new vector builder.
	 * @param {number | V[]} [v] - The initial length or the initial values
	 */
	constructor(v) {
		this._built = false
		if (!v) this._values = []
		else if (Number.isInteger(v) && v >= 0) this._values = new Array(v)
		else if (Array.isArray(v)) this._values = [...v]
		else throw new IllegalArgumentError()
	}

	/**
	 * Sets the given value at the given index.
	 * @param {number} index - The index of the value to be added
	 * @param {V} value - The value to be added
	 * @returns {Vector.Builder} - The vector builder itself
	 */
	set(index, value) {
		if (this._built) throw new IllegalStateError()
		if (index < 1) throw new IllegalArgumentError()
		this._values[index - 1] = value
		return this
	}

	/**
	 * Adds the given value to the end of the vector.
	 * @param {V} value - The value to be added
	 * @returns {Vector.Builder} - The vector builder itself
	 */
	add(value) {
		if (this._built) throw new IllegalStateError()
		this._values.push(value)
		return this
	}

	/**
	 * Terminates the building process. A new vector containing the added values is returned.
	 * @returns {Vector<V>} - The constructed vector
	 */
	build() {
		if (this._built) throw new IllegalStateError()
		this._built = true
		Object.freeze(this)
		return new Vector(this._values)
	}
}

/**
 * Objects of this class represent zero-indexed vectors of a fixed length. Zero-indexed vectors are like
 * one-indexed {@link Vector}s, except that their indexing starts at zero instead of one.
 * @template V
 * @augments {Vector<V>}
 */
export class ZeroIndexedVector extends Vector {

	/** @override */
	get(index) {
		if (!Number.isInteger(index) || index < 0 || index >= this.length)
			throw new IllegalArgumentError()
		return this._values[index]
	}

	/**
	 * Returns a new zero-indexd vector that is identical to the given vector, except for the given new value which is
	 * placed at the given index.
	 * @param {number} index - The index for the new value
	 * @param {V} value - The new value
	 * @returns {ZeroIndexedVector<V>} - The newly created vector
	 */
	replace(index, value) {
		if (!Number.isInteger(index) || index < 0 || index >= this.length)
			throw new IllegalArgumentError()
		return new ZeroIndexedVector(this._values.map((v, i) => i === index ? value : v))
	}

	/**
	 * @override
	 * @returns {ZeroIndexedVector<V>}
	 */
	map(func) {
		return new ZeroIndexedVector(this._values.map(func))
	}

	/** @override */
	every(func) {
		return this._values.every((v, i) => func(v, i))
	}

	/**
	 * Creates a new vector by selecting the values specified by the given {@code indexVector}. The length of the
	 * resulting vector is therefore equal to the length of the index vector, and indexing starts at 0.
	 * @param {Vector<number>} indexVector - The given index vector
	 * @returns {ZeroIndexedVector<V>} A new vector containing the selected values
	 */
	select(indexVector) {
		if (!(indexVector instanceof Vector))
			throw IllegalArgumentError()
		const selections = []
		indexVector._values.forEach(index => selections.push(this.get(index)))
		return new ZeroIndexedVector(selections)
	}
}

/**
 * This builder class is the main tool for constructing zero-indexed vectors from scratch. The only difference compared
 * to {@link Vector.Builder} is that the index starts at zero instead of one.
 * @template V
 * @augments {Vector.Builder<V>}
 */
ZeroIndexedVector.Builder = class extends Vector.Builder {

	/**
	 * @override
	 * @returns {ZeroIndexedVector.Builder<V>}
	 */
	set(index, value) {
		if (this._built) throw new IllegalStateError()
		if (index < 0) throw new IllegalArgumentError()
		this._values[index] = value
		return this
	}

	/**
	 * @override
	 * @returns {ZeroIndexedVector<V>}
	 */
	build() {
		if (this._built) throw new IllegalStateError()
		this._built = true
		Object.freeze(this)
		return new ZeroIndexedVector(this._values)
	}
}
