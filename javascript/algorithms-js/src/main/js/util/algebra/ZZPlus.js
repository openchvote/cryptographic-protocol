/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger, ONE, TWO } from "#util/math/BigInteger.js"
import { IllegalArgumentError } from "#util/Errors.js"
import { Set } from "#util/Set.js"
import { Vector } from "#util/sequence/Vector.js"
import { ZZ } from "./ZZ.js"

/**
 * This class represents the multiplicative group of absolute values modulo `p`, where `p=2q+1` is a safe prime such
 * that `q` is also prime. The elements of the group are represented by objects of type {@link BigInteger}. The group
 * is a finite {@link Set} with a size of `q`. Instances of this class are immutable.
 * @augments {Set<BigInteger>}
 */
export class ZZPlus extends Set {

	/**
	 * The group order
	 * @private
	 * @type {BigInteger}
	 */
	_q

	/**
	 * The corresponding ring of integers (used internally to perform the modular operations)
	 * @private
	 * @type {ZZ}
	 */
	_ZZ_p

	/**
	 * Creates the group of positive integers modulo `p`, where `p` is a safe prime of the form `p=2q+1` such
	 * that `q` is also prime.
	 * @param {BigInteger} p - The given safe prime modulo
	 */
	constructor(p) {
		if (!(p instanceof BigInteger))
			throw new IllegalArgumentError("p must be a BigInteger")
		super()
		this._q = p.subtract(ONE).divide(TWO)
		this._ZZ_p = new ZZ(p)
	}

	/**
	 * Maps a given integer into the multiplicative group of absolute values modulo `p`.
	 * @param {BigInteger} x - The given integer
	 * @returns {BigInteger} - The corresponding element of the group
	 */
	mapToGroup(x) {
		return this._abs(this._ZZ_p.mapToRing(x))
	}

	/** @override */
	contains(x) {
		return x instanceof BigInteger && x.signum() === 1 && x.compareTo(this._q) <= 0
	}

	/**
	 * Computes the absolute value of the modular product of two or three integers `x`, `y`, and optionally `z`.
	 * @param {BigInteger} x - The first integer
	 * @param {BigInteger} y - The second integer
	 * @param {BigInteger} [z] - The optional third integer
	 * @returns {BigInteger} - The absolute value of the modular product `x * y [* z] mod p`
	 */
	multiply(x, y, z) {
		return this._abs(this._ZZ_p.multiply(x, y, z))
	}

	/**
	 * Computes the absolute value of `x` to the power of `y` modulo `p`.
	 * @param {BigInteger} x - The base
	 * @param {BigInteger} y - The exponent
	 * @returns {BigInteger} - The absolute value of `x` to the power of `y` modulo `p`
	 */
	pow(x, y) {
		return this._abs(this._ZZ_p.pow(x, y))
	}

	/**
	 * Computes the absolute value of the multiplicative inverse of `x` modulo `p`.
	 * @param {BigInteger} x - The given integer
	 * @returns {BigInteger} - The absolute value of the multiplicative inverse of `x` modulo `p`
	 */
	invert(x) {
		return this._abs(this._ZZ_p.invert(x))
	}

	/**
	 * Computes the absolute value of the modular division of two integers `x` and `@code y`.
	 * @param {BigInteger} x - The first integer
	 * @param {BigInteger} y - The second integer
	 * @returns {BigInteger} - The absolute value of the modular division `x / y mod p`
	 */
	divide(x, y) {
		return this._abs(this._ZZ_p.divide(x, y))
	}

	/**
	 * Computes the absolute value of the modular product of a vector of integers. Returns 1 if the vector is empty.
	 * @param {Vector<BigInteger>} bold_x - The vector of integers
	 * @returns {BigInteger} - The absolute value of the modular product of the given vector of integers
	 */
	prod(bold_x) {
		return this._abs(this._ZZ_p.prod(bold_x))
	}

	/**
	 * Private helper method to compute the absolute value for inputs `0 <= x < p`.
	 * @param {BigInteger} x - The given integer
	 * @returns {BigInteger} - The absolute value
	 */
	_abs(x) {
		if (x.compareTo(this._q) <= 0) return x
		else return this._ZZ_p.minus(x)
	}
}
