/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger, ONE } from "#util/math/BigInteger.js"
import { IllegalArgumentError } from "#util/Errors.js"
import { Set } from "#util/Set.js"
import { Vector } from "#util/sequence/Vector.js"
import { ZZ } from "./ZZ.js"
import { divides } from "#util/math/Math.js"


/**
 * This class represents a `q`-order subgroup of integers modulo `p`, where `p` and `q` are distinct primes
 * of the form `p = kq + 1` for `k >= 2`. The elements of the group are represented by objects of type
 * {@link BigInteger}. The group is a finite {@link Set} with a size of `q`. Instances of this class are
 * immutable.
 * @augments {Set<BigInteger>}
 */
export class GG extends Set {

	/**
	 * The modulus
	 * @private
	 * @type {BigInteger}
	 */
	_p

	/**
	 * The group order
	 * @private
	 * @type {BigInteger}
	 */
	_q

	/**
	 * The co-factor
	 * @private
	 * @type {BigInteger}
	 */
	_k

	/**
	 * The corresponding ring of integers (used internally to perform the modular operations)
	 * @private
	 * @type {ZZ}
	 */
	_ZZ_p

	/**
	 * Creates the `q`-order subgroup of integers modulo `p`, where `p` and `q` are distinct primes of the form
	 * `p = kq + 1` for `k >= 2`.
	 * @param {BigInteger} p - The given prime modulo
	 * @param {BigInteger} q - The prime order of the subgroup
	 */
	constructor(p, q) {
		if (!(p instanceof BigInteger) || !(q instanceof BigInteger) || p.compareTo(q) <= 0 || !divides(q, p.subtract(ONE)))
			throw new IllegalArgumentError("p and q must be distinct BigInteger and q must divide p-1")
		super()
		this._p = p
		this._q = q
		this._k = this._p.subtract(ONE).divide(this._q)
		this._ZZ_p = new ZZ(p)
		Object.freeze(this)
	}

	/** @override */
	contains(x) {
		return x instanceof BigInteger && x.signum() > 0 && x.compareTo(this._p) < 0 && this._ZZ_p.pow(x, this._q).equals(ONE)
	}


	/**
	 * Computes the modular product of two integers `x` and `y`.
	 * @param {BigInteger} x - The first integer
	 * @param {BigInteger} y - The second integer
	 * @returns {BigInteger} - The modular product `x * y mod n`
	 */
	multiply(x, y) {
		return this._ZZ_p.multiply(x, y)
	}

	/**
	 * Computes {@code x} to the power of `y` modulo `p`.
	 * @param {BigInteger} x - The base
	 * @param {BigInteger} y - The exponent
	 * @returns {BigInteger} - The modular exponentiation `x^y modulo p`
	 */
	pow(x, y) {
		return this._ZZ_p.pow(x, y)
	}

	/**
	 * Computes the multiplicative inverse of `x` modulo `p`.
	 * @param {BigInteger} x - The given integer
	 * @returns {BigInteger} - The multiplicative inverse of `x` modulo `p`
	 */
	invert(x) {
		return this._ZZ_p.invert(x)
	}

	/**
	 * Computes the modular division of two integers `x` and `y`.
	 * @param {BigInteger} x - The first integer
	 * @param {BigInteger} y - The second integer
	 * @returns {BigInteger} - The modular division `x / y mod n`
	 */
	divide(x, y) {
		return this._ZZ_p.divide(x, y)
	}

	/**
	 * Computes the modular product of a vector of integers. Returns 1 if the vector is empty.
	 * @param {Vector<BigInteger>} bold_x - The vector of integers
	 * @returns {BigInteger} - The modular product of the given vector of integers
	 */
	prod(bold_x) {
		return this._ZZ_p.prod(bold_x)
	}
}
