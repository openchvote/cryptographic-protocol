/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger, ONE, TWO, ZERO } from "#util/math/BigInteger.js"
import { IllegalArgumentError } from "#util/Errors.js"
import { Set } from "#util/Set.js"
import { Vector } from "#util/sequence/Vector.js"


/**
 * This class represents the ring of integers modulo `n`. The ring offers two binary operation (addition and
 * multiplication) from which other operations (subtraction, division, etc.) can be derived. The elements of the ring
 * are represented by objects of type {@link BigInteger}. The ring is a finite {@link Set} with a size of `n`.
 * Instances of this class are immutable.
 * @augments {Set<BigInteger>}
 */
export class ZZ extends Set {

	/**
	 * The modulus
	 * @private
	 * @type {BigInteger}
	 */
	_n

	/**
	 * Creates the ring `{0,...,n-1}` of integers modulo `n`.
	 * @param {BigInteger} n - The modulus
	 */
	constructor(n) {
		if (!(n instanceof BigInteger) || n.signum() <= 0)
			throw new IllegalArgumentError("n must be a positive BigInteger")
		super()
		this._n = n
		Object.freeze(this)
	}

	/**
	 * Returns the ring `{0,...,n-1}` of integers modulo `n`.
	 * @param {BigInteger} n - The modulus
	 * @returns {ZZ} - The ring of integers modulo `n`
	 */
	static of(n) {
		return new ZZ(n)
	}

	/**
	 * Returns the set `{0,...,2^e-1}` of integers modulo `2^e`.
	 * @param {number} e - The exponent of the modulo
	 * @returns {ZZ} - The ring of integers modulo `2^e`
	 */
	static ofExp(e) {
		if (!Number.isInteger(e) || e < 0)
			throw new IllegalArgumentError()
		return new ZZ(TWO.pow(e))
	}

	/**
	 * Maps a given integer into the ring of integers modulo `n`.
	 * @param {BigInteger} x - The given integer
	 * @returns {BigInteger} - The corresponding element of the ring
	 */
	mapToRing(x) {
		if (!(x instanceof BigInteger))
			throw new IllegalArgumentError()
		return x.mod(this._n)
	}


	/** @override */
	contains(x) {
		return x instanceof BigInteger && x.signum() >= 0 && x.compareTo(this._n) < 0
	}

	/**
	 * Computes the modular sum of two integers `x` and `y`.
	 * @param {BigInteger} x - The first integer
	 * @param {BigInteger} y - The second integer
	 * @returns {BigInteger} - The modular sum `x + y mod n`
	 */
	add(x, y) {
		if (!(x instanceof BigInteger) || !(y instanceof BigInteger))
			throw new IllegalArgumentError()
		return x.add(y).mod(this._n)
	}

	/**
	 * Computes the modular sum of a vector of integers. Returns 0 if the vector is empty.
	 * @param {Vector<BigInteger>} bold_x - The vector of integers
	 * @returns {BigInteger} - The modular sum of the given vector of integers
	 */
	sum(bold_x) {
		if (!(bold_x instanceof Vector))
			throw new IllegalArgumentError()
		return bold_x.reduce((sum, x) => this.add(sum, x), ZERO)
	}

	/**
	 * Computes the additive inverse of `x` modulo `p`.
	 * @param {BigInteger} x - The given integer
	 * @returns {BigInteger} - The additive inverse of `x` modulo `p`
	 */
	minus(x) {
		if (!(x instanceof BigInteger))
			throw new IllegalArgumentError()
		return this._n.subtract(x).mod(this._n)
	}

	/**
	 * Computes the modular difference of two integers `x` and `y`.
	 * @param {BigInteger} x - The first integer
	 * @param {BigInteger} y - The second integer
	 * @returns {BigInteger} - The modular difference `x - y mod n`
	 */
	subtract(x, y) {
		if (!(x instanceof BigInteger) || !(y instanceof BigInteger))
			throw new IllegalArgumentError()
		return this.add(x, this.minus(y))
	}

	/**
	 * Computes the modular product of two or three integers.
	 * @param {BigInteger} x - The first integer
	 * @param {BigInteger} y - The second integer
	 * @param {BigInteger} [z] - The optional third integer
	 * @returns {BigInteger} - The modular product `x * y [* z] mod n`
	 */
	multiply(x, y, z) {
		if (!(x instanceof BigInteger) || !(y instanceof BigInteger) || (z && !(z instanceof BigInteger)))
			throw new IllegalArgumentError()
		const r = x.multiply(y).mod(this._n)
		return z ? r.multiply(z).mod(this._n) : r
	}

	/**
	 * Computes the modular product of a vector of integers. Returns 1 if the vector is empty.
	 * @param {Vector<BigInteger>} bold_x - The vector of integers
	 * @returns {BigInteger} - The modular product of the given vector of integers
	 */
	prod(bold_x) {
		if (!(bold_x instanceof Vector))
			throw new IllegalArgumentError()
		return bold_x.reduce((prod, x) => this.multiply(prod, x), ONE)
	}

	/**
	 * Computes `x` to the power of `y`.
	 * @param {BigInteger} x - The base
	 * @param {BigInteger} y - The exponent
	 * @returns {BigInteger} - The modular exponentiation `x^y mod n`
	 */
	pow(x, y) {
		if (!(x instanceof BigInteger) || !(y instanceof BigInteger))
			throw new IllegalArgumentError()
		return x.modPow(y, this._n)
	}

	/**
	 * Computes the multiplicative inverse of `x` modulo `n`.
	 * @param {BigInteger} x - The given integer
	 * @returns {BigInteger} - The multiplicative inverse of `x` modulo `n`
	 */
	invert(x) {
		if (!(x instanceof BigInteger))
			throw new IllegalArgumentError()
		return x.modInverse(this._n)
	}

	/**
	 * Computes the modular division of two integers `x` and `y`.
	 * @param {BigInteger} x - The first integer
	 * @param {BigInteger} y - The second integer
	 * @returns {BigInteger} - The modular division `x / y mod n`
	 */
	divide(x, y) {
		if (!(x instanceof BigInteger) || !(y instanceof BigInteger))
			throw new IllegalArgumentError()
		return x.modDivide(y, this._n)
	}
}

