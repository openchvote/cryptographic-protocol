/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BYTE_SIZE } from "#util/sequence/ByteArray.js"
import { BigInteger, ONE, ZERO } from "./BigInteger.js"
import { IllegalArgumentError } from "#util/Errors.js"
import { Vector } from "#util/sequence/Vector.js"

/**
 * Computes the number of necessary bytes to represent the absolute value of `x` in the ordinary binary representation.
 * @param {BigInteger} x - The given value
 * @returns {number} - The number of necessary bytes to represent the absolute value of `x`
 */
export function byteLength(x) {
	return ceilDiv(x.abs().bitLength(), BYTE_SIZE)
}

/**
 * Computes `2^y` for a given exponent `0<=y`. For an invalid input, the return value is not specified.
 * @param {number} y - The given exponent
 * @returns {BigInteger} - The `y`-th power of 2
 */
export function powerOfTwo(y) {
	return ONE.shiftLeft(y)
}

/**
 * Computes the fraction `x/y` rounded up to the next integer, where `x >= 0` and `y >= 1` are the given integer input
 * values. For invalid inputs, the return value is not specified.
 * @param {number} x - The numerator
 * @param {number} y - The denominator
 * @returns {number} - The fraction `x/y` rounded up to the next integer
 */
// noinspection SuspiciousNameCombination
export function ceilDiv(x, y) {
	if (divides(y, x)) {
		return x / y
	} else {
		return Math.floor(x / y) + 1
	}
}

/**
 * Computes the logarithm of `x` to the base `b` rounded up to the next integer.
 * @param {BigInteger} x - The given integer
 * @param {number} b - The base
 * @returns {number} - the logarithm of `x` to the base `b` rounded up to the next integer
 */
export function ceilLog(x, b) {
	let base = new BigInteger(b)
	let result = 0
	let z = ONE
	while (z.compareTo(x) < 0) {
		result++
		z = z.multiply(base)
	}
	return result
}

/**
 * Checks if `x > 0` is an integer divisor of `y >= 0`. For invalid inputs, the return value is not specified.
 * @param {BigInteger | number} x - The first integer
 * @param {BigInteger | number} y - The second integer
 * @returns {boolean} - `true` if `x` is an integer divisor of `y`, `false` otherwise
 */
export function divides(x, y) {
	if (x instanceof BigInteger && y instanceof BigInteger)
		return y.mod(x).equals(ZERO)
	else if (Number.isInteger(x) && Number.isInteger(y))
		return y % x === 0
	else throw new IllegalArgumentError()
}

/**
 * Computes the product of all integer values from a given vector of type `Vector<BigInteger>`. The behaviour of this
 * method is unspecified for vectors containing `null` values and non-`BigInteger` values.
 * @param {Vector<BigInteger>} values - The given vector of integer values
 * @returns {BigInteger} - The product of all integer values
 */
export function prod(values) {
	if (!(values instanceof Vector))
		throw new IllegalArgumentError()
	return values.reduce((prod, v) => prod.multiply(v), ONE)
}

/**
 * Computes the sum of all integer values from a given vector of type `Vector<number>`. The behaviour of this method
 * is unspecified for vectors containing `null` values and non-`number` values.
 * @param {Vector<number>} values - The given vector of integer values
 * @returns {number} - The sum of all integer values
 */
export function intSum(values) {
	if (!(values instanceof Vector))
		throw new IllegalArgumentError()
	return values.reduce((sum, v) => sum + v, 0)
}

/**
 * Computes the sum of products of all integer pairs from two input vectors of type `Vector<number>`. An error is thrown
 * for input vectors of different length. The behaviour of this method is unspecified for vectors containing `null`
 * values and non-`number` values.
 * @param {Vector<number>} vector1 - The first vector of integer values
 * @param {Vector<number>} vector2 - The second vector of integer values
 * @returns {number} - The sum of products of all integer pairs
 */
export function intSumProd(vector1, vector2) {
	if (!(vector1 instanceof Vector) || !(vector2 instanceof Vector))
		throw new IllegalArgumentError()
	if (vector1.length !== vector2.length)
		throw new IllegalArgumentError()
	let sum = 0
	for (let i = 1; i <= vector1.length; i++)
		sum += vector1.get(i) * vector2.get(i)
	return sum
}

/**
 * Tests whether all integer values from the first vector are smaller than the corresponding integer values from the
 * second vector at the same index. An error is thrown for input vectors of different length. The behaviour of this
 * method is unspecified for vectors containing `null` values and non-`number` values.
 * @param {Vector<number>} vector1 - The first vector of integer values
 * @param {Vector<number>} vector2 - The second vector of integer values
 * @returns {boolean} - `true` if every value from the first vector is smaller than the corresponding value from the
 * second vector at the same index, `false` otherwise
 */
export function intIsLess(vector1, vector2) {
	if (!(vector1 instanceof Vector) || !(vector2 instanceof Vector))
		throw new IllegalArgumentError()
	if (vector1.length !== vector2.length)
		throw new IllegalArgumentError()
	return vector1.every((v, i) => v < vector2.get(i))
}
