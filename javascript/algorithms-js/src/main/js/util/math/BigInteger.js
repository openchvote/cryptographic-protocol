/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { IllegalArgumentError } from "#util/Errors.js"
import { BigInteger as JsbnBigInteger } from "jsbn"

/**
 * Objects of this class represent integers of arbitrary size. This class does not implement the underlying arithmetic
 * itself but acts as a wrapper for the [jsbn]{@link https://www.npmjs.com/package/jsbn} library. It is designed to
 * provide an interface that allows the third-party big integer library to be easily replaced if needed. Instances of
 * this class are strictly immutable. However, for performance reasons, the internal jsbn big integer instance is not
 * frozen but is instead implemented as a private property to protect it from malicious access from outside the class.
 */
export class BigInteger {

	/**
	 * Internal jsbn BigInteger instance
	 * @private
	 * @type {JsbnBigInteger}
	 */
	#__bigint

	/**
	 * Creates a new BigInteger instance for the given value. The value can be of type `BigInteger`, `number`, or
	 * `string`. If a string representation of the value is provided, then the base of its encoding can be passed as
	 * second argument.
	 * @param {BigInteger | number | string } n - The value of the BigInteger
	 * @param {number} [base] - The optional base of the encoding (default is 10) in case a string representation of
	 * the value is given
	 */
	constructor(n, base = 10) {
		if (n instanceof BigInteger) {
			this.#__bigint = n.#__bigint
		} else if (Number.isInteger(n)) {
			this.#__bigint = new JsbnBigInteger(n.toString())
		} else if (typeof (n) === "string" || n instanceof String) {
			this.#__bigint = new JsbnBigInteger(n, base)
		} else if (n instanceof JsbnBigInteger) {
			// only for internal use
			this.#__bigint = n
		} else {
			throw new IllegalArgumentError(n)
		}

		// For performance reasons, the internal big integer representation is not frozen
		//Object.freeze(this.#__bigint)
		Object.freeze(this)
	}

	/**
	 * The number of bits used to represent the BigInteger.
	 * @returns {number} - The number of bits
	 */
	bitLength() {
		return this.#__bigint.bitLength()
	}

	/**
	 * The signum of the BigInteger: `-1` for negative, `0` for zero, or `1` for positive.
	 * @returns {number} - The signum
	 */
	signum() {
		return this.#__bigint.signum()
	}

	/**
	 * The absolute value of the BigInteger.
	 * @returns {BigInteger} - The absolute value.
	 */
	abs() {
		return new BigInteger(this.#__bigint.abs())
	}

	/**
	 * Returns a BigInteger whose value is `this + val`.
	 * @param {BigInteger} val - The value to add
	 * @returns {BigInteger} - The result of the addition
	 */
	add(val) {
		if (!(val instanceof BigInteger))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.add(val.#__bigint))
	}

	/**
	 * Returns a BigInteger whose value is `this - val`.
	 * @param {BigInteger} val - The value to subtract
	 * @returns {BigInteger} - The result of the subtraction
	 */
	subtract(val) {
		if (!(val instanceof BigInteger))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.subtract(val.#__bigint))
	}

	/**
	 * Returns a BigInteger whose value is `this * val`.
	 * @param {BigInteger} val - The value to be multiplied by this BigInteger
	 * @returns {BigInteger} - The result of the multiplication
	 */
	multiply(val) {
		if (!(val instanceof BigInteger))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.multiply(val.#__bigint))
	}

	/**
	 * Returns a BigInteger whose value is `this / val` (integer division).
	 * @param {BigInteger} val - The value by which this BigInteger is to be divided
	 * @returns {BigInteger} - The result of the division
	 */
	divide(val) {
		if (!(val instanceof BigInteger))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.divide(val.#__bigint))
	}

	/**
	 * Returns a BigInteger whose value is `this^e`.
	 * @param {number} e - The exponent to which this BigInteger is raised.
	 * @returns {BigInteger} - The result of the exponentiation.
	 */
	pow(e) {
		if (!Number.isInteger(e))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.pow(e))
	}

	/**
	 * Returns a BigInteger whose value is `this mod m`.
	 * @param {BigInteger} m - The modulus
	 * @returns {BigInteger} - The result of the modulo operation
	 */
	mod(m) {
		if (!(m instanceof BigInteger))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.mod(m.#__bigint))
	}

	/**
	 * Returns a BigInteger whose value is `this^e mod m`.
	 * @param {BigInteger} e - The exponent
	 * @param {BigInteger} m - The modulus
	 * @returns {BigInteger} - The result of the modular exponentiation
	 */
	modPow(e, m) {
		if (!(e instanceof BigInteger) || !(m instanceof BigInteger))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.modPow(e.#__bigint, m.#__bigint))
	}

	/**
	 * Returns a BigInteger whose value is `this^-1 mod m`.
	 * @param {BigInteger} m - The modulus
	 * @returns {BigInteger} - The modular inverse
	 */
	modInverse(m) {
		if (!(m instanceof BigInteger))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.modInverse(m.#__bigint))
	}

	/**
	 * Returns a BigInteger whose value is `this / d mod m`.
	 * @param {BigInteger} d - The divisor
	 * @param {BigInteger} m - The modulus
	 * @returns {BigInteger} - The result of the modular division
	 */
	modDivide(d, m) {
		if (!(d instanceof BigInteger) || !(m instanceof BigInteger))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.multiply(d.#__bigint.modInverse(m.#__bigint)).mod(m.#__bigint))
	}

	/**
	 * Returns `true` if this BigInteger is equal to `other`.
	 * @param {BigInteger} other - The value this BigInteger is tested for equality
	 * @returns {boolean} - `true` if this BigInteger is equal to `other`, `false` otherwise
	 */
	equals(other) {
		if (!(other instanceof BigInteger)) return false
		return this.#__bigint.equals(other.#__bigint)
	}

	/**
	 * Compares this BigInteger to `other` and returns a negative number if `this` is less than `other`, a positive
	 * number if `this` is greater than `other`, and `0` if `this` and `other` represents the same integer.
	 * @param {BigInteger} other - The value this BigInteger is compared to
	 * @returns {number} - The result of the comparison
	 */
	compareTo(other) {
		if (!(other instanceof BigInteger))
			throw new IllegalArgumentError()
		return this.#__bigint.compareTo(other.#__bigint)
	}

	/**
	 * Returns a BigInteger whose value is `this >> n`.
	 * @param {number} n - The shift distance in bits
	 * @returns {BigInteger} - The result of the right shift
	 */
	shiftRight(n) {
		if (!Number.isInteger(n))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.shiftRight(n))
	}

	/**
	 * Returns a BigInteger whose value is `this << n`.
	 * @param {number} n - The shift distance in bits
	 * @returns {BigInteger} - The result of the left shift
	 */
	shiftLeft(n) {
		if (!Number.isInteger(n))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.shiftLeft(n))
	}

	/**
	 * Returns the minimum of ths BigInteger and the given values.
	 * @param {...BigInteger} values - The values with wich the minimum is computed
	 * @returns {BigInteger} - The minimum value
	 */
	min(...values) {
		if(!values.every(value => value instanceof BigInteger))
			throw new IllegalArgumentError()
		return new BigInteger(this.#__bigint.min(...values.map(value => value.#__bigint)))
	}

	/**
	 * Returns this BigInteger as integer. If this BigInteger is greater than `Number.MAX_SAFE_INTEGER`, then the
	 * return value is not specified.
	 * @returns {number} - This BigInteger as integer
	 */
	intValue() {
		return this.#__bigint.intValue()
	}

	/**
	 * Returns the string representation of this BigInteger in the given radix.
	 * @param {number} [radix] - The option radix of the string representation (default is 10)
	 * @returns {string} - The string representation in the given radix
	 */
	toString(radix = 10) {
		return this.#__bigint.toString(radix)
	}
}

/** The BigInteger constant zero */
export const ZERO = new BigInteger(0)
/** The BigInteger constant one */
export const ONE = new BigInteger(1)
/** The BigInteger constant two */
export const TWO = new BigInteger(2)
/** The BigInteger constant three */
export const THREE = new BigInteger(3)
/** The BigInteger constant four */
export const FOUR = new BigInteger(4)
/** The BigInteger constant five */
export const FIVE = new BigInteger(5)
/** The BigInteger constant six */
export const SIX = new BigInteger(6)
/** The BigInteger constant seven */
export const SEVEN = new BigInteger(7)
/** The BigInteger constant eight */
export const EIGHT = new BigInteger(8)
/** The BigInteger constant nine */
export const NINE = new BigInteger(9)
