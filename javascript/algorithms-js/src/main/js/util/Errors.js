/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
/**
 * `PreconditionErrors` are thrown by algorithms if the passed arguments don't fulfill the required preconditions.
 */
export class PreconditionError extends Error {
	/** @param {string} [message] - Optional message clarifying the error */
	constructor(message) {
		super(message)
		this.name = "PreconditionError"
	}
}

/**
 * `IllegalStateErrors` are mainly thrown if an already built `Builder` object is altered.
 */
export class IllegalStateError extends Error {
	/** @param {string} [message] - Optional message clarifying the error */
	constructor(message) {
		super(message)
		this.name = "IllegalStateError"
	}
}

/**
 * The `IllegalArgumentError` is a generic error and is thrown by any function whenever certain arguments don't
 * fulfill their requirements.
 */
export class IllegalArgumentError extends Error {
	/** @param {string} [message] - Optional message clarifying the error */
	constructor(message) {
		super(message)
		this.name = "IllegalArgumentError"
	}
}

/**
 * `UnsupportedOperationErrors` are thrown if a subclass doesn't provide the expected functionality.
 */
export class UnsupportedOperationError extends Error {
	/** @param {string} [message] - Optional message clarifying the error */
	constructor(message) {
		super(message)
		this.name = "UnsupportedOperationError"
	}
}

/**
 * `AlgorithmErrors` are thrown by algorithms if an *abort* state is reached.
 */
export class AlgorithmError extends Error {
	/** @param {string} [message] - Optional message clarifying the error */
	constructor(message) {
		super(message)
		this.name = "AlgorithmError"
	}
}

/**
 * `DeserializationErrors` are thrown by deserializers if a given string cannot be deserialized.
 */
export class DeserializationError extends Error {
	/** @param {string} [message] - Optional message clarifying the error */
	constructor(message) {
		super(message)
		this.name = "DeserializationError"
	}
}

/**
 * `SerializationErrors` are thrown by the serializer if a given object cannot be serialized.
 */
export class SerializationError extends Error {
	/** @param {string} [message] - Optional message clarifying the error */
	constructor(message) {
		super(message)
		this.name = "SerializationError"
	}
}
