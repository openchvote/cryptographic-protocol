/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { ByteArray } from "./sequence/ByteArray.js"
import { IllegalArgumentError, IllegalStateError } from "./Errors.js"

/**
 * This is a base class for other classes providing a method for hashing the object into a byte array. A cache is
 * provided to store the hash value for performance reasons. Writing to and reading from the cache is not done
 * automatically. It's the responsibility of the developer to use the 'hash' property accordingly. Once set, the hash
 * value can no langer be changed. Currently, the cache supports only one value for a single hash algorithm.
 */
export class Hashable {

	/**
	 * The hash value as object to be settable and freezable after instantiation
	 * @private
	 * @typedef {object} HashValue
	 * @property {ByteArray} value - The actual hash value
	 */

	/**
	 * Internal cache for the hash value
	 * @private
	 * @type {HashValue}
	 */
	_hash

	/**
	 * The base constructor for hashable objects initializes only the internal cache.
	 */
	constructor() {
		this._hash = { value: null }
	}

	/**
	 * The cached hash value. May be `null` if the hash value hasn't been set yet.
	 * @returns {null|ByteArray} - The hash value or `null`
	 */
	get hash() {
		return this._hash.value
	}

	/**
	 * The hash value can be set only once. If already set, an {@link IllegalStateError} is thrown.
	 * @param {ByteArray} hash - The hash value
	 * @throws IllegalStateError
	 */
	set hash(hash) {
		if (this._hash.value) throw new IllegalStateError()
		if (!(hash instanceof ByteArray)) throw new IllegalArgumentError()
		this._hash.value = hash
		Object.freeze(this._hash)
	}
}
