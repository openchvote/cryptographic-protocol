/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import sha3 from "js-sha3"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { IllegalArgumentError } from "#util/Errors.js"

/**
 * Returns the hash value of the given input message. The used hash algorithm is SHA3-256. Internally,
 * [js-sha3]{@link https://www.npmjs.com/package/js-sha3} is used to perform the actual hash value computation.
 * @param {ByteArray} message - The message to hash
 * @returns {ByteArray} - The resulting hash value
 */
export function Hash(message) {
	if (!(message instanceof ByteArray))
		throw new IllegalArgumentError()
	const hashBuffer = sha3.sha3_256.array(message.toByteArray())
	return ByteArray.of(new Uint8Array(hashBuffer))
}
