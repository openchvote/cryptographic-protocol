/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { ByteArray } from "#util/sequence/ByteArray.js"
import { IllegalArgumentError } from "#util/Errors.js"


const KEY_LENGTH = 16 // number of bytes
const IV_LENGTH = 12  // number of bytes

/**
 * Returns the AES-GCM encryption of the given message (M) using the given key (K) and initialization vector (IV). The
 * key length and initialization vector length are fixed to 128 bits and 96 bits, respectively. Internally, the Web
 * Crypto API is used to perform the AES encryption.
 * @param {ByteArray} K - The secret key
 * @param {ByteArray} IV - The initialization vector
 * @param {ByteArray} M - The message to encrypt
 * @returns {ByteArray} - The ciphertext C
 */
export async function AESEncrypt(K, IV, M) {
	if (!(K instanceof ByteArray) || !(IV instanceof ByteArray) || !(M instanceof ByteArray))
		throw IllegalArgumentError()
	if (K.length !== KEY_LENGTH || IV.length !== IV_LENGTH)
		throw new IllegalArgumentError()

	const key = await self.crypto.subtle.importKey(
		"raw",
		K.toByteArray(),
		"AES-GCM",
		false,
		["encrypt"]
	)

	const C = await self.crypto.subtle.encrypt(
		{
			name: "AES-GCM",
			length: 128,
			iv: IV.toByteArray()
		},
		key,
		M.toByteArray()
	)

	return ByteArray.of(new Uint8Array(C))
}

/**
 * Returns the AES-GCM decryption of the given ciphertext (C) using the given key (K) and initialization vector (IV). The
 * key length and initialization vector length are fixed to 128 bits and 96 bits, respectively. Internally, the Web
 * Crypto API is used to perform the AES decryption.
 * @param {ByteArray} K - The secret key
 * @param {ByteArray} IV - The initialization vector
 * @param {ByteArray} C - The ciphertext to decrypt
 * @returns {ByteArray} - The message M
 */
export async function AESDecrypt(K, IV, C) {
	if (!(K instanceof ByteArray) || !(IV instanceof ByteArray) || !(C instanceof ByteArray))
		throw IllegalArgumentError()
	if (K.length !== KEY_LENGTH || IV.length !== IV_LENGTH)
		throw new IllegalArgumentError()

	const key = await self.crypto.subtle.importKey(
		"raw",
		K.toByteArray(),
		"AES-GCM",
		false,
		["decrypt"]
	)

	const M = await self.crypto.subtle.decrypt(
		{
			name: "AES-GCM",
			length: 128,
			iv: IV.toByteArray()
		},
		key,
		C.toByteArray()
	)

	return ByteArray.of(new Uint8Array(M))
}
