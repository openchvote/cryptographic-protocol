/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
// eslint-disable-next-line no-unused-vars
import { BigInteger } from "#util/math/BigInteger.js"
import { Hashable } from "./Hashable.js"
import { IllegalArgumentError } from "#util/Errors.js"

/**
 * The base class for all tuples. Although it is possible to instantiate this base class directly, in most cases it
 * is recommended to work with concrete subclasses, such as pairs, triples, etc. Objects of this class are immutable.
 */
export class Tuple extends Hashable {

	/**
	 * The values of the tuple
	 * @private
	 * @type {Array<*>}
	 */
	_values

	/**
	 * Constructs a new tuple for the given non-null values.
	 * @param {...*} args - The values of the tuple
	 */
	constructor(...args) {
		if(args.length === 0 || !(args.every(v => v !== null && v !== undefined)))
			throw new IllegalArgumentError("Null pointer")
		super()
		this._values = args
		Object.freeze(this._values)
		Object.freeze(this)
	}

	/**
	 * The size of the tuple.
	 * @returns {number} - The size
	 */
	get size() {
		return this._values.length
	}

	/**
	 * The values of the tuple. The returned array is immutable.
	 * @type {Array<*>}
	 */
	get values() {
		return this._values
	}

	/**
	 * Iterates over the values of the tuple.
	 * @yields {*}
	 */
	* [Symbol.iterator]() {
		yield* this._values
	}
}

/**
 * The singleton: a tuple with one value.
 * @template V1
 */
export class Singleton extends Tuple {

	/**
	 * Constructs a singleton for the given non-null value.
	 * @param {V1} first - The value
	 */
	constructor(first) {
		super(first)
	}

	/**
	 * @override
	 * @type {[V1]}
	 */
	get values() {
		return super.values
	}

	/**
	 * The value.
	 * @type {V1}
	 */
	get first() { return this._values[0] }
}

/**
 * The pair: a tuple with two values.
 * @template V1, V2
 */
export class Pair extends Tuple {

	/**
	 * Constructs a pair for the given non-null values.
	 * @param {V1} first - The first value
	 * @param {V2} second - The second value
	 */
	constructor(first, second) {
		super(first, second)
	}

	/**
	 * @override
	 * @type {[V1, V2]}
	 */
	get values() {
		return super.values
	}

	/**
	 * The first value.
	 * @type {V1}
	 */
	get first() { return this._values[0] }
	/**
	 * The second value.
	 * @type {V2}
	 */
	get second() { return this._values[1] }
}

/**
 * The triple: a tuple with three values.
 * @template V1, V2, V3
 */
export class Triple extends Tuple {

	/**
	 * Constructs a triple for the given non-null values.
	 * @param {V1} first - The first value
	 * @param {V2} second - The second value
	 * @param {V3} third - The third value
	 */
	constructor(first, second, third) {
		super(first, second, third)
	}

	/**
	 * @override
	 * @type {[V1, V2, V3]}
	 */
	get values() {
		return super.values
	}

	/**
	 * The frist value.
	 * @type {V1}
	 */
	get first() { return this._values[0] }
	/**
	 * The second value.
	 * @type {V2}
	 */
	get second() { return this._values[1] }
	/**
	 * The third value.
	 * @type {V3}
	 */
	get third() { return this._values[2] }
}

/**
 * The quadruple: a tuple with four values.
 * @template V1, V2, V3, V4
 */
export class Quadruple extends Tuple {

	/**
	 * Constructs a quadruple for the given non-null values.
	 * @param {V1} first - The first value
	 * @param {V2} second - The second value
	 * @param {V3} third - The third value
	 * @param {V4} fourth - The fourth value
	 */
	constructor(first, second, third, fourth) {
		super(first, second, third, fourth)
	}

	/**
	 * @override
	 * @type {[V1, V2, V3, V4]}
	 */
	get values() {
		return super.values
	}

	/**
	 * The first value.
	 * @type {V1}
	 */
	get first() { return this._values[0] }
	/**
	 * The second value.
	 * @type {V2}
	 */
	get second() { return this._values[1] }
	/**
	 * The third value.
	 * @type {V3}
	 */
	get third() { return this._values[2] }
	/**
	 * The fourth value.
	 * @type {V4}
	 */
	get fourth() { return this._values[3] }
}

/**
 * The quintuple: a tuple with five values.
 * @template V1, V2, V3, V4, V5
 */
export class Quintuple extends Tuple {

	/**
	 * Constructs a quintuple for the given non-null values.
	 * @param {V1} first - The first value
	 * @param {V2} second - The second value
	 * @param {V3} third - The third value
	 * @param {V4} fourth - The fourth value
	 * @param {V5} fifth - The fifth value
	 */
	constructor(first, second, third, fourth, fifth) {
		super(first, second, third, fourth, fifth)
	}

	/**
	 * @override
	 * @type {[V1, V2, V3, V4, V5]}
	 */
	get values() {
		return super.values
	}

	/**
	 * The first value.
	 * @type {V1}
	 */
	get first() { return this._values[0] }
	/**
	 * The second value.
	 * @type {V2}
	 */
	get second() { return this._values[1] }
	/**
	 * The third value.
	 * @type {V3}
	 */
	get third() { return this._values[2] }
	/**
	 * The fourth value.
	 * @type {V4}
	 */
	get fourth() { return this._values[3] }
	/**
	 * The fifth value.
	 * @type {V5}
	 */
	get fifth() { return this._values[4] }
}
