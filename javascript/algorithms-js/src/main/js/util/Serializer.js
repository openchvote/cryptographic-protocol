/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "./math/BigInteger.js"
import { ByteArray } from "./sequence/ByteArray.js"
import { Matrix, ZeroIndexedColMatrix } from "./sequence/Matrix.js"
import { SerializationError } from "./Errors.js"
import { StringBuilder } from "./StringBuilder.js"
import { Tuple } from "./Tuples.js"
import { Vector, ZeroIndexedVector } from "./sequence/Vector.js"

// Special serialization characters and strings
export const OPEN = "["
export const CLOSE = "]"
export const OPEN_REGEX = /^\[/
export const CLOSE_REGEX = /]$/
export const DELIMITER = ","
export const ESCAPE = "\\"
export const ESCAPED_OPEN = ESCAPE + OPEN
export const ESCAPED_CLOSE = ESCAPE + CLOSE

// Some constants
export const TRUE = "1"
export const FALSE = "0"
export const NULL = "*"

/**
 * Serializes arbitrary objects of various types into strings. The supported types are: {@link boolean}, {@link number},
 * {@link BigInteger}, {@link string}, {@link ByteArray}, {@link Array}, {@link Vector} {@link Matrix},
 * and {@link Tuple}. Nesting these types has no restrictions.
 * @param {*} object - The object to serialize
 * @returns {string} - The resulting serialization string
 * @throws {SerializationError}
 */
export function serialize(object) {
	const stringBuilder = new StringBuilder()
	_serialize(stringBuilder, object)
	return stringBuilder.toString()
}


/**
 * @private
 * @param {StringBuilder} stringBuilder
 * @param {*} object
 * @throws {SerializationError}
 */
function _serialize(stringBuilder, object) {
	if (object === null || object === undefined) serializeNull(stringBuilder)
	else if (typeof object === "number") serializeInteger(stringBuilder, object)
	else if (object instanceof BigInteger) serializeBigInteger(stringBuilder, object)
	else if (object instanceof ByteArray) serializeByteArray(stringBuilder, object)
	else if (typeof object === "string") serializeString(stringBuilder, object)
	else if (typeof object === "boolean") serializeBoolean(stringBuilder, object)
	else if (object instanceof Vector) serializeVector(stringBuilder, object)
	else if (object instanceof Matrix) serializeMatrix(stringBuilder, object)
	else if (object instanceof Array) serializeTuple(stringBuilder, object)
	else if (object instanceof Tuple) serializeTuple(stringBuilder, object.values)
	else throw new SerializationError(`Cannot serialize '${object.constructor}'`)
}

/**
 * @private
 * @param {StringBuilder} stringBuilder
 */
function serializeNull(stringBuilder) {
	stringBuilder.append(NULL)
}

/**
 * Special JS values such as NaN and Infinity cannot be serialized.
 * @private
 * @param {StringBuilder} stringBuilder
 * @param {number} obj
 */
function serializeInteger(stringBuilder, obj) {
	if (isNaN(obj) || obj === Infinity)
		throw new SerializationError()
	stringBuilder.append(obj.toString(10))
}

/**
 * @private
 * @param {StringBuilder} stringBuilder
 * @param {BigInteger} obj
 */
function serializeBigInteger(stringBuilder, obj) {
	stringBuilder.append(obj.toString(16))
}

/**
 * @private
 * @param {StringBuilder} stringBuilder
 * @param {ByteArray} obj
 */
function serializeByteArray(stringBuilder, obj) {
	stringBuilder.append(OPEN).append(Array.from(obj.toByteArray()).map(x => x.toString(16).padStart(2, "0").toUpperCase()).join("")).append(CLOSE)
}

/**
 * @private
 * @param {StringBuilder} stringBuilder
 * @param {boolean} obj
 */
function serializeBoolean(stringBuilder, obj) {
	stringBuilder.append(obj === true ? TRUE : FALSE)
}

/**
 * @private
 * @param {StringBuilder} stringBuilder
 * @param {string} obj
 */
function serializeString(stringBuilder, obj) {
	stringBuilder.append(OPEN).append(addEscaping(obj)).append(CLOSE)
}

/**
 * @private
 * @param {string} string
 * @returns {string}
 */
function addEscaping(string) {
	return string.replace(OPEN, ESCAPED_OPEN).replace(CLOSE, ESCAPED_CLOSE)
}

/**
 * @private
 * @param {StringBuilder} stringBuilder
 * @param {Vector<*>} obj
 */
function serializeVector(stringBuilder, obj) {
	let minIndex = obj instanceof ZeroIndexedVector ? 0 : 1
	let maxIndex = obj instanceof ZeroIndexedVector ? obj.length - 1 : obj.length
	stringBuilder.append(OPEN)
	serializeInteger(stringBuilder, minIndex)
	stringBuilder.append(DELIMITER)
	serializeInteger(stringBuilder, maxIndex)
	stringBuilder.append(DELIMITER)
	serializeIterable(stringBuilder, obj.values)
	stringBuilder.append(CLOSE)
}

/**
 * @private
 * @param {StringBuilder} stringBuilder
 * @param {Matrix<*>} obj
 */
function serializeMatrix(stringBuilder, obj) {
	const minRowIndex = 1
	const minColIndex = obj instanceof ZeroIndexedColMatrix ? 0 : 1
	const maxRowIndex = obj.height
	const maxColIndex = obj instanceof ZeroIndexedColMatrix ? obj.width - 1 : obj.width
	stringBuilder.append(OPEN)
	serializeInteger(stringBuilder, minRowIndex)
	stringBuilder.append(DELIMITER)
	serializeInteger(stringBuilder, maxRowIndex)
	stringBuilder.append(DELIMITER)
	serializeInteger(stringBuilder, minColIndex)
	stringBuilder.append(DELIMITER)
	serializeInteger(stringBuilder, maxColIndex)
	stringBuilder.append(DELIMITER)
	serializeIterable(stringBuilder, obj)
	stringBuilder.append(CLOSE)
}

/**
 * @private
 * @param {StringBuilder} stringBuilder
 * @param {Tuple<*>|Array<*>} obj
 */
function serializeTuple(stringBuilder, obj) {
	serializeIterable(stringBuilder, obj)
}

/**
 * @private
 * @param {StringBuilder} stringBuilder
 * @param {Iterable<*>} obj
 */
function serializeIterable(stringBuilder, obj) {
	let items = []
	for (let item of obj) {
		let sbIterable = new StringBuilder()
		_serialize(sbIterable, item)
		items.push(sbIterable.toString())
	}
	stringBuilder.append(OPEN).append(items.join(",")).append(CLOSE)
}

