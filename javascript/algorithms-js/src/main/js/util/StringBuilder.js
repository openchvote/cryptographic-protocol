/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { IllegalArgumentError } from "./Errors.js"

/**
 * Helper class for incrementally creating a string, similar to Java's StringBuilder class. The class implements
 * the chaining pattern.
 */
export class StringBuilder {

	/**
	 * An array holding the strings
	 * @private
	 * @type {string[]}
	 */
	_strings

	/**
	 * Constructs a new builder with the initial size `n`. An initial size is only required if strings are added
	 * using the `setAt` method.
	 * @param {number} [n] - The (optional) initial size
	 */
	constructor(n) {
		if (!n) this._strings = []
		else if (!Number.isInteger(n) || n < 0)
			throw new IllegalArgumentError()
		this._strings = new Array(n)
	}

	/**
	 * Appends a string. Strings are always appended at the end.
	 * @param {string} s - The string to append
	 * @returns {StringBuilder} - The builder instance
	 */
	append(s) {
		if (s) {
			this._strings.push(s)
		}
		return this
	}

	/**
	 * Sets a string at the zero-indexed position `i`.
	 * @param {number} i - The index
	 * @param {string} s - The string
	 * @returns {StringBuilder} - The build instance
	 */
	setAt(i, s) {
		if (i < 0 || i >= this._strings.length)
			throw new IllegalArgumentError()
		this._strings[i] = s
		return this
	}

	/**
	 * Returns the content of the builder as a single, concatenated string.
	 * @returns {string} - The content of the builder as string
	 */
	toString() {
		return this._strings.join("")
	}
}
