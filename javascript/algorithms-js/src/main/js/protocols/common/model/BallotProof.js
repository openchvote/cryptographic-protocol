/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { BigIntegerDeserializer, PairDeserializer, TripleDeserializer } from "#util/Deserializer.js"
import { Pair, Triple } from "#util/Tuples.js"

/**
 * Model class for ballot proofs.
 * @augments {Pair<BigInteger, Triple<BigInteger, BigInteger, BigInteger>>}
 */
export class BallotProof extends Pair {

	/**
	 * @param {BigInteger} c
	 * @param {Triple<BigInteger, BigInteger, BigInteger>} s
	 */
	constructor(c, s) {
		super(c, s)
	}

	/**
	 * @type {BigInteger}
	 */
	get c() {
		return this.first
	}

	/**
	 * @type {Triple<BigInteger, BigInteger, BigInteger>}
	 */
	get s() {
		return this.second
	}

	/**
	 * @override
	 * @returns {string}
	 */
	toString() {
		return `BallotProof(${this.c}, ${this.s})`
	}
}

/**
 * The deserializer class for ballot proofs.
 */
BallotProof.Deserializer = class extends PairDeserializer {
	constructor() {
		super(
			new BigIntegerDeserializer(),
			new TripleDeserializer(new BigIntegerDeserializer(), new BigIntegerDeserializer(), new BigIntegerDeserializer())
		)
	}
	/** @override */
	_deserialize(s) {
		return new BallotProof(...this._parse(s))
	}
}
