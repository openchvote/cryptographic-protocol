/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import {
	BigIntegerDeserializer, ByteArrayDeserializer,
	MatrixDeserializer,
	TripleDeserializer,
	VectorDeserializer
} from "#util/Deserializer.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { Triple } from "#util/Tuples.js"
import { Vector } from "#util/sequence/Vector.js"

/**
 * Model class for Responses.
 * @augments {Triple<Vector<BigInteger>, Matrix<ByteArray>, BigInteger>}
 */
export class Response extends Triple {

	/**
	 * @param {Vector<BigInteger>} bold_b
	 * @param {Matrix<ByteArray>} bold_C
	 * @param {BigInteger} d
	 */
	constructor(bold_b, bold_C, d) {
		super(bold_b, bold_C, d)
	}

	/**
	 * @type {Vector<BigInteger>}
	 */
	get bold_b() {
		return this.first
	}

	/**
	 * @type {Matrix<ByteArray>}
	 */
	get bold_C() {
		return this.second
	}

	/**
	 * @type {BigInteger}
	 */
	get d() {
		return this.third
	}

	/**
	 * @override
	 * @returns {string}
	 */
	toString() {
		return `Response(${this.bold_b}, ${this.bold_C}, ${this.d})`
	}
}

/**
 * The deserializer class for responses.
 */
Response.Deserializer = class extends TripleDeserializer {
	constructor() {
		super(
			new VectorDeserializer(new BigIntegerDeserializer()),
			new MatrixDeserializer(new ByteArrayDeserializer()),
			new BigIntegerDeserializer()
		)
	}
	/** @override */
	_deserialize(s) {
		return new Response(...this._parse(s))
	}
}
