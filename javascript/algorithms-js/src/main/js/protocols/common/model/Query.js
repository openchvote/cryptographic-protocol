/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { BigIntegerDeserializer, PairDeserializer } from "#util/Deserializer.js"
import { Pair } from "#util/Tuples.js"

/**
 * Model class for queries.
 * @augments {Pair<BigInteger, BigInteger>}
 */
export class Query extends Pair {

	/**
	 * @param {BigInteger} a_1
	 * @param {BigInteger} a_2
	 */
	constructor(a_1, a_2) {
		super(a_1, a_2)
	}

	/**
	 * @type {BigInteger}
	 */
	get a_1() {
		return this.first
	}

	/**
	 * @type {BigInteger}
	 */
	get a_2() {
		return this.second
	}

	/**
	 * @override
	 * @returns {string}
	 */
	toString() {
		return `Query(${this.a_1}, ${this.a_2})`
	}
}

/**
 * The deserializer class for queries.
 */
Query.Deserializer = class extends PairDeserializer {
	constructor() {
		super(
			new BigIntegerDeserializer(),
			new BigIntegerDeserializer()
		)
	}
	/** @override */
	_deserialize(s) {
		return new Query(...this._parse(s))
	}
}
