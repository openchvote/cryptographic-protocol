/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { BigIntegerDeserializer, TripleDeserializer } from "#util/Deserializer.js"
import { ConfirmationProof } from "./ConfirmationProof.js"
import { Triple } from "#util/Tuples.js"

/**
 * Model class for confirmations.
 * @augments {Triple<BigInteger, BigInteger, ConfirmationProof>}
 */
export class Confirmation extends Triple {

	/**
	 * @param {BigInteger} y_hat
	 * @param {BigInteger} z_hat
	 * @param {ConfirmationProof} pi
	 */
	constructor(y_hat, z_hat, pi) {
		super(y_hat, z_hat, pi)
	}

	/**
	 * @type {BigInteger}
	 */
	get y_hat() {
		return this.first
	}

	/**
	 * @type {BigInteger}
	 */
	get z_hat() {
		return this.second
	}

	/**
	 * @type {ConfirmationProof}
	 */
	get pi() {
		return this.third
	}

	/**
	 * @override
	 * @returns {string}
	 */
	toString() {
		return `Confirmation(${this.y_hat}, ${this.z_hat}, ${this.pi})`
	}
}

/**
 * The deserializer class for confirmations.
 */
Confirmation.Deserializer = class extends TripleDeserializer {
	constructor() {
		super(
			new BigIntegerDeserializer(),
			new BigIntegerDeserializer(),
			new ConfirmationProof.Deserializer()
		)
	}
	/** @override */
	_deserialize(s) {
		return new Confirmation(...this._parse(s))
	}
}
