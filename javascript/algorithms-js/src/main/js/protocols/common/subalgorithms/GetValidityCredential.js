/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger, ONE, ZERO } from "#util/math/BigInteger.js"
import { Pair } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"

/**
 * Algorithm 8.34
 * @param {Vector<Pair<BigInteger, BigInteger>>} bold_p
 * @param {SecurityParameters} securityParams
 * @returns {BigInteger} z
 */
export function GetValidityCredential(bold_p, securityParams) {

	// PREPARATION
	const {ZZ_q_hat} = securityParams
	const k = bold_p.length

	// ALGORITHM
	let z = ZERO
	for (let i = 1; i <= k; i++) {
		let n = ONE
		let d = ONE
		const x_i = bold_p.get(i).first
		const y_i = bold_p.get(i).second
		for (let j = 1; j <= k; j++) {
			if (i !== j) {
				const x_j = bold_p.get(j).first
				n = ZZ_q_hat.multiply(n, x_j)
				d = ZZ_q_hat.multiply(d, ZZ_q_hat.subtract(x_j, x_i))
			}
		}
		z = ZZ_q_hat.add(z, ZZ_q_hat.multiply(y_i, ZZ_q_hat.divide(n, d)))
	}
	return z
}
