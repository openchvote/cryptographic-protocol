/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { GenRandomInteger } from "#general/algorithms/GenRandomInteger.js"
import { Pair } from "#util/Tuples.js"
import { Query } from "#protocols/common/model/Query.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"

/**
 * Algorithm 8.23
 * @param {Vector<BigInteger>} bold_m
 * @param {BigInteger} pk
 * @param {SecurityParameters} securityParams
 * @returns {Pair<Vector<Query>, Vector<BigInteger>>} (bold_a, bold_r)
 */
export function GenQuery(bold_m, pk, securityParams) {

	// PREPARATION
	const {ZZPlus_p, q, g} = securityParams

	const k = bold_m.length
	const builder_bold_a = new Vector.Builder(k)
	const builder_bold_r = new Vector.Builder(k)

	// ALGORITHM
	for (let j = 1; j <= k; j++) {
		const m_j = bold_m.get(j)
		const r_j = GenRandomInteger(q)
		const a_j = new Query(ZZPlus_p.multiply(m_j, ZZPlus_p.pow(pk, r_j)), ZZPlus_p.pow(g, r_j))
		builder_bold_a.set(j, a_j)
		builder_bold_r.set(j, r_j)
	}
	const bold_a = builder_bold_a.build()
	const bold_r = builder_bold_r.build()
	return new Pair(bold_a, bold_r)
}
