/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { ConfirmationProof } from "#protocols/common/model/ConfirmationProof.js"
import { GenRandomInteger } from "#general/algorithms/GenRandomInteger.js"
import { GetChallenge } from "#general/algorithms/GetChallenge.js"
import { Pair } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"

/**
 * Algorithm 8.35
 * @param {BigInteger} y
 * @param {BigInteger} z
 * @param {BigInteger} y_hat
 * @param {BigInteger} z_hat
 * @param {SecurityParameters} securityParams
 * @returns {ConfirmationProof} pi
 */
export function GenConfirmationProof(y, z, y_hat, z_hat, securityParams) {

	// PREPARATION
	const {ZZ_q_hat, GG_q_hat, q_hat, g_hat} = securityParams

	// ALGORITHM
	const omega_1 = GenRandomInteger(q_hat)
	const omega_2 = GenRandomInteger(q_hat)
	const t_1 = GG_q_hat.pow(g_hat, omega_1)
	const t_2 = GG_q_hat.pow(g_hat, omega_2)
	const t = new Pair(t_1, t_2)
	const c = GetChallenge(new Pair(y_hat, z_hat), t, securityParams)
	const s_1 = ZZ_q_hat.subtract(omega_1, ZZ_q_hat.multiply(c, y))
	const s_2 = ZZ_q_hat.subtract(omega_2, ZZ_q_hat.multiply(c, z))
	const s = new Pair(s_1, s_2)
	const pi = new ConfirmationProof(c, s)
	return pi
}
