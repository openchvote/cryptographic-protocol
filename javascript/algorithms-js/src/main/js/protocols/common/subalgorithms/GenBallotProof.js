/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BallotProof } from "#protocols/common/model/BallotProof.js"
import { BigInteger, ONE } from "#util/math/BigInteger.js"
import { GenRandomInteger } from "#general/algorithms/GenRandomInteger.js"
import { GetChallenge } from "#general/algorithms/GetChallenge.js"
import { Query } from "#protocols/common/model/Query.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Triple } from "#util/Tuples.js"
import { Vector } from "#util/sequence/Vector.js"

/**
 * Algorithm 8.25
 * @param {BigInteger} x
 * @param {BigInteger} m
 * @param {BigInteger} r
 * @param {BigInteger} x_hat
 * @param {Vector<Query>} bold_a
 * @param {BigInteger} pk
 * @param {SecurityParameters} securityParams
 * @returns {BallotProof} (c, s)
 */
export function GenBallotProof(x, m, r, x_hat, bold_a, pk, securityParams) {

	// PREPARATION
	const {ZZ_q, ZZPlus_p, ZZ_q_hat, GG_q_hat, q, g, q_hat, g_hat} = securityParams

	// ALGORITHM
	const omega_1 = GenRandomInteger(q_hat)
	const omega_2 = GenRandomInteger(ONE, q)
	const omega_3 = GenRandomInteger(q)
	const t_1 = GG_q_hat.pow(g_hat, omega_1)
	const t_2 = ZZPlus_p.multiply(omega_2, ZZPlus_p.pow(pk, omega_3))
	const t_3 = ZZPlus_p.pow(g, omega_3)
	const t = new Triple(t_1, t_2, t_3)
	const y = new Triple(x_hat, bold_a, pk)
	const c = GetChallenge(y, t, securityParams)
	const s_1 = ZZ_q_hat.subtract(omega_1, ZZ_q_hat.multiply(c, x))
	const s_2 = ZZPlus_p.multiply(omega_2, ZZPlus_p.pow(m, ZZ_q.minus(c)))
	const s_3 = ZZ_q.subtract(omega_3, ZZ_q.multiply(c, r))
	const s = new Triple(s_1, s_2, s_3)
	const pi = new BallotProof(c, s)
	return pi
}
