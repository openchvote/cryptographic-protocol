/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { AlgorithmError } from "#util/Errors.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { ByteArrayToInteger } from "#general/algorithms/ByteArrayToInteger.js"
import { EMPTY } from "#util/sequence/ByteArray.js"
import { Point } from "#protocols/common/model/Point.js"
import { RecHash } from "#general/algorithms/RecHash.js"
import { Response } from "#protocols/common/model/Response.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { ceilDiv } from "#util/math/Math.js"

/**
 * Algorithm 8.30
 * @param {Response} beta
 * @param {Vector<number>} bold_s
 * @param {Vector<BigInteger>} bold_r
 * @param {SecurityParameters} securityParams
 * @returns {Vector<Point>} bold_p
 */
export function GetPointVector(beta, bold_s, bold_r, securityParams) {

	// PREPARATION
	const {ZZ_q, ZZPlus_p, q_hat, L_M, L} = securityParams

	const [bold_b, bold_C, d] = beta.values
	const k = bold_s.length
	const builder_bold_p = new Vector.Builder(k)

	// ALGORITHM
	const ell_M = ceilDiv(L_M, L)
	for (let j = 1; j <= k; j++) {
		const b_j = bold_b.get(j)
		const r_j = bold_r.get(j)
		const k_j = ZZPlus_p.multiply(b_j, ZZPlus_p.pow(d, ZZ_q.minus(r_j)))
		let K_j = EMPTY
		for (let c = 1; c <= ell_M; c++) {
			K_j = K_j.concatenate(RecHash(k_j, new BigInteger(c)))
		}
		K_j = K_j.truncate(L_M)
		const C_sj_j = bold_C.get(bold_s.get(j), j)
		if (C_sj_j == null) {
			throw new AlgorithmError("Invalid matrix entry")
		}
		const M_j = C_sj_j.xor(K_j)
		const x_j = ByteArrayToInteger(M_j.truncate(L_M / 2))
		const y_j = ByteArrayToInteger(M_j.skip(L_M / 2))
		if (x_j.compareTo(q_hat) >= 0 || y_j.compareTo(q_hat) >= 0) {
			throw new AlgorithmError("Point not in Z_{q_hat}^{2}")
		}
		const p_j = new Point(x_j, y_j)
		builder_bold_p.set(j, p_j)
	}
	const bold_p = builder_bold_p.build()
	return bold_p
}
