/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { AlgorithmError } from "#util/Errors.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { ByteArrayToInteger } from "#general/algorithms/ByteArrayToInteger.js"
import { EMPTY } from "#util/sequence/ByteArray.js"
import { Finalization } from "#protocols/common/model/Finalization.js"
import { GetPrimes } from "#general/algorithms/GetPrimes.js"
import { Point, ZERO_ZERO } from "#protocols/common/model/Point.js"
import { RecHash } from "#general/algorithms/RecHash.js"
import { Response } from "#protocols/common/model/Response.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector, ZeroIndexedVector } from "#util/sequence/Vector.js"
import { intSum } from "#util/math/Math.js"

/**
 * Algorithm 8.39
 * @param {Response} beta
 * @param {Finalization} delta
 * @param {Vector<number>} bold_s
 * @param {Vector<BigInteger>} bold_r
 * @param {Vector<number>} bold_n
 * @param {Vector<number>} bold_k
 * @param {Vector<number>} bold_e
 * @param {BigInteger} pk
 * @param {SecurityParameters} securityParams
 * @returns {Vector<Point>} bold_p
 */
export function GetAllPoints(beta, delta, bold_s, bold_r, bold_n, bold_k, bold_e, pk, securityParams) {

	// PREPARATION
	const {ZZ_q, ZZPlus_p, g, q_hat, L_M, L} = securityParams

	const n = intSum(bold_n)
	const t = bold_n.length
	const [bold_b, bold_C, d] = beta.values
	const [z_1, z_2] = delta.values
	const builder_bold_p = new Vector.Builder(n)

	// ALGORITHM
	const d_prime = ZZPlus_p.multiply(ZZPlus_p.pow(pk, z_1), ZZPlus_p.pow(g, z_2))
	if (!(d.equals(d_prime))) {
		throw new AlgorithmError("Values (z1, z2) incompatible with d")
	}
	const bold_primes = GetPrimes(n, securityParams)
	const ell_M = Math.ceil(L_M / L)
	for (let i = 1; i <= n; i++) {
		builder_bold_p.set(i, ZERO_ZERO)
	}
	let n_prime = 0
	let k_prime = 0

	for (let l = 1; l <= t; l++) {
		const n_l = bold_n.get(l)
		const k_l = bold_k.get(l)
		const e_l = bold_e.get(l)
		if (e_l === 1) {
			const builder_bold_p_prime = new ZeroIndexedVector.Builder()
			for (let i = n_prime + 1; i <= n_prime + n_l; i++) {
				const p_i = bold_primes.get(i)
				const p_prime_i = ZZPlus_p.pow(p_i, z_1)
				builder_bold_p_prime.set(i, p_prime_i)
			}
			const bold_p_prime = builder_bold_p_prime.build()
			const builder_bold_beta = new Vector.Builder()
			for (let j = k_prime + 1; j <= k_prime + k_l; j++) {
				const b_j = bold_b.get(j)
				const p_prime_s_j = bold_p_prime.get(bold_s.get(j))
				const r_j = bold_r.get(j)
				const beta_j = ZZPlus_p.multiply(b_j, ZZPlus_p.pow(d, ZZ_q.minus(r_j)), ZZPlus_p.invert(p_prime_s_j))
				builder_bold_beta.set(j, beta_j)
			}
			const bold_beta = builder_bold_beta.build()
			for (let i = n_prime + 1; i <= n_prime + n_l; i++) {
				const builder_bold_M_i = new Vector.Builder()
				for (let j = k_prime + 1; j <= k_prime + k_l; j++) {
					const p_prime_i = bold_p_prime.get(i)
					const beta_j = bold_beta.get(j)
					const k_ij = ZZPlus_p.multiply(p_prime_i, beta_j)
					let K_ij = EMPTY
					for (let c = 1; c <= ell_M; c++) {
						K_ij = K_ij.concatenate(RecHash(k_ij, c))
					}
					K_ij = K_ij.truncate(L_M)
					const C_ij = bold_C.get(i, j)
					const M_ij = C_ij.xor(K_ij)
					builder_bold_M_i.set(j, M_ij)
				}
				const bold_M_i = builder_bold_M_i.build()
				for (let j = k_prime + 2; j <= k_prime + k_l; j++)
					if (!bold_M_i.get(k_prime + 1).equals(bold_M_i.get(j)))
						throw new AlgorithmError("Incompatible messages")

				const x_i = ByteArrayToInteger(bold_M_i.get(k_prime + 1).truncate(L_M / 2))
				const y_i = ByteArrayToInteger(bold_M_i.get(k_prime + 1).skip(L_M / 2))
				if (x_i.compareTo(q_hat) >= 0 || y_i.compareTo(q_hat) >= 0) {
					throw new AlgorithmError("Point not in ZZ_{q_hat}^2")
				}
				const p_i = new Point(x_i, y_i)
				builder_bold_p.set(i, p_i)
			}
		}
		n_prime = n_prime + n_l
		k_prime = k_prime + e_l * k_l
	}
	const bold_p = builder_bold_p.build()
	return bold_p
}
