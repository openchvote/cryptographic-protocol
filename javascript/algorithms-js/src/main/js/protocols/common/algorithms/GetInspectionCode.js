/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { ByteArray } from "#util/sequence/ByteArray.js"
import { ByteArrayToString } from "#general/algorithms/ByteArrayToString.js"
import { IntSet, Set } from "#util/Set.js"
import { UsabilityParameters } from "#UsabilityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 8.57
 * @param {Vector<ByteArray>} bold_i
 * @param {UsabilityParameters} usabilityParams
 * @returns {string} IC
 */
export function GetInspectionCode(bold_i, usabilityParams) {

	// PREPARATION
	check.Type(UsabilityParameters, usabilityParams)
	const {A_PA, L_PA} = usabilityParams

	check(Set.Vector(Set.B(L_PA)).contains(bold_i))
	check(IntSet.NN_plus.contains(bold_i.length))

	// ALGORITHM
	const I = ByteArray.xor(...bold_i)
	const IC = ByteArrayToString(I, A_PA)
	return IC
}
