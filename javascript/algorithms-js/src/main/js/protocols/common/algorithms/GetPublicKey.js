/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { IntSet, Set } from "#util/Set.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 8.9
 * @param {Vector<BigInteger>} bold_pk
 * @param {SecurityParameters} securityParams
 * @returns {BigInteger} pk
 */
export function GetPublicKey(bold_pk, securityParams) {

	// PREPARATION
	check.Type(SecurityParameters, securityParams)
	const {ZZPlus_p} = securityParams

	check(Set.Vector(ZZPlus_p).contains(bold_pk))
	check(IntSet.NN_plus.contains(bold_pk.length))

	// ALGORITHM
	const pk = ZZPlus_p.prod(bold_pk)
	return pk
}
