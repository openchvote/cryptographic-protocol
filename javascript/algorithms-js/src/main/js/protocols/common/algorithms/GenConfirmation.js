/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { Confirmation } from "#protocols/common/model/Confirmation.js"
import { GenConfirmationProof } from "../subalgorithms/GenConfirmationProof.js"
import { GetValidityCredential } from "../subalgorithms/GetValidityCredential.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { Pair } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Set } from "#util/Set.js"
import { StringToInteger } from "#general/algorithms/StringToInteger.js"
import { UsabilityParameters } from "#UsabilityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 8.33
 * @param {string} Y
 * @param {Matrix<Pair<BigInteger, BigInteger>>} bold_P
 * @param {SecurityParameters} securityParams
 * @param {UsabilityParameters} usabilityParams
 * @returns {Confirmation} gamma
 */
export function GenConfirmation(Y, bold_P, securityParams, usabilityParams) {

	// PREPARATION
	check.Type([SecurityParameters, securityParams], [UsabilityParameters, usabilityParams])
	const {ZZ_q_hat, GG_q_hat, g_hat} = securityParams
	const {A_Y, ell_Y} = usabilityParams

	check(Set.String(A_Y, ell_Y).contains(Y))
	check(Set.Matrix(Set.Pair(ZZ_q_hat, ZZ_q_hat)).contains(bold_P))
	const s = bold_P.width
	const builder_bold_z = new Vector.Builder(s)

	// ALGORITHM
	const y = StringToInteger(Y, A_Y)
	const y_hat = GG_q_hat.pow(g_hat, y)
	for (let j = 1; j <= s; j++) {
		const bold_pj = bold_P.getCol(j)
		const z_j = GetValidityCredential(bold_pj, securityParams)
		builder_bold_z.set(j, z_j)
	}
	const bold_z = builder_bold_z.build()
	const z = ZZ_q_hat.sum(bold_z)
	const z_hat = GG_q_hat.pow(g_hat, z)
	const pi = GenConfirmationProof(y, z, y_hat, z_hat, securityParams)
	const gamma = new Confirmation(y_hat, z_hat, pi)
	return gamma
}
