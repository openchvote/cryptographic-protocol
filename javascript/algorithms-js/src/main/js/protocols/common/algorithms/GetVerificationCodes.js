/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { ByteArrayToString } from "#general/algorithms/ByteArrayToString.js"
import { IntSet, Set } from "#util/Set.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { Pair } from "#util/Tuples.js"
import { RecHash } from "#general/algorithms/RecHash.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { SetWatermark } from "#general/algorithms/SetWatermark.js"
import { UsabilityParameters } from "#UsabilityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 8.31
 * @param {Vector<number>} bold_s
 * @param {Matrix<Pair<BigInteger, BigInteger>>} bold_P
 * @param {SecurityParameters} securityParams
 * @param {UsabilityParameters} usabilityParams
 * @returns {Vector<string>} bold_vc
 */
export function GetVerificationCodes(bold_s, bold_P, securityParams, usabilityParams) {

	// PREPARATION
	check.Type([SecurityParameters, securityParams], [UsabilityParameters, usabilityParams])
	const {ZZ_q_hat} = securityParams
	const {A_V, L_V, n_max} = usabilityParams

	check(Set.Matrix(Set.Pair(ZZ_q_hat, ZZ_q_hat)).contains(bold_P))
	const [k, s] = bold_P.size
	check(IntSet.NN_plus.contains(s))
	check(Set.Vector(IntSet.NN_plus_n(n_max), k).contains(bold_s))
	check(bold_s.isSorted())

	const builder_bold_vc = new Vector.Builder(k)

	// ALGORITHM
	for (let i = 1; i <= k; i++) {
		const builder_V_ij = new Vector.Builder(s)
		for (let j = 1; j <= s; j++) {
			builder_V_ij.set(j, RecHash(bold_P.get(i, j)).truncate(L_V))
		}
		const V_ij = builder_V_ij.build()
		const V_i = SetWatermark(ByteArray.xor(...V_ij), bold_s.get(i) - 1, n_max)
		const VC_i = ByteArrayToString(V_i, A_V)
		builder_bold_vc.set(i, VC_i)
	}
	const bold_vc = builder_bold_vc.build()
	return bold_vc
}
