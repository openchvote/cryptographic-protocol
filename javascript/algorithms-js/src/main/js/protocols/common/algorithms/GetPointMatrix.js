/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { GetPointVector } from "../subalgorithms/GetPointVector.js"
import { IntSet, Set } from "#util/Set.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { Point } from "#protocols/common/model/Point.js"
import { Response } from "#protocols/common/model/Response.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { check } from "#util/Precondition.js"
import { intSum } from "#util/math/Math.js"

/**
 * Algorithm 8.26
 * @param {Vector<number>} bold_n
 * @param {Vector<Response>} bold_beta
 * @param {Vector<number>} bold_s
 * @param {Vector<BigInteger>} bold_r
 * @param {SecurityParameters} securityParams
 * @returns {Matrix<Point>} bold_P
 */
export function GetPointMatrix(bold_n, bold_beta, bold_s, bold_r, securityParams) {

	// PREPARATION
	check.Type(SecurityParameters, securityParams)
	const {ZZ_q, ZZPlus_p, L_M} = securityParams

	check.Type([Vector, bold_n], [Vector, bold_beta], [Vector, bold_r])
	const s = bold_beta.length
	const t = bold_n.length
	const k = bold_r.length
	const n = intSum(bold_n)
	check(Set.Vector(IntSet.NN_plus, t).contains(bold_n))
	check(Set.Vector(Set.Triple(Set.Vector(ZZPlus_p, k), Set.Matrix(Set.B(L_M).orNull(), n, k), ZZPlus_p), s).contains(bold_beta))
	check(Set.Vector(IntSet.NN_plus_n(n), k).contains(bold_s))
	check(Set.Vector(ZZ_q, k).contains(bold_r))
	check(bold_s.isSorted())

	const builder_bold_P = new Matrix.Builder(k, s)

	// ALGORITHM
	for (let j = 1; j <= s; j++) {
		const bold_p_j = GetPointVector(bold_beta.get(j), bold_s, bold_r, securityParams)
		builder_bold_P.setColValues(j, bold_p_j)
	}
	const bold_P = builder_bold_P.build()
	return bold_P
}
