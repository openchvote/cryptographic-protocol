/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { ByteArrayToString } from "#general/algorithms/ByteArrayToString.js"
import { Finalization } from "#protocols/common/model/Finalization.js"
import { GetAllPoints } from "../subalgorithms/GetAllPoints.js"
import { IntSet, Set } from "#util/Set.js"
import { RecHash } from "#general/algorithms/RecHash.js"
import { Response } from "#protocols/common/model/Response.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { UsabilityParameters } from "#UsabilityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { check } from "#util/Precondition.js"
import { intIsLess, intSum, intSumProd } from "#util/math/Math.js"

/**
 * Algorithm 8.38
 * @param {Vector<Response>} bold_beta
 * @param {Vector<Finalization>} bold_delta
 * @param {Vector<number>} bold_s
 * @param {Vector<BigInteger>} bold_r
 * @param {Vector<number>} bold_n
 * @param {Vector<number>} bold_k
 * @param {Vector<number>} bold_e
 * @param {BigInteger} pk
 * @param {SecurityParameters} securityParams
 * @param {UsabilityParameters} usabilityParams
 * @returns {string} PC
 */
export function GetParticipationCode(bold_beta, bold_delta, bold_s, bold_r, bold_n, bold_k, bold_e, pk, securityParams, usabilityParams) {

	// PREPARATION
	check.Type([SecurityParameters, securityParams], [UsabilityParameters, usabilityParams])
	const {ZZ_q, ZZPlus_p, L_M} = securityParams
	const {A_PA, L_PA} = usabilityParams

	const s = bold_beta.length
	check(IntSet.NN_plus.contains(s))
	const t = bold_k.length
	const [n, k] = bold_beta.get(1).bold_C.size
	check(Set.Vector(Set.Triple(Set.Vector(ZZPlus_p, k), Set.Matrix(Set.B(L_M).orNull(), n, k), ZZPlus_p), s).contains(bold_beta))
	check(Set.Vector(Set.Pair(ZZ_q, ZZ_q), s).contains(bold_delta))
	check(Set.Vector(IntSet.NN_plus_n(n), k).contains(bold_s))
	check(Set.Vector(ZZ_q, k).contains(bold_r))
	check(Set.Vector(IntSet.NN_plus, t).contains(bold_n))
	check(Set.Vector(IntSet.NN_plus, t).contains(bold_k))
	check(Set.Vector(IntSet.BB, t).contains(bold_e))
	check(ZZPlus_p.contains(pk))

	const builder_bold_P = new Vector.Builder(s)

	// CONSTRAINTS
	check(n === intSum(bold_n))
	check(k === intSumProd(bold_e, bold_k))
	check(intIsLess(bold_k, bold_n))
	check(bold_s.isSorted())


	// ALGORITHM
	for (let j = 1; j <= s; j++) {
		const bold_p_j = GetAllPoints(bold_beta.get(j), bold_delta.get(j), bold_s, bold_r, bold_n, bold_k, bold_e, pk, securityParams)
		const P_j = RecHash(bold_p_j).truncate(L_PA)
		builder_bold_P.set(j, P_j)
	}
	const bold_P = builder_bold_P.build()
	const PC = ByteArrayToString(ByteArray.xor(...bold_P), A_PA)
	return PC
}
