/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BallotProof } from "#protocols/common/model/BallotProof.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { BigIntegerDeserializer, TripleDeserializer, VectorDeserializer } from "#util/Deserializer.js"
import { Query } from "#protocols/common/model/Query.js"
import { Triple } from "#util/Tuples.js"
import { Vector } from "#util/sequence/Vector.js"

/**
 * Model class for ballots.
 * @augments {Triple<BigInteger, Vector<Query>, BallotProof>}
 */
export class Ballot extends Triple {

	/**
	 * @param {BigInteger} x_hat
	 * @param {Vector<Query>} bold_a
	 * @param {BallotProof} pi
	 */
	constructor(x_hat, bold_a, pi) {
		super(x_hat, bold_a, pi)
	}

	/**
	 * @type {BigInteger}
	 */
	get x_hat() {
		return this.first
	}

	/**
	 * @type {Vector<Query>}
	 */
	get bold_a() {
		return this.second
	}

	/**
	 * @type {BallotProof}
	 */
	get pi() {
		return this.third
	}

	/**
	 * @override
	 * @returns {string}
	 */
	toString() {
		return `Ballot(${this.x_hat}, ${this.a}, ${this.pi})`
	}
}

/**
 * The deserializer class for ballots
 */
Ballot.Deserializer = class extends TripleDeserializer {
	constructor() {
		super(
			new BigIntegerDeserializer(),
			new VectorDeserializer(new Query.Deserializer()),
			new BallotProof.Deserializer()
		)
	}
	/** @override */
	_deserialize(s) {
		return new Ballot(...this._parse(s))
	}
}
