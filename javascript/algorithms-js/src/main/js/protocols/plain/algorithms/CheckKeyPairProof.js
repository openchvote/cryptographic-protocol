/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { GetChallenge } from "#general/algorithms/GetChallenge.js"
import { KeyPairProof } from "#protocols/plain/model/KeyPairProof.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Set } from "#util/Set.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 8.8
 * @param {KeyPairProof} pi
 * @param {BigInteger} pk
 * @param {SecurityParameters} securityParams
 * @returns {boolean}
 */
export function CheckKeyPairProof(pi, pk, securityParams) {

	// PREPARATION
	check.Type(SecurityParameters, securityParams)
	const {ZZ_q, ZZPlus_p, ZZ_twoToTheTau, g} = securityParams

	check(Set.Pair(ZZ_twoToTheTau, ZZ_q).contains(pi))
	check(ZZPlus_p.contains(pk))
	const [c, s] = pi.values

	// ALGORITHM
	const t = ZZPlus_p.multiply(ZZPlus_p.pow(pk, c), ZZPlus_p.pow(g, s))
	const c_prime = GetChallenge(pk, t, securityParams)
	return c.equals(c_prime)
}
