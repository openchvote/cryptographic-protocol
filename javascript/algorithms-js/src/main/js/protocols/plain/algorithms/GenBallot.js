/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { AlgorithmError } from "#util/Errors.js"
import { Ballot } from "#protocols/plain/model/Ballot.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { GenBallotProof } from "#protocols/common/subalgorithms/GenBallotProof.js"
import { GenQuery } from "#protocols/common/subalgorithms/GenQuery.js"
import { GetPrimes } from "#general/algorithms/GetPrimes.js"
import { IntSet, Set } from "#util/Set.js"
import { Pair } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { StringToInteger } from "#general/algorithms/StringToInteger.js"
import { UsabilityParameters } from "#UsabilityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { check } from "#util/Precondition.js"
import { intSum, prod } from "#util/math/Math.js"

/**
 * ALGORITHM 8.22
 * @param {string} X
 * @param {Vector<number>} bold_s
 * @param {BigInteger} pk
 * @param {Vector<number>} bold_n
 * @param {number} w
 * @param {SecurityParameters} securityParams
 * @param {UsabilityParameters} usabilityParams
 * @returns {Pair<Ballot, Vector<BigInteger>>} (alpha, bold_r)
 */
export function GenBallot(X, bold_s, pk, bold_n, w, securityParams, usabilityParams) {

	// PREPARATION
	check.Type([SecurityParameters, securityParams], [UsabilityParameters, usabilityParams])
	const {ZZ_q, ZZPlus_p, GG_q_hat, q, g_hat} = securityParams
	const {A_X, ell_X} = usabilityParams

	check(Set.String(A_X, ell_X).contains(X))
	check(Set.Vector(IntSet.NN_plus).contains(bold_n))
	const n = intSum(bold_n)
	check(Set.Vector(IntSet.NN_plus_n(n)).contains(bold_s))
	check(bold_s.isSorted())
	check(ZZPlus_p.contains(pk))
	check(IntSet.NN_plus.contains(w))

	// ALGORITHM
	const x = StringToInteger(X, A_X)
	const x_hat = GG_q_hat.pow(g_hat, x)
	const bold_p = GetPrimes(n + w, securityParams)
	const bold_m = bold_p.select(bold_s)
	const m = prod(bold_m)
	if (bold_p.get(n + w).multiply(m).compareTo(q) > 0)
		throw new AlgorithmError("bold_s, bold_n, and w are incompatible with Z+_p")
	const [bold_a, bold_r] = GenQuery(bold_m, pk, securityParams).values
	const r = ZZ_q.sum(bold_r)
	const pi = GenBallotProof(x, ZZPlus_p.mapToGroup(m), r, x_hat, bold_a, pk, securityParams)
	const alpha = new Ballot(x_hat, bold_a, pi)

	return new Pair(alpha, bold_r)
}
