/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { GetPublicKey } from "#protocols/common/algorithms/GetPublicKey.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Set } from "#util/Set.js"
import { Vector } from "#util/sequence/Vector.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 9.6
 * @param {Matrix<BigInteger>} bold_PK_prime
 * @param {SecurityParameters} securityParams
 * @returns {Vector<BigInteger>} bold_pk_prime
 */
export function GetPublicKeys(bold_PK_prime, securityParams) {

	// PREPARATION
	check.Type(SecurityParameters, securityParams)
	const {ZZPlus_p} = securityParams

	check(Set.Matrix(ZZPlus_p).contains(bold_PK_prime))
	const z = bold_PK_prime.height
	const builder_bold_pk_prime = new Vector.Builder(z)

	// ALGORITHM
	for (let i = 1; i <= z; i++) {
		const bold_pk_prime_i = bold_PK_prime.getRow(i)
		const pk_prime_i = GetPublicKey(bold_pk_prime_i, securityParams)
		builder_bold_pk_prime.set(i, pk_prime_i)
	}
	const bold_ok_prime = builder_bold_pk_prime.build()
	return bold_ok_prime
}
