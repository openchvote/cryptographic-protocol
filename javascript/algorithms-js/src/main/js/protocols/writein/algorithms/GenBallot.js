/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { AlgorithmError } from "#util/Errors.js"
import { Ballot } from "../model/Ballot.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { GenBallotProof } from "#protocols/common/subalgorithms/GenBallotProof.js"
import { GenQuery } from "#protocols/common/subalgorithms/GenQuery.js"
import { GenWriteInEncryption } from "../subalgorithms/GenWriteInEncryption.js"
import { GenWriteInProof } from "../subalgorithms/GenWriteInProof.js"
import { GetEncodedWriteIns } from "../subalgorithms/GetEncodedWriteIns.js"
import { GetPrimes } from "#general/algorithms/GetPrimes.js"
import { GetWriteInIndices } from "../subalgorithms/GetWriteInIndices.js"
import { IntSet, Set } from "#util/Set.js"
import { Pair } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { StringToInteger } from "#general/algorithms/StringToInteger.js"
import { UsabilityParameters } from "#UsabilityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { WriteIn } from "../model/WriteIn.js"
import { check } from "#util/Precondition.js"
import { intIsLess, intSum, intSumProd, prod } from "#util/math/Math.js"

/**
 * ALGORITHM 9.8
 * @param {string} X
 * @param {Vector<number>} bold_s
 * @param {Vector<WriteIn>} bold_s_prime
 * @param {BigInteger} pk
 * @param {Vector<BigInteger>} bold_pk_prime
 * @param {Vector<number>} bold_n
 * @param {Vector<number>} bold_k
 * @param {number} w
 * @param {Vector<number>} bold_e_hat
 * @param {Vector<number>} bold_v
 * @param {Vector<number>} bold_z
 * @param {SecurityParameters} securityParams
 * @param {UsabilityParameters} usabilityParams
 * @returns {Pair<Ballot, Vector<BigInteger>>} (alpha, bold_r)
 */
export function GenBallot(X, bold_s, bold_s_prime, pk, bold_pk_prime, bold_n, bold_k, w, bold_e_hat, bold_v, bold_z, securityParams, usabilityParams) {

	// PREPARATION
	check.Type([SecurityParameters, securityParams], [UsabilityParameters, usabilityParams])
	const {ZZ_q, ZZPlus_p, GG_q_hat, q, g_hat} = securityParams
	const {A_X, ell_X, A_W, ell_W} = usabilityParams

	check(Set.String(A_X, ell_X).contains(X))
	check(Set.Vector(IntSet.NN_plus).contains(bold_n))
	const n = intSum(bold_n)
	const t = bold_n.length
	check(Set.Vector(IntSet.NN_plus_n(n)).contains(bold_s))
	const k = bold_s.length
	check(Set.Vector(Set.Pair(
		Set.String(A_W, 0, ell_W),
		Set.String(A_W, 0, ell_W))).contains(bold_s_prime))
	const z = bold_s_prime.length
	check(ZZPlus_p.contains(pk))
	check(Set.Vector(ZZPlus_p).contains(bold_pk_prime))
	const z_max = bold_pk_prime.length
	check(Set.Vector(IntSet.NN_plus, t).contains(bold_k))
	check(IntSet.NN_plus.contains(w))
	check(Set.Vector(IntSet.BB, t).contains(bold_e_hat))
	check(Set.Vector(IntSet.BB, n).contains(bold_v))
	check(Set.Vector(IntSet.BB, t).contains(bold_z))

	// CONSTRAINTS
	check(k === intSumProd(bold_e_hat, bold_k))
	check(intIsLess(bold_k, bold_n))
	intSumProd(bold_e_hat, bold_z.times(bold_k))
	check(z <= z_max && z_max <= intSumProd(bold_z, bold_k))
	check(bold_s.isSorted())

	// ALGORITHM
	const x = StringToInteger(X, A_X)
	const x_hat = GG_q_hat.pow(g_hat, x)
	const bold_p = GetPrimes(n + w, securityParams)
	const bold_m = bold_p.select(bold_s)
	const m = prod(bold_m)
	if (bold_p.get(n + w).multiply(m).compareTo(q) > 0) {
		throw new AlgorithmError("bold_s, bold_n, and w are incompatible with Z+_p")
	}
	const [bold_a, bold_r] = GenQuery(bold_m, pk, securityParams)
	const r = ZZ_q.sum(bold_r)
	const pi = GenBallotProof(x, ZZPlus_p.mapToGroup(m), r, x_hat, bold_a, pk, securityParams)
	const bold_m_prime = GetEncodedWriteIns(bold_s_prime, usabilityParams)
	const [e_prime, r_prime] = GenWriteInEncryption(bold_pk_prime, bold_m_prime, securityParams)
	const [I, J] = GetWriteInIndices(bold_n, bold_k, bold_e_hat, bold_v, bold_z)
	const bold_m_I = bold_m.select(I)
	const bold_a_I = bold_a.select(I)
	const bold_r_I = bold_r.select(I)
	const bold_p_J = bold_p.select(J)
	const pi_prime = GenWriteInProof(pk, bold_m_I, bold_a_I, bold_r_I, bold_pk_prime, bold_m_prime, e_prime, r_prime, bold_p_J, securityParams, usabilityParams)
	const alpha = new Ballot(x_hat, bold_a, pi, e_prime, pi_prime)
	return new Pair(alpha, bold_r)
}
