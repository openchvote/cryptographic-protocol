/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { GetChallenge } from "#general/algorithms/GetChallenge.js"
import { KeyPairProof } from "../model/KeyPairProof.js"
import { Pair } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Set } from "#util/Set.js"
import { Vector, ZeroIndexedVector } from "#util/sequence/Vector.js"
import { check } from "#util/Precondition.js"

/**
 * ALGORITHM 9.5
 * @param {KeyPairProof} pi
 * @param {BigInteger} pk
 * @param {Vector<BigInteger>} bold_pk_prime
 * @param {SecurityParameters} securityParams
 * @returns {boolean}
 */
export function CheckKeyPairProof(pi, pk, bold_pk_prime, securityParams) {

	// PREPARATIONS
	check.Type(SecurityParameters, securityParams)
	const {ZZ_q, ZZPlus_p, ZZ_twoToTheTau, g} = securityParams

	check(Set.Vector(ZZPlus_p).contains(bold_pk_prime))
	const z = bold_pk_prime.length
	check(Set.Pair(ZZ_twoToTheTau, Set.Vector(ZZ_q, z + 1)).contains(pi))
	check(ZZPlus_p.contains(pk))
	const [c, bold_s] = pi.values
	const builder_bold_t = new ZeroIndexedVector.Builder(z)

	// ALGORITHM
	const s_0 = bold_s.get(0)
	const t_0 = ZZPlus_p.multiply(ZZPlus_p.pow(pk, c), ZZPlus_p.pow(g, s_0))
	builder_bold_t.set(0, t_0)
	for (let i = 1; i <= z; i++) {
		const s_i = bold_s.get(i)
		const pk_prime_i = bold_pk_prime.get(i)
		const t_i = ZZPlus_p.multiply(ZZPlus_p.pow(pk_prime_i, c), ZZPlus_p.pow(g, s_i))
		builder_bold_t.set(i, t_i)
	}
	const bold_t = builder_bold_t.build()
	const y = new Pair(pk, bold_pk_prime)
	const c_prime = GetChallenge(y, bold_t, securityParams)
	return c.equals(c_prime)
}
