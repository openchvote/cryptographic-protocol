/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { Pair } from "#util/Tuples.js"
import { Vector } from "#util/sequence/Vector.js"

/**
 * ALGORITHM 9.11
 * @param {Vector<number>} bold_n
 * @param {Vector<number>} bold_k
 * @param {Vector<number>} bold_e_hat
 * @param {Vector<number>} bold_v
 * @param {Vector<number>} bold_z
 * @returns {Pair<Vector<number>, Vector<number>>} (I, J)
 */
export function GetWriteInIndices(bold_n, bold_k, bold_e_hat, bold_v, bold_z) {

	// PREPARATION
	const t = bold_n.length
	const builder_I = new Vector.Builder()
	const builder_J = new Vector.Builder()

	// ALGORITHM
	let n_prime = 0
	let k_prime = 0
	for (let l = 1; l <= t; l++) {
		const n_l = bold_n.get(l)
		const k_l = bold_k.get(l)
		const e_hat_l = bold_e_hat.get(l)
		const z_l = bold_z.get(l)
		if (e_hat_l === 1) {
			if (z_l === 1) {
				for (let i = k_prime + 1; i <= k_prime + k_l; i++) {
					builder_I.add(i)
				}
				for (let j = n_prime + 1; j <= n_prime + n_l; j++) {
					const v_j = bold_v.get(j)
					if (v_j === 1) {
						builder_J.add(j)
					}
				}
			}
			k_prime = k_prime + k_l
		}
		n_prime = n_prime + n_l
	}
	const I = builder_I.build()
	const J = builder_J.build()
	return new Pair(I, J)
}
