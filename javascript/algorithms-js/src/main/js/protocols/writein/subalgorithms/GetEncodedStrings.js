/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { Alphabet } from "#Alphabet.js"
import { BigInteger, ONE } from "#util/math/BigInteger.js"
import { StringToInteger } from "#general/algorithms/StringToInteger.js"
import { WriteIn } from "../model/WriteIn.js"

/**
 * ALGORITHM 9.1
 * @param {WriteIn} S
 * @param {Alphabet} A
 * @param {number} ell
 * @param {string} c
 * @returns {BigInteger} x
 */
export function GetEncodedStrings(S, A, ell, c) {

	// PREPARATION
	let [S_1, S_2] = S.values

	// ALGORITHM
	while (S_1.length < ell) {
		S_1 = c + S_1
	}
	while (S_2.length < ell) {
		S_2 = c + S_2
	}
	const S12 = S_1 + S_2
	const x = StringToInteger(S12, A.addCharacter(c)).add(ONE)
	return x
}
