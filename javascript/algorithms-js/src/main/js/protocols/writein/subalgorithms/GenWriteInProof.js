/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { AlgorithmError } from "#util/Errors.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { EMPTY_EMPTY } from "../model/WriteIn.js"
import { GenCNFProof } from "./GenCNFProof.js"
import { GetEncodedStrings } from "./GetEncodedStrings.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { MultiEncryption } from "../model/MultiEncryption.js"
import { Pair, Triple } from "#util/Tuples.js"
import { Query } from "#protocols/common/model/Query.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { UsabilityParameters } from "#UsabilityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { WriteInProof } from "../model/WriteInProof.js"

/**
 * ALGORITHM 9.12
 * @param {BigInteger} pk
 * @param {Vector<BigInteger>} bold_m
 * @param {Vector<Query>} bold_a
 * @param {Vector<BigInteger>} bold_r
 * @param {Vector<BigInteger>} bold_pk_prime
 * @param {Vector<BigInteger>} bold_m_prime
 * @param {MultiEncryption} e_prime
 * @param {BigInteger} r_prime
 * @param {Vector<BigInteger>} bold_p
 * @param {SecurityParameters} securityParams
 * @param {UsabilityParameters} usabilityParams
 * @returns {WriteInProof} pi
 */
export function GenWriteInProof(pk, bold_m, bold_a, bold_r, bold_pk_prime, bold_m_prime, e_prime, r_prime, bold_p, securityParams, usabilityParams) {

	// PREPARATION
	const {A_W, ell_W, c_W} = usabilityParams

	const z = bold_m.length
	const [bold_a_prime, b_prime] = e_prime.values
	const builder_bold_Y_star = new Matrix.Builder(z, 2)
	const builder_bold_r_star = new Vector.Builder(z)
	const builder_bold_j = new Vector.Builder(z)

	// ALGORITHM
	const epsilon = GetEncodedStrings(EMPTY_EMPTY, A_W, ell_W, c_W)
	for (let i = 1; i <= z; i++) {
		const p_i = bold_p.get(i)
		builder_bold_Y_star.set(i, 1, new Triple(pk, p_i, bold_a.get(i)))
		builder_bold_Y_star.set(i, 2, new Triple(bold_pk_prime.get(i), epsilon, new Pair(bold_a_prime.get(i), b_prime)))
		if (bold_m.get(i).equals(p_i)) {
			builder_bold_r_star.set(i, bold_r.get(i))
			builder_bold_j.set(i, 1)
		} else if (bold_m_prime.get(i).equals(epsilon)) {
			builder_bold_r_star.set(i, r_prime)
			builder_bold_j.set(i, 2)
		} else {
			throw new AlgorithmError("Invalid input")
		}
	}
	const bold_Y_star = builder_bold_Y_star.build()
	const bold_r_star = builder_bold_r_star.build()
	const bold_j = builder_bold_j.build()
	const pi = GenCNFProof(bold_Y_star, bold_r_star, bold_j, securityParams)
	return pi
}
