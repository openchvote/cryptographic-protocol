/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { GenRandomInteger } from "#general/algorithms/GenRandomInteger.js"
import { MultiEncryption } from "../model/MultiEncryption.js"
import { Pair } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"

/**
 * ALGORITHM 9.10
 * @param {Vector<BigInteger>} bold_pk
 * @param {Vector<BigInteger>} bold_m
 * @param {SecurityParameters} securityParams
 * @returns {Pair<MultiEncryption, BigInteger>} (e, r)
 */
export function GenWriteInEncryption(bold_pk, bold_m, securityParams) {

	// PREPARATION
	const {ZZPlus_p, q, g} = securityParams

	const z = bold_m.length
	const builder_bold_a = new Vector.Builder(z)

	// ALGORITHM
	const r = GenRandomInteger(q)
	for (let i = 1; i <= z; i++) {
		const a_i = ZZPlus_p.multiply(bold_m.get(i), ZZPlus_p.pow(bold_pk.get(i), r))
		builder_bold_a.set(i, a_i)
	}
	const bold_a = builder_bold_a.build()
	const b = ZZPlus_p.pow(g, r)
	const e = new MultiEncryption(bold_a, b)
	return new Pair(e, r)
}
