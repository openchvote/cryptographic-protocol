/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { GetEncodedStrings } from "./GetEncodedStrings.js"
import { UsabilityParameters } from "#UsabilityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { WriteIn } from "../model/WriteIn.js"

/**
 * ALGORITHM 9.9
 * @param {Vector<WriteIn>} bold_s
 * @param {UsabilityParameters} usabilityParams
 * @returns {Vector<BigInteger>} bold_m
 */
export function GetEncodedWriteIns(bold_s, usabilityParams) {

	// PREPARATION
	const {A_W, ell_W, c_W} = usabilityParams

	const z = bold_s.length
	const builder_bold_m = new Vector.Builder(z)

	// ALGORITHM
	for (let i = 1; i <= z; i++) {
		const m_i = GetEncodedStrings(bold_s.get(i), A_W, ell_W, c_W)
		builder_bold_m.set(i, m_i)
	}
	const bold_m = builder_bold_m.build()
	return bold_m
}
