/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger, ZERO } from "#util/math/BigInteger.js"
import { GenRandomInteger } from "#general/algorithms/GenRandomInteger.js"
import { GetChallenge } from "#general/algorithms/GetChallenge.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { Pair, Triple } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { WriteInProof } from "../model/WriteInProof.js"

/**
 * ALGORITHM 9.13
 * @param {Matrix<Triple<BigInteger, BigInteger, Pair<BigInteger, BigInteger>>>} bold_Y
 * @param {Vector<BigInteger>} bold_r
 * @param {Vector<number>} bold_j
 * @param {SecurityParameters} securityParams
 * @returns {WriteInProof} pi
 */
export function GenCNFProof(bold_Y, bold_r, bold_j, securityParams) {

	// PREPARATION
	const {ZZPlus_p, ZZ_q, ZZ_twoToTheTau, q, g, twoToTheTau} = securityParams

	const [m, n] = bold_Y.size
	const builder_bold_c = new Vector.Builder(m)
	const builder_bold_omega = new Vector.Builder(m)
	const builder_bold_T = new Matrix.Builder(m, n)
	const builder_bold_C = new Matrix.Builder(m, n)
	const builder_bold_S = new Matrix.Builder(m, n)

	// ALGORITHM
	for (let i = 1; i <= m; i++) {
		let c_i = ZERO
		for (let j = 1; j <= n; j++) {
			const [pk_ij, m_ij, [a_ij, b_ij]] = bold_Y.get(i, j)
			let t_ij
			if (j === bold_j.get(i)) {
				const omega_i = GenRandomInteger(q)
				t_ij = new Pair(ZZPlus_p.pow(pk_ij, omega_i), ZZPlus_p.pow(g, omega_i))
				builder_bold_omega.set(i, omega_i)
			} else {
				const c_ij = GenRandomInteger(twoToTheTau)
				const s_ij = GenRandomInteger(q)
				t_ij = new Pair(
					ZZPlus_p.multiply(ZZPlus_p.pow(pk_ij, s_ij), ZZPlus_p.pow(ZZPlus_p.divide(a_ij, m_ij), c_ij)),
					ZZPlus_p.multiply(ZZPlus_p.pow(g, s_ij), ZZPlus_p.pow(b_ij, c_ij)))
				c_i = ZZ_twoToTheTau.add(c_i, c_ij)
				builder_bold_C.set(i, j, c_ij)
				builder_bold_S.set(i, j, s_ij)
			}
			builder_bold_T.set(i, j, t_ij)
		}
		builder_bold_c.set(i, c_i)
	}
	const bold_T = builder_bold_T.build()
	const c = GetChallenge(bold_Y, bold_T, securityParams)
	const bold_c = builder_bold_c.build()
	const bold_omega = builder_bold_omega.build()
	for (let i = 1; i <= m; i++) {
		const j = bold_j.get(i)
		const c_ij = ZZ_twoToTheTau.subtract(c, bold_c.get(i))
		const s_ij = ZZ_q.subtract(bold_omega.get(i), ZZ_q.multiply(c_ij, bold_r.get(i)))
		builder_bold_C.set(i, j, c_ij)
		builder_bold_S.set(i, j, s_ij)
	}
	const bold_C = builder_bold_C.build()
	const bold_S = builder_bold_S.build()
	const pi = new WriteInProof(bold_C, bold_S)
	return pi
}
