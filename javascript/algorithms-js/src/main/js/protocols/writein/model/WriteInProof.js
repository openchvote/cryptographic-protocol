/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { BigIntegerDeserializer, PairDeserializer, MatrixDeserializer } from "#util/Deserializer.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { Pair } from "#util/Tuples.js"

/**
 * Model class for write-in proofs.
 * @augments {Pair<Matrix<BigInteger>, Matrix<BigInteger>>}
 */
export class WriteInProof extends Pair {

	/**
	 * @param {Matrix<BigInteger>} bold_C
	 * @param {Matrix<BigInteger>} bold_S
	 */
	constructor(bold_C, bold_S) {
		super(bold_C, bold_S)
	}

	/**
	 * @type {Matrix<BigInteger>}
	 */
	get bold_C() {
		return this.first
	}

	/**
	 * @type {Matrix<BigInteger>}
	 */
	get bold_S() {
		return this.second
	}

	/**
	 * @override
	 * @returns {string}
	 */
	toString() {
		return `WriteInProof(${this.bold_C}, ${this.bold_S})`
	}
}

/**
 * The deserializer class for write-in proofs.
 */
WriteInProof.Deserializer = class extends PairDeserializer {
	constructor() {
		super(
			new MatrixDeserializer(new BigIntegerDeserializer()),
			new MatrixDeserializer(new BigIntegerDeserializer())
		)
	}
	/** @override */
	_deserialize(s) {
		return new WriteInProof(...this._parse(s))
	}
}
