/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { IllegalArgumentError } from "#util/Errors.js"
import { Pair } from "#util/Tuples.js"
import { StringDeserializer, PairDeserializer } from "#util/Deserializer.js"

/**
 * Model class for write-ins.
 * @augments {Pair<string, string>}
 */
export class WriteIn extends Pair {

	/**
	 * @param {string} S1
	 * @param {string} S2
	 */
	constructor(S1, S2) {
		super(S1, S2)
	}

	/**
	 * @type {string}
	 */
	get S1() {
		return this.first
	}

	/**
	 * @type {string}
	 */
	get S2() {
		return this.second
	}

	/**
	 * @override
	 * @returns {string}
	 */
	toString() {
		return this.S1 + "_" + this.S2
	}

	/**
	 * Compares this write-in to another write-in.
	 * @param {WriteIn} other - The other object to be compared
	 * @returns {number} - A negative integer, zero, or a positive integer as this write-in is in alphabetical order
	 * before, equal to, or after the other write-in.
	 */
	compareTo(other){
		if (!(other instanceof WriteIn))
			throw new IllegalArgumentError()
		const c = this.S1.localeCompare(other.S2)
		return c === 0 ? this.S2.localeCompare(other.S2) : c
	}
}

/**
 * The deserializer class for write-ins.
 */
WriteIn.Deserializer = class extends PairDeserializer {
	constructor() {
		super(
			new StringDeserializer(),
			new StringDeserializer()
		)
	}
	/** @override */
	_deserialize(s) {
		return new WriteIn(...this._parse(s))
	}
}

/**
 * The empty write-in.
 * @type {WriteIn}
 */
export const EMPTY_EMPTY = new WriteIn("", "")
