/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BYTE_SIZE } from "#util/sequence/ByteArray.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { GG } from "#util/algebra/GG.js"
import { IllegalArgumentError } from "#util/Errors.js"
import { ZZ } from "#util/algebra/ZZ.js"
import { ZZPlus } from "#util/algebra/ZZPlus.js"
import { byteLength, powerOfTwo } from "#util/math/Math.js"

/**
 * This class represent the security parameters as defined in [Spec, Section 6.3].
 */
export class SecurityParameters {
	_securityLevel
	_values

	/**
	 * Constructs a security parameters object based on a given security level.
	 * @param {string} SL - The identifier of the security level
	 */
	constructor(SL) {
		if (!securityLevels[SL])
			throw new IllegalArgumentError()

		this._securityLevel = SL
		this._values = securityLevels[SL]

		// Replace functions by their values
		for (let key in this._values) {
			if (this._values[key] instanceof Function) {
				this._values[key] = this._values[key]()
			}
		}

		// Instantiate corresponding groups
		if (!this._values.ZZPlus_p) this._values.ZZPlus_p = new ZZPlus(this._values.p)
		if (!this._values.ZZ_q) this._values.ZZ_q = new ZZ(this._values.q)
		if (!this._values.GG_q_hat) this._values.GG_q_hat = new GG(this._values.p_hat, this._values.q_hat)
		if (!this._values.ZZ_q_hat) this._values.ZZ_q_hat = new ZZ(this._values.q_hat)

		Object.freeze(this._values)
		Object.freeze(this)
	}

	/**
	 * The identifier of the security level provided by the parameters.
	 * @returns {string} - The identifier of the security level
	 */
	get securityLevel() {
		return this._securityLevel
	}

	/**
	 * The security parameter `sigma`.
	 * @returns {number} - The security parameter `sigma`
	 */
	get sigma() {
		return this._values.sigma
	}

	/**
	 * The security parameter `epsilon`.
	 * @returns {number} - The security parameter `epsilon`
	 */
	get epsilon() {
		return this._values.epsilon
	}


	// NIZKP PARAMETERS

	/**
	 * The security parameter `tau` used to generate and verify non-interactive zero-knowledge proofs (NIZKP).
	 * @returns {number} - The security parameter `tau`
	 */
	get tau() {
		return this._values.tau
	}

	/**
	 * `2^tau` for the security parameter `tau`
	 * @returns {BigInteger} - `2`tau`
	 */
	get twoToTheTau() {
		return powerOfTwo(this.tau)
	}

	/**
	 * The corresponding additive group of integers modulo `2^tau`.
	 * @returns {ZZ} - The corresponding additive group of integers
	 */
	get ZZ_twoToTheTau() {
		return ZZ.of(this.twoToTheTau)
	}


	// ZZPlus PARAMETERS

	/**
	 * The modulus `p` of a multiplicative group of absolute values modulo a safe prime `p=2q+1`.
	 * @returns {BigInteger} - The modulus `p`
	 */
	get p() {
		return this._values.p
	}

	/**
	 * The group order `q` of a multiplicative group of absolute values modulo a safe prime `p=2q+1`.
	 * @returns {BigInteger} - The order `q`
	 */
	get q() {
		return this._values.q
	}

	/**
	 * The first group generator `g` of a multiplicative group of absolute values modulo a safe prime `p=2q+1`.
	 * @returns {BigInteger} - The first group generator `g`
	 */
	get g() {
		return this._values.g
	}

	/**
	 * The second group generator `h` of a multiplicative group of absolute values modulo a safe prime `p=2q+1`.
	 * @returns {BigInteger} - The second group generator `h`
	 */
	get h() {
		return this._values.h
	}

	/**
	 * The corresponding multiplicative group of absolute values modulo a safe prime `p=2q+1`.
	 * @returns {ZZPlus} - The corresponding multiplicative group of absolute values modulo `p`
	 */
	get ZZPlus_p() {
		return this._values.ZZPlus_p
	}

	/**
	 * The corresponding additive group of integers modulo `q`.
	 * @returns {ZZ} - The corresponding additive group of integers modulo `q`
	 */
	get ZZ_q() {
		return this._values.ZZ_q
	}


	// GG PARAMETERS

	/**
	 * The modulus `p_hat` of a prime-order subgroup of integers modula a prime.
	 * @returns {BigInteger} - The modulus `p_hat`
	 */
	get p_hat() {
		return this._values.p_hat
	}

	/**
	 * The group order `g_hat` of a prime-order subgroup of integers modulo a prime.
	 * @returns {BigInteger} - The group order `q_hat`
	 */
	get q_hat() {
		return this._values.q_hat
	}

	/**
	 * The group generator `g_hat` of a prime-order subgroup od integers modulo a prime.
	 * @returns {BigInteger} - The group generator `g_hat`
	 */
	get g_hat() {
		return this._values.g_hat
	}

	/**
	 * The corresponding multiplicative subgroup of integers modulo `p_hat`.
	 * @returns {GG} - The corresponding multiplicative subgroup of integers modulo `p_hat`
	 */
	get GG_q_hat() {
		return this._values.GG_q_hat
	}

	/**
	 * The corresponding additive group of integers modulo `q_hat`.
	 * @returns {ZZ} - The corresponding additive group of integers modulo `q_hat`
	 */
	get ZZ_q_hat() {
		return this._values.ZZ_q_hat
	}

	/**
	 * The parameter `L_M`, which is twice the length of `g_hat`in bytes.
	 * @returns {number} - The parameter `L_M`
	 */
	get L_M() {
		return 2 * byteLength(this.q_hat)
	}


	// HASH PARAMETERS
	// (The hash algorithm itself is not configurable but hard-coded in the `Hash` algorithm {@see #util/crypto/Hash.js})

	/**
	 * The length (number of bytes) of the hash values generated by the `Hash` algorithm.
	 * @returns {number} - The hash value length in bytes
	 */
	get L() {
		return this._values.L
	}

	/**
	 * The length (number of bits) of the hash values generated by the `Hash` algorithm.
	 * @returns {number} - The hash value length in bits
	 */
	get ell() {
		return this.L * BYTE_SIZE
	}


	// BLOCK CIPHER PARAMETERS
	// (The block cipher itself is not configurable but hard-coded in the `AES` algorithm {@see #util/crypto/AES.js})

	/**
	 * The key length (number of bytes) of the block cipher used by the `AES` algorithm.
	 * @returns {number} - The key length in bytes
	 */
	get L_K() {
		return this._values.L_K
	}

	/**
	 * The length (number of bytes) of the initialization vector of the block cipher used by the `AES` algorithm.
	 * @returns {number} - The length of the initialization vector in bytes
	 */
	get L_IV() {
		return this._values.L_IV
	}
}


// SECURITY LEVELS

/**
 * This object contains all the group parameters from [Spec, Section 10.1 and 10.2]. The values could be generated
 * using the `GetGroupParameters` algorithm (see [Spec, Alg. 10.1]), but this is a relatively time-consuming operation.
 * For efficiency, BigIntegers are only instantiated when needed.
 * @private
 */
const securityLevels = {
	"LEVEL_0": {
		"sigma": 16,
		"tau": 16,
		"epsilon": 0.999,
		"p": () => new BigInteger("B7E151629927", 16),
		"q": () => new BigInteger("5BF0A8B14C93", 16),
		"g": () => new BigInteger(2),
		"h": () => new BigInteger(3),
		"p_hat": () => new BigInteger("B7FC9CE51713", 16),
		"q_hat": () => new BigInteger("B7E15173", 16),
		"g_hat": () => new BigInteger("8145D710FE7F", 16),
		"L": 32,
		"L_K": 16,
		"L_IV": 12,
	},
	"LEVEL_1": {
		"sigma": 112,
		"tau": 112,
		"epsilon": 0.9999,
		"p": () => new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF324E7738926CFBE5F4BF8D8D8C31D763DA06C80ABB1185EB4F7C7B5757F5958490CFD47D7C19BB42158D9554F7B46BCED55C4D79FD5F24D6613C31C3839A2DDF8A9A276BCFBFA1C877C56284DAB79CD4C2B3293D20E9E5EAF02AC60ACC93ED874422A52ECB238FEEE5AB6ADD835FD1A0753D0A8F78E537D2B95BB79D8DCAEC642C1E9F23B829B5C2780BF38737DF8BB300D01334A0D0BD8645CBFA73A6160FFE393C48CBBBCA060F0FF8EC6D31BEB5CCEED7F2F0BB088017163BC60DF45A0ECB1BCD289B06CBBFEA21AD08E1847F3F7378D56CED94640D6EF0D3D37BE69D0063", 16),
		"q": () => new BigInteger("5BF0A8B1457695355FB8AC404E7A79E3B1738B079C5A6D2B53C26C8228C867F799273B9C49367DF2FA5FC6C6C618EBB1ED0364055D88C2F5A7BE3DABABFACAC24867EA3EBE0CDDA10AC6CAAA7BDA35E76AAE26BCFEAF926B309E18E1C1CD16EFC54D13B5E7DFD0E43BE2B1426D5BCE6A6159949E9074F2F5781563056649F6C3A21152976591C7F772D5B56EC1AFE8D03A9E8547BC729BE95CADDBCEC6E57632160F4F91DC14DAE13C05F9C39BEFC5D98068099A50685EC322E5FD39D30B07FF1C9E2465DDE5030787FC763698DF5AE6776BF9785D84400B8B1DE306FA2D07658DE6944D8365DFF510D68470C23F9FB9BC6AB676CA3206B77869E9BDF34E8031", 16),
		"g": () => new BigInteger(2),
		"h": () => new BigInteger(3),
		"p_hat": () => new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF324E7738926CFBE5F4BF8D8D8C31D763DA06C80ABB1185EB4F7C7B5757F5958490CFD47D7C19BB42158D9554F7B46BCED55C4D79FD5F24D6613C31C3839A2DDF8A9A276BCFBFA1C877C56284DAB79CD4C2B3293D20E9E5EAF02AC60ACC93ED874422A52ECB238FEEE5AB6ADD835FD1A0753D0A8F78E537D2B95BB79D8DCAEC642C1E9F23B829B5C2780BF38737DF8BB300D01334A0D0BD8645CBFA73A6160FFE393C48CBBBCA060F0FF8EC6D31BEB5CCEED7F2F0BB088017163BC60DF45A0ECB1BCD3548E571733F4A8C724DC97F56F0AE89897D8A6B93C6F87D7494503A5D6D", 16),
		"q_hat": () => new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D991", 16),
		"g_hat": () => new BigInteger("7C41B5D002301514D10155BF22BA33947C96EB398837B9E6AC1A25ABFC3F9D44FB7D943A3317771A26615814BB06E58B5531F4D81CF23B778F23A2364FFB0C28A7335AE731761FAB304975C8DB647FCCFC1E64239373F60FAD80FE12D750B3CD753B98D548A325A9A629B06E63A7FC2860D4EB1B885482B64D7177854104554363DFD70DAFDF529F9AFF072F78B7FEAA92D00DC6A7180FF49B60F84979A777919E42484A6A1C014E7F8E8CC184546CAE0557124F7F21FB2C16AC6EF4F122BB70966F9FBF03A7807AF8190CDF95DCDF0509C0FA8302681130E7B60C9E9A65BDF83940F0CCC164989B558B9724D97C524E1A2810E0BB546F83754A846000A9ADB2", 16),
		"L": 32,
		"L_K": 16,
		"L_IV": 12,
	},
	"LEVEL_2": {
		"sigma": 128,
		"tau": 128,
		"epsilon": 0.99999,
		"p": () => new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF324E7738926CFBE5F4BF8D8D8C31D763DA06C80ABB1185EB4F7C7B5757F5958490CFD47D7C19BB42158D9554F7B46BCED55C4D79FD5F24D6613C31C3839A2DDF8A9A276BCFBFA1C877C56284DAB79CD4C2B3293D20E9E5EAF02AC60ACC93ED874422A52ECB238FEEE5AB6ADD835FD1A0753D0A8F78E537D2B95BB79D8DCAEC642C1E9F23B829B5C2780BF38737DF8BB300D01334A0D0BD8645CBFA73A6160FFE393C48CBBBCA060F0FF8EC6D31BEB5CCEED7F2F0BB088017163BC60DF45A0ECB1BCD289B06CBBFEA21AD08E1847F3F7378D56CED94640D6EF0D3D37BE67008E186D1BF275B9B241DEB64749A47DFDFB96632C3EB061B6472BBF84C26144E49C2D04C324EF10DE513D3F5114B8B5D374D93CB8879C7D52FFD72BA0AAE7277DA7BA1B4AF1488D8E836AF14865E6C37AB6876FE690B571121382AF341AFE94F77BCF06C83B8FF5675F0979074AD9A787BC5B9BD4B0C5937D3EDE4C3A79396419CD7", 16),
		"q": () => new BigInteger("5BF0A8B1457695355FB8AC404E7A79E3B1738B079C5A6D2B53C26C8228C867F799273B9C49367DF2FA5FC6C6C618EBB1ED0364055D88C2F5A7BE3DABABFACAC24867EA3EBE0CDDA10AC6CAAA7BDA35E76AAE26BCFEAF926B309E18E1C1CD16EFC54D13B5E7DFD0E43BE2B1426D5BCE6A6159949E9074F2F5781563056649F6C3A21152976591C7F772D5B56EC1AFE8D03A9E8547BC729BE95CADDBCEC6E57632160F4F91DC14DAE13C05F9C39BEFC5D98068099A50685EC322E5FD39D30B07FF1C9E2465DDE5030787FC763698DF5AE6776BF9785D84400B8B1DE306FA2D07658DE6944D8365DFF510D68470C23F9FB9BC6AB676CA3206B77869E9BDF3380470C368DF93ADCD920EF5B23A4D23EFEFDCB31961F5830DB2395DFC26130A2724E1682619277886F289E9FA88A5C5AE9BA6C9E5C43CE3EA97FEB95D0557393BED3DD0DA578A446C741B578A432F361BD5B43B7F3485AB88909C1579A0D7F4A7BBDE783641DC7FAB3AF84BC83A56CD3C3DE2DCDEA5862C9BE9F6F261D3C9CB20CE6B", 16),
		"g": () => new BigInteger(2),
		"h": () => new BigInteger(3),
		"p_hat": () => new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF324E7738926CFBE5F4BF8D8D8C31D763DA06C80ABB1185EB4F7C7B5757F5958490CFD47D7C19BB42158D9554F7B46BCED55C4D79FD5F24D6613C31C3839A2DDF8A9A276BCFBFA1C877C56284DAB79CD4C2B3293D20E9E5EAF02AC60ACC93ED874422A52ECB238FEEE5AB6ADD835FD1A0753D0A8F78E537D2B95BB79D8DCAEC642C1E9F23B829B5C2780BF38737DF8BB300D01334A0D0BD8645CBFA73A6160FFE393C48CBBBCA060F0FF8EC6D31BEB5CCEED7F2F0BB088017163BC60DF45A0ECB1BCD289B06CBBFEA21AD08E1847F3F7378D56CED94640D6EF0D3D37BE67008E186D1BF275B9B241DEB64749A47DFDFB96632C3EB061B6472BBF84C26144E49C2D04C324EF10DE513D3F5114B8B5D374D93CB8879C7D52FFD72BA0AAE7277DA7BA1B4AF1488D8E836AF14865E6C37AB6876FE690B571121382AF341AFE94F790F02FA1BCE9C73886B4C0ACABDC3DD14E0D8C955577C9764844038771FC25F84BB", 16),
		"q_hat": () => new BigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190D05D", 16),
		"g_hat": () => new BigInteger("47DAD70733EFE399D1AFF4FE387250218BB88FD5F4040C31851AE1DF0985D0019950A958710C6B935B6B3BB45C278381DC5883CC933C5B7052D3BC8C77D746E3D1FB2B7EF3630C1014417D2F83BEAD0E1F4DFD986104CDF16C4AEC33BB5906C8149C83E6C5B8837E12AB32E73A69C4ABEB0B014FFF1FBB3173EAD73A1404DAEDF52F62D605D3787900124829751320FEDAA1F5B2D90FB846C7EB7815193E5C2460F93A3A5D16FB7A3DBAC9CE31B7517D2F88D530E61D06B529A43A0806F6A931247C9166C32CC9BAA019823528D3F156B60ECE5DA9A6D60148661F59670AD98A1B8EAFEBC4A68D8A5D3F29105FD33D994751A9AD8E0EB7367D5BFE7A2F082981869FA2F177C472D1988844E4DA58170BB3DDE9DFB2E61DC06FA5249C3200CD3BBBF24D5C257879CB23D7931ED4AD1F9FA168B38FAA3C6DB89AA9D89BB6DB3F47BF1BE57856C12AD2FD708A932DC4C91A48E662B37C4076A5D2BE54AC800EC1E6A13E1FC8EB61CA52E5D7B7608483E3BC225FBC62456AB46E39DA3CF45AB11A50", 16),
		"L": 32,
		"L_K": 16,
		"L_IV": 12,
	}
}

// No deep freeze: functions are replaced by their values and corresponding groups are instantiated once in the
// SecurityParameter's constructor. The object is frozen afterward.
Object.freeze(securityLevels)
