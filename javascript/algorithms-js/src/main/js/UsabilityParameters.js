/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { Alphabet } from "#Alphabet.js"
import { BYTE_SIZE } from "#util/sequence/ByteArray.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { IllegalArgumentError } from "#util/Errors.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { ceilLog, powerOfTwo } from "#util/math/Math.js"

/**
 * This class represents the usability parameters as defined in [Spec, Section 6.3].
 */
export class UsabilityParameters {
	_secParams
	_values

	/**
	 * Constructs a new instance based on a given usability configuration and security level.
	 * @param {string} UC - The usability configuration string
	 * @param {string} SL - The security level identifier
	 */
	constructor(UC, SL) {
		this._secParams = new SecurityParameters(SL)
		this._values = getUsabilityConfiguration(UC)
		Object.freeze(this._values)
		Object.freeze(this)
	}

	/**
	 * The identifier of the usability configuration provided by the parameters.
	 * @returns {string} - The identifier of the usability configuration
	 */
	get usabilityConfiguration() {
		return this._values.UC
	}

	/**
	 * The identifier of the security level provided by the parameters.
	 * @returns {string} - The identifier of the security level
	 */
	get securityLevel() {
		return this._secParams.securityLevel
	}


	// CREDENTIAL PARAMETERS

	/**
	 * The alphabet for encoding the voting credentials.
	 * @returns {Alphabet} - The alphabet for encoding the voting credentials
	 */
	get A_X() {
		return this._values.A_X
	}

	/**
	 * The alphabet for encoding the confirmation credentials.
	 * @returns {Alphabet} - The alphabet for encoding the confirmation credentials
	 */
	get A_Y() {
		return this._values.A_Y
	}

	/**
	 * The length (number of bits) of the voting credentials.
	 * @returns {number} - The length of the voting credentials
	 */
	get ell_X() {
		return ceilLog(powerOfTwo(2 * this._secParams.tau), this.A_X.length)
	}

	/**
	 * The length (number of bits) of the confirmation credentials.
	 * @returns {number} - The length of the confirmation credentials
	 */
	get ell_Y() {
		return ceilLog(powerOfTwo(2 * this._secParams.tau), this.A_Y.length)
	}


	// CODE PARAMETERS

	/**
	 * The alphabet used for the verification codes.
	 * @returns {Alphabet} - The alphabet used for the verification codes
	 */
	get A_V() {
		return this._values.A_V
	}

	/**
	 * The alphabet used for the participation and abstention codes.
	 * @returns {Alphabet} - The alphabet used for the participation and abstention codes
	 */
	get A_PA() {
		return this._values.A_PA
	}

	/**
	 * The maximal number of candidates.
	 * @returns {number} - The maximal number of candidates
	 */
	get n_max() {
		return this._values.n_max
	}

	/**
	 * The length (number of bits) of the verification codes.
	 * @returns {number} - The length of the verification codes
	 */
	get ell_V() {
		return ceilLog(powerOfTwo(BYTE_SIZE * this.L_V), this.A_V.length)
	}

	/**
	 * The length (number of bits) of the participation and abstention codes.
	 * @returns {number} - The length of the participation and abstention codes
	 */
	get ell_PA() {
		return ceilLog(powerOfTwo(BYTE_SIZE * this.L_PA), this.A_PA.length)
	}

	/**
	 * The length (number of bytes) of the verification codes.
	 * @returns {number} - The length of the verification codes
	 */
	get L_V() {
		return ceilLog(new BigInteger(Math.floor((this.n_max - 1) / (1 - this._secParams.epsilon))), 256)
	}

	/**
	 * Return the length (number of bytes) of the participation and abstention codes.
	 * @returns {number} - The length of the participation and abstention codes
	 */
	get L_PA() {
		return ceilLog(new BigInteger(Math.floor(1 / (1 - this._secParams.epsilon))), 256)
	}


	// WRITE-IN PARAMETERS

	/**
	 * The alphabet of allowed write-ins characters.
	 * @returns {Alphabet} - The alphabet of allowed write-ins characters
	 */
	get A_W() {
		return this._values.A_W
	}

	/**
	 * The maximal length of the write-ins.
	 * @returns {number} - The maximal length of the write-ins
	 */
	get ell_W() {
		return this._values.ell_W
	}

	/**
	 * The write-ins separation character.
	 * @returns {string} - The write-ins separation character
	 */
	get c_W() {
		return this._values.c_W
	}
}

// Regex for checking the format of the usability configuration string
const REGEX = /^(A(10|16|26|32|36|52|57|62|64)_){3}A(10|16|26|32|36|52|57|62|64)$/

// Alphabet configuration: maps an alphabet description to a concrete alphabet
const ALPHABETS = {
	A10: Alphabet.BASE_10,
	A16: Alphabet.BASE_16,
	A26: Alphabet.LATIN_26,
	A32: Alphabet.ALPHANUMERIC_32,
	A36: Alphabet.ALPHANUMERIC_36,
	A52: Alphabet.LATIN_52,
	A57: Alphabet.ALPHANUMERIC_57,
	A62: Alphabet.ALPHANUMERIC_62,
	A64: Alphabet.BASE_64
}

/**
 * Private helper function to convert a usability configuration string into a usability configuration object containing
 * the different alphabets and default values as defined in [Spec, Sections 9.1.1 and 11.1.2].
 * @private
 * @param {string} UC - The usability configuration string
 * @returns {object} - The usability configuration object
 */
function getUsabilityConfiguration(UC) {
	if (!REGEX.test(UC))
		throw new IllegalArgumentError()
	const keys = UC.split("_")
	return {
		UC:    UC,
		A_X:   ALPHABETS[keys[0]],
		A_Y:   ALPHABETS[keys[1]],
		A_V:   ALPHABETS[keys[2]],
		A_PA:  ALPHABETS[keys[3]],
		A_W:   Alphabet.LATIN_EXTENDED,
		c_W:   "|",
		ell_W: 80,
		n_max: 1678
	}
}
