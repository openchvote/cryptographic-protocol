/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { check } from "#util/Precondition.js"
import { divides } from "#util/math/Math.js"

/**
 * Algorithm 8.2
 * @param {number} x
 * @returns {boolean}
 */
export function IsPrime(x) {

	// PREPARATION
	check.Number(x)

	// ALGORITHM
	if (x <= 1) return false
	if (x <= 3) return true
	if (divides(2, x) || divides(3, x)) return false
	let i = 5
	while (i * i <= x) {
		if (divides(i, x) || divides(i + 2, x)) return false
		i = i + 6
	}
	return true
}
