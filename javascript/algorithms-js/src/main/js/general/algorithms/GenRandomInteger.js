/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger, ONE } from "#util/math/BigInteger.js"
import { ByteArrayToInteger } from "./ByteArrayToInteger.js"
import { PreconditionError } from "#util/Errors.js"
import { RandomBytes } from "./RandomBytes.js"
import { Set } from "#util/Set.js"
import { SetBit } from "./SetBit.js"
import { Vector } from "#util/sequence/Vector.js"
import { ZZ } from "#util/algebra/ZZ.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithms 4.11, 4.12, 4.13
 * @param {BigInteger} first
 * @param {BigInteger | Vector<BigInteger>} [second]
 * @returns {BigInteger} r
 */
export function GenRandomInteger(first, second) {

	// 4.11
	if (!second) {
		const n = first

		// PREPARATION
		check(Set.NN_plus.contains(n))

		// ALGORITHM
		const r = _GenRandomInteger(n)
		return r
	}
	// 4.12
	if (second instanceof Vector) {
		const n = first
		const X = second

		// PREPARATION
		check(Set.NN_plus.contains(n))
		check(Set.Vector(ZZ.of(n)).contains(X))
		let r

		// ALGORITHM
		do {
			r = _GenRandomInteger(n)
		} while (X.any(v => v.equals(r)))
		return r
	}
	// 4.13
	if (second instanceof BigInteger) {
		const a = first
		const b = second

		// PREPARATION
		check.Type([BigInteger, a], [BigInteger, b])
		check(a.compareTo(b) <= 0)

		// ALGORITHM
		const r_prime = _GenRandomInteger(b.subtract(a).add(ONE))
		const r = a.add(r_prime)
		return r
	}
	throw new PreconditionError()
}

/**
 * Algorithm 4.11
 * @private
 * @param {BigInteger} n
 * @returns {BigInteger} r
 */
function _GenRandomInteger(n) {

	// ALGORITHM
	const ell = n.subtract(ONE).bitLength()
	const L = Math.ceil(ell / 8)
	const k = 8 * L - ell
	let r
	do {
		let R = RandomBytes(L)
		for (let i = 1; i <= k; i++) {
			R = SetBit(R, 8 - i, 0)
		}
		r = ByteArrayToInteger(R)
	} while (r.compareTo(n) >= 0)
	return r
}
