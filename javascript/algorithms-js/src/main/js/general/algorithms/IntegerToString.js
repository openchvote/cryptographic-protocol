/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { Alphabet } from "#Alphabet.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { IntSet, Set } from "#util/Set.js"
import { StringBuilder } from "#util/StringBuilder.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 4.7
 * @param {BigInteger} x
 * @param {number} n
 * @param {Alphabet} A
 * @returns {string} S
 */
export function IntegerToString(x, n, A) {

	// PREPARATION
	check(Set.NN.contains(x))
	check(IntSet.NN.contains(n))
	check.Type(Alphabet, A)
	const N = new BigInteger(A.length)
	check(x.compareTo(N.pow(n)) < 0)
	const builder_S = new StringBuilder(n)

	// ALGORITHM
	for (let i = 1; i <= n; i++) {
		builder_S.setAt(n - i, A.getChar(x.mod(N).intValue()))
		x = x.divide(N)
	}
	const S = builder_S.toString()
	return S
}
