/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { ByteArray } from "#util/sequence/ByteArray.js"
import { IntSet } from "#util/Set.js"
import { check } from "#util/Precondition.js"

/**
 * Returns a ByteArray of `L` random bytes. Internally, the Web Crypto API's `getRandomValues` function  is called to
 * generate the random bytes. ATTENTION: In a practical use case, this function MUST be reviewed and probably be
 * adjusted. It MUST also be ensured that the `getRandomValues` function has not been maliciously tampered with or
 * replaced.
 * @param {number} L -  The desired length of the random byte array
 * @returns {ByteArray} B - The random byte array
 */
export function RandomBytes(L) {

	// PREPARATION
	check(IntSet.NN.contains(L))

	// ALGORITHM
	const arr = new Uint8Array(L)
	self.crypto.getRandomValues(arr)
	const B = ByteArray.of(arr)
	return B
}
