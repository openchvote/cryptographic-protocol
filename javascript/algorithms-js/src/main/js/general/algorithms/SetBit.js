/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { ByteArray } from "#util/sequence/ByteArray.js"
import { IntSet, Set } from "#util/Set.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 4.2
 * @param {ByteArray} B
 * @param {number} i
 * @param {number} b
 * @returns {ByteArray}
 */
export function SetBit(B, i, b) {

	// PREPARATION
	check(Set.B_star.contains(B))
	check(IntSet.ZZ(8 * B.length).contains(i))
	check(IntSet.BB.contains(b))

	//ALGORITHM
	const j = Math.floor(i / 8)
	const x = Math.pow(2, i % 8)
	let z
	if (b === 0) {
		z = B.getByte(j) & (255 - x)
	} else {
		z = B.getByte(j) | x
	}
	B = B.setByte(j, z)
	return B
}
