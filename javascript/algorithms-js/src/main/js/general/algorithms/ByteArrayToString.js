/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { Alphabet } from "#Alphabet.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { ByteArrayToInteger } from "./ByteArrayToInteger.js"
import { IntegerToString } from "./IntegerToString.js"
import { Set } from "#util/Set.js"
import { TWO } from "#util/math/BigInteger.js"
import { ceilLog } from "#util/math/Math.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 4.9
 * @param {ByteArray} B
 * @param {Alphabet} A
 * @returns {string} S
 */
export function ByteArrayToString(B, A) {

	// PREPARATION
	check(Set.B_star.contains(B))
	check.Type(Alphabet, A)
	const n = B.length

	// ALGORITHM
	const N = A.length
	const x_B = ByteArrayToInteger(B)
	const m = ceilLog(TWO.pow(8 * n), N)
	const S = IntegerToString(x_B, m, A)
	return S
}


/**
 * Algorithm 4.10
 * @param {ByteArray} B
 * @returns {string} S
 */
export function ByteArrayToStringUTF8(B) {

	// PREPARATION
	check(Set.UTF8.contains(B))
	const UTF8 = new TextDecoder("utf-8")

	// ALGORITHM
	const S = UTF8.decode(B.toByteArray())
	return S
}
