/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { AESEncrypt } from "#util/crypto/AES.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { Ciphertext } from "#general/model/Ciphertext.js"
import { GenRandomInteger } from "./GenRandomInteger.js"
import { RandomBytes } from "./RandomBytes.js"
import { RecHash } from "./RecHash.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Set } from "#util/Set.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 8.61
 * @param {BigInteger} pk
 * @param {ByteArray} M
 * @param {SecurityParameters} securityParams
 * @returns {Promise<Ciphertext>} (ek, IV, C)
 */
export async function GenCiphertext(pk, M, securityParams) {

	// PREPARATION
	check.Type(SecurityParameters, securityParams)
	const {GG_q_hat, g_hat, q_hat, L_K, L_IV} = securityParams

	check(GG_q_hat.contains(pk))
	check(Set.B_star.contains(M))

	// ALGORITHM
	const r = GenRandomInteger(q_hat)
	const ek = GG_q_hat.pow(g_hat, r)
	const K = RecHash(GG_q_hat.pow(pk, r)).truncate(L_K)
	const IV = RandomBytes(L_IV)
	const C = await AESEncrypt(K, IV, M)
	const e = new Ciphertext(ek, IV, C)
	return e
}
