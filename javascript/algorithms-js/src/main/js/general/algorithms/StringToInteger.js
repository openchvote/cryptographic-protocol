/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { Alphabet } from "#Alphabet.js"
import { BigInteger, ZERO } from "#util/math/BigInteger.js"
import { Set } from "#util/Set.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 4.8
 * @param {string} S
 * @param {Alphabet} A
 * @returns {BigInteger} x
 */
export function StringToInteger(S, A) {

	// PREPARATION
	check.Type(Alphabet, A)
	check(Set.String(A).contains(S))

	//ALGORITHM
	const N = new BigInteger(A.length)
	let x = ZERO
	for (let i = 0; i <= S.length - 1; i++) {
		x = N.multiply(x).add(new BigInteger(A.getRank(S[i])))
	}
	return x
}
