/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { AlgorithmError } from "#util/Errors.js"
import { BigInteger, ONE, TWO } from "#util/math/BigInteger.js"
import { IntSet } from "#util/Set.js"
import { IsPrime } from "../subalgorithms/IsPrime.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Vector } from "#util/sequence/Vector.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 8.1
 * @param {number} n
 * @param {SecurityParameters} securityParams
 * @returns {Vector<BigInteger>} bold_p
 */
export function GetPrimes(n, securityParams) {

	// PREPARATION
	check.Type(SecurityParameters, securityParams)
	const {p} = securityParams

	check(IntSet.NN.contains(n))
	const builder_bold_p = new Vector.Builder(n)

	// ALGORITHM
	let x = ONE
	for (let i = 1; i <= n; i++) {
		do {
			if (x.equals(ONE) || x.equals(TWO)) {
				x = x.add(ONE)
			} else {
				x = x.add(TWO)
			}
			if (x.compareTo(p) > 0) {
				throw new AlgorithmError("n is incompatible with p")
			}
		} while (!IsPrime(x.intValue()))
		builder_bold_p.set(i, x)
	}
	const bold_p = builder_bold_p.build()
	return bold_p
}
