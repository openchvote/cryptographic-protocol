/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { AlgorithmError } from "#util/Errors.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { Hash } from "#util/crypto/Hash.js"
import { Hashable } from "#util/Hashable.js"
import { IntSet } from "#util/Set.js"
import { IntegerToByteArray } from "./IntegerToByteArray.js"
import { Matrix } from "#util/sequence/Matrix.js"
import { StringToByteArray } from "./StringToByteArray.js"
import { Tuple } from "#util/Tuples.js"
import { Vector } from "#util/sequence/Vector.js"
import { check } from "#util/Precondition.js"

const PREFIX_NULL = ByteArray.of(new Uint8Array([0]))
const PREFIX_BYTE_ARRAY = ByteArray.of(new Uint8Array([1]))
const PREFIX_INTEGER = ByteArray.of(new Uint8Array([2]))
const PREFIX_STRING = ByteArray.of(new Uint8Array([3]))
const PREFIX_VECTOR = ByteArray.of(new Uint8Array([4]))
const PREFIX_MATRIX = ByteArray.of(new Uint8Array([5]))

/**
 * Algorithm 4.15
 * @param {*} values
 * @returns {ByteArray}
 */
export function RecHash(...values) {

	// PREPARATION
	const k = values.length
	check(IntSet.NN_plus.contains(k))

	//ALGORITHM
	let v
	if (k === 1) v = values[0]  // includes the case RecHash(null)
	else v = new Vector(values)

	if (v === null)
		return Hash(PREFIX_NULL)
	if (v instanceof ByteArray)
		return Hash(PREFIX_BYTE_ARRAY.concatenate(v))
	if (typeof v === "number")
		return RecHash(new BigInteger(v))
	if (v instanceof BigInteger)
		return Hash(PREFIX_INTEGER.concatenate(IntegerToByteArray(v)))
	if (typeof v === "string")
		return Hash(PREFIX_STRING.concatenate(StringToByteArray(v)))

	if (v instanceof Hashable) {
		let H = v.hash
		if (H === null) {
			if (v instanceof Tuple)
				H = Hash(PREFIX_VECTOR.concatenate(ByteArray.concatenate(...v.values.map(x => RecHash(x)))))
			if (v instanceof Vector)
				H = Hash(PREFIX_VECTOR.concatenate(ByteArray.concatenate(...v.map(x => RecHash(x)))))
			if (v instanceof Matrix) {
				const n = v.size[0]
				const builder_bold_v = new Vector.Builder(n)
				for (let i = 1; i <= n; i++) {
					const v_i = v.getRow(i)
					builder_bold_v.set(i, v_i)
				}
				const bold_v = builder_bold_v.build()
				H = Hash(PREFIX_MATRIX.concatenate(ByteArray.concatenate(...bold_v.map(x => RecHash(x)))))
			}
			v.hash = H
		}
		if (H !== null) return H
	}
	throw new AlgorithmError(`Type of v is not supported (${typeof v})`)
}
