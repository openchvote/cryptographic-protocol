/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { AESDecrypt } from "#util/crypto/AES.js"
import { BigInteger } from "#util/math/BigInteger.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { Ciphertext } from "#general/model/Ciphertext.js"
import { RecHash } from "./RecHash.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { Set } from "#util/Set.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 8.62
 * @param {BigInteger} sk
 * @param {Ciphertext} e (ek, IV, C)
 * @param {SecurityParameters} securityParams
 * @returns {Promise<ByteArray>} M
 */
export async function GetPlaintext(sk, e, securityParams) {

	// PREPARATION
	check.Type(SecurityParameters, securityParams)
	const {GG_q_hat, ZZ_q_hat, L_K, L_IV} = securityParams

	check(ZZ_q_hat.contains(sk))
	check(Set.Triple(GG_q_hat, Set.B(L_IV), Set.B_star).contains(e))
	const [ek, IV, C] = e.values

	// ALGORITHM
	const K = RecHash(GG_q_hat.pow(ek, sk)).truncate(L_K)
	const M = await AESDecrypt(K, IV, C)
	return M
}
