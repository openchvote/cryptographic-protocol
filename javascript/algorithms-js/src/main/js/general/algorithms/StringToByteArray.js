/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { ByteArray } from "#util/sequence/ByteArray.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 4.6
 * @param {string} S
 * @returns {ByteArray} B
 */
export function StringToByteArray(S) {

	// PREPARATION
	check.String(S)
	const UTF8 = new TextEncoder()

	// ALGORITHM
	const bytes = UTF8.encode(S)
	return ByteArray.of(bytes)
}
