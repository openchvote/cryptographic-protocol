/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { ByteArrayToInteger } from "./ByteArrayToInteger.js"
import { RecHash } from "./RecHash.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 8.4
 * @param {*} y
 * @param {*} t
 * @param {SecurityParameters} securityParams
 * @returns {BigInteger} c
 */
export function GetChallenge(y, t, securityParams) {

	// PREPARATION
	check.Type(SecurityParameters, securityParams)
	const {twoToTheTau} = securityParams

	check.notNull(y, t)

	// ALGORITHM
	const c = ByteArrayToInteger(RecHash(y, t)).mod(twoToTheTau)
	return c
}
