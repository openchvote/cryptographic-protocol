/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { ByteArray } from "#util/sequence/ByteArray.js"
import { Set } from "#util/Set.js"
import { check } from "#util/Precondition.js"

const b256 = new BigInteger(256)

/**
 * Algorithm 4.3 and 4.4
 * @param {BigInteger} x
 * @returns {ByteArray} B
 */
export function IntegerToByteArray(x) {

	// PREPARATION
	check(Set.NN.contains(x))

	// ALGORITHM
	const n = Math.ceil(x.bitLength() / 8)
	const builder_B = new ByteArray.Builder(n)
	for (let i = 1; i <= n; i++) {
		builder_B.setByte(n - i, x.mod(b256).intValue())
		x = x.divide(b256)
	}
	const B = builder_B.build()
	return B
}
