/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { ByteArray } from "#util/sequence/ByteArray.js"
import { Set } from "#util/Set.js"
import { SetBit } from "./SetBit.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 4.1
 * @param {ByteArray} B
 * @param {number} m
 * @param {number} n
 * @returns {ByteArray}
 */
export function SetWatermark(B, m, n) {

	// PREPARATION
	check(Set.B_star.contains(B))
	const b = 8 * B.length
	const l = (n - 1).toString(2).length
	check(0 <= m && m < n)
	check(l <= b)

	// ALGORITHM
	for (let j = 0; j <= l - 1; j++) {
		const i = Math.floor((j * b) / l)
		B = SetBit(B, i, m % 2)
		m = Math.floor(m / 2)
	}
	return B
}
