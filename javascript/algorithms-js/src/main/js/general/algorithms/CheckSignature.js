/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { BigInteger } from "#util/math/BigInteger.js"
import { GetChallenge } from "./GetChallenge.js"
import { Pair, Triple } from "#util/Tuples.js"
import { SecurityParameters } from "#SecurityParameters.js"
import { check } from "#util/Precondition.js"

/**
 * Algorithm 8.60
 * @param {Pair<BigInteger, BigInteger>} sigma
 * @param {BigInteger} pk
 * @param {*} m
 * @param {*} aux
 * @param {SecurityParameters} securityParams
 * @returns {boolean}
 */
export function CheckSignature(sigma, pk, m, aux, securityParams) {

	// PREPARATION
	check.Type(SecurityParameters, securityParams)
	const {GG_q_hat, ZZ_q_hat, ZZ_twoToTheTau} = securityParams
	const {p_hat, g_hat} = securityParams

	check.Type(Pair, sigma)
	const [c, s] = sigma.values
	check(ZZ_twoToTheTau.contains(c))
	check(ZZ_q_hat.contains(s))
	check(GG_q_hat.contains(pk))
	check.notNull(m)

	// ALGORITHM
	const t = pk.modPow(c, p_hat).multiply(g_hat.modPow(s, p_hat)).mod(p_hat)
	const y = aux === null ? new Pair(pk, m) : new Triple(pk, m, aux)
	const c_prime = GetChallenge(y, t, securityParams)
	return c.equals(c_prime)
}
