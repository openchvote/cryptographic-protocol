# Algorithms JS

This module provides a JavaScript implementation of the voting client relevant pseudocode algorithms 
described in the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325). Both protocol 
versions (*Plain* and *WriteIn*) are fully covered.

The module is entirely self-contained, i.e. it contains not only the bare algorithms but also all
utilities required for implementing the algorithms in a clear and consistent manner. Basically, it combines the 
voting client relevant functionality of the corresponding Java modules, `algorithms` and `utilities`, and 
translates it into JavaScript. The implementation closely follows the structure and design of the Java version, 
ensuring a straightforward orientation.


## Dependencies

The JavaScript code is written in VanillaJS with as few dependencies as possible. This ensures that the JavaScript 
code requires minimal processing during the build process and guarantees the highest possible level of transparency 
and the ability to examine and verify the cryptographic core executed by the voting client in production. Only two
dependencies are required in production: 

- [js-sha3](https://github.com/emn178/js-sha3): for the SHA-3 hash function
- [jsbn](https://github.com/andyperlitch/jsbn): for the big integer arithmetic

To provide the highest level of code quality a number of dependencies are needed during development:

- [mocha](https://mochajs.org), [chai](https://www.chaijs.com), [global](https://github.com/Raynos/global): for testing
- [c8](https://github.com/bcoe/c8): for test coverage
- [eslint](https://eslint.org/): for linting
- [jsdoc](https://github.com/jsdoc/jsdoc): for generating the JS documentation
- [rollup](https://rollupjs.org/): for building the production build

## Building

The build process is fully integrated into Maven and no external resources are required. 
[Node.js](https://nodejs.org) and [npm](https://github.com/npm/cli) are downloaded by the 
[frontend-maven-plugin](https://github.com/eirslett/frontend-maven-plugin) into the target folder and executed 
from there. 

The resulting artefact `algorithms-js-VERSION.zip` contains a bundle for each protocol version (plain and writein)
and for the three formats `es`, `umd`, and `iife`. In addition to the bundles, a website is built to 
run the tests in the browser. The file `target/dist/test_browser/test.html` can be opened in any browser, where 
the tests are executed automatically and the results are displayed.


## Development

During development, if Node.js/npm is locally installed, the following commands can be used in the current directory:

```sh
# Install dependencies
npm ci

# Lint
npm run lint
npm run lint-fix

# Test
npm run test

# Test coverage
npm run coverage

# Build for deployment
npm run build

# Generate documentation
npm run doc
```
`

