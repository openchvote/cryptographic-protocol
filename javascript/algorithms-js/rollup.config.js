import fs from "fs"

import commonjs from "@rollup/plugin-commonjs"
import multiEntry from "@rollup/plugin-multi-entry"
import replace from "@rollup/plugin-replace"
import {nodeResolve} from "@rollup/plugin-node-resolve"

export default [
	// Tests
	{
		input: {
			include: ["src/test/js/**/*.test.js"]
		},
		output: {
			file: "./target/dist/test_browser/tests.js",
			format: "es"
		},
		external: [/^mocha.*/],
		plugins: [
			multiEntry(),
			commonjs(),
			nodeResolve({browser: true}),
			replace({
				preventAssignment: true,
				"import { describe, it } from 'mocha';": "",
				delimiters: ["", ""]
			}),
			copy([
				"src/test/js/test.html",
				"node_modules/mocha/mocha.js",
				"node_modules/mocha/mocha.css"
			])
		]
	},

	// Build plain
	{
		input: [
			"src/main/js/*.js",
			"src/main/js/util/**/*.js",
			"src/main/js/general/**/*.js",
			"src/main/js/protocols/common/**/*.js",
			"src/main/js/protocols/plain/**/*.js"
		],
		output: [
			{
				format: "es",
				file: "target/dist/algorithms.plain.js"
			},
			{
				format: "umd",
				name: "algorithms.plain",
				file: "target/dist/algorithms.plain.umd.js"
			},
			{
				format: "iife",
				name: "algorithms.plain",
				file: "target/dist/algorithms.plain.iife.js"
			}
		],
		plugins: [
			multiEntry(),
			commonjs(),
			nodeResolve({browser: true})
		]
	},

	// Build write-in
	{
		input: [
			"src/main/js/*.js",
			"src/main/js/util/**/*.js",
			"src/main/js/general/**/*.js",
			"src/main/js/protocols/common/**/*.js",
			"src/main/js/protocols/writein/**/*.js"
		],
		output: [
			{
				format: "es",
				file: "target/dist/algorithms.writein.js"
			},
			{
				format: "umd",
				name: "algorithms.writein",
				file: "target/dist/algorithms.writein.umd.js"
			},
			{
				format: "iife",
				name: "algorithms.writein",
				file: "target/dist/algorithms.writein.iife.js"
			}
		],
		plugins: [
			multiEntry(),
			commonjs(),
			nodeResolve({browser: true})
		]
	},
]


/**
 * @param {string[]} files
 * @returns {object}
 */
function copy(files) {
	return {
		name: "copy",
		async generateBundle() {
			files.forEach(file => {
				this.emitFile({
					type: "asset",
					fileName: file.substring(file.lastIndexOf("/")+1),
					source: fs.readFileSync(file)
				})
			})
		}
	}
}
