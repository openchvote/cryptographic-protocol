# Algorithms JS Service

The sole purpose of this module is to provide an `AlgorithmService` for the JavaScript implementation of the voting 
client algorithms (`algorithms-js`). This module is of no practical use in a real-world application. It is only needed 
for the integration of the JavaScript algorithms into the simulator, demonstrating the seamless interaction of the 
algorithms across language boundaries.

Since Java does not provide an efficient possibility to execute JavaScript code within the JVM, algorithm calls must be 
delegated to an external JavaScript environment. Thus, the content of this module is twofold: on one hand, it includes 
the Java implementation of the `AlgorithmService`, which primarily serializes the algorithm arguments and invokes the
respective endpoint of the Node.js server; on the other hand, it encompasses the implementation of the Node.js server 
itself. The Node.js server has also only a very limited functionality. It simply listens for algorithm execution 
requests, deserializes the arguments, executes the corresponding algorithm, and responds with the serialized result.

The Node.js server can be started either manually or automatically by the service. To start the server manually, simply
execute the command `npm start` in the root directory of this module. Additionally, configure the service to use an 
external server by setting the configuration property `userExternalServer` to `true`. By default, the service starts 
the server automatically within a Docker container. During the Maven build process, a Docker image is created and 
registered in the local Docker registry. Upon the first algorithm call, the service uses 
[Testcontainers](https://java.testcontainers.org) to create and start a container based on the registered image. 

Please note, that the Docker image is registered during the Maven `compile` phase rather than the `package` phase. 
This is because during the `test` phase the simulator utilizes the `JavaScriptAlgorithmService`, if available, and 
therefore, the image must already be accessible. Also note that we only use Docker as the lowest common denominator to 
prevent non-JavaScript developer from needing to install Node.js locally.

## Maven Dependencies

The following dependency tree shows the Maven dependencies of this module. `slf4j` is only needed as logging framework
by the `testcontainers`.

```console
mvn dependency:tree -Dscope=compile -Dverbose=true
```

Output:

```text
[INFO] io.gitlab.openchvote:services-js:jar:2.4
[INFO] +- io.gitlab.openchvote:algorithms:jar:2.4:compile
[INFO] |  \- io.gitlab.openchvote:utilities:jar:2.4:compile
[INFO] |     \- com.verificatum:vmgj:jar:1.2.2:compile
[INFO] +- io.gitlab.openchvote:algorithms-js:zip:2.4:compile
[INFO] +- org.testcontainers:testcontainers:jar:1.20.4:compile
[INFO] |  +- junit:junit:jar:4.13.2:compile
[INFO] |  |  \- org.hamcrest:hamcrest-core:jar:1.3:compile
[INFO] |  \- org.rnorth.duct-tape:duct-tape:jar:1.0.8:compile
[INFO] |     \- org.jetbrains:annotations:jar:17.0.0:compile
[INFO] +- com.github.docker-java:docker-java-api:jar:3.4.1:compile
[INFO] |  \- com.fasterxml.jackson.core:jackson-annotations:jar:2.10.3:compile
[INFO] +- com.github.docker-java:docker-java-transport-zerodep:jar:3.4.1:compile
[INFO] |  +- com.github.docker-java:docker-java-transport:jar:3.4.1:compile
[INFO] |  \- net.java.dev.jna:jna:jar:5.13.0:compile
[INFO] +- org.slf4j:slf4j-api:jar:2.0.7:compile
[INFO] \- org.slf4j:slf4j-nop:jar:2.0.7:compile
[INFO]    \- (org.slf4j:slf4j-api:jar:2.0.7:compile - omitted for duplicate)
```

## JavaScript Dependencies

The only external JavaScript dependency is [Express](http://expressjs.com), needed to simplify the implementation of 
the Node.js server.  

## Java Module Dependencies

The following module dependency diagram illustrates the Java module dependencies to other modules within this project:

![Java module dependencies](img/services.svg)
