/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.javascript.services.JavaScriptAlgorithmService;

/**
 * This module provides a JavaScript algorithm service.
 */
module ch.openchvote.javascript.services {
    requires transitive ch.openchvote.core.algorithms;
    requires java.net.http;
    requires testcontainers;
    requires org.slf4j;
    requires java.sql; // required by testcontainers/slf4j
    requires com.fasterxml.jackson.annotation; // required by testcontainers/slf4j
    requires org.jetbrains.annotations; // required by testcontainers/slf4j

    exports ch.openchvote.javascript.services; // For JavaDoc
    provides AlgorithmService with JavaScriptAlgorithmService;
}
