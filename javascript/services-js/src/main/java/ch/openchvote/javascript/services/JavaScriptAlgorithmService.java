/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.javascript.services;

import ch.openchvote.base.utilities.serializer.Serializer;
import ch.openchvote.base.utilities.tuples.Tuple;
import ch.openchvote.core.algorithms.Algorithm;
import ch.openchvote.core.algorithms.AlgorithmService;
import ch.openchvote.core.algorithms.parameters.security.SecurityParameters;
import ch.openchvote.core.algorithms.parameters.usability.UsabilityParameters;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.Properties;

import static java.lang.System.Logger.Level.INFO;
import static java.lang.System.Logger.Level.TRACE;

/**
 * This class implements the {@link AlgorithmService} interface for the JavaScript implementation of the voting client
 * algorithms within the algorithms-js module. The language gap between Java and JavaScript is bridged by delegating the
 * execution of the algorithms to an external Node.js server. For that, the algorithm arguments are serialized and sent
 * via an HTTP request to the respective server endpoint. The server then deserializes the arguments, calls the
 * algorithm, and responds with the serialized result. The Node.js server can either be started manually or
 * automatically by the service. For the service to automatically start the server on the first algorithm call, Docker
 * must be installed and the image containing the Node.js server must be available in the Docker's registry.
 */
public class JavaScriptAlgorithmService implements AlgorithmService {

    // name of the configuration file
    private static final String CONFIG_FILE_NAME = "/config.properties";

    // default configuration values
    private static final String DEFAULT_DOCKER_IMAGE_NAME = "openchvote-algorithms-js:latest";
    private static final int DEFAULT_SERVER_PORT = 8000;
    private static final boolean DEFAULT_USE_EXTERNAL_SERVER = false;

    // private fields
    private static String dockerImageName;
    private static int serverPort;
    private static boolean useExternalServer;

    private static System.Logger logger;
    private static GenericContainer<?> container;

    // load configuration
    {
        try (var inputStream = this.getClass().getResourceAsStream(CONFIG_FILE_NAME)) {
            Properties properties = new Properties();
            properties.load(inputStream);
            dockerImageName = properties.getProperty("dockerImageName", DEFAULT_DOCKER_IMAGE_NAME);
            serverPort = Integer.parseInt(properties.getProperty("serverPort", String.valueOf(DEFAULT_SERVER_PORT)));
            useExternalServer = Boolean.parseBoolean(properties.getProperty("useExternalServer", Boolean.toString(DEFAULT_USE_EXTERNAL_SERVER)));
            logger = System.getLogger(this.getClass().getSimpleName());
        } catch (IOException exception) {
            throw new RuntimeException("Configuration file not found: " + CONFIG_FILE_NAME);
        } catch (NumberFormatException exception) {
            throw new RuntimeException("Invalid configuration file: " + CONFIG_FILE_NAME);
        }
    }

    // --- Public interface ---

    @Override
    public Language getLanguage() {
        return Language.JAVASCRIPT;
    }

    @Override
    public <R> R run(Class<? extends Algorithm<R>> algorithm, Object... arguments) throws Algorithm.Exception {
        var protocolName = algorithm.getPackageName().split("\\.")[5];
        var algorithmName = algorithm.getSimpleName();
        var tuple = Tuple.of(arguments);
        var payload = Serializer.serialize(tuple);
        var ret = callAlgorithm(protocolName, algorithmName, payload);
        return Serializer.deserialize(ret, Algorithm.getReturnType(algorithm));
    }

    @Override
    public <R> R run(Class<? extends Algorithm<R>> algorithm, SecurityParameters securityParameters, Object... arguments) throws Algorithm.Exception {
        var extArguments = Arrays.copyOf(arguments, arguments.length + 1);
        extArguments[extArguments.length - 1] = securityParameters.getSecurityLevel();
        return run(algorithm, extArguments);
    }

    @Override
    public <R> R run(Class<? extends Algorithm<R>> algorithm, UsabilityParameters usabilityParameters, Object... arguments) throws Algorithm.Exception {
        var extArguments = Arrays.copyOf(arguments, arguments.length + 2);
        extArguments[extArguments.length - 2] = usabilityParameters.getSecurityLevel();
        extArguments[extArguments.length - 1] = usabilityParameters.getUsabilityConfiguration();
        return run(algorithm, extArguments);
    }

    @Override
    public <R> R run(Class<? extends Algorithm<R>> algorithm, SecurityParameters securityParameters, UsabilityParameters usabilityParameters, Object... arguments) throws Algorithm.Exception {
        var extArguments = Arrays.copyOf(arguments, arguments.length + 2);
        extArguments[extArguments.length - 2] = securityParameters.getSecurityLevel();
        extArguments[extArguments.length - 1] = usabilityParameters.getUsabilityConfiguration();
        return run(algorithm, extArguments);
    }

    @SuppressWarnings("HttpUrlsUsage")
    // private helper method for calling a JavaScript algorithm over http
    static private String callAlgorithm(String protocolName, String algorithmName, String payload) {
        if (!useExternalServer) {
            startContainer();
        }
        try {
            String host = useExternalServer ? "localhost" : container.getHost();
            int port = useExternalServer ? serverPort : container.getFirstMappedPort();
            var request = HttpRequest.newBuilder()
                    .uri(URI.create("http://" + host + ":" + port + "/" + protocolName + "/" + algorithmName))
                    .header("Content-Type", "text/plain; charset=UTF-8")
                    .POST(HttpRequest.BodyPublishers.ofString(payload))
                    .build();
            logTrace("Request:  " + request);

            var context = SSLContext.getInstance("TLSv1.3", "SunJSSE");
            context.init(null, null, null);

            try (var client = HttpClient.newHttpClient()) {
                HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
                logTrace("Response: " + response + "\n                    " + response.body());
                if (response.statusCode() == 408) return callAlgorithm(protocolName, algorithmName, payload);
                return response.body();
            }
        } catch (Exception exception) {
            throw new RuntimeException(String.format("Calling '%s' via JavaScriptAlgorithmService failed", algorithmName), exception);
        }
    }

    @SuppressWarnings("resource")
    // private helper method for starting a new docker container
    static private synchronized void startContainer() {
        if (container == null) {
            try {
                container = new GenericContainer<>(dockerImageName)
                        .withExposedPorts(serverPort)
                        .withLogConsumer(msg -> logTrace("JS", msg.getUtf8StringWithoutLineEnding()))
                        .waitingFor(Wait.forLogMessage(".*Server ready on port.*\\n", 1));
                container.start();
                logger.log(INFO, "System    : JavaScript server started (computations will be slower)");
            } catch (Exception exception) {
                throw new RuntimeException("Starting docker container failed", exception);
            }
        }
    }

    // private logger method (level=TRACE)
    static private void logTrace(String message) {
        logTrace("J ", message);
    }

    // private logger method (level=TRACE)
    static private void logTrace(String language, String message) {
        logger.log(TRACE, String.format("JSAS-%s %s", language, message));
    }

}
