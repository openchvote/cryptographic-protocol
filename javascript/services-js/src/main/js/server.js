/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import express from "express"
import algorithms from "./util/algorithms-adapter.js"
import deserializers from "./util/deserializers.js"

import config from "../resources/config.json" with { type: "json" }

const PORT = config.port || 8000
const MAX_REQUEST_BODY_SIZE = config.maximumRequestBodySize || "10mb"

typeof self == "undefined" && typeof global == "object" && (global.self = global)

const app = express()
app.use(express.text({limit: MAX_REQUEST_BODY_SIZE}))

app.get("/", (req, res) => {
	res.send("OpenCHVote Algorithms JS")
})

app.post("/:protocol/:algorithm", async (req, res) => {
	console.log(`Request:  ${req.params.algorithm}\n          ${req.body}`)
	const protocol = req.params.protocol
	const algorithm = req.params.algorithm
	try {
		if (!algorithms[protocol]?.[algorithm]) {
			res.status(404).send(`Unknown algorithm '${protocol}/${algorithm}'`)
		} else {
			const alg = algorithms[protocol][algorithm]
			const paramsDeserializer = deserializers[protocol][algorithm]
			const params = paramsDeserializer.deserialize(req.body)
			const ret = alg(...params)
			res.send(ret)
		}
	} catch(ex) {
		console.log(ex)
		res.status(500).send(`Executing algorithm '${protocol}/${algorithm}' failed!`)
	}
})

app.listen(PORT, () => {
	console.log(`Server ready on port ${PORT}`)
})
