/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { SecurityParameters, UsabilityParameters, serialize } from "#lib/algorithms.plain.js"
import * as algs from "#lib/algorithms.plain.js"

export function CheckKeyPairProof(pi, pk, SL) {
	return serialize(algs.CheckKeyPairProof(pi, pk, new SecurityParameters(SL)))
}

export function GenBallot(X, bold_s, pk, bold_n, w, SL, UC) {
	return serialize(algs.GenBallot(X, bold_s, pk, bold_n, w, new SecurityParameters(SL), new UsabilityParameters(UC, SL)))
}
