/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import {
	BigIntegerDeserializer,
	IntegerDeserializer,
	StringDeserializer,
	ByteArrayDeserializer,
	TupleDeserializer,
	PairDeserializer,
	VectorDeserializer,
	MatrixDeserializer,
	Response,
	Finalization
} from "#lib/algorithms.plain.js"

export const GetPublicKey = new TupleDeserializer(
	new VectorDeserializer(new BigIntegerDeserializer()),
	new StringDeserializer())

export const GetPointMatrix = new TupleDeserializer(
	new VectorDeserializer(new IntegerDeserializer()),
	new VectorDeserializer(new Response.Deserializer()),
	new VectorDeserializer(new IntegerDeserializer()),
	new VectorDeserializer(new BigIntegerDeserializer()),
	new StringDeserializer())

export const GetVerificationCodes = new TupleDeserializer(
	new VectorDeserializer(new IntegerDeserializer()),
	new MatrixDeserializer(new PairDeserializer(new BigIntegerDeserializer(), new BigIntegerDeserializer())),
	new StringDeserializer(),
	new StringDeserializer())

export const GenConfirmation = new TupleDeserializer(
	new StringDeserializer(),
	new MatrixDeserializer(new PairDeserializer(new BigIntegerDeserializer(), new BigIntegerDeserializer())),
	new StringDeserializer(),
	new StringDeserializer())

export const GetParticipationCode = new TupleDeserializer(
	new VectorDeserializer(new Response.Deserializer()),
	new VectorDeserializer(new Finalization.Deserializer()),
	new VectorDeserializer(new IntegerDeserializer()),
	new VectorDeserializer(new BigIntegerDeserializer()),
	new VectorDeserializer(new IntegerDeserializer()),
	new VectorDeserializer(new IntegerDeserializer()),
	new VectorDeserializer(new IntegerDeserializer()),
	new BigIntegerDeserializer(),
	new StringDeserializer(),
	new StringDeserializer())

export const GetInspectionCode = new TupleDeserializer(
	new VectorDeserializer(new ByteArrayDeserializer()),
	new StringDeserializer(),
	new StringDeserializer())
