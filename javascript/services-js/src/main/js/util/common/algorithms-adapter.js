/*
 * Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
import { SecurityParameters, UsabilityParameters, serialize } from "#lib/algorithms.plain.js"
import * as algs from "#lib/algorithms.plain.js"

export function GetPublicKey(bold_pk, SL) {
	return serialize(algs.GetPublicKey(bold_pk, new SecurityParameters(SL)))
}

export function GetPointMatrix(bold_n, bold_beta, bold_s, bold_r, SL) {
	return serialize(algs.GetPointMatrix(bold_n, bold_beta, bold_s, bold_r, new SecurityParameters(SL)))
}

export function GetVerificationCodes(bold_s, bold_P, SL, UC) {
	return serialize(algs.GetVerificationCodes(bold_s, bold_P, new SecurityParameters(SL), new UsabilityParameters(UC, SL)))
}

export function GenConfirmation(Y_v, bold_P, SL, UC) {
	return serialize(algs.GenConfirmation(Y_v, bold_P, new SecurityParameters(SL), new UsabilityParameters(UC, SL)))
}

export function GetParticipationCode(bold_beta, bold_delta, bold_s, bold_r, bold_n, bold_k, bold_e_v, pk, SL, UC) {
	return serialize(algs.GetParticipationCode(bold_beta, bold_delta, bold_s, bold_r, bold_n, bold_k, bold_e_v, pk, new SecurityParameters(SL), new UsabilityParameters(UC, SL)))
}

export function GetInspectionCode(bold_i, SL, UC) {
	return serialize(algs.GetInspectionCode(bold_i, new UsabilityParameters(UC, SL)))
}
