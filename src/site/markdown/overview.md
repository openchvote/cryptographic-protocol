## Project Overview

The origin of this project is a collaboration between the State of Geneva and the Bern University of Applied Sciences,
which lasted from 2016 until 2019. The aforementioned CHVote Protocol Specification document is the main output of this
collaboration.

The software development for this project started in October 2019. At an early stage of the project, the code released
by the State of Geneva in June 2019 at <https://chvote2.gitlab.io> served as a reference point for the software design
in this project. Otherwise, the two projects have nothing in common.

The general goal of this project is to pursue the achievements of CHVote project and to make them available to the
public under a free [license](license.html). By releasing the source code to the public, we invite the
community to participate in this project.
