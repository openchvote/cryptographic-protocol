## About OpenCHVote Internet Voting System

This OpenCHVote system is a concise Java implementation of the CHVote voting protocol. The protocol specification is
published on the *Cryptology ePrint Archive* since 2017. Current and earlier versions of this document are available
at <https://eprint.iacr.org/2017/325>. The current Version 2.4.2 of this software matches with Version 4.3 of the
specification document.
