## Scope

In the design of the system, we introduced a strict separation between *cryptographically relevant* and *
cryptographically irrelevant* components. As the scope of our software is limited to the cryptographically relevant
components, OpenCHVote does not provide a complete Internet voting system that is ready to use in practice. However, the
current software allows simulating elections events by executing the CHVote protocol on a single machine. All
cryptographically relevant aspects of the protocol are included in such a simulation.

Within this scope, the implementation is centered around the CHVote voting protocol. The code covers almost all relevant
aspects of the specification document, including the extended protocol that allows write-in candidates and the proposed
performance optimizations. In order to achieve the highest possible degree of correspondence, we looked at the
specification and the code as two facets of *the same thing*. In such a mindset, deviations are only acceptable in
exceptional cases, which need to be well documented.
