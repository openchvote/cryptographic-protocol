## License

For this software, the [GNU AFFERO GENERAL PUBLIC LICENSE](https://www.gnu.org/licenses/#AGPL) Version 3, 19 November 2007, applies.
