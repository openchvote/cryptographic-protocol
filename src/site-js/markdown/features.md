## Features

### Key Features

The current OpenCHVote implementation provides the following key features:

* Complete parameterizable protocol run.
* Simultaneous execution of multiple protocol runs.
* Two protocol variants: *Plain* (basic protocol without write-ins) and *WriteIn* (extended protocol with write-ins).
* Performance optimizations: fixed-based exponentiations, product exponentiations, group membership tests, caching of
  recursive hash values, etc.
* Infrastructure interfaces to cryptographically irrelevant components (messaging service, persistence service, logging,
  etc.).
* No compulsory dependencies to third-party libraries or frameworks (dependencies
  to [VMGJ](https://github.com/verificatum/verificatum-vmgj) and [Spock](http://spockframework.org/) are optional).
* No dependencies to non-JavaSE technologies.

### Missing Features

* A software for performing the universal verification (the *Verifier*).
* A JavaScript implementation of the voting client.
* Reliable and secure infrastructure services.
* Other infrastructure software components.
* Strategy to recover from error states.
* Rigorous testing of algorithms and framework.
* Update of formal proofs.
