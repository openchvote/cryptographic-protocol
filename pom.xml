<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!--

    Copyright (C) 2024-2025 Berner Fachhochschule https://e-voting.bfh.ch

     - This program is free software: you can redistribute it and/or modify                           -
     - it under the terms of the GNU Affero General Public License as published by                    -
     - the Free Software Foundation, either version 3 of the License, or                              -
     - (at your option) any later version.                                                            -
     -                                                                                                -
     - This program is distributed in the hope that it will be useful,                                -
     - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
     - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
     - GNU General Public License for more details.                                                   -
     -                                                                                                -
     - You should have received a copy of the GNU Affero General Public License                       -
     - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>io.gitlab.openchvote</groupId>
    <artifactId>openchvote</artifactId>
    <version>2.4.2</version>
    <packaging>pom</packaging>

    <name>openchvote</name>
    <description>
        OpenCHVote is an implementation of the CHVote Internet e-voting system targeted for Switzerland. CHVote itself
        denotes a rigorous, formal specification of an e-voting system.
    </description>
    <url>https://openchvote.gitlab.io/cryptographic-protocol/</url>

    <organization>
        <name>Berner Fachhochschule, Switzerland - E-Voting Group</name>
        <url>https://e-voting.bfh.ch</url>
    </organization>

    <developers>
        <!-- Alphabetical order -->
        <developer>
            <id>due1</id>
            <name>Dubuis, Eric</name>
            <email>eric.dubuis@bfh.ch</email>
            <roles>
                <role>architect</role>
                <role>developer</role>
            </roles>
        </developer>
        <developer>
            <id>hnr1</id>
            <name>Haenni, Rolf</name>
            <email>rolf.haenni@bfh.ch</email>
            <roles>
                <role>project leader</role>
                <role>architect</role>
                <role>developer</role>
            </roles>
        </developer>
        <developer>
            <id>knr1</id>
            <name>Koenig, Reto E.</name>
            <email>reto.koenig@bfh.ch</email>
            <roles>
                <role>developer</role>
                <role>reviewer</role>
            </roles>
        </developer>
        <developer>
            <id>lhp2</id>
            <name>Locher, Philipp</name>
            <email>philipp.locher@bfh.ch</email>
            <roles>
                <role>developer</role>
                <role>reviewer</role>
            </roles>
        </developer>
    </developers>

    <modules>
        <!-- include only non-JavaScript modules -->
        <module>base</module>
        <module>core</module>
        <module>simulation</module>
    </modules>

    <profiles>
        <profile>
            <id>javascript</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <modules>
                <!-- include all modules -->
                <module>base</module>
                <module>core</module>
                <module>javascript</module>
                <module>simulation</module>
            </modules>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-site-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>root-site-generation</id>
                                <configuration>
                                    <siteDirectory>src/site-js</siteDirectory>
                                </configuration>
                                <!-- Ensure this execution applies only to the root module -->
                                <goals>
                                    <goal>site</goal>
                                </goals>
                                <phase>site</phase>
                                <inherited>false</inherited>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>dependency-check</id>
            <reporting>
                <plugins>
                    <plugin>
                        <groupId>org.owasp</groupId>
                        <artifactId>dependency-check-maven</artifactId>
                        <version>11.1.1</version>
                        <configuration>
                            <nvdApiKey>807a44f9-f847-4aae-96b2-1dbe17f04014</nvdApiKey>
                            <failBuildOnCVSS>7</failBuildOnCVSS>
                            <format>HTML</format>
                            <outputDirectory>${project.build.directory}/dependency-check-report</outputDirectory>
                        </configuration>
                    </plugin>
                </plugins>
            </reporting>
        </profile>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>3.3.1</version>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar-no-fork</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <configuration>
                            <release>${maven.compiler.release}</release>
                            <show>public</show>
                            <doclint>none</doclint>
                        </configuration>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>3.2.3</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.7.0</version>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>ossrh</serverId>
                            <nexusUrl>https://s01.oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>false</autoReleaseAfterClose>
                            <keepStagingRepositoryOnFailure>true</keepStagingRepositoryOnFailure>
                            <keepStagingRepositoryOnCloseRuleFailure>true</keepStagingRepositoryOnCloseRuleFailure>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <licenses>
        <license>
            <name>GNU AFFERO GENERAL PUBLIC LICENSE</name>
            <url>https://www.gnu.org/licenses/#AGPL</url>
        </license>
    </licenses>

    <inceptionYear>2024</inceptionYear>

    <scm>
        <connection>scm:git:https://gitlab.com/openchvote/cryptographic-protocol.git</connection>
        <developerConnection>scm:git:NOT-MEANT-FOR-THE-PUBLIC</developerConnection>
        <url>https://gitlab.com/openchvote/cryptographic-protocol</url>
    </scm>

    <distributionManagement>
        <site>
            <id>openchvote</id>
            <url>https://gitlab.com/openchvote</url>
        </site>
        <snapshotRepository>
            <id>ossrh</id>
            <url>https://s01.oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <url>https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>

    <repositories>
        <repository>
            <id>local-maven-repo</id>
            <!-- Explicitly set on Maven's command line with -Dmyproject.basedir=$(pwd) or implicitly by directory plugin -->
            <!--suppress UnresolvedMavenProperty -->
            <url>file://${myproject.basedir}/local-maven-repo</url>
        </repository>
    </repositories>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>21</maven.compiler.source>
        <maven.compiler.target>21</maven.compiler.target>
        <maven.compiler.release>21</maven.compiler.release>
        <mycila.license.plugin.version>4.6</mycila.license.plugin.version>
        <spock.core.version>2.3-groovy-4.0</spock.core.version>
        <groovy.version>4.0.21</groovy.version>
    </properties>

    <!-- Modules are defined in respective profiles -->

    <dependencyManagement>
        <dependencies>
            <!-- Needed for the Spock framework -->
            <!-- See: https://blog.devgenius.io/find-and-fix-transitive-dependency-version-upgrade-conflicts-in-maven-7ffb4ceae683 -->
            <dependency>
                <groupId>org.apache.groovy</groupId>
                <artifactId>groovy</artifactId>
                <version>${groovy.version}</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.11.0</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>exec-maven-plugin</artifactId>
                    <version>3.5.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <version>3.6.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>3.3.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>3.11.1</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-gpg-plugin</artifactId>
                    <version>3.1.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>3.6.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-clean-plugin</artifactId>
                    <version>3.2.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>3.1.1</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-install-plugin</artifactId>
                    <version>3.1.1</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>3.3.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>3.3.1</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-site-plugin</artifactId>
                    <version>3.21.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-project-info-reports-plugin</artifactId>
                    <version>3.8.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jxr-plugin</artifactId>
                    <version>3.6.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>3.2.5</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-failsafe-plugin</artifactId>
                    <version>3.1.2</version>
                </plugin>
                <plugin>
                    <groupId>com.mycila</groupId>
                    <artifactId>license-maven-plugin</artifactId>
                    <version>${mycila.license.plugin.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <!-- See: https://maven.apache.org/enforcer/enforcer-rules/requireMavenVersion.html -->
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>3.3.0</version>
                <executions>
                    <execution>
                        <id>enforce-maven</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireMavenVersion>
                                    <version>3.3.9</version>
                                </requireMavenVersion>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>exec-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>exec</goal>
                        </goals>
                    </execution>
                </executions>
                <!-- Prevent the execution of Java in the root -->
                <!-- See: https://stackoverflow.com/questions/3459928/running-a-specific-maven-plugin-goal-from-the-command-line-in-a-sub-module-of-a/26448447#26448447 -->
                <configuration>
                    <skip>true</skip>
                    <executable>java</executable>
                    <mainClass>none</mainClass>
                </configuration>
            </plugin>
            <!-- https://mvnrepository.com/artifact/org.codehaus.gmavenplus/gmavenplus-plugin -->
            <plugin>
                <groupId>org.codehaus.gmavenplus</groupId>
                <artifactId>gmavenplus-plugin</artifactId>
                <version>3.0.2</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>compileTests</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>21</source>
                    <target>21</target>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <includes>
                        <include>**/*Spec</include>
                        <include>**/*Test</include>
                    </includes>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-failsafe-plugin</artifactId>
            </plugin>
            <!-- See: https://github.com/jdcasey/directory-maven-plugin -->
            <!-- To be used for: -->
            <!-- - Mycila's license-maven-plugin -->
            <!-- - Locating the project-local Maven repository for VMGJ wrapper artifact -->
            <plugin>
                <groupId>org.commonjava.maven.plugins</groupId>
                <artifactId>directory-maven-plugin</artifactId>
                <version>0.1</version>
                <executions>
                    <execution>
                        <id>directories</id>
                        <goals>
                            <goal>directory-of</goal>
                        </goals>
                        <phase>initialize</phase>
                        <configuration>
                            <property>myproject.basedir</property>
                            <project>
                                <groupId>io.gitlab.openchvote</groupId>
                                <artifactId>openchvote</artifactId>
                            </project>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <!-- See: https://mycila.carbou.me/license-maven-plugin/ -->
                <!-- Standalone usage: mvn initialize license:check [-Pjavascript] -->
                <groupId>com.mycila</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <configuration>
                    <properties>
                        <owner>Berner Fachhochschule https://e-voting.bfh.ch</owner>
                        <email>chvote@bfh.ch</email>
                        <year>2025</year>
                    </properties>
                    <aggregate>true</aggregate>
                    <licenseSets>
                        <licenseSet>
                            <!--suppress UnresolvedMavenProperty -->
                            <header>${myproject.basedir}/etc/license-header.txt</header>
                            <excludes>
                                <exclude>**/Readme.md</exclude>
                                <exclude>**/LICENSE.txt</exclude>
                                <exclude>**/Dockerfile</exclude>
                                <exclude>**/test.html</exclude>
                                <exclude>**/*.cjs</exclude>
                            </excludes>
                            <includes>
                                <include>src/**/java/**/*.java</include>
                                <include>src/**/js/**/*.js</include>
                                <include>src/**/groovy/**/*.groovy</include>
                                <include>/**/pom.xml</include>
                            </includes>
                        </licenseSet>
                    </licenseSets>
                </configuration>
                <executions>
                    <execution>
                        <id>first</id>
                        <goals>
                            <goal>check</goal>
                        </goals>
                        <phase>process-sources</phase>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <!-- default groupId is org.apache.maven.plugins -->
                <artifactId>maven-assembly-plugin</artifactId>
                <version>3.6.0</version>
                <configuration>
                    <tarLongFileMode>gnu</tarLongFileMode>
                    <descriptors>
                        <descriptor>etc/assembly-src.xml</descriptor>
                    </descriptors>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <release>${maven.compiler.release}</release>
                    <show>public</show>
                    <doclint>all,-missing</doclint> <!-- turns off warnings for missing Javadoc -->
                    <verbose>false</verbose>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <configuration>
                    <siteDirectory>src/site</siteDirectory>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jxr-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jxr-plugin</artifactId>
                <reportSets>
                    <reportSet>
                        <id>aggregate</id>
                        <inherited>false</inherited>
                        <reports>
                            <report>aggregate</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <release>${maven.compiler.release}</release>
                    <show>public</show>
                    <doclint>all,-missing</doclint> <!-- turns off warnings for missing Javadoc -->
                    <verbose>false</verbose>
                </configuration>
                <!-- See: https://maven.apache.org/plugins/maven-javadoc-plugin/examples/selective-javadocs-report.html -->
                <reportSets>
                    <reportSet>
                        <id>default</id>
                        <reports>
                            <report>aggregate-no-fork</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
        </plugins>
    </reporting>

</project>
